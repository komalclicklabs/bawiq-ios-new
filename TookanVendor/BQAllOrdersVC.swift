//
//  BQAllOrdersVC.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 10/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQAllOrdersVC: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!
    var currentViewController = BQMyOrdersVC()
    var arrSavedAddress = [BQOrderListModel]()
    @IBOutlet weak var lblNoDataFound: UILabel!
    var isPullToRefreshActive: Bool = false
    var refreshControl = UIRefreshControl()
    
    private var skip: Int = 0
    private var limit: Int = 10
    var isPaginationRequired: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.addPulltoRefresh()
        self.getAllOrders()
    }
    
    private func setupTableView() {
        self.tblView.register(UINib(nibName: "BQMyOderCell", bundle: nil), forCellReuseIdentifier: "BQMyOderCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
    }
    
    
    private func addPulltoRefresh() {
        //Add pullto refresh
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor(red: 246/255, green: 246/255, blue: 246/255, alpha: 1.0)
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        self.tblView.addSubview(refreshControl)
    }
    
    @objc func pullToRefresh() {
        isPullToRefreshActive = true
        self.arrSavedAddress.removeAll()
        self.reset()
        self.getAllOrders()
        
    }
    
    func reset() {
        skip = 0
        limit = 10
    }
    
    func paginationHit() {
        if self.isPaginationRequired == true {
            skip += 10
            self.getAllOrders(isFromPagination: true)
        }
    }
    
    private func getAllOrders(isFromPagination: Bool = false) {
        
        let params = getParamsToDeleteLocation()
        print("Get all orders params:", params)
        if isPullToRefreshActive == true {
             isPullToRefreshActive = false
            ActivityIndicator.sharedInstance.showActivityIndicator()
        }
        HTTPClient.makeConcurrentConnectionWith(method: .POST, para: params, extendedUrl: "get_all_jobs") { (responseObject, error, _, statusCode) in
            
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            
            guard statusCode == .some(STATUS_CODES.SHOW_DATA) else {
                return
            }
            print(responseObject ?? "")
            if let dataRes = responseObject as? [String:Any] {
                if let data = dataRes["data"] as? [String:Any] {
                    if let favLocations = data["jobs"] as? [[String: Any]] {
                        print(favLocations)
                        //self.arrSavedAddress.removeAll()
                        
                        var tempArray = [BQOrderListModel]()
                        for item in favLocations {
                            let rollData = BQOrderListModel.init(param: item)
                            tempArray.append(rollData)
                            //   self.arrSavedAddress.append(notificationData)
                        }
                        
                        if let totalCount = data["total_count"] as? Int, tempArray.count > 0 {
                            self.isPaginationRequired =  totalCount > self.arrSavedAddress.count
                            if !self.isPaginationRequired {
                                self.arrSavedAddress = tempArray
                            } else {
                                self.arrSavedAddress += tempArray
                            }
                        }
                        
//                        for item in favLocations {
//                            let rollData = BQOrderListModel.init(param: item)
//                            self.arrSavedAddress.append(rollData)
//                        }
                        
                        if self.arrSavedAddress.count > 0 {
                            self.lblNoDataFound.isHidden = true
                            self.tblView.isHidden = false
                            self.tblView.reloadData()
                        } else {
                            self.lblNoDataFound.isHidden = false
                            self.tblView.isHidden = true
                        }
                    } else {
                        self.lblNoDataFound.isHidden = false
                        self.tblView.isHidden = true
                    }
                    print(data)
                }
            }
        }
    }
    
    private func getParamsToDeleteLocation() -> [String: Any] {
        return [
            "app_access_token": Vendor.current!.appAccessToken!,
            "limit": limit,
            "skip": skip
        ]
    }
    
    
}


extension BQAllOrdersVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSavedAddress.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BQMyOderCell", for: indexPath) as? BQMyOderCell else {
            return UITableViewCell()
        }
        if self.arrSavedAddress.count > 0 {
            let cellDataModel: BQOrderListModel = self.arrSavedAddress[indexPath.row]
            cell.lblOderID.text = "#\(cellDataModel.category_id ?? 0)"
            
            let amountText = NSMutableAttributedString.init(string: "AED \(cellDataModel.total_price ?? 0.0)")
            
            // set the custom font and color for the 0,1 range in string
            amountText.setAttributes([NSFontAttributeName: UIFont.systemFont(ofSize: 12),
                                      NSForegroundColorAttributeName: UIColor.gray],
                                     range: NSMakeRange(0, 3))
            // if you want, you can add more attributes for different ranges calling .setAttributes many times
            
            // set the attributed string to the UILabel object
            cell.lblTotalPrice.attributedText = amountText
            
            
            
            // cell.lblTotalPrice.text = "AED \(cellDataModel.total_price)"
            cell.lblDate.text = cellDataModel.job_delivery_datetime
            cell.lblStatus.text = cellDataModel.job_status
            cell.lblStatus.backgroundColor = cellDataModel.statusColor
            cell.lblItemnameAndQuantity.text = "Total Items (" + "\(cellDataModel.productListModel.count))"
            cell.btnViewDetails.tag = indexPath.row
            cell.delegate = self
            cell.lblStatus.layer.cornerRadius = 15.0
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellDataModel: BQOrderListModel = self.arrSavedAddress[indexPath.row]
        
        if let vc = UIStoryboard(name: "MyOrders", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQOrderDetailsVC") as? BQOrderDetailsVC {
            vc.orderID = cellDataModel.category_id
            self.currentViewController.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if (tblView.contentOffset.y + tblView.frame.size.height) >= tblView.contentSize.height - 20 {
            self.paginationHit()
        }
    }
    
    
}

extension BQAllOrdersVC: BQMyOderCellDelegate {
    func showOrderDetails(senderTag: Int) {
        print("showOrderDetails")
        let cellDataModel: BQOrderListModel = self.arrSavedAddress[senderTag]
        if let vc = UIStoryboard(name: "MyOrders", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQOrderDetailsVC") as? BQOrderDetailsVC {
            vc.orderID = cellDataModel.category_id
            self.currentViewController.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
