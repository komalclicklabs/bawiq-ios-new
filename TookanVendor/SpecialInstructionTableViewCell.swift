//
//  SpecialInstructionTableViewCell.swift
//  TookanVendor
//
//  Created by Harshit Parikh on 21/06/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class SpecialInstructionTableViewCell: UITableViewCell {

    @IBOutlet weak var specialInstructionLbl: UILabel!
    @IBOutlet weak var specialInstructionTxtView: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
