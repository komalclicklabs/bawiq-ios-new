//
//  CustomFlowLayout.swift
//  DGActivityIndicatorView
//
//  Created by Vishal on 13/02/18.
//

import UIKit

protocol SubCategoryFlowLayouttDelegate: class {
    // 1. Method to ask the delegate for the height of the image
 //   func collectionView(_ collectionView:UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat
}

class SubCategoryFlowLayout: UICollectionViewLayout {
    //1. Pinterest Layout Delegate
    weak var delegate: SubCategoryFlowLayouttDelegate!
    
    //2. Configurable properties
    fileprivate var numberOfColumns = 1
    fileprivate var cellPadding: CGFloat = 6
    
    //3. Array to keep a cache of attributes.
    fileprivate var cache = [UICollectionViewLayoutAttributes]()
    
    //4. Content height and size
    fileprivate var contentHeight: CGFloat = 0
    
    fileprivate var contentWidth: CGFloat = 0
//    {
//        guard let collectionView = collectionView else {
//            return 0
//        }
//        let insets = collectionView.contentInset
//        return collectionView.bounds.width - (insets.left + insets.right)
//    }

    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    override func prepare() {
        // 1. Only calculate once
        guard cache.isEmpty == true, let collectionView = collectionView else {
            return
        }
        // 2. Pre-Calculates the X Offset for every column and adds an array to increment the currently max Y Offset for each column
        var columnWidth = contentWidth /// CGFloat(numberOfColumns)
        var xOffset = [CGFloat]()
        for column in 0 ..< numberOfColumns {
            xOffset.append(CGFloat(column) * columnWidth)
        }
        var column = 0
        var yOffset = [CGFloat](repeating: 0, count: numberOfColumns)
        
        
        for section in 0..<collectionView.numberOfSections {

            if section == 0 {
                 var column = 0
                numberOfColumns = collectionView.numberOfItems(inSection: 0)
                xOffset.removeAll()
                var yOffset = [CGFloat](repeating: 0, count: numberOfColumns)
                columnWidth = collectionView.bounds.width

                for column in 0 ..< numberOfColumns {
                    xOffset.append(CGFloat(column) * columnWidth)
                }
                for item in 0 ..< collectionView.numberOfItems(inSection: 0) {
                    let indexPath = IndexPath(item: item, section: 0)
                    
                    // 4. Asks the delegate for the height of the picture and the annotation and calculates the cell frame.
                    let photoHeight = CGFloat(180)
                    let height = cellPadding * 2 + photoHeight
                    let frame = CGRect(x: xOffset[column],
                                       y: yOffset[column],
                                       width: columnWidth,
                                       height: height)
                    let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
                    
                    // 5. Creates an UICollectionViewLayoutItem with the frame and add it to the cache
                    let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                    attributes.frame = insetFrame
                    cache.append(attributes)
                    // 6. Updates the collection view content height
                    contentHeight = max(contentHeight, frame.maxY)
                    contentWidth = max(contentWidth, frame.maxX)
                    xOffset[column] = xOffset[column] + columnWidth
                  //  yOffset[column] = yOffset[column] + height
                    column = column < (numberOfColumns - 1) ? (column + 1) : 0
                }
            } else if section == 1 {
                 var column = 0
                let numberOfColumns = collectionView.numberOfItems(inSection: 1)
                var xOffset = [CGFloat]()
                var yOffset = [CGFloat](repeating: 0, count: numberOfColumns)
                let columnWidth2 = CGFloat(100)
                
                for column in 0 ..< numberOfColumns {
                    xOffset.append(CGFloat(column) * columnWidth2)
                }
                for item in 0 ..< collectionView.numberOfItems(inSection: 1) {
                    let indexPath = IndexPath(item: item, section: 1)
                    
                    // 4. Asks the delegate for the height of the picture and the annotation and calculates the cell frame.
                    let photoHeight = CGFloat(50)
                    let height = cellPadding * 2 + photoHeight
                    let frame = CGRect(x: xOffset[column],
                                       y: yOffset[column] + CGFloat(200),
                                       width: columnWidth2,
                                       height: height)
                    let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
                    
                    // 5. Creates an UICollectionViewLayoutItem with the frame and add it to the cache
                    let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                    attributes.frame = insetFrame
                    cache.append(attributes)
                    // 6. Updates the collection view content height
                    contentHeight = max(contentHeight, frame.maxY)
                    contentWidth = max(contentWidth, frame.maxX)
                    xOffset[column] = xOffset[column] + columnWidth2
                    //  yOffset[column] = yOffset[column] + height
                    column = column < (numberOfColumns - 1) ? (column + 1) : 0
                }
            } else if section == 2 {
                 var column = 0
                
            } else {
                // 3. Iterates through the list of items in the first section
                numberOfColumns = collectionView.numberOfItems(inSection: 2)
                for item in 0 ..< numberOfColumns {
                    
                    let indexPath = IndexPath(item: item, section: 0)
                    
                    // 4. Asks the delegate for the height of the picture and the annotation and calculates the cell frame.
                    let photoHeight = CGFloat(180) //delegate.collectionView(collectionView, heightForPhotoAtIndexPath: indexPath)
                    let height = cellPadding * 2 + photoHeight
                    let frame = CGRect(x: xOffset[column], y: yOffset[column], width: columnWidth, height: height)
                    let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
                    
                    // 5. Creates an UICollectionViewLayoutItem with the frame and add it to the cache
                    let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                    attributes.frame = insetFrame
                    cache.append(attributes)
                    
                    // 6. Updates the collection view content height
                    contentHeight = max(contentHeight, frame.maxY)
                    yOffset[column] = yOffset[column] + height
                    
                    column = column < (numberOfColumns - 1) ? (column + 1) : 0
                }
            }
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var visibleLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        // Loop through the cache and look for items in the rect
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        return visibleLayoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[indexPath.item]
    }
    
}
