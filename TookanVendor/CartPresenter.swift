//
//  CartPresenter.swift
//  TookanVendor
//
//  Created by Vishal on 16/02/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import Foundation

@objc protocol CartView: class  {
    func reloadTable()
    func refreshCell(at index: Int, isAdded: Bool, cartCount: Int, totalPrice: Float)
    @objc optional func successFullBookingCreate(orderID: Int)
    @objc optional func successFullOrderIDCreate(orderID: String)
    @objc optional func successFullWalletMoneyAddedToCart()
    @objc optional func successFullApplypromoCode(with benefitType: Int, promoValue: Int, promoID: Int)
    func errorInbookingCreation(message: String)
    
}

protocol CartPresenter {
    func numberOfItemsInTable() -> Int
    func setupDataTable(view: ProductCellView, index: Int)
    func setupDataTableForConfirmBooking(view: ProductCellView, index: Int)
    func updateView(view: ProductCellView, index: Int, add: Bool)
    func removeCategoryFromTheCart(view: ProductCellView, index: Int, add: Bool)
    func createBooking(transactionRefID: String, transactionOrderID: String)
    func createdWalletMoney(transactionRefID: String, transactionOrderID: String)
    func getOrderIDForPayment()
    func applyPromoCode(totalPrice: String, promoCode: String)
    func getCartProducts()
}

class CartPresenterImplementation: CartPresenter {

    var products = [CartProducts]()
    weak var view: CartView?
    
    init(withView view: CartView) {
        
        self.view = view
        if view is ResultController {
            
        } else {
            getCartProducts()
        }
    }
    

    func numberOfItemsInTable() -> Int {
        return products.count
    }

    func setupDataTable(view: ProductCellView, index: Int) {
        let product = products[index]
        view.setupData(data: product)
        
    }
    func setupDataTableForConfirmBooking(view: ProductCellView, index: Int) {
        let product = products[index]
        view.setupDataForConfirmBooking(data: product)
        
    }
    
    func getCartProducts() {
        CartProducts.getCartProducts { (success, products, totalPrice) in
            DispatchQueue.main.async {
                if success {
                    guard let _cProducts = products else {
                        return
                    }
                    self.products = _cProducts
                    
                    if let price = totalPrice {
                       // if price > 0 {
                            self.view?.refreshCell(at: 0, isAdded: true, cartCount: _cProducts.count, totalPrice: price)
                        //}
                    }
                    
                    self.view?.reloadTable()
                }
            }
        }
    }
    
    func updateView(view: ProductCellView, index: Int, add: Bool) {
        if add {
            addProductToCart(product: products[index],view: view, index: index)
        } else {
            deleteProductFromCart(product: products[index],view: view, index: index)
        }
        
    }
    func removeCategoryFromTheCart(view: ProductCellView, index: Int, add: Bool) {
        removeCategoryFromTheCart(product: products[index],view: view, index: index)
    }
    func removeCategoryFromTheCart(product: CartProducts, view: ProductCellView, index: Int) {
        product.removeCategoryFormTheCart { (success, response) in
            //if success {
               self.getCartProducts()
            //}
        }
    }
    
    func addProductToCart(product: CartProducts, view: ProductCellView, index: Int) {
        product.addProduct { (success, response) in
            if success {
                view.updateValue(added: true)
                
                if let totalPrice = response!["total_price"] as? Double, let cartCount = response!["cart_count"] as? Int {
                    print(totalPrice, cartCount)
                    self.view?.refreshCell(at: index, isAdded: true, cartCount: cartCount, totalPrice: Float(totalPrice))
                }
            }
        }
    }
    
    func deleteProductFromCart(product: CartProducts, view: ProductCellView, index: Int) {
        product.deleteProduct { (success, response) in
            if success {
                view.updateValue(added: false)
                if let totalPrice = response!["total_price"] as? Double, let cartCount = response!["cart_count"] as? Int {
                    print(totalPrice, cartCount)
                    self.view?.refreshCell(at: index, isAdded: true, cartCount: cartCount, totalPrice: Float(totalPrice))
                }
//                self.view?.reloadTable()
            }
        }
    }
    
    func createdWalletMoney(transactionRefID: String, transactionOrderID: String) {
        self.addMoneyToWalletAPICalled(transactionRefID: transactionRefID, transactionOrderID: transactionOrderID) { (success, response) in
            if success {
                print(response as Any)
                self.view?.successFullWalletMoneyAddedToCart!()
                
                
            } else {
                if let result = response as? [String:Any] {
                    if let error = result["message"] as? String {
                        self.view?.errorInbookingCreation(message: error)
                    } else {
                        self.view?.errorInbookingCreation(message: "Please recharge your wallet.")
                    }
                }
            }
        }
    }
    
    func addMoneyToWalletAPICalled(transactionRefID: String, transactionOrderID: String, receivedResponse: @escaping (_ succeeded: Bool, _ response:[String: Any]?) -> ()) {
        
        
        var params = [String: Any]()
        
        //Required
        params["transaction_amount"] = BQBookingModel.shared.walletMoney
        params["app_access_token"] = Vendor.current!.appAccessToken!
        params["transaction_ref_id"] = transactionRefID
        params["transaction_order_id"] = transactionOrderID
       
        
        
        print("createBookingForCart: \(params)")
        
        NetworkingHelper.sharedInstance.commonServerCall(apiName: "add_amount_in_wallet",
                                                         params: params as [String : AnyObject],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                            DispatchQueue.main.async {
                                                                if succeeded {
                                                                    guard let data = response["data"] as? [String: Any] else {
                                                                        receivedResponse(false,nil)
                                                                        return
                                                                    }
                                                                    receivedResponse(true, data)
                                                                } else {
                                                                    receivedResponse(false,response)
                                                                }
                                                            }
                                                            
        }
    }
    
    func createBooking(transactionRefID: String, transactionOrderID: String) {
        self.createBookingForCart(transactionRefID: transactionRefID, transactionOrderID: transactionOrderID) { (success, response) in
            if success {
                print(response as Any)
                if let dataRes = response {
                    if let job = dataRes["job"] as? [String: Any], let jobID = job["job_id"] as? Int {
                        self.view?.successFullBookingCreate!(orderID: jobID)
                    }
                }
                
                
            } else {
                if BQBookingModel.shared.paymentOption == "Wallet" {
                    if let result = response as? [String: Any] {
                        if let message = result["message"] as? String {
                            self.view?.errorInbookingCreation(message: message)
                        } else {
                            self.view?.errorInbookingCreation(message: ERROR_MESSAGE.SERVER_NOT_RESPONDING)
                        }
                    }
                } else {
                    self.view?.errorInbookingCreation(message: "Something went wrong")
                }
                
            }
        }
    }
    
    func getOrderIDForPayment() {
        self.getOrderID { (success, response) in
            if success {
                print(response as Any)
                if let dataRes = response {
                    if let job = dataRes["order"] as? [String: Any], let jobID = job["order_id"] as? String {
                        self.view?.successFullOrderIDCreate!(orderID: jobID)
                    }
                }
                
                
            }
        }
    }
    
    
    func applyPromoCode(totalPrice: String, promoCode: String) {
        self.applyPromoCodeForBooking(totalPrice: totalPrice, promoCode: promoCode) { (success, response) in
            if success {
                print(response as Any)
                if let dataRes = response {
                    if let job = dataRes["promo_data"] as? [String: Any], let promo_type = job["promo_type"] as? Int, let promo_value = job["promo_value"] as? Int, let promo_id = job["promo_id"] as? Int  {
                        self.view?.successFullApplypromoCode!(with: promo_type, promoValue: promo_value, promoID: promo_id)
                    }
                }
                
                
            } else {
                
                if let dataRes = response {
                    if let message = dataRes["message"] as? String {
                        ErrorView.showWith(message: message, removed: nil)
                    }
                }
                
            }
        }
    }
    
    
    
    func utcToLocal(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if let dt = dateFormatter.date(from: date) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = "dd MMM, yyyy, h:mm a"
            return dateFormatter.string(from: dt)
        }
        return ""
    }
    
    
    func applyPromoCodeForBooking(totalPrice: String, promoCode: String, receivedResponse: @escaping (_ succeeded: Bool, _ response:[String: Any]?) -> ()) {
        guard let lat = BQBookingModel.shared.latitude else {
            return
        }
        guard let long = BQBookingModel.shared.longitude else {
            return
        }
        var params = [String: Any]()
        params["app_access_token"] = Vendor.current!.appAccessToken!
        params["promo_code"] = promoCode
        params["total_price"] = totalPrice
        params["lattitude"] = lat
        params["longitude"] = long
        
        NetworkingHelper.sharedInstance.commonServerCall(apiName: "apply_promo_code",
                                                         params: params as [String : AnyObject],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                            DispatchQueue.main.async {
                                                                if succeeded {
                                                                    guard let data = response["data"] as? [String: Any] else {
                                                                        receivedResponse(false,nil)
                                                                        return
                                                                    }
                                                                    receivedResponse(true, data)
                                                                } else {
                                                                    receivedResponse(false,response)
                                                                }
                                                            }
                                                            
        }
        
    }
    
    func getOrderID(receivedResponse: @escaping (_ succeeded: Bool, _ response:[String: Any]?) -> ()) {
        //get_order_id
        NetworkingHelper.sharedInstance.commonServerCall(apiName: "get_order_id",
                                                         params: [:],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                            DispatchQueue.main.async {
                                                                if succeeded {
                                                                    guard let data = response["data"] as? [String: Any] else {
                                                                        receivedResponse(false,nil)
                                                                        return
                                                                    }
                                                                    receivedResponse(true, data)
                                                                } else {
                                                                    receivedResponse(false,nil)
                                                                }
                                                            }
                                                            
        }
        
    }
    
    func createBookingForCart(transactionRefID: String, transactionOrderID: String, receivedResponse: @escaping (_ succeeded: Bool, _ response:[String: Any]?) -> ()) {
        
        
        var params = [String: Any]()
        
        //Required
        params["job_latitude"] = BQBookingModel.shared.latitude!
        params["job_longitude"] = BQBookingModel.shared.longitude!
        params["customer_username"] = BQBookingModel.shared.userName!
        if let code = BQBookingModel.shared.addressLine3 {
            params["postal_code"] = code
        }
        params["is_scheduled"] = BQBookingModel.shared.isScheduled
        params["app_access_token"] = Vendor.current!.appAccessToken!
        
        params["utcOffset"] = String((NSTimeZone.local.secondsFromGMT()/60))
        if BQBookingModel.shared.paymentOption == "Cash" {
            params["payment_type"] = 1
        } else if BQBookingModel.shared.paymentOption == "Card" {
            params["payment_type"] = 2
            params["transaction_ref_id"] = transactionRefID
            params["transaction_order_id"] = transactionOrderID
//            if BQBookingModel.shared.totalAmountAfterBankCharge != "" {
//                params["transaction_ref_id"] = transactionRefID
//                params["transaction_order_id"] = transactionOrderID
//            }
        } else if BQBookingModel.shared.paymentOption == "Wallet" {
            params["payment_type"] = 3
        }
 
        //Optional
        if BQBookingModel.shared.promoID != 0 {
            params["promo_id"] = BQBookingModel.shared.promoID
        }
        
        //Optional
        if let instruction = BQBookingModel.shared.specialInstruction {
            params["job_description"] = instruction
        }
        
        //Optional
        if let phone = BQBookingModel.shared.phone {
            params["customer_phone"] = phone
        }
        //Optional
        if let email = BQBookingModel.shared.email {
            params["customer_email"] = email
        }
        //Optional
        if BQBookingModel.shared.isScheduled == 1 {
            params["job_delivery_datetime"] = BQBookingModel.shared.dateTime!
        }
        //Optional
        if let landmark = BQBookingModel.shared.addressLine1 {
            params["landmark"] = "Landmark: " + landmark
        }
        
        
        var tempAddString = [String]()
    
        if let buildingNo = BQBookingModel.shared.apartmentNo, !buildingNo.isEmpty {
            tempAddString.append("Apartment Number: " + buildingNo)
        }
        if let floorNo = BQBookingModel.shared.floorNo, !floorNo.isEmpty {
            tempAddString.append("Floor Number: " + floorNo)
        }
        if let doorNo = BQBookingModel.shared.doorNo, !doorNo.isEmpty {
            tempAddString.append("Door Number: " + doorNo)
        }
        if let streetNo = BQBookingModel.shared.streetNo, !streetNo.isEmpty {
            tempAddString.append("Street Number: " + streetNo)
        }
        
        let additionalAddressString = tempAddString.joined(separator: "@")
        
        //Optional
        if let area = BQBookingModel.shared.addressLine2 {
            params["job_address"] = additionalAddressString + "Area: " + area
//            if let additionalAddress = BQBookingModel.shared.addressLine4 {
//                params["job_address"] = additionalAddress + " " + area
//            } else {
//                params["job_address"] = area
//            }
        }
        
        print("createBookingForCart: \(params)")
        
        // V1 API
//        NetworkingHelper.sharedInstance.commonServerCall(apiName: "create_job",
//                                                         params: params as [String : AnyObject],
//                                                         httpMethod: "POST") { (succeeded, response) in
//                                                            DispatchQueue.main.async {
//                                                                if succeeded {
//                                                                    guard let data = response["data"] as? [String: Any] else {
//                                                                        receivedResponse(false,nil)
//                                                                        return
//                                                                    }
//                                                                    receivedResponse(true, data)
//                                                                } else {
//                                                                    receivedResponse(false,response)
//                                                                }
//                                                            }
        
        
//        // V2 API
        NetworkingHelper.sharedInstance.commonServerCall(apiName: "v2/create_job",
                                                         params: params as [String : AnyObject],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                            DispatchQueue.main.async {
                                                                if succeeded {
                                                                    guard let data = response["data"] as? [String: Any] else {
                                                                        receivedResponse(false,nil)
                                                                        return
                                                                    }
                                                                    receivedResponse(true, data)
                                                                } else {
                                                                    receivedResponse(false,response)
                                                                }
                                                            }
        
        }
    }
    
    
}
