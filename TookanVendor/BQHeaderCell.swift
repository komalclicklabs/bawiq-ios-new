//
//  BQHeaderCell.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 13/06/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit
protocol BQHeaderCellDelegate: class {
    func editUserLocationAddress()
}

class BQHeaderCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    weak var delegate: BQHeaderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    @IBAction func editButtonAction(_ sender: UIButton) {
        self.delegate?.editUserLocationAddress()
    }
}
