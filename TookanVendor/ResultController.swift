//
//  ResultController.swift
//  Telr_SDK
//
//  Created by Staff on 5/25/17.
//  Copyright © 2017 Telr. All rights reserved.
//

import UIKit
import TelrSDK

class ResultController: TelrResponseController, CartView {
    func errorInbookingCreation(message: String) {
        statusLabel.text = message
        btnViewDetails.setTitle("Ok", for: .normal)
    }
    
    
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var btnViewDetails: UIButton!
    
    
    var isComingFromWallet = false
    
    var presenter: CartPresenter!
    var navigationBar:NavigationView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.statusLabel.text = ""
        self.setUpPresenter()
        self.setNavigationBar()
        
        self.view.backgroundColor = COLOR.App_Red_COLOR
    }
    
    
    private func setUpPresenter() {
        presenter = CartPresenterImplementation(withView: self)
        if status == "A" {
            if let transReference = tranRef {
                let valueBool = UserDefaults.standard.bool(forKey: "isComingFromWallet")
                if valueBool == true {
                    print("isComingFromWallet")
                    //If it's coming from wallet then add money to waller api called
                    self.presenter.createdWalletMoney(transactionRefID: transReference, transactionOrderID: BQBookingModel.shared.transactionOrderID ?? "")
                } else {
                     //If it's coming from booking then create booking api called api called
                    self.presenter.createBooking(transactionRefID: transReference, transactionOrderID: BQBookingModel.shared.transactionOrderID ?? "")
                }
                
                
            }
        } else {
            let valueBool = UserDefaults.standard.bool(forKey: "isComingFromWallet")
            if valueBool == true {
                statusLabel.text = "Transaction Cancelled."
                btnViewDetails.setTitle("Ok", for: .normal)
            } else {
                statusLabel.text = message!
            }
        }
    }
    
    func successFullWalletMoneyAddedToCart() {
        print("successFullWalletMoneyAddedToCart")
        let valueBool = UserDefaults.standard.bool(forKey: "isComingFromWallet")
        if valueBool == true {
            statusLabel.text = "Amount credited to Bwallet successfully."
            btnViewDetails.setTitle("Ok", for: .normal)
        } else {
            statusLabel.text = message!
        }

//        Singleton.sharedInstance.pushToHomeScreen() {
//            
//        }
    }
    
    func successFullBookingCreate(orderID: Int) {
        print("successFullBookingCreate")
        if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "ConfirmViewController") as? ConfirmViewController {
            vc.orderID = orderID
            
            let aObjNavi = UINavigationController(rootViewController: vc)
            self.present(aObjNavi, animated: false) {

            }
        }
    }
    
    func errorInbookingCreation() {
        print("errorInbookingCreation")
        let valueBool = UserDefaults.standard.bool(forKey: "isComingFromWallet")
        if valueBool == true {
            statusLabel.text = "Transaction failed"
            btnViewDetails.setTitle("Ok", for: .normal)
        } else {
            statusLabel.text = message!
        }

    }
    
    func reloadTable() {
        
    }
    
    func refreshCell(at index: Int, isAdded: Bool, cartCount: Int, totalPrice: Float) {
    
    }
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        //Use for showing two right bar buttons on navigation bar
        
        navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "Payment", leftButtonImage: nil, rightButtonImage: nil, secondRightButtonImage: nil)
            , leftButtonAction: nil,
              rightButtonAction: nil,
              rightButtonSecondAction: nil
        )
        navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
    }
    
    
    @IBAction func backBtnPressed(_ sender: Any) {
        
        
        
   //     self.dismiss(animated: true, completion: nil)
        let valueBool = UserDefaults.standard.bool(forKey: "isComingFromWallet")
        if valueBool == true {
            self.navigationController?.popToRootViewController(animated: false)
            self.dismiss(animated: false) {
                print("Dismiss view controller...")
                
                guard let navControllers = (UIApplication.shared.keyWindow?.rootViewController as? UINavigationController) else {
                    return
                }
                
                for viewController in navControllers.viewControllers where viewController is BQWalletVC {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateWalletBalance"), object: nil)
                    navControllers.popToViewController(viewController, animated: true)
                    break
                }
                navControllers.popViewController(animated: true)
            }
        } else {
            Singleton.sharedInstance.pushToHomeScreen() {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateWalletBalance"), object: nil)
            }
        }
        
//        Singleton.sharedInstance.pushToHomeScreen() {
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateWalletBalance"), object: nil)
//        }
        
    }
    


}
