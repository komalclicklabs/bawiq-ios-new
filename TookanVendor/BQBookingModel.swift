//
//  BQBookingModel.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 09/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQBookingModel: NSObject {
    static var shared = BQBookingModel()
    
    var addressLine1: String?
    var addressLine2: String?
    var addressLine3: String?
    var addressLine4: String?
    var streetNo: String?
    var floorNo: String?
    var doorNo: String?
    var apartmentNo: String?
    
    var latitude: Double?
    var longitude: Double?
    
    var userName: String?
    var email: String?
    var phone: String?
    
    var totalProducts = 0
    var totalAmount: String?
    var totalAmountAfterBankCharge: String?
    var dateTime: String?
    var paymentOption: String?
    var transactionRefID: String?
    var transactionOrderID: String?
    var specialInstruction: String?
    var amountAfterPromoCode: String?
    var walletMoney: String?
    var discountPrice = 0.0
    var promoID = 0
    
    var isScheduled = 2
    private override init() {
        
    }
}
