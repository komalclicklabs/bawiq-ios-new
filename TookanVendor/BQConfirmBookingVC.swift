//
//  BQConfirmBookingVC.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 09/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit
import TelrSDK

class BQConfirmBookingVC: UIViewController, CartView {

    @IBOutlet weak var productTable: UITableView!
    @IBOutlet weak var btnContinue: UIView!
    @IBOutlet weak var txtPromocode: UITextField!
    @IBOutlet weak var btnApplyPromoCode: UIButton!
    var paymentRequest: PaymentRequest?
    
    var products = [Products]()
    var presenter: CartPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpPresenter()
        self.setUpTable()
        self.setUpUIComponents()
        BQBookingModel.shared.promoID = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        presenter.getCartProducts()
        productTable.reloadData()
    }
    
    private func setUpPresenter() {
        presenter = CartPresenterImplementation(withView: self)
        
    }
    private func setUpTable() {
        self.productTable.register(UINib(nibName: "ProductCell", bundle: nil), forCellReuseIdentifier: "ProductCell")
        self.productTable.register(UINib(nibName: "DeliveryAddressCell", bundle: nil), forCellReuseIdentifier: "DeliveryAddressCell")
        self.productTable.register(UINib(nibName: "BQPaymentDetailCell", bundle: nil), forCellReuseIdentifier: "BQPaymentDetailCell")
        self.productTable.register(UINib(nibName: "SpecialInstructionTableViewCell", bundle: nil), forCellReuseIdentifier: "SpecialInstructionTableViewCell")
        self.productTable.delegate = self
        self.productTable.dataSource = self
        self.productTable.rowHeight = UITableViewAutomaticDimension
        self.productTable.estimatedRowHeight = 200
    }
    private func setUpUIComponents() {
        txtPromocode.attributedPlaceholder = NSAttributedString(string: "PROMO CODE",
                                                               attributes: [NSForegroundColorAttributeName: UIColor.white])
        self.btnApplyPromoCode.layer.borderColor = UIColor.white.cgColor
        self.btnApplyPromoCode.layer.borderWidth = 1.0
        self.btnApplyPromoCode.layer.cornerRadius = 18.0
        self.btnContinue.layer.cornerRadius = 26.0
    
    }
    
    @IBAction func continueButtonAction(_ sender: UIButton) {
        
        if BQBookingModel.shared.paymentOption == "Cash" {
            self.presenter.createBooking(transactionRefID: "", transactionOrderID: "")
        } else if BQBookingModel.shared.paymentOption == "Card" {
            if Vendor.current?.email != "" {
                self.presenter.getOrderIDForPayment()
            } else {
                self.updateEmailIDForPayment()
            }
        } else if BQBookingModel.shared.paymentOption == "Wallet" {
            self.presenter.createBooking(transactionRefID: "", transactionOrderID: "")
        }
    }
    
    private func updateEmailIDForPayment() {
        let alertController = UIAlertController(title: "Message", message: "You are not registered with email id. Please update.", preferredStyle: .actionSheet)
        let yesAction = UIAlertAction(title: "Ok", style: .destructive, handler: { (action) -> Void in
            
            if let controler = UIStoryboard(name: "AfterLogin", bundle: Bundle.main).instantiateViewController(withIdentifier: "VendorProfileVC") as? VendorProfileVC {
                controler.isComingFromConfirmBooking = true
                self.navigationController?.pushViewController(controler, animated: true)
            }
        })
        let noAction = UIAlertAction(title: "Not Now", style: .cancel, handler: { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    func successFullOrderIDCreate(orderID: String) {
        
        BQBookingModel.shared.transactionOrderID = orderID
        paymentRequest = self.preparePaymentRequest(orderID: orderID)
        performSegue(withIdentifier: "TelrSegue", sender: paymentRequest)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? TelrController{
            UserDefaults.standard.set(false, forKey: "isComingFromWallet")
            destination.paymentRequest = paymentRequest!
        }
    }
    
    
    private func preparePaymentRequest(orderID: String) -> PaymentRequest{
        
        let paymentReq = PaymentRequest()
        paymentReq.key = "VPbSB@pX6L^QfJfL"
        paymentReq.store = "20528"
        paymentReq.appId = "123456789"
        paymentReq.appName = "Bawiq"
        paymentReq.appUser = "123456"
        paymentReq.appVersion = "0.0.1"
//        paymentReq.transTest = "1"   // DEV/TEST
        paymentReq.transTest = "0"    // CLIENT
        paymentReq.transType = "auth"
        paymentReq.transClass = "paypage"
        paymentReq.transCartid = orderID //String(arc4random())
        paymentReq.transDesc = "Bawiq Order"
        paymentReq.transCurrency = "AED"
        
        //        if let amount = BQBookingModel.shared.amountAfterPromoCode {
        //                paymentReq.transAmount = amount
        //        }
        if let amount = BQBookingModel.shared.totalAmountAfterBankCharge {
            paymentReq.transAmount = amount
        }
        paymentReq.billingEmail = Vendor.current?.email ?? ""
        paymentReq.billingFName = Vendor.current?.firstName ?? ""
        paymentReq.billingLName = Vendor.current?.username ?? ""
        paymentReq.billingTitle = "Mr"
        paymentReq.city = BQBookingModel.shared.addressLine1 ?? ""
        paymentReq.country = "AE" //BQBookingModel.shared.addressLine3 ?? ""
        paymentReq.region = BQBookingModel.shared.addressLine2 ?? ""
        paymentReq.address = BQBookingModel.shared.addressLine2 ?? ""
        return paymentReq
        
//        let paymentReq = PaymentRequest()
//        paymentReq.key = "M4M5q^55fbT-SdLN"
//        paymentReq.store = "20294"
//        paymentReq.appId = "123456789"
//        paymentReq.appName = "Bawiq"
//        paymentReq.appUser = "123456"
//        paymentReq.appVersion = "0.0.1"
//        paymentReq.transTest = "1"
//        paymentReq.transType = "sale"
//        paymentReq.transClass = "paypage"
//        paymentReq.transCartid = orderID //String(arc4random())
//        paymentReq.transDesc = "Bawiq Order"
//        paymentReq.transCurrency = "AED"
//
////        if let amount = BQBookingModel.shared.amountAfterPromoCode {
////                paymentReq.transAmount = amount
////        }
//        if let amount = BQBookingModel.shared.totalAmountAfterBankCharge {
//            paymentReq.transAmount = amount
//        }
//        paymentReq.billingEmail = Vendor.current?.email ?? ""
//        paymentReq.billingFName = Vendor.current?.firstName ?? ""
//        paymentReq.billingLName = Vendor.current?.username ?? ""
//        paymentReq.billingTitle = "Mr"
//        paymentReq.city = BQBookingModel.shared.addressLine1 ?? ""
//        paymentReq.country = "AE" //BQBookingModel.shared.addressLine3 ?? ""
//        paymentReq.region = BQBookingModel.shared.addressLine2 ?? ""
//        paymentReq.address = BQBookingModel.shared.addressLine2 ?? ""
//        return paymentReq
    }
    
    
    @IBAction func addNewItemAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func successFullApplypromoCode(with benefitType: Int, promoValue: Int, promoID: Int) {
        print("successFullApplypromoCode")
        
        let alertController = UIAlertController(title: "", message: "Promocode applied successfully.", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Ok", style: .destructive, handler: { (action) -> Void in
            BQBookingModel.shared.promoID = promoID
            if benefitType == 2 {
                self.calculateFlatPriceAfterApplyingPromocode(with: Double(promoValue))
                
            } else if benefitType == 1 {
                print("benefitType==3")
                self.calculatePercentPriceAfterApplyingPromocode(with: Double(promoValue))
            }
        })
        
        alertController.addAction(yesAction)
        self.present(alertController, animated: true, completion: nil)
    }
    //Calculate flat price 
    private func calculateFlatPriceAfterApplyingPromocode(with promoValue: Double) {
    
        if BQBookingModel.shared.paymentOption == "Card" {
            if let amount = BQBookingModel.shared.totalAmount {
                BQBookingModel.shared.discountPrice = promoValue
                let amountAfterDiscount = Double(amount)! - Double(promoValue)
                let bankFees = (Double(amountAfterDiscount)*5)/100
                if amountAfterDiscount > 0 {
                    BQBookingModel.shared.totalAmountAfterBankCharge = String(amountAfterDiscount) //String(amountAfterDiscount + Double(bankFees))
                } else {
                    BQBookingModel.shared.totalAmountAfterBankCharge = String(amountAfterDiscount)
                   // self.presenter.createBooking(transactionRefID: "", transactionOrderID: "")
                }
            }
        } else {
            if let amount = BQBookingModel.shared.totalAmount {
                BQBookingModel.shared.discountPrice = promoValue
                BQBookingModel.shared.amountAfterPromoCode = String(Double(amount)! - Double(promoValue))
                
            }
        }
        productTable.reloadData()
    }
    
    //Calculate percentage price
    private func calculatePercentPriceAfterApplyingPromocode(with promoValue: Double) {
        if BQBookingModel.shared.paymentOption == "Card" {
            if let amount = BQBookingModel.shared.totalAmount {
                let percentAmount = (Double(amount)!*promoValue)/100
               
                BQBookingModel.shared.discountPrice = percentAmount
                let amountAfterDiscount = Double(amount)! - Double(percentAmount)
                let bankFees = (Double(amountAfterDiscount)*5)/100
                if amountAfterDiscount > 0 {
                    BQBookingModel.shared.totalAmountAfterBankCharge = String(amountAfterDiscount) //String(amountAfterDiscount + Double(bankFees))
                } else {
                    BQBookingModel.shared.totalAmountAfterBankCharge = String(amountAfterDiscount)
                   // self.presenter.createBooking(transactionRefID: "", transactionOrderID: "")
                }
                
//                let amountWithBank = (Double(amount)!*5)/100 + Double(amount)!
//                let percentAmount = (Double(amountWithBank)*promoValue)/100
//                BQBookingModel.shared.discountPrice = percentAmount
//                BQBookingModel.shared.totalAmountAfterBankCharge = String(Double(amountWithBank) - Double(percentAmount))
                
            }
        } else {
            if let amount = BQBookingModel.shared.totalAmount {
                let percentAmount = (Double(amount)!*promoValue)/100
                BQBookingModel.shared.discountPrice = percentAmount
                BQBookingModel.shared.amountAfterPromoCode = String(Double(amount)! - Double(percentAmount))
                
            }
        }
        
         productTable.reloadData()
    }
    
    
    
    
    func successFullBookingCreate(orderID: Int) {
        print("successFullBookingCreate")
        
        let alertController = UIAlertController(title: "", message: "The order has been placed  successfully.", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) -> Void in
            if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "ConfirmViewController") as? ConfirmViewController {
                BQBookingModel.shared.specialInstruction = nil
                BQBookingModel.shared.discountPrice = 0.0
                vc.orderID = orderID
                self.navigationController?.pushViewController(vc, animated: true)
            }
        })
        
        alertController.addAction(yesAction)
        self.present(alertController, animated: true, completion: nil)
    
    }
    
    func errorInbookingCreation(message: String) {
        ErrorView.showWith(message: message, removed: nil)
    }
    
    func reloadTable() {
        self.productTable.reloadData()
    }
    
    @IBAction func applypromoCodeAction(_ sender: Any) {
        self.txtPromocode.resignFirstResponder()
        if txtPromocode.text != "" {
            self.presenter.applyPromoCode(totalPrice: BQBookingModel.shared.totalAmount!, promoCode: txtPromocode.text!)
            txtPromocode.text = ""
        } else {
            ErrorView.showWith(message: "Please enter promocode.", removed: nil)
        }
    }
    func refreshCell(at index: Int, isAdded: Bool, cartCount: Int, totalPrice: Float) {
        BQBookingModel.shared.totalProducts = cartCount
        BQBookingModel.shared.totalAmount = String(totalPrice)
        BQBookingModel.shared.amountAfterPromoCode = String(totalPrice)
    }
}

extension BQConfirmBookingVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Cart"
            
        } else if section == 1 {
            return "Address Details"
        }
        if let checkSI = BQBookingModel.shared.specialInstruction {
            if section == 2 {
                return "Special Instructions"
            }
        }
        return "Payment Details"
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1.0)
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let checkSI = BQBookingModel.shared.specialInstruction {
            return 4
        }
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return presenter.numberOfItemsInTable()
        }
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as? ProductCell else {
                return UITableViewCell()
            }
            cell.leftProductDescription.isHidden = false
            cell.productDescription.isHidden = true
            cell.delegate = self
            cell.deleteButton.isHidden = false
            cell.favBtn.isHidden = true
            presenter.setupDataTableForConfirmBooking(view: cell, index: indexPath.row)
            return cell
        } else if indexPath.section == 1 {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryAddressCell", for: indexPath) as? DeliveryAddressCell else {
                return UITableViewCell()
            }
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            cell.title.text = ""
            cell.setUserData()
            cell.btnDelete.isHidden = true
            cell.delegate = self
            return cell
            
        }
        if let checkSI = BQBookingModel.shared.specialInstruction {
            if indexPath.section == 2 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SpecialInstructionTableViewCell", for: indexPath) as? SpecialInstructionTableViewCell else {
                    return UITableViewCell()
                }
                cell.specialInstructionTxtView.text = BQBookingModel.shared.specialInstruction
                return cell
            }
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BQPaymentDetailCell", for: indexPath) as? BQPaymentDetailCell else {
            return UITableViewCell()
        }
        cell.setupDataForPayment()
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       
//        if indexPath.section == 1 {
//            return UITableViewAutomaticDimension
//        }
        if let checkSI = BQBookingModel.shared.specialInstruction?.trimText, !checkSI.isEmpty {
            if indexPath.section == 2 {
                return 100
            } else if indexPath.section == 3 {
                return 135
            }
        } else if indexPath.section == 2 {
            return 135
        }
        
        return UITableViewAutomaticDimension
    }
}


extension BQConfirmBookingVC: BQUpdateAddressVCDelegate {
    func saveUserContactAddress() {
        productTable.reloadData()
    }
}
extension BQConfirmBookingVC: DeliveryAddressDelegate {
    func editAddress() {
        if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQUpdateAddressVC") as? BQUpdateAddressVC {
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension BQConfirmBookingVC: ProductCellDelegate {
    func productCellAddButtonTapped(cell: ProductCell) {
        if let index = productTable.indexPath(for: cell) {
            presenter.updateView(view: cell, index: index.row, add: true)
        }
    }
    func productCellRemoveCategoryButtonTapped(cell: ProductCell){
        print("productCellRemoveCategoryButtonTapped")
    }
    
    func productCellDeleteButtonTapped(cell: ProductCell) {
        if let index = productTable.indexPath(for: cell) {
            presenter.updateView(view: cell, index: index.row, add: false)
        }
    }
    func productCellFavButtonTapped(cell: ProductCell, isfavStatus: Bool) {
        
    }
}


