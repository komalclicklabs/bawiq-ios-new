//
//  cartViewCell.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 26/04/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class cartViewCell: UITableViewCell {

    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var productLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        price.font = UIFont(name: FONT.regular, size: FONT_SIZE.priceFontSize)
        price.textColor = COLOR.SPLASH_TEXT_COLOR
        self.selectionStyle = .none
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellLabelsText(quantity:Int,productName:String,finalAmmount:Double){
        let attributedString = NSMutableAttributedString(string: "\(quantity)X" + " ", attributes: [NSFontAttributeName:UIFont(name: FONT.light, size: 18)!, NSForegroundColorAttributeName:COLOR.THEME_FOREGROUND_COLOR])
        let secondAttributedString = NSMutableAttributedString(string: "\(productName)", attributes: [NSFontAttributeName:UIFont(name: FONT.light, size: 16)!, NSForegroundColorAttributeName:COLOR.SPLASH_TEXT_COLOR])
        attributedString.append(secondAttributedString)
        if quantity != 0{
        productLabel.attributedText = attributedString
        }else{
            productLabel.attributedText = secondAttributedString
        }
        price.text = "\(Singleton.sharedInstance.formDetailsInfo.currencyid) " + "\(finalAmmount)"
    }

}
