//
//  AddNewItemTableViewCell.swift
//  TookanVendor
//
//  Created by Harshit Parikh on 22/10/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

protocol AddNewItemProtocol: class {
    func addNewItemAction()
}

class AddNewItemTableViewCell: UITableViewCell {

    weak var delegate: AddNewItemProtocol?
    
    @IBAction func addNewItemAction(_ sender: Any) {
        self.delegate?.addNewItemAction()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
