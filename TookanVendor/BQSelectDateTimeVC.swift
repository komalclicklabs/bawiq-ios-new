//
//  BQSelectDateTimeVC.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 08/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

protocol BQSelectDateTimeVCDelegate: class {
    func bookingDate(date: String)
    func showError(message: String)
}
class BQSelectDateTimeVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var txtDate: UITextField! {
        didSet {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM dd, yyyy"
            let calendar = Calendar.current
            let now = NSDate()
            let nowDateValue = now as Date
            let todayAtNineAM = calendar.date(bySettingHour: 7, minute: 0, second: 0, of: nowDateValue, matchingPolicy: .strict, repeatedTimePolicy: .first, direction: .forward)
            let todayAtTSevenPM = calendar.date(bySettingHour: 23, minute: 0, second: 0, of: nowDateValue, matchingPolicy: .strict, repeatedTimePolicy: .first, direction: .forward)
            
            if nowDateValue >= todayAtNineAM! && nowDateValue <= todayAtTSevenPM! {
                // date is in range
                self.selectedDate = Date()
                txtDate.text = dateFormatter.string(from: Date())
            } else {
                self.selectedDate = Date().addingTimeInterval(24*60*60)
                txtDate.text = dateFormatter.string(from: Date().addingTimeInterval(24*60*60))
            }
        }
    }
    @IBOutlet weak var txtTime: UITextField!
    @IBOutlet weak var txtAM: UITextField!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnCross: UIButton!
    @IBOutlet weak var btnSetDateTime: UIButton!
    weak var delegate: BQSelectDateTimeVCDelegate?
    
    var selectedDate: Date?
    
    var navigationBar:NavigationView!
    var datePicker = UIDatePicker()
    var isFromAddItem: Bool = false
    var messageStr = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if isFromAddItem {
            self.txtTime.isHidden = true
            self.txtAM.isHidden = true
            self.txtDate.placeholder = "Select Date of Birth"
            self.messageLbl.numberOfLines = 0
            self.messageLbl.text = messageStr
        }
        
        self.setUIComponenets()
    }

    
    private func setUIComponenets() {
        btnSetDateTime.layer.cornerRadius = 20.0
        bgView.layer.cornerRadius = 15.0
    }
    
    
    @IBAction func dateButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func timeButtomPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func crossButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func setDateTimeButtonPressed(_ sender: Any) {
        self.dismiss(animated: true) {
            var completeDateTime = ""
            if let date = self.txtDate.text, let time = self.txtTime.text, let am = self.txtAM.text {
            
                if self.isFromAddItem {
                    if date.isEmpty {
                        self.delegate?.showError(message: "Please select your date of birth.")
                        return
                    }
                    completeDateTime = date
                    let age = self.calcAge(birthday: completeDateTime)
                    if age < 21 {
                        self.delegate?.showError(message: "You must be 21 years above to place this order.")
                    } else {
                        self.delegate?.bookingDate(date: completeDateTime)
                    }
                } else {
                    if date.isEmpty || time.isEmpty || am.isEmpty {
                        self.delegate?.showError(message: "Please select any date and time.")
                        return
                    }
                    completeDateTime = date + " " + time + " " + am
                    self.delegate?.bookingDate(date: completeDateTime)
                }
            }
        }
    }
    
    func calcAge(birthday: String) -> Int {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "MMM dd, yyyy"  // Jun 23, 2018
        if let birthdayDate = dateFormater.date(from: birthday) {
            if let calendar: NSCalendar = NSCalendar(calendarIdentifier: .gregorian) {
                let now = Date()
                let calcAge = calendar.components(.year, from: birthdayDate, to: now, options: [])
                if let age = calcAge.year {
                    return age
                }
            }
        }
        return 0
    }
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        //Use for showing two right bar buttons on navigation bar
        navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "Cart", leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"), rightButtonImage: nil, secondRightButtonImage: nil)
            , leftButtonAction: {[weak self] in
                self?.backAction()
            }, rightButtonAction: nil,
               rightButtonSecondAction: nil
        )
        navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
    }
    
    // MARK: - IBAction
    func backAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.inputView = datePicker
        if textField == txtDate {
            txtAM.text = ""
            txtTime.text = ""
            datePicker.datePickerMode = .date
            if isFromAddItem {
                datePicker.maximumDate = NSDate() as Date
            } else {
                datePicker.minimumDate = NSDate() as Date
            }
            datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
        } else {
//            datePicker.datePickerMode = .time
//            datePicker.addTarget(self, action: #selector(timePickerValueChanged), for: UIControlEvents.valueChanged)
            
            // Ordering time can be between 7 AM to 11 PM
            let timePickerView: UIDatePicker = UIDatePicker()
            timePickerView.datePickerMode = UIDatePickerMode.time
            timePickerView.minuteInterval = 30
            let startHour: Int = 7   // 7 am
            let endHour: Int = 23    // 11 pm
            let date1: Date = Date()
            if let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian) {
                var components: DateComponents = gregorian.components(([.day, .month, .year]), from: date1)
                let hour = gregorian.component(.hour, from: date1)
                let minutes = gregorian.component(.minute, from: date1)
                let seconds = gregorian.component(.second, from: date1)
                
                //                components.hour = startHour
                //                components.minute = 0
                //                components.second = 0
                if let date = self.selectedDate, date > Date() {
                    components.hour = startHour
                    components.minute = 0
                    components.second = 0
                } else {
                    components.hour = hour
                    components.minute = minutes
                    components.second = seconds
                }
                let startDate: Date = gregorian.date(from: components)!
                components.hour = endHour
                components.minute = 0
                components.second = 0
                let endDate: Date = gregorian.date(from: components)!
                timePickerView.minimumDate = startDate.addingTimeInterval(60 * 30)
                timePickerView.maximumDate = endDate
                
                textField.inputView = timePickerView
                timePickerView.addTarget(self, action: #selector(timePickerValueChanged), for: .valueChanged)
            }

        }
    }
    
    func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        self.selectedDate = sender.date
        txtDate.text = dateFormatter.string(from: sender.date)
    }
    
    func timePickerValueChanged(sender: UIDatePicker) {
//        let dateFormatter = DateFormatter()
//        dateFormatter.timeStyle = DateFormatter.Style.short
//        let time = String(dateFormatter.string(from: sender.date).suffix(2))
//        txtAM.text = time
//        let tempTime = dateFormatter.string(from: sender.date)
//        let timeArr = tempTime.components(separatedBy: " ")
//        txtTime.text = timeArr[0] //dateFormatter.string(from: sender.date).dropLast(2)
        
        if let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian) {
            var currentDateComponent: DateComponents = gregorian.components(([.day]), from: Date())
            
            if let date = self.selectedDate {
                var selectedDateComponent: DateComponents = gregorian.components(([.day]), from: date)
                if selectedDateComponent.day == currentDateComponent.day {
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeStyle = .short
                    let time = String(dateFormatter.string(from: sender.date).suffix(2))
                    txtAM.text = time
                    let tempTime = dateFormatter.string(from: sender.date)
                    let timeArr = tempTime.components(separatedBy: " ")
                    txtTime.text = timeArr[0] //dateFormatter.string(from: sender.date).dropLast(2)
                    //        txtTime.text = dateFormatter.string(from: sender.date)
                } else {
                    if date > Date() {
                        let dateFormatter = DateFormatter()
                        dateFormatter.timeStyle = .short
                        let time = String(dateFormatter.string(from: sender.date).suffix(2))
                        txtAM.text = time
                        let tempTime = dateFormatter.string(from: sender.date)
                        let timeArr = tempTime.components(separatedBy: " ")
                        txtTime.text = timeArr[0] //dateFormatter.string(from: sender.date).dropLast(2)
                        //        txtTime.text = dateFormatter.string(from: sender.date)
                    }

                }
            }
            
        }
        
//        if let date = self.selectedDate, date > Date() {
//            let dateFormatter = DateFormatter()
//            dateFormatter.timeStyle = .short
//            let time = String(dateFormatter.string(from: sender.date).suffix(2))
//            txtAM.text = time
//            let tempTime = dateFormatter.string(from: sender.date)
//            let timeArr = tempTime.components(separatedBy: " ")
//            txtTime.text = timeArr[0] //dateFormatter.string(from: sender.date).dropLast(2)
//            //        txtTime.text = dateFormatter.string(from: sender.date)
//        }
        
    }
    
}

extension String {
    func dropLast(_ n: Int = 1) -> String {
        return String(dropLast(n))
    }
    var dropLast: String {
        return dropLast()
    }
}

