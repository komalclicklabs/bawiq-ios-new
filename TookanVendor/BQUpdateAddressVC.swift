//
//  BQUpdateAddressVC.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 09/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import QuartzCore

protocol BQUpdateAddressVCDelegate: class {
    func saveUserContactAddress()
}


class BQUpdateAddressVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnSave: UIButton!
    
    weak var delegate: BQUpdateAddressVCDelegate?
    
    var navigationBar:NavigationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(setUserLocationFromMap), name: NSNotification.Name(rawValue: "AddUserLocation"), object: nil)
        
        self.setupUIComponents()
        self.registerTableViewCells()
        self.setNavigationBar()
    }
    
    private func setupUIComponents() {
        btnSave.layer.cornerRadius = 25
        
    }
    
    private func registerTableViewCells() {
        tblView.registerCellWith(nibName: "BQUpdateAddressCell", reuseIdentifier: "BQUpdateAddressCell")
        tblView.registerCellWith(nibName: "BQHeaderCell", reuseIdentifier: "BQHeaderCell")
        
    }
    
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        
        if let name = BQBookingModel.shared.userName {
            guard name.length > 0 else {
                ErrorView.showWith(message: "Please enter name", removed: nil)
                return
            }
        }
        
        if let email = BQBookingModel.shared.email {
            if !Singleton.sharedInstance.validateEmail(email) {
                ErrorView.showWith(message: ERROR_MESSAGE.INVALID_EMAIL, removed: nil)
                return
            }
        }
        
        if let email = BQBookingModel.shared.phone {
            if !Singleton.sharedInstance.validatePhoneNumber(phoneNumber: email) {
                ErrorView.showWith(message: ERROR_MESSAGE.INVALID_PHONE_NUMBER, removed: nil)
                return
            }
        }
        
        self.delegate?.saveUserContactAddress()
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        //Use for showing two right bar buttons on navigation bar
        
        navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "Update Address", leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"), rightButtonImage: nil, secondRightButtonImage: nil)
            , leftButtonAction: {[weak self] in
                self?.backAction()
            }, rightButtonAction: nil,
               rightButtonSecondAction: nil
        )
        navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
    }
    
    // MARK: - IBAction
    func backAction() {
        BQBookingModel.shared.email = Vendor.current?.email ?? ""
        BQBookingModel.shared.phone = Vendor.current?.phoneNo ?? ""
        BQBookingModel.shared.userName = Vendor.current?.username ?? ""
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension BQUpdateAddressVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 67
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        view.tintColor = UIColor.clear
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let addressCell = tableView.dequeueReusableCell(withIdentifier: "BQHeaderCell") as! BQHeaderCell
        if section == 0 {
            addressCell.lblTitle.text = "Point of Contact"
            addressCell.btnEdit.isHidden = true
        } else {
            addressCell.btnEdit.isHidden = false
            addressCell.lblTitle.text = "Delivery Address"
        }
        addressCell.delegate = self
        return addressCell
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        if section == 0 {
//            return "Point of Contact"
//        }
//        return "Delivery Address"
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let addressCell = tableView.dequeueReusableCell(withIdentifier: "BQUpdateAddressCell") as! BQUpdateAddressCell
        if indexPath.section == 0 {
            addressCell.imageIconLeadingConstraint.constant = 31
            addressCell.imageIconWidthConstraint.constant = 25
            addressCell.txtinputField.isUserInteractionEnabled = true
            if indexPath.row == 0 {
                if let userName = BQBookingModel.shared.userName {
                    addressCell.txtinputField.text = userName
                }
                
                addressCell.imgIcon.image = #imageLiteral(resourceName: "avatarRed")
            } else if indexPath.row == 1 {
                if let email = BQBookingModel.shared.email {
                    addressCell.txtinputField.text = email
                } else {
                    addressCell.txtinputField.text = "Email Address"
                }
                
                addressCell.imgIcon.image = #imageLiteral(resourceName: "mail")
            } else {
                addressCell.txtinputField.setFormatting("XXX-XXX-XXXX", replacementChar: "X")
                if let phone = BQBookingModel.shared.phone {
                    if phone.count < 10 {
                        addressCell.txtinputField.setFormatting("XX-XXX-XXXX", replacementChar: "X")
                        addressCell.txtinputField.text = phone.applyPatternOnNumbers(pattern: "##-###-####", replacmentCharacter: "#")
                    } else {
                        addressCell.txtinputField.text = phone.applyPatternOnNumbers(pattern: "###-###-####", replacmentCharacter: "#")
                    }
                } else {
                    addressCell.txtinputField.text = "Phone Number e.g. (052-993-3645)"
                }
                
                //phoneNumberField.setFormatting("XX-XXX XXXX", replacementChar: "X")
                addressCell.imgIcon.image = #imageLiteral(resourceName: "smartphone")
            }
            
        } else {
            addressCell.txtinputField.isUserInteractionEnabled = false
            addressCell.imageIconLeadingConstraint.constant = 20
            addressCell.imageIconWidthConstraint.constant = 0
            if indexPath.row == 0 {
                addressCell.txtinputField.text = BQBookingModel.shared.addressLine1
            } else if indexPath.row == 1 {
                addressCell.txtinputField.text = BQBookingModel.shared.addressLine2
            } else {
                addressCell.txtinputField.text = BQBookingModel.shared.addressLine3
            }
        }
        addressCell.txtinputField.tag = indexPath.row
        addressCell.delegate = self
        return addressCell
        
        
    }
}

extension BQUpdateAddressVC : BQHeaderCellDelegate, BQUserSavedLocationsVCDelegate {
    func editUserLocationAddress() {
        if let vc = UIStoryboard(name: "Booking", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQUserSavedLocationsVC") as? BQUserSavedLocationsVC {
            vc.isComingFromCart = true
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func selectedAddress(address: BQFavoriteAddressModel) {
        if let locType = address.locType {
            
            
            //Deepak App is getting crashed after getting any nil value in address
            if let landmark = address.landmark {
                BQBookingModel.shared.addressLine1 = landmark
            }
            
            if let postalCode = address.postal_code {
                BQBookingModel.shared.addressLine3 = postalCode
            }
            
            if let area = address.address {
                BQBookingModel.shared.addressLine2 = area
            }
            
            var tempAddString = [String]()
            let buildingNo = address.building_number ?? ""
            let floorNo = address.floor_number ?? ""
            let doorNo = address.door_number ?? ""
            let streetNo = address.street_number ?? ""
            
            if !buildingNo.isEmpty {
                tempAddString.append(buildingNo)
                BQBookingModel.shared.apartmentNo = buildingNo
            }
            if !floorNo.isEmpty {
                tempAddString.append(floorNo)
                BQBookingModel.shared.floorNo = floorNo
            }
            if !doorNo.isEmpty {
                tempAddString.append(doorNo)
                BQBookingModel.shared.doorNo = doorNo
            }
            if !streetNo.isEmpty {
                tempAddString.append(streetNo)
                BQBookingModel.shared.streetNo = streetNo
            }
            
            let additionalAddressString = tempAddString.joined(separator: ", ")

            BQBookingModel.shared.addressLine4 = additionalAddressString
            
            BQBookingModel.shared.latitude = Double("\(address.latitude ?? "")")
            BQBookingModel.shared.longitude = Double("\(address.longitude ?? "")")
            self.tblView.reloadData()
        }
        
    }
    func setUserLocationFromMap(notification: NSNotification) {
        print("setUserLocationFromMap")
        if let userAddress = notification.object as? GMSReverseGeocodeResponse {
            // do something with your object
            print(userAddress)
            
            if (userAddress.results()?.count)! > 0{
                if userAddress.results()?[0].lines?.count != 0 {
                    if let lines = userAddress.firstResult()?.lines {
                        
                        //Deepak App is getting crashed after getting any nil value in address
                        if let thoroughfare = userAddress.results()?[0].thoroughfare {
                            BQBookingModel.shared.addressLine1 = thoroughfare
                        }
                        
                        if let postalCode = userAddress.results()?[0].postalCode {
                            BQBookingModel.shared.addressLine3 = postalCode
                        }
                        
                        
                        var middleAddress = ""
                        if let subLocality = userAddress.results()?[0].subLocality {
                            middleAddress = subLocality
                        }
                        if let locality = userAddress.results()?[0].locality {
                            if middleAddress.length != 0 {
                                middleAddress = middleAddress + ", " + locality
                            } else {
                                middleAddress = locality
                            }
                        }
                        if let country = userAddress.results()?[0].country {
                            if middleAddress.length != 0 {
                                middleAddress = middleAddress + ", " + country
                            } else {
                                middleAddress = country
                            }
                        }
                        BQBookingModel.shared.addressLine2 = middleAddress
                        
                        BQBookingModel.shared.latitude = userAddress.results()?[0].coordinate.latitude
                        BQBookingModel.shared.longitude = userAddress.results()?[0].coordinate.longitude
                        
                        tblView.reloadData()
                    }
                }
            }
        }
    }
    
}

extension BQUpdateAddressVC: BQUpdateAddressCellDelegate {
    
    func updateUserData(textFieldTag: Int, textValue: String) {
        if textFieldTag == 0 {
            BQBookingModel.shared.userName = textValue
        } else if textFieldTag == 1 {
            BQBookingModel.shared.email = textValue
        } else {
            BQBookingModel.shared.phone = textValue
        }
    }
}
