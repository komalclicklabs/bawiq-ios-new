//
//  BQPaymentDetailCell.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 10/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit
import QuartzCore

class BQPaymentDetailCell: UITableViewCell {

    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblBankFee: UILabel!
    @IBOutlet weak var lblPaymentMode: UILabel!
    @IBOutlet weak var lblItmesAmount: UILabel!
    @IBOutlet weak var lblItemCount: UILabel!
    @IBOutlet weak var lblDiscountAmount: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var bankFeesStack: UIStackView!
    @IBOutlet weak var discountStack: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bgView.layer.backgroundColor = UIColor.white.cgColor
        discountStack.isHidden = true
        bankFeesStack.isHidden = true
       // bgView.layer.masksToBounds = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupDataForBookingPaymentDetails(orderModel: BQOrderListModel) {
        print("\(orderModel.productListModel.count)")
        lblItemCount.text = "Items (" + "\(orderModel.productListModel.count)" + ")"
        lblTotalAmount.text = "AED " + "\(orderModel.payableAmount ?? 0.0)"
//        lblTotalAmount.text = "AED " + "\(orderModel.total_price ?? 0.0)"
        if let paymentType = orderModel.payment_type {
            if paymentType == "CASH" {
                bankFeesStack.isHidden = true
            } else if paymentType == "CARD" {
                bankFeesStack.isHidden = true //false
                if let bankFee = orderModel.bank_fee {
                    lblBankFee.text = "AED " + "\(bankFee)"
                }
//                lblBankFee.text = "AED " + "\(orderModel.bank_fee ?? 0.0)"
            } else {
                bankFeesStack.isHidden = true
            }
        }
        
        if let promoAmount = orderModel.promo_value {
            if promoAmount > 0 {
                discountStack.isHidden = false
                lblDiscountAmount.text = "AED " + "\(promoAmount)"
            } else {
                discountStack.isHidden = true
            }
        }
        
        lblPaymentMode.text = orderModel.payment_type
        lblItmesAmount.text = "AED " + "\(orderModel.total_price ?? 0.0)"
        
    }
    
    func setupDataForPayment() {
        lblItemCount.text = "Items (" + "\(BQBookingModel.shared.totalProducts)" + ")"
        lblItmesAmount.text = "AED " + "\(BQBookingModel.shared.totalAmount ?? "")"
        let discount = BQBookingModel.shared.discountPrice
        if discount > 0 {
            discountStack.isHidden = false
            lblDiscountAmount.text = "AED " + "\(BQBookingModel.shared.discountPrice)"
        } else {
            discountStack.isHidden = true
        }
        
        lblPaymentMode.text = BQBookingModel.shared.paymentOption
        
        if BQBookingModel.shared.paymentOption == "Card" {
            bankFeesStack.isHidden = true //false
            lblBankFee.text = "5%"
            if let amount = BQBookingModel.shared.totalAmountAfterBankCharge {
                lblTotalAmount.text = "AED " + "\(amount)"
                
            } else {
                if let amount = BQBookingModel.shared.totalAmount {
                    lblTotalAmount.text = "AED " + "\(amount)"
                }
            }
            
            
        } else {
            bankFeesStack.isHidden = true
            lblBankFee.text = "0%"
            if let amount = BQBookingModel.shared.amountAfterPromoCode {
                lblTotalAmount.text = "AED " + "\(amount)"
            }
        }
        
    }
}
