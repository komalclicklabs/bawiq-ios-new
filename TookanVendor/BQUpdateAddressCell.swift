//
//  BQUpdateAddressCell.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 09/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit
protocol BQUpdateAddressCellDelegate: class {
    func updateUserData(textFieldTag: Int, textValue: String)
}


class BQUpdateAddressCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var txtinputField: VSTextField!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var imageIconWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageIconLeadingConstraint: NSLayoutConstraint!
    weak var delegate: BQUpdateAddressCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func textFieldChangeValues(_ sender: UITextField) {
        if let textValue = sender.text {
            self.delegate?.updateUserData(textFieldTag: sender.tag, textValue: textValue)
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case txtinputField:
            txtinputField.setFormatting("XXX-XXX-XXXX", replacementChar: "X")
            if textField.text == "" {
                //            if string == "0" {
                //                phoneField.text = string
                //            } else {
                //                phoneField.text = "0" + string
                //            }
                txtinputField.text = "0" + string
                return false
            }
            return true
        default:
            break
        }
        return true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
