//
//  ConversationsViewController.swift
//  Fugu
//
//  Created by CL-macmini-88 on 5/9/17.
//  Copyright © 2017 CL-macmini-88. All rights reserved.
//

import UIKit
import FayeSwift
import Photos

protocol NewChatSentDelegate {
    func newChatStartedDelgegate(isChatUpdated: Bool)
}

let imageCacheFugu = NSCache<AnyObject, AnyObject>()

public class ConversationsViewController: UIViewController {

    // MARK: OUTLETS
    
    @IBOutlet var navigationBackgroundView: UIView!
    @IBOutlet var navigationTitleLabel: UILabel!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var navigationHeaderView: UIView!
    @IBOutlet var chatScreenTableView: UITableView!
    @IBOutlet var sendMessageButton: UIButton!
    @IBOutlet var messageTextView: UITextView!
    @IBOutlet var textViewBackGroundViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var textViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var isTypingLabel: UILabel!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var errorLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet var textViewBgView: UIView!
    @IBOutlet var placeHolderLabel: UILabel!
    @IBOutlet var addFileButtonAction: UIButton!
    @IBOutlet var loaderbutton: UIButton!
    @IBOutlet var seperatorView: UIView!
    
    // MARK: PROPERTIES
    
    var delegate: NewChatSentDelegate? = nil
    var keyboardHeight : CGFloat = 0
    var channelName: String? = ""
    
    var returnImage:UIImage = UIImage()
    let textViewFixedHeight = 50
    let textViewPlaceHolder = "Send a message..."
    let activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    var client = FayeClient(aFayeURLString: FuguConfig.shared.fayeBaseURLString!, channel: "")
    
    var userDetailsObj = [String: Any]()
    var dateGroupedArray = [[String: Any]]()
    var messagesArray = [[String : Any]]()
    var channelId = "-1"
    
    var typingMessageValue = TypingMessage.messageRecieved.rawValue
    enum TypingMessage: Int {
        case messageRecieved = 0, startTyping, stopTyping
    }

    var userSubscribedValue = UserSubscriptionStatus.notSubscribed.rawValue
    
    enum UserSubscriptionStatus: Int {
        case notSubscribed = 0, subscribed
    }
    
    enum ReadUnReadStatus: Int {
        case none = 0, sent, delivered, read
    }
    
    enum MessageType: Int {
        case none = 0, normal = 1, forFile = 10
    }
    
    var textInTextField = ""
    var userName = "Anonymous"
    var willPaginationWork = false
    var timer = Timer()
    var isChatDeactivated = false
    let userType = 1 //Means it is a mobile user else a Web user.
    let messageType = 1
    var labelId = -1
    let accessoryView = UIView()
    let accessoryTextView = UITextView()
    var sendMessageButtonInAccessoryView = UIButton()
    
    var imagePicker = UIImagePickerController()
    var isGettingResponse = false
    var filePath: String? = ""
    var directoryPath: String? = ""
    var chatImagetag = 0
    var messageArrayCount = 0
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: LIFECYCLE
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        navigationSetUp()
        uiSetUp()
        tableViewSetUp()
        self.messageTextView.textAlignment = .justified
        errorLabel.text = ""
        if errorLabelTopConstraint != nil {
            errorLabelTopConstraint.constant = -20
        }
        
        sendMessageButton.isEnabled = false
        
        client.delegate = self
        client.connectToServer()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground(_:)), name: .UIApplicationWillEnterForeground, object: nil)
        
        if FuguConfig.shared.pushInfo.keys.count > 0 {
            
            if let userIdPush = FuguConfig.shared.pushInfo["user_id"] as? Int {
                UserDefaults.standard.set(userIdPush, forKey: FUGU_USER_ID)
            }
            
            if let channelId = FuguConfig.shared.pushInfo["channel_id"] as? Int {
                self.channelId = "/\(channelId)"
                
                if !client.fayeConnected! {
                    self.client = FayeClient(aFayeURLString: FuguConfig.shared.fayeBaseURLString!, channel: "/\(channelId)")
                    self.client.delegate = self
                    self.client.connectToServer()
                }
                
                getMessagesBasedOnChannel(channelId, pageStart: 1)
            }
            FuguConfig.shared.pushInfo = [:]
        }
        if !isChatDeactivated {
           self.messageTextView.becomeFirstResponder()
        }
        
    }
    
    override public func viewWillAppear(_ animated: Bool) {

        messageTextView.contentInset.top = 8

        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        
    }
    
    override public func viewWillDisappear(_ animated: Bool) {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func willEnterForeground(_ notification: NSNotification!) {
        
        if !client.fayeConnected! {
            self.client = FayeClient(aFayeURLString: FuguConfig.shared.fayeBaseURLString!, channel: "\(channelId)")
            self.client.delegate = self
            self.client.connectToServer()
        }
        
    }
    
    func tableViewSetUp() {
        
        self.automaticallyAdjustsScrollViewInsets = false
        var contentInset:UIEdgeInsets = self.chatScreenTableView.contentInset
        contentInset.top = 5.0
        self.chatScreenTableView.contentInset = contentInset
        
    }
  
    func uiSetUp() {

        directoryPath = pathToDocumentsDirectory() + "/Feedback"
        
        createImgDirectory("Feedback")
        deleteContentsOfFolder(directoryPath!)


    }
    
    func showTextViewBasedOnChatStatus() {
        
        if textViewBgView == nil {
            return
        }
        
        if isChatDeactivated {
            textViewBgView.isHidden = true
            textViewBackGroundViewHeightConstraint.constant = 0
        } else {
            textViewBgView.isHidden = false
            textViewBackGroundViewHeightConstraint.constant = CGFloat(textViewFixedHeight)
        }
        
    }
    
    func navigationSetUp() {
        
        navigationBackgroundView.layer.shadowColor = UIColor.black.cgColor
        navigationBackgroundView.layer.shadowOpacity = 0.25
        navigationBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        navigationBackgroundView.layer.shadowRadius = 4
        
        if FuguConfig.shared.navigationHeaderViewBackgroundColor != nil {
           navigationHeaderView.backgroundColor = FuguConfig.shared.navigationHeaderViewBackgroundColor
        }
        
        if FuguConfig.shared.navigationBackgroundColor != nil {
           navigationBackgroundView.backgroundColor = FuguConfig.shared.navigationBackgroundColor
        }
        
        if FuguConfig.shared.navigationTitleColor != nil {
            navigationTitleLabel.textColor =  FuguConfig.shared.navigationTitleColor
        }
        
        if FuguConfig.shared.navigationTitleFontSize != nil {
            navigationTitleLabel.font = FuguConfig.shared.navigationTitleFontSize
        }

        if FuguConfig.shared.sendMessageButtonIcon != nil {
            sendMessageButton.setImage(FuguConfig.shared.sendMessageButtonIcon, for: .normal)
            sendMessageButton.setTitle("", for: .normal)
        } else {
            sendMessageButton.setTitle("SEND", for: .normal)
        }
        //UIColor(red: 140.0/255.0, green: 140.0/255.0, blue: 140.0/255.0, alpha: 1.0)
        
        if (FuguConfig.shared.chatScrenBackButtonText?.count)! > 0 {
            backButton.setTitle((" " + FuguConfig.shared.chatScrenBackButtonText!), for: .normal)
            
            if FuguConfig.shared.backButtonFont != nil {
                backButton.titleLabel?.font = FuguConfig.shared.backButtonFont
            }
            
            if FuguConfig.shared.backButtonTextColor != nil {
                backButton.setTitleColor(FuguConfig.shared.backButtonTextColor, for: .normal)
            } else if FuguConfig.shared.navigationTitleColor != nil {
                backButton.setTitleColor(FuguConfig.shared.navigationTitleColor, for: .normal)
            }
            
        } else {
            if FuguConfig.shared.backButtonImageIcon != nil {
                backButton.setImage(FuguConfig.shared.backButtonImageIcon, for: .normal)
            }
        }
        
        if FuguConfig.shared.navigationTitleText != nil {
            navigationTitleLabel.text = FuguConfig.shared.navigationTitleText
        }
        
        if FuguConfig.shared.navigationTitleFontSize != nil {
            navigationTitleLabel.font = FuguConfig.shared.navigationTitleFontSize
        }
        
        if FuguConfig.shared.navigationTitleTextAlignMent != nil {
            navigationTitleLabel.textAlignment = FuguConfig.shared.navigationTitleTextAlignMent!
        }
        
        
        if (channelName?.count)! > 0 {
            navigationTitleLabel.text = channelName
        } else {
            if let businessName = userDetailsObj["business_name"] as? String {
                navigationTitleLabel.text = businessName
            }
        }

        //messageTextView.text = textViewPlaceHolder
        //messageTextView.textColor = UIColor(red: 73.0/255.0, green: 73.0/255.0, blue: 73.0/255.0, alpha: 1.0)
        textViewBackGroundViewHeightConstraint.constant = CGFloat(textViewFixedHeight)
        
    }
    
    // MARK: UIButton Actions
    
    @IBAction func addImagesButtonAction(_ sender: UIButton) {
        
        //self.view.endEditing(true)
        filePath = directoryPath! + "\(chatImagetag).jpg"
        imagePicker.delegate = self
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cameraAction = UIAlertAction(title: "New Image via Camera", style: .default , handler: {
            (alert: UIAlertAction!) -> Void in

            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
           // self.present(self.imagePicker, animated: true, completion: nil)
            
            self.performActionBasedOnCameraPermission()
            }
        })
        
        let photoLibraryAction = UIAlertAction(title: "Photo Library", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in

            self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
           // self.present(self.imagePicker, animated: true, completion: nil)
            
            self.performActionBasedOnGalleryPermission()

        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        actionSheet.addAction(photoLibraryAction)
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(cancelAction)
        
        let rootViewController: UIViewController = (UIApplication.shared.windows.last?.rootViewController)!
            //UIApplication.shared.windows.lastObject.rootViewController!!
        rootViewController.present(actionSheet, animated: true, completion: nil)
        
        //self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func checkPhotoLibraryPermission() {
        
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized: break
        //handle authorized status
        case .denied, .restricted : break
        //handle denied status
        case .notDetermined:
            // ask for permissions
            PHPhotoLibrary.requestAuthorization() { status in
                switch status {
                case .authorized: break
                // as above
                case .denied, .restricted: break
                // as above
                case .notDetermined: break
                    // won't happen but still
                }
            }
        }
    }
    
    @IBAction func sendMessageButtonAction(_ sender: UIButton) {
        
        if self.messageTextView.text.replacingOccurrences(of: " ", with: "").count == 0 || self.messageTextView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            
            self.updateErrorLabelView(isHiding: false)
            self.errorLabel.text = "Please enter some text."
            self.updateErrorLabelView(isHiding: true)
            return
            
        }
        
        if !connectedToNetwork() {
            
            self.updateErrorLabelView(isHiding: false)
            self.errorLabel.text = "NO INTERNET CONNECTION."
            self.updateErrorLabelView(isHiding: true)
            return
            
        }
        
        if !client.fayeConnected! {
            
            if channelId.count == 0 || channelId == "-1" {
                startNewConversation()
                return
            } else {
                self.updateErrorLabelView(isHiding: false)
                self.errorLabel.text = "No Internet Connection. Please retry after sometime."
                self.updateErrorLabelView(isHiding: true)
                
                self.client = FayeClient(aFayeURLString: FuguConfig.shared.fayeBaseURLString!, channel: "\(channelId)")
                self.client.delegate = self
                self.client.connectToServer()
                
            }
            
        }
        
        if channelId.count == 0 || channelId == "-1" || channelId == "/-1" {
            startNewConversation()
            return
        }
        
        var when = DispatchTime.now() + 0
        
        if !client.isSubscribedToChannel(self.channelId) {
            client.delegate = self
            client.connectToServer()
            when = DispatchTime.now() + 0.5
        }

        DispatchQueue.main.asyncAfter(deadline: when) {
            self.publishMessageOnServer()
        }
        
        self.sendMessageButton.isEnabled = false
    }

    @IBAction func backButtonAction(_ sender: UIButton) {
        
        if channelId != "-1" {
            
            self.client.sendMessage(["message": "", "user_id": getSavedUserId(), "full_name": userName, "date_time": convertCurrentDateTimeToUTC(), "is_typing": TypingMessage.stopTyping.rawValue, "message_type" : messageType, "user_type": userType, "on_subscribe": UserSubscriptionStatus.notSubscribed.rawValue, "channel_id": channelId.replacingOccurrences(of: "/", with: "")], channel: channelId)

        }
        
        FuguConfig.shared.subscribedChannelId = ""
        _ = self.navigationController?.popViewController(animated: true)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func publishMessageOnServer() {
        if self.channelId != "-1" {
            
            self.typingMessageValue = TypingMessage.messageRecieved.rawValue
            
            if getSavedUserId() == -1 {
                return
            }

            self.sendMessageToFaye(message: self.messageTextView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines), isTyping: typingMessageValue)
            
            self.typingMessageValue = TypingMessage.startTyping.rawValue
            self.messageTextView.text = ""
            self.placeHolderLabel.isHidden = false
            self.textViewBackGroundViewHeightConstraint.constant = CGFloat(textViewFixedHeight)
            
            //self.chatScreenTableView.reloadData()
            
        } else if self.channelId == "-1" {
            self.updateErrorLabelView(isHiding: false)
            self.errorLabel.text = "Please enter some text."
            self.updateErrorLabelView(isHiding: true)
        }
    }
    
    func changeShapeOfImageView(_ popupView: UIView, isReverse: Bool) {
        let path = UIBezierPath()
        
        if isReverse {
            path.move(to: CGPoint(x: popupView.frame.size.width, y: 0))
            path.addLine(to: CGPoint(x: popupView.frame.size.width - 10, y: 12.5))
            path.addLine(to: CGPoint(x: popupView.frame.size.width - 10, y: popupView.frame.size.height))
            path.addLine(to: CGPoint(x: 0, y: popupView.frame.size.height))
            path.addLine(to: CGPoint(x: 0, y: 0))
        } else {
            path.move(to: CGPoint(x: 10, y: 12.5))
            path.addLine(to: CGPoint(x: 10, y: popupView.frame.size.height))
            path.addLine(to: CGPoint(x: popupView.frame.size.width, y: popupView.frame.size.height))
            path.addLine(to: CGPoint(x: popupView.frame.size.width, y: 0))
            path.addLine(to: CGPoint(x: 0, y: 0))
            path.move(to: CGPoint(x: 0, y: 0))
        }

        UIColor.red.setFill()
        path.stroke()
        path.reversing()
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.frame = popupView.bounds
        shapeLayer.path = path.cgPath
        shapeLayer.lineJoin = kCALineJoinRound
        shapeLayer.lineCap = kCALineCapRound
        shapeLayer.fillColor = UIColor.cyan.cgColor
        popupView.layer.mask = shapeLayer
        
        popupView.layer.masksToBounds = true

    }
    
    func updateUserDefaultsMessages(_ messagesGroup: [String: Any]) {

        var chatCachedArray = UserDefaults.standard.object(forKey: FUGU_DEFAULT_MESSAGES) as? [[String: Any]]
        
        if UserDefaults.standard.object(forKey: FUGU_DEFAULT_MESSAGES) == nil {
            chatCachedArray = []
        }
        
        if (chatCachedArray?.isEmpty)! {
            
            chatCachedArray?.append(messagesGroup)
            UserDefaults.standard.set(chatCachedArray, forKey: FUGU_DEFAULT_MESSAGES)
            return
        }

        var isAlreadyCached = false
        for (index, chatObj) in chatCachedArray!.enumerated() {
            
            guard
                let savedChannelId = chatObj["channel_id"] as? Int
                else {
                    continue
            }
            
            if savedChannelId == Int(self.channelId.replacingOccurrences(of: "/", with: "")) {
                
                if messagesGroup.isEmpty {
                    if let messagesArray = chatObj["messages"] as? [Any], !messagesArray.isEmpty {
                        self.checkIfDateExistsElseUpdate(messagesArray)
                        DispatchQueue.main.async {
                            self.chatScreenTableView.reloadData()
                        }
                        
                    }
                    return
                }
                
                isAlreadyCached = true
                chatCachedArray?[index] = messagesGroup
                UserDefaults.standard.set(chatCachedArray, forKey: FUGU_DEFAULT_MESSAGES)

                return
            }
        }
        
        if !isAlreadyCached && !messagesGroup.isEmpty {
            
            chatCachedArray?.append(messagesGroup)
            UserDefaults.standard.set(chatCachedArray, forKey: FUGU_DEFAULT_MESSAGES)
            
        }
        
        
    }
    
    // MARK: SERVER HIT
    
    func getMessagesBasedOnChannel(_ channelId: Int, pageStart: Int) {

        self.updateUserDefaultsMessages([:])
        
        if !connectedToNetwork() {

            if self.dateGroupedArray.isEmpty {
                self.updateErrorLabelView(isHiding: false)
                self.errorLabel.text = "NO INTERNET CONNECTION."
                self.updateErrorLabelView(isHiding: true)
            }
            return
            
        }
        
        if isGettingResponse {
            return
        }
        
        if let user_id = userDetailsObj["user_id"] as? Int {
            UserDefaults.standard.set(user_id, forKey: FUGU_USER_ID)
        }

        var params = [
            
            "app_secret_key" : FuguConfig.shared.appSecretKey,
            "channel_id" : channelId,
            "user_id" : getSavedUserId()
            
            ] as [String: Any]
        
        params["page_start"] = pageStart
        
        self.isGettingResponse = true
        
        HTTPClient.shared.makeSingletonConnectionWith(method: .POST, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: false, para: params, extendedUrl: API_GET_MESSAGES) { (responseObject, error, tag, statusCode) in
            
            self.isGettingResponse = false
            self.showTextViewBasedOnChatStatus()
            self.userSubscribedValue = UserSubscriptionStatus.notSubscribed.rawValue
            
            //if pageStart == 1 {
            
            //}
            
            if statusCode == STATUS_CODE_SUCCESS {
                
                let response = responseObject as? [String: Any]
                
                guard
                    let data = response?["data"] as? [String: Any]
                    else {
                        return
                }
                
                if let channelName = data["label"] as? String {
                    
                    self.channelName = channelName
                    self.navigationTitleLabel.text = channelName
                    
                } else if let fullName = data["full_name"] as? String, fullName.count > 0 {
                    self.navigationTitleLabel.text = fullName
                }
                
                if let onSubscribed = data["on_subscribe"] as? Int {
                    self.userSubscribedValue = onSubscribed
                }
                
                if let messages = data["messages"] as? [Any], messages.count > 0 {
                    self.dateGroupedArray = []
                    
                    self.willPaginationWork = false
                    if let pageSize = data["page_size"] as? Int {
                        
                        if messages.count - pageSize == 0 {
                            self.willPaginationWork = true
                        }
                        
                    }
                    self.messagesArray = messages as! [[String : Any]] + self.messagesArray
                    self.checkIfDateExistsElseUpdate(self.messagesArray)
                    
                    self.messageArrayCount = messages.count + self.messageArrayCount
                    
                    if self.messagesArray.count < 25 || pageStart == 1 {
                    self.updateUserDefaultsMessages(data)
                    }
                    
                    self.chatScreenTableView.reloadData()
                    
                    if pageStart == 1 {
                    self.scrollTableViewToBottom()
                    }
                }
                
            }
            
        }
        
    }
    
    func checkIfDateExistsElseUpdate(_ chatMessagesArray: [Any]) {

        for j in 0 ..< chatMessagesArray.count {
            
            if let messageObj = chatMessagesArray[j] as? [String: Any] {
                if let dateTimeString = messageObj["date_time"] as? String, dateTimeString.count > 0 {
                    
                    if self.dateGroupedArray.isEmpty {
                        
                        self.addNewDate(messageObj, dateTimeString: dateTimeString)
                        
                    } else {
                        var isDateFound = false
                        
                        for i in 0 ..< self.dateGroupedArray.count {
                            
                             let obj = self.dateGroupedArray[i]
                                
                                var updatedObj = obj
                                if let dateString = obj["date"] as? String {

                                    let comparisonResult = Calendar.current.compare(dateString.toDate!, to: dateTimeString.toDate!, toGranularity: .day)
                                    
                                    switch comparisonResult {
                                        
                                    case .orderedSame:
                                        if let messagesArray = obj["message"] as? [Any] {
                                            
                                            isDateFound = true
                                            
                                            var updatedMessageArray = messagesArray
                                            updatedMessageArray.append(messageObj)
                                            updatedObj["message"] = updatedMessageArray
                                            self.dateGroupedArray[i] = updatedObj
                                            
                                            break
                                            
                                        }
                                        
                                    default:
                                        break
                                    }
                                }
                        }
                        
                        if !isDateFound {
                            addNewDate(messageObj, dateTimeString: dateTimeString)
                        }
                    }
                }
            }
        }
    }
    
    func addNewDate(_ dateObj: [String: Any], dateTimeString: String) {
        
        var arrayOfDates = [String: Any]()
        
        arrayOfDates["date"] = dateTimeString
        arrayOfDates["message"] = [dateObj]
        
        self.dateGroupedArray.append(arrayOfDates)
        
    }
    
    func openChatBasedOnSpecificLabel(_ labelId: Int, pageStart: Int) {
        
        self.updateUserDefaultsMessages([:])
        
        if !connectedToNetwork() {

            if self.dateGroupedArray.isEmpty {
                self.updateErrorLabelView(isHiding: false)
                self.errorLabel.text = "NO INTERNET CONNECTION."
                self.updateErrorLabelView(isHiding: true)
            }
            return
            
        }
        
        self.labelId = labelId
        
        var params = [
            
            "app_secret_key" : FuguConfig.shared.appSecretKey,
            "label_id" : labelId,
            "user_id" : getSavedUserId()
            
            ] as [String: Any]
        
        params["page_start"] = pageStart
        
        HTTPClient.shared.makeSingletonConnectionWith(method: .POST, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: false, para: params, extendedUrl: API_GET_MESSAGES_BASED_ON_LABEL) { (responseObject, error, tag, statusCode) in

            self.userSubscribedValue = UserSubscriptionStatus.notSubscribed.rawValue
            
            self.isChatDeactivated = false
            
            if statusCode == STATUS_CODE_SUCCESS {
                
                let response = responseObject as? [String: Any]
                
                if let data = response?["data"] as? [String: Any] {
                    
                    if let channelName = data["label"] as? String {
                        self.channelName = channelName
                        self.navigationTitleLabel.text = channelName
                    } else if let fullName = data["full_name"] as? String, fullName.count > 0 {
                        self.navigationTitleLabel.text = fullName
                    }
                    
                    if let onSubscribed = data["on_subscribe"] as? Int {
                        self.userSubscribedValue = onSubscribed
                    }
                    
                    if let chatActivationStatus = data["status"] as? Int, chatActivationStatus == 0 {
                        self.isChatDeactivated = true
                        self.showTextViewBasedOnChatStatus()
                    }
                    
                    if let channelId = data["channel_id"] as? Int, channelId != -1 {
                        self.channelId = "/\(channelId)"
                        self.client = FayeClient(aFayeURLString: FuguConfig.shared.fayeBaseURLString!, channel: "/\(channelId)")
                        self.client.delegate = self
                        self.client.connectToServer()
                    }
                    
                    if let messages = data["messages"] as? [Any], messages.count > 0 {
                        self.dateGroupedArray = []
                        self.willPaginationWork = false
                        if let pageSize = data["page_size"] as? Int {
                            
                            if messages.count - pageSize == 0 {
                                self.willPaginationWork = true
                            }
                            
                        }
                        self.messagesArray = messages as! [[String : Any]] + self.messagesArray
                        self.checkIfDateExistsElseUpdate(self.messagesArray)
                        self.messageArrayCount = messages.count + self.messageArrayCount
                        
                        if self.messagesArray.count < 25 || pageStart == 1 {
                            self.updateUserDefaultsMessages(data)
                        }
                        
                        self.chatScreenTableView.reloadData()
                        
                        if pageStart == 1 {
                            self.scrollTableViewToBottom()
                        }
                        /*
                        self.checkIfDateExistsElseUpdate(messages)
                        self.chatScreenTableView.reloadData()
                        self.scrollTableViewToBottom()
                        */
                    }

                }
                
            }
            
        }
    }
    
    func startNewConversation(imageData: [String: Any]? = nil) {
        
        if !connectedToNetwork() {
            
            self.updateErrorLabelView(isHiding: false)
            self.errorLabel.text = "NO INTERNET CONNECTION."
            self.updateErrorLabelView(isHiding: true)
            return
            
        }
        activityIndicator.stopAnimating()
        activityIndicator.frame = sendMessageButton.bounds
        activityIndicator.backgroundColor = UIColor.white
        sendMessageButton.addSubview(activityIndicator)
        activityIndicator.bringSubview(toFront: sendMessageButton)
        activityIndicator.startAnimating()
        
        var params = [String: Any]()
        params["app_secret_key"] = FuguConfig.shared.appSecretKey
        params["user_id"] = getSavedUserId()
        
        if self.channelId == "-1" || self.channelId == "/-1" {
            params["label_id"] = -1
            if !self.dateGroupedArray.isEmpty {
                
                let dateGroupObj = self.dateGroupedArray[0]
                
                if let messagesArray = dateGroupObj["message"] as? [[String: Any]], messagesArray.count > 0 {
                    let chatObj = messagesArray[0]
                    
                    if self.labelId != -1 {
                        params["label_id"] = self.labelId
                    } else if let labelId = chatObj["label_id"] as? Int {
                        params["label_id"] = labelId
                        self.labelId = labelId
                    }
                    
                }
            }
        }
        
        HTTPClient.shared.makeSingletonConnectionWith(method: .POST, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: false, para: params, extendedUrl: API_CREATE_CONVERSATION) { (responseObject, error, tag, statusCode) in
            
            let response = responseObject as? [String: Any]
            
            if statusCode == STATUS_CODE_SUCCESS {
                
                if let chatObj = response?["data"] as? [String: Any] {
                    
                    if let channelId = chatObj["channel_id"] as? Int {
                        
                        self.channelId = "/\(channelId)"
                        self.client = FayeClient(aFayeURLString: FuguConfig.shared.fayeBaseURLString!, channel: "/\(channelId)")
                        self.client.delegate = self
                        self.client.connectToServer()
                        if let channelName = chatObj["label"] as? String {
                            self.channelName = channelName
                            self.navigationTitleLabel.text = channelName
                        }

                        if imageData != nil {
                            if (imageData?.keys.count)! > 0 {
                                
                                guard
                                    let imageUrl = imageData?["image_url"] as? String,
                                    let thumbnailUrl = imageData?["thumbnail_url"] as? String
                                    else {
                                        return
                                }
                                
                                self.client.connectToServer()

                                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                                    self.client.sendMessage(["message": "", "user_id": self.getSavedUserId(), "full_name": self.userName, "date_time": convertCurrentDateTimeToUTC(), "is_typing": TypingMessage.messageRecieved.rawValue, "message_type" : MessageType.forFile.rawValue, "user_type": self.userType, "image_url": imageUrl, "thumbnail_url": thumbnailUrl], channel: self.channelId)
                                }
                                
                            }
                        } else {
                            
                            self.client.connectToServer()
                            
                            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0) {
                                self.publishMessageOnServer()
                            }
                            
                        }
                        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.8) {
                            self.activityIndicator.stopAnimating()
                        }
                    }
                    
                }
                
            } else {
                self.activityIndicator.stopAnimating()
            }
            
        }
        
    }
    
    // MARK: HELPERS
    func getSavedUserId() -> Int {
        
        if let savedUserId = UserDefaults.standard.value(forKey: FUGU_USER_ID) as? Int {
            return savedUserId
        }
        
        return -1
        
    }
    
    
    
    
    func imageTapped(_ sender: UITapGestureRecognizer) {
        
        
        let destinationVC = self.storyboard?.instantiateViewController(withIdentifier: "ShowImageViewController") as! ShowImageViewController
        
        let imageView = sender.view as! UIImageView
        
        self.modalPresentationStyle = .overCurrentContext
        destinationVC.imageToShow = imageView.image!
        
        self.present(destinationVC, animated: true, completion: nil)
        
        
        /*self.view.endEditing(true)
        
        
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        newImageView.contentMode = .scaleAspectFit
        newImageView.clipsToBounds = true
        newImageView.isUserInteractionEnabled = true
        
        let closeButton = UIButton()
        
        closeButton.setTitle("X", for: .normal)
        closeButton.setTitleColor(.black, for: .normal)
        closeButton.frame = CGRect(x: UIScreen.main.bounds.width - 40.0, y: 20.0, width: 30.0, height: 30.0)
        closeButton.isUserInteractionEnabled = false
        closeButton.backgroundColor = .white
        closeButton.layer.cornerRadius = 15.0
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        newImageView.addSubview(closeButton)
        self.view.bringSubview(toFront: closeButton)
        self.view.addSubview(newImageView)*/
        
        
    }
    
    func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {

        sender.view?.removeFromSuperview()
        
    }
    
    func watcherOnTextView() {
        
        if textInTextField == messageTextView.text && self.typingMessageValue == TypingMessage.stopTyping.rawValue, self.channelId != "-1" {

            sendMessageToFaye(message: "", isTyping: typingMessageValue)
            self.typingMessageValue = TypingMessage.startTyping.rawValue
            
        } else {
            textInTextField = messageTextView.text
        }
        
    }
    
    func updateErrorLabelView(isHiding: Bool) {
        
        if isHiding {
            if self.errorLabelTopConstraint.constant == 0 {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
                    self.errorLabelTopConstraint.constant = -20
                    UIView.animate(withDuration: 0.5, animations: {
                        self.errorLabel.text = ""
                        self.view.layoutIfNeeded()
                    })
                }
            }
            return
        }
        if self.errorLabelTopConstraint != nil {
            self.errorLabelTopConstraint.constant = 0
        }
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
        
        
    }
    
    func sendMessageToFaye(message: String, isTyping: Int) {
        
        var messageDict = [String: AnyObject]()
        
        messageDict["message"] = message as AnyObject
        messageDict["user_id"] = getSavedUserId() as AnyObject
        messageDict["full_name"] = userName as AnyObject
        messageDict["date_time"] = convertCurrentDateTimeToUTC() as AnyObject
        messageDict["is_typing"] = isTyping as AnyObject
        messageDict["message_type"] = messageType as AnyObject
        messageDict["user_type"] = userType as AnyObject
        
        client.sendMessage(messageDict, channel: channelId)
        
        
        //client.sendMessage(["message": message, "user_id": FuguConfig.shared.fuguUserId, "full_name": userName, "date_time": convertCurrentDateTimeToUTC(), "is_typing": isTyping, "message_type" : messageType, "user_type": userType], channel: channelId)
        
    }
    
    func returnImageUsingCacheWithURLString(url: NSURL, index: IndexPath) -> (UIImage) {

        if let cachedImage = imageCacheFugu.object(forKey: url) as? UIImage {
            return cachedImage
        } else {

            URLSession.shared.dataTask(with: url as URL, completionHandler: { (data, response, error) in
                
                if error == nil {
                    
                    DispatchQueue.main.async(execute: {
                        
                        // Cache to image so it doesn't need to be reloaded everytime the user scrolls and table cells are re-used.
                        if let downloadedImage = UIImage(data: data!) {
                            
                            imageCacheFugu.setObject(downloadedImage, forKey: url)
                            
                            if let cellToUpdate = self.chatScreenTableView.cellForRow(at: index) as? SelfMessageTableViewCell {

                                cellToUpdate.chatImageView.image = downloadedImage
                                self.chatScreenTableView.reloadRows(at: [index], with: .none)

                            }
                            self.returnImage = downloadedImage
                        }
                    })
                }
            }).resume()
            return returnImage
        }
    }
    
    // MARK: Notification Methods
    
    func keyboardWillShow(_ notification: Notification) {
        
        if let userInfo = (notification as NSNotification).userInfo {
            if let keyboardSize = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                
                if let duration = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSValue) {
                    UIView.animate(withDuration: duration as! TimeInterval, animations: {
                        self.textViewBottomConstraint.constant = keyboardSize.height
                        
                        
                    })
                }

                self.scrollTableViewToBottom()
            }
        }
    }
    
    func keyboardWillHide(_ notification: Notification) {
        
        if !isChatDeactivated {
            textViewBackGroundViewHeightConstraint.constant = CGFloat(textViewFixedHeight)
            textViewBottomConstraint.constant = 0
        }

        if self.typingMessageValue == TypingMessage.startTyping.rawValue, self.channelId != "-1" {
            self.typingMessageValue = TypingMessage.stopTyping.rawValue
            sendMessageToFaye(message: "", isTyping: typingMessageValue)
        }
        self.addRemoveShadowInTextView(toAdd: false)
        
    }
    
    func addRemoveShadowInTextView(toAdd: Bool) {
        self.seperatorView.isHidden = true
        //self.textViewBgView.layer.masksToBounds = true
        
        if toAdd {
            self.seperatorView.isHidden = false
            /*self.textViewBgView.layer.masksToBounds = false
            self.textViewBgView.layer.shadowColor = UIColor.black.cgColor
            self.textViewBgView.layer.shadowOffset = CGSize(width: CGFloat(0.0), height: CGFloat(-1.0))
            self.textViewBgView.layer.shadowOpacity = 0.3
            //self.textViewBgView.layer.shadowRadius = 4.0*/
            
        }

    }
    
    func scrollTableViewToBottom() {
        
        DispatchQueue.main.async {
            if self.dateGroupedArray.count > 0 {
                 let datObj = self.dateGroupedArray[self.dateGroupedArray.count - 1] //{
                    if let messagesArray = datObj["message"] as? [Any], messagesArray.count > 0 {
                        let indexPath = IndexPath(row: messagesArray.count - 1, section: self.dateGroupedArray.count - 1)
                        self.chatScreenTableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
                    }
                //}
            }
        }
        
    }

}

extension ConversationsViewController: UIScrollViewDelegate {
    
    // MARK: UIScrollViewDelegate
    
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {

        if self.chatScreenTableView.contentOffset.y < -5.0 && self.willPaginationWork {
            self.getMessagesBasedOnChannel(Int(channelId.replacingOccurrences(of: "/", with: ""))!, pageStart: messageArrayCount + 1)
        }
        
    }

}

extension ConversationsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: UIImagePicker Delegates

    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            if UIImageJPEGRepresentation(pickedImage, 1.0)!.count > 3*1024 {
                try? UIImageJPEGRepresentation(pickedImage, 0.1)?.write(to: Foundation.URL(fileURLWithPath: String(filePath! as NSString)), options: [.atomic])
            }
            else if UIImageJPEGRepresentation(pickedImage, 1.0)!.count > 2*1024 {
                try? UIImageJPEGRepresentation(pickedImage, 0.5)?.write(to: Foundation.URL(fileURLWithPath: String(filePath! as NSString)), options: [.atomic])
            }
            else {
                try? UIImageJPEGRepresentation(pickedImage, 0.75)?.write(to: Foundation.URL(fileURLWithPath: String(filePath! as NSString)), options: [.atomic])
            }
        }
        
        print("File Path:", filePath!)
        
        if (filePath?.count)! > 0 {
            sendImageDataToServer(filePath: filePath!)
        }
        chatImagetag += 1
        picker.dismiss(animated: true, completion: nil)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func performActionBasedOnGalleryPermission() {
        
        let status = PHPhotoLibrary.authorizationStatus()
        
        if status == .authorized {
            
            DispatchQueue.main.async {
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            
            
        } else if status == .denied {
            
            self.updateErrorLabelView(isHiding: false)
            self.errorLabel.text = "Access to photo library is denied. Please enable from setings."
            self.updateErrorLabelView(isHiding: true)

            
        } else if status == .notDetermined {
            
            PHPhotoLibrary.requestAuthorization({ (newStatus) in
                
                if (newStatus == .authorized) {
                    DispatchQueue.main.async {
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                } else {
                    self.performActionBasedOnGalleryPermission()
                }
            })
        }
    }
    
    func performActionBasedOnCameraPermission() {
        
        let status = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        
        if status == .authorized {
            DispatchQueue.main.async {
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        } else if status == .denied {
            self.updateErrorLabelView(isHiding: false)
            self.errorLabel.text = "Access to Camera is denied. Please enable from setings."
            self.updateErrorLabelView(isHiding: true)
        } else if status == .notDetermined {
            
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (newStatus) in
                
                if newStatus {
                    DispatchQueue.main.async {
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                } else {
                    self.performActionBasedOnCameraPermission()
                }
               
            })
            
        }

    }
    
    func sendImageDataToServer(filePath: String) {
        
        if !connectedToNetwork() {
            
            self.updateErrorLabelView(isHiding: false)
            self.errorLabel.text = "NO INTERNET CONNECTION."
            self.updateErrorLabelView(isHiding: true)
            return
            
        }
        
        activityIndicator.stopAnimating()
        activityIndicator.frame = sendMessageButton.bounds
        activityIndicator.backgroundColor = UIColor(red: 230.0/255.0, green: 232.0/255.0, blue: 233.0/255.0, alpha: 1.0)
        
        sendMessageButton.addSubview(activityIndicator)
        activityIndicator.bringSubview(toFront: loaderbutton)
        activityIndicator.startAnimating()
        
        var params = [String: Any]()
        
        params["app_secret_key"] = FuguConfig.shared.appSecretKey
        params["file_type"] = "image"
        
        
        var imageList = [String: Any]()
        imageList["file"] = filePath
        
        HTTPClient.makeMultiPartRequestWith(method: .POST, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: false, para: params, baseUrl: FuguConfig.shared.baseUrl, extendedUrl: API_UPLOAD_FILE, imageList: imageList) { (responseObject, error, tag, statusCode) in
            
            if statusCode == STATUS_UPLOAD_SUCCESS {
                
                let response = responseObject as? [String: Any]
                
                if let data = response?["data"] as? [String: Any] {
                    
                    guard
                        let imageUrl = data["image_url"] as? String,
                        let thumbnailUrl = data["thumbnail_url"] as? String
                        else {
                            return
                    }
                    
                    if self.channelId == "-1" {
                        self.startNewConversation(imageData: data)
                        return
                    }
                    
                    self.client.sendMessage(["message": "", "user_id": self.getSavedUserId(), "full_name": self.userName, "date_time": convertCurrentDateTimeToUTC(), "is_typing": TypingMessage.messageRecieved.rawValue, "message_type" : MessageType.forFile.rawValue, "user_type": self.userType, "image_url": imageUrl, "thumbnail_url": thumbnailUrl], channel: self.channelId)
                    
                }
                
            } else {
                
                self.activityIndicator.stopAnimating()
                self.updateErrorLabelView(isHiding: false)
                self.errorLabel.text = "NO INTERNET CONNECTION."
                self.updateErrorLabelView(isHiding: true)

            }
        }
    }
    
}

extension ConversationsViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: UITableView Delegates
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        if !isTypingLabel.isHidden {
            return self.dateGroupedArray.count + 1
        }
        return self.dateGroupedArray.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section < self.dateGroupedArray.count {
            let datObj = self.dateGroupedArray[section] //as? [String: Any] {
            if let messagesArray = datObj["message"] as? [Any], messagesArray.count > 0 {
                /*if !isTypingLabel.isHidden && section == self.dateGroupedArray.count - 1 {
                 return messagesArray.count + 1
                 }*/
                return messagesArray.count
            }
            //}
        } else {
            if !isTypingLabel.isHidden {
                return 1
            }
        }
        return 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
            
        case let typingSection where typingSection == self.dateGroupedArray.count && !isTypingLabel.isHidden:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "TypingViewTableViewCell", for: indexPath) as! TypingViewTableViewCell
            
            cell.selectionStyle = .none
            cell.bgView.isHidden = false
            cell.gifImageView.image = nil
            cell.bgView.backgroundColor = .white
            cell.bgView.layer.cornerRadius = 15.0
            cell.gifImageView.layer.cornerRadius = 15.0
            
            if let getImagePath = Bundle.main.path(forResource: "typingImage", ofType: ".gif") {
                cell.gifImageView.image = UIImage.animatedImageWithData(try! Data(contentsOf: URL(fileURLWithPath: getImagePath)))!
            }
            
            return cell
            
        case let chatSection where chatSection < self.dateGroupedArray.count:
            
            var datObj = self.dateGroupedArray[chatSection]
            
            if let messagesArray = datObj["message"] as? [Any], messagesArray.count > 0 {
                
                var updatedMessagesArray = messagesArray
                if let chatMessageObject = messagesArray[indexPath.row] as? [String: Any] {
                    
                    var updatedChatObj = chatMessageObject
                    if let user_id = chatMessageObject["user_id"] as? Int, getSavedUserId() != user_id {
                        
                        let cell = tableView.dequeueReusableCell(withIdentifier: "SupportMessageTableViewCell", for: indexPath) as! SupportMessageTableViewCell
                        
                        cell.selectionStyle = .none
                        cell.supportMessageLabel.text = ""
                        cell.supportMessageLabel.attributedText = nil
                        cell.dateTimeLabel.text = ""
                        cell.bgView.layer.cornerRadius = 6.0
                        cell.supportImageView.image = nil
                        
                        DispatchQueue.main.async {
                            self.changeShapeOfImageView(cell.bgView, isReverse: false)
                        }
                        if let messageString = chatMessageObject["message"] as? String, messageString.count > 0 {
                            
                            var userNameString = "--"
                            if let full_name = chatMessageObject["full_name"] as? String, full_name.count > 0 {
                                userNameString = full_name
                            } else if let businessName = userDetailsObj["business_name"] as? String {
                                userNameString = businessName
                            } else if (self.channelName?.count)! > 0 {
                                userNameString = self.channelName!
                            }
                            
                            var dateTimeString = ""
                            if channelId == "-1" {
                                dateTimeString = ""
                            } else if let dateTime = chatMessageObject["date_time"] as? String, dateTime.count > 0 {
                                cell.dateTimeLabel.text = changeDateToParticularFormat((dateTime.toDate)!, dateFormat: "hh:mm a", showInFormat: true)
                            }
                            
                            cell.supportMessageLabel.attributedText = attributedStringForLabel(userNameString, secondString: "\n" + messageString, thirdString: dateTimeString, colorOfFirstString: UIColor(red: 140.0/255.0, green: 140.0/255.0, blue: 140.0/255.0, alpha: 1.0), colorOfSecondString: UIColor.black.withAlphaComponent(0.6), colorOfThirdString: UIColor.black.withAlphaComponent(0.5), fontOfFirstString: UIFont.systemFont(ofSize: 14.0), fontOfSecondString: UIFont.systemFont(ofSize: 16.0), fontOfThirdString: UIFont.systemFont(ofSize: 11.0), textAlighnment: .left, dateAlignment: .right)
                            
                            if let thumbnailUrl = chatMessageObject["thumbnail_url"] as? String, thumbnailUrl.count > 0 {
                                
                                if let url = NSURL(string: thumbnailUrl) {
                                    
                                    if connectedToNetwork() {
                                        cell.supportImageView.image = self.returnImageUsingCacheWithURLString(url: url, index: indexPath)
                                        
                                    }
                                }
                            }
                        }
                        
                        
                        if chatSection == 0 && indexPath.row == 0 && labelId != -1 {
                            cell.dateTimeLabel.text = ""
                        }
                        
                        return cell
                    } else {

                        let cell = tableView.dequeueReusableCell(withIdentifier: "SelfMessageTableViewCell", for: indexPath) as! SelfMessageTableViewCell
                        
                        cell.selectionStyle = .none
                        cell.selfMessageLabel.text = ""
                        cell.timeLabel.text = ""
                        cell.chatImageView.image = nil
                        if let thumbnailUrl = chatMessageObject["thumbnail_url"] as? String, thumbnailUrl.count > 0 {
                            
                            cell.chatImageView.image = UIImage(named: "placeholderImg")
                        }

                        cell.bgView.layer.cornerRadius = 6.0
                        DispatchQueue.main.async {
                            self.changeShapeOfImageView(cell.bgView, isReverse: true)
                        }
                        
                        cell.readUnreadImageView.image = UIImage(named: "unreadMessageImage")
                        
                        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(_:)))
                        cell.chatImageView.isUserInteractionEnabled = true
                        cell.chatImageView.addGestureRecognizer(tapGestureRecognizer)
                        
                        if userSubscribedValue == UserSubscriptionStatus.subscribed.rawValue {
                            cell.readUnreadImageView.image = UIImage(named: "readMessageImage")
                            
                            updatedChatObj["message_status"] = ReadUnReadStatus.read.rawValue
                            updatedMessagesArray[indexPath.row] = updatedChatObj
                            
                            datObj["message"] = updatedMessagesArray
                            self.dateGroupedArray[chatSection] = datObj

                        } else {
                            if let messageReadStatus = chatMessageObject["message_status"] as? Int {
                                if messageReadStatus == ReadUnReadStatus.read.rawValue {
                                    cell.readUnreadImageView.image = UIImage(named: "readMessageImage")
                                }
                            }
                        }

                        if let messageString = chatMessageObject["message"] as? String, messageString.count > 0 {
                            cell.selfMessageLabel.text = messageString
                            
                        }
                        
                        if let dateTime = chatMessageObject["date_time"] as? String, dateTime.count > 0 {
                            cell.timeLabel.text = changeDateToParticularFormat((dateTime.toDate)!, dateFormat: "hh:mm a", showInFormat: true)
                        }
                        
                        if let thumbnailUrl = chatMessageObject["thumbnail_url"] as? String, thumbnailUrl.count > 0 {
                            
                            cell.chatImageView.image = UIImage(named: "placeholderImg")
                            
                            cell.chatImageView.layer.cornerRadius = 6.0
                            if let url = NSURL(string: thumbnailUrl) {
                                
                                self.activityIndicator.stopAnimating()
                                cell.chatImageView.image = self.returnImageUsingCacheWithURLString(url: url, index: indexPath)
                                
                                if cell.chatImageView.image == nil {
                                    cell.chatImageView.image = UIImage(named: "placeholderImg")
                                }

                            }
                            
                        }

                        return cell
                    }
                    
                }
            }

        default:
            return UITableViewCell()
        }
        
        
        
        return UITableViewCell()
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section < self.dateGroupedArray.count {
            if section == 0 && channelId == "-1" {
                return 0.0
            } /*else if self.dateGroupedArray.count > 1 {
                if section == 0 && self.labelId != -1 && self.channelId == "-1" {
                    return 0.0
                }
            }*/
            return 18.0
        }
        return 0.0
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let labelBgView = UIView()
        
        labelBgView.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: 18.0)
        labelBgView.backgroundColor = .clear
        
        let dateLabel = UILabel()
        dateLabel.layer.masksToBounds = true

        dateLabel.text = ""
        dateLabel.layer.cornerRadius = 5.0
        dateLabel.textColor = UIColor(red: 89.0/255.0, green: 89.0/255.0, blue: 104.0/255.0, alpha: 1.0)
        dateLabel.textAlignment = .center
        dateLabel.font = UIFont.boldSystemFont(ofSize: 12.0)

        dateLabel.backgroundColor = UIColor(red: 221/255.0, green: 244/255.0, blue: 213/255.0, alpha: 1.0)
        if section < self.dateGroupedArray.count {
            let datObj = self.dateGroupedArray[section]
            if let dateTime = datObj["date"] as? String, dateTime.count > 0 {
                dateLabel.text = changeDateToParticularFormat((dateTime.toDate)!, dateFormat: "MMM d, yyyy", showInFormat: false).capitalized
            }
        }
        let widthIs: CGFloat = CGFloat(dateLabel.text!.boundingRect(with: dateLabel.frame.size, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: dateLabel.font], context: nil).size.width)
        
        //dateLabel.frame = CGRect(x: Double(UIScreen.main.bounds.size.width / 2) - (widthIs/2), y: 0.0, width: widthIs + 10, height: 15.0)
        dateLabel.frame = CGRect(x: (UIScreen.main.bounds.size.width / 2) - (widthIs/2), y: 0.0, width: widthIs + 10, height: 15.0)
        labelBgView.addSubview(dateLabel)
        
        return labelBgView
    }
}

extension ConversationsViewController: FayeClientDelegate {
    
    // MARK: FayeClientDelegate
    
    public func connectedtoser(_ client: FayeClient) {
        print("Connected to Faye server")
    }
    
    public func connectionFailed(_ client: FayeClient) {
        print("Failed to connect to Faye server!")
    }
    
    public func disconnectedFromServer(_ client: FayeClient) {
        print("Disconnected from Faye server")
    }
    
    public func didSubscribeToChannel(_ client: FayeClient, channel: String) {
        
        print("Subscribed to channel \(channel)")
        var updatedChannel = channel
        updatedChannel.remove(at: updatedChannel.startIndex)
        FuguConfig.shared.subscribedChannelId = updatedChannel

        self.client.sendMessage(["message": "", "user_id": self.getSavedUserId(), "full_name": userName, "date_time": convertCurrentDateTimeToUTC(), "is_typing": typingMessageValue, "message_type" : messageType, "user_type": userType, "on_subscribe": UserSubscriptionStatus.subscribed.rawValue, "channel_id": channel.replacingOccurrences(of: "/", with: "")], channel: channel)

    }
    
    public func didUnsubscribeFromChannel(_ client: FayeClient, channel: String) {
        print("Unsubscribed from channel \(channel)")
    }
    
    public func subscriptionFailedWithError(_ client: FayeClient, error: subscriptionError) {
        print("Subscription failed")
    }
    
    public func messageReceived(_ client: FayeClient, messageDict: NSDictionary, channel: String) {
        
        isTypingLabel.isHidden = true
        
        if let messageType = messageDict["message_type"] as? Int {

            if messageType == MessageType.normal.rawValue || messageType == MessageType.forFile.rawValue {
                
                if let message = messageDict["message"] as? String, message.count > 0 {
                    
                    self.updateMessagesArrayLocally(messageDict)
                    
                } else if let thumbnailUrl = messageDict["thumbnail_url"] as? String, thumbnailUrl.count > 0 {
                    
                    self.updateMessagesArrayLocally(messageDict)
                    
                } else if let image_url = messageDict["image_url"] as? String, image_url.count > 0 {
                    
                    self.updateMessagesArrayLocally(messageDict)
                    
                }
                
                if let user_id = messageDict["user_id"] as? Int, getSavedUserId() != user_id {
                    
                    if let onSubscribed = messageDict["on_subscribe"] as? Int {
                        userSubscribedValue = onSubscribed
                    }
                    
                    if let isTyping = messageDict["is_typing"] as? Int {
                        if isTyping == 1 {
                            isTypingLabel.isHidden = false
                        } else {
                            isTypingLabel.isHidden = true
                        }
                    }
                }
                
                self.chatScreenTableView.reloadData()
                
                if isTypingLabel.isHidden == false {
                    let indexPathOfGifCell = IndexPath(row: 0, section: self.dateGroupedArray.count)
                    chatScreenTableView.scrollToRow(at: indexPathOfGifCell, at: .none, animated: false)
                } else {
                    scrollTableViewToBottom()
                }
            }
            
        }
        
    }
    
    public func pongReceived(_ client: FayeClient) {
        print("pong")
    }
    
    func updateMessagesArrayLocally(_ messageDict: NSDictionary) {

        if delegate != nil {
            delegate?.newChatStartedDelgegate(isChatUpdated: true)
        }
        self.checkIfDateExistsElseUpdate([messageDict])
        self.messageArrayCount += 1

    }
    
}

// MARK: UITextViewDelegates

extension ConversationsViewController: UITextViewDelegate {
    
    public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        self.addRemoveShadowInTextView(toAdd: true)
        
        self.placeHolderLabel.textColor = UIColor(red: 73.0/255.0, green: 73.0/255.0, blue: 73.0/255.0, alpha: 0.8)
        textInTextField = textView.text
        textViewBgView.backgroundColor = .white
        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.watcherOnTextView), userInfo: nil, repeats: true)

        return true
    }
    
    public func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        
        textViewBgView.backgroundColor = UIColor(red: 230.0/255.0, green: 232.0/255.0, blue: 233.0/255.0, alpha: 1.0)
        self.placeHolderLabel.textColor = UIColor(red: 73.0/255.0, green: 73.0/255.0, blue: 73.0/255.0, alpha: 0.5)
        
        timer.invalidate()
        return true
    }
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        
        typingMessageValue = TypingMessage.startTyping.rawValue
        
        
        /*if messageTextView.text == textViewPlaceHolder {
            messageTextView.text = ""
            messageTextView.textColor = UIColor(red: 140.0/255.0, green: 140.0/255.0, blue: 140.0/255.0, alpha: 1.0)
        }*/
        
    }
    
    public func textViewDidChange(_ textView: UITextView) {
        
        if  messageTextView.contentSize.height >= CGFloat(textViewFixedHeight) && messageTextView.contentSize.height <= 130 {
            
            textViewBackGroundViewHeightConstraint.constant = messageTextView.contentSize.height
            
        } else if messageTextView.contentSize.height > 130 {
            
            self.textViewBackGroundViewHeightConstraint.constant = 130
            
        } else {
            
            self.textViewBackGroundViewHeightConstraint.constant = CGFloat(textViewFixedHeight)
            
        }

    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        
        if messageTextView.text.count == 0 {
            
            textViewBackGroundViewHeightConstraint.constant = CGFloat(textViewFixedHeight)
            //messageTextView.text = textViewPlaceHolder
            //messageTextView.textColor = UIColor(red: 73.0/255.0, green: 73.0/255.0, blue: 73.0/255.0, alpha: 1.0)
            
            if placeHolderLabel != nil {
                placeHolderLabel!.isHidden = false
            }
            
        } else {
            
            if placeHolderLabel != nil {
                placeHolderLabel!.isHidden = true
            }
            
            //messageTextView.textColor = UIColor(red: 140.0/255.0, green: 140.0/255.0, blue: 140.0/255.0, alpha: 1.0)
        }
        
        if  messageTextView.contentSize.height >= CGFloat(textViewFixedHeight) && messageTextView.contentSize.height <= 180 {
            textViewBackGroundViewHeightConstraint.constant = messageTextView.contentSize.height - 10
        }
        
    }
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let newText = ((textView.text as NSString?)?.replacingCharacters(in: range, with: text))!
        if newText.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {

            if placeHolderLabel != nil {
                placeHolderLabel.isHidden = false
            }
            self.sendMessageButton.isEnabled = false
            if text == "\n" {
                textView.resignFirstResponder()
            }
            
            if self.channelId != "-1" {
                self.typingMessageValue = TypingMessage.stopTyping.rawValue
                sendMessageToFaye(message: "", isTyping: typingMessageValue)
                self.typingMessageValue = TypingMessage.startTyping.rawValue
            }
            if text == " " {
                return false
            }
            
        } else {
            
            if placeHolderLabel != nil {
                placeHolderLabel.isHidden = true
            }
            self.sendMessageButton.isEnabled = true
            if typingMessageValue == TypingMessage.startTyping.rawValue, self.channelId != "-1" {
                sendMessageToFaye(message: "", isTyping: typingMessageValue)
                self.typingMessageValue = TypingMessage.stopTyping.rawValue
            }
            
        }
        
        /*
         if text == "\n" {
         textView.resignFirstResponder()
         }
         */
        return true
        
    }

}

// MARK: UITableView Cells

class SelfMessageTableViewCell: UITableViewCell {

    @IBOutlet var selfMessageLabel: UILabel!
    @IBOutlet var bgView: UIView!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var readUnreadImageView: UIImageView!
    @IBOutlet var chatImageView: UIImageView!
 
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

class SupportMessageTableViewCell: UITableViewCell {
    
    @IBOutlet var bgView: UIView!
    @IBOutlet var supportMessageLabel: UILabel!
    @IBOutlet var dateTimeLabel: UILabel!
    @IBOutlet var supportImageView: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}

class TypingViewTableViewCell: UITableViewCell {
    
    @IBOutlet var bgView: UIView!
    @IBOutlet var gifImageView: UIImageView!
    
    
}




