//
//  HTTPClient.swift
//  Pally Asia
//  
//  Created by cl-macmini-117 on 26/09/16.
//  Copyright © 2015 CilckLabs. All rights reserved.
//
import Foundation

enum HttpMethodType: String {
    case GET
    case POST
    case PUT
    case DELETE
}

enum ParsingErrors: Error {
    case notConvertible
    case statusCodeNotFound
}

let STATUS_CODE_SUCCESS = 200
let STATUS_UPLOAD_SUCCESS = 201

let API_PUT_USER_DETAILS = "api/users/putUserDetails"
let API_GET_CONVERSATIONS = "api/conversation/getConversations"
let API_GET_MESSAGES = "api/conversation/getMessages"
let API_CREATE_CONVERSATION = "api/conversation/createConversation"
let API_GET_MESSAGES_BASED_ON_LABEL = "api/conversation/getByLabelId"
let API_CLEAR_USER_DATA_LOGOUT = "api/users/userlogout"
let API_UPLOAD_FILE = "api/conversation/uploadFile"

class HTTPClient {
    
    // MARK: - Properties
    var dataTask: URLSessionDataTask?
    
    // MARK: - Type Properties
    static var shared = HTTPClient()
    
    static var baseUrl: String? = FuguConfig.shared.baseUrl
    

    typealias ServiceResponse = (_ responseObject: Any?, _ error: Error?, _ extendedUrl: String?, _ statusCode: Int?) -> Void
    
    // MARK: - Methods
    func makeSingletonConnectionWith(method: HttpMethodType, showAlert: Bool = true, showAlertInDefaultCase: Bool = true, showActivityIndicator: Bool = true, para: [String: Any]? = nil, extendedUrl: String, callback: @escaping ServiceResponse) {
        
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        dataTask = HTTPClient.makeConcurrentConnectionWith(method: method, showAlert: showAlert, showAlertInDefaultCase: showAlertInDefaultCase, showActivityIndicator: showActivityIndicator, para: para, extendedUrl: extendedUrl, callback: callback)
    }
    
    private class func performDataTaskWith(request: URLRequest, showAlert: Bool, showAlertInDefaultCase: Bool, callback: @escaping ServiceResponse, extendedUrl: String) -> URLSessionDataTask {
        let dataTask = URLSession.shared.dataTask(with: request) {
            (data, urlResponse, error) in
            DispatchQueue.main.async {
                
                HTTPClient.stopAnimatingActivityIndicator()
                
                guard error == nil && data != nil else {
                    // connection to server failed
                    //TODO:- Do not show alert if dataTask was cancelled
                    
                    //showAlert == true ? showAlertMessage("OK", viewController: nil, message: error?.localizedDescription, title: "Error!", completion: nil): ()
                    callback(nil, error, extendedUrl, nil)
                    return
                }
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                    
                    
                    //print(json)

                    var statusCode = 0
                    
                    let responseObject = json as? [String: Any]
                    if let tempStatusCode = responseObject?["statusCode"] as? NSNumber {
                        //if statusCode is in response from server else getting it from httpResponse
                        statusCode = tempStatusCode.intValue
                    } else {
                        guard let httpUrlResponce = urlResponse as? HTTPURLResponse else {
                            callback(nil, ParsingErrors.statusCodeNotFound, extendedUrl, nil)
                            print("Error -> Status code not found")
                            return
                        }
                        statusCode = httpUrlResponce.statusCode
                    }
                    
                    let message: String = responseObject?["message"] as? String ?? "Something went wrong"
                    switch statusCode {
                    case 401:
                        expireTheSession(message)
                    case 200..<300:
                        break
                    case 500: break
                        //showAlertInDefaultCase ? showAlertMessage("OK", viewController: nil, message: "Internal server error. Please try again later.", title: "", completion: nil): ()
                    default: break
                        //showAlertInDefaultCase ? showAlertMessage("OK", viewController: nil, message: message, title: "", completion: nil): ()
                    }
                    callback(json, nil, extendedUrl, statusCode)
                    
                } catch let jsonError {
                    print("parsing error -> \(jsonError)")
                    print("Wrong json -> " + (String(data: data!, encoding: .utf8) ?? ""))
                    
                    //showAlertInDefaultCase ? showAlertMessage("OK", viewController: nil, message: "Something went wrong. Please try again later.", title: "", completion: nil): ()
                    
                    callback(nil, error, extendedUrl, nil)
                }
                //                print("urlResponse -> \(urlResponse)")
                //                print("error -> \(error)")
            }
        }
        
        return dataTask
    }
    
    
    // MARK: - Private Type Method
    private class func createRequestWith(method: HttpMethodType, timeout: Double, baseUrl: String, extendedUrl: String, contentType: String) -> URLRequest {
        let url = URL(string: baseUrl + extendedUrl)!
        var mutableRequest = URLRequest(url: url)
        mutableRequest.timeoutInterval = timeout
        mutableRequest.httpMethod = method.rawValue
        
        mutableRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        /*if let token = "" {
            mutableRequest.setValue("\(token)", forHTTPHeaderField: "authorization")
        }*/
        return mutableRequest
    }

    private class func startAnimatingActivityIndicator() {
        //ActivityIndicator.shared.startAnimatingIndicator()
    }
    
    private class func stopAnimatingActivityIndicator() {
       // ActivityIndicator.shared.stopAnimatingIndicator()
    }
    
    
    // MARK: - Type Methods
    
///     Concurrent connection with server.
    
///      - parameter method:  Pass Http Method like .GET.
///       - parameter showAlert: should show alert like no internet connection etc.
///       - parameter showAlertInDefaultCase:  Should show alert in case connection was successful but resulted in error.
///       - parameter para: Parameters to send to server.
///       - parameter extendedUrl: url afer baseurl
///      - parameter callback: responce from server
    
    @discardableResult
    class func makeConcurrentConnectionWith(method: HttpMethodType, showAlert: Bool = true, showAlertInDefaultCase: Bool = true, showActivityIndicator: Bool = true, para: [String: Any]? = nil, baseUrl: String = HTTPClient.baseUrl!, extendedUrl: String, callback: @escaping ServiceResponse) -> URLSessionDataTask? {
        
        //Request
        var mutableRequest = HTTPClient.createRequestWith(method: method, timeout: 30, baseUrl: baseUrl, extendedUrl: extendedUrl, contentType: "application/json")
        
        showActivityIndicator ? HTTPClient.startAnimatingActivityIndicator(): ()
        
        if para != nil {
            if let body = try? JSONSerialization.data(withJSONObject: para!, options: []) {
                mutableRequest.httpBody = body
            }
        }
        
        //DataTask
        let dataTask = HTTPClient.performDataTaskWith(request: mutableRequest, showAlert: showAlert, showAlertInDefaultCase: showAlertInDefaultCase, callback: callback, extendedUrl: extendedUrl)

        dataTask.resume()
        
        return dataTask
    }
    
    ///     Multipart form-data connection with server.
    
    ///      - parameter method:  Pass Http Method like .GET.
    ///       - parameter showAlert: should show alert like no internet connection etc.
    ///       - parameter showAlertInDefaultCase:  Should show alert in case connection was successful but resulted in error.
    ///       - parameter para: Parameters to send to server.
    ///       - parameter extendedUrl: url afer baseurl
    ///       - parameter imageList: Dictionary type list whose 'key' represents key of the parameter against which image is to be sent to server and 'value' of respective key is path of the image or array of paths in Directory.
    ///      - parameter callback: responce from server
//    @discardableResult
    class func makeMultiPartRequestWith(method: HttpMethodType, showAlert: Bool = true, showAlertInDefaultCase: Bool = true, showActivityIndicator: Bool = true, para: [String: Any]? = nil, baseUrl: String = HTTPClient.baseUrl!, extendedUrl: String, imageList: [String: Any]? = nil, callback: @escaping ServiceResponse) {

        let boundary = "Boundary+\(arc4random())\(arc4random())"
        
        showActivityIndicator ? HTTPClient.startAnimatingActivityIndicator(): ()
        
        let timeout: Double = 30 + Double(15 * (imageList?.count ?? 0))
        var mutableRequest = HTTPClient.createRequestWith(method: method, timeout: timeout, baseUrl: baseUrl, extendedUrl: extendedUrl, contentType: "multipart/form-data; boundary=\(boundary)")
        
        var body = Data()
        
        //Image upload
        if imageList != nil {
            for (key, path) in imageList! {
                
                if let arrayOfPaths = path as? [String] {
                    for tempPath in arrayOfPaths {
                        body.appendImageWith(key: key, path: tempPath, boundary: boundary)
                    }
                    continue
                } else if let pathString = path as? String {
                    body.appendImageWith(key: key, path: pathString, boundary: boundary)
                } else {
                    print("Error -> Not valid path of image in imageList")
                }
                
            }
        }
        
        //appending parameters
        if para != nil {
            
            for (key, value) in para! {
                
                if value is [String: Any] || value is [Any] {
                    
                do {
                        body.append(boundary: boundary)
                        let data  = try JSONSerialization.data(withJSONObject: value, options: JSONSerialization.WritingOptions.prettyPrinted )
                        let jsonString: NSString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)!
                        body.appendParameter(name: key)
                        body.append("\(jsonString)\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
                    } catch let error as NSError {
                        print("json error: \(error.localizedDescription)")
                    }
                    
                } else {
                    
                    body.append(boundary: boundary)
                    body.appendParameter(name: key)
                    body.append("\(value)\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
                }
            }
        }
        
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        
        mutableRequest.httpBody = body
        
        let dataTask = performDataTaskWith(request: mutableRequest, showAlert: showAlert, showAlertInDefaultCase: showAlertInDefaultCase, callback: callback, extendedUrl: extendedUrl)
        dataTask.resume()
    }
    
    class func expireTheSession(_ message: String?) {
      NSLog("-----Logging Out--------")
        if let message = message {
            //showAlertMessage("OK", viewController: nil, message: message, title: nil, completion: nil)
        }
        //setToken(nil)
        //setStoryboardOnWindow(StoryBoardName.BeforeLogin)
        //MessagingManager.shared.logout()
        //UserDetails.current = nil
        //clearNotificationCenter()
    }
    
    
}

private extension Data {
    mutating func append(boundary: String) {
        self.append("--\(boundary)\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
    }
    
    mutating func appendContentDepositionWith(key: String, fileName: String) {
        self.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(fileName).jpeg\"\r\n" .data(using: String.Encoding.utf8, allowLossyConversion: true)!)
    }
    
    mutating func appendContentTypeData(type: String) {
        self.append("Content-Type: image/\(type)\r\n\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
    }
    
    mutating func appendParameter(name: String) {
        self.append("Content-Disposition: form-data; name=\"\(name)\"\r\n\r\n" .data(using: String.Encoding.utf8, allowLossyConversion: true)!)
    }
    
    mutating func appendImageWith(key: String, path: String, boundary: String) {
        guard let imageData = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
            print("ERROR -> Image file not found")
            return
        }
        self.append(boundary: boundary)
        self.appendContentDepositionWith(key: key, fileName: "Image" + key)
        self.appendContentTypeData(type: "jpg")
        self.append(imageData)
        self.append("\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
    }
}
