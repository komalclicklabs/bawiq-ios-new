//
//  FuguConfig.swift
//  Fugu
//
//  Created by CL-macmini-88 on 5/16/17.
//  Copyright © 2017 CL-macmini-88. All rights reserved.
//

import Foundation
import UIKit

@objc public class FuguConfig : NSObject {
    
    public static var shared = FuguConfig()
    
    var fullName: String? = ""
    var email: String? = ""
    var phoneNumber: String? = ""
    var userUniqueKey: String? = ""
    var deviceToken: String? = ""
    var subscribedChannelId = ""
    let deviceTypeIOS = 2
    

    //https://alpha-api.fuguchat.com:3000/ <- DEV disugf74r982fh9IUUHU82E8HD98
    //https://beta-api.fuguchat.com:3001/ <- TEST kenwfdhwoiujGSDIUUHU82E8HD98
    //https://api.fuguchat.com:3002/ <- LIVE vdfvvbdfgb786347823vhjdvwfjcvj
    

    open var fayeBaseURLString: String? = "https://api.fuguchat.com:3002/faye"
    
    open var appSecretKey = ""
    open var baseUrl = "https://api.fuguchat.com:3002/"

    
    open var navigationBackgroundColor: UIColor? = UIColor(red: 33.0/255.0, green: 150.0/255.0, blue: 243.0/255.0, alpha: 1.0)
    open var navigationHeaderViewBackgroundColor: UIColor? = UIColor(red: 33.0/255.0, green: 150.0/255.0, blue: 243.0/255.0, alpha: 1.0)
    
    open var navigationTitleColor: UIColor? = .white
    
    open var backButtonImageIcon = UIImage(named: "backButtonNormalStateIcon")
    open var sendMessageButtonIcon: UIImage? = UIImage(named: "sendMessageIcon")
    
    open var allConversationBackButtonText: String? = ""
    open var chatScrenBackButtonText: String? = ""
    
    open var backButtonFont: UIFont? = UIFont.systemFont(ofSize: 13.0)
    open var backButtonTextColor: UIColor? = .white
    
    open var navigationTitleText: String? = "Support"
    open var navigationTitleFontSize: UIFont? = UIFont.boldSystemFont(ofSize: 17.0)
    open var navigationTitleTextAlignMent: NSTextAlignment? = .center
    
    open var themColor: UIColor? = UIColor(red: 33.0/255.0, green: 150.0/255.0, blue: 243.0/255.0, alpha: 1.0)
    
    var fuguUserId = -1
    var pushInfo = [String: Any]()

    override init() {
    }
    
    public convenience init(fullName: String?, email: String?, phoneNumber: String?, userUniqueKey: String?, deviceToken: String?) {
        self.init()
        
        self.fullName = fullName
        self.email = email
        self.phoneNumber = phoneNumber
        self.userUniqueKey = userUniqueKey
        self.deviceToken = deviceToken
    }
    
    public func updateFuguUserDetails(fullName: String?, email: String?, phoneNumber: String?, userUniqueKey: String?, deviceToken: String?) {
        
        self.fullName = fullName
        self.email = email
        self.phoneNumber = phoneNumber
        self.userUniqueKey = userUniqueKey
        self.deviceToken = deviceToken

    }

    public func presentChatsViewController(_ viewCtrl: UIViewController) {
        
        presentAllChatsViewController(viewController: viewCtrl)
        
    }
    
    public func pushChatsViewController(_ navCtrl: UINavigationController) {
        
        pushAllChatsViewController(navigationController: navCtrl)
        
    }
    
    public func openSpecificChatViewController(_ viewCtrl: UIViewController, messageChannelId: Int) {

        openChatViewController(viewController: viewCtrl, messageChannelId: messageChannelId)
    }
    
    public func clearFuguUserData() {
        
        var params = [String: Any]()
        params["app_secret_key"] = appSecretKey
        
        if let savedUserId = UserDefaults.standard.value(forKey: FUGU_USER_ID) as? Int {
            params["user_id"] = savedUserId
        }

        HTTPClient.shared.makeSingletonConnectionWith(method: .POST, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: false, para: params, extendedUrl: API_CLEAR_USER_DATA_LOGOUT) { (responseObject, error, tag, statusCode) in
            
            let defaults = UserDefaults.standard
            
            defaults.removeObject(forKey: FUGU_DEFAULT_CHANNELS)
            defaults.removeObject(forKey: FUGU_DEFAULT_MESSAGES)
            defaults.removeObject(forKey: FUGU_USER_ID)
            
            defaults.synchronize()
            
        }
        
    }

    public func switchEnvironment(_ envType: String) {
        
        if envType == "TEST" {
            
            fayeBaseURLString = "https://beta-api.fuguchat.com:3001/faye"
            baseUrl = "https://beta-api.fuguchat.com:3001/"
            
        } else if envType == "DEV" {
             
            fayeBaseURLString = "https://alpha-api.fuguchat.com:3000/faye"
            baseUrl = "https://alpha-api.fuguchat.com:3000/"
            
        } else {
            
            fayeBaseURLString = "https://api.fuguchat.com:3002/faye"
            baseUrl = "https://api.fuguchat.com:3002/"
            
        }
        
    }
    
    public func isFuguNotification(userInfo: [String: Any]) -> Bool {
        
        if let pushSource = userInfo["push_source"] as? String, pushSource == "FUGU" {
            
            return toShowInAppNotification(userInfo: userInfo)
        }
        return false
    }
    
    func createParamsForPutUserDetails() -> [String: Any] {
        
        var params = [
            
            "device_id" : UIDevice.current.identifierForVendor?.uuidString ?? 0,
            "app_secret_key" : FuguConfig.shared.appSecretKey,
            "device_type" : FuguConfig.shared.deviceTypeIOS
            
            ] as [String: Any]
        
        if (FuguConfig.shared.fullName?.count)! > 0 {
            params["full_name"] = FuguConfig.shared.fullName
        }
        
        if (FuguConfig.shared.email?.count)! > 0 {
            params["email"] = FuguConfig.shared.email
        }
        
        if (FuguConfig.shared.phoneNumber?.count)! > 0 {
            params["phone_number"] = FuguConfig.shared.phoneNumber
        }
        
        if (FuguConfig.shared.userUniqueKey?.count)! > 0 {
            params["user_unique_key"] = FuguConfig.shared.userUniqueKey
        }
        
        if (FuguConfig.shared.deviceToken?.count)! > 0 {
            params["device_token"] = FuguConfig.shared.deviceToken
        }
        
        var deviceDetails = [String: Any]()
        deviceDetails["ios_operating_system"] = "IOS"
        deviceDetails["ios_model"] = UIDevice.current.model
        deviceDetails["ios_manufacturer"] = "APPLE"
        deviceDetails["ios_os_version"] = UIDevice.current.systemVersion
        
        do {
            
            let jsonData = try JSONSerialization.data(withJSONObject: deviceDetails, options: .prettyPrinted)
            
            var service = (NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! ) as String
            service = service.replacingOccurrences(of: "\n", with: "")
            service = service.replacingOccurrences(of: "\\", with: "")
            
            params["device_details"] = service
            
        } catch let error as NSError {
            
            print(error.description)
            
        }
        
        return params
    }
    
    public func registerUserDetails() {
        
        HTTPClient.shared.makeSingletonConnectionWith(method: .POST, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: false, para: createParamsForPutUserDetails(), extendedUrl: API_PUT_USER_DETAILS) { (responseObject, error, tag, statusCode) in
            
            let response = responseObject as? [String: Any]
            
            if let data = response?["data"] as? [String: Any] {
                
                if let userId = data["user_id"] as? Int {
                    UserDefaults.standard.set(userId, forKey: FUGU_USER_ID)
                }
                
            }
            
        }
        
    }
    
    public func toShowInAppNotification(userInfo: [String: Any]) -> Bool {
        
        if UIApplication.shared.applicationState == .active {
            
            if let childViewControllers = UIApplication.shared.keyWindow?.rootViewController?.childViewControllers {
                
                if let lastVisibleCtrl = getViewControllerCurrentlyShownByContainerView(childViewControllers), lastVisibleCtrl.classForCoder == ConversationsViewController.classForCoder() {
                    
                    if let channelId = userInfo["channel_id"] as? Int {
                        
                        if self.subscribedChannelId == "\(channelId)" {
                            
                            return false
                        }
                        
                    }
                }
                
                if let lastVisibleCtrl = getViewControllerCurrentlyShownByContainerView(childViewControllers), lastVisibleCtrl.classForCoder == ShowAllConersationsViewController.classForCoder() {
                    
                    (lastVisibleCtrl as! ShowAllConersationsViewController).updateChannelsWithrespectToPush(pushInfo: userInfo, moveToChatController: false)
                    
                    return true
                }
            }
            return true
            
        } else if UIApplication.shared.applicationState == .background {
            
            if let childViewControllers = UIApplication.shared.keyWindow?.rootViewController?.childViewControllers {
                
                if let lastVisibleCtrl = getViewControllerCurrentlyShownByContainerView(childViewControllers), lastVisibleCtrl.classForCoder == ShowAllConersationsViewController.classForCoder() {
                    
                    if lastVisibleCtrl.classForCoder == ShowAllConersationsViewController.classForCoder() {
                        (lastVisibleCtrl as! ShowAllConersationsViewController).updateChannelsWithrespectToPush(pushInfo: userInfo, moveToChatController: true)
                    }
                    
                    
                } else if let lastVisibleCtrl = getViewControllerCurrentlyShownByContainerView(childViewControllers), lastVisibleCtrl.classForCoder == ConversationsViewController.classForCoder() {
                    
                    guard let channelId = userInfo["channel_id"] as? Int, self.subscribedChannelId != "\(channelId)" else {
                        return false
                    }
                    
                    if lastVisibleCtrl.classForCoder == ConversationsViewController.classForCoder() {
                        DispatchQueue.main.async {
                            (lastVisibleCtrl as! ConversationsViewController).isChatDeactivated = false
                            (lastVisibleCtrl as! ConversationsViewController).getMessagesBasedOnChannel(channelId, pageStart: 1)
                        }
                    }
                    
                } else {
                    isHandlingNotification = true
                    pushInfo = userInfo
                    presentAllChatsViewController(viewController: (UIApplication.shared.keyWindow?.rootViewController)!)
                }
                
            }
            return false
            
        }
        
        return false
        
    }
    
    func getViewControllerCurrentlyShownByContainerView(_ childViewControllers : [Any]) -> UIViewController? {
        
        if let controller =  childViewControllers.first as? UINavigationController {
            
            return returnFromNavigationViewController(navigationController: controller)
            
        } else {
            
            if let controller =  childViewControllers.last as? UINavigationController {
                
                return returnFromNavigationViewController(navigationController: controller)
                
            }
            
            if let controller =  childViewControllers.first as? UITabBarController {
                
                if let visibleController = controller.selectedViewController {
                    
                    if  visibleController is UINavigationController {
                        if (visibleController as! UINavigationController).viewControllers.count > 0 {
                            return returnFromNavigationViewController(navigationController: visibleController as! UINavigationController)
                        } else {
                            if let moreNav = controller.moreNavigationController as? UINavigationController {
                                return returnFromNavigationViewController(navigationController: moreNav)
                            }
                            
                        }
                        
                    }
                    
                    if visibleController is UITabBarController {
                        return getViewControllerCurrentlyShownByContainerView([visibleController])
                    }
                    
                    return visibleController
                    
                }
                
            }
            
            if let controller =  childViewControllers.last as? UITabBarController {
                
                if let visibleController = controller.selectedViewController {
                    if  visibleController is UINavigationController {
                        return returnFromNavigationViewController(navigationController: visibleController as! UINavigationController)
                    }
                    
                    if visibleController is UITabBarController {
                        return getViewControllerCurrentlyShownByContainerView([visibleController])
                    }
                    
                    
                    return visibleController
                    
                }
                
            }
            
            if let controller = childViewControllers.first as? UIViewController {
                return controller
            }
            if let controller = childViewControllers.last as? UIViewController {
                return controller
            }
            return nil
            
        }
        
    }
    
    func returnFromNavigationViewController(navigationController: UINavigationController) -> UIViewController? {
        
        if let visibleVC = navigationController.visibleViewController {
            
            return visibleVC
            
        } else if let presentedVC = navigationController.presentedViewController {
            
            return presentedVC
            
        } else {
            
            let controllers = navigationController.viewControllers
            if controllers.count > 0 {
                return controllers[controllers.count - 1]
            }
            return navigationController
        }
        
    }
    
    public func handleRemoteNotification(userInfo: [String: Any]) {
        
        if UIApplication.shared.applicationState == .active {
            
            if let childViewControllers = UIApplication.shared.keyWindow?.rootViewController?.childViewControllers {
                
                if let lastVisibleCtrl = getViewControllerCurrentlyShownByContainerView(childViewControllers), lastVisibleCtrl.classForCoder == ShowAllConersationsViewController.classForCoder() {
                    
                    (lastVisibleCtrl as! ShowAllConersationsViewController).updateChannelsWithrespectToPush(pushInfo: userInfo, moveToChatController: true)
                    
                    return
                } else if let lastVisibleCtrl = getViewControllerCurrentlyShownByContainerView(childViewControllers), lastVisibleCtrl.classForCoder == ConversationsViewController.classForCoder() {
                    
                    guard let channelId = userInfo["channel_id"] as? Int, self.subscribedChannelId != "\(channelId)" else {
                        return
                    }
                    
                    DispatchQueue.main.async {
                        (lastVisibleCtrl as! ConversationsViewController).isChatDeactivated = false
                        (lastVisibleCtrl as! ConversationsViewController).getMessagesBasedOnChannel(channelId, pageStart: 1)
                    }
                    
                    return
                } else {
                    isHandlingNotification = true
                    pushInfo = userInfo
                    presentAllChatsViewController(viewController: (UIApplication.shared.keyWindow?.rootViewController)!)
                }
                
            }
            
        } else {
            presentAllChatsViewController(viewController: (UIApplication.shared.keyWindow?.rootViewController)!)
        }
        
    }
    
}
