//
//  Helpers.swift
//  Fugu
//
//  Created by CL-macmini-88 on 5/11/17.
//  Copyright © 2017 CL-macmini-88. All rights reserved.
//

import Foundation
import UIKit
import ImageIO
import SystemConfiguration
import AVFoundation
// MARK: Navigation

var isHandlingNotification: Bool = false
let FUGU_DEFAULT_CHANNELS = "fuguDefaultChannels"
let FUGU_DEFAULT_MESSAGES = "fuguDefaultMessages"
let FUGU_USER_ID = "fuguUserId"

var bundle:Bundle {
    
    let podBundle = Bundle(for: ShowAllConersationsViewController.self)
    let bundleURL = podBundle.url(forResource: "Fugu", withExtension: "bundle")
    return Bundle(url: bundleURL!)!
    
}

public func presentAllChatsViewController(viewController: UIViewController) {
    
    let storyboard = UIStoryboard(name: "FuguUnique", bundle: bundle)
    if let navigationController = storyboard.instantiateViewController(withIdentifier: "FuguNavigationController") as? UINavigationController {
        
        if isHandlingNotification {
            isHandlingNotification = false
            var controllersInStack = navigationController.viewControllers
            if let pushController = storyboard.instantiateViewController(withIdentifier: "ConversationsViewController") as? ConversationsViewController {
                controllersInStack.append(pushController)
            }
            navigationController.viewControllers = controllersInStack
        }
        navigationController.modalPresentationStyle = .overCurrentContext
        viewController.present(navigationController, animated: true, completion: nil)
    }
    
}

public func pushAllChatsViewController(navigationController: UINavigationController) {
    
    let storyboard = UIStoryboard(name: "FuguUnique", bundle: bundle)
    if let pushToFilterViewController = storyboard.instantiateViewController(withIdentifier: "ShowAllConersationsViewController") as? ShowAllConersationsViewController {
        pushToFilterViewController.isPushed = true
        _ = navigationController.pushViewController(pushToFilterViewController, animated: true)
    }
 
}


public func openChatViewController(viewController: UIViewController, messageChannelId: Int) {

    let storyboard = UIStoryboard(name: "FuguUnique", bundle: bundle)
    if let pushToFilterViewController = storyboard.instantiateViewController(withIdentifier: "ConversationsViewController") as? ConversationsViewController {
        pushToFilterViewController.openChatBasedOnSpecificLabel(messageChannelId, pageStart: 1)
        viewController.present(pushToFilterViewController, animated: true, completion: nil)
        
    }
    
}

// MARK: Helper Methods

func pathToDocumentsDirectory() -> String {
    
    let documentsPath : AnyObject = NSSearchPathForDirectoriesInDomains(.documentDirectory,.userDomainMask,true)[0] as AnyObject
    return documentsPath as! String
}

func createImgDirectory(_ name:String){
    let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
    let documentsDirectory: NSString = paths[0] as NSString
    
    let directoryImgFolderPath = documentsDirectory.appendingPathComponent(name)
    do {
        try FileManager.default.createDirectory(atPath: directoryImgFolderPath, withIntermediateDirectories: false, attributes: nil)
    } catch let error as NSError {
        print(error.localizedDescription);
    }
}

public func deleteContentsOfFolder(_ folderPath:String) {
    // folderURL
    if let folderURL = Foundation.URL(string: folderPath)
    {
        // enumerator
        if let enumerator = FileManager.default.enumerator(at: folderURL, includingPropertiesForKeys: nil, options: [], errorHandler: nil)
        {
            // item
            while let item = enumerator.nextObject() {
                // itemURL
                if let itemURL = item as? Foundation.URL {
                    do {
                        try FileManager.default.removeItem(at: itemURL)
                    }
                    catch let error as NSError {
                        print("JBSFile Exception: Could not delete item within folder.  \(error)")
                    }
                    catch {
                        print("JBSFile Exception: Could not delete item within folder.")
                    }
                }
            }
        }
    }
}

func convertCurrentDateTimeToUTC() -> String {
    
    var utcDateTimeString = String(describing: Date())
    utcDateTimeString = utcDateTimeString.replacingOccurrences(of: " +0000", with: "")
    
    let formatterUTC = DateFormatter()
    formatterUTC.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    formatterUTC.timeZone = TimeZone(secondsFromGMT: 0)
    
    return formatterUTC.string(from: Date())
    
}


func attributedStringForLabel(_ firstString: String, secondString: String, thirdString: String, colorOfFirstString: UIColor, colorOfSecondString: UIColor, colorOfThirdString: UIColor, fontOfFirstString: UIFont, fontOfSecondString: UIFont, fontOfThirdString: UIFont, textAlighnment: NSTextAlignment, dateAlignment: NSTextAlignment) -> NSMutableAttributedString {
    
    let combinedString = "\(firstString)\(secondString)\(thirdString)" as NSString
    
    /*
     let paragraphStyle = NSMutableParagraphStyle()
     paragraphStyle.alignment = textAlighnment
     */
    
    let rangeOfFirstString = combinedString.range(of: firstString)
    let rangeOfSecondString = combinedString.range(of: secondString)
    let rangeOfThirdString = combinedString.range(of: thirdString)
    
    let firstStringStyle = NSMutableParagraphStyle()
    firstStringStyle.alignment = textAlighnment
    
    let thirdStringStyle = NSMutableParagraphStyle()
    thirdStringStyle.alignment = dateAlignment
    
    let attributedTitle = NSMutableAttributedString(string: combinedString as String)
    
    attributedTitle.addAttribute(NSForegroundColorAttributeName, value: colorOfFirstString, range: rangeOfFirstString)
    attributedTitle.addAttribute(NSFontAttributeName, value: fontOfFirstString, range: rangeOfFirstString)
    attributedTitle.addAttribute(NSParagraphStyleAttributeName, value: firstStringStyle, range: rangeOfFirstString)
    
    attributedTitle.addAttribute(NSForegroundColorAttributeName, value: colorOfSecondString, range: rangeOfSecondString)
    attributedTitle.addAttribute(NSFontAttributeName, value: fontOfSecondString, range: rangeOfSecondString)
    attributedTitle.addAttribute(NSParagraphStyleAttributeName, value: firstStringStyle, range: rangeOfSecondString)
    
    attributedTitle.addAttribute(NSForegroundColorAttributeName, value: colorOfThirdString, range: rangeOfThirdString)
    attributedTitle.addAttribute(NSFontAttributeName, value: fontOfThirdString, range: rangeOfThirdString)
    attributedTitle.addAttribute(NSParagraphStyleAttributeName, value: thirdStringStyle, range: rangeOfThirdString)
    
    /*
     attributedTitle.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSRange(location: 0, length: (combinedString as String).count))
     */
    
    return attributedTitle
}

func attributedStringForLabelForTwoStrings(_ firstString: String, secondString: String, colorOfFirstString: UIColor, colorOfSecondString: UIColor, fontOfFirstString: UIFont, fontOfSecondString: UIFont, textAlighnment: NSTextAlignment, dateAlignment: NSTextAlignment) -> NSMutableAttributedString {
    
    let combinedString = "\(firstString)\(secondString)" as NSString
    
    let rangeOfFirstString = combinedString.range(of: firstString)
    let rangeOfSecondString = combinedString.range(of: secondString)
    
    let firstStringStyle = NSMutableParagraphStyle()
    firstStringStyle.alignment = textAlighnment
    
    let thirdStringStyle = NSMutableParagraphStyle()
    thirdStringStyle.alignment = dateAlignment
    
    let attributedTitle = NSMutableAttributedString(string: combinedString as String)
    
    attributedTitle.addAttribute(NSForegroundColorAttributeName, value: colorOfFirstString, range: rangeOfFirstString)
    attributedTitle.addAttribute(NSFontAttributeName, value: fontOfFirstString, range: rangeOfFirstString)
    attributedTitle.addAttribute(NSParagraphStyleAttributeName, value: firstStringStyle, range: rangeOfFirstString)
    
    attributedTitle.addAttribute(NSForegroundColorAttributeName, value: colorOfSecondString, range: rangeOfSecondString)
    attributedTitle.addAttribute(NSFontAttributeName, value: fontOfSecondString, range: rangeOfSecondString)
    attributedTitle.addAttribute(NSParagraphStyleAttributeName, value: firstStringStyle, range: rangeOfSecondString)
    
    return attributedTitle
}

func changeDateToParticularFormat(_ dateTobeConverted: Date, dateFormat: String, showInFormat: Bool) -> String {
    
    let formatter = DateFormatter()
    formatter.dateFormat = dateFormat   //"d MMM yyyy, hh:mm a"
    formatter.locale = Locale(identifier: "en_US_POSIX")
    formatter.timeZone = TimeZone.current

    if !showInFormat {
        let comparisonResult = Calendar.current.compare(dateTobeConverted, to: Date(), toGranularity: .day)
        
        switch comparisonResult {
            
        case .orderedSame:

            return "Today"
            
        default:
            
            let calendar = NSCalendar.current
            let dateOfMsg = calendar.startOfDay(for: dateTobeConverted)
            let currentDate = calendar.startOfDay(for: Date())
            
            let dateDifference = calendar.dateComponents([.day], from: dateOfMsg, to: currentDate).day
            
            if dateDifference == 1 {
                return "Yesterday"
            }
            return formatter.string(from: dateTobeConverted)
            
        }
    }

    return formatter.string(from: dateTobeConverted)
    
}

func connectedToNetwork() -> Bool {
    
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
            SCNetworkReachabilityCreateWithAddress(nil, $0)
        }
    }) else {
        return false
    }
    
    var flags: SCNetworkReachabilityFlags = []
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
        return false
    }
    
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    
    return (isReachable && !needsConnection)
}

//MARK: Extentions

extension String {
    
    var toDate: Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        guard let date = formatter.date(from: self) else {
            return nil
        }
        
        return date
    }
    
    func checkNA() -> String {
        let str = self.trimmingCharacters(in: .whitespacesAndNewlines)
        if str.count == 0 {
            return "--"
        } else {
            return self
        }
    }
}

extension Date {
    
    var toString: String! {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM yyyy, hh:mm a"
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone.current

        let comparisonResult = Calendar.current.compare(self, to: Date(), toGranularity: .day)

        switch comparisonResult {
            
        case .orderedSame:
            
            formatter.dateFormat = "hh:mm a"
            return formatter.string(from: self)
            
        default:
            
            let calendar = NSCalendar.current
            let dateOfMsg = calendar.startOfDay(for: self)
            let currentDate = calendar.startOfDay(for: Date())
            
            let dateDifference = calendar.dateComponents([.day], from: dateOfMsg, to: currentDate).day
            
            if dateDifference == 1 {
                formatter.dateFormat = "hh:mm a"
                return "Yesterday"
            } else if (dateDifference! > 1 && dateDifference! < 8) {
                return "\(dateDifference ?? 2) days ago"
            } else {
                formatter.dateFormat = "dd/MM/yyyy"
                return formatter.string(from: self)
            }
        }
    }
    
}

extension UIImageView {

    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        downloadedFrom(url: url, contentMode: mode)
    }
    


}
