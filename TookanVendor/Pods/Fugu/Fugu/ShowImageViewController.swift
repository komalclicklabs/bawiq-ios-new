//
//  ShowImageViewController.swift
//  ChitChapp
//
//  Created by Click Labs 65 on 7/23/15.
//  Copyright (c) 2015 click Labs. All rights reserved.
//

import UIKit

class ShowImageViewController: UIViewController , UIScrollViewDelegate {
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var crossButton: UIButton!
  var imageToShow = UIImage()
 
  override func viewDidLoad() {
        super.viewDidLoad()

    crossButton.layer.cornerRadius = 31.5
    }
  
  override func viewWillAppear(_ animated: Bool) {
    self.imageView.image = imageToShow
    scrollView.minimumZoomScale = 1.0
    scrollView.maximumZoomScale = 5
    scrollView.flashScrollIndicators()
  }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  func viewForZooming(in scrollView: UIScrollView) -> UIView? {
    return self.imageView
  }
  
  @IBAction func crossButtonTapped(_ sender :AnyObject) {
    self.dismiss(animated: true, completion: nil)
  }
  

}
