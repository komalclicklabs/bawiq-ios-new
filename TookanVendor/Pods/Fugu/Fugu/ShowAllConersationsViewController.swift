//
//  ShowAllConersationsViewController.swift
//  Fugu
//
//  Created by CL-macmini-88 on 5/10/17.
//  Copyright © 2017 CL-macmini-88. All rights reserved.
//

import UIKit
import FayeSwift

public class ShowAllConersationsViewController: UIViewController, NewChatSentDelegate {

    // MARK: OUTLETS
    
    @IBOutlet var showConversationsTableView: UITableView!
    @IBOutlet var navigationBackgroundView: UIView!
    @IBOutlet var navigationTitleLabel: UILabel!
    @IBOutlet var backButton: UIButton!
    @IBOutlet var navigationHeaderView: UIView!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var errorLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet var newConversationButton: UIButton!
    @IBOutlet var newConversationButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet var poweredByFuguLabel: UILabel!
    
    // MARK: PROPERTIES

    //var conversationsArray = [Any]()
    var userDetailsObject = [String: Any]()
    var isGetAllConversations = false
    var isPushed = false
    let refreshControl = UIRefreshControl()
    var returnImage:UIImage = UIImage()
    
    var tableViewDefaultText = "Loading ..."
    let urlForFuguChat = "https://fuguchat.com/"
    // MARK: LIFECYCLE
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.automaticallyAdjustsScrollViewInsets = false
        navigationSetUp()
        putUserDetailsAndGetConversations()
        uiSetup()
    }

    public override func viewWillAppear(_ animated: Bool) {
        getAllConversations()
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func uiSetup() {
        
        newConversationButton.layer.cornerRadius = 30
        errorLabel.text = ""
        errorLabelTopConstraint.constant = -20
        
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        showConversationsTableView.backgroundView = refreshControl
        
        poweredByFuguLabel.attributedText = attributedStringForLabelForTwoStrings("Powered by ", secondString: "Fugu", colorOfFirstString: UIColor(red: 127.0/255.0, green: 127.0/255.0, blue: 127.0/255.0, alpha: 1.0), colorOfSecondString: UIColor(red: 140.0/255.0, green: 140.0/255.0, blue: 140.0/255.0, alpha: 1.0), fontOfFirstString: UIFont.systemFont(ofSize: 10.0), fontOfSecondString: UIFont.boldSystemFont(ofSize: 10.0), textAlighnment: .center, dateAlignment: .center)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.openFuguChatWebLink(_:)))
        poweredByFuguLabel.addGestureRecognizer(tap)
        
    }

    func navigationSetUp() {
        
        navigationBackgroundView.layer.shadowColor = UIColor.black.cgColor
        navigationBackgroundView.layer.shadowOpacity = 0.25
        navigationBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        navigationBackgroundView.layer.shadowRadius = 4
        
        if FuguConfig.shared.navigationHeaderViewBackgroundColor != nil {
            navigationHeaderView.backgroundColor = FuguConfig.shared.navigationHeaderViewBackgroundColor
        }
        
        if FuguConfig.shared.navigationBackgroundColor != nil {
            navigationBackgroundView.backgroundColor = FuguConfig.shared.navigationBackgroundColor
        }
        
        if FuguConfig.shared.navigationTitleColor != nil {
            navigationTitleLabel.textColor =  FuguConfig.shared.navigationTitleColor
        }
        
        if (FuguConfig.shared.allConversationBackButtonText?.count)! > 0 {
            backButton.setTitle((" " + FuguConfig.shared.allConversationBackButtonText!), for: .normal)
            
            if FuguConfig.shared.backButtonFont != nil {
                backButton.titleLabel?.font = FuguConfig.shared.backButtonFont
            }
            
            if FuguConfig.shared.backButtonTextColor != nil {
                backButton.setTitleColor(FuguConfig.shared.backButtonTextColor, for: .normal)
            } else if FuguConfig.shared.navigationTitleColor != nil {
                backButton.setTitleColor(FuguConfig.shared.navigationTitleColor, for: .normal)
            }
            
        } else {
            if FuguConfig.shared.backButtonImageIcon != nil {
                backButton.setImage(FuguConfig.shared.backButtonImageIcon, for: .normal)
            }
        }
        
        if FuguConfig.shared.navigationTitleText != nil {
            navigationTitleLabel.text = FuguConfig.shared.navigationTitleText
        }
        
        if FuguConfig.shared.navigationTitleFontSize != nil {
            navigationTitleLabel.font = FuguConfig.shared.navigationTitleFontSize
        }
        
        if FuguConfig.shared.navigationTitleTextAlignMent != nil {
            navigationTitleLabel.textAlignment = FuguConfig.shared.navigationTitleTextAlignMent!
        }
        
    }
    
    // MARK: UIView Actions
    
    func openFuguChatWebLink(_ sender: UITapGestureRecognizer) {
        
        UIApplication.shared.openURL(NSURL(string: urlForFuguChat)! as URL)
        
    }
    
    // MARK: UIButton Actions
    
    @IBAction func newConversationButtonAction(_ sender: UIButton) {
        
        //moveToChatViewController(chatObj: [:], isNew: true)
        
        let alert = UIAlertController(title: "Add label.", message: "Label?", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.keyboardType = .numberPad
            textField.text = ""
        }

        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            
            if (textField?.text?.count)! > 0 {
                let labelId = Int((textField?.text)!)
                openChatViewController(viewController: self, messageChannelId: labelId!)
            }
            
            
        }))

        self.present(alert, animated: true, completion: nil)
        

        /*DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.5) {
            openChatViewController(viewController: self, labelId: 21)
        }*/
        

    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        if !self.isPushed {
            self.dismiss(animated: true, completion: nil)
            return
        }
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    func headerEmptyAction(_ sender: UITapGestureRecognizer) {
        
        tableViewDefaultText = "Loading ..."
        self.showConversationsTableView.reloadData()
        
        self.putUserDetailsAndGetConversations()
        
    }
    
    // MARK: UIRefreshControl
    
    func refresh(_ refreshControl: UIRefreshControl) {
        
        if let chatCachedArray = UserDefaults.standard.object(forKey: FUGU_DEFAULT_CHANNELS) as? [Any], chatCachedArray.isEmpty {
            self.putUserDetailsAndGetConversations()
            return
        }
        
        isGetAllConversations = true
        getAllConversations()
    }
    
    // MARK: SERVER HIT
    
    
    func putUserDetailsAndGetConversations() {
        
        if !connectedToNetwork() {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                self.tableViewDefaultText = "No Internet Connection\n Tap to retry"
                self.showConversationsTableView.reloadData()
            }
            return
        }
        
        HTTPClient.shared.makeSingletonConnectionWith(method: .POST, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: false, para: FuguConfig.shared.createParamsForPutUserDetails(), extendedUrl: API_PUT_USER_DETAILS) { (responseObject, error, tag, statusCode) in
            
            //self.conversationsArray = []
            
            let response = responseObject as? [String: Any]
            
            if statusCode == STATUS_CODE_SUCCESS {
                
                if let data = response?["data"] as? [String: Any] {
                    self.userDetailsObject = data
                    
                    if let userId = self.userDetailsObject["user_id"] as? Int {
                        UserDefaults.standard.set(userId, forKey: FUGU_USER_ID)
                    }
                    
                    guard let botChannelsArray = data["conversations"] as? [Any], !botChannelsArray.isEmpty else {
                        self.tableViewDefaultText = "Start a New Conversation."
                        if self.showConversationsTableView != nil {
                            self.showConversationsTableView.reloadData()
                        }
                        return
                    }
                    
                    UserDefaults.standard.set(botChannelsArray, forKey: FUGU_DEFAULT_CHANNELS)
                    //self.conversationsArray = botChannelsArray
                    if self.showConversationsTableView != nil {
                        self.showConversationsTableView.reloadData()
                    }
                    
                }
                
            } else {
                if self.errorLabel != nil {
                    if let message = response?["message"] as? String, message.count > 0 {
                        
                        self.updateErrorLabelView(isHiding: false)
                        self.errorLabel.text = message
                        self.updateErrorLabelView(isHiding: true)
                        
                        
                    } else {
                        
                        self.updateErrorLabelView(isHiding: false)
                        self.errorLabel.text = "NO INTERNET CONNECTION."
                        self.updateErrorLabelView(isHiding: true)
                        
                    }
                }
                
                
            }
            
        }
        
    }
    
    func getAllConversations() {
        
        if !connectedToNetwork() {
            
            guard
                let chatCachedArray = UserDefaults.standard.object(forKey: FUGU_DEFAULT_CHANNELS) as? [Any], !chatCachedArray.isEmpty
                else {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
                        self.tableViewDefaultText = "No Internet Connection\n Tap to retry"
                        self.showConversationsTableView.reloadData()
                    }
                    return
            }
            
            self.refreshControl.endRefreshing()
            self.isGetAllConversations = false
            self.updateErrorLabelView(isHiding: false)
            self.errorLabel.text = "No Internet Connection."
            self.updateErrorLabelView(isHiding: true)
            return
            
        }
        
        if !isGetAllConversations {
            return
        }
        
        var params = [String: Any]()
        params["app_secret_key"] = FuguConfig.shared.appSecretKey
        
        if let userId = UserDefaults.standard.value(forKey: FUGU_USER_ID) {
            params["user_id"] = userId
        } else if let userId = userDetailsObject["user_id"] as? Int {
            params["user_id"] = userId
        }
        
        /*
        if FuguConfig.shared.fuguUserId != -1 {
            
        } else if let userId = userDetailsObject["user_id"] as? Int {
            params["user_id"] = userId
        }*/
        
        HTTPClient.shared.makeSingletonConnectionWith(method: .POST, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: false, para: params, extendedUrl: API_GET_CONVERSATIONS) { (responseObject, error, tag, statusCode) in
            
            let response = responseObject as? [String: Any]
            self.refreshControl.endRefreshing()
            self.isGetAllConversations = false
            
            if statusCode == STATUS_CODE_SUCCESS {
 
                if let data = response?["data"] as? [String: Any] {

                    if let conversationListArray = data["conversation_list"] as? [Any], !conversationListArray.isEmpty {
                        UserDefaults.standard.set(conversationListArray, forKey: FUGU_DEFAULT_CHANNELS)
                        //self.conversationsArray = conversationListArray
                        self.showConversationsTableView.reloadData()
                    }
                    
                }
                
            } else {
                
                if let message = response?["message"] as? String, message.count > 0 {
                    
                    self.updateErrorLabelView(isHiding: false)
                    self.errorLabel.text = message
                    self.updateErrorLabelView(isHiding: true)
                    
                } else {
                    
                    self.updateErrorLabelView(isHiding: false)
                    self.errorLabel.text = "NO INTERNET CONNECTION."
                    self.updateErrorLabelView(isHiding: true)
                    
                }
                
            }
            
        }
        
    }
    
    // MARK: HELPER
    
    func returnImageUsingCacheWithURLString(url: NSURL, index: IndexPath) -> (UIImage) {
        
        if let cachedImage = imageCacheFugu.object(forKey: url) as? UIImage {
            
            return cachedImage
        } else {
            
            URLSession.shared.dataTask(with: url as URL, completionHandler: { (data, response, error) in
                
                if error == nil {
                    
                    DispatchQueue.main.async(execute: {
                        
                        // Cache to image so it doesn't need to be reloaded everytime the user scrolls and table cells are re-used.
                        if let downloadedImage = UIImage(data: data!) {
                            
                            imageCacheFugu.setObject(downloadedImage, forKey: url)
                            
                            if let cellToUpdate = self.showConversationsTableView.cellForRow(at: index) as? ConversationView {
                                cellToUpdate.channelImageView.image = downloadedImage
                            }
                            self.returnImage = downloadedImage
                            
                        }
                    })
                    
                }
            }).resume()
            return returnImage
        }
    }
    
    func updateErrorLabelView(isHiding: Bool) {
        
        if isHiding {
            if self.errorLabelTopConstraint.constant == 0 {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
                    self.errorLabelTopConstraint.constant = -20
                    self.errorLabel.text = ""
                    
                    UIView.animate(withDuration: 0.5, animations: {
                        
                        self.view.layoutIfNeeded()
                    })
                }
            }
            return
        }
        
        self.errorLabelTopConstraint.constant = 0
        UIView.animate(withDuration: 0.5, animations: {
            self.view.layoutIfNeeded()
        })
        
        
    }
    
    // MARK: NewChatSentDelegate
    
    func newChatStartedDelgegate(isChatUpdated: Bool) {
        isGetAllConversations = isChatUpdated
    }
    
    // MARK: Navigation
    
    func moveToChatViewController(chatObj: [String: Any], isNew: Bool, selectedCell: Int? = -1) {
        
        let pushController = self.storyboard?.instantiateViewController(withIdentifier: "ConversationsViewController") as? ConversationsViewController
        
        pushController?.delegate = self
        pushController?.userDetailsObj = self.userDetailsObject
        pushController?.channelId = "-1"
        
        if chatObj.keys.count > 0 , let channelId = chatObj["channel_id"] as? Int {

            pushController?.channelId = "/\(channelId)"
            pushController?.client = FayeClient(aFayeURLString: FuguConfig.shared.fayeBaseURLString!, channel: "/\(channelId)")
            
            if let activationStatus = chatObj["status"] as? Int, activationStatus == 0 {
                pushController?.isChatDeactivated = true
            }
            
            if let labelId = chatObj["label_id"] as? Int {
                pushController?.labelId = labelId
            }
            
            if selectedCell! > -1, let unreadCount = chatObj["unread_count"] as? Int, unreadCount > 0 {
                
                var chatCachedArray = UserDefaults.standard.object(forKey: FUGU_DEFAULT_CHANNELS) as? [Any]
                //if !conversationsArray.isEmpty {

                var updatedChatObj = chatObj
                updatedChatObj["unread_count"] = 0
                chatCachedArray?[selectedCell!] = updatedChatObj
                UserDefaults.standard.set(chatCachedArray, forKey: FUGU_DEFAULT_CHANNELS)
                //conversationsArray[selectedCell!] = updatedChatObj
                self.showConversationsTableView.reloadRows(at: [IndexPath(row: selectedCell!, section: 0)], with: .none)

            }
            
            if let user_name = userDetailsObject["full_name"] as? String {
                pushController?.userName = user_name
            }
            
            if !isNew && channelId != -1 {
                pushController?.getMessagesBasedOnChannel(channelId, pageStart: 1)
                
            } else if channelId == -1 {
                
                pushController?.channelId = "\(channelId)"
                pushController?.checkIfDateExistsElseUpdate([chatObj])
                
            }

            if let channelName = chatObj["label"] as? String {
                pushController?.channelName = channelName
            }

        }
        
        self.navigationController?.pushViewController(pushController!, animated: true)
        
    }
    
    // MARK: HANDLE PUSH NOTIFICATION
    
    func updateChannelsWithrespectToPush(pushInfo: [String: Any], moveToChatController: Bool) {
        
        guard let pushChannelId = pushInfo["channel_id"] as? Int else {
            return
        }
        
        guard let pushMessage = pushInfo["new_message"] as? String else {
            return
        }
        
        var chatCachedArray = UserDefaults.standard.object(forKey: FUGU_DEFAULT_CHANNELS) as? [Any]

        for channel in 0 ..< (chatCachedArray?.count ?? 0) {
            
            guard let channelObj = chatCachedArray![channel] as? [String: Any] else {
                return
            }
            
            guard let channelId = channelObj["channel_id"] as? Int else {
                return
            }
            
            if channelId == pushChannelId {
                
                var updatedChannelObj = channelObj
                updatedChannelObj["message"] = pushMessage
                
                if let unreadCount = pushInfo["unread_count"] as? Int {
                    updatedChannelObj["unread_count"] = unreadCount
                }
                
                if let dateTime = pushInfo["date_time"] as? String, dateTime.count > 0 {
                    updatedChannelObj["date_time"] = dateTime
                }
                
                chatCachedArray![channel] = updatedChannelObj
                UserDefaults.standard.set(chatCachedArray, forKey: FUGU_DEFAULT_CHANNELS)
                
                /*
                 showConversationsTableView.beginUpdates()
                 showConversationsTableView.moveRow(at: IndexPath(row: channel, section: 0), to: IndexPath(row: 0, section: 0))
                 
                 showConversationsTableView.endUpdates()
                 
                 conversationsArray.remove(at: channel)
                 conversationsArray.insert(updatedChannelObj, at: 0)
                 */
                
                showConversationsTableView.reloadRows(at: [IndexPath(row: channel, section: 0)], with: .none)
                
                if moveToChatController {
                    moveToChatViewController(chatObj: updatedChannelObj, isNew: false, selectedCell: channel)
                }
                break
            }
            
        }
    }

}

extension ShowAllConersationsViewController: UITableViewDelegate, UITableViewDataSource {
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let chatCachedArray = UserDefaults.standard.object(forKey: FUGU_DEFAULT_CHANNELS) as? [Any] {
            return chatCachedArray.count
        }
        
        return 0
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ConversationView", for: indexPath) as! ConversationView

        cell.headingLabel.text = ""
        cell.chatTextLabel.text = ""
        cell.timeLabel.text = ""

        cell.channelImageView.image = nil
        cell.channelImageView.layer.masksToBounds = true
        cell.channelImageView.layer.cornerRadius = 15.0
        
        cell.placeHolderImageButton.isHidden = true
        cell.placeHolderImageButton.setImage(nil, for: .normal)
        cell.placeHolderImageButton.layer.cornerRadius = 0.0
        cell.placeHolderImageButton.backgroundColor = .white
        cell.placeHolderImageButton.setTitle("", for: .normal)
        cell.unreadCountLabel.layer.masksToBounds = true
        
        cell.unreadCountLabel.text = ""
        cell.unreadCountLabel.backgroundColor = .clear

        cell.timeLabel.textColor = UIColor.black.withAlphaComponent(0.37)
        
        if let chatCachedArray = UserDefaults.standard.object(forKey: FUGU_DEFAULT_CHANNELS) as? [Any] {

            if !chatCachedArray.isEmpty {
                
                if let conersationObj = chatCachedArray[indexPath.row] as? [String: Any] {
                    
                    if let channelName = conersationObj["label"] as? String, channelName.count > 0 {
                        cell.headingLabel.text = channelName
                    }
                    
                    if let unreadCount = conersationObj["unread_count"] as? Int, unreadCount > 0 {
                        
                        cell.unreadCountLabel.text = "\(unreadCount)"
                        if FuguConfig.shared.themColor != nil {
                            cell.unreadCountLabel.backgroundColor = FuguConfig.shared.themColor
                            cell.timeLabel.textColor = FuguConfig.shared.themColor
                        } else {
                            cell.unreadCountLabel.backgroundColor = .lightGray
                        }
                        
                        cell.unreadCountLabel.layer.cornerRadius = 9.5
                        
                    }
                    
                    if let channelImage = conersationObj["channel_image"] as? String, channelImage.count > 0 {
                        
                        cell.channelImageView.image = nil
                        
                        if let url = NSURL(string: channelImage) {
                            cell.channelImageView.image = self.returnImageUsingCacheWithURLString(url: url, index: indexPath)
                        }
                        
                    } else if let channelName = conersationObj["label"] as? String, channelName.count > 0 {
                        
                        cell.placeHolderImageButton.isHidden = false
                        cell.placeHolderImageButton.setImage(nil, for: .normal)
                        cell.placeHolderImageButton.backgroundColor = .lightGray
                        var channelNameInitials = channelName
                        cell.placeHolderImageButton.setTitle(String(channelNameInitials.remove(at: channelNameInitials.startIndex)).capitalized, for: .normal)
                        cell.placeHolderImageButton.layer.cornerRadius = 15.0
                        
                    }
                    
                    if let message = conersationObj["message"] as? String, message.count > 0 {
                        cell.chatTextLabel.text = message
                    } else {
                        cell.chatTextLabel.text = "Attachment: Image"
                    }
                    
                    if let channelId = conersationObj["channel_id"] as? Int, channelId == -1 {
                        cell.timeLabel.text = ""
                    } else if let dateTime = conersationObj["date_time"] as? String {
                        cell.timeLabel.text = dateTime.toDate?.toString
                    }
                    
                }
                
            }
            
        }
        return cell
    }

    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: true)

        guard
            let chatCachedArray = UserDefaults.standard.object(forKey: FUGU_DEFAULT_CHANNELS) as? [Any], !chatCachedArray.isEmpty
            else {
             return
        }
        
        if let conversationObj = chatCachedArray[indexPath.row] as? [String: Any] {
            moveToChatViewController(chatObj: conversationObj, isNew: false, selectedCell: indexPath.row)
        }

    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        tableView.isScrollEnabled = true
        
        guard
            let chatCachedArray = UserDefaults.standard.object(forKey: FUGU_DEFAULT_CHANNELS) as? [Any], !chatCachedArray.isEmpty
            else {
                tableView.isScrollEnabled = false
                return tableView.frame.height
        }
        
        return 0
    }
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UIView()
        footerView.frame = CGRect(x: 0.0, y: 0.0, width: tableView.frame.size.width, height: tableView.frame.size.height)
        
        let footerLabel:UILabel = UILabel(frame: CGRect(x: 0, y: (tableView.frame.height / 2) - 90, width: tableView.frame.width, height: 90))
        footerLabel.textAlignment = NSTextAlignment.center
        footerLabel.textColor =  UIColor(red: 89.0/255.0, green: 89.0/255.0, blue: 104.0/255.0, alpha: 1.0)
        footerLabel.numberOfLines = 0
        footerLabel.font = UIFont.systemFont(ofSize: 16.0)
        
        footerLabel.text = tableViewDefaultText

        footerView.addSubview(footerLabel)
        
        let emptyAction = UITapGestureRecognizer(target: self, action: #selector(headerEmptyAction(_:)))
        footerView.addGestureRecognizer(emptyAction)
        
        return footerView
        
    }
    
}

class ConversationView : UITableViewCell {

    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var chatTextLabel: UILabel!
    @IBOutlet var headingLabel: UILabel!
    @IBOutlet var placeHolderImageButton: UIButton!
    @IBOutlet var bgView: UIView!
    @IBOutlet var channelImageView: UIImageView!
    @IBOutlet var unreadCountLabel: UILabel!
    @IBOutlet var unreadCountWidthLabel: NSLayoutConstraint!

}

