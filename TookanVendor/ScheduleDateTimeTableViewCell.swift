//
//  ScheduleDateTimeTableViewCell.swift
//  TookanVendor
//
//  Created by Harshit Parikh on 23/10/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

protocol ScheduleDateTimeDelegate: class {
    func dateTimeSelected()
}

class ScheduleDateTimeTableViewCell: UITableViewCell {
    
    weak var delegate: ScheduleDateTimeDelegate?
    
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var selectedDateTime: UILabel!

    @IBOutlet weak var dateTimeBtn: UIButton!
    
    @IBAction func dateTimeBtnAction(_ sender: Any) {
        if dateTimeBtn.isSelected == true {
            dateTimeBtn.isSelected = false
            BQBookingModel.shared.isScheduled = 0
            dateTimeBtn.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
            selectedDateTime.text = "Schedule as per your convinience"
        } else {
            delegate?.dateTimeSelected()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backGroundView.backgroundColor = .clear
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
