//
//  FavouriteViewController.swift
//  TookanVendor
//
//  Created by Harshit Parikh on 13/09/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class FavouriteViewController: UIViewController {

    var isPullToRefreshActive: Bool = false
    var refreshControl = UIRefreshControl()
    fileprivate var skip: Int = 0
    fileprivate var limit: Int = 10
    var isPaginationRequired: Bool = false
    var products = [Products]()
    fileprivate var itemCellView: ProductCellView?
    fileprivate var itemSelectedIndex: Int = -1
    
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noFavFoundLbl: UILabel!
    @IBOutlet weak var cartBottomView: UIView!
    @IBOutlet weak var nextBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noFavFoundLbl.isHidden = true
        self.setNavigationBar()
        self.setupTableView()
        self.addPulltoRefresh()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getAllFavouriteProducts()
    }
    
    @IBAction func nextBtnAction(_ sender: Any) {
        gotoCart()
    }
    
    private func setNavigationBar() {
        //Use for showing two right bar buttons on navigation bar
        
        let navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "My Favourites", leftButtonImage: #imageLiteral(resourceName: "menu"), rightButtonImage: nil, secondRightButtonImage: nil)
            , leftButtonAction: {[weak self] in
                self?.presentLeftMenuViewController()
            }, rightButtonAction: nil,
               rightButtonSecondAction: nil
        )
        navigationBar.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
        
    }
    

    private func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "ProductCell", bundle: nil), forCellReuseIdentifier: "ProductCell")
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 200
        
    }
    private func addPulltoRefresh() {
        //Add pullto refresh
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = .white
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    @objc func pullToRefresh() {
        isPullToRefreshActive = true
        self.products.removeAll()
        self.reset()
        self.getAllFavouriteProducts()
        
    }
    
    func reset() {
        skip = 0
        limit = 10
    }
    
    func paginationHit() {
        if self.isPaginationRequired == true {
            skip += 10
            self.getAllFavouriteProducts()
        }
    }
}

extension FavouriteViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.products.count > 0 {
            self.noFavFoundLbl.isHidden = true
            return self.products.count
        } else {
            self.noFavFoundLbl.isHidden = false
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as? ProductCell else {
            return UITableViewCell()
        }
        
        let cellDataModel: Products = self.products[indexPath.row]
        cell.leftProductDescription.isHidden = true
        cell.productDescription.isHidden = false
        cell.favBtn.isHidden = false
        cell.delegate = self
        cell.contentView.backgroundColor = .clear
        cell.stepperView.isHidden = false
        cell.deleteButton.isHidden = true
        cell.discountImage.isHidden = true
        cell.setupDataForSubCategoryProducts(data: cellDataModel)
        
        return cell
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if (tableView.contentOffset.y + tableView.frame.size.height) >= tableView.contentSize.height - 20 {
            self.paginationHit()
        }
    }
    
}

extension FavouriteViewController: ProductCellDelegate {
    func productCellAddButtonTapped(cell: ProductCell) {
        if let index = tableView.indexPath(for: cell) {
            self.updateView(view: cell, index: index.row, add: true, dobString: "")
        }
    }
    
    func productCellDeleteButtonTapped(cell: ProductCell) {
        if let index = tableView.indexPath(for: cell) {
            self.updateView(view: cell, index: index.row, add: false, dobString: "")
        }
    }
    
    func productCellRemoveCategoryButtonTapped(cell: ProductCell) {
        
    }
    
    func productCellFavButtonTapped(cell: ProductCell, isfavStatus: Bool) {
        if let index = tableView.indexPath(for: cell) {
            self.markFavUnfavProduct(view: cell, index: index.row)
        }
    }
}

extension FavouriteViewController: BQSelectDateTimeVCDelegate {
    func updateView(view: ProductCellView, index: Int, add: Bool, dobString: String = "") {
        if add {
            addProductToCart(product: products[index],view: view, index: index, dobString: dobString)
        } else {
            deleteProductFromCart(product: products[index],view: view, index: index)
        }
    }
    
    
    func addProductToCart(product: Products, view: ProductCellView, index: Int, dobString: String = "") {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        product.addProduct(dobString: dobString) { (success, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if success {
                view.updateValue(added: true)
                if let count = self.products[index].inCartCount {
                    self.products[index].inCartCount = count + 1
                }
                
                if let totalPrice = response!["total_price"] as? Double, let cartCount = response!["cart_count"] as? Int {
                    print(totalPrice, cartCount)
                    self.refreshCell(at: index, isAdded: true, cartCount: cartCount, totalPrice: Float(totalPrice))
                }
            } else {
                if let data = response!["message"] as? String {
                    self.selectDOB()
                }
            }
            
        }
    }
    
    func deleteProductFromCart(product: Products, view: ProductCellView, index: Int) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        product.deleteProduct { (success, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if success {
                view.updateValue(added: false)
                if let count = self.products[index].inCartCount {
                    self.products[index].inCartCount = count - 1
                }
                if let totalPrice = response!["total_price"] as? Double, let cartCount = response!["cart_count"] as? Int {
                    print(totalPrice, cartCount)
                    self.refreshCell(at: index, isAdded: false, cartCount: cartCount, totalPrice: Float(totalPrice))
                }
            }
        }
    }
    
    func refreshCell(at index: Int, isAdded: Bool, cartCount: Int, totalPrice: Float) {
        
        
        if cartCount > 0 {
            nextBtn.isUserInteractionEnabled = true
            cartBottomView.isHidden = false
        } else {
            nextBtn.isUserInteractionEnabled = false
            cartBottomView.isHidden = false
        }
        
        self.lblTotalAmount.text = String(format: "%.2f", totalPrice)
        //String(totalPrice)
        //        if subCategoryTableView != nil {
        //            subCategoryTableView.reloadData()
        //        }
        
        //        let indexPath = IndexPath(row: index, section: 0)
        //        subCategoryTableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func selectDOB() {
        let selectDateVC = BQSelectDateTimeVC(nibName: "BQSelectDateTimeVC", bundle: nil)
        selectDateVC.modalPresentationStyle = .overCurrentContext
        if let vc = UIApplication.shared.keyWindow?.topMostWindowController {
            selectDateVC.delegate = self
            selectDateVC.isFromAddItem = true
            selectDateVC.messageStr = "You must be 21 years above to add this product."
            self.present(selectDateVC, animated: true, completion: nil)
        }
    }
    
    func bookingDate(date: String) {
        if self.itemSelectedIndex > -1 {
            if let view = self.itemCellView {
                self.updateView(view: view, index: self.itemSelectedIndex, add: true, dobString: date)
                print("Your age is greater than 21", date)
            }
        }
    }
    
    func showError(message: String) {
        Singleton.sharedInstance.showAlert(message)
    }
    
    fileprivate func gotoCart() {
        
        if self.cartBottomView.isHidden {
            if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQNoItemsInCartViewVC") as? BQNoItemsInCartViewVC {
                self.navigationController?.present(vc, animated: true, completion: nil)
            }
        } else {
            if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartSegmentViewController") as? CartSegmentViewController {
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
    }
}

extension FavouriteViewController {
    
    func markFavUnfavProduct(view: ProductCellView, index: Int) {
        let product = self.products[index]
        ActivityIndicator.sharedInstance.showActivityIndicator()
        product.markFavUnfavApi { (success, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if success {
                if let status = self.products[index].is_fav, status == 1 {
                    self.products[index].is_fav = 0
                } else {
                    self.products[index].is_fav = 1
                }
                self.getAllFavouriteProducts()
            }
        }
    }
    
    func getAllFavouriteProducts() {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        self.getProducts(limit: limit, skip: skip) { (success, products, paginationRequired) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if success {
                    guard let _products = products else {
                        return
                    }
                    self.isPaginationRequired = paginationRequired
                    if !paginationRequired {
                        self.products = _products
                    } else {
                        self.products += _products
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func getProducts(limit: Int = 10, skip: Int = 0, _ receivedResponse: @escaping (_ succeeded: Bool, _ response:[Products]?, _ paginationRequired: Bool) -> ()){
        guard let lat = BQBookingModel.shared.latitude else {
            return
        }
        guard let long = BQBookingModel.shared.longitude else {
            return
        }
        let params: [String : Any] = [
            "user_type":0,
            "limit": limit,
            "skip": skip,
            "access_token": Vendor.current!.appAccessToken!,
            "lattitude": lat,
            "longitude": long,
            "is_fav": 1]
        
        print("Get products param: ", params)
        
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getProducts,
                                                         params: params as [String : AnyObject],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                           
                                if self.refreshControl.isRefreshing {
                                    self.refreshControl.endRefreshing()
                                }
                                                        
                                                            DispatchQueue.main.async {
                                                                if succeeded {
                                                                    guard let data = response["data"] as? [String: Any], let products = data["data"] as? [[String: Any]] else {
                                                                        receivedResponse(false,nil, false)
                                                                        return
                                                                    }
                                                                    guard let productCount = data["iTotalRecords"] as? Int else {
                                                                        return
                                                                    }
                                                                    guard let totalPrice = data["total_price"] as? Double else {
                                                                        return
                                                                    }
                                                                    self.lblTotalAmount.text = String(format: "%.2f", totalPrice)
                                                                    
                                                                    guard let cartCount = data["cartCount"] as? Double else {
                                                                        return
                                                                    }
                                                                    if cartCount > 0 {
                                                                        self.nextBtn.isUserInteractionEnabled = true
                                                                        self.cartBottomView.isHidden = false
                                                                    } else {
                                                                        self.nextBtn.isUserInteractionEnabled = false
                                                                        self.cartBottomView.isHidden = false
                                                                    }
                                                                    
                                                                    let stringJson = products.jsonString
                                                                    let productsData = stringJson.data(using: .utf8)
                                                                    do {
                                                                        let productsArray = try JSONDecoder().decode([Products].self, from: productsData!)
                                                                        self.isPaginationRequired =  productCount > productsArray.count
                                                                        receivedResponse(true, productsArray,self.isPaginationRequired)
                                                                        
                                                                    } catch {
                                                                        print(error.localizedDescription)
                                                                        receivedResponse(false,nil,false)
                                                                    }
                                                                } else {
                                                                    receivedResponse(false,nil,false)
                                                                }
                                                            }
                                                            
        }
    }
}

