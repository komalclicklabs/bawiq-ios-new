//
//  UIButton+Extension.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 12/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit


extension UIButton {
  
    func setButtonShadow() {
        self.layer.shadowOffset = CGSize(width: 0, height: 5)
        self.layer.shadowOpacity = 0.1
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1).cgColor
        self.layer.shadowRadius = 5
    }
  // MARK: - Config
  func configureSmallButtonWith(title: String) {
    setTitle(title, for: .normal)
    titleLabel?.font = UIFont(name: FONT.regular, size: 15.0)
    setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: UIControlState.normal)
  }
  
  func configureNormalButtonWith(title: String) {
    setTitle(title, for: .normal)
    titleLabel?.font = UIFont(name: FONT.regular, size: 17.0)
    setTitleColor(COLOR.LOGIN_BUTTON_TITLE_COLOR, for: .normal)
    backgroundColor = COLOR.THEME_FOREGROUND_COLOR
    layer.cornerRadius = 2
    setSameColorShadow()
  }
  
  func configureNormalButtonWithOppositeColorThemeWith(title: String) {
    configureNormalButtonWith(title: title)
    
    setTitleColor(COLOR.SIGNIN_TITLE_TYPE_LABEL, for: .normal)
    backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
    layer.borderColor = COLOR.SIGNIN_TITLE_TYPE_LABEL.cgColor
    setSameColorShadow()
    layer.borderWidth = 1
  }
    
  func setGradientColorFrom(_ colors: [UIColor]? =  [UIColor.ThemeGradients.StartGradient, UIColor.ThemeGradients.EndGradient]) {
        
        if colors != nil {
            self.setBackgroundImage(UIImage.getHorizontalGradientImage(colors!, size: self.bounds.size), for: .normal)
        } else {
            self.setBackgroundImage(UIImage.getHorizontalGradientImage([UIColor.ThemeGradients.StartGradient, UIColor.ThemeGradients.EndGradient], size: self.bounds.size), for: .normal)
        }
    }
    
   func setGradientColorForHighlightedState(_ colors: [UIColor]? =  [UIColor.ThemeGradients.StartGradient, UIColor.ThemeGradients.EndGradient]) {
        if colors != nil {
            self.setBackgroundImage(UIImage.getHorizontalGradientImage(colors!, size: self.bounds.size), for: .highlighted)
        } else {
            self.setBackgroundImage(UIImage.getHorizontalGradientImage([UIColor.ThemeGradients.StartGradient, UIColor.ThemeGradients.EndGradient], size: self.bounds.size), for: .highlighted)
        }
    }
    
}


import UIKit
import Foundation

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    
    public class var AddressTappableColor: UIColor
    {
        return UIColor.init(netHex: 0xEEEEEE)
    }
    
    
    struct ThemeGradients {
        static let StartGradient = UIColor(red: 232 / 255.0, green: 93 / 255.0, blue: 123 / 255.0, alpha: 1.0)
        static let EndGradient = UIColor(red: 239 / 255.0, green: 166 / 255.0, blue: 69 / 255.0, alpha: 1.0)
    }
    
    struct MainAppColors {
        static let TextBlackColor = UIColor(red: 89 / 255.0, green: 89 / 255.0, blue: 104 / 255.0, alpha: 1.0)
        static let OrangeColor = UIColor(red: 253 / 255.0, green: 121 / 255.0, blue: 69 / 255.0, alpha: 1.0)
    }
    
    struct Offering {
        static let rides = UIColor(red: 253 / 255.0, green: 121 / 255.0, blue: 69 / 255.0, alpha: 1.0)
        static let meals = UIColor.init(red: 241, green: 119, blue: 159)
        static let fatafat = UIColor.init(red: 114, green: 197, blue: 89)
        static let menus = UIColor.init(red: 179, green: 104, blue: 169)
    }
    
}

