//
//  ErrorView.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 18/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

protocol ErrorDelegate {
  func removeErrorView()
}

class ErrorView: UIView {
  
  @IBOutlet var statusIcon: UIImageView!
  @IBOutlet var errorMessage: UILabel!
  
  var delegate:ErrorDelegate!
  var removingFromSuperView: (() -> Void)?
  var keyboardChange: KeyBoard?
  
  override func awakeFromNib() {
    
    errorMessage.textColor = UIColor.white
    errorMessage.font = UIFont(name: FONT.light, size: FONT_SIZE.small)
    
//    self.statusIcon.image = #imageLiteral(resourceName: "iconCancelChangesProfile").withRenderingMode(.alwaysTemplate)
    self.statusIcon.tintColor = UIColor.white
    let tap = UITapGestureRecognizer(target: self, action: #selector(self.hideErrorMessage))
    tap.numberOfTapsRequired = 1
    self.addGestureRecognizer(tap)
    
    keyboardChange = KeyBoard { [weak self] newHeight in
      self?.frame.origin.y = SCREEN_SIZE.height - newHeight - (self?.frame.height ?? HEIGHT.errorMessageHeight)
    }
  }
  

  
  func setErrorMessage(message:String, isError:Bool) {
    if isError == true {
      self.backgroundColor = COLOR.ERROR_COLOR
    } else {
      self.backgroundColor = COLOR.SUCCESS_COLOR
    }
    self.errorMessage.text = message
    print(UIFont(name: FONT.light, size: FONT_SIZE.small))
    let size = message.heightWithConstrainedWidth(width: SCREEN_SIZE.width - 57, font: UIFont(name: FONT.light, size: FONT_SIZE.small)!)
    if size.height > 14 {
      self.frame.size.height = (HEIGHT.errorMessageHeight - 13) + size.height
    }
    
    self.showErrorMessage()
  }
  
  func showErrorMessage() {
    UIView.animate(withDuration: 0.3, animations: {
      self.transform = CGAffineTransform(translationX: 0, y: -self.frame.height)
    }, completion: { finished in
      self.perform(#selector(self.hideErrorMessage), with: nil, afterDelay: 3.0)
    })
  }
  
  func hideErrorMessage() {
    NSObject.cancelPreviousPerformRequests(withTarget: self)
    UIView.animate(withDuration: 0.5, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
      self.transform = CGAffineTransform.identity
    }, completion: { [weak self] finished in
      self?.delegate?.removeErrorView()
      self?.removingFromSuperView?()
      self?.removeFromSuperview()
    })
  }
  
  // MARK: - Loading
  static func showWith(message: String, isErrorMessage: Bool = true, removed: (() -> Void)?) {
    let keyWindow = getKeyWindow()
    
    checkAndRemoveErrorViewIfAlreadyPresent()
    
    let errorMessageView = UINib(nibName: NIB_NAME.errorView, bundle: frameworkBundle).instantiate(withOwner: self, options: nil)[0] as! ErrorView
    errorMessageView.frame = CGRect(x: 0, y: SCREEN_SIZE.height, width: SCREEN_SIZE.width, height: HEIGHT.errorMessageHeight)
    keyWindow?.addSubview(errorMessageView)
    
    errorMessageView.setErrorMessage(message: message,isError: isErrorMessage)
  }
  
  private static func checkAndRemoveErrorViewIfAlreadyPresent() {
    let keyWindow = getKeyWindow()
   
   for tempSubview in keyWindow!.subviews.reversed() {
      if let subView = tempSubview as? ErrorView {
         subView.removeFromSuperview()
         return
      }
   }
   
  }
  
  private static func getKeyWindow() -> UIView? {
    return UIApplication.shared.keyWindow
  }
  
  
  
}

//class KeyBoard {
//  
//  static var height: CGFloat = 0
//  static var width: CGFloat = 0
//  static var isKeyBoardPresent: Bool {
//    return height > 0
//  }
//  
//  var heightChanged: ((CGFloat) -> Void)?
//  
//  init(heightChanged:  ((CGFloat) -> Void)?) {
//    self.heightChanged = heightChanged
//
//    NotificationCenter.default.addObserver(self, selector: #selector(KeyBoard.keyBoardWillShow(_:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
//    NotificationCenter.default.addObserver(self, selector: #selector(KeyBoard.keyBoardWillHide(_:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
//   NotificationCenter.default.addObserver(self, selector: #selector(keyBoardDidChangeHeight(_:)), name: Notification.Name.UIKeyboardDidChangeFrame, object: nil)
//  }
//  
//  @objc private func keyBoardWillShow(_ notification: Notification) {
//    setHeightWidthFrom(notification: notification)
//  }
//  
//  @objc private func keyBoardWillHide(_ notification: Notification) {
//    KeyBoard.height = 0
//    KeyBoard.width = 0
//    heightChanged?(0)
//  }
//   
//   @objc private func keyBoardDidChangeHeight(_ notification: Notification) {
//      guard let keyboardFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect, KeyBoard.height != 0 else {
//         return
//      }
//      KeyBoard.height = keyboardFrame.height
//      KeyBoard.width = keyboardFrame.width
//      heightChanged?(KeyBoard.height)
//   }
//   
//   private func setHeightWidthFrom(notification: Notification) {
//      let keyboardFrame = notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? CGRect
//      KeyBoard.height = keyboardFrame?.height ?? 0
//      KeyBoard.width = keyboardFrame?.width ?? 0
//      heightChanged?(KeyBoard.height)
//   }
//
//  
//}




class KeyBoard {

   static var height: CGFloat = 0
   static var width: CGFloat = 0
   static var isKeyBoardPresent: Bool {
      return height > 0
   }

   var heightChanged: ((CGFloat) -> Void)?

   init(heightChanged:  ((CGFloat) -> Void)?) {
      self.heightChanged = heightChanged

      NotificationCenter.default.addObserver(self, selector: #selector(KeyBoard.keyBoardWillShow(_:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(KeyBoard.keyBoardWillHide(_:)), name: Notification.Name.UIKeyboardWillHide, object: nil)
      NotificationCenter.default.addObserver(self, selector: #selector(keyBoardDidChangeHeight(_:)), name: Notification.Name.UIKeyboardDidChangeFrame, object: nil)
   }

   @objc private func keyBoardWillShow(_ notification: Notification) {
      setHeightWidthFrom(notification: notification)
   }

   @objc private func keyBoardWillHide(_ notification: Notification) {
//      KeyBoard.height = 0
//      KeyBoard.width = 0
//      heightChanged?(0)
   }

   @objc private func keyBoardDidChangeHeight(_ notification: Notification) {
      guard let keyboardFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect, KeyBoard.height != 0 else {
         return
      }
      KeyBoard.height = keyboardFrame.height
      KeyBoard.width = keyboardFrame.width
      heightChanged?(KeyBoard.height)
   }

   private func setHeightWidthFrom(notification: Notification) {
      let keyboardFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? CGRect
      KeyBoard.height = keyboardFrame?.height ?? 0
      KeyBoard.width = keyboardFrame?.width ?? 0
      heightChanged?(KeyBoard.height)
   }


}

