//
//  CardsModel.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 02/03/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation


class cardsModel{
    
    var id = String()
    var brand = String()
    var expiryDate = String()
    var lastFourDigit = String()
    var funding = String()

    
    init(json:[String:Any]) {
        if let value = json["card_id"] as? String {
            self.id = value
        }
        
        if let value = json["brand"] as? String {
            self.brand = value
        }
        
        if let value = json["expiry_date"] as? String {
            self.expiryDate = value
        }
        
        if let value = json["last4_digits"] as? Int {
            self.lastFourDigit = "\(value)"
        }else if let value = json["last4_digits"] as? String {
            self.lastFourDigit  = value
        }
        
        if let value = json["funding"] as? String {
            self.funding = value
        }
        
    }
    
}
