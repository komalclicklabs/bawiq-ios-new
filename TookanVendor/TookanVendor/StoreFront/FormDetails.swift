//
//  FormDetails.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 03/12/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

enum Vertical: String {
   case pdaf = "0" // Pickup, Delivery, Appointment, FOS
   case taxi = "1"
   case multipleCategory = "2"
}


public  class FormDetails: NSObject {

   // MARK: - Properties
    var auto_assign:Int?
    var color:String?
    var domain_name:String?
    var font:String?
    var form_id:String?
    var logo = String()
    var pickup_delivery_flag:Int?
    var status:Int?
    var user_id:Int?
    var work_flow:Int?
    static var currentForm = FormDetails(json: [String : Any]())
    static var allFormDetails = [FormDetails]()
    var call_tasks_as:String = "Task"
    var callServiceProvidersAs = "Agent"
    var payementMethod = Int()
    var toShowPayment = false
    var forcePickupDelivery = 0
    var verticalType = Vertical.pdaf
    var waiting_screen_time = Int()
    var isDestinationRequired = String()
    var cancelDict = [String: Any]()
    var paymentMethodArray = [Int]()
    var isRatingRequired  = "0"
    var isSchedulingEnabled = "0"
    var scheduleOffsetTime = Int()
    var currencyid = String()
    var formName = String()
    var showPrefilledDataType = "0"
    var isNLevel = false
    var nLevelController: NLevelFlowManager?
    var showServiceProvider = false
    var tipDetails: Tip
    var apiKey = String()
    var minimumOrderValue: Double = 140.0
   
   // MARK: - Intializers

    init(json: [String: Any]) {
      print("in json")
        
        if let value = json["show_prefilled_data"] as? String{
            self.showPrefilledDataType  = value
        }else if let value = json["show_prefilled_data"] as? Int{
            self.showPrefilledDataType = "\(value)"
        }
        
        if let value = json["form_name"] as? String{
            self.formName = value
        }
      
        if let value = json["logo"] as? String{
            self.logo = value
        }
      
        if let value = json["auto_assign"] as? String {
            self.auto_assign = Int(value)
        } else if let value = json["auto_assign"] as? Int {
            self.auto_assign = value
        } else {
            self.auto_assign = 0
        }
   
        if let value = json["domain_name"] as? String {
            self.domain_name = value
        } else {
            self.domain_name = ""
        }
        
        if let value = json["pickup_delivery_flag"] as? String {
            self.pickup_delivery_flag = Int(value)
        } else if let value = json["pickup_delivery_flag"] as? Int {
            self.pickup_delivery_flag = value
        } else {
            self.pickup_delivery_flag = 0
        }
      
      tipDetails = Tip(json: json)
      isNLevel =  (json["is_nlevel"] as? Bool) ?? false
        
        if let value = json["work_flow"] as? String {
            self.work_flow = Int(value)!
        } else if let value = json["work_flow"] as? Int {
            self.work_flow = value
        } else {
            self.work_flow = 0
        }
        
        if let value = json["call_tasks_as"] as? String {
            self.call_tasks_as = value
        }
      
      showServiceProvider = (json["show_service_providers"] as? Bool) ?? false
        
        if let value = json["payment_methods"] as? [Any]{
            self.paymentMethodArray = []
            for i in value{
                print(i)
                if let paymentData = i as? [String:Any]{
                    if let isEnabled = paymentData["enabled"] as? Int{
                        if isEnabled == 1{
                            toShowPayment = true
                            if let value = paymentData["value"] as? Int{
                                self.payementMethod = value
                                self.paymentMethodArray.append(value)
                            }
                        }else{
                            toShowPayment = false
                        }
                    }
                }
                if paymentMethodArray.count > 0{
                    self.payementMethod = paymentMethodArray[0]
                }
            }
            
            if self.paymentMethodArray.count > 0{
                self.toShowPayment   = true
            }else{
                self.toShowPayment = false
            }
        }
      
        if let value = json["force_pickup_delivery"] as? Int{
            forcePickupDelivery = value
        }else if let value = json["force_pickup_delivery"] as? String{
            if let _ = Int(value){
                forcePickupDelivery = Int(value)!
            }
        }
        
        if let value = json["user_id"] as? Int{
            self.user_id = value
        }else if let value = json["user_id"] as? String{
            self.user_id = Int(value)
        }
        
        if let rawVertical = json["vertical"] {
            self.verticalType = Vertical(rawValue: "\(rawVertical)")!
        }
        
        if let value = json["waiting_screen_time"] as? String{
            self.waiting_screen_time = Int(value)!
        }else if let value = json["waiting_screen_time"] as? Int{
            self.waiting_screen_time = value
        }
      
        if let value = json["is_destination_required"] as? String{
            self.isDestinationRequired = value
        }else if let value = json["is_destination_required"] as? Int{
            self.isDestinationRequired = "\(value)"
        }
        
        if let value = json["cancel_config"] as? [Any]{
            self.cancelDict = [String:String]()
            for i in value {
                if let data = i as? [String:Any]{
                    cancelDict[returnData(key: "job_status", fromObject: data)] = returnData(key: "is_cancel", fromObject: data)
                }
            }
            
        }
      
        if let value = json["is_rating_required"] as? String{
            self.isRatingRequired = value
        }else if let value = json["is_rating_required"] as? Int {
            self.isRatingRequired = "\(value)"
        }
        
        if let value = json["form_id"] as? String{
            self.form_id = value
        }else if let value = json["form_id"] as? Int{
            self.form_id  = "\(value)"
        }
      
        if let value = json["is_scheduling_enabled"] as? String{
            self.isSchedulingEnabled = value
        }else if let value = json["is_scheduling_enabled"] as? Int{
            self.isSchedulingEnabled = "\(value)"
        }
      
        if let value = json["schedule_offset_time"] as? String{
            self.scheduleOffsetTime = Int(value)!
        } else if let value = json["schedule_offset_time"] as? Int{
            self.scheduleOffsetTime = value 
        }
//        
        if let value = json["payment_settings"] as? [Any]{
            print(value)
            
            for i in value{
                if let data = i as? [String:Any]{
                    if let symbol = data["symbol"] as? String{
                        self.currencyid = symbol
                    }
                }
            }
        }
    }
   
   // MARK: - Methods
    func getPrefiledData() -> ConstantFieldHandlingType{
        switch self.showPrefilledDataType {
        case "0":
            return .empty
        case "1":
            return .showPrefiled
        case "2":
            return .hidePrefiled
        default:
            return .empty
        }
    }
   
   func hasPaymentMethodOtherThanCash() -> Bool {
      if paymentMethodArray.count == 1 &&  hasCashAsAPaymentMethod() {
         return false
      }
      return true
   }
   
   func hasCashAsAPaymentMethod() -> Bool {
      return paymentMethodArray.contains(8)
   }
    
}



func returnData(key: String, fromObject: [String:Any]) -> String {
    if let value = fromObject[key] as? String{
        return value
    }else if let value = fromObject[key] as? Int{
        return "\(value)"
    }else{
        return ""
    }
}
