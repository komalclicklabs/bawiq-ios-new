//
//  UIImage+Extension.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 22/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

extension UIImage {
  func renderWithAlwaysTemplateMode() -> UIImage {
    return self.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
  }
    
    
    
    class func getHorizontalGradientImage(_ fromColors: [UIColor], size : CGSize) -> UIImage? {
        
        var gradientColors = [CGColor]()
        for value in fromColors {
            gradientColors.append(value.cgColor)
        }
        if gradientColors.count == 0 {
            gradientColors.append(UIColor.clear.cgColor)
        }
        
        let gradientLayer = CAGradientLayer()
        var actualSize = size
        if size.width == 0 || size.height == 0 {
            actualSize = CGSize(width: UIScreen.main.bounds.width, height: 60)
        }
        gradientLayer.frame = CGRect(x: 0, y: 0, width: actualSize.width, height: actualSize.height)
        gradientLayer.colors = gradientColors
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        UIGraphicsBeginImageContext(actualSize)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        return image
        
    }
    
    class func getVerticalGradientImage(_ fromColors: [UIColor], size : CGSize) -> UIImage? {
        
        var gradientColors = [CGColor]()
        for value in fromColors {
            gradientColors.append(value.cgColor)
        }
        if gradientColors.count == 0 {
            gradientColors.append(UIColor.clear.cgColor)
        }
        
        let gradientLayer = CAGradientLayer()
        var actualSize = size
        if size.width == 0 || size.height == 0 {
            actualSize = CGSize(width: UIScreen.main.bounds.width, height: 60)
        }
        gradientLayer.frame = CGRect(x: 0, y: 0, width: actualSize.width, height: actualSize.height)
        gradientLayer.colors = gradientColors
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        UIGraphicsBeginImageContext(actualSize)
        gradientLayer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        return image
        
    }
    
    func convertImageToBW() -> UIImage? {
        let width = self.size.width
        let height = self.size.height
        let rect = CGRect(x: 0.0, y: 0.0, width: width, height: height)
        let colorSpace = CGColorSpaceCreateDeviceGray()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue)
        
        guard let context = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue) else {
            return nil
        }
        guard let cgImage = cgImage else { return nil }
        
        context.draw(cgImage, in: rect)
        guard let imageRef = context.makeImage() else { return nil }
        let newImage = UIImage(cgImage: imageRef.copy()!)
        
        return newImage
    }
    
    
}
