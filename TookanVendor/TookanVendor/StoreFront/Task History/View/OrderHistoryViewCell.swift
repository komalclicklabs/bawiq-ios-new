//
//  OrderHistoryViewCell.swift
//  TookanVendor
//
//  Created by CL-Macmini-110 on 10/4/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class OrderHistoryViewCell: UITableViewCell {
   @IBOutlet var leftImageView: UIImageView!
   @IBOutlet var statusLabel: UILabel!
   @IBOutlet var statusDetail: UILabel!
   @IBOutlet var idLabel: UILabel!
   @IBOutlet var idDetail: UILabel!
   @IBOutlet var pickupAddressLabel: UILabel!
   @IBOutlet var pickupAddressDetail: UILabel!
   @IBOutlet var deliveryLabel: UILabel!
   @IBOutlet var deliveryDetail: UILabel!
   @IBOutlet var rightRupeesLabel: UILabel!
   @IBOutlet var backView: UIView!
   @IBOutlet var appointmentLabel: UILabel!
   @IBOutlet var apponitmentDetail: UILabel!
   @IBOutlet var pickupView: UIView!
   @IBOutlet var deliveryView: UIView!
   @IBOutlet var AppointmentView: UIView!
   @IBOutlet var trailingOfStatusLabel: NSLayoutConstraint!
   @IBOutlet var trailingOfIDLabel: NSLayoutConstraint!
   
    
    let serverDateFormat = "yyyy-MM-dd'T'HH:mm:ss.000Z"
    let pickupServerDateFormat = "MM/dd/yyyy hh:mm a"
    let displayDateFormat = "dd, MMM yyyy, hh:mm a"
    
    override func awakeFromNib() {
      super.awakeFromNib()
      self.setIDLabel()
      self.setIDDetail()
      self.setLeftImage()
      self.setRupeeLabel()
      self.setPickupLabel()
      self.setPickupDetail()
      self.setStatusLabel()
      self.setStatusDetail()
      self.setDeliveryLabel()
      self.setDeliveryDetail()
      self.setBackgroundView()
      self.setAppointmentLabel()
      self.setApponitmentDetail()
    }

   //MARK: Setting Views
   func setBackgroundView()  {
      self.backView.backgroundColor = COLOR.shadowColor
      self.backView.layer.cornerRadius = 3.0
   }
   
   func setLeftImage() {
      self.leftImageView.image = #imageLiteral(resourceName: "greenTick")
   }
   
   func setStatusLabel() {
      self.statusLabel.text = ""//"\(TEXT.STATUS):"
      self.statusLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.small)
      self.statusLabel.textColor = UIColor.black
   }
   
   func setStatusDetail() {
      self.statusDetail.font = UIFont(name: FONT.light, size: FONT_SIZE.small)
      self.statusDetail.textColor = UIColor.black
   }
   
   func setIDLabel() {
      self.idLabel.text = "\(TEXT.ID_CAP):"
      self.idLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.small-1.0)
      self.idLabel.textColor = UIColor.black
   }
   
   func setIDDetail() {
      self.idDetail.font = UIFont(name: FONT.light, size: FONT_SIZE.small)
      self.idDetail.textColor = UIColor.black
   }
   
   func setPickupLabel() {
      self.pickupAddressLabel.text = "\(TEXT.orderDetails):"
      self.pickupAddressLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.small)
      self.pickupAddressLabel.textColor = UIColor.black
   }
   
   func setPickupDetail() {
      self.pickupAddressDetail.font = UIFont(name: FONT.light, size: FONT_SIZE.small)
      self.pickupAddressDetail.textColor = UIColor.black
   }
   
   func setDeliveryLabel() {
      self.deliveryLabel.text = "\(TEXT.DeliveryDetails):"
      self.deliveryLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.small)
      self.deliveryLabel.textColor = UIColor.black
   }
   
   func setDeliveryDetail() {
      self.deliveryDetail.font = UIFont(name: FONT.light, size: FONT_SIZE.small)
      self.deliveryDetail.textColor = UIColor.black
   }
   
   func setRupeeLabel() {
      self.rightRupeesLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.medium)
      self.rightRupeesLabel.textColor = UIColor.black
      self.rightRupeesLabel.isHidden = true
   }
   
   func setApponitmentDetail() {
      self.apponitmentDetail.font = UIFont(name: FONT.light, size: FONT_SIZE.small)
      self.apponitmentDetail.textColor = UIColor.black
   }
   
   func setAppointmentLabel() {
      self.appointmentLabel.text = TEXT.AppointmentDetails
      self.appointmentLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
      self.appointmentLabel.textColor = UIColor.black
   }
   
   //MARK: Setting Cell
   func setCellWith(order: OrderHistory) {
      
      guard order.hasTasks() else {
         print("No Task found in Order")
         return
      }
      
      if order.hasSingleTask() {
         setCellForSingle(task: order.tasksOfOrder[0])
      } else {
         setCellForMultiple(tasks: order.tasksOfOrder)
      }
      setStatuslabelFor(order: order)
      self.setIDLabel(task: order.tasksOfOrder[0])
   }
   
   //MARK: Setting Cell For Single Task
   private func setCellForSingle(task: OrderDetails) {
      self.setAllLabels(task: task)
   }
   
   //MARK: Setting Cell For Multiple Task
   private func setCellForMultiple(tasks: [OrderDetails]) {
      let firstTask = tasks[1]
      let secondtask = tasks[0]
      self.setAllLabels(task: firstTask)
      self.pickupAddressDetail.text = "\(firstTask.getDate())\n\(firstTask.getAddress())"
      self.deliveryDetail.text = "\(secondtask.getDate())\n\(secondtask.getAddress())"
   }
   
   func setAllLabels(task: OrderDetails) {
      switch task.job_type {
      case .appointment:
         self.pickupAddressLabel.isHidden = true
         self.pickupAddressDetail.isHidden = true
         self.deliveryDetail.isHidden = true
         self.deliveryLabel.isHidden = true
         self.apponitmentDetail.isHidden = false
         self.appointmentLabel.isHidden = false
         self.pickupView.isHidden = true
         self.deliveryView.isHidden = true
         self.AppointmentView.isHidden = false
         self.trailingOfIDLabel.constant = 77
         self.trailingOfStatusLabel.constant = 77
         self.apponitmentDetail.text = "\(task.taskHistory[0].creationDatetime.convertDateFromUTCtoLocal(input: serverDateFormat, output: displayDateFormat,toConvert:false))\n\(task.getAddress())"
      case .pickup:
         self.pickupAddressLabel.isHidden = false
         self.pickupAddressDetail.isHidden = false
         self.deliveryDetail.isHidden = true
         self.deliveryLabel.isHidden = true
         self.apponitmentDetail.isHidden = true
         self.appointmentLabel.isHidden = true
         self.pickupView.isHidden = false
         self.deliveryView.isHidden = true
         self.AppointmentView.isHidden = true
         self.trailingOfIDLabel.constant = 8
         self.trailingOfStatusLabel.constant = 8
         print(task.creationDateTime)
         self.pickupAddressDetail.text = "\(task.creationDateTime.convertDateFromUTCtoLocal(input: serverDateFormat, output: displayDateFormat,toConvert:false))\n\(task.getAddress())"
      case .delivery:
         self.pickupAddressLabel.isHidden = true
         self.pickupAddressDetail.isHidden = true
         self.deliveryDetail.isHidden = false
         self.deliveryLabel.isHidden = false
         self.apponitmentDetail.isHidden = true
         self.appointmentLabel.isHidden = true
         self.pickupView.isHidden = true
         self.deliveryView.isHidden = false
         self.AppointmentView.isHidden = true
         self.trailingOfIDLabel.constant = 8
         self.trailingOfStatusLabel.constant = 8
         self.deliveryDetail.text = "\(task.creationDateTime.convertDateFromUTCtoLocal(input: serverDateFormat, output: displayDateFormat,toConvert:false))\n\(task.getAddress())"
      case .both:
         self.pickupAddressLabel.isHidden = false
         self.pickupAddressDetail.isHidden = false
         self.deliveryDetail.isHidden = false
         self.deliveryLabel.isHidden = false
         self.apponitmentDetail.isHidden = true
         self.appointmentLabel.isHidden = true
         self.pickupView.isHidden = false
         self.deliveryView.isHidden = false
         self.AppointmentView.isHidden = true
         self.trailingOfIDLabel.constant = 8
         self.trailingOfStatusLabel.constant = 8
         self.pickupAddressDetail.text = "\(task.creationDateTime.convertDateFromUTCtoLocal(input: serverDateFormat, output: displayDateFormat,toConvert:false))\n\(task.getAddress())"
         self.deliveryDetail.text = "\(task.creationDateTime.convertDateFromUTCtoLocal(input: serverDateFormat, output: displayDateFormat,toConvert:false))\n\(task.getAddress())"
      default:
         break
      }
   }
   
   //MARK: Setting Status Label
   private func setStatuslabelFor(order: OrderHistory) {
     
      if order.hasSingleTask() {
         let (statustext, statusColor) = getStatusTextAndColorFrom(order: order)
         self.statusDetail.text = ""//statustext
         self.statusDetail.textColor = statusColor
      } else {
         let firstTask = order.tasksOfOrder[1]
         let secondTask = order.tasksOfOrder[0]
         
         if firstTask.job_status!.processingOftaskStopped() {
            let (text, color) = secondTask.job_status!.getTextAndColor(vertical: secondTask.vertical)
            self.statusDetail.text = ""//text
            self.statusDetail.textColor = color
         } else {
            let (text, color) = firstTask.job_status!.getTextAndColor(vertical: firstTask.vertical)
            self.statusDetail.text = ""//text
            self.statusDetail.textColor = color
         }
      }
   }
   
   //MARK: Setting ID Label
   func setIDLabel(task : OrderDetails) {
      self.idDetail.text = task.job_id
   }
   
   private func getStatusTextAndColorFrom(order: OrderHistory) -> (text: String, color: UIColor) {
      let (status, vertical) = order.getLatestStatusAndVertical()
      return status.getTextAndColor(vertical: vertical)
   }
}
