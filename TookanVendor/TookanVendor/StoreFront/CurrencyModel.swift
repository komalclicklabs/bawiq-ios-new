//
//  CurrencyModel.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 04/03/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

class Currency: NSObject {
    var id  = String()
    var symbol = String()
    var name = String()
    private var code = ""
  
    
    init?(json:[String:Any]) {
      guard let symbol = json["symbol"] as? String else {
        print("Error -> Unable to parse currency symbol")
          return nil
      }
      self.symbol = symbol
        
        if let value = json["currency_id"] as? Int {
            self.id = "\(value)"
        }else if let value = json["currency_id"] as? String{
            self.id = value
        }
        
        if let value = json["name"] as? String{
            self.name = value
        }
      
      self.code = (json["code"] as? String) ?? ""
        
    }
    
    
}
