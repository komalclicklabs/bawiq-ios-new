//
//  URL+Extension.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 13/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

extension URL {
   static func get(from string: String) -> URL? {
      if let url = URL(string: string) {
         return url
      } else if let url = URL(string: string.addingPercentEncoding(withAllowedCharacters:
         CharacterSet.urlQueryAllowed) ?? "") {
         return url
      } else {
         print("Error -> Not valid URL string")
         return nil
      }
   }
}
