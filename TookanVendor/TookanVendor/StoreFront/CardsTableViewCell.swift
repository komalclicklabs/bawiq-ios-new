//
//  CardsTableViewCell.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 15/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class CardsTableViewCell: UITableViewCell {

    
    //IBOUTLETS
    @IBOutlet weak var isSelectedIcon: UIImageView!
    @IBOutlet weak var cardNumber: UILabel!
    @IBOutlet weak var leftImage: UIImageView!
    @IBOutlet weak var bottomLineDevider: UILabel!
    
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        cardNumber.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
        cardNumber.textColor = COLOR.SPLASH_TEXT_COLOR
       
        self.selectionStyle = .none
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
