//
//  RestaurantMenuObject.swift
//  Jugnoo Autos
//
//  Created by Sourabh on 19/12/16.
//  Copyright © 2016 Socomo Technologies. All rights reserved.
//

import Foundation

enum sortBy:String {
    case delivaryTime = "Delivery Time"
    case popularity = "Popularity"
    case distance = "Distance"
    case price = "Price"
    
    static let allValues = [delivaryTime, popularity, distance, price]
    static let allServerValues = ["delivery_time","popularity","distance","price_range"]

}

enum quickFilters:String {
    case accept_online_payment = "Accepts Online Payment"
    case offers_discount = "Offers Discount"
    case pure_veg = "Pure Vegetarian"
    case free_delivery = "Free Delivery"
    
    static let allValues = [offers_discount, free_delivery, accept_online_payment, pure_veg]
    static let allServerValues = ["offers_discount","free_delivery","accepts_online","pure_veg"]
}


class RestaurantMenuObject {
    
    static let sharedInstance = RestaurantMenuObject()
    static let sharedFilterInstance = RestaurantFilterObjects()
    static let sharedMenuInstance = RestaurantMenuDetailObjects()
    
    var selectedOrderObjectArray = [SelectedOrderObject]()
    var selectedRestaurantID:String!
    var selectedRestaurantName:String!
    var selectedRestaurantImage:String!
    var selectedRestaurantAddress:String!
    
    var previousRestaurantName:String!
    var previousRestaurantID:String!
    
    var restaurantMenuDetailObjectArray = [RestaurantMenuDetailObjects]()
    var restaurantVegMenuDetailObjectArray = [RestaurantMenuDetailObjects]()
    var restaurantMenuDetailFilteredObjectArray = [RestaurantMenuDetailObjects]()
    
    var restaurantMenuObjectArray = [RestaurantNearByObjects]()
    
    var isFilterSelected = false
    
    var filteredRestaurantMenuObjectArray = [RestaurantNearByObjects]()
    var allRestaurantsWithoutSearchArray = [RestaurantNearByObjects]()
    
    var selectedSortType = String()
    var selectedQuickFiltersArray = [String]()
    var selectedCuisinesArray = [String]()
    var selectedCuisinesIdArray = [Int]()
    var selectedMinOrder = Double()
    var selectedDeliveryTime = Double()
    
    // MARK: - Filters for Nearby restaurants
    
    func reset() {
        selectedOrderObjectArray = [SelectedOrderObject]()
        restaurantMenuDetailObjectArray = [RestaurantMenuDetailObjects]()
        restaurantVegMenuDetailObjectArray = [RestaurantMenuDetailObjects]()
        restaurantMenuDetailFilteredObjectArray = [RestaurantMenuDetailObjects]()
        restaurantMenuObjectArray = [RestaurantNearByObjects]()
        isFilterSelected = false
        filteredRestaurantMenuObjectArray = [RestaurantNearByObjects]()
        allRestaurantsWithoutSearchArray = [RestaurantNearByObjects]()
        selectedSortType = String()
        selectedQuickFiltersArray = [String]()
        selectedCuisinesArray = [String]()
        selectedCuisinesIdArray = [Int]()
        selectedMinOrder = Double()
        selectedDeliveryTime = Double()
    }
    
    func applyFilterSelectedNearByArray() {
        // delivery_status_tick_icon
        
        if selectedSortType == "" && selectedQuickFiltersArray.count == 0 && selectedCuisinesArray.count == 0 && selectedMinOrder == 0 && selectedDeliveryTime == 0 {
            self.isFilterSelected = false
        }
        else {
            isFilterSelected = true
        }
        
        
        var filtered = RestaurantMenuObject.sharedInstance.restaurantMenuObjectArray//self.filterRestaurantObjects()
        
       // filtered = self.sortFilter(filterArray: filtered)
        
//        let openRestau = filtered.filter { (element) -> Bool in
//            if !element.isRestaurantClosed && element.isRestaurantAvailable {
//                return true
//            } else {
//                return false
//            }
//        }
//        
//        let closedRestau = filtered.filter { (element) -> Bool in
//            if (element.isRestaurantClosed || !element.isRestaurantClosed) && !element.isRestaurantAvailable {
//                return true
//            } else {
//                return false
//            }
//        }
//        
//        filtered.removeAll()
//        filtered.append(contentsOf: openRestau)
//        filtered.append(contentsOf: closedRestau)
        //filtered = filtered.sorted(by: { !$0.isRestaurantClosed && $1.isRestaurantClosed
            
//        })
        
        RestaurantMenuObject.sharedInstance.filteredRestaurantMenuObjectArray = filtered
    }

    func filterRestaurantObjects() -> [RestaurantNearByObjects] {
        
        var filtered = RestaurantMenuObject.sharedInstance.restaurantMenuObjectArray
        
        filtered = filtered.filter({ (text) -> Bool in
            
            //MARK:- Quick Sort Filter
            
            var is_pure_veg = true
            var is_offers_discount = true
            var accept_online_payment = true
            var is_free_delivery = true
            
            var is_selectedCuisine = true
            var is_selectedMinOrder = true
            var is_selectedDeliveryTime = true
            
            var analyticsQuickFilterText = ""
            var analyticsCuisinesText = ""
            
            var analyticsMinOrderText = ""
            var analyticsDeliveryText = ""
            
            if self.selectedQuickFiltersArray.contains(quickFilters.pure_veg.rawValue){
                
                analyticsQuickFilterText = "Pure Veg"

                is_pure_veg = false
                if text.isRestaurantVeg == true {
                    is_pure_veg = true
                    //                    return true
                }
            }
            
            if self.selectedQuickFiltersArray.contains(quickFilters.offers_discount.rawValue){
                
                analyticsQuickFilterText = "\(analyticsQuickFilterText), Offer Discount"
                
                is_offers_discount = false
                if text.isRestaurantOfferingDiscount == true {
                    is_offers_discount = true
                }
            }
            
            if self.selectedQuickFiltersArray.contains(quickFilters.accept_online_payment.rawValue){
                
                analyticsQuickFilterText = "\(analyticsQuickFilterText), Accepts Online Payment"
                accept_online_payment = false
                if text.restaurantPaymentMode == 2 || text.restaurantPaymentMode == 1{
                    accept_online_payment = true
                }
            }
            
            if self.selectedQuickFiltersArray.contains(quickFilters.free_delivery.rawValue){
                
                analyticsQuickFilterText = "\(analyticsQuickFilterText), Free Delivery"
                is_free_delivery = false
                if text.isRestaurantDeliverFree == true {
                    is_free_delivery = true
                }
                
            }
            //MARK:- Cuisines Filter
            
            if self.selectedCuisinesArray.count > 0 {
                
                analyticsCuisinesText = self.selectedCuisinesArray.joined(separator: ",")
                
                is_selectedCuisine = false
                for val in text.restaurantCuisines {
                    
                    if self.selectedCuisinesArray.contains(val){
                        
                        is_selectedCuisine = true
                    }
                }
            }
            
            //MARK:- Minimum Order Filter
            
            if self.selectedMinOrder > 0 {
                
                analyticsMinOrderText = "\(self.selectedMinOrder)"
                
                is_selectedMinOrder = false
                if text.restaurantMinOrderAmount <= self.selectedMinOrder {
                    
                    is_selectedMinOrder = true
                }
            }
            
            //MARK:- Delivery Time Filter
            
            if self.selectedDeliveryTime > 0 {
                
                analyticsDeliveryText = "\(self.selectedDeliveryTime)"
                
                is_selectedDeliveryTime = false
                if text.restaurantMinDeliveryTime <= self.selectedDeliveryTime {
                    
                    is_selectedDeliveryTime = true
                }
            }
            
            if is_pure_veg && is_offers_discount &&
                accept_online_payment && is_free_delivery &&
                is_selectedCuisine && is_selectedMinOrder &&
                is_selectedDeliveryTime {
                
                if analyticsQuickFilterText != "" {
                    
                }
                if analyticsCuisinesText != "" {
//                    CommonFunctions.shareCommonMethods().ga(withCategory: Category_Menus, action: Action_Filters_Cuisine, label:analyticsCuisinesText , value: nil)
                }
                if analyticsMinOrderText != "" {
                }
                if analyticsDeliveryText != "" {
                }
                
                return true
            }
            
            return false
        })
        
        return filtered
    }
    
    func sortFilter(filterArray:[RestaurantNearByObjects]) -> [RestaurantNearByObjects]{
        
        var analyticsSortByText = ""
        
        var filtered = filterArray
        if self.selectedSortType == sortBy.popularity.rawValue {
            
            analyticsSortByText = "Popularity"
            filtered = filtered.sorted {
                $0.restaurantPopularity > $1.restaurantPopularity
            }
        }
        else if self.selectedSortType == sortBy.distance.rawValue {
            
            analyticsSortByText = "Distance"
            
            filtered = filtered.sorted {
                $0.restaurantDistance < $1.restaurantDistance
            }
        }
        else if self.selectedSortType == sortBy.price.rawValue {
            
            analyticsSortByText = "Price"
            
            filtered = filtered.sorted {
                $0.restaurantPriceRange < $1.restaurantPriceRange
            }
        } else if self.selectedSortType == sortBy.delivaryTime.rawValue {
            
            analyticsSortByText = "Delivery Time"
            
            filtered = filtered.sorted {
                $0.restaurantMinDeliveryTime < $1.restaurantMinDeliveryTime
            }
        }
        
        
        return filtered
    }
    
    // MARK: - Parse Nearby Json
    
    func cancelSearch() {
        if self.allRestaurantsWithoutSearchArray.count > 0 {
            self.restaurantMenuObjectArray = self.allRestaurantsWithoutSearchArray
            self.allRestaurantsWithoutSearchArray = []
            self.applyFilterSelectedNearByArray()
        }
        
    }
    
    func parseNearByJson(jsonArray:[Any], append: Bool = false, isSearching: Bool = false) -> Bool {
        
        if isSearching {
            if allRestaurantsWithoutSearchArray.count == 0 {
                self.allRestaurantsWithoutSearchArray = restaurantMenuObjectArray
            }
        } else {
            self.allRestaurantsWithoutSearchArray = []
        }
        
        if !append {
            restaurantMenuObjectArray.removeAll()
        }
        
        for jsonObject in jsonArray {
            
            if let json = jsonObject as? [String:Any] {
                
                let restaurantMenuObject = RestaurantNearByObjects()
                
                
                if let restaurantID = json["restaurant_id"] as? Int {
                    
                    restaurantMenuObject.restaurantID = restaurantID
                }
                
//                if !restaurantMenuObjectArray.map({$0.restaurantID}).contains(restaurantMenuObject.restaurantID) {
                
                    
                    if let applicable_payment_mode = json["applicable_payment_mode"] as? Int {
                        
                        restaurantMenuObject.restaurantPaymentMode = applicable_payment_mode
                    }
                    
                    if let restaurantName = json["name"] as? String {
                        
                        restaurantMenuObject.restaurantName = restaurantName
                    }
                    
                    if let restaurantDisplayAddress = json["display_address"] as? String {
                        
                        restaurantMenuObject.restaurantDisplayAddress = restaurantDisplayAddress
                    }
                    
                    if let restaurantOpensAt = json["opens_at"] as? String {
                        
                        restaurantMenuObject.restaurantOpensAt = restaurantOpensAt
                    }
                    
                    if let restaurantCloseAt = json["close_at"] as? String {
                        
                        restaurantMenuObject.restaurantCloseAt = restaurantCloseAt
                    }
                    
                    if Singleton.sharedInstance.getTimeInMinsFromDate(timeString: restaurantMenuObject.restaurantCloseAt) < Singleton.sharedInstance.getTimeInMinsFromDate(timeString: restaurantMenuObject.restaurantOpensAt) {
                        restaurantMenuObject.timeDifference = (Singleton.sharedInstance.getTimeInMinsFromCurrentDate() - Singleton.sharedInstance.getTimeInMinsFromDate(timeString: restaurantMenuObject.restaurantOpensAt))
                    }
                        
                    else {
                        restaurantMenuObject.timeDifference = (Singleton.sharedInstance.getTimeInMinsFromDate(timeString: restaurantMenuObject.restaurantCloseAt) - Singleton.sharedInstance.getTimeInMinsFromCurrentDate())
                    }
                    
                    
                    if let restaurantCloseInBuffer = json["close_in_buffer"] as? Int {
                        
                        restaurantMenuObject.restaurantCloseInBuffer = restaurantCloseInBuffer
                    }
                    
                    if let restaurantimage = json["image"] as? String {
                        
                        restaurantMenuObject.restaurantImage = restaurantimage
                    }
                    
                    if let restaurantPopularity = json["popularity"] as? Int {
                        
                        restaurantMenuObject.restaurantPopularity = restaurantPopularity
                    }
                    
                    if let restaurantCuisines = json["cuisines"] as? [String] {
                        
                        restaurantMenuObject.restaurantCuisines = restaurantCuisines
                        if restaurantCuisines.count > 0 {
                            restaurantMenuObject.restaurantCuisinesAttributedValue = restaurantCuisines.joined(separator: " • ")
                        }
                    }
                    
//                    if restaurantMenuObject.restaurantCuisines.count > 0 {
//                        for i in 0 ..< restaurantMenuObject.restaurantCuisines.count {
//                            if i < 3 {
//                                //attriburedCuisines = "\(attriburedCuisines)•\(restaurantInstance.restaurantCuisines[i])"
//                                if restaurantMenuObject.restaurantCuisinesAttributedValue != "" {
//                                    restaurantMenuObject.restaurantCuisinesAttributedValue = restaurantMenuObject.restaurantCuisinesAttributedValue + " • " + restaurantMenuObject.restaurantCuisines[i]
//                                } else {
//                                    restaurantMenuObject.restaurantCuisinesAttributedValue = restaurantMenuObject.restaurantCuisines[i]
//                                }
//                            }
//                        }
//                    }
                    
                    if let restaurantDistance = json["distance"] as? Double {
                        
                        restaurantMenuObject.restaurantDistance = restaurantDistance
                    }
                    if let restaurantIsAvailable = json["is_available"] as? Bool {
                        
                        restaurantMenuObject.isRestaurantAvailable = restaurantIsAvailable
                    }
                    if let restaurantIsClosed = json["is_closed"] as? Bool {
                        
                        restaurantMenuObject.isRestaurantClosed = restaurantIsClosed
                    }
                    if let restaurantIsVeg = json["pure_veg"] as? Bool {
                        
                        restaurantMenuObject.isRestaurantVeg = restaurantIsVeg
                    }
                    if let isRestaurantDeliverFree = json["free_delivery"] as? Bool {
                        
                        restaurantMenuObject.isRestaurantDeliverFree = isRestaurantDeliverFree
                    }
                    
                    if let isRestaurantOfferingDiscount = json["offer_discount"] as? Bool {
                        
                        restaurantMenuObject.isRestaurantOfferingDiscount = isRestaurantOfferingDiscount
                    }
                    
                    if let restaurantDeliveryTime = json["delivery_time"] as? Double {
                        
                        restaurantMenuObject.restaurantDeliveryTime = restaurantDeliveryTime
                    }
                    if let restaurantOpeningTime = json["opens_at"] as? String {
                        
                        restaurantMenuObject.restaurantOpeningTime = restaurantOpeningTime
                    }
                    
                    if let restaurantServiceTax = json["service_tax"] as? Double {
                        
                        restaurantMenuObject.restaurantServiceTax = restaurantServiceTax
                    }
                    if let restaurantPackingCharges = json["packing_charges"] as? Double {
                        
                        restaurantMenuObject.restaurantPackingCharges = restaurantPackingCharges
                    }
                    
                    if let restaurantClosedMessage = json["restaurant_menu_closed_message"] as? String {
                        
                        restaurantMenuObject.restaurantClosedMessage = restaurantClosedMessage
                    }
                    
                    
                    
                    if let restaurantMinOrderAmount = json["minimum_order_amount"] as? Double {
                        
                        restaurantMenuObject.restaurantMinOrderAmount = restaurantMinOrderAmount
                    }
                    
                    if restaurantMenuObject.restaurantMinOrderAmount == 0 {
                        restaurantMenuObject.restaurantMinOrderAmountString = "No minimum order"
                    }
                    else {
                        
                        restaurantMenuObject.restaurantMinOrderAmountString = "Minimum order ₹\(Singleton.sharedInstance.convertPriceToString(value: restaurantMenuObject.restaurantMinOrderAmount))"
                    }
                    
                    if let restaurantMinDeliveryTime = json["min_delivery_time"] as? Double {
                        
                        restaurantMenuObject.restaurantMinDeliveryTime = restaurantMinDeliveryTime
                    }
                    
                    if let deliveryTimeText = json["delivery_time_text"] as? String {
                        
                        restaurantMenuObject.restaurantDeliveryTimeText = deliveryTimeText
                        if (deliveryTimeText.characters.count) > 0 {
                            if let messageString = deliveryTimeText.htmlAttributedString() as? NSMutableAttributedString {
                                restaurantMenuObject.restaurantDeliveryTimeTextAttributedString = getTimingAttributedString(image: #imageLiteral(resourceName: "deliveryTimeIcon"), string: messageString)
                            }
                        } else {
                            restaurantMenuObject.restaurantDeliveryTimeTextAttributedString = NSAttributedString(string: "")
                        }
                    } else {
                        var messageString:NSMutableAttributedString = NSMutableAttributedString(string : "")
                        if restaurantMenuObject.isRestaurantAvailable == false {
                            messageString = deliveryTimeString(" Delivers in ", secondString: "\(Int(restaurantMenuObject.restaurantMinDeliveryTime))-\(Int(restaurantMenuObject.restaurantDeliveryTime)) mins.", textAlighnment: .left)
                        } else {
                            if !restaurantMenuObject.restaurantOpensAt.isEmpty {
                                messageString = deliveryTimeString(" Opens at ", secondString: "\(Singleton.sharedInstance.convert24HourTo12Hour(restaurantMenuObject.restaurantOpensAt))", textAlighnment: .left)
                            }
                        }
                        restaurantMenuObject.restaurantDeliveryTimeTextAttributedString = getTimingAttributedString(image: #imageLiteral(resourceName: "deliveryTimeIcon"), string: messageString)
                }
                    
                    if let minOrderTimeText = json["min_order_text"] as? String {
                        
                        restaurantMenuObject.restaurantMinOrderText = minOrderTimeText
                        if (minOrderTimeText.characters.count) > 0 {
                            restaurantMenuObject.restaurantMinOrderTextAttributedString = minOrderTimeText.htmlAttributedString() ?? NSAttributedString(string: "")
                        } else {
                            restaurantMenuObject.restaurantMinOrderTextAttributedString = NSAttributedString(string: "")
                        }
                    } else {
                        restaurantMenuObject.restaurantMinOrderTextAttributedString = NSAttributedString(string: "")
                    }
                    
                    if let restaurantVAT = json["value_added_tax"] as? Double {
                        
                        restaurantMenuObject.restaurantVAT = restaurantVAT
                    }
                    
                    if let restaurantPriceRange = json["price_range"] as? Double {
                        
                        restaurantMenuObject.restaurantPriceRange = restaurantPriceRange
                    }
                    
                    if let restaurantRatingCount = json["rating"] as? Double {
                        
                        restaurantMenuObject.restaurantRatingCount = restaurantRatingCount
                        
                        restaurantMenuObject.ratingButtonTitle = "\(restaurantRatingCount.rounded(toPlaces: 1))"
                    }
                    
                    if let restaurantRatingColour = json["rating_color"] as? String {
                        
                        restaurantMenuObject.restaurantRatingColour = restaurantRatingColour
                    }
                    
                    if let restaurantReviewCount = json["review_count"] as? Int {
                        
                        restaurantMenuObject.restaurantReviewCount = restaurantReviewCount
                    }
                    
                    if let restaurantOffersText = json["offer_text"] as? String {
                        
                        restaurantMenuObject.restaurantOffersText = restaurantOffersText
                    }
                    
                    restaurantMenuObjectArray.append(restaurantMenuObject)
//                }
                
                
            }
        }
        
        filteredRestaurantMenuObjectArray = restaurantMenuObjectArray

        return true
    }
    
    func getTimingAttributedString(image: UIImage, string: Any) -> NSAttributedString {
        
        let attachment = NSTextAttachment()
        attachment.bounds = CGRect(x: 0, y: -2, width: 10, height: 10)
        attachment.image = image
        let attachmentString = NSAttributedString(attachment: attachment)
        
        let mutableAttachmentString = NSMutableAttributedString(attributedString: attachmentString)
        mutableAttachmentString.append(NSAttributedString(string: " "))
        
        switch string {
        case is NSAttributedString:
            mutableAttachmentString.append(NSAttributedString(attributedString: string as! NSAttributedString))
        case is String:
            mutableAttachmentString.append(NSAttributedString(string: string as! String))
        default:
          return  NSAttributedString(string: "")
        }
        
        return mutableAttachmentString
    }
    

    func deliveryTimeString(_ firstString: String, secondString: String, textAlighnment: NSTextAlignment) -> NSMutableAttributedString {
        
        let combinedString = "\(firstString)\(secondString)"
        
        let rangeOfFirstString = (combinedString as NSString).range(of: firstString)//combinedString.NSRangeFromRange(range: combinedString.range(of: firstString)!)
        let rangeOfSecondString = (combinedString as NSString).range(of: secondString)//combinedString.NSRangeFromRange(range: combinedString.range(of: secondString)!)
        
        let attributedTitle = NSMutableAttributedString(string: combinedString)
        
        attributedTitle.addAttribute(NSForegroundColorAttributeName, value: Singleton.sharedInstance.colorFromRGB(89.0, green: 89.0, blue: 104.0, alpha: 1.0), range: rangeOfFirstString)
        attributedTitle.addAttribute(NSFontAttributeName, value: UIFont(name: FONT.light, size: CGFloat(11.5))!, range: rangeOfFirstString)
        
        attributedTitle.addAttribute(NSForegroundColorAttributeName, value: Singleton.sharedInstance.colorFromRGB(89.0, green: 89.0, blue: 104.0, alpha: 1.0), range: rangeOfSecondString)
        attributedTitle.addAttribute(NSFontAttributeName, value: UIFont(name: FONT.light, size: CGFloat(11.5))!, range: rangeOfSecondString)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = textAlighnment
        
        attributedTitle.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSRange(location: 0, length: combinedString.characters.count))
        
        return attributedTitle
    }
    
    // MARK: - Cart Object
    func cartObject()-> (String, [AnyObject]) {
        
        var cartMutableArray = [AnyObject]()
        for selectedItem in RestaurantMenuObject.sharedInstance.selectedOrderObjectArray {
            
            var cartObjectDictionary = [String:AnyObject]()
            var customizationArray = [AnyObject]()
            
            cartObjectDictionary["item_id"] = selectedItem.productVarietyID as AnyObject
            
            cartObjectDictionary["quantity"] = selectedItem.quantity as AnyObject
            
            var customizationDictionary = [String:AnyObject]()
            
            customizationDictionary["id"] = selectedItem.productVarietyID as AnyObject?
            
            
            var uniqueCustomIDArray = [String]()
            for i in 0..<selectedItem.customizeIDArray.count {
                
                if !uniqueCustomIDArray.contains(selectedItem.customizeIDArray[i]) {
                    uniqueCustomIDArray.append(selectedItem.customizeIDArray[i])
                }
            }
            
            for i in 0..<uniqueCustomIDArray.count {
                
                var customizeOptionObjectArray = [AnyObject]()
                
                for j in 0..<selectedItem.customizeOptionIDArray.count {
                    
                    if selectedItem.customizeIDArray[j] == uniqueCustomIDArray[i] {
                        var customizeOptionObject = [String:AnyObject]()
                        
                        customizeOptionObject["id"] = selectedItem.customizeOptionIDArray[j] as AnyObject?
                        
                        customizeOptionObjectArray.append(customizeOptionObject as AnyObject)
                    }
                }
                
                customizationDictionary["id"] = uniqueCustomIDArray[i] as AnyObject?
                customizationDictionary["options"] = customizeOptionObjectArray as AnyObject?
                
                customizationArray.append(customizationDictionary as AnyObject)
            }
            
            cartObjectDictionary["customisations"] = customizationArray as AnyObject?
            
            cartMutableArray.append(cartObjectDictionary as AnyObject)
        }
        
        do {
//            print(cartMutableArray)
            let data = try JSONSerialization.data(withJSONObject: cartMutableArray, options: .prettyPrinted)
            let string = String(data: data, encoding: .utf8)//NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            
            return (string!, cartMutableArray)
        } catch let error {
            print("geocodingResultsData ERROR", error)
        }
        
        return ("", [])
    }
    
}

class RestaurantNearByObjects {
    
    var restaurantName = String()
    var restaurantID = Int()
    var restaurantImage = String()
    
    var restaurantDisplayAddress = String()
    var restaurantOpensAt = String()
    var timeDifference = 0
    var restaurantCloseAt = String()
    var restaurantCloseInBuffer = Int()
    
    var restaurantPopularity = Int()
    var restaurantCuisines = [String]()
    var restaurantCuisinesAttributedValue = ""
    
    var restaurantDistance = Double()
    
    var isRestaurantAvailable = Bool()
    var isRestaurantDeliverFree = Bool()
    var isRestaurantClosed = Bool()
    var isRestaurantVeg = Bool()
    var isRestaurantOfferingDiscount = Bool()
    
    var restaurantDeliveryTime = Double()
    var restaurantOpeningTime = String()
    
    var restaurantServiceTax = Double()
    var restaurantPackingCharges = Double()
    var restaurantMinOrderAmount = Double()
    var restaurantMinOrderAmountString = "No minimum order"
    
    var restaurantClosedMessage = String()
    var restaurantMinOrderText: String?
    var restaurantDeliveryTimeText: String?
    var restaurantPriceRange = Double()
    var restaurantMinDeliveryTime = Double()
    
    var restaurantVAT:Double!
    
    var restaurantPaymentMode = Int()
    var restaurantRatingCount = Double()
    var restaurantRatingColour = String()
    var restaurantReviewCount = Int()
    
    var restaurantOffersText = String()
    
    //Data needed for cell for row
    
    var timeDifferenceToClose:Int = 0
    var restaurantMinOrderTextAttributedString = NSAttributedString()
    var ratingButtonTitle = String()
    var restaurantDeliveryTimeTextAttributedString = NSAttributedString()
}

class FeedbackObject {
    
    static let sharedInstance = FeedbackObject()
    var imagesPathArray = [String]()
	var imageSizeArray = [Any]()
}

// MARK: - Restore Cart

//func restoreCart(isClosed:Bool) {
//    
//    if UserDefaults.standard.value(forKey: kCheckOutDictionary) != nil {
//        
//        if let checkOutDictionary = UserDefaults.standard.object(forKey: kCheckOutDictionary) as? [String:Any] {
//            
//            if let restaurantNameKey = checkOutDictionary["restaurantNameKey"] as? String {
//                RestaurantMenuObject.sharedInstance.previousRestaurantName = restaurantNameKey
//            }
//            
//            if let restaurantIDKey = checkOutDictionary["restaurantIDKey"] as? String {
//                
//                RestaurantMenuObject.sharedInstance.previousRestaurantID = restaurantIDKey
//                
//                if restaurantIDKey == RestaurantMenuObject.sharedInstance.selectedRestaurantID && isClosed == false {
//                    if let selectedArray = NSKeyedUnarchiver.unarchiveObject(with: checkOutDictionary["checkoutMenuArray"] as! Data) as? [SelectedOrderObject] {
//                        RestaurantMenuObject.sharedInstance.selectedOrderObjectArray = selectedArray
//                    }
//                }
//                else {
//                    RestaurantMenuObject.sharedInstance.selectedOrderObjectArray.removeAll()
//                }
//                
//                print(RestaurantMenuObject.sharedInstance.selectedOrderObjectArray)
//            }
//        }
//        
//    }
//    else {
//        RestaurantMenuObject.sharedInstance.selectedOrderObjectArray.removeAll()
//    }
//    
//}

//func updateAddCartAddress() {
//    
//    if let cachedDeliveryAddressInfo = UserDefaults.standard.object(forKey: USER_OTHER_ADDRESS) as? [String: Any] {
//        UserDefaults.standard.set(cachedDeliveryAddressInfo, forKey: USER_ADD_CART_ADDRESS)
//    }
//    
//    if let deliveryLongitude = UserDefaults.standard.object(forKey: kDeliveryLongitudeString) as? String {
//        UserDefaults.standard.set(deliveryLongitude, forKey: kAddCartLongitudeString)
//    }
//    
//    if let deliveryLatitude = UserDefaults.standard.object(forKey: kDeliveryLatitudeString) as? String {
//        UserDefaults.standard.set(deliveryLatitude, forKey: kAddCartLatitudeString)
//    }
//    
//}

//func saveMenuCartData() {
//    
//    updateAddCartAddress()
//    
//    if RestaurantMenuObject.sharedInstance.selectedOrderObjectArray.count != 0 {
//        var checkOutDictionary = [String:Any]()
//        
//        let checkOutDictionaryData = NSKeyedArchiver.archivedData(withRootObject: RestaurantMenuObject.sharedInstance.selectedOrderObjectArray)
//        
//        checkOutDictionary["checkoutMenuArray"] = checkOutDictionaryData
//        
//        checkOutDictionary["restaurantIDKey"] = RestaurantMenuObject.sharedInstance.selectedRestaurantID
//        checkOutDictionary["restaurantNameKey"] = RestaurantMenuObject.sharedInstance.selectedRestaurantName
//        
//        RestaurantMenuObject.sharedInstance.previousRestaurantID = RestaurantMenuObject.sharedInstance.selectedRestaurantID
//        RestaurantMenuObject.sharedInstance.previousRestaurantName = RestaurantMenuObject.sharedInstance.selectedRestaurantName
//        
//        UserDefaults.standard.set(checkOutDictionary, forKey: kCheckOutDictionary)
//        UserDefaults.standard.synchronize()
//    } else {
//        UserDefaults.standard.removeObject(forKey: kCheckOutDictionary)
//    }
//    
//    
//}

//func resetCartData() {
//    
//    UserDefaults.standard.removeObject(forKey: kCheckOutDictionary)
//    UserDefaults.standard.synchronize()
//}

/*
 
 var filtered = RestaurantMenuObject.sharedInstance.restaurantMenuObjectArray.filter({ (text) -> Bool in
 
 if RestaurantMenuObject.sharedInstance.restaurantFilterObject.selectedCuisinesArray.count > 0 {
 isCuisinesSelected = false
 
 for val in text.restaurantCuisines {
 
 if RestaurantMenuObject.sharedInstance.restaurantFilterObject.selectedCuisinesArray.contains(val){
 isCuisinesSelected = true
 break
 }
 }
 }
 
 if RestaurantMenuObject.sharedInstance.restaurantFilterObject.selectedMinOrder > 0 {
 isMinOrderSelected = false
 if text.restaurantMinOrderAmount >= RestaurantMenuObject.sharedInstance.restaurantFilterObject.selectedMinOrder {
 isMinOrderSelected = true
 } else {
 return false
 }
 }
 if RestaurantMenuObject.sharedInstance.restaurantFilterObject.selectedDeliveryTime > 0 {
 isDeliveryTimeSelected = false
 
 if text.restaurantDeliveryTime >= RestaurantMenuObject.sharedInstance.restaurantFilterObject.selectedDeliveryTime {
 isDeliveryTimeSelected = true
 } else {
 return false
 }
 }
 
 if isCuisinesSelected == true &&
 isMinOrderSelected == true &&
 isDeliveryTimeSelected == true {
 return true
 }
 else {
 return false
 }
 })
 
 */
