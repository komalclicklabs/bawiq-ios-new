//
//  NLevelActionView.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 15/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

enum ButtonPressedType{
    case Add
    case Subtract
    case Next
}


class NLevelActionView: UIView {
typealias OnActivityButtonClick = (( _ withType:ButtonPressedType) -> Void)
    
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var MiddleButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!

    var onClickHandler:OnActivityButtonClick?
    var isZero : Bool?
    
    
    override func awakeFromNib() {
        MiddleButton.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: UIControlState.normal)
        MiddleButton.setTitle("Add", for: UIControlState.normal)
        MiddleButton.setTitle("Remove", for: UIControlState.selected)
        MiddleButton.titleLabel?.font = UIFont(name: FONT.semiBold, size: FONT_SIZE.buttonTitle)
        MiddleButton.isHidden = true
        countLabel.isHidden = true
    }
    
    var actionType : ActionType?
    
    
    @IBAction func middleButtonAction(_ sender: UIButton) {
        self.addButtonAction()
    }

    func addButtonAction() {
        switch self.actionType! {
        case .next:
            self.onClickHandler!(ButtonPressedType.Next)
        case .singleAddition:
            if isZero  == true {
                self.onClickHandler!(ButtonPressedType.Add)
            }else{
                self.onClickHandler!(ButtonPressedType.Subtract)
            }
        case .multipleAddition:
            self.onClickHandler!(ButtonPressedType.Add)
        default:
            self.onClickHandler!(ButtonPressedType.Next)
        }
    }
    
    @IBAction func rightButtonAction(_ sender: UIButton) {
        self.addButtonAction()
        //self.onClickHandler!(ButtonPressedType.Add)
    }
    
    
    @IBAction func leftButtonAction(_ sender: UIButton) {
        self.onClickHandler!(ButtonPressedType.Subtract)
    }
    

    
    func setupActionView(withType:ActionType,isZero:Bool,buttonPressed:@escaping OnActivityButtonClick){
        self.onClickHandler = buttonPressed
        self.actionType = withType
        self.isZero = isZero
        print(withType)
        print(isZero)
        switch withType {
        case .multipleAddition:
            setupForMultiSelection(isZero: isZero)
        case .singleAddition:
            setupForSingleSelect(isZero: isZero)
        case .next:
            setUpForNext()
        default:
             hideAllButtons()
        }
    }
    
    func setCountLabel(count:Int) {
        self.countLabel.text = "\(count)"
        if count == 0 {
            self.countLabel.isHidden = true
        } else {
            self.countLabel.isHidden = false
        }
    }
    
    func setCornerRadiusAndShadow() {
        self.rightButton.backgroundColor = .white
        self.rightButton.layer.cornerRadius = self.rightButton.frame.height / 2
        self.rightButton.layer.masksToBounds = false
        self.rightButton.setButtonShadow()
        self.removeViewShadow()
    }
    
    func removeButtonShadow() {
        self.rightButton.layer.shadowOffset = CGSize(width: 0, height: 0.0)
        self.rightButton.layer.shadowOpacity = 0.0
        self.rightButton.layer.shadowColor = nil
        self.rightButton.layer.shadowRadius = 0
    }
    
    func setFullViewRadiusAndShadow() {
        self.removeButtonShadow()
        self.backgroundColor = .white
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 5)
        self.layer.shadowOpacity = 0.1
        self.layer.shadowRadius = 5
    }
    
    func removeViewShadow() {
        self.layer.shadowColor = nil
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = 0.0
        self.layer.shadowRadius = 0
        self.layer.cornerRadius = 0.0
    }
    
    func setupForMultiSelection(isZero:Bool = true){
        let addImage  = UIImage(named: "addRed", in: frameworkBundle, compatibleWith: nil)?.renderWithAlwaysTemplateMode()
        let remove = UIImage(named: "remove", in: frameworkBundle, compatibleWith: nil)
        self.rightButton.setImage(addImage, for: UIControlState.normal)
        self.rightButton.tintColor = COLOR.THEME_FOREGROUND_COLOR
        self.rightButton.setImage(remove, for: .selected)
        self.leftButton.setImage(remove, for: UIControlState.normal)
        rightButton.isHidden = false //isZero
        leftButton.isHidden = isZero
        MiddleButton.isHidden = true
        if isZero == true {
            self.setCornerRadiusAndShadow()
        } else {
            self.setFullViewRadiusAndShadow()
        }
       // MiddleButton.isHidden = !isZero
    }
    
    func setupForSingleSelect(isZero:Bool = true){
        //MiddleButton.isHidden = false
        rightButton.isHidden = false
        leftButton.isHidden = true
        //MiddleButton.isSelected = !isZero
        MiddleButton.isHidden = true
        if isZero == true {
            rightButton.isSelected = false
        } else {
            rightButton.isSelected = true
        }
        self.setCornerRadiusAndShadow()
    }
    
    func setUpForNext(){
        MiddleButton.isHidden = false
        MiddleButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)
        MiddleButton.titleLabel?.textColor = COLOR.THEME_FOREGROUND_COLOR
        MiddleButton.setTitle("", for: UIControlState.normal)
        MiddleButton.tintColor = COLOR.THEME_FOREGROUND_COLOR
        MiddleButton.setImage(#imageLiteral(resourceName: "iconSigninWelcome").renderWithAlwaysTemplateMode(), for: UIControlState.normal)
    }
    
    func hideAllButtons(){
        MiddleButton.isHidden = true
        leftButton.isHidden = true
        rightButton.isHidden = true
    }
    
    static func getWidthFor(action:ActionType) -> CGFloat{
        switch action {
            
        case .multipleAddition:
            return 124
            
        case .singleAddition :
            return 104
         
        case .next:
         return 30
        case .nextButButtonHidden:
            return 0
            
        }
        
    }
    
    
    
    }
    
    
    
    

