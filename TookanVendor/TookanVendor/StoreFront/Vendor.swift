//
//  Vendor.swift
//  StoreFront
//
//  Created by cl-macmini-45 on 19/09/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

//class Vendor: NSObject {
//    var key = ""
//    var form_id = ""
//    var user_id = ""
//    var phoneNo = ""
//    var firstName = ""
//    var email = ""
//    var id = 0
//    var appAccessToken = ""
//    
//    
//    static var shared = Vendor()
//    
//    override init() {
//    }
//    
//    init(json:[String:Any]) {
//        
//        
//        
//        if let value = json["api_key"] as? String {
//            self.key = value
//        }
//        
//        if let value = json["form_id"] as? String {
//            self.form_id = value
//        } else if let value  = json["form_id"] as? NSNumber {
//            self.form_id = "\(value)"
//        }
//        
//        if let value = json["user_id"] as? String {
//            self.user_id = value
//        } else if let value  = json["user_id"] as? NSNumber {
//            self.user_id = "\(value)"
//        }
//        
//    }
//}


import UIKit
import CoreLocation

class Vendor: NSObject, NSCoding {
    
    static var current: Vendor?
    
    var accessToken: String?
    var address: String?
    var appAccessToken: String?
    var appVersionCode: String?
    var company: String?
    var creationDateTime: String?
    var vendorDescription: String?
    var email: String?
    var firstName: String?
    var isBlocked: Int?
    var language: String?
    var lastLoginDateTime: String?
    var lastName: String?
    var latitude: Double?
    var longitude: Double?
    var phoneNo: String?
    var userId: Int?
    var id: Int?
    var vendorImage: String?
    var is_phone_verified = "0"
    var pendingAmount = Double()
    var referralCode = String()
    var credits = String()
    var isFirstSignup = false
    var welcomePopUpMessage = ""
    var isSosEnabled = false
    
    required init(coder aDecoder: NSCoder) {
        
        accessToken = aDecoder.decodeObject(forKey: "accessToken") as? String
        address = aDecoder.decodeObject(forKey: "address") as? String
        appAccessToken = aDecoder.decodeObject(forKey: "appAccessToken") as? String
        appVersionCode = aDecoder.decodeObject(forKey: "appVersionCode") as? String
        company = aDecoder.decodeObject(forKey: "company") as? String
        creationDateTime = aDecoder.decodeObject(forKey: "creationDateTime") as? String
        vendorDescription = aDecoder.decodeObject(forKey: "vendorDescription") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        firstName = aDecoder.decodeObject(forKey: "firstName") as? String
        isBlocked = aDecoder.decodeObject(forKey: "isBlocked") as? Int
        language = aDecoder.decodeObject(forKey: "language") as? String
        lastLoginDateTime = aDecoder.decodeObject(forKey: "lastLoginDateTime") as? String
        lastName = aDecoder.decodeObject(forKey: "lastName") as? String
        latitude = aDecoder.decodeObject(forKey: "latitude") as? Double
        longitude = aDecoder.decodeObject(forKey: "longitude") as? Double
        phoneNo = aDecoder.decodeObject(forKey: "phoneNo") as? String
        vendorImage = aDecoder.decodeObject(forKey: "vendor_image") as? String
        userId = aDecoder.decodeObject(forKey: "userId") as? Int
        id = aDecoder.decodeObject(forKey: "vendorId") as? Int
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(accessToken, forKey: "accessToken")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(appAccessToken, forKey: "appAccessToken")
        aCoder.encode(appVersionCode, forKey: "appVersionCode")
        aCoder.encode(company, forKey: "company")
        aCoder.encode(creationDateTime, forKey: "creationDateTime")
        aCoder.encode(vendorDescription, forKey: "vendorDescription")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(isBlocked, forKey: "isBlocked")
        aCoder.encode(language, forKey: "language")
        aCoder.encode(lastLoginDateTime, forKey: "lastLoginDateTime")
        aCoder.encode(lastName, forKey: "lastName")
        aCoder.encode(latitude, forKey: "latitude")
        aCoder.encode(longitude, forKey: "longitude")
        aCoder.encode(phoneNo, forKey: "phoneNo")
        aCoder.encode(userId, forKey: "userId")
        aCoder.encode(id, forKey: "vendorId")
        aCoder.encode(vendorImage, forKey: "vendor_image")
    }
    
    init(json: [String: Any]) {
        
        print(json)
        accessToken = (json["access_token"] as? String) ?? ""
        appAccessToken = (json["app_access_token"] as? String) ?? ""
        if let value = json["app_versioncode"] {
            appVersionCode = "\(value)"
        }
        
        if let value = json["vendor_id"] {
            id = Int("\(value)") ?? 0
        }
        if let value = json["user_id"] as? Int {
            self.userId = value
        } else if let value = json["user_id"] as? String {
            self.userId = Int(value)
        } else {
            self.userId = 0
        }
        
        firstName = (json["first_name"] as? String) ?? ""
        lastName = (json["last_name"] as? String) ?? ""
        vendorImage = (json["vendor_image"] as? String) ?? ""
        email = (json["email"] as? String) ?? ""
        phoneNo = (json["phone_no"] as? String) ?? ""
        address = (json["address"] as? String) ?? ""
        referralCode = (json["referral_code"] as? String) ?? ""
        vendorDescription = (json["description"] as? String) ?? ""
        company = (json["company"] as? String) ?? ""
        
        if let value = json["credits"] {
            self.credits = (Double("\(value)") ?? 0).description
        }
        
        if let value = json["latitude"] {
            self.latitude = Double("\(value)") ?? 0
        }
        
        if let value = json["longitude"] {
            self.longitude = Double("\(value)") ?? 0
        }
        
        creationDateTime = (json["creation_date_time"] as? String) ?? ""
        lastLoginDateTime = (json["last_login_date_time"] as? String) ?? ""
        isBlocked = (json["is_blocked"] as? Int) ?? 0
        language = (json["language"] as? String) ?? ""
        
        if let value = json["is_phone_verified"] as? String{
            self.is_phone_verified = value
        }else if let value = json["is_phone_verified"] as? Int{
            self.is_phone_verified = "\(value)"
        }
        
        if let value = json["pending_amount"] as? Double{
            self.pendingAmount = 0.0
        }else if let value = json["pending_amount"] as? String{
            self.pendingAmount = 0.0
        }
    }
    
    // MARK: - Methods
    func getCreditsDescription() -> String {
        let creditsInInt = Int(Double(credits) ?? 0)
        let creditString = creditsInInt == 0 ? TEXT.NO : creditsInInt.description
        
        let creditSuffix: String
        if creditsInInt == 0 || creditsInInt > 1 {
            creditSuffix = "Credits"
        } else {
            creditSuffix = "Credit"
        }
        
        return creditString + " " + creditSuffix
    }
    
    
    func sendReferralCodeToServer(referralCode: String, completion: Completion) {
        guard let params = getParamsForSendingreferalCode(referralCode: referralCode) else {
            completion?(false)
            return
        }
        
        HTTPClient.makeConcurrentConnectionWith(method: .POST, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: false, para: params, extendedUrl: API_NAME.sendReferral) { (responseObject, _, _, statusCode) in
            
            if let response = responseObject as? [String: Any],
                let data = response["data"] as? [String: Any],
                let newCredits = data["new_credits"] {
                let creditsInInt = Int("\(newCredits)") ?? 0
                self.credits = creditsInInt.description
            }
            
            if statusCode == .some(STATUS_CODES.SHOW_DATA) {
                completion?(true)
            } else {
                completion?(false)
            }
        }
    }
    
    private func getParamsForSendingreferalCode(referralCode: String) -> [String: Any]? {
        guard let accessToken = self.accessToken,
            let appAccessToken = self.appAccessToken,
            let userId = self.userId else {
                print("UserId or AppAccessToken or accessToken not found")
                return nil
        }
        
        return [
            "user_id": userId,
            "access_token": accessToken,
            "app_access_token": appAccessToken,
//            "device_token": APIManager.sharedInstance.getDeviceToken(),
            "app_version": APP_VERSION,
            "app_device_type": APP_DEVICE_TYPE,
            "referral_code": referralCode
        ]
    }
    
    
    // MARK: - Type Methods
    
    
    static func checkUserWithEmailExists(email: String, completion: Completion) {
        let params = getParamsToCheckEmailExists(email: email)
        
        HTTPClient.makeConcurrentConnectionWith(method: .POST, showAlertInDefaultCase: false, para: params, extendedUrl: API_NAME.checkEmailRegistered) { (responseObject, _, _, statusCode) in
            if statusCode == .some(STATUS_CODES.SHOW_DATA) {
                completion?(true)
            } else if statusCode == .some(STATUS_CODES.BAD_REQUEST) {
                completion?(false)
            }
        }
    }
    
    private static func getParamsToCheckEmailExists(email: String) -> [String: Any] {
        return [
//            "device_token": APIManager.sharedInstance.getDeviceToken(),
            "app_version": APP_VERSION,
            "app_device_type": APP_DEVICE_TYPE,
            "email": email
        ]
    }
    
    static func hasSignedUpBefore() -> Bool {
        if let flag = UserDefaults.standard.value(forKey: "hasUserSignedUpBefore") as? Bool {
            return flag
        }
        
        return false
    }
    
    static func userHasLoggedIntoAppAtLeastOnce() {
        UserDefaults.standard.set(true, forKey: "hasUserSignedUpBefore")
    }
    
    func isDemoUser() -> Bool {
        if let accessToken = self.appAccessToken {
            return accessToken == "" ? true : false
        }
        return true
    }
    
}
