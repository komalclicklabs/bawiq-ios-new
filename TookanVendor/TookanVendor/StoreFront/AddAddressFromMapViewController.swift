//
//  AddAddressFromMapViewController.swift
//  TookanVendor
//
//  Created by Aditi on 7/25/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

import GoogleMaps
import GooglePlaces
import CoreLocation


class AddAddressFromMapViewController: UIViewController,GMSMapViewDelegate,CLLocationManagerDelegate {
   
   // MARK: - Properties
   var locationManager = CLLocationManager()
    @IBOutlet weak var  googleMapView: GMSMapView!
   var currentLocation: FavouriteAddress!
   
   var favLocationManager: FavouriteLocationManager?
   var savingForType: favLocationType!
   
   // MARK: - IBOutlets
   @IBOutlet weak var addressView: UIView!
   @IBOutlet weak var iconImage: UIImageView!
   @IBOutlet weak var outletAddressButton: UIButton!
   @IBOutlet weak var textAddress: MarqueeLabel!
   @IBOutlet weak var nextButtonOutlet: UIButton!
   @IBOutlet weak var marker: UIImageView!
   @IBOutlet weak var backButton: UIButton!
   
   // MARK: - View Life Cycle
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      self.nextButtonOutlet.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
//      self.googleMapView.frame = self.view.frame
    
   }
   
   override func viewDidLoad() {
      super.viewDidLoad()
      marker.image = iconDropOff!
      configureViews()
      setupMap()
      setLocationManager()
      setIntialCameraPositionOfMap()
   }
   
   private func configureViews() {
      textAddress.font = UIFont(name: FONT.light, size: FONT_SIZE.priceFontSize)
      textAddress.text = ""
      
      addressView.setShadow()

      let nextButtonTitle = favLocationManager?.InFlowforSavingLocation() == true ? TEXT.NEXT : TEXT.DONE
      nextButtonOutlet.configureNormalButtonWith(title: nextButtonTitle)
//    nextButtonOutlet.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
   }
   
   private func setupMap() {
    backButton.setImage(backButtonImage, for: UIControlState.normal)
    
    DispatchQueue.main.async {
      self.googleMapView.delegate = self
      let status = CLLocationManager.authorizationStatus()
      self.googleMapView.isMyLocationEnabled = status != .denied
      self.googleMapView.settings.myLocationButton = false
    }
      
//      view.bringSubview(toFront: backButton)
   }
   
   private func setLocationManager() {
      locationManager.delegate = self
      locationManager.desiredAccuracy = kCLLocationAccuracyBest
   }
   
   private func setIntialCameraPositionOfMap() {
      guard let location = currentLocation else {
         let dispatchTimeAfter = DispatchTime.now() + 0.8
         DispatchQueue.main.asyncAfter(deadline: dispatchTimeAfter) { [weak self] in
            self?.locationManager.startUpdatingLocation()
         }
         return
      }
      
      let latitude = Double(location.latitude) ?? 0
      let longitude = Double(location.longitude) ?? 0
      
      let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
      let cameraPosition = GMSCameraPosition.init(target: coordinate, zoom: 17, bearing: 0, viewingAngle: 0)
    DispatchQueue.main.async {
      self.googleMapView.camera = cameraPosition
    }
      textAddress.text = location.streetAddress
   }
   
   // MARK: - IBAction
   @IBAction func nextButton(_ sender: Any) {
      guard checkCurrentLocationIsValid() else {
//         Singleton.sharedInstance.showAlert("Please select a location first")
      ErrorView.showWith(message:"Please select a location first", isErrorMessage: true, removed: nil)
        
         return
      }
      
      guard favLocationManager?.InFlowforSavingLocation() == true else {
         
         let coordinate = CLLocationCoordinate2D(latitude: Double(currentLocation.latitude)!, longitude: Double(currentLocation.longitude)!)
         favLocationManager?.addressSelectedWith(address: currentLocation.address, coordinate: coordinate)
         return
      }
      
      currentLocation.type = savingForType
      SaveAddressWithLabelViewController.pushIn(navVC: self.navigationController!, manager: favLocationManager!, address: currentLocation)
   }
   
   fileprivate func checkCurrentLocationIsValid() -> Bool {
      guard let tempCurrentLocation = currentLocation,
         !tempCurrentLocation.address.isEmpty else {
         return false
      }
    if tempCurrentLocation.address == TEXT.GettingAddress {
        return false
    }
      
      return true
   }
   
   @IBAction func getAddress(_ sender: Any) {
      let autocompleteController = GMSAutocompleteViewController()
      autocompleteController.delegate = self
      present(autocompleteController, animated: true, completion: nil)
   }
   
   @IBAction func backButtonPressed(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
   }
   

   //MARK: - Google Map Delegete
   
   func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
      
      reverseGeocodeCoordinate(coordinate: position.target)
   }
   
   func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
      locationManager.startUpdatingLocation()
      
      return false
   }
   
   // MARK: - Location Manager
   
   func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
      if currentLocation == nil {
         locationManager.startUpdatingLocation()
      }
    DispatchQueue.main.async {
      self.googleMapView.isMyLocationEnabled = (status != .denied)
    }
   }
   
   func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
      
      guard let location = locations.last else {
         return
      }
      
      let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 17.0)
       DispatchQueue.main.async {
      self.googleMapView.animate(to: camera)
    }
      
      reverseGeocodeCoordinate(coordinate: location.coordinate)
      
      //Finally stop updating location otherwise it will come again and again in this delegate
      self.locationManager.stopUpdatingLocation()
      
   }
   
   
   // MARK: - Geocoder
   func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
      textAddress.text = TEXT.GettingAddress

      guard IJReachability.isConnectedToNetwork() else {
         return
      }
      
      let geocoder = GMSGeocoder()
      geocoder.reverseGeocodeCoordinate(coordinate) { [weak self]response, error in
         if let address = response?.firstResult() {
            guard let _ = self else {
                self?.textAddress.text = TEXT.goToPinText
                self?.intializeCurrentLocationWith(coordinates: coordinate, address: TEXT.goToPinText)
               return
            }
            
            var addressStringified = ""
            
            if let lines = address.lines {
               addressStringified = lines.joined(separator: " ")
            }
            
            self?.textAddress.text = addressStringified
            
            self?.intializeCurrentLocationWith(coordinates: coordinate, address: addressStringified)
         }
      }
   }
   
   func intializeCurrentLocationWith(coordinates: CLLocationCoordinate2D, address: String) {
      let label = currentLocation?.label ?? ""
      let  houseNo = currentLocation?.flatNo ?? ""
      let landmark = currentLocation?.landmark ?? ""
      let type = currentLocation?.type ?? savingForType!
      let favId = currentLocation?.favId ?? ""
      
      currentLocation = FavouriteAddress(label: label, address: address, houseNo: houseNo, landmark: landmark, lattitude: coordinates.latitude.description, longitude: coordinates.longitude.description, type: type, favId: favId)
   }
   
   // MARK: - Navigation
   static func pushViewControllerIn(navVC: UINavigationController, manager: FavouriteLocationManager?, savingforType: favLocationType, locationToEdit: FavouriteAddress?) {
      let vc = findIn(storyboard: .favLocation, withIdentifier: "AddAddressFromMapViewController") as! AddAddressFromMapViewController
      vc.favLocationManager = manager
      vc.savingForType = savingforType
      vc.currentLocation = locationToEdit
      navVC.pushViewController(vc, animated: true)
    
   }
   
   
}

extension AddAddressFromMapViewController: GMSAutocompleteViewControllerDelegate {
   
   // Handle the user's selection.
   func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
      
      let lat = place.coordinate.latitude
      let long = place.coordinate.longitude
      
      let address = place.formattedAddress ?? ""
      self.textAddress.text = address
      
      intializeCurrentLocationWith(coordinates: place.coordinate, address: address)
      
      let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 17.0)
     DispatchQueue.main.async {
      self.googleMapView.camera = camera
    }
      dismiss(animated: true, completion: nil)
   }
   
   func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
      // TODO: handle the error.
      print("Error: ", error.localizedDescription)
   }
   
   // User canceled the operation.
   func wasCancelled(_ viewController: GMSAutocompleteViewController) {
      dismiss(animated: true, completion: nil)
   }
   
   // Turn the network activity indicator on and off again.
   func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
      UIApplication.shared.isNetworkActivityIndicatorVisible = true
   }
   
   func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
      UIApplication.shared.isNetworkActivityIndicatorVisible = false
   }
}

