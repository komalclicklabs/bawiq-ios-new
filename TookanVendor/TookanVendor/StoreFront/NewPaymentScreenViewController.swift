//
//  NewPaymentScreenViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 15/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation

protocol updateMetaData:class {
    func updateMetaData(data:[Any],date:String,currency:String,paymentMethod:String,cardId:String,ammount:String,isNow:Bool,tip:String)
}


class NewPaymentScreenViewController: UIViewController, UITableViewDelegate,UITableViewDataSource, GMSMapViewDelegate {
   
   // MARK: - IBOutlets
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var tableView: UITableView!
   @IBOutlet weak var backButton: UIButton!
   @IBOutlet weak var payButton: UIButton!
   @IBOutlet weak var bottomButtonHeight: NSLayoutConstraint!
   @IBOutlet weak var creditsLabel: UILabel!
   @IBOutlet weak var navigationView: UIView!
   @IBOutlet weak var bottomLineInNavigationBar: UIView!
   
   
   // MARK: - Properties
   var headerViewArray = [tableViewHeaderType]()
   var tipDictionary = [1:"1",2:"2",3:"3",4:"4",2232:"0"]
   var selectedTag = 2232
   
   var isTipSelected: Bool {
      return selectedTag != 2232
   }
   
   let sectionHeaderNib = UINib(nibName: NIB_NAME.PaymentHeaders, bundle: frameworkBundle)
   
   let tipHeading = "Tip"
   
   var initialAmmount = String()
   
   var showPromo = false
   var showBillBreakUp = FormDetails.currentForm.verticalType == .taxi ? false : true
   var showTipCell = false
   
   var cardsArray = [cardsModel]()
   
   var billNameArray = [String]()
   var billValueArray = [String]()
   
   var selectedCardIndex = 200
   var selectedPromoIndex = 200
   
   var isPromoSelected: Bool {
      return selectedPromoIndex != 200
   }
   
   
   var showLoaderForCards = false
   var showLoaderForBill = false
   var showloaderForPromo = false
   
   var delegate : updateMetaData?
   var pickupCustomFieldTemplate = ""
   var selectedLat = Double()
   var selectedLong = Double()
   var isNow = true
   var dateInString = String()
   var dateInDateFormat = Date()
   var ammountLabelText : String?
   
   
   var isForPayment = false
   var forPending = false
   
   var isDeliveryAdded = true
   var selected = Int()
   var pickUpMetaData = [Any]()
   
   var selectedDropLat = Double()
   var selectedDropLong = Double()
   var selectedDestinationAddress = String()
   var currencyId = ""
   
   
   var AddDestination:(addDestination:Bool,onCompletion:(_ estimatedPrice:String)->Void)?
   var amount : String?
   
   var allCardsArray = [Any]()
   var afterSelecting : ((_ id: String,_ Type: String,_ tip:String) -> Void)?
   
   
   var isOpenedFromSideMenu = false
   

   // MARK: - View Life Cycle
   override func viewDidLoad() {
      
      super.viewDidLoad()
      
      configureViews()
      setUpTipConfiguration()
      createSuperArray()
      registerCells()
      setupTableView()


      DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
         
         if FormDetails.currentForm.hasPaymentMethodOtherThanCash() {
            self.apiHitToGetAllCards()
         }else{
            self.selectedCardIndex = 0
        }
         
         if FormDetails.currentForm.verticalType != .taxi && self.isOpenedFromSideMenu == false {
            self.apiHitToGetBillBreakDown(promo_value: "", benefit_type: "")
         }
         
         if AppConfiguration.current.isPromoRequired == "1"{
            self.getPromo()
         }
         
      }
   }
   
   func setUpTipConfiguration(){
      for i in 0..<FormDetails.currentForm.tipDetails.defaultValues.count {
         let number = i
         self.tipDictionary[number+1] = "\(FormDetails.currentForm.tipDetails.defaultValues[i])"
      }
      
      self.tableView.reloadData()
   }
   
   func configureViews() {
      bottomLineInNavigationBar.backgroundColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.05)
      self.view.backgroundColor =  COLOR.SPLASH_BACKGROUND_COLOR
      tableView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      
      configureTitleLabel()
      configureBackButton()
      configurePayButton()
   }
   
   func configureBackButton() {
      backButton.setImage(backButtonImage!.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: UIControlState.normal)
      backButton.tintColor = COLOR.SPLASH_TEXT_COLOR
      backButton.setTitle("", for: UIControlState.normal)
   }
   
   func configureTitleLabel() {
      titleLabel.text = isOpenedFromSideMenu ? TEXT.payments : TEXT.PAYMENT.capitalized
      titleLabel.textColor = COLOR.SPLASH_TEXT_COLOR
      titleLabel.font = UIFont(name: FONT.light, size: CGFloat(20))
      titleLabel.letterSpacing = 1
   }
   
   func configurePayButton() {
      payButton.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
      payButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.priceFontSize)
      payButton.setTitleColor(COLOR.SPLASH_BACKGROUND_COLOR, for: UIControlState.normal)
      
      if FormDetails.currentForm.verticalType == .taxi {
         payButton.setTitle(TEXT.BookText, for: UIControlState.normal)
         payButton.titleLabel?.letterSpacing = 1
         
      }else{
         amount = ""
         payButton.setTitle("Pay \(amount!)", for: UIControlState.normal)
         payButton.titleLabel?.letterSpacing = 1
      }
   }
   
   func setupTableView() {
      tableView.dataSource = self
      tableView.delegate  = self
      tableView.separatorStyle = .none
      tableView.rowHeight = UITableViewAutomaticDimension
      tableView.estimatedRowHeight = 60
      tableView.contentInset = UIEdgeInsets(top: -35, left: 0, bottom: 0, right: 0)
   }
   
   func registerCells() {
      tableView.register(UINib(nibName: NIB_NAME.CardsCell, bundle: frameworkBundle), forCellReuseIdentifier:  CELL_IDENTIFIER.cardCell)
    
    tableView.register(UINib(nibName: NIB_NAME.promoCell, bundle: frameworkBundle), forCellReuseIdentifier:  CELL_IDENTIFIER.promoCell)
    tableView.register(UINib(nibName: NIB_NAME.BillBreakupCell, bundle:frameworkBundle), forCellReuseIdentifier:  CELL_IDENTIFIER.billCell)
     tableView.register(UINib(nibName: NIB_NAME.AddNewPromoCell, bundle: frameworkBundle), forCellReuseIdentifier:  CELL_IDENTIFIER.addNewPromo)
    tableView.register(UINib(nibName: NIB_NAME.TipCell, bundle:frameworkBundle), forCellReuseIdentifier:  CELL_IDENTIFIER.tip)
   }
   
   // MARK: -
   
   
   @IBAction func backButtonAction(_ sender: UIButton) {
      _ = self.navigationController?.popViewController(animated:true)
   }
   
   // MARK: - Table View DataSource
   func numberOfSections(in tableView: UITableView) -> Int {
      return headerViewArray.count
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
      switch headerViewArray[section] {
      case .cards:
         if FormDetails.currentForm.hasCashAsAPaymentMethod() {
            return cardsArray.count + 1
         } else {
            return cardsArray.count
         }
      case .promo:
         if showPromo == true    {
            return Singleton.sharedInstance.couponsArray.count  + 1
         }else{
            return 0
         }
      case .billOrEstimation:
         return showBillBreakUp ? billNameArray.count : 0
      case .tip:
         return showTipCell ? 1 : 0
      }
      
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      switch headerViewArray[indexPath.section] {
      case .cards:
         return getCardTableViewCellFor(indexPath: indexPath)
      case .promo:
         return getPromoTableViewCellFor(indexPath: indexPath)
      case .billOrEstimation:
         return getBillEstimationTableViewCellFor(indexPath: indexPath)
      case .tip:
         return getTipTableViewCellFor(indexPath: indexPath)
      }
   }
   
   func getTipTableViewCellFor(indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.tip, for: indexPath) as? TipTableViewCell
      cell?.tipDict = self.tipDictionary
      cell?.setUpButtons()
      cell?.selectedTag = selectedTag
      cell?.onButtonClick = { [weak self] (value,tag) in
         self?.selectedTag  = tag
         self?.showTipCell = !(self?.isTipSelected ?? true)
         self?.tableView.reloadData()
         self?.updateBilBreakDownOrAppEstimate()
      }
      cell?.resetAllButtonsAccept(tag: self.selectedTag)
      return cell!
   }
   
   func getBillEstimationTableViewCellFor(indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.billCell, for: indexPath) as? BillBreakUpTableViewCell
      cell?.valueLabel.text =   billValueArray[indexPath.row]
      cell?.valeuName.text = billNameArray[indexPath.row]
      
      
      if indexPath.row < billNameArray.count - 1{
         
         cell?.valeuName.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
         
         cell?.valueLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)
         
         cell?.upperHorizontalLine.isHidden = true
         cell?.lowerHorizontalLine.isHidden = true
      }else{
         
         cell?.valeuName.font = UIFont(name: FONT.light, size: FONT_SIZE.textfieldSize)
         
         cell?.valueLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.textfieldSize)
         
         cell?.upperHorizontalLine.isHidden = false
         cell?.lowerHorizontalLine.isHidden = true
      }
      
      
      return cell!
   }
   
   func getPromoTableViewCellFor(indexPath: IndexPath) -> UITableViewCell {
      if indexPath.row < Singleton.sharedInstance.couponsArray.count {
         let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.promoCell, for: indexPath) as? PromoTableViewCell
         
         
         
         let firstAttributedText = NSMutableAttributedString(string: Singleton.sharedInstance.couponsArray[indexPath.row].description, attributes: [NSFontAttributeName:UIFont(name: FONT.light, size: FONT_SIZE.priceFontSize)!,NSForegroundColorAttributeName:COLOR.SPLASH_TEXT_COLOR])
         
         
         cell?.promoText.attributedText = firstAttributedText
         
         if selectedPromoIndex == indexPath.row{
            cell?.iconImage.isHidden = false
         }else{
            cell?.iconImage.isHidden = true
         }
         
         cell?.setIconToGreenTick(shouldSet: !isOpenedFromSideMenu)
         
         if isOpenedFromSideMenu == true  {
            cell?.iconImage.image = #imageLiteral(resourceName: "info")
            cell?.iconImage.isHidden = false
         }
         
         return cell!
         
      }else{
         
         let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.addNewPromo, for: indexPath) as? AddNewPromoCodeTableViewCell
         cell?.topLabel.text = TEXT.addACouponcode
         return cell!
         
      }
   }
   
   func getCardTableViewCellFor(indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.cardCell, for: indexPath) as? CardsTableViewCell
      if FormDetails.currentForm.hasCashAsAPaymentMethod() && indexPath.row == cardsArray.count {
         cell?.bottomLineDevider.isHidden = true
      } else if indexPath.row == cardsArray.count - 1 {
         cell?.bottomLineDevider.isHidden = true
      }
      if FormDetails.currentForm.paymentMethodArray.contains(8) == true  {
         if cardsArray.count == 0 || indexPath.row == 0 {
            if FormDetails.currentForm.paymentMethodArray.contains(8) == true{
               cell?.cardNumber.text = TEXT.PayByCash
               cell?.leftImage.image = iconCash!
            }
         }else{
            if indexPath.row == 0{
               cell?.cardNumber.text = TEXT.PayByCash
               cell?.leftImage.image =  iconCash!
            }else{
               cell?.cardNumber.text = "**** \(cardsArray[indexPath.row - 1].lastFourDigit)"
               cell?.leftImage.image = sampleCard
            }
         }
         if selectedCardIndex == indexPath.row{
            cell?.isSelectedIcon.image = greenTick!
         }else{
            if self.isOpenedFromSideMenu == false       {
               cell?.isSelectedIcon.image = optionEmpty
            }else{
               cell?.isSelectedIcon.isHidden = true
            }
         }
         
         cell?.cardNumber.letterSpacing =  0.5
         return cell!
      }else{
         cell?.cardNumber.text = "**** \(cardsArray[indexPath.row].lastFourDigit)"
         cell?.leftImage.image = sampleCard!
         
         if selectedCardIndex == indexPath.row{
            if self.isOpenedFromSideMenu == false       {
               cell?.isSelectedIcon.image = greenTick!
            }else{
               cell?.isSelectedIcon.isHidden = true
            }
         }else{
            if self.isOpenedFromSideMenu == false       {
               cell?.isSelectedIcon.image = optionEmpty!
            }else{
               cell?.isSelectedIcon.isHidden = true
            }
         }
         
         cell?.cardNumber.letterSpacing =  0.5
         return cell!
      }
   }
   
   //MARK: - TABLEVIEW DELEGATE FUNCTIONS
   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      
      guard section < headerViewArray.count else {
         return nil
      }
      
      switch headerViewArray[section] {
      case .cards:
         return getCardSectionHeader()
      case .billOrEstimation:
         return getBillEstimationSectionHeader()
      case .promo:
         return getPromoSectionHeader()
      case .tip:
         return getTipSectionHeader()
      }
      
   }
   
   func getTipSectionHeader() -> UIView {
      
      let headerView = sectionHeaderNib.instantiate(withOwner: self, options: nil)[0] as! PaymentHeaderView
      headerView.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 50)
      headerView.icon.image = iconTip!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
      headerView.icon.tintColor = COLOR.SPLASH_TEXT_COLOR
      headerView.paymentTypeName.text = tipHeading
      headerView.paymentTypeName.letterSpacing = 0.4
      
      let rightButtonTitle = getTipRightButtonTitle()
      headerView.setRightButtonWithThemeTextColor(title: rightButtonTitle)
      
      headerView.didTapHeaderButton.addTarget(self, action:  #selector(self.tipSectionHeaderTapped(sender:)), for: UIControlEvents.touchUpInside)
      headerView.rightbutton.isHidden = false
      
      
      return headerView
   }
   
   func getTipRightButtonTitle() -> String {
      switch (isTipSelected, showTipCell) {
      case (false, false):
         return TEXT.addString
      case (true, false):
         return getTipDescription()
      default:
         return ""
      }
   }
   
   func getTipDescription() -> String {
      let currency = FormDetails.currentForm.currencyid
      let tipValue = tipDictionary[selectedTag] ?? "0"
      
      if FormDetails.currentForm.tipDetails.type == .amount {
         return currency + " " + tipValue
      } else {
         return tipValue + "%"
      }
   }
   
   func getPromoSectionHeader() -> UIView {
      let headerView = sectionHeaderNib.instantiate(withOwner: self, options: nil)[0] as! PaymentHeaderView
      
      headerView.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 50)
      
      headerView.icon.image = iconPromo!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
      headerView.icon.tintColor = COLOR.SPLASH_TEXT_COLOR
      
      
      
      headerView.paymentTypeName.text = TEXT.ApplyPromocode
      headerView.paymentTypeName.letterSpacing = 0.4
      
      
      headerView.rightbutton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)
      
      let rightButtontitle = getTitleOfRightButtonInPromoSectionHeader()
      
      headerView.rightbutton.setTitle(rightButtontitle, for: .normal)
      headerView.didTapHeaderButton.addTarget(self, action:  #selector(self.promoSectionHeaderTapped(sender:)), for: UIControlEvents.touchUpInside)
      
      if shouldSetPromoSectionHeaderWithBorder() {
         headerView.setRightButtonWithBordersWith(color: COLOR.paymentGreenColor)
      } else {
         headerView.setRightButtonWithThemeTextColor(title: rightButtontitle)
      }
      
      if showloaderForPromo {
         headerView.startLoaderAnimation()
      } else {
         headerView.stopLoaderAnimation()
      }
      

      headerView.rightbutton.isHidden = showloaderForPromo || rightButtontitle.isEmpty

      
      return headerView
   }
   
   func getTitleOfRightButtonInPromoSectionHeader() -> String {
      switch (showPromo, isPromoSelected) {
      case (false, false):
         return TEXT.addString
      case (false, true):
         return " Applied "
      default:
         return ""
      }
   }
   
   func shouldSetPromoSectionHeaderWithBorder() -> Bool {
      return !showPromo && isPromoSelected
   }
   
   func getBillEstimationSectionHeader() -> UIView {
      let headerView = sectionHeaderNib.instantiate(withOwner: self, options: nil)[0] as! PaymentHeaderView
      
      headerView.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 50)
      
      headerView.icon.image = iconBill!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
      headerView.icon.tintColor = COLOR.SPLASH_TEXT_COLOR
      
      headerView.paymentTypeName.text = getTitleOfBillSectionHeader()
      headerView.paymentTypeName.letterSpacing = 0.4
      
      
      headerView.didTapHeaderButton.addTarget(self, action: #selector(NewPaymentScreenViewController.billSectionHeaderTapped(sender:)), for: UIControlEvents.touchUpInside)
      
      let rightButtonTitle = getRightButtonTitleInBillSectionHeader()
      headerView.setRightButtonWithThemeTextColor(title: rightButtonTitle)

      if self.showLoaderForBill {
         headerView.startLoaderAnimation()
      }else{
         headerView.stopLoaderAnimation()
      }
      headerView.rightbutton.isHidden = showLoaderForBill

      return headerView
   }
   
   func getTitleOfBillSectionHeader() -> String {
      if FormDetails.currentForm.verticalType == .taxi {
         return isDeliveryAdded ? "Estimated Fare" : "Fare Estimate"
      }else{
         return "Bill Summary"
      }
   }
   
   func getRightButtonTitleInBillSectionHeader() -> String {
      guard FormDetails.currentForm.verticalType != .taxi else {
         return isDeliveryAdded ? (amount ?? "0") : TEXT.AddDropLocation
      }
      
      return showBillBreakUp ? "" : (amount ?? "0")
   }
   
   func getCardSectionHeader() -> UIView {
      let headerView = sectionHeaderNib.instantiate(withOwner: self, options: nil)[0] as! PaymentHeaderView
      headerView.rightbutton.isHidden = true
      headerView.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 50)
      headerView.icon.image = iconPayments!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
      headerView.icon.tintColor = COLOR.SPLASH_TEXT_COLOR
      headerView.paymentTypeName.text = TEXT.PaymentOptions
      headerView.paymentTypeName.letterSpacing = 0.4
      if self.showLoaderForCards == true  {
         headerView.loader.startAnimating()
         headerView.loader.isHidden = false
      }else{
         headerView.loader.stopAnimating()
         headerView.loader.isHidden = true
      }
      return headerView
   }
   
   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      return 50
   }
   
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
      if headerViewArray[indexPath.section] == .cards {
         if isOpenedFromSideMenu == false    {
            let previousCell  = tableView.cellForRow(at: IndexPath(item: selectedCardIndex, section: indexPath.section)) as? CardsTableViewCell
            previousCell?.isSelectedIcon.image = optionEmpty
            selectedCardIndex = indexPath.row
            let currentIndex = tableView.cellForRow(at: IndexPath(item: selectedCardIndex, section: indexPath.section)) as? CardsTableViewCell
            currentIndex?.isSelectedIcon.image = greenTick
         }else{
            if indexPath.row != 0 && FormDetails.currentForm.paymentMethodArray.contains(8) == true  {
               let vc = self.storyboard?.instantiateViewController(withIdentifier: "viewCard") as? ViewCardViewcontroller
               vc?.cardData = cardsArray[indexPath.row - 1]
               vc?.completionHandler = {
                  self.apiHitToGetAllCards()
               }
               self.navigationController?.pushViewController(vc!, animated: true)
            }else if  FormDetails.currentForm.paymentMethodArray.contains(8) == false    {
               let vc = self.storyboard?.instantiateViewController(withIdentifier: "viewCard") as? ViewCardViewcontroller
               vc?.cardData = cardsArray[indexPath.row]
               vc?.completionHandler = {
                  self.apiHitToGetAllCards()
               }
               self.navigationController?.pushViewController(vc!, animated: true)
            }
         }
      } else if headerViewArray[indexPath.section] == .promo {
         
         guard !showLoaderForBill else {
            self.showErrorMessage(error: "Please Wait", isError: false)
            return
         }
         
         if indexPath.row < Singleton.sharedInstance.couponsArray.count {
            if isOpenedFromSideMenu == false    {
               let selectedData = Singleton.sharedInstance.couponsArray[indexPath.row]
               
               if indexPath.row == selectedPromoIndex {
                  selectedPromoIndex = 200
                  showPromo = true

                  
                  self.tableView.reloadData()
                  DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                     
                     
                     if FormDetails.currentForm.verticalType != .taxi {
                        
                        self.apiHitToGetBillBreakDown(promo_value: "", benefit_type: "")
                        
                     }else{
                        
                        if self.isDeliveryAdded == true   {
                           self.getEstimatedCost()
                        }
                        
                     }
                     
                  })
               }else{
                  
                  
                  selectedPromoIndex = indexPath.row
                  showPromo = false

                  self.tableView.reloadData()
                  
                  DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                     if FormDetails.currentForm.verticalType != .taxi {
                        
                        self.apiHitToGetBillBreakDown(promo_value: selectedData.promoValue, benefit_type: selectedData.benefitType)
                        
                     }else{
                        
                        if self.isDeliveryAdded == true   {
                           self.getEstimatedCost()
                        }
                        
                     }
                  })
               }
            }else{
               
               let selectedPromo = Singleton.sharedInstance.couponsArray[indexPath.row]
               let dateFont = NSMutableAttributedString(string: "Valid till - " + selectedPromo.expiryDatetimeUtc , attributes: [NSFontAttributeName:UIFont(name: FONT.light, size: 12)]) //NSAttributedString(string: selectedPromo.expiryDatetimeUtc)
               
               self.tableView.isUserInteractionEnabled = false
               DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
                  self.tableView.isUserInteractionEnabled = true
               })
               InfoPopUpView.loadWith(info: InfoPopUpView.InfoPopUp(heading: selectedPromo.description, subHeading: dateFont, description: selectedPromo.details, image: nil, closeButtontitle: "Close", type: InfoPopUpView.PopUpType.normal))
               
            }
         } else {
            addPromoCodeButtonPressed()
         }
      }
   }
   
   func addPromoCodeButtonPressed() {
      
      let popUpParams = ChangePhoneNumberView.PopupParams(heading: "", textFieldPlaceholder: "Promo Code", textFieldtext: "", removeAlertButtonTitle: TEXT.CANCEL, saveButtonAlertTitle: "Add")
      var addPromoView: ChangePhoneNumberView?
      addPromoView = ChangePhoneNumberView.loadWithSingleTextField(texts: popUpParams, keyboardType: .default, capitalization: .allCharacters, newTextSaved: { [weak self] (promoCode) in
         self?.promoCodeEnteredIn(view: addPromoView!, promoCode: promoCode)
      })
      
   }
   
   
   func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
      
      if headerViewArray[section] == .cards{
         if FormDetails.currentForm.paymentMethodArray.count == 1 &&  FormDetails.currentForm.paymentMethodArray.contains(8) == true{
            return nil
         }
         let footerView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 35))
         let button = UIButton(frame: CGRect(x: 29, y: 0, width:SCREEN_SIZE.width , height: 35))
         button.setTitle(TEXT.AddAnotherMethod, for: UIControlState.normal)
         button.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: UIControlState.normal)
         button.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)
         button.titleLabel?.letterSpacing = 0.4
         button.addTarget(self, action: #selector(NewPaymentScreenViewController.addCard), for: UIControlEvents.touchUpInside)
         button.contentHorizontalAlignment = .left
         button.clipsToBounds = true
         
         footerView.addSubview(button)
         return footerView
      }else if headerViewArray[section] == .tip{
         if showTipCell == true  {
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 35))
            let button = UIButton(frame: CGRect(x: 29, y: 0, width:SCREEN_SIZE.width , height: 35))
            
            
            if FormDetails.currentForm.tipDetails.type == .amount {
               button.setTitle("Enter tip manually", for: UIControlState.normal)
            }else{
               button.setTitle("Enter tip manually", for: UIControlState.normal)
            }
            button.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: UIControlState.normal)
            button.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)
            button.titleLabel?.letterSpacing = 0.4
            button.addTarget(self, action: #selector(NewPaymentScreenViewController.showCustomTipPoUp), for: UIControlEvents.touchUpInside)
            button.contentHorizontalAlignment = .left
            button.clipsToBounds = true
            footerView.addSubview(button)
            return footerView
         }else{
            return nil
         }
      }else{
         return nil
      }
   }
   
   func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
      if headerViewArray[section] == .cards{
         if FormDetails.currentForm.paymentMethodArray.count == 1 &&  FormDetails.currentForm.paymentMethodArray.contains(8) == true   {
            return 0
            
         }
         
         return 35
         
      }else if headerViewArray[section] == .tip{
         if showTipCell == true    {
            return 35
         }else{
            return 0
         }
      }else{
         return 0
      }
   }
   
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      if headerViewArray[indexPath.section] == .cards{
         return 54
      }else if headerViewArray[indexPath.section] == .promo{
         
         return UITableViewAutomaticDimension
         
      } else if headerViewArray[indexPath.section] == .billOrEstimation {
         if indexPath.row != billNameArray.count - 1{
            return 35
         }else{
            return 50
         }
         
      }else{
         return UITableViewAutomaticDimension
      }
   }
   
   // MARK: -
   func apiHitToGetAllCards() {
      
      var paymentMethodToSend = Int()
      for paymentMethod in FormDetails.currentForm.paymentMethodArray {
         if !isPaymentMethodCash(paymentMethod: paymentMethod) {
            paymentMethodToSend = paymentMethod
            break
         }
      }
      
      let param = [
         "access_token" : Vendor.current!.appAccessToken!,
         "app_device_type" : APP_DEVICE_TYPE,
         "payment_method": "\(paymentMethodToSend)",
        "form_id":FormDetails.currentForm.form_id!,
        "user_id":"\(FormDetails.currentForm.user_id!)",
        "api_key" :FormDetails.currentForm.apiKey,
        "reference_id": Merchant.shared!.vedorRefferenceId,
        
         ] as [String : String]
      
      
      self.showLoaderForCards = true
      self.tableView.reloadData()
      
      NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.fetchCards, params: param as [String : AnyObject]?, httpMethod: "POST") { (isSuccess, data) in
         print(data)
         self.showLoaderForCards = false
         if isSuccess == true{
            if let dataObject = data["data"] as? [Any]{
               
               var newCardArray = [cardsModel]()
               
               if dataObject.count > 0 {
                  for i in dataObject {
                     newCardArray.append(cardsModel(json: i as! [String : Any]))
                  }
                  self.cardsArray = newCardArray
                  
               }
               
               
               if self.selectedCardIndex == 200 {
                  self.selectedCardIndex = 0
               }
               
               
               self.tableView.reloadData()
            }else{
               // self.addButtonAction(UIButton())
            }
         }else{
            self.tableView.reloadData()
            if let message = data["message"] as? String{
               if message == "No data found."{
                  if let dataObject = data["data"] as? [Any]{
                     print(dataObject)
                     self.cardsArray.removeAll()
                     if dataObject.count > 0 {
                        for i in dataObject {
                           self.cardsArray.append(cardsModel(json: i as! [String : Any]))
                        }
                        
                     }
                     //                            self.tableView.reloadSections([self.cardsLocation], with: UITableViewRowAnimation.none)
                     self.tableView.reloadData()
                  }
               }else{
                  //                        self.showErrorMessage(error:  data["message"] as! String!, isError: true    )
                  ErrorView.showWith(message: data["message"] as! String!, isErrorMessage: true, removed: nil)
                  _ = self.navigationController?.popViewController(animated: true)
               }
            }
            
            
         }
      }
   }
   
   private func isPaymentMethodCash(paymentMethod: Int) -> Bool {
      return paymentMethod == 8
   }
   
   func showErrorMessage(error:String,isError:Bool) {
      ErrorView.showWith(message: error, isErrorMessage: isError, removed: nil)
   }
   
   func addCard(){
      if IJReachability.isConnectedToNetwork()  == true{
         let vc = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.addCardWeb) as? AddCardWebViewViewController
         vc?.completionHandler = {
            self.apiHitToGetAllCards()
         }
         self.present(vc!, animated: true, completion: nil)
      }
      else{
         self.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: true)
      }
   }
   
   
   
   @IBAction func payButtonAction(_ sender: UIButton) {
      
      var promoId = ""
      var accessId = ""
      if self.tipDictionary[self.selectedTag]! != "0"{
         if FormDetails.currentForm.tipDetails.type == .amount {
         }else{
         }
      }
      
      if self.selectedPromoIndex < Singleton.sharedInstance.couponsArray.count{
         if Singleton.sharedInstance.couponsArray[self.selectedPromoIndex].accessId != ""{
            accessId = Singleton.sharedInstance.couponsArray[self.selectedPromoIndex].accessId
         }else{
            promoId = Singleton.sharedInstance.couponsArray[self.selectedPromoIndex].promoId
         }
      }
      
      Singleton.sharedInstance.promoId = promoId
      Singleton.sharedInstance.promoAccessId = accessId
      
      
      var cardMethod = Int()
      for paymentMethod in FormDetails.currentForm.paymentMethodArray {
         if !isPaymentMethodCash(paymentMethod: paymentMethod) {
            cardMethod = paymentMethod
            break
         }
      }
      
      
      if selectedCardIndex != 200 {
         
         if self.forPending == false{
//            let endIndex = FormDetails.currentForm.call_tasks_as.index(FormDetails.currentForm.call_tasks_as.endIndex, offsetBy: -1)
//            let calTaskAs = FormDetails.currentForm.call_tasks_as[FormDetails.currentForm.call_tasks_as.index(before: FormDetails.currentForm.call_tasks_as.endIndex)] == "s" ? FormDetails.currentForm.call_tasks_as.substring(to: endIndex).lowercased() :  FormDetails.currentForm.call_tasks_as
            Singleton.sharedInstance.showAlertWithOption(owner: self, title: TEXT.PLACE_THIS_ORDER, message: "", showRight: true, leftButtonAction: {
               print("cancel")
            }, rightButtonAction: {
               if FormDetails.currentForm.verticalType != .taxi {
                  if self.selectedCardIndex != 0 && FormDetails.currentForm.paymentMethodArray.contains(8) == true  {
                     self.afterSelecting!(self.cardsArray[self.selectedCardIndex-1].id,"\(cardMethod)","\(self.tipDictionary[self.selectedTag]!)")
                  } else if self.selectedCardIndex == 0 && FormDetails.currentForm.paymentMethodArray.contains(8) == true  {
                     self.afterSelecting!("","8","\(self.tipDictionary[self.selectedTag]!)")
                } else {
                     self.afterSelecting!(self.cardsArray[self.selectedCardIndex].id,"\(cardMethod)","\(self.tipDictionary[self.selectedTag]!)")
                    
                  }
               } else {
                  var dateString = String()
                  
                  if self.isNow == true{
                     let dateArray = "\(Date())".components(separatedBy: " ")
                     dateString = ("\(dateArray[0]) " + "\(dateArray[1])").convertDateFromUTCtoLocal(input: "yyyy-MM-dd HH:mm:ss", output: "yyyy-MM-dd HH:mm:ss")
                  }else{
                     let dateFormatter = DateFormatter()
                     dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
                     dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
                     self.dateInDateFormat = dateFormatter.date(from: self.dateInString)!
                     
                     let difference =  self.dateInDateFormat.timeIntervalSince(Date())
                     print(self.dateInDateFormat)
                     print(Int(difference/60.0))
                     print( FormDetails.currentForm.scheduleOffsetTime)
                     if Int(difference/60.0) < (FormDetails.currentForm.scheduleOffsetTime/60){
                        _ = self.navigationController?.popViewController(animated: true)
                        ErrorView.showWith(message: TEXT.scheduleErrorMessage, removed: nil)
                        return
                        
                     }
                     dateString = ("\(self.dateInString)").convertDateFromUTCtoLocal(input: "dd MMM yyyy hh:mm a", output: "yyyy-MM-dd HH:mm:ss",toConvert:false)
                  }
                  print(dateString)
                  
                  
                  if self.selectedCardIndex != 0 && FormDetails.currentForm.paymentMethodArray.contains(8) == true{
                     
                     
                     print(FormDetails.currentForm.paymentMethodArray)
                     
                     
                     FormDetails.currentForm.payementMethod = cardMethod
                     
                     self.makeMetaData(paymentMethod: cardMethod)
                     self.delegate?.updateMetaData(data: self.pickUpMetaData, date: dateString, currency: self.currencyId, paymentMethod: "\(cardMethod)", cardId: self.cardsArray[self.selectedCardIndex-1].id, ammount: "",isNow: self.isNow,tip:"\(self.tipDictionary[self.selectedTag]!)")
                  }else if self.selectedCardIndex == 0 && FormDetails.currentForm.paymentMethodArray.contains(8) == true{
                     FormDetails.currentForm.payementMethod = 8
                     self.makeMetaData(paymentMethod: 8)
                    
                     self.delegate?.updateMetaData(data: self.pickUpMetaData, date: dateString, currency: self.currencyId, paymentMethod: "\(8)", cardId: "", ammount: "",isNow: self.isNow,tip:"\(self.tipDictionary[self.selectedTag]!)")
                  }else{
                     
                     FormDetails.currentForm.payementMethod = cardMethod
                     self.makeMetaData(paymentMethod: cardMethod)
                     self.delegate?.updateMetaData(data: self.pickUpMetaData, date: dateString, currency: self.currencyId, paymentMethod: "\(cardMethod)", cardId: self.cardsArray[self.selectedCardIndex].id, ammount: "",isNow: self.isNow,tip:"\(self.tipDictionary[self.selectedTag]!)")
                  }
                  _ = self.navigationController?.popViewController(animated: true)
               }
               
            }, leftButtonTitle: TEXT.NO.capitalized, rightButtonTitle: TEXT.YES_ACTION.capitalized)
            
         }else{
            
            //            self.apiHitForPendingPayment(cardId: self.cardsArray[indexPath.row].id, paymentMethod: "\(FormDetails.currentForm.paymentMethodArray[0])")
            
         }
      }else{
         self.showErrorMessage(error: "Please select a payment method", isError: true)
      }
      
   }
   
   
   func billSectionHeaderTapped(sender:UIButton){
      if FormDetails.currentForm.verticalType == .taxi {
         let vc: AddNewLocationViewController?
         vc = UIViewController.findIn(storyboard: .favLocation, withIdentifier: STORYBOARD_ID.addLocation) as? AddNewLocationViewController
         vc?.completionHandler = {(coordinate,address) in
//            self.updateMap(coordinate: coordinate, address: address)
         }
         if IJReachability.isConnectedToNetwork() == true{
            self.navigationController?.pushViewController(vc!, animated: true)
         }else{
            ErrorView.showWith(message: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isErrorMessage: true, removed: nil)
         }
         
      } else {
            showBillBreakUp = !showBillBreakUp
            self.tableView.reloadData()
      }
   }
   
   
   func createSuperArray(){
      creditsLabel.text =   " " + Vendor.current!.getCreditsDescription() + "   "

      creditsLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
      creditsLabel.textColor = COLOR.SPLASH_TEXT_COLOR
      creditsLabel.letterSpacing = 0.4
      creditsLabel.textAlignment = .center
      creditsLabel.layer.borderWidth = 1
      creditsLabel.layer.borderColor = COLOR.SPLASH_TEXT_COLOR.cgColor
      creditsLabel.layer.cornerRadius = CGFloat(2)
      creditsLabel.clipsToBounds = true
      
      
      
      if isOpenedFromSideMenu == false{
         creditsLabel.isHidden = true
         if FormDetails.currentForm.tipDetails.isEnabled && FormDetails.currentForm.tipDetails.location == .paymentScreen {
            headerViewArray = [.tip,.billOrEstimation,.cards]
         } else {
            headerViewArray = [.billOrEstimation,.cards]
         }
         
         
         if AppConfiguration.current.isPromoRequired == "1"{
            var newArray = [tableViewHeaderType.promo]
            newArray.append(contentsOf: headerViewArray)
            headerViewArray = newArray
            
         }
         bottomButtonHeight.constant = 50
         
         //       headerViewArray.append(.tip)
         print(headerViewArray)
         self.tableView.reloadData()
      }else{
         if AppConfiguration.current.isPromoRequired == "1"  && AppConfiguration.current.isReferralRequired {
            creditsLabel.isHidden = false
         }else{
            creditsLabel.isHidden = true
         }
         headerViewArray = [.cards]
         if AppConfiguration.current.isPromoRequired == "1"{
            headerViewArray.append(tableViewHeaderType.promo)
            payButton.isHidden = true
         }
         bottomButtonHeight.constant = 0
         self.tableView.reloadData()
      }
   }
   
   
   
   enum tableViewHeaderType {
      case cards
      case billOrEstimation
      case promo
      case tip
   }
   
   
   func getEstimatedCost() {
      makeMetaData()
      
      var benifitType = String()
      var value = String()
      
      if selectedPromoIndex < Singleton.sharedInstance.couponsArray.count{
         let selectedData = Singleton.sharedInstance.couponsArray[selectedPromoIndex]
         benifitType = selectedData.benefitType
         value       = selectedData.promoValue
      }

      let param = [
         "access_token":"\(Vendor.current!.appAccessToken!)",
//         "device_token":"\(APIManager.sharedInstance.getDeviceToken())",
         "app_version":"\(APP_VERSION)",
         "app_device_type":APP_DEVICE_TYPE,
         "app_access_token":"\(Vendor.current!.appAccessToken!)",
         "pickup_latitude":"\(selectedLat)",
         "pickup_longitude":"\(selectedLong)",
         "user_id":"\(FormDetails.currentForm.user_id!)",
         "pickup_meta_data":self.pickUpMetaData,
         "vendor_id":"\(Vendor.current!.id!)",
         "payment_method":"\(FormDetails.currentForm.paymentMethodArray[0])",
         "layout_type":"\(FormDetails.currentForm.work_flow!)",
         "has_pickup":"1",
         "has_delivery":"0",
         "benefit_type":benifitType,
         "promo_value":value,
         "tip_amount" : "\(tipDictionary[selectedTag]!)",
         "tip_type" : FormDetails.currentForm.tipDetails.type.rawValue,
         "form_id":FormDetails.currentForm.form_id!,
        "api_key" :FormDetails.currentForm.apiKey,
        "reference_id": Merchant.shared!.vedorRefferenceId,
         ] as [String : Any]
      
      print(param)
      
      self.showLoaderForBill = true
      self.tableView.reloadData()
      NetworkingHelper.sharedInstance.commonServerCall(apiName: "fare_estimate", params: param as [String : AnyObject]?, httpMethod: "POST", receivedResponse: { (isSuccess, response) in
         
         self.showLoaderForBill = false
         self.tableView.reloadData()
         
         if isSuccess == true    {
            if let data = response["data"] as? [String:Any]{
               if let value = data["currency"] as? [String:Any]{
                  guard let currencyValue = Currency(json: value) else {
                     return
                  }
                  if let totalCost = data["fare_lower_limit"] as? NSNumber{
                     if let upperCost = data["fare_upper_limit"] as? NSNumber{
                        self.amount = "\(currencyValue.symbol) \(totalCost.intValue) - \(currencyValue.symbol) \(upperCost.intValue)"
                        self.isDeliveryAdded = true
                        self.tableView.reloadData()
                     }
                  }
               }
            }
         }else{
            if let message = response["message"] as? String{
               ErrorView.showWith(message: message, isErrorMessage: true, removed: nil)
            }
         }
         print(response)
      })
   }
   
   
   func makeMetaData(paymentMethod : Int = FormDetails.currentForm.payementMethod )  {
      var pickUpMentaData = [[String:String]]()
      for i in Singleton.sharedInstance.createTaskDetail.pickupMetadata {
         if let data  = i as? CustomFieldDetails {
            pickupCustomFieldTemplate = data.template_id!
            print(selected)
            switch data.label! {
            case "baseFare":
               pickUpMentaData.append([
                  "label":data.label!,
                  "data":Singleton.sharedInstance.typeCategories[selected].baseFare
                  ])
            case "destinationAddress":
               pickUpMentaData.append(["label":data.label!,"data":selectedDestinationAddress])
            case "destinationLatitude":
               pickUpMentaData.append(["label":data.label!,"data":"\(selectedDropLat == Double() ? "" : "\(selectedDropLat)")"])
            case "destinationLongitude":
               pickUpMentaData.append(["label":data.label!,"data":"\(selectedDropLong == Double() ? "" : "\(selectedDropLong)")"])
            case "categoryId":
               pickUpMentaData.append(["label":data.label!,"data":"\(Singleton.sharedInstance.typeCategories[selected].categoryId )"])
            case "paymentMode":
               pickUpMentaData.append(["label":data.label!,"data":"\(paymentTypeDict["\(paymentMethod)"]!)"])
            case "paymentMethod":
               pickUpMentaData.append(["label":data.label!,"data":"\(paymentMethod)"])
            case "cancellationCharges":
               pickUpMentaData.append(["label":data.label!,"data":"\(Singleton.sharedInstance.typeCategories[selected].cancellation_charges)"])
            case "distanceFare" :
               
               pickUpMentaData.append(["label":data.label!,"data":"\(Singleton.sharedInstance.typeCategories[selected].perKilometerCharges)"])
               
            case "timeFare" :
               
               pickUpMentaData.append(["label":data.label!,"data":"\(Singleton.sharedInstance.typeCategories[selected].perMinuteCharges)"])
               
               
            default:
               pickUpMentaData.append(["label":data.label!,"data":data.data!])
            }
            
         }
      }
      print(pickUpMentaData)
      print(Singleton.sharedInstance.typeCategories[selected])
      self.pickUpMetaData = pickUpMentaData
   }
   
   func promoSectionHeaderTapped(sender: UIButton) {
      showPromo = !showPromo
      UIView.animate(withDuration: 1, animations: {
         self.tableView.reloadData()
      })
   }
   
   func showCustomTipPoUp(){
      var customTipView: ChangePhoneNumberView?
      customTipView  =  ChangePhoneNumberView.loadWithSingleTextField(texts: ChangePhoneNumberView.PopupParams.init(heading: "", textFieldPlaceholder: "Enter tip manually", textFieldtext: "", removeAlertButtonTitle: "Cancel", saveButtonAlertTitle: "Add"), keyboardType: .decimalPad, newTextSaved:  { (newTip) in
         print(newTip)
         if Double(newTip) != nil{
            if Double(newTip) == 0.0{
               ErrorView.showWith(message: "Please add a valid amount.", isErrorMessage: true, removed: nil)
               return
            }
         }else{
            ErrorView.showWith(message: "Please add a valid amount.", isErrorMessage: true, removed: nil)
            return
         }
         self.tipDictionary[4] = newTip
         self.selectedTag = 4
         self.tableView.reloadData()
         customTipView?.removeByAnimating()
         self.updateBilBreakDownOrAppEstimate()
      })
   }
   
   
   func tipSectionHeaderTapped(sender: UIButton) {
      showTipCell = !showTipCell
      
      UIView.animate(withDuration: 1, animations: {
         self.tableView.reloadData()
      })
   }
   
   func apiHitToGetBillBreakDown(promo_value:String, benefit_type: String){
      
      let param = [
         "access_token" : Vendor.current!.appAccessToken!,
//         "device_token": APIManager.sharedInstance.getDeviceToken(),
         "app_version" : APP_VERSION,
         "app_device_type" : APP_DEVICE_TYPE,
         "user_id" : Vendor.current!.userId!,
         "app_access_token":Vendor.current!.appAccessToken!,
         "amount":initialAmmount,
         "benefit_type":benefit_type,
         "promo_value":promo_value,
         "tip_amount" : "\(tipDictionary[selectedTag]!)",
         "tip_type" : FormDetails.currentForm.tipDetails.type.rawValue,
         "api_key" :FormDetails.currentForm.apiKey,
         "reference_id": Merchant.shared!.vedorRefferenceId,
         "form_id":FormDetails.currentForm.form_id!
         ] as [String : Any]
      
      self.amount = ""
      self.showLoaderForBill = true
      //        self.tableView.reloadSections([billLocationInt], with: UITableViewRowAnimation.none)
      self.tableView.reloadData()
      NetworkingHelper.sharedInstance.commonServerCall(apiName: "get_bill_breakdown", params: param as [String : AnyObject]?, httpMethod: "POST") { (IsSuccess, Response) in
         
         
         
         if IsSuccess == true {
            if let data = Response["data"] as? [String:Any]{
               let billDetails = BillModal(json: data)
               self.billNameArray = billDetails.getDataArray().nameArray
               self.billValueArray = billDetails.getDataArray().valueArray
               self.tableView.reloadData()
               self.amount = FormDetails.currentForm.currencyid + billDetails.netPaybleAmount
               
               if FormDetails.currentForm.verticalType == .taxi {
                  self.payButton.setTitle(TEXT.BookText, for: UIControlState.normal)
                  self.payButton.titleLabel?.letterSpacing = 1
                  
               } else {
                  if self.amount! != FormDetails.currentForm.currencyid + "0" && self.amount! != FormDetails.currentForm.currencyid + "0.0" {
                     
                     self.payButton.setTitle("Pay \(self.amount!)", for: UIControlState.normal)
                  }else{
                     self.payButton.setTitle("Continue for free", for: UIControlState.normal)
                  }
                  self.payButton.titleLabel?.letterSpacing = 1
               }
               
               self.showLoaderForBill = false
               self.tableView.reloadData()
            }
            
         } else {
            
            if let message = Response["message"]  as? String{
               
               ErrorView.showWith(message: message, isErrorMessage: true, removed: nil)
               _ = self.navigationController?.popViewController(animated: true)
            }
            
            self.showLoaderForBill = false
            self.tableView.reloadData()
         }
      }
   }
   
   // MARK: - Promo Code
   func promoCodeEnteredIn(view: ChangePhoneNumberView, promoCode: String) {
      
      guard !promoCode.isEmpty else {
         self.showErrorMessage(error: TEXT.pleaseAddAPromoCode, isError: true)
         return
      }
      
      view.phoneNumberField.resignFirstResponder()
      
      let param = getParamsToSendPromoCodeToServer(promoCode: promoCode)
      
      
      HTTPClient.makeConcurrentConnectionWith(method: .POST, para: param, extendedUrl: API_NAME.assignPromo) { [weak self] (responseObject, error, _, statusCode) in
         
         guard error == nil,
            statusCode == STATUS_CODES.SHOW_DATA,
            let response = responseObject as? [String: Any],
            let data = response["data"] as? [String: Any] else {
               return
         }
         
         Singleton.sharedInstance.getPromoCodeArray(data: data)
         
         guard self?.isOpenedFromSideMenu == false else {
            self?.tableView.reloadData()
            view.removeByAnimating()
            return
         }
         
         self?.selectPromoWith(code: promoCode)
         self?.newPromoCodeSelected()
         view.removeByAnimating()
      }
   }
   
   
   func getParamsToSendPromoCodeToServer(promoCode: String) -> [String: Any] {
      return [
         "access_token" : Vendor.current!.appAccessToken!,
//         "device_token" : APIManager.sharedInstance.getDeviceToken(),
         "app_version" : APP_VERSION,
         "app_device_type" : APP_DEVICE_TYPE,
         "user_id" : FormDetails.currentForm.user_id!,
         "app_access_token" : Vendor.current!.appAccessToken!,
         "promo_code" : promoCode,
         "form_id": FormDetails.currentForm.form_id!,
         "api_key" :FormDetails.currentForm.apiKey,
         "reference_id": Merchant.shared!.vedorRefferenceId,

         ] as [String : Any]
   }
   
   
   func selectPromoWith(code: String) {
      if Singleton.sharedInstance.couponsArray.count != 0 {
         for i in 0...Singleton.sharedInstance.couponsArray.count-1 {
            if Singleton.sharedInstance.couponsArray[i].promoCode == code {
               selectedPromoIndex = i
            }
         }
      }
   }
   
   func newPromoCodeSelected() {
      
      guard self.selectedPromoIndex < Singleton.sharedInstance.couponsArray.count else {
         return
      }
      
      let selectedPromoCode = Singleton.sharedInstance.couponsArray[self.selectedPromoIndex]
      
      self.showPromo = true
      
      DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
         
         if FormDetails.currentForm.verticalType != .taxi {
            self.apiHitToGetBillBreakDown(promo_value: selectedPromoCode.promoValue, benefit_type: selectedPromoCode.benefitType)
         } else if self.isDeliveryAdded == true {
            self.getEstimatedCost()
         }
      })
   }
   
   func getPromo() {
      
    let param: [String: Any] = [
         "access_token" : Vendor.current!.appAccessToken!,
//         "device_token" : APIManager.sharedInstance.getDeviceToken(),
         "app_version" : APP_VERSION,
         "app_device_type" : APP_DEVICE_TYPE,
         "user_id" :FormDetails.currentForm.user_id!,
         "app_access_token" : Vendor.current!.appAccessToken!,
         "form_id":FormDetails.currentForm.form_id!,
         "api_key" :FormDetails.currentForm.apiKey,
         "reference_id": Merchant.shared!.vedorRefferenceId
         ] 
      
      showloaderForPromo = true
      self.tableView.reloadData()
      
      NetworkingHelper.sharedInstance.commonServerCall(apiName: "get_customer_promos", params: param as [String : AnyObject]?, httpMethod: "POST") { (isSucess, Response) in
         self.showloaderForPromo = false
         self.tableView.reloadData()
         if isSucess == true {
            print(Response)
            if let data = Response["data"] as? [String:Any]{
               Singleton.sharedInstance.getPromoCodeArray(data: data)
            }
            self.tableView.reloadData()
            
         }else{
            if let message = Response["message"] as? String{
               ErrorView.showWith(message: message, isErrorMessage: true, removed: nil)
            }
         }
         
         
      }
      
   }
   
   // MARK: -
   
   func updateBilBreakDownOrAppEstimate() {
      var selectedData = PromoCodeModal(json: [String : Any]())
      if selectedPromoIndex != 200 {
         selectedData = Singleton.sharedInstance.couponsArray[self.selectedPromoIndex]
      }
      
      if FormDetails.currentForm.verticalType != .taxi {
         
         self.apiHitToGetBillBreakDown(promo_value: selectedData.promoValue, benefit_type: selectedData.benefitType)
         
      }else{
         
         if self.isDeliveryAdded == true   {
            self.getEstimatedCost()
         }
         
      }
      
      
   }
   
   
//   func updateMap(coordinate:CLLocationCoordinate2D,address:String){
//      selectedDropLat = coordinate.latitude
//      Singleton.sharedInstance.selectedDestinationLatForTaxi = coordinate.latitude
//      selectedDropLong = coordinate.longitude
//      Singleton.sharedInstance.selectedDestinationlongForTaxi = coordinate.longitude
//      selectedDestinationAddress = address
//      Singleton.sharedInstance.selectedDestinationAddressForTaxi = address
//      getEstimatedCost()
//   }

}
