//
//  ExtensionClass.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 15/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

//MARK: String
extension String {
    func blank(_ text: String) -> Bool {
        let trimmed = text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return trimmed.isEmpty
    }
    
    var trimText:String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    var localized: String {
        if let path = frameworkBundle!.path(forResource: UserDefaults.standard.value(forKey: USER_DEFAULT.selectedLocale) as? String, ofType: "lproj") {
            let bundle = Bundle(path: path)
            return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        }
        return self
    }
    
    var length: Int {
        return self.characters.count
    }
    
    var jsonObjectArray: [Any] {
        do {
            let value = try JSONSerialization.jsonObject(with: self.data(using: String.Encoding.utf8)!, options: JSONSerialization.ReadingOptions.mutableLeaves) as? [Any]
            if value != nil{
            return value!
            }else{
                return []
            }
        } catch {
            print("Error")
        }
        return []
    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGRect {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox
    }
    
    
    func datefromYourInputFormatToOutputFormat(input:String,output:String)-> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = input
        dateFormatter.timeZone = TimeZone(abbreviation:  "UTC")
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = output
        //dateFormatter.timeZone = NSTimeZone.local
        guard date != nil else {
            return ""
        }
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
    
    func convertDateFromUTCtoLocal(input:String,output:String,toConvert:Bool = true )-> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = input
        if toConvert == true    {
        dateFormatter.timeZone = TimeZone(abbreviation:  "UTC")
        }else{
          dateFormatter.timeZone = NSTimeZone.local
        }
        print(self)
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = output
        dateFormatter.timeZone = NSTimeZone.local
        guard date != nil else {
            return ""
        }
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
    
    func dateFromString(withFormat:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone.local
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = withFormat
        dateFormatter.timeZone = NSTimeZone.local
        guard date != nil else {
            return ""
        }
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
  
  func stripOutUnwantedCharacters(charactersYouWant: String) -> String {
    let unwantedcharacters = CharacterSet(charactersIn: charactersYouWant).inverted
    let filteredString = self.components(separatedBy: unwantedcharacters).joined(separator: "")
    return filteredString
  }
}


extension UILabel {
    func setLabelWithFontColorText(yourtext:String, yourColor:UIColor, fontSize: CGFloat,fontName:String) {
        self.font = UIFont(name: fontName, size: fontSize)
        self.textColor = yourColor
        self.text = yourtext
    }
}


//MARK: Array
extension Array {
    var jsonString:String {
        do {
            let dataObject:Data? = try JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted)
            if let data = dataObject {
                let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                if let json = json {
                    return json as String
                }
            }
        } catch {
            print("Error")
        }
        return ""
    }
}

extension UIImageView{
   func loadImageAsynchronously(_ imageURL:String, withPlaceholderImage placeholderImage: UIImage? = nil) {
      
      if placeholderImage != nil {
         image = placeholderImage
      }
      
//        if let cachedImage = Singleton.sharedInstance.allImagesCache.object(forKey: "\(imageURL)" as NSString) {
//            //image caching
//            self.image = cachedImage
//            
//        } else {
//            // The image isn't cached, download the img data
//            // We should perform this in a background thread
//            let url = URL(string: imageURL as String)
//            let request: Foundation.URLRequest = Foundation.URLRequest(url: url!)
//            let mainQueue = OperationQueue.main
//            
//            NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
//                if error == nil {
//                    // Convert the downloaded data in to a UIImage object
//                    let image = UIImage(data: data!)
//                    // Store the image in to our cache
//                    // Update the cell
//                        if(image != nil) {
//                            Singleton.sharedInstance.allImagesCache.setObject(image!, forKey: "\(request.url!.absoluteString)" as NSString)
//                            
//                            self.image = image
//                        }
//                }
//                else {
//                    print("Error")
//                }
//            })
//        }
    }
    //,copletionHandler:((_ image:UIImage)->Void)
    func loadImageAsynchronouslyWithPlaceHolderAndCompletion(_ imageURL:String,placeHolder:UIImage) {
//        if let cachedImage = Singleton.sharedInstance.allImagesCache.object(forKey: "\(imageURL)" as NSString) {
//            //image caching
//            self.image = cachedImage
//            
//        } else {
//            // The image isn't cached, download the img data
//            // We should perform this in a background thread
//            self.image = placeHolder
//            let url = URL(string: imageURL as String)
//            let request: Foundation.URLRequest = Foundation.URLRequest(url: url!)
//            let mainQueue = OperationQueue.main
//            
//            NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
//                if error == nil {
//                    // Convert the downloaded data in to a UIImage object
//                    let image = UIImage(data: data!)
//                    // Store the image in to our cache
//                    // Update the cell
//                    DispatchQueue.main.async(execute: {
//                        if(image != nil) {
//                            Singleton.sharedInstance.allImagesCache.setObject(image!, forKey: "\(request.url!.absoluteString)" as NSString)
//                            self.image = image
//                        }
//                    })
//                }
//                else {
//                    print("Error")
//                }
//            })
//        }
    }
    
    
    
    
    
    func setImageUrl(urlString:String, placeHolderImage:UIImage, indexPath:IndexPath, view:AnyObject) {
        
//        if let cacheImage = Singleton.sharedInstance.allImagesCache.object(forKey: "\(urlString)" as NSString) {//image caching
//            
//            self.image = cacheImage
//            
//            return
//            
//        } else {
//            
//            // The image isn't cached, download the img data
//            
//            // We should perform this in a background thread
//            
//            let url = URL(string: urlString as String)
//            
//            let request: Foundation.URLRequest = Foundation.URLRequest(url: url!)
//            
//            let mainQueue = OperationQueue.main
//            
//            NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue,  completionHandler: { (response, data, error) -> Void in
//                
//                if error == nil {
//                    
//                    if urlString == request.url!.absoluteString {
//                        
//                        // Convert the downloaded data in to a UIImage object
//                        
//                        let image = UIImage(data: data!)
//                        
//                        // Store the image in to our cache
//                        
//                        
//                        
//                        DispatchQueue.main.async(execute: {
//                            
//                            if(image != nil) {
//                                
//                                Singleton.sharedInstance.allImagesCache.setObject(image!, forKey: "\(request.url!.absoluteString)" as NSString)
//                                
//                                // Update the cell
//                                
//                                //                                if view.isKind(of: UITableView.classForCoder()) == true {
//                                
//                                //                                    let tableView = view as! UITableView
//                                
//                                //                                    if let cellToUpdate = tableView.cellForRow(at: indexPath) {
//                                
//                                //                                        cellToUpdate.imageView?.image = image
//                                
//                                //                                    }
//                                
//                                //                                }
//                                
//                                if view.isKind(of: UICollectionView.classForCoder()) == true {
//                                    
//                                    let collectionView = view as! UICollectionView
//                                    
//                                    if let cellToUpdate = collectionView.cellForItem(at: indexPath) as? TaxiCarTypes {   //collectionView.cellForRow(at: indexPath) {
//                                        
//                                        cellToUpdate.carImage.image  = image
//                                        
//                                    }
//                                    
//                                }
//                                
//                                return
//                                
//                            } else {
//                                
//                                self.image = #imageLiteral(resourceName: "placeHolder")
//                                
//                                return
//                                
//                            }
//                            
//                        })
//                        
//                    } else {
//                        
//                        return
//                        
//                    }
//                    
//                }
//                    
//                else {
//                    
//                    self.image = #imageLiteral(resourceName: "placeHolder")
//                    
//                    return
//                    
//                }
//                
//            })
//            
//        }
//        
//        self.image = placeHolderImage
        
    }
    
    
    
    
    
    
    
}

extension NSLocale {
    struct locale {
        let countryCode: String
        let countryName: String
        
        static func getCurrentDialingCode() -> String {
            let locale = NSLocale.current as NSLocale
            guard let code = locale.object(forKey: .countryCode) as? String else {
                return ""
            }
            return dialingCode[code] ?? ""
        }
        
    }
    
    class func locales() -> [locale] {
        var locales = [locale]()
        for localeCode in NSLocale.isoCountryCodes {
            let countryName = Locale.current.localizedString(forRegionCode: localeCode)
            // let countryName = NSLocale().displayName(forKey: NSLocale.Key.countryCode, value: localeCode)!
            let countryCode = localeCode
            let loc = locale(countryCode: countryCode, countryName: countryName!)
            locales.append(loc)
        }
        return locales
    }
}

public extension UIFont {
    public static func registerFontWithFilenameString(filenameString: String, bundleIdentifierString: String) {
        guard let bundle = frameworkBundle else {
//            print("UIFont+:  Failed to register font - bundle identifier invalid.")
            return
        }
        
        guard let pathForResourceString = bundle.path(forResource:filenameString, ofType: nil) else {
//            print("UIFont+:  Failed to register font - path for resource not found.")
            return
        }
        
        guard let fontData = NSData(contentsOfFile: pathForResourceString) else {
//            print("UIFont+:  Failed to register font - font data could not be loaded.")
            return
        }
        
        guard let dataProvider = CGDataProvider.init(data: fontData) else {
//            print("UIFont+:  Failed to register font - data provider could not be loaded.")
            return
        }
        
         let fontRef = CGFont.init(dataProvider) 
        
        var errorRef: Unmanaged<CFError>? = nil
        if (CTFontManagerRegisterGraphicsFont(fontRef, &errorRef) == false) {
//            print("UIFont+:  Failed to register font - register graphics font failed - this font may have already been registered in the main bundle.")
        }
    }
}

extension String {
    func htmlAttributedString() -> NSAttributedString? {
        guard let data = self.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
        
        
        guard let html = try? NSMutableAttributedString(
            data: data,
            options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
            documentAttributes: nil) else { return nil }
        return html
    }
    
    func htmlAttributedString(alignment: NSTextAlignment) -> NSAttributedString? {
        guard let data = self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        
        //        attributedText.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSRange(location: 0, length: attributedText.characters.count))
        
        guard let html = try? NSMutableAttributedString(
            data: data,
            options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSParagraphStyleAttributeName: paragraphStyle],
            documentAttributes: nil) else { return nil }
        
        //     let str = html.string
        //  html.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: NSRange(location: 0, length: str.characters.count))
        return html
    }
}


extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
