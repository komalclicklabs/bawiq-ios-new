//
//  Product.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 16/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation


class Product: LevelElement {
   var quantity = 0
   var customString = ""
   private(set) var price: Double = 0
   private(set) var unitDescription = ""
   private(set) var custoMizableArray = [MenuCustomizableObjects]()
   var selectedCustomization = [String:SelectedCustomProductData]()
    
   override init?(json: [String : Any], level: Int) {
      super.init(json: json, level: level)
      print(json)
      self.price = (json["price"] as? NSNumber)?.doubleValue ?? 0
      self.unitDescription = (json["description"] as? String) ?? ""
      self.custoMizableArray = self.parseCustomisation(items: json)
        print("this is cutomisation object")
        print(custoMizableArray)
        print("this is cutomisation object end")
    
      if actionType == .next || actionType == .nextButButtonHidden {
         actionType = .singleAddition
      }
    
   }
   
   func getPricingDescription() -> String {
      return "\(name) * \(quantity) = \(FormDetails.currentForm.currencyid)\(price * Double(quantity))"
   }
   
   func getPrice() -> Double {
      return getTotalCostFromCust()
   }
    
    func canRemoveWithoutCart() -> Bool{
        if self.selectedCustomization.count > 1{
            return false
        }else{
            return true
        }
    }
    
    private func getTotalCostFromCust() -> Double{
        var total = price * Double(quantity)
        for (_,val) in self.selectedCustomization{
            total = total + (Double(val.quantity)*val.extraPrice)
        }
        return total
    }
    
    
    func addCustomizationData(data:SelectedCustomProductData,key:String){
        if self.selectedCustomization[key] != nil{
            let dataToUpdate = self.selectedCustomization[key]
            dataToUpdate!.quantity = dataToUpdate!.quantity + data.quantity
            self.selectedCustomization[key] = dataToUpdate
        }else{
            self.selectedCustomization[key] = data
        }
        
        print("price after update \(getTotalCostFromCust()) ")
    }
    
   
   class func fetchProductsFromServerOf(parentWithid id: String?, andFormId formId: String, atLevel level: Int, showActivityIndicator: Bool, completion: @escaping (Bool, [Product]?) -> Void) {
      
      let coordinates = CustomLocationManager.latestCoordinate
         
         let params = getParamsToFetchChildren(ofParentWithId: id, latitude: coordinates.latitude, longitude: coordinates.longitude, formId: formId)
         
         HTTPClient.makeConcurrentConnectionWith(method: .POST,showActivityIndicator: showActivityIndicator, para: params, extendedUrl: API_NAME.getProductsForCategory) { (responseObject, error, _, statusCode) in
        
            if statusCode == .some(STATUS_CODES.SHOW_DATA),
               let response = responseObject as? [String: Any],
               let rawLevels = response["data"] as? [[String: Any]] {

               guard rawLevels.count > 0 else {
                  let message = error?.localizedDescription ?? ERROR_MESSAGE.unableToFetchProducts
                  ErrorView.showWith(message: message, removed: nil)
                  completion(false, nil)
                  return
               }
               
               let products = getArrayOfProductsFrom(json: rawLevels, level: level)
               
               completion(true, products)
            } else {
               completion(false, nil)
            }
         }
      
   }
   
    
    func getArrayOfObjects() -> [Product]{
        
        var arrayOfProducts = [Product]()
        
        if self.selectedCustomization.count == 0{
            return [self]
        }else{
            for (key,value ) in self.selectedCustomization{
                let data = Product(json: ["price":value.extraPrice+self.price,"product_id":self.id,"type":self.actionType.rawValue], level: 0)
                data?.quantity = value.quantity
                
                data?.name = self.name
                data?.customString = key
                data?.id = self.id
                data?.actionType = self.actionType
                arrayOfProducts.append(data!)
            }
        }
        return arrayOfProducts
        
    }

    func getDummyJson(name:String){
        
    }
    
    func getFirstValueOfCustomDictionary() -> String{
        if selectedCustomization.count > 0{
        for (key,val) in self.selectedCustomization{
            return key
        }
        }else{
            return ""
        }
        return ""
    }
    
   private class func getParamsToFetchChildren(ofParentWithId id: String?, latitude: Double, longitude: Double, formId: String) -> [String: Any] {
      var params =  [String: Any]()
    
    params["api_key"] = FormDetails.currentForm.apiKey
    params["reference_id"] = Merchant.shared!.vedorRefferenceId
    params["user_id"] = FormDetails.currentForm.user_id!
    params["form_id"] = FormDetails.currentForm.form_id!
    params["parent_category_id"] = id ?? ""
    params["app_device_type"] = APP_DEVICE_TYPE
    print(params)
    
      
      if id == nil {
         params.removeValue(forKey: "parent_category_id")
      }
      
      
      return params
   }
   
   private class func getArrayOfProductsFrom(json: [[String: Any]], level: Int) -> [Product] {
      var products = [Product]()
      for rawProduct in json {
         if let product = Product(json: rawProduct, level: level) {
            products.append(product)
         }
      }
      return products
   }
    
    func getJsonFormatOfSelectedProduct() -> [[String:Any]]{
        var dataToSend = [[String:Any]]()
       if self.selectedCustomization.count > 0{
            for (key,value) in self.selectedCustomization{
                var singleData = [String:Any]()
                singleData["product_id"] = self.id
                singleData["unit_price"] = self.price
                singleData["quantity"] = value.quantity
                singleData["total_price"] = Double(value.quantity) * self.price
                singleData["customizations"] = self.getCustomisationArray(customisationData:value.selectedCustId)
                dataToSend.append(singleData)
                
                
            }
       }else{
        var singleData = [String:Any]()
        singleData["product_id"] = self.id
        singleData["unit_price"] = self.price
        singleData["quantity"] = self.quantity
        singleData["total_price"] = Double(self.quantity) * self.price
        dataToSend.append(singleData)
        }
    
    return dataToSend
    
    }
    
    
    func getCustomisationArray(customisationData:[(id:String, customizeOptionName:String ,isDefault:Bool,customizePrice:Double, additionalCost:Double)]) -> [[String:Any]]{
        var dataToSend = [[String:Any]]()
        for i in customisationData{
            var singleData = [String:Any]()
            singleData["cust_id"] = i.id
            singleData["unit_price"] = i.customizePrice
            singleData["quantity"] = 1
            singleData["total_price"] = i.customizePrice
            dataToSend.append(singleData)
        }
        return dataToSend
    }
    
    func parseCustomisation(items:[String:Any]) -> [MenuCustomizableObjects]   {
        
        var menuCustomizeObjectsForItem = [MenuCustomizableObjects]()
        
        //        return menuCustomizeObjectsForItem
        
        if let customizeItemArray = items["customization"] as? [AnyObject] {
            
            
            for customizeItem in customizeItemArray {
                
                let menuCustomizeObject = MenuCustomizableObjects()
                
                if let customizeId = customizeItem["customize_id"] as? Int {
                    
                    menuCustomizeObject.customizeID = customizeId
                }
                if let customizeItemName = customizeItem["name"] as? String {
                    
                    menuCustomizeObject.customizeName  = customizeItemName
                }
                if let isCustomizationCheckBox = customizeItem["is_check_box"] as? Bool {
                    
                    menuCustomizeObject.isCheckBox  = isCustomizationCheckBox
                }
                
                var customizeTuppleArray = [(id:String, customizeOptionName:String ,isDefault:Bool,customizePrice:Double, additionalCost:Double)]()
                
                if let customizeOptionArray = customizeItem["customize_options"] as? [AnyObject] {
                    for customizeOption in customizeOptionArray {
                        
                        var customizeOptionID = String(), customizeOptionName = String(), additionalCost = Double(), customizePrice = Double(), isDefault = Bool()
                        
                        if let additional_cost = customizeOption["additional_cost"] as? Double {
                            
                            additionalCost = additional_cost
                        }
                        
                        if let customize_option_id = customizeOption["cust_id"] as? Int {
                            
                            customizeOptionID = "\(customize_option_id)"
                        }
                        
                        if let customize_option_name = customizeOption["name"] as? String {
                            // customizeOptionIDArray
                            customizeOptionName = customize_option_name
                        }
                        
                        if let customize_price = customizeOption["price"] as? Double {
                            
                            customizePrice = customize_price
                        }
                        if let is_default = customizeOption["is_default"] as? Bool {
                            
                            isDefault = is_default
                        }
                        
                        customizeTuppleArray.append((id:customizeOptionID, customizeOptionName:customizeOptionName ,isDefault:isDefault,customizePrice:customizePrice, additionalCost:additionalCost))
                        
                    }
                }
                
                menuCustomizeObject.customizeSubOptionArray = customizeTuppleArray
                
                
                menuCustomizeObjectsForItem.append(menuCustomizeObject)
            }
            
        }
        
        //        print(menuCustomizeObjectsForItem)
        
        return menuCustomizeObjectsForItem
    }
    
    
    
}
