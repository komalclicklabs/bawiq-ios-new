//
//  CreateTaskDetails.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 30/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

public class CreateTaskDetails: NSObject {
    
    var autoAssignment:Int = 1
    var customerAddress:String = ""
    var customerEmail:String = ""
    var customerPhone:String = ""
    var customerUsername:String = ""
    var jobDeliveryDatetime:String = ""
    var latitude:String = ""
    var longitude:String = ""
    var hasDelivery:Int = 0
    var hasPickup:Int = 1
    var jobDescription:String = ""
    var jobPickupAddress:String = ""
    var jobPickupDateTime:String = ""
    var jobPickupLatitude:String = ""
    var jobPickupLongitude:String = ""
    var jobPickupName:String = ""
    var jobPickupEmail:String = ""
    var jobPickupPhone:String = ""
    var layoutType:Int = 0
    var refImages:[String] = []
    var domainName:String = ""
    var vendorId:Int = 2
    var hidePickupDelivery:Bool = false
    var pickupMetadata = [Any]()
    var metaData = [Any]()
    var fleetId = ""
    var responseOfGetPayment: [String: Any] = [:]
    var metaDataArray = [[String:Any]]()
    var pickUpMetaDataArray = [[String:Any]]()
    var createTaskParamsToSend = [String:Any]()
    
    init(json: [String:Any]) {
        
    }
   
   func getJSON() -> [String: Any] {
      var params:[String: Any] = ["autoAssignment" : self.autoAssignment]
      params["customerAddress"] = self.customerAddress
      params["customerEmail"] = self.customerEmail
      params["customerPhone"] = self.customerPhone
      params["customerUsername"] = self.customerUsername
      params["jobDeliveryDatetime"] = self.jobDeliveryDatetime
      params["latitude"] = self.latitude
      params["longitude"] = self.longitude
      params["hasDelivery"] = self.hasDelivery
      params["hasPickup"] = self.hasPickup
      params["jobDescription"] = self.jobDescription
      params["jobPickupAddress"] = self.jobPickupAddress
      params["jobPickupDateTime"] = self.jobPickupDateTime
      params["jobPickupLatitude"] = self.jobPickupLatitude
      params["jobPickupLongitude"] = self.jobPickupLongitude
      params["jobPickupName"] = self.jobPickupName
      params["jobPickupEmail"] = self.jobPickupEmail
      params["jobPickupPhone"] = self.jobPickupPhone
      params["layoutType"] = self.layoutType
      params["refImages"] = self.refImages
      params["domainName"] = self.domainName
      params["vendorId"] = self.vendorId
      params["hidePickupDelivery"] = self.hidePickupDelivery
      params["pickupMetadata"] = self.pickupMetadata
      params["metaData"] = self.metaData
      params["fleetId"] = self.fleetId
      params["responseOfGetPayment"] = self.responseOfGetPayment
      params["metaDataArray"] = self.metaDataArray
      params["pickUpMetaDataArray"] = self.pickUpMetaDataArray
      return params
   }
}
