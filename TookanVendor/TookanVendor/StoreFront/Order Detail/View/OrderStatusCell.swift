//
//  OrderStatusCell.swift
//  TookanVendor
//
//  Created by CL-Macmini-110 on 10/5/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class OrderStatusCell: UITableViewCell {

   //MARK:- Outlets
   @IBOutlet var leftLabel: UILabel!
   @IBOutlet var rightLabel: UILabel!
   @IBOutlet var bottomLine: UIView!
   
    override func awakeFromNib() {
      super.awakeFromNib()
      self.setLeftLabel()
      self.setRightLabel()
      self.setBottomLine()
    }

   //MARK:- Setting Views
   func setLeftLabel() {
      self.leftLabel.textColor = COLOR.textColorSlightLight
      self.leftLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
   }
   
   func setRightLabel() {
      self.rightLabel.textColor = UIColor.black
      self.rightLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.medium)
   }
   
   func setBottomLine() {
      self.bottomLine.backgroundColor = COLOR.SPLASH_LINE_COLOR
   }
   
}
