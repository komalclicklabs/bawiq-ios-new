//
//  OrderSummaryCell.swift
//  TookanVendor
//
//  Created by CL-Macmini-110 on 10/5/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class OrderSummaryCell: UITableViewCell {

   @IBOutlet var outerView: UIView!
   @IBOutlet var leftLabel: UILabel!
   @IBOutlet var rightLabel: UILabel!
   @IBOutlet var dashedHorizontalLineView: UIView!
   
   override func awakeFromNib() {
      super.awakeFromNib()
      self.setLeftLabel()
      self.setRightLabel()
      self.dashedHorizontalLineView.addDashedLine(fromPoint: CGPoint(x: 0, y: 0), toPoint: CGPoint(x:self.contentView.frame.size.width , y: 0))
    }
   
   //MARK: Setting Views
   func setRightLabel() {
      self.rightLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
      self.rightLabel.textColor = UIColor.black
   }
   
   func setLeftLabel() {
      self.leftLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
      self.leftLabel.textColor = UIColor.black
   }

}
