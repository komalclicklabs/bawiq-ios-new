//
//  Constants.swift
//  StoreFront
//
//  Created by cl-macmini-45 on 18/09/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

let frameworkBundle = Bundle(identifier: "com.clicklabs.StoreFront")
let placeholdeImage  = UIImage(named: "placeHolder", in: frameworkBundle, compatibleWith: nil)
let homeAddressImage = UIImage(named: "homeAddress", in: frameworkBundle, compatibleWith: nil)
let workAddress = UIImage(named: "workAddress", in: frameworkBundle, compatibleWith: nil)
let otherAddress = UIImage(named: "otherAddress", in: frameworkBundle, compatibleWith: nil)
let deleteAddress = UIImage(named: "deleteAddress", in: frameworkBundle, compatibleWith: nil)
let editAddress = UIImage(named: "addressOptions", in: frameworkBundle, compatibleWith: nil)
let backButtonImage = UIImage(named: "back", in: frameworkBundle, compatibleWith: nil)
let iconCash = UIImage(named: "iconCash", in: frameworkBundle, compatibleWith: nil)
let optionEmpty = UIImage(named: "optionEmpty", in: frameworkBundle, compatibleWith: nil)
let iconTip = UIImage(named: "iconTip", in: frameworkBundle, compatibleWith: nil)
let iconPromo = UIImage(named: "iconPromocode", in: frameworkBundle, compatibleWith: nil)
let iconBill = UIImage(named: "iconBill", in: frameworkBundle, compatibleWith: nil)
let iconPayments = UIImage(named: "iconPayments", in: frameworkBundle, compatibleWith: nil)
let greenTick = UIImage(named: "greenTick", in: frameworkBundle, compatibleWith: nil)
let sampleCard = UIImage(named: "sampleCard", in: frameworkBundle, compatibleWith: nil)
let downArrow = UIImage(named: "downArrow", in: frameworkBundle, compatibleWith: nil)
let iconDropOff = UIImage(named: "iconDropOff", in: frameworkBundle, compatibleWith: nil)
let iconAddImage = UIImage(named: "iconAddImage", in: frameworkBundle, compatibleWith: nil)
let pickupHeading = UIImage(named:"pickupHeading", in: frameworkBundle, compatibleWith: nil)
let cartHeading = UIImage(named: "cartHeading", in: frameworkBundle, compatibleWith: nil)
let taskDescription = UIImage(named: "taskDescription", in: frameworkBundle, compatibleWith: nil)
let checkboxEmpty = UIImage(named: "checkboxEmpty", in: frameworkBundle, compatibleWith: nil)
let checked = UIImage(named: "checked", in: frameworkBundle, compatibleWith: nil)

class Constants: NSObject {

}
let SCREEN_SIZE = UIScreen.main.bounds


struct USER_DEFAULT {
    static let selectedServer = "selectedServer"
    static let accessToken = "accessToken"
    static let selectedLocale = "locale"
}

struct KEYS {
    static let GOOGLE_MAPS_API_KEY = "AIzaSyCx_xRaB_xQwbwxk89CuP_6qArT2E5Uun4"
    static let GOOGLE_BROWSER_KEY = "AIzaSyCx_xRaB_xQwbwxk89CuP_6qArT2E5Uun4"
}

//MARK: FONT
struct FONT {
    static let regular = "OpenSans"
    static let bold = "OpenSans-Bold"
    static let light = "OpenSans-Light"
    static let semiBold = "OpenSans-Semibold"
}

let paymentTypeDict = [
    "2":"Card",
    "4":"Card",
    "8":"Cash"
]

enum JOB_TYPE: Int {
   case pickup = 0
   case delivery = 1
   case fieldWorkforce = 2
   case appointment = 3
   case both = 10
   
   
   func getPrefix() -> String {
      switch self {
      case .pickup:
         return "P"
      case .delivery:
         return "D"
      default:
         return ""
      }
   }
}

enum JOB_STATUS: Int {
   case assigned = 0
   case started = 1
   case successful = 2
   case failed = 3
   case arrived = 4
   case partial = 5
   case unAssigned = 6
   case accepted = 7 // Acknowledged
   case declined = 8
   case canceled = 9
   case ignored = 11
   case deleted = 10
   
   func getTextAndColor(vertical: Vertical = FormDetails.currentForm.verticalType) -> (text: String, color: UIColor) {
      
      switch self {
      case .assigned:
         return (TEXT.ASSIGNED_STATUS, JOB_STATUS_COLOR.ASSIGNED)
      case .started:
         
         return (TEXT.STARTED_STATUS , JOB_STATUS_COLOR.STARTED)
         
      case .successful:
         if vertical == .taxi {
            return (TEXT.completed , JOB_STATUS_COLOR.SUCCESSFUL)
         }else{
            return (TEXT.SUCCESSFUL_STATUS,JOB_STATUS_COLOR.SUCCESSFUL)
         }
      case .failed:
         
         return (TEXT.FAILED_STATUS,JOB_STATUS_COLOR.FAILED)
         
      case .arrived:
         
         return (TEXT.ARRIVED_STATUS, JOB_STATUS_COLOR.INPROGRESS)
         
      case .partial:
         
         return (TEXT.PARTIAL_STATUS,JOB_STATUS_COLOR.INPROGRESS)
         
      case .unAssigned:
         if vertical == .taxi {
            return (TEXT.scheduled, JOB_STATUS_COLOR.SCHEDULED)
         } else {
            return (TEXT.UNASSIGNED_STATUS, JOB_STATUS_COLOR.UNASSIGNED)
         }
      case .accepted:
         return (TEXT.ACCEPTED_STATUS,JOB_STATUS_COLOR.ACCEPTED)
      case .declined:
         return (TEXT.DECLINED_STATUS,JOB_STATUS_COLOR.DECLINED)
      case .canceled:
         return (TEXT.CANCELED_STATUS, JOB_STATUS_COLOR.CANCELLED)
      case .ignored:
         return (TEXT.IGNORED_STATUS, JOB_STATUS_COLOR.UNASSIGNED)
      case .deleted:
         return ("", UIColor.red)
      }
   }
   
   func shouldHideSosButton() -> Bool {
      switch self {
      case .started, .arrived:
         return false
      default:
         return true
      }
   }
   
   func shouldShowCallButton() -> Bool {
      switch self {
      case .successful, .canceled, .declined, .failed, .unAssigned:
         return false
      default:
         return true
      }
   }
   
   func shouldStopTracking() -> Bool {
      
      switch self {
      case .successful, .canceled, .failed:
         return true
      default:
         return false
      }
   }
   
   func processingOftaskStopped() -> Bool {
      switch self {
      case .successful, .canceled, .failed:
         return true
      default:
         return false
      }
   }
   
}


//MARK: NIB NAME
struct NIB_NAME {
    static let menuNavigationView = "MenuNavigationView"
    static let JugnooHeaderForCatalogue = "JugnooHeaderForCatalogue"
    static let listCell = "ListTableViewCell"
    static let errorView = "ErrorView"
    static let menuHeader = "MenuHeaderCell"
    static let NLevelActionView = "NLevelActionView"
    static let menuTableCollectionViewCell = "MenuTableCollectionViewCell"
    static let CheckoutHeaderViews = "CheckoutHeaderViews"
    static let DropDownCell = "DropDownCell"
    static let dateCustomField = "dateCustomField"
    static let UnEdit = "UnEdit"
    static let DateCustomField = "DateCustomField"
    static let custCheckBox = "custCheckBox"
    static let imageCollectionCell = "ImageCollectionCell"
    static let checkoutCartCell = "CartCell"
    static let CheckBoxCustomField = "CheckBoxCustomField"
    static let TaskDescriptionCell = "TaskDescriptionCell"
    static let CustomFTextField = "CustomFTextField"
    static let AddressCustomeField = "AddressCustomeField"
    static let phoneNumberCell = "phoneNumberCell"
    static let TextFieldForUnEditing = "TextFieldForUnEditing"
    static let checkoutSubtotalCell = "SubtotalTableViewCell"
    static let ImgesCustomField = "ImgesCustomField"
    static let imagesPreview = "ImagesPreview"
    static let navigation = "NavigationView"
    static let imagePreviewCell = "ImagePreviewCell"
    static let noInternetConnectionView = "NoInternetConnectionView"
    static let welcomeView = "WelcomeView"
    static let socialSectionView = "SocialSectionView"
    static let titleView = "TitleView"
    static let customTextField = "CustomTextField"
    static let ADDRESS_NIB = "AddressViewCell"
    static let PICKUPDELIVERYOPTION_NIB = "PickUpOptionCell"
    static let CONTACT_NAME_NIB = "ContactViewCell"
    static let EMAIL_PHONE_NIB = "EmailPhoneCell"
    static let DESCRIPTION_NIB = "DescriptionCell"
    static let customFieldCell = "CustomFieldCell"
    static let vendroProfileCell = "VendorProfileCell"
    static let vendorNamePicCell = "VendorNamePicCell"
    static let taskReviewCell = "TaskReviewCell"
    static let addressListCell = "AddressListCell"
    static let taskFooterView = "TaskFooterView"
    static let customDropdown = "CustomDropdown"
    static let pickupDeliveryBarCell = "PickupDeliveryBarCell"
    static let orderHistory = "OrderHistoryCell"
    static let sideMenuCell = "SideMenuCell"
    static let orderIDCallInfoCell = "OrderIDCallInfoCell"
    static let orderDetailCustomFieldAndActivityCell = "OrderDetailCustomFieldAndActivityCell"
    static let imageCustomCell = "ImageCustomCell"
    static let currentTaskCell = "CurrentTaskCell"
    static let pushNotificationView = "PushNotificationView"
    static let tutorialCell = "TutorialCell"
    static let PayementMethodCell = "PayementMethodCell"
    static let TaxiCarTypes = "TaxiCarTypes"
    static let PleaseWaitView = "PleaseWaitView"
    static let scheduleViewId:String  = "bookingDateView"
    static let ChangePhoneNumberView = "ChangePhoneNumberView"
    static let WorkFlowOptionsCell = "WorkFlowOptionsCell"
    static let laundryProductCell = "laundryProductCell"
    static let cartViewCell = "cartViewCell"
    static let CardsCell = "CardsCell"
    static let PaymentHeaders = "PaymentHeaders"
    static let promoCell = "PromoCell"
    static let BillBreakupCell = "BillBreakupCell"
    static let AddNewPromoCell = "AddNewPromoCell"
    static let TipCell = "TipCell"
    static let infoPopUpView = "InfoPopUpView"
    static let FavLocationCell = "FavLocationCell"
    static let SideMenuProfileCell = "SideMenuProfileCell"
    static let SelectAgentTableViewCell = "SelectAgentTableViewCell"
    static let VendorProfileCellEdit = "vendorProfileCellEdit"
    
   static let orderHistoryViewCell = "OrderHistoryViewCell"
   static let orderStatusCell = "OrderStatusCell"
   static let orderSummaryCell = "OrderSummaryCell"
}

//MARK: - TABLECELL IDENTIFIERS
struct CELL_IDENTIFIER {
    static let menuHeader = "MenuHeaderViewCell"
    static let menuTableCollectionViewCell = "menuTableCollectionViewCell"
    static let taskDescription = "taskDescription"
    static let customTextField = "customeTextField"
    static let AddressCustomeField = "addressTextField"
    static let phoneCode = "phoneCode"
    static let dateCustomField = "dateCustomField"
    static let UnEdit = "UnEdit"
    static let DropDownCell = "DropDownCell"
    static let custCheckBox = "custCheckBox"
    static let ImageCustom = "ImageCustom"
    static let checkoutCartCell = "CheckoutCartCell"
     static let checkoutSubtotalCell = "SubtotalTableViewCell"
    
    static let ADDRESSCELL = "AddressCell"
    static let PICKUPDELIVERYOPTIONCELL = "PickUpDeliveryOptionCell"
    static let CONTACT_NAME_CELL = "ContactNameCell"
    static let EMAIL_PHONE_CELL = "EmailPhoneCell"
    static let DESCRIPTION_CELL = "DescriptionCell"
    static let vendorProfileIdentifier = "VendorProfileCell"
    static let vendorNamePicCellIdentifier = "vendorNamePicCellIdentifier"
    static let taskReviewCellIdentifier = "TaskReviewCellIdentifier"
    static let addressCellIdentifier = "AddressCellIdentifier"
    static let orderHistoryCellIdentifier = "OrderHistoryCell"
    static let sideMenuCellIdentifier = "SideMenuCellIdentifier"
    static let orderIDCallInfoCell = "OrderIDCallInfoCell"
    static let orderDetailCustomFieldAndActivityCell = "OrderDetailCustomFieldAndActivityCell"
    static let payementMethod = "payementMethod"
    static let carTypeItem = "carTypeItem"
    static let servcesCell = "servcesCell"
    static let laundryProductCell = "LaundyCell"
    static let cartCell = "cartCell"
    static let cardCell = "cardCell"
    static let promoCell = "promoCell"
    static let billCell = "billCell"
    static let addNewPromo = "addNewPromo"
    static let tip = "tip"
    static let FavLocationCell = "FavLocationCell"
    static let sideMenuProfile = "sideMenuProfile"
    static let SelectAgentCell = "SelectAgentCell"
    static let VendorProfileCell = "VendorProfileCellNew"
    
    static let orderHistoryViewCell = "OrderHistoryViewCell"
    
}

//MARK: STORYBOARD
enum StoryBoard: String {
    case afterLogin = "AfterLogin"
    case main = "Main"
    case taxi = "TaxiAfterLogIn"
    case nLevel = "NLevel"
    case favLocation = "FavouriteLocation"
}

struct STORYBOARD_NAME {
    static let afterLogin = StoryBoard.afterLogin.rawValue //Appointment,Deliveries
    static let main = StoryBoard.main.rawValue //Common(Appointment,Deliveries,Taxi)
    static let taxiStoryBoardId = StoryBoard.taxi.rawValue //Taxi
}


//MARK: HEIGHT
struct HEIGHT {
    static let navigationHeight:CGFloat = 81 + getTopInsetofSafeArea()
    static let jugnooHeaderHeight:CGFloat = 131.1
    static let titleHeight:CGFloat = 40.0
    static let textFieldHeight:CGFloat = 60.0
    static let errorMessageHeight:CGFloat = 43.0
    static let menuNavigationHeight:CGFloat = 64.0
   
   static private func getTopInsetofSafeArea() -> CGFloat {
      let topInset = UIApplication.shared.keyWindow!.safeAreaInsetsForAllOS.top
      if topInset > 0 {
         return topInset - 20
      }
      return topInset
   }
}
//MARK: STORYBOARD
struct STORYBOARD_ID {
    static let menuView = "menuView"
    static let listVC = "ListTableViewController"
    static let addLocation = "addLocation"
    static let addCardWeb = "addCardWeb"
     static let payment = "payment"
   static let orderDetail = "OrderDetail"
   static let orderHistoryVC = "OrderHistoryVC"

}

//MARK: - NOTIFICATION OBSERVER
struct NOTIFICATION_OBSERVER {
   static let updateOnPushNotification = "pushNotification"
   
}

//MARK: CUSTOM FIELD TYPE
enum CUSTOM_FIELD_TYPE {
    case address
    case option
    case contact
    case email
    case phone
    case description
    case date
    case startDate
    case endDate
    case image
    case dropDown
    case text
    case number
    case dateCustomField
    case checkbox
    case telephone
    case emailCustomField
    case url
    case checklist
    case dateFuture
    case datePast
    case dateTime
    case dateTimeFuture
    case dateTimePast
    case table
    case barcode
    case driverInfo
}

enum WORKFLOW: Int {
    case pickupDelivery = 0
    case appointment
    case fieldWorkforce
    case Taxi
}

//MARK: PICKUP/DELIVERY
enum PICKUP_DELIVERY:Int {
    case pickup = 0
    case delivery
    case both
    case addDeliveryDetails
}


struct ACTIVE_INACTIVE_STATES {
    static let active = "active"
    static let inActive = "inActive"
    static let filled = "filled"
}

struct CUSTOM_FIELD_DATA_TYPE {
    static let dropDown = "Dropdown"
    static let text = "Text"
    static let number = "Number"
    static let image = "Image"
    static let date = "Date"
    static let checkbox = "Checkbox"
    static let telephone = "Telephone"
    static let email = "Email"
    static let url = "URL"
    static let checklist = "Checklist"
    static let dateFuture = "Date-Future"
    static let datePast = "Date-Past"
    static let dateTime = "Date-Time"
    static let dateTimeFuture = "Datetime-Future"
    static let dateTimePast = "Datetime-Past"
    static let table = "Table"
    static let barcode = "Barcode"
}

let dialingCode = ["AC":"247","BD": "880", "BE": "32", "BF": "226", "BG": "359", "BA": "387", "BB": "1-246", "WF": "681", "BL": "590", "BM": "1-441", "BN": "673", "BO": "591", "BH": "973", "BI": "257", "BJ": "229", "BT": "975", "JM": "1-876", "BV": "", "BW": "267", "WS": "685", "BQ": "599", "BR": "55", "BS": "1-242", "JE": "44-1534", "BY": "375", "BZ": "501", "RU": "7", "RW": "250", "RS": "381", "TL": "670", "RE": "262", "TM": "993", "TJ": "992", "RO": "40", "TK": "690", "GW": "245", "GU": "1-671", "GT": "502", "GS": "500 ", "GR": "30", "GQ": "240", "GP": "590", "JP": "81", "GY": "592", "GG": "44-1481", "GF": "594", "GE": "995", "GD": "1-473", "GB": "44", "GA": "241", "SV": "503", "GN": "224", "GM": "220", "GL": "299", "GI": "350", "GH": "233", "OM": "968", "TN": "216", "JO": "962", "HR": "385", "HT": "509", "HU": "36", "HK": "852", "HN": "504", "HM": " ", "VE": "58", "PR": "1-787 and 1-939", "PS": "970", "PW": "680", "PT": "351", "SJ": "47", "PY": "595", "IQ": "964", "PA": "507", "PF": "689", "PG": "675", "PE": "51", "PK": "92", "PH": "63", "PN": "870", "PL": "48", "PM": "508", "ZM": "260", "EH": "212", "EE": "372", "EG": "20", "ZA": "27", "EC": "593", "IT": "39", "VN": "84", "SB": "677", "ET": "251", "SO": "252", "ZW": "263", "SA": "966", "ES": "34", "ER": "291", "ME": "382", "MD": "373", "MG": "261", "MF": "590", "MA": "212", "MC": "377", "UZ": "998", "MM": "95", "ML": "223", "MO": "853", "MN": "976", "MH": "692", "MK": "389", "MU": "230", "MT": "356", "MW": "265", "MV": "960", "MQ": "596", "MP": "1-670", "MS": "1-664", "MR": "222", "IM": "44-1624", "UG": "256", "TZ": "255", "MY": "60", "MX": "52", "IL": "972", "FR": "33", "IO": "246", "SH": "290", "FI": "358", "FJ": "679", "FK": "500", "FM": "691", "FO": "298", "NI": "505", "NL": "31", "NO": "47", "NA": "264", "VU": "678", "NC": "687", "NE": "227", "NF": "672", "NG": "234", "NZ": "64", "NP": "977", "NR": "674", "NU": "683", "CK": "682", "XK": "", "CI": "225", "CH": "41", "CO": "57", "CN": "86", "CM": "237", "CL": "56","CHL":"56", "CC": "61", "CA": "1", "CG": "242", "CF": "236", "CD": "243", "CZ": "420", "CY": "357", "CX": "61", "CR": "506", "CW": "599", "CV": "238", "CU": "53", "SZ": "268", "SY": "963", "SX": "599", "KG": "996", "KE": "254", "SS": "211", "SR": "597", "KI": "686", "KH": "855", "KN": "1-869", "KM": "269", "ST": "239", "SK": "421", "KR": "82", "SI": "386", "KP": "850", "KW": "965", "SN": "221", "SM": "378", "SL": "232", "SC": "248", "KZ": "7", "KY": "1-345", "SG": "65", "SE": "46", "SD": "249", "DO": "1-809 and 1-829", "DM": "1-767", "DJ": "253", "DK": "45", "VG": "1-284", "DE": "49", "YE": "967", "DZ": "213", "US": "1", "UY": "598", "YT": "262", "UM": "1", "LB": "961", "LC": "1-758", "LA": "856", "TV": "688", "TW": "886", "TT": "1-868", "TR": "90", "LK": "94", "LI": "423", "LV": "371", "TO": "676", "LT": "370", "LU": "352", "LR": "231", "LS": "266", "TH": "66", "TF": "", "TG": "228", "TD": "235", "TC": "1-649", "LY": "218", "VA": "379", "VC": "1-784", "AE": "971", "AD": "376", "AG": "1-268", "AF": "93", "AI": "1-264", "VI": "1-340", "IS": "354", "IR": "98", "AM": "374", "AL": "355", "AO": "244", "AQ": "", "AS": "1-684", "AR": "54", "AU": "61", "AT": "43", "AW": "297", "IN": "91", "AX": "358-18", "AZ": "994", "IE": "353", "ID": "62", "UA": "380", "QA": "974", "MZ": "258"]

