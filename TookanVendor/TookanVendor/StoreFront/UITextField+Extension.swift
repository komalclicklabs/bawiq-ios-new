//
//  UITextField+Extension.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 15/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

extension MKTextField {
  
  func configureWith(text: String, placeholder: String) {
    self.placeholder = placeholder
    self.text = text
    tintColor = COLOR.TITLE_UNDER_LINE_COLOR
    bottomBorderColor = COLOR.PLACEHOLDER_COLOR
    textColor = COLOR.SPLASH_TEXT_COLOR
    placeHolderColor = COLOR.PLACEHOLDER_COLOR
    font = UIFont(name: FONT.light, size: FONT_SIZE.textfieldSize)
  }
}



import Foundation

extension NSMutableAttributedString{
    
    class func addUnderLineString(givenString: String, givenStringColor: UIColor, underLineStyle: NSUnderlineStyle, underLineColor: UIColor, fontOfString: UIFont) -> NSMutableAttributedString {
        let attrString = NSMutableAttributedString(string: givenString)
        let attrs = [NSFontAttributeName:fontOfString,
                     NSUnderlineStyleAttributeName: underLineStyle,
                     NSUnderlineColorAttributeName: underLineColor,
                     NSForegroundColorAttributeName: givenStringColor] as [String : Any]
        
        attrString.setAttributes(attrs, range: NSMakeRange(0, attrString.length))
        return attrString
    }
    
    class func createAttributeText(_ color:UIColor, font:UIFont, text:String,  spacing:Float) ->NSMutableAttributedString {
        
        let attrString = NSMutableAttributedString(string:text as String)
        let attrs = [NSFontAttributeName:font]
        
        attrString.setAttributes(attrs, range: NSMakeRange(0, attrString.length))
        attrString.addAttribute(NSKernAttributeName, value:CGFloat(spacing), range: NSRange(location: 0, length: (text as String).characters.count))
        attrString.addAttribute(NSForegroundColorAttributeName, value:color, range:NSRange(location: 0, length: (text as String).characters.count))
        
        //        var paragraphStyle:NSMutableParagraphStyle = NSMutableParagraphStyle.alloc();
        //        paragraphStyle.lineSpacing = 0
        
        
        //        attrString.addAttribute(NSParagraphStyleAttributeName, value:paragraphStyle, range:NSMakeRange(0, count(text as String)))
        
        return attrString
    }
    
    class func addImageInAttributedString(givenImage: UIImage, yPosition: CGFloat) -> NSMutableAttributedString {
        let iconBound = CGRect(x: 0, y: yPosition, width: givenImage.size.width, height: givenImage.size.height)
        
        let textAttachment = NSTextAttachment()
        textAttachment.image = givenImage
        textAttachment.bounds = iconBound
        
        let attrStringWithImage = NSAttributedString(attachment: textAttachment)
        
        return NSMutableAttributedString(attributedString: attrStringWithImage)
    }
    
    class func planOptionString(firstString: String, secondString: String, boldString: String) -> NSMutableAttributedString {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5.0
        paragraphStyle.lineBreakMode = .byWordWrapping
        paragraphStyle.alignment = .center
        
        let combinedString = (firstString + "\n" + secondString) as NSString
        
        let rangeOfCombinedString = NSMakeRange(0, combinedString.length)
        let rangeOfFirstString = combinedString.range(of: firstString)
        let rangeOfSecondString = combinedString.range(of: secondString)
        let rangeOfBoldString = combinedString.range(of: boldString)
        
        let attrString = NSMutableAttributedString(string:combinedString as String)
        
        attrString.addAttribute(NSForegroundColorAttributeName, value: Singleton.sharedInstance.colorWithHexString("595968"), range: rangeOfCombinedString)
        attrString.addAttribute(NSFontAttributeName, value: UIFont(name: FONT.light, size: 22.0)!, range: rangeOfFirstString)
        
        attrString.addAttribute(NSFontAttributeName, value: UIFont(name: FONT.light, size: 12.0)!, range: rangeOfSecondString)
        
        attrString.addAttribute(NSFontAttributeName, value: UIFont(name: FONT.light, size: 20.0)!, range: rangeOfBoldString)
        
        attrString.addAttribute(NSParagraphStyleAttributeName, value: paragraphStyle, range: rangeOfCombinedString)
        return attrString
    }
    
    class func priceOfPlanString(completeString: String, boldString: String) -> NSMutableAttributedString {
        
        let rangeOfCompleteString = NSMakeRange(0, completeString.characters.count)
        let rangeBoldString = (completeString as NSString).range(of: boldString)
        
        
        let attrString = NSMutableAttributedString(string:completeString as String)
        attrString.addAttribute(NSForegroundColorAttributeName, value: Singleton.sharedInstance.colorWithHexString("595968"), range: rangeOfCompleteString)
        attrString.addAttribute(NSFontAttributeName, value: UIFont(name: FONT.light, size: 11.0)!, range: rangeOfCompleteString)
        
        attrString.addAttribute(NSFontAttributeName, value: UIFont(name: FONT.light, size: 16.0)!, range: rangeBoldString)
        
        return attrString
    }
}
