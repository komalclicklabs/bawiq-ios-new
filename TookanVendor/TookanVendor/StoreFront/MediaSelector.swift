//
//  SelectImage.swift
//  CVIQ_Candidate
//
//  Created by cl-macmini-24 on 11/08/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
import MediaPlayer
import MobileCoreServices
import AVFoundation

enum SelectImageError: LocalizedError {
   case cameraNotFound
   case photoLibraryNotFound
   
   var errorDescription: String? {
      switch self {
      case .cameraNotFound:
         return "Camera not found in this device.".localized
      case .photoLibraryNotFound:
         return "Photo library is not available".localized
      }
   }
}


class MediaSelector: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
   
   struct Result {
      let isSuccessful: Bool
      let error: Error?
      let filePath: String?
   }
   
   enum FileType {
      case video
      case image
      
      func getTitle() -> String {
         switch self {
         case .video:
            return "Take a new video"
         default:
            return "Take a new photo"
         }
      }
   }
   
   // MARK: - Properties
   private var filePath = String()
   private var completion: (Result) -> Void
   
   // MARK: - Intializer
   init(completion: @escaping (Result) -> Void) {
      self.completion = completion
   }

   // MARK: - Methods
   func selectImage(viewController: UIViewController, fileName: String, fileType: FileType) {
      
      let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
      let cameraTitle = fileType.getTitle()
      
      let cameraAction = UIAlertAction(title: cameraTitle, style: .default, handler: {
         [weak self] (alert: UIAlertAction!) -> Void in
         self?.openCameraFor(fileName: fileName, fileType: fileType, inViewController: viewController)
      })
      
      let photoLibraryAction = UIAlertAction(title: "Choose from existing", style: .default, handler: { [weak self]
         (alert: UIAlertAction!) -> Void in
         self?.openPhotoLibraryFor(fileName: fileName, fileType: fileType, inViewController: viewController)
      })
      
      let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
         (alert: UIAlertAction!) -> Void in
      })
      
      actionSheet.addAction(cameraAction)
      actionSheet.addAction(photoLibraryAction)
      actionSheet.addAction(cancelAction)
      
      viewController.present(actionSheet, animated: true, completion: nil)
   }
   
   
   
   func openCameraFor(fileName: String, fileType: FileType, inViewController vc: UIViewController) {
      
      setFilePathWith(fileName: fileName)
      
      if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
         let picker = getImagePicker()
         setPickerForCamera(picker: picker, forFileType: fileType)
         vc.present(picker, animated: true, completion: nil)
      } else {
         let result = Result(isSuccessful: false, error: SelectImageError.cameraNotFound, filePath: nil)
         self.completion(result)
      }
   }
   
   func openPhotoLibraryFor(fileName: String, fileType: FileType, inViewController vc: UIViewController) {
      
      setFilePathWith(fileName: fileName)
      if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
         let picker = getImagePicker()
         self.setPickerForPhotoLibrary(picker: picker, forFileType: fileType)
         vc.present(picker, animated: true, completion: nil)
      } else {
         let result = Result(isSuccessful: false, error: SelectImageError.photoLibraryNotFound, filePath: nil)
         self.completion(result)
      }
      
   }
   
   private func getImagePicker() -> UIImagePickerController {
      let imagePicker = UIImagePickerController()
      imagePicker.delegate = self
      return imagePicker
   }
   
   private func setFilePathWith(fileName: String) {
      filePath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0].appendingFormat("/\(fileName)")
   }
   
   private func setPickerForCamera(picker: UIImagePickerController, forFileType fileType: FileType) {
      picker.sourceType = UIImagePickerControllerSourceType.camera
      
      switch fileType {
      case .video:
         picker.mediaTypes = [kUTTypeMovie as String, kUTTypeGIF as String]
         picker.delegate = self
         picker.videoMaximumDuration = 20.0
      case .image:
         picker.mediaTypes = [kUTTypeImage as String]
         picker.delegate = self
      }
   }
   
   private func setPickerForPhotoLibrary(picker: UIImagePickerController, forFileType fileType: FileType) {
      picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
      if fileType == .video {
         picker.mediaTypes = [kUTTypeMovie as String, kUTTypeGIF as String]
         picker.delegate = self
      } else {
         picker.mediaTypes = ["public.image"]
         picker.delegate = self
      }
   }
   
   //MARK: --
   
   private func rotateCameraImageToProperOrientation(imageSource: UIImage, maxResolution: CGFloat) -> UIImage {
      
      let imgRef = imageSource.cgImage
      
      let width = CGFloat(imgRef!.width)
      let height = CGFloat(imgRef!.height)
      
      var bounds = CGRect.init(x: 0, y: 0, width: width, height: height)
      var scaleRatio: CGFloat = 1
      
      if width > maxResolution || height > maxResolution {
         
         scaleRatio = min(maxResolution / bounds.size.width, maxResolution / bounds.size.height)
         bounds.size.height *= scaleRatio
         bounds.size.width *= scaleRatio
      }
      
      var transform = CGAffineTransform.identity
      let orient = imageSource.imageOrientation
      let imageSize = CGSize(width: imgRef!.width, height: imgRef!.height)
      
      switch imageSource.imageOrientation {
         
      case .up :
         transform = CGAffineTransform.identity
         
      case .upMirrored :
         
         transform = CGAffineTransform(translationX: imageSize.width, y: 0.0)
         transform = transform.scaledBy(x: -1.0, y: 1.0)
         
      case .down :
         
         transform = CGAffineTransform(translationX: imageSize.width, y: imageSize.height)
         transform = transform.rotated(by: CGFloat(Double.pi))
         
      case .downMirrored :
         
         transform = CGAffineTransform(translationX: 0.0, y: imageSize.height)
         transform = transform.scaledBy(x: 1.0, y: -1.0)
         
      case .left :
         
         let storedHeight = bounds.size.height
         bounds.size.height = bounds.size.width
         bounds.size.width = storedHeight
         
         transform = transform.rotated(by: 3.0 * CGFloat(Double.pi) / 2.0)
         
         
      case .leftMirrored :
         
         let storedHeight = bounds.size.height
         bounds.size.height = bounds.size.width
         bounds.size.width = storedHeight;
         transform = CGAffineTransform(translationX: imageSize.height, y: imageSize.width)
         transform = transform.scaledBy(x: -1.0, y: 1.0)
         transform = transform.rotated(by: 3.0 * CGFloat(Double.pi) / 2.0)
         
      case .right :
         
         let storedHeight = bounds.size.height
         
         bounds.size.height = bounds.size.width
         bounds.size.width = storedHeight
         transform = CGAffineTransform(translationX: imageSize.height, y: 0.0)
         transform = transform.rotated(by: CGFloat(Double.pi) / 2.0)
         
      case .rightMirrored :
         
         let storedHeight = bounds.size.height
         bounds.size.height = bounds.size.width
         bounds.size.width = storedHeight
         transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
         transform = transform.rotated(by: CGFloat(Double.pi) / 2.0)
         
      }
      
      UIGraphicsBeginImageContext(bounds.size)
      let context = UIGraphicsGetCurrentContext()
      
      
      if orient == .right || orient == .left {
         
         context!.scaleBy(x: -scaleRatio, y: scaleRatio)
         context!.translateBy(x: -height, y: 0)
         
      } else {
         
         context!.scaleBy(x: scaleRatio, y: -scaleRatio)
         context!.translateBy(x: 0, y: -height)
         
      }
      
      context?.concatenate(transform)

      context?.draw(imgRef!, in: CGRect.init(x: 0, y: 0, width: width, height: height))
      
      let imageCopy = UIGraphicsGetImageFromCurrentImageContext()
      UIGraphicsEndImageContext()
      
      return imageCopy!
   }
   
   // MARK: Image Picker Delegates
   func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
      
      if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
         if UIImageJPEGRepresentation(pickedImage, 1.0)!.count > 2*1024
         {
            do {
               try UIImageJPEGRepresentation(rotateCameraImageToProperOrientation(imageSource: pickedImage, maxResolution: 1024), 0.05)?.write(to: URL(fileURLWithPath: filePath), options: .atomic)
            }catch {
               let result = Result(isSuccessful: false, error: error, filePath: nil)
               completion(result)
            }
         }
         else {
            do {
               try UIImageJPEGRepresentation(rotateCameraImageToProperOrientation(imageSource: pickedImage, maxResolution: 1024), 0.55)?.write(to: URL(fileURLWithPath: filePath), options: .atomic)
            }catch {
               let result = Result(isSuccessful: false, error: error, filePath: nil)
               completion(result)
            }
         }
      }else {
         filePath = (info[UIImagePickerControllerMediaURL] as? NSURL)?.description ?? ""
         
         do {
            let attr: NSDictionary? = try FileManager.default.attributesOfItem(atPath: filePath) as NSDictionary?
         } catch {
            let result = Result(isSuccessful: false, error: error, filePath: nil)
            completion(result)
         }
      }
      
      let result = Result(isSuccessful: true, error: nil, filePath: filePath)
      completion(result)
      picker.dismiss(animated: true, completion: nil)
   }
   
   func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
      picker.dismiss(animated: true, completion: nil)
   }
   
}
