//
//  Config.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 15/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class Config: NSObject {
    
}
//com.product.taxi
//com.clicklabs.TookanVendor

//1 FOR YES / 0 FOR NO


let APP_NAME = "Yelo"
var APP_DEVICE_TYPE =  "797"


let isWhiteLabel = false


/* #################
 1 for enterprise build
 3 for Appointment build
 5 for Deliveries build
 ################# */
let menuHeaderType = 1 //1 For simple top bar //0 for topBarWithImage
let APP_VERSION = 102
let vehicleNotes = ["fleet_vehicle_color", "fleet_vehicle_description", "fleet_license"]


struct allAppDeviceType{
    static var nexTask = "70"
}

struct NexTaskVariable{
    static var package_Type: String = "Package_Type"
    static var Quantity: String = "Quantity"
    static var Distance: String = "Distance"
}

struct APP_STORE {
    static let value = 0
}

let defaultMapType: mapType = mapType.normal

//MARK: FONT SIZE
struct FONT_SIZE {
    static let smallest:CGFloat = 12.0
    static let small:CGFloat = 13.0
    static let medium:CGFloat = 15.0
    static let large:CGFloat = 19.0
    static let buttonTitle:CGFloat = 14.0
    static let extraLarge:CGFloat = 22.0
    static let extraSmall : CGFloat = 7.8
    static let carTypeFontSize: CGFloat = 11
    static let carTimeLabelSize :CGFloat = 10
    static let serviceNameFontSize : CGFloat = 17.0
    static let priceFontSize : CGFloat = 16.0
    static let textfieldSize: CGFloat = 18
}


struct JOB_STATUS_COLOR {
    static let UNASSIGNED =  UIColor(red: 186/255, green: 186/255, blue: 186/255, alpha: 1.0)
    static let ASSIGNED =  UIColor(red: 245/255, green: 169/255, blue: 70/255, alpha: 1.0)
    static let ACCEPTED =  UIColor(red: 186/255, green: 104/255, blue: 200/255, alpha: 1.0)
    static let STARTED =  UIColor(red: 70/255, green: 149/255, blue: 245/255, alpha: 1.0)
    static let INPROGRESS =  UIColor(red: 22/255, green: 107/255, blue: 211/255, alpha: 1.0)
    static let SUCCESSFUL =  UIColor(red: 102/255, green: 208/255, blue: 42/255, alpha: 1.0)
    static let FAILED =  UIColor(red: 226/255, green: 87/255, blue: 76/255, alpha: 1.0)
    static let DECLINED =  UIColor(red: 203/255, green: 53/255, blue: 41/255, alpha: 1.0)
    static let CANCELLED =  UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1.0)
    static let SCHEDULED = UIColor(red: 245/255, green: 169/255, blue: 70/255, alpha: 1.0)
}

//COLOR
struct COLOR {
    static let THEME_FOREGROUND_COLOR = UIColor(red: 225/255, green: 61/255, blue: 54/255, alpha: 1.0)//UIColor(red: 70/255, green: 149/255, blue: 246/255, alpha: 1.0)
    
    static let TITLE_POSITIVE_COLOR = UIColor.white
    static let TITLE_NEGATIVE_COLOR = UIColor(red: 62/255, green: 89/255, blue: 165/255, alpha: 1.0)
    static let BUTTON_BACKGROUND_POSITIVE_COLOR = UIColor(red: 62/255, green: 89/255, blue: 165/255, alpha: 1.0)
    static let BUTTON_BACKGROUND_NEGATIVE_COLOR = UIColor.white
    static let ACTIVE_IMAGE_COLOR =  UIColor(red: 225/255, green: 61/255, blue: 54/255, alpha: 1.0)
    
    
    //Screen BackGround color
    static let SPLASH_BACKGROUND_COLOR = UIColor.white
    
    //Button Title COLOR
    static let LOGIN_BUTTON_TITLE_COLOR = UIColor.white
    
    //Title Underline color
    static let TITLE_UNDER_LINE_COLOR =  UIColor(red: 225/255, green: 61/255, blue: 54/255, alpha: 1.0)//UIColor(red: 70/255, green: 149/255, blue: 246/255, alpha: 1.0)
    
    //TEXTColor
    static let SPLASH_TEXT_COLOR = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1.0)
    static let SIGNIN_TITLE_TYPE_LABEL = UIColor(red: 144/255, green: 144/255, blue: 144/255, alpha: 1)
    
    //PopUpColor
    static let popUpColor = UIColor.white
    
    static let seperatorColor = UIColor.black.withAlphaComponent(0.6)
    static let IN_ACTIVE_IMAGE_COLOR =  UIColor(red: 178/255, green: 178/255, blue: 178/255, alpha: 1.0)
    static let FILLED_IMAGE_COLOR =  UIColor(red: 71/255, green: 71/255, blue: 71/255, alpha: 1.0)
    static let TRANS_COLOR = UIColor(red: 0, green: 0.0, blue: 0.0, alpha: 0.5)
    static let SPLASH_LINE_COLOR = UIColor(red: 219/255, green: 219/255, blue: 219/255, alpha: 1.0)
    static let PLACEHOLDER_COLOR = UIColor(red: 178/255, green: 178/255, blue: 178/255, alpha: 1.0)
    static let ERROR_COLOR = UIColor(red: 226/255, green: 87/255, blue: 76/255, alpha: 0.9)
    static let SUCCESS_COLOR = UIColor(red: 102/255, green: 208/255, blue: 42/255, alpha: 0.9)
    static let REVIEW_BACKGROUND_COLOR = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
    static let DELETE_COLOR = UIColor(red: 226/255, green: 87/255, blue: 76/255, alpha: 1.0)
    static let NOTIFICATION_BACKGROUND_COLOR = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
    static let ADD_NEW_CARD_HEADING_LABEL = UIColor(colorLiteralRed: 146/255, green: 146/255, blue: 146/255, alpha: 1)
    static let border_color = UIColor(colorLiteralRed: 219/255, green: 219/255, blue: 219/255, alpha: 0.5)
    static let skipButtonColor = UIColor(colorLiteralRed: 52/255, green: 56/255, blue: 57/255, alpha: 1)
    static let cancelColor = UIColor(red: 148/255, green: 148/255, blue: 148/255, alpha: 1.0)
    static let refferHeadingColor = UIColor(red: 144/255, green: 144/255, blue: 144/255, alpha: 1.0)
   
   
   static let paymentGreenColor = UIColor(red: 130/255, green: 197/255, blue: 72/255, alpha: 1)
   static let textColorSlightLight = UIColor(red: 89/255, green: 89/255, blue: 104/255, alpha: 1.0)
   static let shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05)
}


enum mapType {
    case white
    case black
    case normal
}
