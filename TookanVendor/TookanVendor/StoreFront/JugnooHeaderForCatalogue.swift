//
//  JugnooHeaderForCatalogue.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 18/09/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
//import Kingfisher


public class JugnooHeaderForCatalogue: UIView {

    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var lowerLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
     private var leftButtonAction: (() -> Void)?
    var rightButtonAction : (() -> Void)?
    
    
    
    @IBAction func backButtonAction(_ sender: UIButton) {
         self.leftButtonAction?()
    }
    
    
    @IBAction func rightButtonAction(_ sender: UIButton) {
        
        rightButtonAction != nil ? rightButtonAction!() : print("nil")
        
    }
    
    
   
    
    func setUiAttributes(){
       self.setShadow()
        headingLabel.font = UIFont(name: FONT.semiBold, size: FONT_SIZE.large)
        headingLabel.textColor = COLOR.SPLASH_BACKGROUND_COLOR
        headingLabel.text = ""
        let backImage = UIImage(named: "back", in: frameworkBundle, compatibleWith: nil)
        backButton.setImage(backImage!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)  , for: UIControlState.normal)
        backButton.tintColor = COLOR.SPLASH_BACKGROUND_COLOR
        
    }
    
    private func setButtonActions(leftButtonAction: (() -> Void)?) {
        self.leftButtonAction = leftButtonAction
    }
    
    func setUpImage(imageUrlString:String){
        let imageUrl = URL.get(from: imageUrlString)
        self.backgroundImage.kf.setImage(with: imageUrl, placeholder: placeholdeImage)
    }
    
    
    func setFrame(withHeight:CGFloat = HEIGHT.jugnooHeaderHeight) {
        frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: withHeight)
    }
    
    
    class func getNibFile(withHeight:CGFloat = HEIGHT.jugnooHeaderHeight , leftButtonAction: (() -> Void)?) -> JugnooHeaderForCatalogue {
        
        let navBar = frameworkBundle!.loadNibNamed(NIB_NAME.JugnooHeaderForCatalogue, owner: self, options: nil)?.first as! JugnooHeaderForCatalogue
        
        navBar.setFrame(withHeight: withHeight)
        navBar.setButtonActions(leftButtonAction: leftButtonAction)
        navBar.setUiAttributes()
        
        return navBar
    }
    
    
    
}
