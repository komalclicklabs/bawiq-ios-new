//
//  ProductFilterScreen.swift
//  Jugnoo Autos
//
//  Created by Gagandeep  on 20/01/17.
//  Copyright © 2017 Socomo Technologies. All rights reserved.
//

import UIKit



enum sortType{
    case alphabetically
    case priceHigh
    case priceLow
}

enum SortScreenOption: Int {
    case Fresh = 0
    case Menus = 1
}

protocol ProductFilterScreenDelegate {
    func productFilterScreen(productFilterScreen:ProductFilterScreen, clickedFilterButton:UIButton)
    func updateGivenProductsDataAfterSortOptionSelection(productFilterScreen:ProductFilterScreen, selectedSortOption: Int)
}

class ProductFilterScreen: UIView, UITableViewDelegate, UITableViewDataSource {
    
    
    var filterOptionsArray = [sortCellClass("A-Z",sortType.alphabetically)  ,sortCellClass("Price: Low to High",sortType.priceLow) , sortCellClass("Price: High to Low",sortType.priceHigh)]
    
    @IBOutlet var mainBackgroundView: UIView!
    @IBOutlet var backgroundViewOfTableView: UIView!
    @IBOutlet var tableViewOfFilter: UITableView!
    @IBOutlet var heightOfBackgroundTableView: NSLayoutConstraint!
    @IBOutlet weak var filterButton: UIButton!
    
    var selectedSortType = sortType.alphabetically
    var productFilterScreenDelegate: ProductFilterScreenDelegate? = nil
    var sortScreenIsFrom = SortScreenOption.Fresh.rawValue
    var selectedSortOptionForMenus = -1
    
    var onSortCellClick:((_ sortType:sortType)->Void)?
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        
        self.heightOfBackgroundTableView.constant = 0
        UIView.animate(withDuration: 0.4, animations: {
            //self.filterButton.alpha = 1
            //self.layoutIfNeeded()
            self.alpha = 0
            self.mainBackgroundView.backgroundColor = UIColor.clear
            self.layoutIfNeeded()
        }, completion: { (success) in
            self.removeFromSuperview()
        })
    }
    
    class func instanceProductFilterScreenFromNib() -> ProductFilterScreen {
        return UINib(nibName: "ProductFilterScreen", bundle: frameworkBundle).instantiate(withOwner: nil, options: nil)[0] as! ProductFilterScreen
    }
    
    func registerNib() {
      
//        var contentInset:UIEdgeInsets = tableViewOfFilter.contentInset
//        contentInset.top = 25.0
//        tableViewOfFilter.contentInset = contentInset
        //self.filterButton.setImage(#imageLiteral(resourceName: "ic_freshfiltersIcon"), for: .normal)
        tableViewOfFilter.register(UINib.init(nibName: "FilterOptionTableViewCell", bundle: frameworkBundle), forCellReuseIdentifier: "FilterOptionTableViewCell")
        tableViewOfFilter.delegate = self
        tableViewOfFilter.dataSource = self
       // tableViewOfFilter.delegate = self
       // tableViewOfFilter.dataSource = self
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if productFilterScreenDelegate != nil {
            productFilterScreenDelegate?.productFilterScreen(productFilterScreen: self, clickedFilterButton: UIButton())
        }
    }
    //MARK: - UITableView Delegate And DataSource Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterOptionsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if sortScreenIsFrom == SortScreenOption.Menus.rawValue {
            let nameOfSort = filterOptionsArray[indexPath.row].title
            
            if nameOfSort == "Popularity" {
                return 0
            }
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 10.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print("HERE")
        if let cell = tableView.dequeueReusableCell(withIdentifier: "FilterOptionTableViewCell", for: indexPath) as? FilterOptionTableViewCell {
            print("THERE")
            cell.backgroundColor = UIColor.clear
            cell.clipsToBounds = true
            cell.selectionStyle = .none
            cell.horizontalLineView.isHidden = false
            
            cell.filterLabel.text = filterOptionsArray[indexPath.row].title
            print("WHERE")
            cell.bgView.backgroundColor = UIColor.white
            
                if selectedSortOptionForMenus == indexPath.row {
                    cell.tickIconImageView.isHidden = false
                    cell.filterLabel.font = UIFont(name: FONT.light, size: 14.0)
                    cell.filterLabel.textColor = UIColor(colorLiteralRed: 89.0, green: 89.0, blue: 104.0, alpha: 1.0)
                } else {
                    cell.tickIconImageView.isHidden = true
                    cell.filterLabel.font = UIFont(name: FONT.light, size: 14.0)
                    cell.filterLabel.textColor = UIColor(colorLiteralRed: 89.0, green: 89.0, blue: 104.0, alpha: 1.0)
                }
            
            cell.isSelectedSortType = filterOptionsArray[indexPath.row].sortType == self.selectedSortType
            
            
            cell.filterLabel.textColor = COLOR.THEME_FOREGROUND_COLOR
            
            let totalNumberOfRows = self.tableView(tableView, numberOfRowsInSection: indexPath.section)
            
            if indexPath.row == (totalNumberOfRows - 1) {
                cell.horizontalLineView.isHidden = true
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       self.selectedSortType = filterOptionsArray[indexPath.row].sortType!
        self.onSortCellClick!(self.selectedSortType)
        self.tableViewOfFilter.reloadData()
        self.backButtonAction(UIButton())
    }
    
    //MARK: - UIButton Action Methods
    
    @IBAction func filterButtonAction(_ sender: UIButton) {
        if productFilterScreenDelegate != nil {
            productFilterScreenDelegate?.productFilterScreen(productFilterScreen: self, clickedFilterButton: sender)
        }
    }
    
 
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

class sortCellClass{
    var title = String()
    var sortType : sortType?
    init(_ title:String, _ type:sortType) {
        self.title = title
        self.sortType = type
    }
    
    
    
}

