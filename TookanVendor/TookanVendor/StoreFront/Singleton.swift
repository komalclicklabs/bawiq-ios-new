//
//  Singleton.swift
//  StoreFront
//
//  Created by Samneet Kharbanda on 20/09/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation
class Singleton{
    var createTaskDetail = CreateTaskDetails(json: [:])
    var customFieldOptionsPickup = [String:Any]()
    var customFieldOptionsDelivery = [String:Any]()
    static let sharedInstance = Singleton()
    var allImagesCache = NSCache<NSString,UIImage>()
    var promoAccessId = ""
    var promoId = ""
    var couponsArray = [PromoCodeModal]()
    var typeCategories = [CarTypeModal]()
    
    func resetPickupMetaData() {
        Singleton.sharedInstance.createTaskDetail.pickupMetadata = [Any]()
        if let items = self.customFieldOptionsPickup["items"] as? [Any] {
            for item in items {
                let customField = CustomFieldDetails(json: item as! [String : Any])
               switch customField.dataType {
               case .checklist, .table:
                  break
               default:
                    Singleton.sharedInstance.createTaskDetail.pickupMetadata.append(customField)
                    break
                }
            }
        }
    }
    
    
    func getTimeZone() -> Int {
        let timezone = -(NSTimeZone.system.secondsFromGMT() / 60)
        return timezone
    }
    
    func resetPickupCustomField() {
        Singleton.sharedInstance.createTaskDetail.metaData = [Any]()
        if let items = self.customFieldOptionsPickup["items"] as? [Any] {
            for item in items {
                let customField = CustomFieldDetails(json: item as! [String : Any])
                switch customField.dataType {
                case .checklist, .table:
                    break
                default:
                    Singleton.sharedInstance.createTaskDetail.metaData.append(customField)
                    break
                }
            }
        }
    }
    
    func resetDeliveryCustonField() {
        Singleton.sharedInstance.createTaskDetail.metaData = [Any]()
        if let items = self.customFieldOptionsDelivery["items"] as? [Any] {
            for item in items {
                let customField = CustomFieldDetails(json: item as! [String : Any])
                switch customField.dataType {
                case .checklist, .table:
                    break
                default:
                    Singleton.sharedInstance.createTaskDetail.metaData.append(customField)
                    break
                }
            }
        }
    }
    
    
    
    func resetMetaData() {
        switch Singleton.sharedInstance.getTaskTypeTosetDetailsForCreateTask() {
        case PICKUP_DELIVERY.pickup:
            self.resetPickupCustomField()
            break
        case PICKUP_DELIVERY.delivery:
            self.resetPickupCustomField()
            break
        case PICKUP_DELIVERY.both, PICKUP_DELIVERY.addDeliveryDetails:
            self.resetDeliveryCustonField()
            break
        }
        
        
    }
    
    
    func getTaskTypeTosetDetailsForCreateTask() -> PICKUP_DELIVERY {
        switch FormDetails.currentForm.work_flow {
        case WORKFLOW.pickupDelivery.rawValue?:
            switch FormDetails.currentForm.pickup_delivery_flag {
            case PICKUP_DELIVERY.pickup.rawValue?:
                return PICKUP_DELIVERY.pickup
            case PICKUP_DELIVERY.delivery.rawValue?:
                return PICKUP_DELIVERY.delivery
            case PICKUP_DELIVERY.both.rawValue?:
                return PICKUP_DELIVERY.both
            case PICKUP_DELIVERY.addDeliveryDetails.rawValue?:
                return PICKUP_DELIVERY.addDeliveryDetails
            default:
                break
            }
            break
        case WORKFLOW.appointment.rawValue?:
            return PICKUP_DELIVERY.delivery
        case WORKFLOW.fieldWorkforce.rawValue?:
            return PICKUP_DELIVERY.delivery
        default:
            break
        }
        return PICKUP_DELIVERY.pickup
    }
    
    
    func validateEmail(_ candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
    func validPassword(password:String) -> Bool {
        if password.length >= 6 {
            return true
        }
        return false
    }
    
    func validateDate(jobDate:String) -> Bool {
        if jobDate.length > 0 {
            let dateFormatter = DateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"//"MM/dd/yyyy HH:mm"
            let startDate = dateFormatter.date(from: jobDate)
            if(Int((startDate?.timeIntervalSince(Date()))!) > 0) {
                return true
            }
        }
        return false
    }
    
    func comparePickupAndDeliveryDate(pickupDate:String, deliveryDate:String,greaterMessage:String = "Start time should be before end time" , bufferMessage : String = "There should be buffer of atleast 15 mins between start time and end time.") -> Bool {
        if pickupDate.length > 0 && deliveryDate.length > 0 {
            let dateFormatter = DateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"//"MM/dd/yyyy HH:mm"
            let pickupDate = dateFormatter.date(from: pickupDate)
            let deliveryDate = dateFormatter.date(from: deliveryDate)
            let timeInterval = Int((deliveryDate?.timeIntervalSince(pickupDate!))!)
            if(timeInterval > 60*15) {
                return true
            }else if timeInterval < 0{
                ErrorView.showWith(message: greaterMessage, isErrorMessage: true, removed: nil)
            }else{
                ErrorView.showWith(message: bufferMessage, isErrorMessage: true, removed: nil)
            }
        }
        return false
    }
    
    func validatePhoneNumber(phoneNumber:String) -> Bool {
        let array = phoneNumber.components(separatedBy: " ")
        if array.count > 1 {
            let number = array[1]
            if number.length >= 8 && number.length <= 15  {
                return true
            }
        } else {
            let number = array[0]
            if number.length >= 8 && number.length <= 15  {
                return true
            }
        }
        
        return false
    }
    
    func validateAddress(address:String, latitude:String, longitude:String) -> Bool {
        guard address.length > 0 else {
            return false
        }
        
        guard latitude.length > 0 else {
            return false
        }
        
        guard Double(latitude) != 0.0 else {
            return false
        }
        
        guard longitude.length > 0 else {
            return false
        }
        
        guard Double(longitude) != 0.0 else {
            return false
        }
        
        return true
    }
    
    func verifyUrl (_ urlString: String?) -> Bool {
        //Check for nil
        if let _ = URL(string: urlString!){
            return true
        }
        return false
    }
    
    
    func resetDeliveryDetails() {
        self.createTaskDetail.customerAddress = TEXT.ADD_DELIVERY_LOCATION
        self.createTaskDetail.latitude = ""
        self.createTaskDetail.longitude = ""
        self.createTaskDetail.customerUsername = ""
        self.createTaskDetail.customerEmail = ""
        self.createTaskDetail.customerPhone = ""
        self.createTaskDetail.jobDeliveryDatetime = ""
        self.resetMetaData()
    }
    
    func resetPickupDetails() {
        self.createTaskDetail.jobPickupAddress = TEXT.ADD_PICKUP_LOCATION
        self.createTaskDetail.jobPickupLatitude = ""
        self.createTaskDetail.jobPickupLongitude = ""
        self.createTaskDetail.jobPickupName = ""
        self.createTaskDetail.jobPickupEmail = ""
        self.createTaskDetail.jobPickupPhone = ""
        self.createTaskDetail.jobPickupDateTime = ""
        self.resetPickupMetaData()
    }
    
    func isFileExistAtPath(_ path:String) -> Bool {
        //  print(path)
        let filePath = self.createPath(path)
        let fileManager = FileManager.default
        return fileManager.fileExists(atPath: filePath)
    }
    
    func createPath(_ imageName:String) -> String {
        let fileManager = FileManager.default
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = URL(fileURLWithPath: path)
        let imageDirUrl = url.appendingPathComponent("Image")
        var isDirectory:ObjCBool = false
        if fileManager.fileExists(atPath: imageDirUrl.path, isDirectory: &isDirectory) == false {
            do {
                try FileManager.default.createDirectory(atPath: imageDirUrl.path, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError {
                print(error.localizedDescription);
            }
        }
        let filePath = imageDirUrl.appendingPathComponent(imageName).path
        return filePath
    }
    
    func showAlertWithOption(owner:UIViewController,title:String = "",message:String,showRight:Bool = false,leftButtonAction: (()->Void)?,rightButtonAction: (()->Void)? = nil ,leftButtonTitle:String = "No",rightButtonTitle :String = "Yes"){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let rightButtonAction = UIAlertAction(title: rightButtonTitle, style: UIAlertActionStyle.default) { (action) in
            if rightButtonAction != nil {
                rightButtonAction!()
            }
        }
        
        
        let leftButtonAction = UIAlertAction(title: leftButtonTitle, style: UIAlertActionStyle.default) { (action) in
            leftButtonAction?()
        }
        alert.addAction(leftButtonAction)
        if showRight == true{
            alert.addAction(rightButtonAction)
        }
        
        DispatchQueue.main.async {
            owner.present(alert, animated: true, completion: nil)
            
        }
    }
    
    
    func convertDateToString() -> String {
        let styler = DateFormatter()
        styler.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
        styler.timeZone = TimeZone(abbreviation:  "UTC")
        styler.dateFormat = "yyyy-MM-dd_HH:mm:ss"
        return styler.string(from: Date())
    }
    
    func deleteImageFromDocumentDirectory(_ imagePath:String) {
        // print(imagePath)
        let fileManager = FileManager.default
        let filePath = self.createPath(imagePath)
        do {
            try fileManager.removeItem(atPath: filePath)
        } catch let error as NSError {
            print("Error in removing Path = \(error.description)")
        }
    }
    
    
    func saveImageToDocumentDirectory(_ imageName:String, image:UIImage) -> String {
        let filePath = self.createPath(imageName)
        let pngImageData = UIImageJPEGRepresentation(image, 0.1)
        let fileManager = FileManager.default
        if(fileManager.fileExists(atPath: filePath) == true) {
            do {
                try fileManager.removeItem(atPath: filePath)
                self.allImagesCache.removeObject(forKey: "\(imageName)" as NSString)
            } catch let error as NSError {
                print("Error in removing Path = \(error.description)")
            }
        }
        try? pngImageData!.write(to: URL(fileURLWithPath: filePath), options: [.atomic])
        return imageName
    }
    
    func enableDisableIQKeyboard(enable:Bool){
        
//        IQKeyboardManager.shared().isEnabled = enable
//        IQKeyboardManager.shared().isEnableAutoToolbar = enable
//        IQKeyboardManager.shared().shouldShowTextFieldPlaceholder = false
    }
    
    func getPromoCodeArray(data:[String:Any]){
        print(data)
        couponsArray.removeAll()
        if let value = data["coupons"] as? [Any]{
            for i in value{
                couponsArray.append(PromoCodeModal(json: i as! [String : Any]))
            }
            
        }
        
        if let  promos = data["promos"] as? [Any]{
            for i in promos{
                couponsArray.append(PromoCodeModal(json: i as! [String : Any]))
            }
        }
    }
    
    
    func convertPriceToString(value:Double) -> String {
        
        let doubleSortVal = value
        let intSortVal = Int(value)
        
        if doubleSortVal - Double(intSortVal) == 0 {
            
            return "\(Int(value))"
        }
        else {
            return "\(value)"
        }
    }
    
    func adjustHeight(value:CGFloat) -> CGFloat {
        
        return (UIScreen.main.bounds.height*value)/568;
    }
    
    func colorWithHexString (_ hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()//NSString(hex).stringByTrimmingCharactersInSet(.whitespaceAndNewlineCharacterSet).uppercased()//hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.characters.count != 6) {
            return UIColor.gray
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    
    func colorWithHexWithAlphaString (_ hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()//NSString(hex).stringByTrimmingCharactersInSet(.whitespaceAndNewlineCharacterSet).uppercased()//hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.characters.count != 6) {
            return UIColor.gray
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(0.3))
    }
    
    func colorFromRGB (_ red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor{
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
    }

    func roundCorners(corners:UIRectCorner, radius: CGFloat, view: UIView) {
        let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        view.layer.mask = mask
    }
    
    func getTimeInMinsFromDate(timeString:String) -> Int {
        if timeString.characters.count > 0 {
            let timeArray = timeString.components(separatedBy: ":")
            if timeArray.count > 1 {
                let totalTimeInMin = Int(timeArray[0])!*60 + Int(timeArray[1])!
                
                return totalTimeInMin
            }
        }
        return 0
    }
    
    func convert24HourTo12Hour(_ date:String) ->String {
        
        if date == "" || date.isEmpty {
            return ""
        }
        else {
            let estDf: DateFormatter = DateFormatter()
            
            estDf.dateFormat = "HH:mm:ss";
            if let newDate = estDf.date(from: date) {
                estDf.dateFormat = "h:mm a"
                let pmamDateString = estDf.string(from: newDate)
                
                return pmamDateString
            }
            return ""
            
        }
        
    }
    
    func getTimeInMinsFromCurrentDate() -> Int {
        
        let date = Date()
        let calendar = NSCalendar.current
        let hour = calendar.component(.hour, from: date as Date)
        let minutes = calendar.component(.minute, from: date as Date)
        
        let totalTimeInMin = hour*60 + minutes
        
        return totalTimeInMin
        
    }
    
   
    
    
}
