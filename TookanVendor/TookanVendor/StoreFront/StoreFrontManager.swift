//
//  StoreFrontManager.swift
//  StoreFront
//
//  Created by cl-macmini-45 on 19/09/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps


public enum CallingType {
   case catalogue
   case orderHistory
}

 struct Merchant {
    var key = ""
    var referenceId = ""
    var vedorRefferenceId = ""
    var email = ""
    var phone = ""
    var name = ""
    
    var startLoadingBlock : (()->Void)?
    var stopLoadingBlock : (()->Void)?
    var completionHandler : ((_ createTaskDetails: [String: Any])->Void)?
    var isDemo = false
    static var shared: Merchant?
    
    init(key : String,referenceId:String,vedorRefferenceId:String,email:String,phone:String,name:String,startLoadingblockRef : (()->Void)? , stopLoadingBlockRef : (()->Void)?,completionHandler :((_ createTaskDetails: [String: Any])->Void)?) {
        self.email = email
        self.phone = phone
        self.referenceId = referenceId
        self.key = key
        self.vedorRefferenceId = vedorRefferenceId
        self.name = name
        self.startLoadingBlock = startLoadingblockRef
        self.stopLoadingBlock = stopLoadingBlockRef
      self.completionHandler = completionHandler
    }
    
    
    func startLoading(){
        self.startLoadingBlock == nil ? print("") : self.startLoadingBlock!()
    }
    
    func stopLoding(){
        self.stopLoadingBlock == nil ? print("") : self.stopLoadingBlock!()
    }
    
    
    
    func getParametersToAuthenticateMerchant() -> [String:String] {
        var params = ["api_key":self.key]
        params["merchant_reference_id"] = self.referenceId
        params["vendor_reference_id"] = self.vedorRefferenceId
        params["email"] = self.email
        params["phone"] = self.phone
        params["app_device_type"] = "\(APP_DEVICE_TYPE)"
        
        print("merchant params")
        print(params)
        
        
        return params
    }
}

public class StoreFrontManager: NSObject {
    var levelFlowManager : NLevelFlowManager?
    public static var shared = StoreFrontManager()
//    private var merchant = Merchant()
    private var merchantAppController:UINavigationController?
    
    public override init() {
    }
    

    public var companyName:String = ""
    public var companyLogo:String = ""
    public var startLoadingBlock : (()->Void)?
    public var stopLoadingBlock : (()->Void)?
    public var completionHandler : ((_ createTaskDetails: [String: Any])->Void)?
    public func changeAppDeviceType(deviceToken:String){
        APP_DEVICE_TYPE = deviceToken
    }
    public func setCompanyName(name:String) {
        self.companyName = name
    }
    
    // MARK: - Server Hit
    public func checkAuthentication(merchantKey: String, referenceId:
        String,vedorRefferenceId:String,email:String,phone:String,name:String, type: CallingType = CallingType.catalogue, navigationController:UINavigationController,isDemo : Bool = false, completion: ((Bool, [String:Any]?) -> Void)?) {
      //self.completionHandler = nil
      
        Merchant.shared = Merchant(key: merchantKey, referenceId: referenceId, vedorRefferenceId: vedorRefferenceId, email: email, phone: phone, name: name, startLoadingblockRef: startLoadingBlock, stopLoadingBlockRef: stopLoadingBlock,completionHandler:completionHandler)
      Merchant.shared?.isDemo = isDemo
      registerForGoogle()
         resgisterFont()
        self.merchantAppController = navigationController
//        ActivityIndicator.stopAnimatingView()
        startLoadingBlock == nil ? print("") : startLoadingBlock!()
        let params = Merchant.shared!.getParametersToAuthenticateMerchant()
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.authenticate_both, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) {[weak self] (isSucceeded, response) in
            self?.stopLoadingBlock == nil ? print("") : self?.stopLoadingBlock!()
            DispatchQueue.main.async {
                print("in")
                if isSucceeded == true {
                    if let data = response["data"] as? [String:Any] {
                        Vendor.current = Vendor.init(json:data)
                        if let offeringArray = data["offerings"] as? [String:Any]{
                            FormDetails.currentForm = FormDetails(json: offeringArray)
                            if let userOptions = offeringArray["userOptions"] as? [String:Any]{
                                Singleton.sharedInstance.customFieldOptionsPickup = userOptions
                            }
                        }else{
                            print("offering issue")
                        }
                        
                        if let config = data["app_configurations"] as? [String:Any]{
                            AppConfiguration.current = AppConfiguration(json:config)
                        }else{
                            print("config issue")
                        }
                        
                        if let vendorData = data["vendorDetails"] as? [String:Any]{
                            Vendor.current = Vendor(json: vendorData)
                        }else{
                            print("vendor issue")
                        }
                        
                        if let api_key = data["api_key"] as? String{
                            FormDetails.currentForm.apiKey = api_key
                        }else if let api_key = data["api_key"] as? Int{
                            FormDetails.currentForm.apiKey = "\(api_key)"
                        }
                        
                        if let value = data["minimum_order_value"] as? Int{
                            FormDetails.currentForm.minimumOrderValue = Double(value)
                        }else if let value = data["minimum_order_value"] as? Double{
                            FormDetails.currentForm.minimumOrderValue = Double(value)
                        }
                     if type == .catalogue {
                        self?.setCatalogue()
                     } else {
                        //self?.setCatalogue()
                        self?.setOrderDetails()
                     }
                     print("here")
                        print("success")
                        completion?(true, nil)
                    } else {
                        print("error 2")
                        completion?(false, ["error":response["message"]!])
                    }
                } else {
                    ErrorView.showWith(message: response["message"] as! String, removed: nil)
                    print("error 1")
                    completion?(false, ["error":response["message"]!])
                }
            }
        }
    }
    
   public func setCatalogue() {
        print("setCataloge")
        Catalogue.getCatalogueOfFormWith(formId: "", showActivityIndicator: true, showAlert: true) {[weak self] success, catalogue, error in
            if success {
//                print(catalogue)
                self?.levelFlowManager = NLevelFlowManager(navigationController: (self?.merchantAppController)!, catalogue: catalogue!)
                self?.levelFlowManager?.startFlow()
            } else {
                print("faliure")
            }
        }
    }
   
   
   public func setOrderDetails() {
      
      var changeVC : OrderHistoryVC!
      let storyboard = UIStoryboard(name: STORYBOARD_NAME.afterLogin, bundle: frameworkBundle)
      changeVC = storyboard.instantiateViewController(withIdentifier: STORYBOARD_ID.orderHistoryVC) as! OrderHistoryVC
      self.merchantAppController?.pushViewController(changeVC, animated: true)
   }
   
    
   public func registerForGoogle(){
        GMSPlacesClient.provideAPIKey("AIzaSyCx_xRaB_xQwbwxk89CuP_6qArT2E5Uun4")
         GMSServices.provideAPIKey("AIzaSyCx_xRaB_xQwbwxk89CuP_6qArT2E5Uun4")
    }
    
   public func resgisterFont(){
        UIFont.registerFontWithFilenameString(filenameString: FONT.semiBold + ".ttf", bundleIdentifierString: "com.clicklabs.StoreFront")
        UIFont.registerFontWithFilenameString(filenameString: FONT.regular + ".ttf", bundleIdentifierString: "com.clicklabs.StoreFront")
        UIFont.registerFontWithFilenameString(filenameString: FONT.light + ".ttf", bundleIdentifierString: "com.clicklabs.StoreFront")
        UIFont.registerFontWithFilenameString(filenameString: FONT.bold + ".ttf", bundleIdentifierString: "com.clicklabs.StoreFront")
        
    }

}

