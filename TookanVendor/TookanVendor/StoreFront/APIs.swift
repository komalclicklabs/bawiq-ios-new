//  APIs.swift
//  Tookan
//  Created by cl-macmini-45 on 09/01/17.
//  Copyright © 2017 Click Labs. All rights reserved.

import UIKit

class APIs: NSObject {

}

struct SERVER {
    static let live = "http://test-api.yelo.red:3001/v2/"//"https://beta-api.yelo.red/v2/"//"https://test-api.yelo.red/v2/"//"http://test.tookanapp.com:8033/v2/"//"https://api.yelo.red/v2/""https://api.yelo.red/v2/"//
}

struct HTTP_METHOD {
    static let POST = "POST"
    static let GET = "GET"
    static let PUT = "PUT"
}

struct STATUS_CODES {
    static let INVALID_ACCESS_TOKEN = 101
    static let BAD_REQUEST = 400
    static let UNAUTHORIZED_ACCESS = 401
    static let PICKUP_TASK = 410
    static let ERROR_IN_EXECUTION = 500
    static let SHOW_ERROR_MESSAGE = 304
    static let NOT_FOUND_MESSAGE = 404
    static let UNAUTHORIZED_FOR_AVAILABILITY = 210
    static let SHOW_MESSAGE = 201
    static let SHOW_DATA = 200
    static let SLOW_INTERNET_CONNECTION = 999
    static let DELETED_TASK = 501
}

struct API_NAME {
    static let authenticate_merchant = "authenticate_merchant"
    static let authenticate_both = "authenticate_merchant_and_vendor"
    static let getProductsForCategory = "get_products_for_category"
    static let getAppCatalogue = "get_app_catalogue"
    static let getPayment = "send_payment_for_task"
    static let createTask = "create_task_via_vendor"
    static let fetchAppConfiguration = "fetch_app_configuration"
    static let checkEmailRegistered = "check_email_exists"
    static let signup = "vendor_signup"
    static let sendReferral = "apply_referral"
    static let loginEmail = "vendor_login"
    static let loginAccessToken = "vendor_login_via_access_token"
    static let forgotPass = "vendor_forgot_password"
    static let changePassword = "change_vendor_password"
    //POST /edit_vendor_profile
    static let editProfile = "edit_vendor_profile"
    static let getOrderHistory = "get_order_history"
    static let trackAgent = "track_agent"
    static let fetchCards = "fetch_merchant_cards"
    static let getFavouriteLocation = "get_fav_location"
    static let deleteFavLocation = "delete_fav_location"
    static let addFavLocation = "add_fav_location"
    static let getAgents = "get_service_providers"
    static let editFavLocation = "edit_fav_location"
    static let assignPromo = "assign_coupon"
   static let uploadImageForReference = "upload_reference_images"

   
}


