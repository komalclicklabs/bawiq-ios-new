//
//  HTTPClient.swift
//  Pally Asia
//  
//  Created by cl-macmini-117 on 26/09/16.
//  Copyright © 2015 CilckLabs. All rights reserved.
//
import Foundation
//import FBSDKLoginKit
//import FBSDKCoreKit

enum HttpMethodType: String {
    case GET
    case POST
    case PUT
    case DELETE
}

enum APIErrors: LocalizedError {
    case notConvertible
    case statusCodeNotFound
  case serverThrewError(message: String)
  
   var errorDescription: String? {
      switch self {
      case .serverThrewError(message: let errorMessage):
         return errorMessage
      default:
         return ERROR_MESSAGE.SERVER_NOT_RESPONDING
      }
   }
}

struct NetworkError: LocalizedError {
   var errorDescription: String? {
      return ERROR_MESSAGE.NO_INTERNET_CONNECTION
   }
}

class HTTPClient {
    
    // MARK: - Properties
//    var loginManager: FBSDKLoginManager!
    var dataTask: URLSessionDataTask?
    
    // MARK: - Type Properties
    static var shared = HTTPClient()
    
    static var baseUrl: String {
        return "http://test-api.yelo.red:3001/v2/"//"https://beta-api.yelo.red/v2/"//"https://test-api.yelo.red/v2/"//"https://api.yelo.red/v2/"//"https://api.yelo.red/v2/"//"http://test.tookanapp.com:8033/v2/"
    }
    

    typealias ServiceResponse = (_ responseObject: Any?, _ error: Error?, _ extendedUrl: String?, _ statusCode: Int?) -> Void
    
    // MARK: - Methods
    func makeSingletonConnectionWith(method: HttpMethodType, showAlert: Bool = true, showAlertInDefaultCase: Bool = true, showActivityIndicator: Bool = true, para: [String: Any]? = nil, baseUrl: String = HTTPClient.baseUrl, extendedUrl: String, callback: @escaping ServiceResponse) {
        
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        dataTask = HTTPClient.makeConcurrentConnectionWith(method: method, showAlert: showAlert, showAlertInDefaultCase: showAlertInDefaultCase, showActivityIndicator: showActivityIndicator, para: para, baseUrl: baseUrl, extendedUrl: extendedUrl, callback: callback)
    }
  
  
  
    // MARK: - Type Methods
  
///     Concurrent connection with server.
  
///      - parameter method:  Pass Http Method like .GET.
///       - parameter showAlert: should show alert like no internet connection etc.
///       - parameter showAlertInDefaultCase:  Should show alert in case connection was successful but resulted in error.
///       - parameter para: Parameters to send to server.
///       - parameter extendedUrl: url afer baseurl
///      - parameter callback: responce from server
    
    @discardableResult
    class func makeConcurrentConnectionWith(method: HttpMethodType, showAlert: Bool = true, showAlertInDefaultCase: Bool = true, showActivityIndicator: Bool = true, para: [String: Any]? = nil, baseUrl: String = HTTPClient.baseUrl, extendedUrl: String, callback: @escaping ServiceResponse) -> URLSessionDataTask? {
      
      guard isConnectedToNetwork() else {
        let error = NetworkError()
        callback(nil, error, nil, 404)
        showAlert ? ErrorView.showWith(message: ERROR_MESSAGE.NO_INTERNET_CONNECTION, removed: nil) : ()
        return nil
      }
        
        //Request
        var mutableRequest = HTTPClient.createRequestWith(method: method, timeout: 30, baseUrl: baseUrl, extendedUrl: extendedUrl, contentType: "application/json")
        print(baseUrl+extendedUrl)
        
        showActivityIndicator ? HTTPClient.startAnimatingActivityIndicator(): ()
        
        if para != nil {
            if let body = try? JSONSerialization.data(withJSONObject: para!, options: []) {
                mutableRequest.httpBody = body
            }
        }
        
        //DataTask
        let dataTask = HTTPClient.performDataTaskWith(request: mutableRequest, showAlert: showAlert, showAlertInDefaultCase: showAlertInDefaultCase, showActivityIndicator: showActivityIndicator, callback: callback, extendedUrl: extendedUrl)

        dataTask.resume()
        
        return dataTask
    }
    
    ///     Multipart form-data connection with server.
    
    ///      - parameter method:  Pass Http Method like .GET.
    ///       - parameter showAlert: should show alert like no internet connection etc.
    ///       - parameter showAlertInDefaultCase:  Should show alert in case connection was successful but resulted in error.
    ///       - parameter para: Parameters to send to server.
    ///       - parameter extendedUrl: url afer baseurl
    ///       - parameter imageList: Dictionary type list whose 'key' represents key of the parameter against which image is to be sent to server and 'value' of respective key is path of the image or array of paths in Directory.
    ///      - parameter callback: responce from server
//    @discardableResult
    class func makeMultiPartRequestWith(method: HttpMethodType, showAlert: Bool = true, showAlertInDefaultCase: Bool = true, showActivityIndicator: Bool = true, para: [String: Any]? = nil, baseUrl: String = HTTPClient.baseUrl, extendedUrl: String, imageList: [String: Any]? = nil, callback: @escaping ServiceResponse) {
      
      guard isConnectedToNetwork() else {
        let error = NetworkError()
        callback(nil, error, nil, 404)
        showAlert ? ErrorView.showWith(message: error.localizedDescription, removed: nil) : ()
        return
      }

        let boundary = "Boundary+\(arc4random())\(arc4random())"
        
        showActivityIndicator ? HTTPClient.startAnimatingActivityIndicator(): ()
        
        let timeout: Double = 30 + Double(15 * (imageList?.count ?? 0))
        var mutableRequest = HTTPClient.createRequestWith(method: method, timeout: timeout, baseUrl: baseUrl, extendedUrl: extendedUrl, contentType: "multipart/form-data; boundary=\(boundary)")
        
        var body = Data()
        
        //Image upload
        if imageList != nil {
            for (key, path) in imageList! {
                
                if let arrayOfPaths = path as? [String] {
                    for tempPath in arrayOfPaths {
                        body.appendImageWith(key: key, path: tempPath, boundary: boundary)
                    }
                    continue
                } else if let pathString = path as? String {
                    body.appendImageWith(key: key, path: pathString, boundary: boundary)
                } else {
                    print("Error -> Not valid path of image in imageList")
                }
                
            }
        }
        
        //appending parameters
        if para != nil {
            
            for (key, value) in para! {
                
                if value is [String: Any] || value is [Any] {
                    
                do {
                        body.append(boundary: boundary)
                        let data  = try JSONSerialization.data(withJSONObject: value, options: JSONSerialization.WritingOptions.prettyPrinted )
                        let jsonString: NSString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)!
                        body.appendParameter(name: key)
                        body.append("\(jsonString)\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
                    } catch let error as NSError {
                        print("json error: \(error.localizedDescription)")
                    }
                    
                } else {
                    
                    body.append(boundary: boundary)
                    body.appendParameter(name: key)
                    body.append("\(value)\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
                }
            }
        }
        
        body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        
        mutableRequest.httpBody = body
        
        let dataTask = performDataTaskWith(request: mutableRequest, showAlert: showAlert, showAlertInDefaultCase: showAlertInDefaultCase, showActivityIndicator: showActivityIndicator, callback: callback, extendedUrl: extendedUrl)
        dataTask.resume()
    }
    
    class func expireTheSession(_ message: String?) {
//      Singleton.sharedInstance.logoutButtonAction()
    }
  
  // MARK: - Private Type Method
  private class func createRequestWith(method: HttpMethodType, timeout: Double, baseUrl: String, extendedUrl: String, contentType: String) -> URLRequest {
    let url = URL(string: baseUrl + extendedUrl)!
    var mutableRequest = URLRequest(url: url)
    mutableRequest.timeoutInterval = timeout
    mutableRequest.httpMethod = method.rawValue
    
    mutableRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
    if let token = getToken() {
      mutableRequest.setValue("Bearer \(token)", forHTTPHeaderField: "authorization")
    }
    return mutableRequest
  }
  
   private class func performDataTaskWith(request: URLRequest, showAlert: Bool, showAlertInDefaultCase: Bool, showActivityIndicator: Bool, callback: @escaping ServiceResponse, extendedUrl: String) -> URLSessionDataTask {
    let dataTask = URLSession.shared.dataTask(with: request) {
      (data, urlResponse, error) in
      DispatchQueue.main.async {
        
         if showActivityIndicator {
            HTTPClient.stopAnimatingActivityIndicator()
         }
        
        guard error == nil && data != nil else {
          // connection to server failed
          //TODO:  Do not show alert if dataTask was cancelled
          
          showAlert == true ? ErrorView.showWith(message: ERROR_MESSAGE.NO_INTERNET_CONNECTION, removed: nil): ()
         print(error!.localizedDescription)
          callback(nil, error, extendedUrl, nil)
          return
        }
        
        do {
          let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
          
//          print(json)
          
          var statusCode = 0
          
          let responseObject = json as? [String: Any]
          if let tempStatusCode = responseObject?["status"] as? NSNumber {
            //if statusCode is in response from server else getting it from httpResponse
            statusCode = tempStatusCode.intValue
          } else {
            guard let httpUrlResponce = urlResponse as? HTTPURLResponse else {
              callback(nil, APIErrors.statusCodeNotFound, extendedUrl, nil)
              print("Error -> Status code not found")
              return
            }
            statusCode = httpUrlResponce.statusCode
          }
          
          let message: String = responseObject?["message"] as? String ?? ERROR_MESSAGE.SERVER_NOT_RESPONDING
          switch statusCode {
          case STATUS_CODES.INVALID_ACCESS_TOKEN:
            showAlertInDefaultCase ? ErrorView.showWith(message: ERROR_MESSAGE.SERVER_NOT_RESPONDING, removed: nil) : ()
            expireTheSession(message)
          case STATUS_CODES.SHOW_DATA:
            callback(json, nil, extendedUrl, statusCode)
            return
          case STATUS_CODES.ERROR_IN_EXECUTION:
            showAlertInDefaultCase ? ErrorView.showWith(message: ERROR_MESSAGE.SERVER_NOT_RESPONDING, removed: nil) : ()
          default:
            
            showAlertInDefaultCase ? ErrorView.showWith(message: message, isErrorMessage: true, removed: nil) : ()
          }
          
          let error = APIErrors.serverThrewError(message: message)
          callback(json, error, extendedUrl, statusCode)
          
        } catch let jsonError {
          print("parsing error -> \(jsonError)")
          print("Wrong json -> " + (String(data: data!, encoding: .utf8) ?? ""))
          
          showAlert ? ErrorView.showWith(message: ERROR_MESSAGE.SERVER_NOT_RESPONDING, removed: nil) : ()
          
          callback(nil, jsonError, extendedUrl, nil)
        }
      }
    }
    
    return dataTask
  }
  
  private class func isConnectedToNetwork() -> Bool {
    return IJReachability.isConnectedToNetwork()
  }
  
  private class func startAnimatingActivityIndicator() {
//    ActivityIndicator.sharedInstance.showActivityIndicator()
    Merchant.shared?.startLoading()
  }
  
  private class func stopAnimatingActivityIndicator() {
//    ActivityIndicator.sharedInstance.hideActivityIndicator()
    Merchant.shared?.stopLoding()
  }
  
  private class func getToken() -> String? {
//    return Vendor.current?.accessToken
    return ""
    
  }
  
  
}

// MARK: - Data
private extension Data {
    mutating func append(boundary: String) {
        self.append("--\(boundary)\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
    }
    
    mutating func appendContentDepositionWith(key: String, fileName: String) {
        self.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(fileName).jpeg\"\r\n" .data(using: String.Encoding.utf8, allowLossyConversion: true)!)
    }
    
    mutating func appendContentTypeData(type: String) {
        self.append("Content-Type: image/\(type)\r\n\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
    }
    
    mutating func appendParameter(name: String) {
        self.append("Content-Disposition: form-data; name=\"\(name)\"\r\n\r\n" .data(using: String.Encoding.utf8, allowLossyConversion: true)!)
    }
    
    mutating func appendImageWith(key: String, path: String, boundary: String) {
        guard let imageData = try? Data(contentsOf: URL(fileURLWithPath: path)) else {
            print("ERROR -> Image file not found")
            return
        }
        self.append(boundary: boundary)
        self.appendContentDepositionWith(key: key, fileName: "Image" + key)
        self.appendContentTypeData(type: "jpg")
        self.append(imageData)
        self.append("\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
    }
}
