//
//  MenuViewController.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 15/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

protocol MenuViewControllerDelegate: class {
   func buttonPressedWith(actionType: ButtonPressedType, forElement element: LevelElement, atIndexInChildArray index: Int)
   func getQuantityOfElementWith(id: String) -> Int
}

class MenuViewController: UIViewController {
   
   // MARK: - Properties
   var level: [LevelElement]!
   fileprivate weak var delegate: NLevelFlowDelegate?
   fileprivate var currentPage = 0
   
   var heightOfCollectionViewBoundsBeforeFrameChange: CGFloat = 0
   let menuCollectionViewNib = UINib(nibName: NIB_NAME.menuTableCollectionViewCell, bundle: frameworkBundle)
   
   // MARK: - IBOutlets
   @IBOutlet weak var shadowView: UIView!
   @IBOutlet weak var productsCollectionView: UICollectionView!
   @IBOutlet weak var headerCollectionView: UICollectionView!
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
    productsCollectionView.bounces = false  
      setParentReloadClosure()
    headerCollectionView.setShadow()
      headerCollectionView.register(UINib(nibName: NIB_NAME.menuHeader, bundle: frameworkBundle), forCellWithReuseIdentifier: CELL_IDENTIFIER.menuHeader)
      productsCollectionView.register(menuCollectionViewNib, forCellWithReuseIdentifier: CELL_IDENTIFIER.menuTableCollectionViewCell)
      
      shadowView.setShadow()
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      heightOfCollectionViewBoundsBeforeFrameChange = productsCollectionView.bounds.height
      productsCollectionView.reloadData()
   }
   
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      
      if heightOfCollectionViewBoundsBeforeFrameChange != productsCollectionView.bounds.height {
         productsCollectionView.layoutIfNeeded()
         productsCollectionView.collectionViewLayout.invalidateLayout()
         heightOfCollectionViewBoundsBeforeFrameChange = productsCollectionView.bounds.height
      }
      
   }
   
   // MARK: - Navigation
   class func getWith(level: [LevelElement], delegate: NLevelFlowDelegate) -> MenuViewController {
      let menuVC = findIn(storyboard: .nLevel, withIdentifier: STORYBOARD_ID.menuView) as! MenuViewController
      menuVC.level = level
      menuVC.delegate = delegate
      return menuVC
   }
    
    override func didMove(toParentViewController parent: UIViewController?) {
        setParentReloadClosure()
    }
    
    func setParentReloadClosure(){
        print("in par")
        if self.parent is NLevelParentViewController{
            print("out par")
            (self.parent as! NLevelParentViewController).childReloadClosure = {[weak self](action) in
                self?.productsCollectionView.reloadData()
                
            }
        }
    }
    

}

// MARK: - Collection View Delegate
extension MenuViewController: UICollectionViewDataSource {
   
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return level.count
   }
   
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      switch collectionView {
      case headerCollectionView:
         return setUpHeaderCollectionViewCellIn(collectionView: collectionView, atIndexPath: indexPath)
      default:
         return setUpTableViewCollectionViewCellIn(collectionView: collectionView, atIndexPath: indexPath)
      }
   }
   
   private func setUpHeaderCollectionViewCellIn(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> MenuHeaderViewCell {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CELL_IDENTIFIER.menuHeader, for: indexPath) as! MenuHeaderViewCell
      
      let element = level[indexPath.item]
      cell.setCellWith(text: element.name, isSelected: currentPage == indexPath.item)
      
      return cell
   }
   
   private func setUpTableViewCollectionViewCellIn(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> MenuTableCollectionViewCell {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CELL_IDENTIFIER.menuTableCollectionViewCell, for: indexPath) as! MenuTableCollectionViewCell
      cell.tag = indexPath.generateTag()
      cell.delegate = self
      
      let element = level[indexPath.item]
      
      cell.startedFetchingData()
      delegate?.getChildrenOf(element: element, inCellWithTag: cell.tag) { (success, levelElements) in
         
         guard indexPath.generateTag() == cell.tag else {
            return
         }
         cell.levelElements = levelElements
         cell.endedFetchingData()
         cell.tableView.reloadData()
      }
      return cell
   }

}

// MARK: - Collection View Delegate
extension MenuViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      if collectionView == headerCollectionView {
         moveAllCollectionViewsTo(page: indexPath.item)
      }
   }
   
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      switch collectionView {
      case headerCollectionView:
            let tempLabel = UILabel()
            tempLabel.text = level?[indexPath.row].name ?? ""
            let font = UIFont.init(name: FONT.regular, size: 17)!
            let size: CGSize = tempLabel.text!.size(attributes: [NSFontAttributeName: font])
            let height: CGFloat = 51
            let width = size.width + 30
            return CGSize(width: width, height: height)
      default:
         return collectionView.frame.size
      }
   }
   
   private func calculateHeightOfCellWithTableView() -> CGFloat {
      let expectedOriginY = headerCollectionView.frame.maxY
      return SCREEN_SIZE.height - expectedOriginY
   }
}

// MARK: - ScrollViewDelegate
extension MenuViewController: UIScrollViewDelegate {
   func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
      if scrollView == productsCollectionView  {
         
         currentPage = caculateCurrentPageOf(scrollView: scrollView)
         moveAllCollectionViewsTo(page: currentPage)
      }
   }
   
   fileprivate func caculateCurrentPageOf(scrollView: UIScrollView) -> Int {
      let page = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
      
      guard isPageValid(page: page) else {
         return getValidPageFrom(page: page)
      }
      return page
   }
   
   fileprivate func isPageValid(page: Int) -> Bool {
      guard let levelCount = level?.count else {
         return false
      }
      return (page >= 0 && page < levelCount)
   }
   
   fileprivate func getValidPageFrom(page: Int) -> Int {
      guard let levelCount = level?.count, page < 0 else {
         return 0
      }

      if page >= levelCount {
         return levelCount - 1
      }
      
      return page
   }
   
   fileprivate func moveAllCollectionViewsTo(page: Int) {
      
      let indexPathOfPage = getIndexPathOf(page: page)
      
      currentPage = page
      headerCollectionView.scrollToItem(at: indexPathOfPage, at: .centeredHorizontally, animated: true)
      productsCollectionView.scrollToItem(at: indexPathOfPage, at: .centeredHorizontally, animated: true)
      
      self.headerCollectionView.reloadData()
   }
   
   fileprivate func getIndexPathOf(page: Int) -> IndexPath {
      return IndexPath(item: page, section: 0)
   }
}

// MARK: - ActionButtonDelegate
extension MenuViewController: MenuViewControllerDelegate {

   func buttonPressedWith(actionType: ButtonPressedType, forElement element: LevelElement, atIndexInChildArray index: Int) {
      switch actionType {
      case .Add:
         delegate?.addQuantityButtonPressed(forElement: element)
      case .Next:
         delegate?.nextButtonPressed(forElement: element, atIndexInChildArray: index)
      case .Subtract:
         delegate?.subtractQuantityButtonPressed(forElement: element)
      }
   }
   
   func getQuantityOfElementWith(id: String) -> Int {
      return delegate?.getQuantityOfProductWith(id: id) ?? 0
   }
}
