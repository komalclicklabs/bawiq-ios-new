//
//  MenuTableCollectionViewCell.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 15/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class MenuTableCollectionViewCell: UICollectionViewCell {
   
   // MARK: - Properties
   var levelElements: [LevelElement]?
   weak var delegate: MenuViewControllerDelegate?
   @IBOutlet weak var errorLabel: UILabel!
   
   // MARK: - IBOutlets
   @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
   
   // MARK: - View Life Cycle
   override func awakeFromNib() {
      super.awakeFromNib()
      tableView.bounces = false 
      tableView.delegate = self
      tableView.dataSource = self
      
      tableView.register(UINib(nibName: NIB_NAME.listCell, bundle: frameworkBundle), forCellReuseIdentifier: ListTableViewCell.identifier)
      
      tableView.tableFooterView = UIView()
      tableView.estimatedRowHeight = 110
      tableView.rowHeight = UITableViewAutomaticDimension
      activityIndicator.color = COLOR.THEME_FOREGROUND_COLOR
      errorLabel.configureDescriptionTypeLabelWith(text: "")
   }
   
   // MARK: - Methods
   fileprivate func reloadRowAt(index: Int) {
      let indexPath = IndexPath(row: index, section: 0)
      DispatchQueue.main.async { [weak self] in
         self?.tableView.reloadRows(at: [indexPath], with: .none)
      }
   }
   
   func startedFetchingData() {
      activityIndicator.startAnimating()
      self.bringSubview(toFront: activityIndicator)
      tableView.isHidden = true
      errorLabel.text = ""
      
      self.sendSubview(toBack: errorLabel)
   }
   
   func endedFetchingData() {
      activityIndicator.stopAnimating()
      self.sendSubview(toBack: activityIndicator)
      tableView.isHidden = false
      if levelElements?.first == nil {
         errorLabel.text = ERROR_MESSAGE.unableToFetchProducts
         self.bringSubview(toFront: errorLabel)
         tableView.isHidden = true
      } else {
         tableView.isHidden = false
      }
   }
}

// MARK: - Table View Delegate
extension MenuTableCollectionViewCell: UITableViewDelegate {
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      tableView.deselectRow(at: indexPath, animated: true)
      let levelElement = levelElements![indexPath.row]
      
      tableView.deselectRow(at: indexPath, animated: true)
      
      guard levelElement.actionType == .next || levelElement.actionType == .nextButButtonHidden else {
         return
      }
      
      delegate?.buttonPressedWith(actionType: .Next, forElement: levelElement, atIndexInChildArray: indexPath.row)
   }
}

// MARK: - Table View Datasource
extension MenuTableCollectionViewCell: UITableViewDataSource {
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return levelElements?.count ?? 0
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: ListTableViewCell.identifier, for: indexPath) as! ListTableViewCell
      
      cell.delegate = self
      cell.tag = indexPath.generateTag()

      guard let element = levelElements?[indexPath.row] else {
         return cell
      }
      
      var quantity = 0
      
      if let product = element as? Product {
         cell.selectionStyle = .none
         quantity = delegate?.getQuantityOfElementWith(id: product.id) ?? 0
         product.quantity = quantity
      }
      
    cell.setCellWith(imageUrl: element.imageUrl, description: element.lines.getAttributedStringDescription(), actionType: element.actionType, quantity: quantity,imageSize:element.imageSize == nil ? 1 :  element.imageSize!)      
      
      return cell
   }
}

// MARK: - ActionButtonDelegate
extension MenuTableCollectionViewCell: ActionButtonDelegate {
   func actionButtonPressed(type:ButtonPressedType, tag: Int) {
      
      let indexRow = tag.getRowAndSection().row
      let element = levelElements![indexRow]

      delegate?.buttonPressedWith(actionType: type, forElement: element, atIndexInChildArray: indexRow)
      reloadRowAt(index: indexRow)

   }
}
