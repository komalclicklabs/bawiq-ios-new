//
//  NavigationView.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 16/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
protocol NavigationDelegate: class {
   func backAction()
}

class NavigationView: UIView {
   
   // MARK: - Properties
   weak var delegate: NavigationDelegate!

   private var leftButtonAction: (() -> Void)?
   private var rightButtonAction: (() -> Void)?
   
   private var params = NavigationViewParams()
   var isShadowVisible: Bool = true {
      didSet {
         layer.shadowColor = isShadowVisible ? UIColor.black.withAlphaComponent(0.3).cgColor : UIColor.clear.cgColor
      }
   }
   var isBottomLineVisible: Bool = true {
      didSet {
         bottomLine.isHidden = !isBottomLineVisible
      }
   }
   
   // MARK: - IBOutlets
   @IBOutlet weak var backButton: UIButton!
   @IBOutlet weak var rightButton: UIButton!
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var bottomLine: UIView!
   @IBOutlet weak var titleImage: UIImageView!
   
   // MARK: - View Life Cycle
   override func awakeFromNib() {
      super.awakeFromNib()
      
   }
   
   // MARK: - IBAction
   @IBAction func backAction(_ sender: AnyObject) {
      leftButtonAction?()
   }
   
   @IBAction func rightButtonPressed() {
      rightButtonAction?()
   }
   
   // MARK: - View Configuration
   private func configureViews() {
      bottomLine.backgroundColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.06)
      self.setShadow()
      configurebackButton()
      configureRightButton()
      configureTitle()
      setBackgroundColor(color: COLOR.SPLASH_BACKGROUND_COLOR, andTintColor: COLOR.SPLASH_TEXT_COLOR)
      titleImage.image = params.titleImage
   }
   
   private func configureRightButton() {
      rightButton.setImage(params.rightButtonImage?.renderWithAlwaysTemplateMode(), for: .normal)
      rightButton.setTitle(params.rightButtonTitle, for: .normal)
      rightButton.titleLabel?.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
    rightButton.isHidden = params.rightButtonImage == nil && params.rightButtonTitle.isEmpty
   }
   
   func configurebackButton() {
      self.backButton.setImage(params.leftButtonImage?.renderWithAlwaysTemplateMode(), for: .normal)
      rightButton.titleLabel?.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
   }
   
   private func configureTitle() {
      titleLabel.configureHeadingTypeLabelWith(text: params.title)
      titleLabel.font = UIFont(name: FONT.light, size: 20)
      titleLabel.isHidden = params.title.isEmpty
   }
   
   func setBackgroundColor(color: UIColor, andTintColor tintColor: UIColor) {
      backgroundColor = color
      backButton.tintColor = tintColor
      titleLabel.tintColor = tintColor
      rightButton.tintColor = tintColor
   }
   
   // MARK: - Loading
    class func getNibFile(withHeight:CGFloat = HEIGHT.navigationHeight ,params: NavigationViewParams = NavigationViewParams(), leftButtonAction: (() -> Void)?, rightButtonAction: (() -> Void)?) -> NavigationView {
      
      let navBar = frameworkBundle!.loadNibNamed(NIB_NAME.navigation, owner: self, options: nil)?.first as! NavigationView
      
        navBar.setFrame(withHeight: withHeight)
      navBar.setButtonActions(leftButtonAction: leftButtonAction, rightButtonAction: rightButtonAction)
      navBar.params = params
      navBar.configureViews()
      
      return navBar
   }
   
    func setFrame(withHeight:CGFloat = HEIGHT.navigationHeight) {
      frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: withHeight)
   }
   
   private func setButtonActions(leftButtonAction: (() -> Void)?, rightButtonAction: (() -> Void)?) {
      self.leftButtonAction = leftButtonAction
      self.rightButtonAction = rightButtonAction
   }
}


extension NavigationView {
   struct NavigationViewParams {
      var leftButtonTitle = ""
      var rightButtonTitle = ""
      var title = ""
      var leftButtonImage: UIImage? = UIImage(named: "back", in: frameworkBundle, compatibleWith: nil)
      var rightButtonImage: UIImage?
      var titleImage: UIImage?
    
      init() {}
      
      init(leftButtonTitle: String, rightButtonTitle: String, title: String, leftButtonImage: UIImage? = UIImage(named: "back", in: frameworkBundle, compatibleWith: nil), rightButtonImage: UIImage? = nil, titleImage: UIImage? = nil) {
         self.leftButtonTitle = leftButtonTitle
         self.rightButtonTitle = rightButtonTitle
         self.title = title
         self.leftButtonImage = leftButtonImage
         self.rightButtonImage = rightButtonImage
         self.titleImage = titleImage
      }
   }
}

extension UIView {
    func roundedEdges(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
