//
//  ImageCustomCell.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 16/12/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

protocol ImageCustomDelegate: class {
    func showImagesPreview(_ indexItem:Int, imagesArray:[Any], customFieldRow:Int,headerTypeOfCell:headerType)
}

class ImageCustomCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    @IBOutlet var headerLabel: UILabel!
    @IBOutlet var addImageButton: UIButton!
    @IBOutlet var icon: UIImageView!
    @IBOutlet var imageCollectionView: UICollectionView!
    @IBOutlet var bottomLine: UIView!
    @IBOutlet var imageCollectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonAndCollectionViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var labelAndCollectionViewConstraint: NSLayoutConstraint!
    
    var isForEditing:Bool{
        set{
            switch newValue {
            case true:
                if labelAndCollectionViewConstraint != nil{
                labelAndCollectionViewConstraint.isActive = false
                }
                break
            default:
                if buttonAndCollectionViewConstraint != nil{
                buttonAndCollectionViewConstraint.isActive = false
                }
                labelAndCollectionViewConstraint.constant = 25
                break
            }
        }
        get{
            if labelAndCollectionViewConstraint.constant == 25{
                return false
            }else{
            return false
            }
        }
    }
    
    
    
    
    let imageCollecitonHeight:CGFloat = 76.0
    var imageCollectionArray = [Any]()
    var delegate:ImageCustomDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        /*--------- Header Label -------------*/
        headerLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        headerLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        
        /*--------- Add Image Button ---------------*/
        addImageButton.setTitle(TEXT.ADD_IMAGE, for: UIControlState.normal)
        addImageButton.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: UIControlState.normal)
        addImageButton.titleLabel?.font = UIFont(name: FONT.light, size: FONT_SIZE.large)

        /*--------- UnderLine ------------*/
        self.bottomLine.backgroundColor = COLOR.SPLASH_LINE_COLOR
        
        /*------------- ImageCollectionView ------------*/
       // self.imageCollectionView.backgroundColor = COLOR.SPLASH_LINE_COLOR
        self.imageCollectionView.delegate = self
        self.imageCollectionView.dataSource = self
        self.imageCollectionView.register(UINib(nibName: NIB_NAME.imageCollectionCell, bundle: frameworkBundle), forCellWithReuseIdentifier: NIB_NAME.imageCollectionCell)
        /*-------------- icon -------------*/
        self.icon.image = #imageLiteral(resourceName: "iconImageInactive").withRenderingMode(.alwaysTemplate)
    }
    
    func setImageCollectionView(label:String, imageArray:[Any]) {
        self.imageCollectionArray = imageArray
        self.headerLabel.text = label
        if imageArray.count == 0 {
            self.imageCollectionHeightConstraint.constant = 0
            self.setActiveInActiveStatus(yourState: ACTIVE_INACTIVE_STATES.inActive)
            self.bottomLine.isHidden = false
        } else {
            self.imageCollectionHeightConstraint.constant = self.imageCollecitonHeight
            self.setActiveInActiveStatus(yourState: ACTIVE_INACTIVE_STATES.filled)
            self.bottomLine.isHidden = true
        }
        self.imageCollectionView.reloadData()
    }

    func setActiveInActiveStatus(yourState:String) {
        switch yourState {
        case ACTIVE_INACTIVE_STATES.active:
            self.icon.tintColor = COLOR.ACTIVE_IMAGE_COLOR
        case ACTIVE_INACTIVE_STATES.inActive:
            self.icon.tintColor = COLOR.IN_ACTIVE_IMAGE_COLOR
        case ACTIVE_INACTIVE_STATES.filled:
            self.icon.tintColor = COLOR.FILLED_IMAGE_COLOR
        default :
            self.icon.tintColor = COLOR.IN_ACTIVE_IMAGE_COLOR
        }
    }
    
    //MARK: UICOLLECTIONVIEW DELEGATE
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NIB_NAME.imageCollectionCell, for: indexPath) as! ImageCollectionCell
        
        if Singleton.sharedInstance.isFileExistAtPath(self.imageCollectionArray[indexPath.item] as! String) == true {
            getImageFromDocumentDirectory(self.imageCollectionArray[indexPath.item] as! String, imageView: cell.imageView, placeHolderImage: #imageLiteral(resourceName: "iconImageInactive"), contentMode: UIViewContentMode.scaleAspectFill)
            cell.transLayer.isHidden = false
            cell.activityIndicator.startAnimating()
        } else {
            cell.transLayer.isHidden = true
            cell.activityIndicator.stopAnimating()
            URL(string: self.imageCollectionArray[indexPath.item] as! String)
            cell.imageView.kf.setImage(with: URL(string: self.imageCollectionArray[indexPath.item] as! String), placeholder: placeholdeImage)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageCollectionArray.count
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top:0, left:0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let width = (SCREEN_SIZE.width - 30) / 4
        if width < imageCollecitonHeight {
            return CGSize(width: width, height: width)
        }
        return CGSize(width: imageCollecitonHeight, height: imageCollecitonHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate.showImagesPreview(indexPath.item, imagesArray: self.imageCollectionArray,customFieldRow: collectionView.tag, headerTypeOfCell: .AppointmentDetails)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    
}
