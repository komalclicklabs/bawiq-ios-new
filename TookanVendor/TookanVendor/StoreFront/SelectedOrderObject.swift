//
//  SelectedOrderObject.swift
//  Jugnoo Autos
//
//  Created by Sourabh on 28/12/16.
//  Copyright © 2016 Socomo Technologies. All rights reserved.
//

import Foundation

class SelectedOrderObject: NSObject, NSCoding {
    
    var productVarietyID = ""//String() // Eg.. Sub Category of Pizza (Classic--Margherita) ID\
    var productVarietyName = String()
    var productVarietyPrice = Double()
    
    var isProductVeg = Bool()
    
    var quantity = Int()
    
    var productVarietyTaxes = [AnyObject]()
    
    var customizeIDArray = [String]()
    
    var customizeOptionIDArray = [String]()
    var customizeNameArray = [String]()
    var customizePriceArray = [Double]()
    var additionalCostArray = [Double]()
    
    init(productVarietyID: String, productVarietyName:String, productVarietyPrice:Double, quantity: Int, customizeIDArray: [String], customizeOptionIDArray: [String], customizeNameArray:[String], customizePriceArray: [Double], additionalCostArray: [Double], productVarietyTaxes:[AnyObject], isProductVeg:Bool) {
        
        self.productVarietyID = productVarietyID
        self.productVarietyName = productVarietyName
        self.productVarietyPrice = productVarietyPrice
        self.quantity = quantity
        self.customizeIDArray = customizeIDArray
        self.customizeOptionIDArray = customizeOptionIDArray
        self.customizeNameArray = customizeNameArray
        self.customizePriceArray = customizePriceArray
        self.additionalCostArray = additionalCostArray
        self.productVarietyTaxes = productVarietyTaxes
        self.isProductVeg = isProductVeg
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(productVarietyID, forKey: "productVarietyID")
        aCoder.encode(productVarietyName, forKey: "productVarietyName")
        aCoder.encode(productVarietyPrice, forKey: "productVarietyPrice")
        
        aCoder.encode(quantity,    forKey: "quantity")
        aCoder.encode(customizeOptionIDArray, forKey: "customizeOptionIDArray")
        
        aCoder.encode(customizeIDArray, forKey: "customizeIDArray")
        aCoder.encode(customizeNameArray, forKey: "customizeNameArray")
        
        aCoder.encode(customizePriceArray,    forKey: "customizePriceArray")
        
        aCoder.encode(additionalCostArray, forKey: "additionalCostArray")
        aCoder.encode(productVarietyTaxes, forKey: "productVarietyTaxes")
        
        aCoder.encode(isProductVeg, forKey: "isProductVeg")
    
        
    }
    
    
    convenience required init?(coder aDecoder: NSCoder) {
        
         /*
        let productVarietyID = aDecoder.decodeObject(forKey: "productVarietyID") as! String
        let productVarietyName = aDecoder.decodeObject(forKey: "productVarietyName") as! String
        let productVarietyPrice = aDecoder.decodeDouble(forKey: "productVarietyPrice")
        let quantity    = aDecoder.decodeInteger(forKey: "quantity")
        let customizeIDArray = aDecoder.decodeObject(forKey: "customizeIDArray") as! [String]
        let customizePriceArray    = aDecoder.decodeObject(forKey: "customizePriceArray")    as! [Double]
        let additionalCostArray = aDecoder.decodeObject(forKey: "additionalCostArray") as! [Double]
        let productVarietyTaxes = aDecoder.decodeObject(forKey: "productVarietyTaxes") as! [AnyObject]
        let isProductVeg = aDecoder.decodeBool(forKey: "isProductVeg")
        
        */
        guard
            let productVarietyID = aDecoder.decodeObject(forKey: "productVarietyID") as? String,
            let productVarietyName = aDecoder.decodeObject(forKey: "productVarietyName") as? String,
            let productVarietyPrice = aDecoder.decodeDouble(forKey: "productVarietyPrice") as? Double,
            let quantity    = aDecoder.decodeInteger(forKey: "quantity")   as? Int,
            let customizeIDArray = aDecoder.decodeObject(forKey: "customizeIDArray") as? [String],
            let customizeOptionIDArray = aDecoder.decodeObject(forKey: "customizeOptionIDArray") as? [String],
            let customizeNameArray = aDecoder.decodeObject(forKey: "customizeNameArray") as? [String],
            let customizePriceArray    = aDecoder.decodeObject(forKey: "customizePriceArray")    as? [Double],
            let additionalCostArray = aDecoder.decodeObject(forKey: "additionalCostArray") as? [Double],
            let productVarietyTaxes = aDecoder.decodeObject(forKey: "productVarietyTaxes") as? [AnyObject],
            let isProductVeg = aDecoder.decodeBool(forKey: "isProductVeg") as? Bool
        
            else {
                return nil
        }
        
        self.init(productVarietyID: productVarietyID,productVarietyName: productVarietyName, productVarietyPrice:productVarietyPrice, quantity: quantity, customizeIDArray:customizeIDArray, customizeOptionIDArray: customizeOptionIDArray,customizeNameArray:customizeNameArray, customizePriceArray: customizePriceArray, additionalCostArray: additionalCostArray, productVarietyTaxes:productVarietyTaxes,isProductVeg:isProductVeg)
    }
    
    
    
    
//    var customizeSubOptionArray = [(id:[String], customizeOptionName:[String] ,isDefault:[Bool],customizePrice:[Double], additionalCost:[Double])]()
}
