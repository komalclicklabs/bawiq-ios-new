//
//  SelectedCustomProductData.swift
//  StoreFront
//
//  Created by Samneet Kharbanda on 03/10/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

class SelectedCustomProductData{
    
    var quantity = 0
    var selectedCustId = [(id:String, customizeOptionName:String ,isDefault:Bool,customizePrice:Double, additionalCost:Double)]()
    var extraPrice = Double()
    
    
    init(quantity:Int,arrayOfId:[(id:String, customizeOptionName:String ,isDefault:Bool,customizePrice:Double, additionalCost:Double)],additionalCost:Double) {
        self.quantity = quantity
        self.selectedCustId = arrayOfId
        self.extraPrice = additionalCost
    }
    
    
    
    
}
