//
//  PromoTableViewCell.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 15/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class PromoTableViewCell: UITableViewCell {

    @IBOutlet weak var promoText: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        // Initialization code
    }

   func setIconToGreenTick(shouldSet: Bool) {
      iconImage.image = shouldSet ? #imageLiteral(resourceName: "greenTick") : #imageLiteral(resourceName: "info")
   }

}
