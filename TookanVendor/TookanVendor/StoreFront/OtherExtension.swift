//
//  OtherExtension.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 19/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit


extension IndexPath {
   func generateTag() -> Int {
      let tag = (1000 * (self.section + 1)) + (self.row + 1)
      return tag
   }
   
}

extension Int {
   func getRowAndSection() -> (row: Int, section: Int) {
      return ((self % 1000)-1, (self / 1000) - 1)
   }
}

extension UITableView {
   func registerCellWith(nibName: String, reuseIdentifier: String) {
      let cellNib = UINib(nibName: nibName, bundle: frameworkBundle)
      self.register(cellNib, forCellReuseIdentifier: reuseIdentifier)
   }
}


