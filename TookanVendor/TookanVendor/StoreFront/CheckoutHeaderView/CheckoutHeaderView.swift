//
//  CheckoutHeaderView.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 06/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class CheckoutHeaderView: UIView {

   //MARK: - IBOutLets
    
    @IBOutlet weak var downArrow: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var HeaderLabel: UILabel!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var clearButton: UIButton!
   @IBOutlet weak var cartCountLabel: UILabel!
    

    var rightButtonAction : (()->Void)?
    var leftButtonAction  : (()->Void)?
    var addAction : (()->Void)?
    
   override func awakeFromNib() {
      setUiAttributes()
      cartCountLabel.isHidden = true
      cartCountLabel.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
      headerImage.tintColor = COLOR.SPLASH_TEXT_COLOR
   }
   
    
    static func getHeaderViewWith(headerType:headerType,headerImage:UIImage,HeaderLabelString:String,showLoader:Bool,isAdded:Bool,leftButtonAction:(()->Void)?,rightButtonAction:(() -> Void)?,AddAction:(() -> Void)?) ->CheckoutHeaderView{
        

        let actionView = UINib(nibName: NIB_NAME.CheckoutHeaderViews, bundle: frameworkBundle).instantiate(withOwner: self, options: nil)[0] as? CheckoutHeaderView
        actionView?.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 50)
        actionView?.rightButtonAction = rightButtonAction
        actionView?.leftButtonAction = leftButtonAction
        actionView?.headerImage.image = headerImage.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        actionView?.HeaderLabel.text = HeaderLabelString
        actionView?.HeaderLabel.letterSpacing = 0.4
        actionView?.rightButton.isHidden = showLoader
        actionView?.addAction = AddAction
        actionView?.activityIndicator.isHidden = !showLoader
        isAdded == true ? actionView?.rightButton.setTitle("Remove", for: UIControlState.normal) :  actionView?.rightButton.setTitle("Add", for: UIControlState.normal)
        showLoader == true ? actionView?.activityIndicator.startAnimating(): actionView?.activityIndicator.stopAnimating()
        actionView?.downArrow.isHidden = true
        switch headerType {
        case .Cart:
            actionView?.downArrow.isHidden = false
            actionView?.cartCountLabel.isHidden = false
            actionView?.rightButton.setTitle("Clear", for: UIControlState.normal)
        case .Description:
             actionView?.rightButton.isHidden = true
        default:
            actionView?.rightButton.isHidden = false
        }
        
        
        
     return actionView!
        
    }
    
    
    func setCount(count:Int){
        self.cartCountLabel.text = "\(count)"
    }
    
    
    
    
    
    func setUiAttributes(){
        self.HeaderLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)
        self.HeaderLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        rightButton.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: UIControlState.normal)
        rightButton.titleLabel?.font = UIFont(name: FONT.semiBold, size: FONT_SIZE.buttonTitle)
        self.backgroundView.layer.shadowColor = UIColor.black.cgColor
        self.backgroundView.layer.shadowOpacity = 0.2
        self.backgroundView.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.backgroundView.layer.shadowRadius = 1
    }
    
    
    
    
    @IBAction func leftButtonAction(_ sender: UIButton) {
        leftButtonAction != nil ? leftButtonAction!() : print("")
    }
    
    
    @IBAction func rightButtonAction(_ sender: UIButton) {
        switch sender.titleLabel!.text! {
        case "Add":
            addAction != nil ? addAction!() : print("")
        default:
            rightButtonAction != nil ? rightButtonAction!() : print("")
        }
        
    }
    
    
    
    
    
    

}
