//
//  RestaurantMenuDetailObjects.swift
//  Jugnoo Autos
//
//  Created by Sourabh on 28/12/16.
//  Copyright © 2016 Socomo Technologies. All rights reserved.
//

import Foundation

typealias ProductSubItemsType = (isItemActive: Bool, itemDisplayPrice:String, itemPrice:Double,itemTaxes:[AnyObject], itemDescription:String, isItemVeg:Bool, itemName:String,itemID:String,itemCustomizeObjectArray:[MenuCustomizableObjects])

class RestaurantMenuDetailObjects {
    func copy() -> RestaurantMenuDetailObjects {
        return RestaurantMenuDetailObjects(self)
    }
    init(_ otherObject:RestaurantMenuDetailObjects) {
        categoryID = otherObject.categoryID
        categoryName = otherObject.categoryName
        categoryImage = otherObject.categoryImage
        subCategoryIdArray = otherObject.subCategoryIdArray
        subCategoryNameArray = otherObject.subCategoryNameArray
        vegProductSubItemsArray = otherObject.vegProductSubItemsArray
        productSubItemsArray = otherObject.productSubItemsArray
        categoryID = otherObject.categoryID
        
        
    }
    
    init() {
        
    }
    var categoryID = Int()
    var categoryName = String()
    var categoryImage = String()
    
    var subCategoryIdArray = [String]()
    var subCategoryNameArray = [String]()
    var vegProductSubItemsArray = [[ProductSubItemsType]]()
    var productSubItemsArray = [[ProductSubItemsType]]()
    
//    var productSubItemsArray = [(isItemActive:[Bool],itemDisplayPrice:[String], itemPrice:[Double],itemTaxes:[AnyObject], itemDescription:[String], isItemVeg:[Bool], itemName:[String],itemID:[String],itemCustomizeObjectArray:[[MenuCustomizableObjects]])]()
    
    // MARK: - Restaurant Menu Json
    
    func parseRestaurantMenuDetailJson(jsonArray:[Any]) -> Bool {
        
        RestaurantMenuObject.sharedInstance.restaurantMenuDetailFilteredObjectArray.removeAll()
        RestaurantMenuObject.sharedInstance.restaurantMenuDetailObjectArray.removeAll()
        for jsonObject in jsonArray {
            
            if let json = jsonObject as? [String:Any] {
                
                let restaurantMenuObject = RestaurantMenuDetailObjects()
                
                if let productID = json["category_id"] as? Int {
                    
                    restaurantMenuObject.categoryID = productID
                }
                
                if let productName = json["category_name"] as? String {
                    
                    restaurantMenuObject.categoryName = productName
                }
                
                if let productImage = json["category_image"] as? String {
                    
                    restaurantMenuObject.categoryImage = productImage
                }
                
                if let productSubItemsArray = json["subcategories"] as? [AnyObject] {
                    
                    for subCategory in productSubItemsArray {
                        
                        if let _ = subCategory as? [String:AnyObject] {
                            
                            var subItemId = "", subItemName = ""
                            
                            var tempProductSubItemsArray = [ProductSubItemsType]()
                            var tempVegProductSubItemsArray = [ProductSubItemsType]()
                            if let id = subCategory["subcategory_id"] as? String {
                                
                                subItemId = id
                            }
                            else {
                                subItemId = "0"
                            }
                            
                            if let name = subCategory["subcategory_name"] as? String {
                                
                                subItemName = name
                            }
                            
                            /*
                            var subItemId = "", subItemName = "", subItemDisplayPrice = [String](), subItemPrice = [Double](), subItemIsActive = [Bool](), subItemDescription = [String](), subItemIsVeg = [Bool](), subProductVarietyName = [String](), subProductVarietyID = [String](), subItemTaxes = [AnyObject]()
                            
                            */
                            if let itemsArray = subCategory["items"] as? [AnyObject] {
                                
                                for items in itemsArray {
                                    
                                    if let _ = items as? [String:AnyObject] {
                                        
                                        var tempProductSubItemsObject: ProductSubItemsType = (isItemActive: false, itemDisplayPrice:"", itemPrice:0.0,itemTaxes:[AnyObject](), itemDescription:"", isItemVeg:false, itemName:"",itemID:"",itemCustomizeObjectArray:[MenuCustomizableObjects]())
                                        
                                        tempProductSubItemsObject.itemCustomizeObjectArray = (self.parseCustomisation(items: items as! [String : AnyObject]))
                                        
                                        if let is_active = items["is_active"] as? Bool {
                                            tempProductSubItemsObject.isItemActive = is_active
                                        } else {
                                            tempProductSubItemsObject.isItemActive = true
                                        }
                                        
                                        if let isVeg = items["is_veg"] as? Bool {
                                            
                                            if isVeg {
                                                tempProductSubItemsObject.isItemVeg = true
                                            }
                                            else {
                                                tempProductSubItemsObject.isItemVeg = false
                                            }
                                        } else {
                                            tempProductSubItemsObject.isItemVeg = true
                                        }
                                        
                                        if let subItemTaxArray = items["taxes"] as? [AnyObject] {
                                            
                                            tempProductSubItemsObject.itemTaxes = subItemTaxArray
                                        }
                                        
                                        
                                        if let itemDescription = items["item_details"] as? String {
                                            
                                            tempProductSubItemsObject.itemDescription = itemDescription
                                        }
                                        else {
                                            tempProductSubItemsObject.itemDescription = "No Description."
                                        }
                                        
                                        if let itemPrice = items["price"] as? Double {
                                            
                                            tempProductSubItemsObject.itemPrice = itemPrice
                                        }
                                        else {
                                            tempProductSubItemsObject.itemPrice = 0.0
                                        }
                                        
                                        if let itemDisplayPrice = items["display_price"] as? String {
                                            
                                            tempProductSubItemsObject.itemDisplayPrice = itemDisplayPrice
                                        }
                                        else {
                                            tempProductSubItemsObject.itemDisplayPrice = "\(tempProductSubItemsObject.itemPrice)"
                                        }
                                        
                                        if let subCustomizationID = items["restaurant_item_id"] as? Int {
                                            
                                            tempProductSubItemsObject.itemID = "\(subCustomizationID)"
                                        }
                                        else {
                                            tempProductSubItemsObject.itemID = ""
                                        }
                                        
                                        if let productVariety = items["item_name"] as? String {
                                            
                                            tempProductSubItemsObject.itemName = productVariety
                                        }
                                        else {
                                            tempProductSubItemsObject.itemName = ""
                                        }
                                        
                                        if tempProductSubItemsObject.isItemVeg {
                                            tempVegProductSubItemsArray.append(tempProductSubItemsObject)
                                        }
                                        tempProductSubItemsArray.append(tempProductSubItemsObject)
                                    }
                                }
                            }
                            
                            restaurantMenuObject.subCategoryIdArray.append(subItemId)
                            restaurantMenuObject.subCategoryNameArray.append(subItemName)
                            restaurantMenuObject.productSubItemsArray.append(tempProductSubItemsArray)
                            
                            
                        }
                    }
                }
                else {
                    
                    if let itemsArray = json["items"] as? [AnyObject] {
                        
                        var subItemId = "", subItemName = ""
                        
                        var tempProductSubItemsArray = [ProductSubItemsType]()
                        var tempVegProductSubItemsArray = [ProductSubItemsType]()
                        for items in itemsArray {
                            
                            if let _ = items as? [String:AnyObject] {
                                
                                
                                var tempProductSubItemsObject: ProductSubItemsType = (isItemActive: false, itemDisplayPrice:"", itemPrice:0.0,itemTaxes:[AnyObject](), itemDescription:"", isItemVeg:false, itemName:"",itemID:"",itemCustomizeObjectArray:[MenuCustomizableObjects]())
                                
                                tempProductSubItemsObject.itemCustomizeObjectArray = (self.parseCustomisation(items: items as! [String : AnyObject]))
                                
                                if let is_active = items["is_active"] as? Bool {
                                    tempProductSubItemsObject.isItemActive = is_active
                                } else {
                                    tempProductSubItemsObject.isItemActive = true
                                }
                                
                                if let isVeg = items["is_veg"] as? Bool {
                                    
                                    if isVeg {
                                        tempProductSubItemsObject.isItemVeg = true
                                    }
                                    else {
                                        tempProductSubItemsObject.isItemVeg = false
                                    }
                                }
                                else {
                                    tempProductSubItemsObject.isItemVeg = true
                                }
                                
                                if let subItemTaxArray = items["taxes"] as? [AnyObject] {
                                    
                                    tempProductSubItemsObject.itemTaxes = subItemTaxArray
                                }
                                
                                if let itemDescription = items["item_details"] as? String {
                                    
                                    tempProductSubItemsObject.itemDescription = itemDescription
                                }
                                else {
                                    tempProductSubItemsObject.itemDescription = "No Description."
                                }
                                
                                if let itemPrice = items["price"] as? Double {
                                    
                                    tempProductSubItemsObject.itemPrice = itemPrice
                                }
                                else {
                                    tempProductSubItemsObject.itemPrice = 0.0
                                }
                                
                                if let itemDisplayPrice = items["display_price"] as? String {
                                    
                                    tempProductSubItemsObject.itemDisplayPrice = itemDisplayPrice
                                }
                                else {
                                    tempProductSubItemsObject.itemDisplayPrice = "\(tempProductSubItemsObject.itemPrice)"
                                }
                                
                                if let subCustomizationID = items["restaurant_item_id"] as? Int {
                                    
                                    tempProductSubItemsObject.itemID = "\(subCustomizationID)"
                                }
                                else {
                                    tempProductSubItemsObject.itemID = ""
                                }
                                
                                if let productVariety = items["item_name"] as? String {
                                    
                                    tempProductSubItemsObject.itemName = productVariety
                                }
                                else {
                                    tempProductSubItemsObject.itemName = ""
                                }
                                
                                if tempProductSubItemsObject.isItemVeg {
                                    tempVegProductSubItemsArray.append(tempProductSubItemsObject)
                                }
                                
                                tempProductSubItemsArray.append(tempProductSubItemsObject)
                            }
                        }
                        
                        restaurantMenuObject.subCategoryIdArray.append(subItemId)
                        restaurantMenuObject.subCategoryNameArray.append(subItemName)
                        restaurantMenuObject.productSubItemsArray.append(tempProductSubItemsArray)
                        restaurantMenuObject.vegProductSubItemsArray.append(tempVegProductSubItemsArray)
                    }
                    
                }
                
                RestaurantMenuObject.sharedInstance.restaurantMenuDetailFilteredObjectArray.append(restaurantMenuObject)
                
                let restaurantMenuFilteredObject = restaurantMenuObject.copy()
                 RestaurantMenuObject.sharedInstance.restaurantMenuDetailObjectArray.append(restaurantMenuFilteredObject)
            }
        }
//        let detailsArray = RestaurantMenuObject.sharedInstance.restaurantMenuDetailFilteredObjectArray
//
//        RestaurantMenuObject.sharedInstance.restaurantMenuDetailObjectArray = detailsArray        
//        print(RestaurantMenuObject.sharedInstance.restaurantMenuDetailFilteredObjectArray)
        
        return true
    }
    
    func parseCustomisation(items:[String:AnyObject]) -> [MenuCustomizableObjects]   {
        
        var menuCustomizeObjectsForItem = [MenuCustomizableObjects]()
        
//        return menuCustomizeObjectsForItem
        
        if let customizeItemArray = items["customization"] as? [AnyObject] {
            
            
            for customizeItem in customizeItemArray {
                
                let menuCustomizeObject = MenuCustomizableObjects()
                
                if let customizeId = customizeItem["customize_id"] as? Int {
                    
                    menuCustomizeObject.customizeID = customizeId
                }
                if let customizeItemName = customizeItem["customize_item_name"] as? String {
                    
                    menuCustomizeObject.customizeName  = customizeItemName
                }
                if let isCustomizationCheckBox = customizeItem["is_check_box"] as? Bool {
                    
                    menuCustomizeObject.isCheckBox  = isCustomizationCheckBox
                }
                
                var customizeTuppleArray = [(id:String, customizeOptionName:String ,isDefault:Bool,customizePrice:Double, additionalCost:Double)]()
                
                if let customizeOptionArray = customizeItem["customize_options"] as? [AnyObject] {
                    for customizeOption in customizeOptionArray {
                        
                        var customizeOptionID = String(), customizeOptionName = String(), additionalCost = Double(), customizePrice = Double(), isDefault = Bool()
                        
                        if let additional_cost = customizeOption["additional_cost"] as? Double {
                            
                            additionalCost = additional_cost
                        }
                        
                        if let customize_option_id = customizeOption["customize_option_id"] as? Int {
                            
                            customizeOptionID = "\(customize_option_id)"
                        }
                        
                        if let customize_option_name = customizeOption["customize_option_name"] as? String {
                            // customizeOptionIDArray
                            customizeOptionName = customize_option_name
                        }
                        
                        if let customize_price = customizeOption["customize_price"] as? Double {
                            
                            customizePrice = customize_price
                        }
                        if let is_default = customizeOption["is_default"] as? Bool {
                            
                            isDefault = is_default
                        }
                        
                        customizeTuppleArray.append((id:customizeOptionID, customizeOptionName:customizeOptionName ,isDefault:isDefault,customizePrice:customizePrice, additionalCost:additionalCost))
                        
                    }
                }
                
                menuCustomizeObject.customizeSubOptionArray = customizeTuppleArray
                
                
                menuCustomizeObjectsForItem.append(menuCustomizeObject)
            }
            
        }
        
        //        print(menuCustomizeObjectsForItem)
        
        return menuCustomizeObjectsForItem
    }

}
