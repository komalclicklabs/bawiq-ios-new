import UIKit

protocol CountryPhoneCodePickerDelegate: class {
    func countryPhoneCodePicker(picker: CountryPicker, didSelectCountryCountryWithName name: String, countryCode: String, phoneCode: String)
}

struct Country {
    var code: String?
    var name: String?
    var phoneCode: String?
    
    init(code: String?, name: String?, phoneCode: String?) {
        self.code = code
        self.name = name
        self.phoneCode = phoneCode
    }
}

class CountryPicker: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var countries: [Country]!
    weak var countryPhoneCodeDelegate: CountryPhoneCodePickerDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        super.dataSource = self
        super.delegate = self
        
        countries = countryNamesByCode()
      selectLocalCountryCode()
    }
    
    // MARK: - Country Methods
    
    func setCountry(code: String) {
        var row = 0
        for index in 0..<countries.count {
            if countries[index].code == code {
                row = index
                break
            }
        }
        let country = countries[row]
        countryPhoneCodeDelegate?.countryPhoneCodePicker(picker: self, didSelectCountryCountryWithName: country.name!, countryCode: country.code!, phoneCode: country.phoneCode!)
        self.selectRow(row, inComponent: 0, animated: true)
    }
    func selectLocalCountryCode() {
        let locale = NSLocale.current as NSLocale
        if let code = locale.object(forKey: .countryCode) as? String {
            self.setCountry(code: code)
        }
    }
    
    func countryNamesByCode() -> [Country] {
        var countries = [Country]()
        
        for code in NSLocale.isoCountryCodes {
         let locale = Locale(identifier: Locale.current.identifier)
         
            let countryName = locale.localizedString(forRegionCode: code)
            
            let phoneCode: String = "+" + "\(dialingCode[code] ?? "")"
            
            if phoneCode != "+0" {
                let country = Country(code: code, name: countryName, phoneCode: phoneCode)
                countries.append(country)
            }
        }
//        $0.name! < $1.name!
        return countries.sorted {$0.name! < $1.name!}
        
    }
    
    // MARK: - Picker Methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var resultView: CountryView?
        
        if view == nil {
            resultView = (frameworkBundle!.loadNibNamed("CountryView", owner: self, options: nil)?[0] as? CountryView)
        } else {
            resultView = view as? CountryView
        }
        
        resultView?.setup(country: countries[row])
        
        if let tempResultView = resultView {
            return tempResultView
        } else {
            fatalError("CountryView not found")
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let country = countries[row]
        if let countryPhoneCodeDelegate = countryPhoneCodeDelegate {
            countryPhoneCodeDelegate.countryPhoneCodePicker(picker: self, didSelectCountryCountryWithName: country.name!, countryCode: country.code!, phoneCode: country.phoneCode!)
        }
    }
}
