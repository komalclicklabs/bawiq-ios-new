//
//  CartSegmentViewController.swift
//  TookanVendor
//
//  Created by Vishal on 15/02/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

enum CartController: Int {
    case cart = 1
    case delivery = 2
    case payment = 3
    case confirm = 4
}

class CartSegmentViewController: UIViewController {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var deliveryView: UIImageView!
    @IBOutlet weak var deliveyLabel: UILabel!
    @IBOutlet weak var deliveryImage: UIImageView!
    
    @IBOutlet weak var paymentView: UIImageView!
    @IBOutlet weak var paymentLAbel: UILabel!
    @IBOutlet weak var paymentImage: UIImageView!
    
    @IBOutlet weak var confirmView: UIImageView!
    @IBOutlet weak var confirmLabel: UILabel!
    @IBOutlet weak var confirmImage: UIImageView!
    
    private lazy var confirmViewController: BQConfirmBookingVC = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Cart", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "BQConfirmBookingVC") as! BQConfirmBookingVC
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var updateAddressVC: BQUpdateAddressVC = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Cart", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "BQUpdateAddressVC") as! BQUpdateAddressVC
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var cartViewController: CartViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Cart", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var deliveryViewController: DeliveryViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Cart", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "DeliveryViewController") as! DeliveryViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    private lazy var paymentViewController: PaymentViewController = {
        // Load Storyboard
        let storyboard = UIStoryboard(name: "Cart", bundle: Bundle.main)
        
        // Instantiate View Controller
        var viewController = storyboard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        
        // Add View Controller as Child View Controller
        self.add(asChildViewController: viewController)
        
        return viewController
    }()
    
    
//    private lazy var confirmViewController: ConfirmViewController = {
//        // Load Storyboard
//        let storyboard = UIStoryboard(name: "Cart", bundle: Bundle.main)
//
//        // Instantiate View Controller
//        var viewController = storyboard.instantiateViewController(withIdentifier: "ConfirmViewController") as! ConfirmViewController
//
//        // Add View Controller as Child View Controller
//        self.add(asChildViewController: viewController)
//
//        return viewController
//    }()
    
    var navigationBar:NavigationView!

    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar()

        showCartView()
    }
    
    fileprivate func switchView(tag: Int) {
        switch tag {
        case 1:
            showCartView()
            break
        case 2:
            //if BQBookingModel.shared.isScheduled == 0 || BQBookingModel.shared.isScheduled == 1  {
                showDeliveryView()
            //}
            
            break
        case 3:
            showPaymentView()
            break
        case 4:
            showConfirmView()
            break
        default:
            break
        }
    }
    
    @IBAction func changeCartSegment(_ sender: UIButton) {
        if sender.tag == 2 {
            if BQBookingModel.shared.isScheduled != 0 && BQBookingModel.shared.isScheduled != 1  {
                return
            }
        } else if sender.tag == 3 {
            if BQBookingModel.shared.paymentOption == nil {
                return
            }
        } else if sender.tag == 4 {
            if BQBookingModel.shared.paymentOption == nil || BQBookingModel.shared.isScheduled == 2 {
                return
            }
        }
        switchView(tag: sender.tag)
    }
    
    @IBAction func deliveryButtonAction(_ sender: UIButton) {
        switchView(tag: sender.tag)
    }
    
    func switchController(from controller: CartController) {
        switchView(tag: controller.rawValue+1)
    }
    
    
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        navigationBar = NavigationView.getNibFile(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "Cart", leftButtonImage: #imageLiteral(resourceName: "BackButtonWhite"), rightButtonImage: nil)
            , leftButtonAction: {[weak self] in
                self?.backButtonAction()
            }, rightButtonAction: nil)
        
        navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
    }
    
    
    func backButtonAction() {
        
        BQBookingModel.shared.paymentOption = nil
        BQBookingModel.shared.specialInstruction = nil
        BQBookingModel.shared.isScheduled = 2
        BQBookingModel.shared.paymentOption = nil
        BQBookingModel.shared.dateTime = nil
        
        if let navController = self.navigationController {
            let viewControllers: [UIViewController] = navController.viewControllers
            for aViewController in viewControllers {
                if aViewController is SSASideMenu {
                    if self.containerView.subviews.count == 2 {
                        resetAllStepsViews()
                        //self.containerView.subviews.first?.removeFromSuperview()
                        self.containerView.subviews.last?.removeFromSuperview()
                        add(asChildViewController: cartViewController)
                        
                    } else if self.containerView.subviews.count == 3 {
                        resetAllStepsViews()
                        showDeleveryStep()
                        //  removeAllViews()
                        //self.containerView.subviews.first?.removeFromSuperview()
                        self.containerView.subviews.last?.removeFromSuperview()
                        add(asChildViewController: deliveryViewController)
                        
                    } else if self.containerView.subviews.count == 4 {
                        resetAllStepsViews()
                        showPaymentStep()
                        //  removeAllViews()
                        // self.containerView.subviews.first?.removeFromSuperview()
                        self.containerView.subviews.last?.removeFromSuperview()
                        add(asChildViewController: paymentViewController)
                    } else {
                        self.navigationController!.popToViewController(aViewController, animated: true)
                    }
                }
            }
        }
        
    }
    
    func showCartView() {
        resetAllStepsViews()
        removeAllViews()
        add(asChildViewController: cartViewController)
    }

    fileprivate func showDeleveryStep() {
        deliveryView.backgroundColor = COLOR.App_Green_COLOR
        deliveryImage.image = #imageLiteral(resourceName: "deliverySelected")
        deliveyLabel.textColor = COLOR.App_Green_COLOR
        
        paymentView.backgroundColor = COLOR.SPLASH_LINE_COLOR
        paymentImage.image = #imageLiteral(resourceName: "payment")
        paymentLAbel.textColor = COLOR.SPLASH_LINE_COLOR
        
        confirmView.backgroundColor = COLOR.SPLASH_LINE_COLOR
        confirmImage.image = #imageLiteral(resourceName: "confirm")
        confirmLabel.textColor = COLOR.SPLASH_LINE_COLOR
    }
    
    func showDeliveryView() {
        showDeleveryStep()
      //  removeAllViews()
        add(asChildViewController: deliveryViewController)
    }
    
    fileprivate func showPaymentStep() {
        deliveryView.backgroundColor = COLOR.App_Green_COLOR
        deliveryImage.image = #imageLiteral(resourceName: "deliverySelected")
        deliveyLabel.textColor = COLOR.App_Green_COLOR
        
        paymentView.backgroundColor = COLOR.App_Green_COLOR
        paymentImage.image = #imageLiteral(resourceName: "paymentSelected")
        paymentLAbel.textColor = COLOR.App_Green_COLOR
        
        confirmView.backgroundColor = COLOR.SPLASH_LINE_COLOR
        confirmImage.image = #imageLiteral(resourceName: "confirm")
        confirmLabel.textColor = COLOR.SPLASH_LINE_COLOR
    }
    
    func showPaymentView() {
        showPaymentStep()
      //  removeAllViews()
        add(asChildViewController: paymentViewController)
    }
    
    fileprivate func showConfirmViewStep() {
        deliveryView.backgroundColor = COLOR.App_Green_COLOR
        deliveryImage.image = #imageLiteral(resourceName: "deliverySelected")
        deliveyLabel.textColor = COLOR.App_Green_COLOR
        
        paymentView.backgroundColor = COLOR.App_Green_COLOR
        paymentImage.image = #imageLiteral(resourceName: "paymentSelected")
        paymentLAbel.textColor = COLOR.App_Green_COLOR
        
        confirmView.backgroundColor = COLOR.App_Green_COLOR
        confirmImage.image = #imageLiteral(resourceName: "confirmSelected")
        confirmLabel.textColor = COLOR.App_Green_COLOR
    }
    
    func showConfirmView() {
        showConfirmViewStep()
       // removeAllViews()
        add(asChildViewController: confirmViewController)
    }
    
    func resetAllStepsViews() {
        
        deliveryView.backgroundColor = COLOR.SPLASH_LINE_COLOR
        deliveryImage.image = #imageLiteral(resourceName: "delivery")
        deliveyLabel.textColor = COLOR.SPLASH_LINE_COLOR
        
        paymentView.backgroundColor = COLOR.SPLASH_LINE_COLOR
        paymentImage.image = #imageLiteral(resourceName: "payment")
        paymentLAbel.textColor = COLOR.SPLASH_LINE_COLOR
        
        confirmView.backgroundColor = COLOR.SPLASH_LINE_COLOR
        confirmImage.image = #imageLiteral(resourceName: "confirm")
        confirmLabel.textColor = COLOR.SPLASH_LINE_COLOR
    }
    
    
    func removeAllViews() {
        for view in containerView.subviews {
            view.removeFromSuperview()
        }
    }
    
    
//    private func updateView() {
//        if segmentedControl.selectedSegmentIndex == 0 {
//            remove(asChildViewController: sessionsViewController)
//            add(asChildViewController: summaryViewController)
//        } else {
//            remove(asChildViewController: summaryViewController)
//            add(asChildViewController: sessionsViewController)
//        }
//    }


    
    // MARK: - Helper Methods
    private func add(asChildViewController viewController: UIViewController) {
        // Add Child View Controller
        addChildViewController(viewController)
        // Add Child View as Subview
        containerView.addSubview(viewController.view)
        // Configure Child View
        viewController.view.frame = containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // Notify Child View Controller
        viewController.didMove(toParentViewController: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        // Notify Child View Controller
        viewController.willMove(toParentViewController: nil)
        // Remove Child View From Superview
        viewController.view.removeFromSuperview()
        // Notify Child View Controller
        viewController.removeFromParentViewController()
    }
}
