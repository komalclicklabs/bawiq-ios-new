//
//  MerchantRefModal.swift
//  TookanVendor
//
//  Created by CL-Macmini-110 on 10/9/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation


class MerchantRefModal{
   
   var merchantKey = String()
   var merchantId = String()
   
   static var current : MerchantRefModal?
   
   
   init(json:[String:Any]) {
      if let value = json["api_keys"] as? [Any]{
         if value.count > 0{
            if let key = value[0] as? String{
               self.merchantKey = key
         }
      }
      }
      
      if let value = json["merchant_reference_ids"] as? [Any]{
         if value.count > 0{
            if let id = value[0] as? String{
               self.merchantId = id
            }else if let id = value[0] as? NSNumber{
               self.merchantId = id.stringValue
            }
         }
      }
      }
   }
   
   
   

