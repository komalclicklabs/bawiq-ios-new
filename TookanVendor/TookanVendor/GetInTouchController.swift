//
//  GetInTouchController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 09/01/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class GetInTouchController: UIViewController,PickerDelegate,ErrorDelegate ,UITextViewDelegate{

    @IBOutlet weak var middleLabel: UILabel!
    @IBOutlet weak var commentTextField: UITextView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var getInTouchLabel: UILabel!
    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    var errorMessageView:ErrorView!
    
    var cellInfoArray = [CUSTOM_FIELD_TYPE]()
    var pickerForDropdown:CustomDropdown!
    var countryCodeAndCountryName = [String:String]()
    var selectedLocale:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        commentTextField.delegate = self
        email.textColor = COLOR.SPLASH_TEXT_COLOR
        email.font = UIFont(name: FONT.ultraLight, size: 19.0)
        phoneNumber.textColor = COLOR.SPLASH_TEXT_COLOR
        phoneNumber.font = UIFont(name: FONT.ultraLight, size: 19.0)
        submitButton.setTitle(TEXT.SUBMIT, for: UIControlState.normal)
        submitButton.titleLabel?.font = UIFont(name: FONT.light, size: 19.0)
        titleLabel.text = TEXT.TUTORIALHEADING
        let firstAttributedText = NSMutableAttributedString(string: TEXT.TOCHECKOUT, attributes: [NSFontAttributeName:UIFont(name: FONT.light, size: FONT_SIZE.small)!])
        let secondAttributedText = NSMutableAttributedString(string: " " + TEXT.YOURTOOKANBOLD, attributes: [NSFontAttributeName:UIFont(name: FONT.regular, size: FONT_SIZE.small)!])
        let thirdAttributed = NSMutableAttributedString(string:" " + TEXT.CREATEDSPECIALY, attributes: [NSFontAttributeName:UIFont(name: FONT.light, size: FONT_SIZE.small)!])
        let fourthText = NSMutableAttributedString(string:  " " + TEXT.YOURBRANDBOLD, attributes: [NSFontAttributeName:UIFont(name: FONT.regular, size: FONT_SIZE.small)!])
        firstAttributedText.append(secondAttributedText)
        firstAttributedText.append(thirdAttributed)
         firstAttributedText.append(fourthText)
        middleLabel.attributedText = firstAttributedText
        titleLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.medium)
        
        if APP_DEVICE_TYPE == "5"{
        contentLabel.text = TEXT.TUTORIALCONTENTDEL
        }else{
        contentLabel.text = TEXT.TUTORIALCONTENTAPOINTMENT
        }
        contentLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.small)
        getInTouchLabel.text = TEXT.GET_IN_TOUCH
        getInTouchLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.medium)
        email.text = Vendor.current!.email!
        commentTextField.text = TEXT.ADD_COMMENT
        commentTextField.textColor = COLOR.PLACEHOLDER_COLOR
        countryCode.font = UIFont(name: FONT.light, size: 19.0)
        commentTextField.font = UIFont(name: FONT.ultraLight, size: 19.0)
        if (Vendor.current!.phoneNo.components(separatedBy: " ").count) > 1{
            let array = Vendor.current!.phoneNo.components(separatedBy: " ")
            phoneNumber.text = array[1]
            countryCode.text = "+"+array[0].replacingOccurrences(of: "+", with: "")
        }else{
             phoneNumber.text = Vendor.current!.phoneNo
        }
        
       
        
        // Do any additional setup after loading the view.
        
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == TEXT.ADD_COMMENT{
            textView.text = ""
            textView.textColor = COLOR.SPLASH_TEXT_COLOR
        }
    
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            textView.text = TEXT.ADD_COMMENT
            textView.textColor = COLOR.PLACEHOLDER_COLOR
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        submitButton.layer.cornerRadius = 20

        submitButton.clipsToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func countryButtonAction(_ sender: UIButton) {
        showCountryCode(sender: sender)
    }
    
    
    
    func showCountryCode(sender:UIButton) {
        self.view.endEditing(true)
        let countryArray = NSMutableArray()
        for locale in NSLocale.locales() {
            countryArray.add(locale.countryName)
            countryCodeAndCountryName[locale.countryName] = locale.countryCode
        }
        pickerForDropdown = UINib(nibName: NIB_NAME.customDropdown, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomDropdown
        pickerForDropdown.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        pickerForDropdown.valueArray =  countryArray
        pickerForDropdown.delegate = self
        self.view.addSubview(pickerForDropdown)
        pickerForDropdown.setPickerView(customFieldRow: sender.tag)
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        
          checkValidation()
        
        
    }
    
    
    
    func checkValidation() {
        self.view.endEditing(true)
        let email = (self.email.text?.trimText)!
        let contact = (countryCode.text)! + " " + (self.phoneNumber.text?.trimText)!
        var comment = String()
        
        guard Singleton.sharedInstance.validateEmail(email) == true else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_EMAIL)
            return
        }
        
        guard Singleton.sharedInstance.validatePhoneNumber(phoneNumber: contact) == true else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PHONE_NUMBER)
            return
        }
        
        if commentTextField.text == TEXT.ADD_COMMENT {
            comment = ""
        }else{
            comment = commentTextField.text!
        }
        
        self.sendRequest(email: email, phoneNumber: contact, comments: comment)
      //  self.serverRequest(email: email, password: password, name: name, contact: contact)
    }
    
    
    func showErrorMessage(error:String) {
        if errorMessageView == nil {
            errorMessageView = UINib(nibName: NIB_NAME.errorView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
            errorMessageView.delegate = self
            errorMessageView.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: HEIGHT.errorMessageHeight)
            self.view.addSubview(errorMessageView)
            errorMessageView.setErrorMessage(message: error,isError: true)
        }
    }
    
    //MARK: ERROR DELEGATE METHOD
    func removeErrorView() {
        self.errorMessageView = nil
    }
    
    func sendRequest(email:String,phoneNumber:String,comments:String) {
        let param = [
        "name":Vendor.current!.firstName!,
        "email":"\(email)",
        "phone_no":phoneNumber,
        "comments":comments,
        "app_access_token":Vendor.current!.appAccessToken!,
        "app_device_type":APP_DEVICE_TYPE
        ]
        print(param)
        if IJReachability.isConnectedToNetwork() == true{
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.serverCall(apiName: "get_in_touch_vendor", params: param as [String : AnyObject]?, httpMethod: "POST") { (success, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if success == true{
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.refreshHomeScreenAfterCreateTask), object: nil, userInfo: response)
              _ = self.navigationController?.popToRootViewController(animated: true)
                UserDefaults.standard.set("0", forKey: USER_DEFAULT.isFirstTimeSignUp)
            }else{
                self.showErrorMessage(error: response["message"] as? String ?? "")
            }
        }
        }else{
            
              self.showErrorMessage(error:ERROR_MESSAGE.NO_INTERNET_CONNECTION)
        }
    }
    
    
    
    func dismissPicker() {
        pickerForDropdown = nil
    }
    
    func selectedValueFromPicker(_ value: String, customFieldRowIndex: Int) {
        if countryCodeAndCountryName[value] != nil {
            let locale = countryCodeAndCountryName[value]!
            self.selectedLocale = locale
            if dialingCode[selectedLocale] != nil {
                countryCode.text = "+" + dialingCode[self.selectedLocale]!
            } else {
                countryCode.text = "+1"
            }
        } else {
            countryCode.text = "+1"
        }
        pickerForDropdown = nil
        
        
    }

}
