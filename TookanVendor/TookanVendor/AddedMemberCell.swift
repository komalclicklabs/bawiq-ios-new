//
//  AddedMemberCell.swift
//  TookanVendor
//
//  Created by Vishal on 31/01/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit
protocol AddedMemberCellDelegate {
    func deleteButtonAction(tag: Int)
    func editButtonAction(tag: Int)
}

class AddedMemberCell: UITableViewCell {

    @IBOutlet weak var backgroundViewCell: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var walletLimit: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var editMemberBtn: UIButton!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var dobLbl: UILabel!
    @IBOutlet weak var nationalityLbl: UILabel!
    
    var delegate: AddedMemberCellDelegate?
    
    @IBAction func deleteButtonAction(_ sender: UIButton) {
        delegate?.deleteButtonAction(tag: sender.tag)
    }
    
    @IBAction func editButtonAction(_ sender: UIButton) {
        delegate?.editButtonAction(tag: sender.tag)
    }
    
    
    func setupData(_data: FamilyMember) {
        self.name.text = _data.username
        self.email.text = _data.email
        self.walletLimit.text = "AED " + String(_data.walletLimit)
        self.phone.text = _data.phoneNo
        if let gender = _data.gender {
            self.genderLbl.isHidden = false
            self.genderLbl.text = gender
        } else {
            self.genderLbl.isHidden = true
        }
        if let dob = _data.dob {
            self.dobLbl.isHidden = false
            self.dobLbl.text = dob
        } else {
            self.dobLbl.isHidden = true
        }
        if let nationality = _data.nationality {
            self.nationalityLbl.isHidden = false
            self.nationalityLbl.text = nationality
        } else {
            self.nationalityLbl.isHidden = true
        }
        self.backgroundViewCell.setShadow()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
