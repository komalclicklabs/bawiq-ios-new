//
//  BQOrderDetailsPresenter.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 15/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

@objc protocol OrderDetailApi: class  {
    @objc optional func successInOrderDetail()
    @objc optional func errorInOrderDetailPage()
    
}

protocol OrderDetailsView {
    func numberOfItemsInTable() -> Int
    func setupDataTable(view: ProductCellView, index: Int)
    func viewItemsInOrder()
    func cancelOrder()
}


class BQOrderDetailsPresenter: NSObject {
    var products = [CartProducts]()
    weak var view: OrderDetailApi?
    
    init(withView view: OrderDetailApi) {
        self.view = view
    }
}
