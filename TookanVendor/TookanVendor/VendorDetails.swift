//
//  VendorDetails.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 18/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
import CoreLocation

enum SignUptype: Int {
    case family = 1
    case individual = 0
}

class Vendor: NSObject, NSCoding {
    
    static var current: Vendor?
    
    
    var addressLine1: String?
    var addressLine2: String?
    var addressLine3: String?
    
    var accessToken: String?
    var address: String?
    var appAccessToken: String?
    var appVersionCode: String?
    var company: String?
    var creationDateTime: String?
    var vendorDescription: String?
    var email: String? = ""
    var firstName: String? = ""
    var isBlocked: Int?
    var language: String?
    var lastLoginDateTime: String?
    var lastName: String? = ""
    var latitude: Double?
    var longitude: Double?
    var phoneNo: String = ""
    var userId: Int?
    var id: Int? = 0
    var vendorImage: String?
    var is_phone_verified = "0"
    var pendingAmount = Double()
    var referralCode = String()
    var credits = String()
    var isFirstSignup = false   
    var welcomePopUpMessage = ""
    var isSosEnabled = false
    var referenceId = ""
    var isAccountVerified: String? = "0"
    var username: String? = ""
    var signUpType = 0 // 1 == family
    var refferal_code: String?
    var wallet_amount: Double? = 0.0
    var socialSignUpType: Int? = 0
    var promotions: Promotions?
    var gender: String?
    var dob: String?
    var nationality: String?
    
    required init(coder aDecoder: NSCoder) {
        
        accessToken = aDecoder.decodeObject(forKey: "app_access_token") as? String
        address = aDecoder.decodeObject(forKey: "address") as? String
        appAccessToken = aDecoder.decodeObject(forKey: "appAccessToken") as? String
        appVersionCode = aDecoder.decodeObject(forKey: "appVersionCode") as? String
        company = aDecoder.decodeObject(forKey: "company") as? String
        creationDateTime = aDecoder.decodeObject(forKey: "creationDateTime") as? String
        vendorDescription = aDecoder.decodeObject(forKey: "vendorDescription") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        firstName = aDecoder.decodeObject(forKey: "firstName") as? String
        isBlocked = aDecoder.decodeObject(forKey: "isBlocked") as? Int
        language = aDecoder.decodeObject(forKey: "language") as? String
        lastLoginDateTime = aDecoder.decodeObject(forKey: "lastLoginDateTime") as? String
        lastName = aDecoder.decodeObject(forKey: "lastName") as? String
        latitude = aDecoder.decodeObject(forKey: "latitude") as? Double
        longitude = aDecoder.decodeObject(forKey: "longitude") as? Double
        phoneNo = (aDecoder.decodeObject(forKey: "phone_no") as? String) ?? ""
        vendorImage = aDecoder.decodeObject(forKey: "vendor_image") as? String
        userId = aDecoder.decodeObject(forKey: "userId") as? Int
        id = aDecoder.decodeObject(forKey: "vendorId") as? Int
        isAccountVerified = aDecoder.decodeObject(forKey: "is_account_verified") as? String
        isAccountVerified = aDecoder.decodeObject(forKey: "refferal_code") as? String
        
        username = aDecoder.decodeObject(forKey: "username") as? String
        wallet_amount = aDecoder.decodeObject(forKey: "wallet_amount") as? Double
        
        signUpType = (aDecoder.decodeObject(forKey: "sign_up_type") as? Int)!
        socialSignUpType = aDecoder.decodeObject(forKey: "social_sign_up_type") as? Int
        promotions = aDecoder.decodeObject(forKey: "promotions") as? Promotions
        
        gender = aDecoder.decodeObject(forKey: "gender") as? String
        dob = aDecoder.decodeObject(forKey: "dob") as? String
        nationality = aDecoder.decodeObject(forKey: "nationality") as? String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(accessToken, forKey: "accessToken")
        aCoder.encode(address, forKey: "address")
        aCoder.encode(appAccessToken, forKey: "appAccessToken")
        aCoder.encode(appVersionCode, forKey: "appVersionCode")
        aCoder.encode(company, forKey: "company")
        aCoder.encode(creationDateTime, forKey: "creationDateTime")
        aCoder.encode(vendorDescription, forKey: "vendorDescription")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(isBlocked, forKey: "isBlocked")
        aCoder.encode(language, forKey: "language")
        aCoder.encode(lastLoginDateTime, forKey: "lastLoginDateTime")
        aCoder.encode(lastName, forKey: "lastName")
        aCoder.encode(latitude, forKey: "latitude")
        aCoder.encode(longitude, forKey: "longitude")
        aCoder.encode(phoneNo, forKey: "phone_no")
        aCoder.encode(userId, forKey: "userId")
        aCoder.encode(id, forKey: "vendorId")
        aCoder.encode(vendorImage, forKey: "vendor_image")
        aCoder.encode(isAccountVerified, forKey: "is_account_verified")
        aCoder.encode(username, forKey: "username")
        aCoder.encode(username, forKey: "sign_up_type")
        aCoder.encode(refferal_code, forKey: "refferal_code")
        aCoder.encode(wallet_amount, forKey: "wallet_amount")
        aCoder.encode(socialSignUpType, forKey: "social_sign_up_type")
        aCoder.encode(promotions, forKey: "promotions")
        
        aCoder.encode(gender, forKey: "gender")
        aCoder.encode(dob, forKey: "dob")
        aCoder.encode(nationality, forKey: "nationality")
        
    }
    
    init(json: [String: Any]) {
        
        print(json)
        accessToken = (json["access_token"] as? String) ?? ""
        appAccessToken = (json["app_access_token"] as? String) ?? ""
        if let value = json["app_versioncode"] {
            appVersionCode = "\(value)"
        }
        
        if let value = json["vendor_id"] {
            id = Int("\(value)") ?? 0
        }
        if let value = json["user_id"] as? Int {
            self.userId = value
        } else if let value = json["user_id"] as? String {
            self.userId = Int(value)
        } else {
            self.userId = 0
        }
        username = (json["username"] as? String) ?? ""
        firstName = (json["first_name"] as? String) ?? ""
        lastName = (json["last_name"] as? String) ?? ""
        vendorImage = (json["vendor_image"] as? String) ?? ""
        email = (json["email"] as? String) ?? ""
        phoneNo = (json["phone_no"] as? String) ?? ""
        address = (json["address"] as? String) ?? ""
        referralCode = (json["referral_code"] as? String) ?? ""
        vendorDescription = (json["description"] as? String) ?? ""
        company = (json["company"] as? String) ?? ""
        refferal_code = (json["refferal_code"] as? String) ?? ""
        wallet_amount = (json["wallet_amount"] as? Double) ?? 0.0
        socialSignUpType = (json["social_sign_up_type"] as? Int) ?? 0
        
        gender = (json["gender"] as? String) ?? ""
        dob = (json["dob"] as? String) ?? ""
        nationality = (json["nationality"] as? String) ?? ""
        
        
        if let value = json["credits"] {
            self.credits = (Double("\(value)") ?? 0).description
        }
        
        if let value = json["latitude"] {
            self.latitude = Double("\(value)") ?? 0
        }
        
        if let value = json["longitude"] {
            self.longitude = Double("\(value)") ?? 0
        }
        
        creationDateTime = (json["creation_date_time"] as? String) ?? ""
        lastLoginDateTime = (json["last_login_date_time"] as? String) ?? ""
        isBlocked = (json["is_blocked"] as? Int) ?? 0
        language = (json["language"] as? String) ?? ""
        
        if let value = json["is_phone_verified"] as? String{
            self.is_phone_verified = value
        }else if let value = json["is_phone_verified"] as? Int{
            self.is_phone_verified = "\(value)"
        }
        
        if let value = json["is_account_verified"] as? String {
            self.isAccountVerified = value
        }else if let value = json["is_account_verified"] as? Int{
            self.isAccountVerified = "\(value)"
        }
        
        
        if let value = json["pending_amount"] as? Double{
            self.pendingAmount = 0.0
        }else if let value = json["pending_amount"] as? String{
            self.pendingAmount = 0.0
        }
        
        if let id = json["reference_id"] as? String{
            self.referenceId = id
        }else if let id = json["reference_id"] as? NSNumber{
            self.referenceId = id.stringValue
        }
        
        if let sut = json["sign_up_type"] as? Int {
            self.signUpType = sut
        }
    }
    
    
    struct Promotions {
        var description: String?
        var image_url: String?
        var promotion_id: Int?
        var title: String?
        
        init(params: [String: Any]) {
            if let description = params["description"] as? String {
                self.description = description
            }
            if let image_url = params["image_url"] as? String {
                self.image_url = image_url
            }
            if let promotion_id = params["promotion_id"] as? Int {
                self.promotion_id = promotion_id
            }
            if let title = params["title"] as? String {
                self.title = title
            }
        }
    }
    
    
    // MARK: - Methods
    func getCreditsDescription() -> String {
        let creditsInInt = Int(Double(credits) ?? 0)
        let creditString = creditsInInt == 0 ? TEXT.NO : creditsInInt.description
        
        let creditSuffix: String
        if creditsInInt == 0 || creditsInInt > 1 {
            creditSuffix = "Credits"
        } else {
            creditSuffix = "Credit"
        }
        
        return creditString + " " + creditSuffix
    }
    
    
    func sendReferralCodeToServer(referralCode: String, completion: Completion) {
        guard let params = getParamsForSendingreferalCode(referralCode: referralCode) else {
            completion?(false)
            return
        }
        
        HTTPClient.makeConcurrentConnectionWith(method: .POST, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: false, para: params, extendedUrl: API_NAME.sendReferral) { (responseObject, _, _, statusCode) in
            
            if let response = responseObject as? [String: Any],
                let data = response["data"] as? [String: Any],
                let newCredits = data["new_credits"] {
                let creditsInInt = Int("\(newCredits)") ?? 0
                self.credits = creditsInInt.description
            }
            
            if statusCode == .some(STATUS_CODES.SHOW_DATA) {
                completion?(true)
            } else {
                completion?(false)
            }
        }
    }
    
    private func getParamsForSendingreferalCode(referralCode: String) -> [String: Any]? {
        guard let accessToken = self.accessToken,
            let appAccessToken = self.appAccessToken,
            let userId = self.userId else {
                print("UserId or AppAccessToken or accessToken not found")
                return nil
        }
        
        return [
            "user_id": userId,
            "access_token": accessToken,
            "app_access_token": appAccessToken,
            "device_token": APIManager.sharedInstance.getDeviceToken(),
            "app_version": APP_VERSION,
            "app_device_type": APP_DEVICE_TYPE,
            "referral_code": referralCode
        ]
    }
    
    
    // MARK: - Type Methods
    static func logInWith(data: [String: Any]) {
        
        if let value = data["welcome_pop_up"] as? String {
            Vendor.current?.welcomePopUpMessage = value
        } else {
            Vendor.current?.welcomePopUpMessage = ""
        }
        
        
        if let details = data["vendor_details"] as? [String:Any] {
            let vendorDetails = Vendor(json: details)
            Vendor.current = vendorDetails
            Vendor.userHasLoggedIntoAppAtLeastOnce()
            
            UserDefaults.standard.set(vendorDetails.appAccessToken, forKey: USER_DEFAULT.accessToken)
            UserDefaults.standard.set((vendorDetails.firstName ?? "") + " " + (vendorDetails.lastName ?? ""), forKey: USER_DEFAULT.vendorName)
            UserDefaults.standard.set(vendorDetails.email, forKey: USER_DEFAULT.emailAddress)
            UserDefaults.standard.set(vendorDetails.company, forKey: USER_DEFAULT.companyName)
            UserDefaults.standard.set(vendorDetails.address, forKey: USER_DEFAULT.address)
            UserDefaults.standard.set(vendorDetails.phoneNo, forKey: USER_DEFAULT.phoneNumber)
        }
        
        if let promotionsData = data["promotion"] as? [[String: Any]] {
            if promotionsData.count > 0 {
                let result = Promotions(params: promotionsData[0])
                Vendor.current?.promotions = result
            }
        }
        
        if let typeCategories = data["categories"] as? [Any] {
            Singleton.sharedInstance.typeCategories.removeAll()
            print(typeCategories)
            for i in typeCategories{
                if let data = i as? [String:Any]{
                    Singleton.sharedInstance.typeCategories.append(CarTypeModal(json:data))
                }
            }
        }
        if let formDetails = data["formSettings"] as? [AnyObject] {
            if let details = formDetails[0] as? [String:Any] {
                Singleton.sharedInstance.formDetailsInfo = FormDetails(json: details)
                print(formDetails)
                Singleton.sharedInstance.formDetailsInfo.currencyid = AppConfiguration.current.currencyid
            }
        }
        if let userOptions = data["userOptions"] as? [String:Any] {
            Singleton.sharedInstance.customFieldOptionsPickup = userOptions
        }
        if let userOptions = data["deliveryOptions"] as? [String:Any] {
            Singleton.sharedInstance.customFieldOptionsDelivery = userOptions
        }
        
        if let value = data["vendorPromos"] as? [String:Any] {
            Singleton.sharedInstance.getPromoCodeArray(data: value)
        }
        
        if let value = data["new_user_id"] as? Int{
            Vendor.current?.userId = value
        }
        
        if let newDeviceType = data["ios_device_type"] as? Int{
            logEvent(label: "user_sign_up_demo")
            UserDefaults.standard.set("\(newDeviceType)", forKey: "deviceType")
        }
        
        Singleton.sharedInstance.isSignedIn = true
    }
    
    static func checkUserWithEmailExists(email: String,owener:UIViewController = UIViewController(), completion: Completion) {
        let params = getParamsToCheckEmailExists(email: email)
        
        HTTPClient.makeConcurrentConnectionWith(method: .POST, showAlertInDefaultCase: false, para: params, extendedUrl: API_NAME.checkEmailRegistered) { (responseObject, _, _, statusCode) in
            if statusCode == .some(STATUS_CODES.SHOW_DATA) {
                completion?(true)
            } else if statusCode == .some(STATUS_CODES.BAD_REQUEST) {
                completion?(false)
            } else if statusCode == .some(STATUS_CODES.INVALID_ACCESS_TOKEN) {
                Singleton.sharedInstance.showAlertWithOption(owner: owener, title: "", message: (responseObject as! [String:Any])["message"] as! String, showRight: false, leftButtonAction: {
                    print("")
                }, rightButtonAction: nil, leftButtonTitle: "Ok", rightButtonTitle: "")
                //        completion?(false)
            }
        }
    }
    
    private static func getParamsToCheckEmailExists(email: String) -> [String: Any] {
        return [
            "device_token": APIManager.sharedInstance.getDeviceToken(),
            "app_version": APP_VERSION,
            "app_device_type": APP_DEVICE_TYPE,
            "email": email
        ]
    }
    
    static func hasSignedUpBefore() -> Bool {
        if let flag = UserDefaults.standard.value(forKey: "hasUserSignedUpBefore") as? Bool {
            return flag
        }
        
        return false
    }
    
    static func userHasLoggedIntoAppAtLeastOnce() {
        UserDefaults.standard.set(true, forKey: "hasUserSignedUpBefore")
    }
    
    func isDemoUser() -> Bool {
        if let accessToken = self.appAccessToken {
            return accessToken == "" ? true : false
        }
        return true
    }
}
