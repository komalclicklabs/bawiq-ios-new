//
//  PleaseWaitView.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 27/03/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class PleaseWaitView: UIView {

    @IBOutlet weak var pleaseWaitWhile: UILabel!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
    pleaseWaitWhile.text = TEXT.WHILE_WE_FETCH
    pleaseWaitWhile.textColor = COLOR.SPLASH_TEXT_COLOR
    self.backgroundColor = COLOR.popUpColor
    pleaseWaitWhile.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
    setTitleView()
    
    }
    
    
    func setTitleView() {
        let titleView = UINib(nibName: NIB_NAME.titleView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TitleView
        titleView.frame = CGRect(x: 0, y: 15, width: SCREEN_SIZE.width, height: 70)
        titleView.backgroundColor = UIColor.clear
        titleView.setTitle(title: TEXT.PLEASE.capitalized, boldTitle: TEXT.WAIT)
        self.addSubview(titleView)
       
    }

}
