//
//  BQFeedbackImageCell.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 14/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQFeedbackImageCell: UITableViewCell {

   @IBOutlet weak var imgStore: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgStore.layer.cornerRadius = 0.0
        imgStore.layer.borderColor = UIColor.gray.cgColor
        imgStore.layer.borderWidth = 1.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
