//
//  CustomTextField.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 16/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit


@objc protocol ProfileProtocol {
    @objc func fieldEndUpdate(fieldType:Int,yourText:String)
}
protocol CustomFieldDelegate {
    func customFieldShouldReturn(fieldType:FIELD_TYPE)
}

class CustomTextField: UIView, UITextFieldDelegate {

    @IBOutlet var textField: UITextField!
    @IBOutlet var rightButton: UIButton!
    @IBOutlet var underline: UIView!
    @IBOutlet var viewPasswordButton: UIButton!
    @IBOutlet var validStatus: UIImageView!
    @IBOutlet weak var countryCodeButton: UIButton!
    @IBOutlet weak var textfieldLeadingConstraint: NSLayoutConstraint!
    
    var inactiveImage:UIImage!
  //  var activeImage:UIImage!
  //  var filledImage:UIImage!
    var unlockImage:UIImage!
    var delegate:CustomFieldDelegate!
    var fieldType:FIELD_TYPE!
    var profileDelegate : ProfileProtocol!
    
    @IBOutlet weak var bottomLineBottomConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        underline.backgroundColor = COLOR.SPLASH_LINE_COLOR
        textField.textColor = COLOR.SPLASH_TEXT_COLOR
        textField.font = UIFont(name: FONT.ultraLight, size: 19.0)
        textField.delegate = self
        textField.tintColor = COLOR.THEME_FOREGROUND_COLOR
        
        self.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        /*----------- View password Button ---------------*/
        viewPasswordButton.titleLabel?.font = UIFont(name: FONT.regular, size: 10.0)
        viewPasswordButton.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: UIControlState.normal)
        viewPasswordButton.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: UIControlState.selected)
        viewPasswordButton.setTitle(TEXT.VIEW_PASSWORD, for: UIControlState.normal)
        viewPasswordButton.setTitle(TEXT.HIDE_PASSWORD, for: UIControlState.selected)
        viewPasswordButton.isHidden = true
        
        /*------------ Country Code -------------*/
        countryCodeButton.titleLabel?.font = UIFont(name: FONT.ultraLight, size: FONT_SIZE.large)
        countryCodeButton.setTitleColor(COLOR.SPLASH_TEXT_COLOR, for: UIControlState.normal)
        let code:String!
        let selectedLocale = Singleton.sharedInstance.currentDialingCode().uppercased()
        if(selectedLocale.length > 0) {
            if dialingCode[selectedLocale] != nil {
                code = "+" + dialingCode[selectedLocale]!
            } else {
                code = "+1"
            }
        } else {
            code = "+1"
        }
        countryCodeButton.setTitle(code, for: UIControlState.normal)
    }
    
    func setViewForPhoneOrNot(){
       //
        self.countryCodeButton.isHidden = true
        switch fieldType! {
        case .contact:
            //self.bottomLineBottomConstraint.constant = 0
            self.countryCodeButton.isHidden = false
            self.textfieldLeadingConstraint.constant = 70
//        case .contact:
//            self.countryCodeButton.isHidden = false
//            self.textfieldLeadingConstraint.constant = 70
        default:
            self.textfieldLeadingConstraint.constant = 15
        }
        self.layoutIfNeeded()
        
        let code:String!
        let selectedLocale = Singleton.sharedInstance.currentDialingCode().uppercased()
        if(selectedLocale.length > 0) {
            if dialingCode[selectedLocale] != nil {
                code = "+" + dialingCode[selectedLocale]!
            } else {
                code = "+1"
            }
        } else {
            code = "+1"
        }
        self.countryCodeButton.setTitle(code, for: UIControlState.normal)
    }
    
    func setImageForDifferentStates(inactive:UIImage, placeholderText:String, isPasswordField:Bool, unlock:UIImage?) {
        inactiveImage = inactive
        self.textField.attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSForegroundColorAttributeName: COLOR.PLACEHOLDER_COLOR])
        self.rightButton.setImage(self.inactiveImage.withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        if (self.textField.text?.length)! > 0 {
            setActiveInActiveStatus(yourState: ACTIVE_INACTIVE_STATES.filled)
        } else {
            setActiveInActiveStatus(yourState: ACTIVE_INACTIVE_STATES.inActive)
        }
        if isPasswordField == true {
            self.textField.isSecureTextEntry = true
            self.textField.clearsOnBeginEditing = false
            self.viewPasswordButton.isHidden = false
            if unlock != nil {
                self.unlockImage = unlock
            }
        }
    }
    
    //MARK: UITEXTFIELD DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.validStatus.isHidden = true
        self.shrinkUnderLine()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.showInactiveUnderline()
        profileDelegate?.fieldEndUpdate(fieldType: self.fieldType.hashValue, yourText: textField.text!)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        switch self.fieldType! {
        case .contact:
            let newText = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
            if newText.length > 15 && string != "" {
                return false
            }
        default: break
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        delegate.customFieldShouldReturn(fieldType: self.fieldType)
        return true
    }
    
    
    func shrinkUnderLine() {
        UIView.animate(withDuration: 0.2, animations: {
            self.underline.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            }, completion: { finished in
                self.showActiveUnderLine()
        })
    }
    
    func setActiveInActiveStatus(yourState:String) {
        switch yourState {
        case ACTIVE_INACTIVE_STATES.active:
            self.rightButton.tintColor = COLOR.ACTIVE_IMAGE_COLOR
        case ACTIVE_INACTIVE_STATES.inActive:
            self.rightButton.tintColor = COLOR.IN_ACTIVE_IMAGE_COLOR
        case ACTIVE_INACTIVE_STATES.filled:
            self.rightButton.tintColor = COLOR.FILLED_IMAGE_COLOR
        default :
            self.rightButton.tintColor = COLOR.IN_ACTIVE_IMAGE_COLOR
        }
        //IMAGE
       /* switch yourState {
        case ACTIVE_INACTIVE_STATES.active:
            self.rightButton.setImage(self.activeImage, for: UIControlState.normal)
        case ACTIVE_INACTIVE_STATES.inActive:
            self.rightButton.setImage(self.inactiveImage, for: UIControlState.normal)
        case ACTIVE_INACTIVE_STATES.filled:
            self.rightButton.setImage(self.filledImage, for: UIControlState.normal)
        default :
           print("")
        }*/
    }
    
    func showActiveUnderLine() {
        UIView.animate(withDuration: 0.2, animations: {
            self.underline.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
            if self.viewPasswordButton.isHidden == false && self.viewPasswordButton.isSelected == true {
                self.rightButton.setImage(self.unlockImage.withRenderingMode(.alwaysTemplate), for: .normal)
                self.setActiveInActiveStatus(yourState:ACTIVE_INACTIVE_STATES.active)
            } else {
                self.rightButton.setImage(self.inactiveImage.withRenderingMode(.alwaysTemplate), for: .normal)
                self.setActiveInActiveStatus(yourState:ACTIVE_INACTIVE_STATES.active)
            }
            self.underline.transform = CGAffineTransform.identity
            }, completion: { finished in
                
        })
    }
    
    func showInactiveUnderline() {
        self.underline.backgroundColor = COLOR.SPLASH_LINE_COLOR
        if self.textField.text?.length == 0 {
            setActiveInActiveStatus(yourState:ACTIVE_INACTIVE_STATES.inActive )
        } else {
            switch fieldType! {
            case .email:
                if Singleton.sharedInstance.validateEmail((self.textField.text?.trimText)!) == true {
                    self.setSuccessStatus()
                } else {
                    self.setFailureStatus()
                }
                setActiveInActiveStatus(yourState:ACTIVE_INACTIVE_STATES.filled )
                //self.rightButton.setImage(self.filledImage, for: UIControlState.normal)
                break
            default:
                setActiveInActiveStatus(yourState:ACTIVE_INACTIVE_STATES.filled )
                break
            }
            
        }
        self.underline.transform = CGAffineTransform.identity
    }
    
    @IBAction func showHidePasswordAction(_ sender: AnyObject) {
        viewPasswordButton.isSelected = !viewPasswordButton.isSelected
        self.textField.resignFirstResponder()
        if viewPasswordButton.isSelected == true {
            self.textField.isSecureTextEntry = false
            self.rightButton.setImage(unlockImage.withRenderingMode(.alwaysTemplate), for: .normal)
            if textField.isFirstResponder == true {
                setActiveInActiveStatus(yourState:ACTIVE_INACTIVE_STATES.active)
            } else {
                setActiveInActiveStatus(yourState:ACTIVE_INACTIVE_STATES.inActive)
            }
        } else {
            self.rightButton.setImage(self.inactiveImage.withRenderingMode(.alwaysTemplate), for: .normal)
            self.textField.isSecureTextEntry = true
            if textField.isFirstResponder == true {
                setActiveInActiveStatus(yourState:ACTIVE_INACTIVE_STATES.active)
            } else {
                setActiveInActiveStatus(yourState:ACTIVE_INACTIVE_STATES.inActive)
            }
        }
        textField.font = UIFont(name: FONT.ultraLight, size: 19.0)
        self.textField.becomeFirstResponder()
    }
    
    func setSuccessStatus() {
        self.validStatus.image = IMAGE.successIcon//.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        self.validStatus.tintColor = COLOR.SPLASH_TEXT_COLOR
        self.validStatus.isHidden = false
    }
    
    func setFailureStatus() {
        
        self.validStatus.image = IMAGE.failureIcon//.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        self.validStatus.tintColor = COLOR.SPLASH_TEXT_COLOR
        self.validStatus.isHidden = false
    }
    
    func setValidStatusHide() {
        self.validStatus.isHidden = true
    }
}
