//
//  SideMenuViewController.swift
//  Picker
//
//  Created by ANKUSH BHATIA on 29/01/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {
    // MARK: - Properties
    var presenter: SidemenuPresenter!
    
    // MARK: - IBOutlets
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageUser: UIImageView!
    @IBOutlet weak var labelName: UILabel! {
        didSet {
            labelName.text = Vendor.current?.username ?? "Guest User" // LoginManager.share.me?.fullName ?? ""
        }
    }
    @IBOutlet weak var labelEmail: UILabel! {
        didSet {
            labelEmail.text = Vendor.current?.email ?? "" // LoginManager.share.me?.email ?? ""
        }
    }
    
    fileprivate var selectedIndex = IndexPath(row: 0, section: 0)
    
// MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        shoWProfileImage(on: self.imageUser)
    }
    
    func shoWProfileImage(on image: UIImageView) {
        if let url = Vendor.current?.vendorImage {
            let imageUrl = URL.get(from: url)
            image.kf.setImage(with: imageUrl, placeholder: placeholdeImage)
        } 
    }
    @IBAction func openProfile(_ sender: UIButton) {
        sideMenuViewController?.hideMenuViewController()
        let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
        if isGuestUser {
            print("Guest User")
        } else {
            guard let controler = self.storyboard?.instantiateViewController(withIdentifier: "VendorProfileVC") as? VendorProfileVC else {
                return
            }
            sideMenuViewController?.contentViewController = controler
        }
    }
    
    // MARK: - Functions
    func setup() {
        self.presenter = SidemenuPresenter(withView: self)
        let nib = UINib(nibName: "SidebarTableViewCell", bundle: Bundle.main)
        self.tableView.register(nib, forCellReuseIdentifier: "SidebarCell")
    }
}

extension SideMenuViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.noOfSections
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.noOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SidebarCell", for: indexPath) as? SidebarTableViewCell else {
            fatalError("No cell found with this identifier")
        }
        
        let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
        if isGuestUser {
            guard let section = SideMenuSectionForGuest(rawValue: indexPath.section) else {
                return cell
            }
            if section != .home {
                cell.backview.backgroundColor = .gray
            }
            cell.labelTitle.text = SideMenuSectionForGuest.value(forSection: section)
            
            if let selectedIndex = selectedIndex as? IndexPath, selectedIndex.section == indexPath.section {
                cell.backview.backgroundColor = .gray
            } else {
                cell.backview.backgroundColor = .clear
            }
        } else {
            guard let section = SidemenuSection(rawValue: indexPath.section) else {
                return cell
            }
            if section != .home {
                cell.backview.backgroundColor = .gray
            }
            cell.labelTitle.text = SidemenuSection.value(forSection: section)
            
            if let selectedIndex = selectedIndex as? IndexPath, selectedIndex.section == indexPath.section {
                cell.backview.backgroundColor = .gray
            } else {
                cell.backview.backgroundColor = .clear
            }
        }
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        //func deselectOtherButton() {
            //let tableView = self.superview as! UITableView
            //let tappedCellIndexPath = tableView.indexPath(for: self)!
            //let section = tappedCellIndexPath.section
            //let rowCounts = tableView.numberOfRows(inSection: indexPath.section)
        
        let previousIndex = selectedIndex
        selectedIndex = indexPath
        if let indexPathIs = previousIndex as? IndexPath {
            tableView.reloadRows(at: [indexPathIs], with: .none)
        }
        tableView.reloadRows(at: [indexPath], with: .none)

        let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
        if isGuestUser {
            guard let section = SideMenuSectionForGuest(rawValue: indexPath.section) else {
                return
            }
            sideMenuViewController?.hideMenuViewController()
            switch section {
            case .home:
                guard let controler = self.storyboard?.instantiateViewController(withIdentifier: "HomeController") as? HomeVC else {
                    return
                }
                sideMenuViewController?.contentViewController = controler
            case .contactUs:
                if let vc = UIStoryboard(name: "MyOrders", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQContactUsVC") as? BQContactUsVC {
                    
                    sideMenuViewController?.contentViewController = vc
                }
            case .loginSignup:
                Singleton.sharedInstance.logoutButtonAction()
            }
        } else {
            guard let section = SidemenuSection(rawValue: indexPath.section) else {
                return
            }
            sideMenuViewController?.hideMenuViewController()
            switch section {
            case .home:
                guard let controler = self.storyboard?.instantiateViewController(withIdentifier: "HomeController") as? HomeVC else {
                    return
                }
                sideMenuViewController?.contentViewController = controler
            case .profile:
                guard let controler = self.storyboard?.instantiateViewController(withIdentifier: "VendorProfileVC") as? VendorProfileVC else {
                    return
                }
                sideMenuViewController?.contentViewController = controler
                
            case .savedLocation:
                if let vc = UIStoryboard(name: "Booking", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQUserSavedLocationsVC") as? BQUserSavedLocationsVC {
                    
                    sideMenuViewController?.contentViewController = vc
                }
            case .order:
                guard let controler = self.storyboard?.instantiateViewController(withIdentifier: "BQMyOrdersVC") as? BQMyOrdersVC else {
                    return
                }
                sideMenuViewController?.contentViewController = controler
                
            case .referEarn:
                if let vc = UIStoryboard(name: "MyOrders", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQReferEarnVC") as? BQReferEarnVC {
                    
                    sideMenuViewController?.contentViewController = vc
                }
                
            case .notification:
                if let vc = UIStoryboard(name: "MyOrders", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQnotificationsListVC") as? BQnotificationsListVC {
                    sideMenuViewController?.contentViewController = vc
                }
                
            case .contactUs:
                if let vc = UIStoryboard(name: "MyOrders", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQContactUsVC") as? BQContactUsVC {
                    
                    sideMenuViewController?.contentViewController = vc
                }
                
            case .payment:
                if let vc = UIStoryboard(name: "MyOrders", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQWalletVC") as? BQWalletVC {
                    
                    sideMenuViewController?.contentViewController = vc
                }
                
            case .myFavourites:
                if let vc = UIStoryboard(name: "MyOrders", bundle: Bundle.main).instantiateViewController(withIdentifier: "FavouriteViewController") as? FavouriteViewController {
                    sideMenuViewController?.contentViewController = vc
                }
            case .logout:
                logoutAction()
            }
        }
        
    }
    
    func logoutAction() {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: { [weak self] in
            let alertController = UIAlertController(title: "", message: TEXT.ARE_YOU_WANT_LOGOUT_ALERT, preferredStyle: .alert)
            let yesAction = UIAlertAction(title: TEXT.YES_ACTION, style: .default, handler: { (action) -> Void in
                Singleton.sharedInstance.logoutButtonAction()
                logEvent(label: "user_log_out")
            })
            let noAction = UIAlertAction(title: TEXT.NO, style: .cancel, handler: { (action) -> Void in
                self?.dismiss(animated: true, completion: nil)
            })
            alertController.addAction(yesAction)
            alertController.addAction(noAction)
            self?.present(alertController, animated: true, completion: nil)
        })
    }
}

extension SideMenuViewController: SidemenuView {
    func sendToSignInScreen() {
        guard let navigation = self.navigationController else {
            return
        }
//        for controller in navigation.viewControllers where controller is SplashVC {
//            self.navigationController?.popToViewController(controller, animated: true)
//        }
    }
}
