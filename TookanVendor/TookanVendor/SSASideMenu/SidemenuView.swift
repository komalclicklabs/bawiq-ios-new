//
//  SidemenuView.swift
//  Picker
//
//  Created by ANKUSH BHATIA on 29/01/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

protocol SidemenuView: class {
    func sendToSignInScreen()
}
