//
//  SidemenuPresenter.swift
//  Picker
//
//  Created by ANKUSH BHATIA on 29/01/18.
//  Copyright © 2018 Deepak. All rights reserved.
//

import UIKit

enum SidemenuSection: Int, CaseCountable {
    case home
    case profile
    case order
    case payment
    case myFavourites
    case savedLocation
    case referEarn
    case notification
    case contactUs
    case logout
    
    static func value(forSection section: SidemenuSection) -> String {
        switch section {
        case .home:
            return "Home".localized
        case .profile:
            return "My Account".localized
        case .order:
            return "Order".localized
        case .payment:
            return "My Payments".localized
        case .myFavourites:
            return "My favourites".localized
        case .savedLocation:
            return "My Saved Location(s)".localized
        case .referEarn:
            return "Promotional Vouchers" //"Refer & Earn".localized
        case .notification:
            return "My Notifications".localized
        case .contactUs:
            return "Contact Us".localized
        case .logout:
            return "Logout"
        }
    }
}

enum SideMenuSectionForGuest: Int, CaseCountable {
    case home
    case contactUs
    case loginSignup
    
    static func value(forSection section: SideMenuSectionForGuest) -> String {
        switch section {
        case .home:
            return "Home".localized
        case .contactUs:
            return "Contact Us".localized
        case .loginSignup:
            return "Login / SignUp"
        }
    }
}

class SidemenuPresenter: NSObject {
    // MARK: - Properties
    weak var view: SidemenuView?
    
    var noOfSectionsForUser: [SidemenuSection] = [.home, .profile, .order, .payment, .myFavourites, .savedLocation, .referEarn, .notification, .contactUs, .logout]
    
    var noOfSectionsForGuest: [SideMenuSectionForGuest] = [.home, .contactUs, .loginSignup]
    
    var noOfSections: Int {
        let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
        if isGuestUser {
            return noOfSectionsForGuest.count
        } else {
            return noOfSectionsForUser.count
        }
    }
    
    var noOfRows: Int {
        return 1
    }
    
    // MARK: - Initialisers
    init(withView view: SidemenuView) {
        self.view = view
    }
    
    // MARK: - Functions
    func logout() {
        
        Singleton.sharedInstance.logoutButtonAction()

//       LoginManager.share.logout { (response, error) in
//            guard error == nil else {
//                return
//            }
//            self.view?.sendToSignInScreen()
//        }
    }
}
