//
//  ProfileModel.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 12/13/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import Foundation
import UIKit


class ProfileDetails: NSObject {
    var image:UIImage!
    var fullName = ""
    var email = ""
    var phoneNumber = ""
    var company = ""
    var address = ""
    var countryCode = "+"
    var imageString = ""
    var firstName = ""
    var lastName = ""
    var oldPassWord = ""
    var newPassWord = ""
    var gender = ""
    var dob = ""
    var nationality = ""
    
    override init() {
    }
    
    func returnJson()->[String:AnyObject] {
        
        if fullName.components(separatedBy: " ").count > 1 {
            lastName = fullName.components(separatedBy: " ")[1]
        } else {
            lastName = ""
        }
        firstName = fullName.components(separatedBy: " ")[0]
        
      let jsonObject = [
         "access_token": Vendor.current!.appAccessToken!,
         "app_access_token" : Vendor.current!.appAccessToken!,
         "phone_no":/*countryCode + " " + */phoneNumber.trimText ,
         "first_name":firstName,
         "last_name" : lastName.trimText,
         "app_device_type": APP_DEVICE_TYPE
      ]
        return jsonObject as [String : AnyObject]
    }
}
