//
//  CustomDropdown.swift
//  Tookan
//
//  Created by Rakesh Kumar on 1/4/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

protocol PickerDelegate {
    func dismissPicker()
    func selectedValueFromPicker(_ value:String, customFieldRowIndex:Int)
}

class CustomDropdown: UIView, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var cancelButotn: UIBarButtonItem!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var toolbar: UIToolbar!
    @IBOutlet weak var customPicker: UIPickerView!
    var valueArray:NSMutableArray!
    var delegate:PickerDelegate!
    var customFieldRowIndex:Int!
    
    override func awakeFromNib() {
        customPicker.delegate = self
        customPicker.dataSource = self
        customPicker.backgroundColor = UIColor.white
        customPicker.transform = CGAffineTransform(translationX: 0, y: self.frame.height)
        toolbar.transform = CGAffineTransform(translationX: 0, y: self.frame.height)
        cancelButotn.title = "Cancel".localized
        doneButton.title = "Done".localized
    }
    
    func setPickerView(customFieldRow:Int) {
        self.customFieldRowIndex = customFieldRow
        UIView.animate(withDuration: 1.0, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.customPicker.transform = CGAffineTransform.identity
            self.toolbar.transform = CGAffineTransform.identity
            }, completion: nil)
    }
    
    
    @IBAction func cancelAction(_ sender: AnyObject) {
       self.delegate.dismissPicker()
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.customPicker.transform = CGAffineTransform(translationX: 0, y: self.frame.height)
            self.toolbar.transform = CGAffineTransform(translationX: 0, y: self.frame.height)
            }, completion: { finished in
                self.removeFromSuperview()
        })
    }
    
    @IBAction func doneAction(_ sender: AnyObject) {
        print(customPicker.selectedRow(inComponent: 0))
        let selectedValue = valueArray.object(at: customPicker.selectedRow(inComponent: 0))
        self.delegate.selectedValueFromPicker(selectedValue as! String, customFieldRowIndex: self.customFieldRowIndex)
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.customPicker.transform = CGAffineTransform(translationX: 0, y: self.frame.height)
            self.toolbar.transform = CGAffineTransform(translationX: 0, y: self.frame.height)
            }, completion: { finished in
                self.removeFromSuperview()
        })
    }
    
    //MARK: UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {

        let attributedString = NSMutableAttributedString(string: valueArray.object(at: row) as! String)
        return attributedString
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return valueArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
}
