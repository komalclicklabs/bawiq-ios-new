//
//  ProductView.swift
//  TookanVendor
//
//  Created by Vishal on 12/02/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

protocol ProductCellModel {
    var brand_name: String? {get}
    var productImage: String? {get}
    var productName: String? {get}
    var productDescription: String? {get}
    var productPrice: Double? {get}
    var product_id: Int? {get}
    var inCartCount: Int? {get set}
    var measuring_unit: String? {get}
    var pack_size: String? {get}
    var discount_percentage: Int? {get}
    var discounted_price: Float? {get}
    var is_fav: Int? {get}
    //var quantity: Int? {get}
}

struct ProductCellImplemention: ProductCellModel {
    var brand_name: String?
    var productName: String?
    var productDescription: String?
    var productPrice: Double?
    var productImage: String?
    var product_id: Int?
    var inCartCount: Int?
    var measuring_unit: String?
    var pack_size: String?
    var discount_percentage: Int?
    var discounted_price: Float?
    var is_fav: Int? = 0
    //var quantity: Int?
    
}

extension ProductCellImplemention {
    
    init(data: Products) {
        if let brandName = data.brand_name {
            self.brand_name = brandName
        } else {
            self.brand_name = ""
        }
        
        if let image = data.productImage {
            self.productImage = image
        } else {
            self.productImage = ""
        }
        if let name = data.productName {
            self.productName = name
        } else {
            self.productName = ""
        }
        if let desc = data.productDescription {
            self.productDescription = desc
        } else {
            self.productDescription = ""
        }
        if let price = data.productPrice {
            self.productPrice = price
        } else {
            self.productPrice = 0
        }
        
        if let id = data.product_id {
            self.product_id = id
        } else {
            self.product_id = 0
        }
        if let count = data.inCartCount {
            self.inCartCount = count
        } else {
            self.inCartCount = 0
        }
        
        if let isFav = data.is_fav {
            self.is_fav = isFav
        } else {
            self.is_fav = 0
        }
    }
}


protocol ProductCellView {
    func setupData(data: ProductCellModel)
    func setupDataForSubCategoryProducts(data: ProductCellModel)
    func setupDataForConfirmBooking(data: ProductCellModel)
    func updateValue(added: Bool)
}

protocol ProductCellDelegate {
    func productCellAddButtonTapped(cell: ProductCell)
    func productCellDeleteButtonTapped(cell: ProductCell)
    func productCellRemoveCategoryButtonTapped(cell: ProductCell)
    func productCellFavButtonTapped(cell: ProductCell, isfavStatus: Bool)
}
class ProductCell: UITableViewCell, ProductCellView {

    @IBOutlet weak var brandName: UILabel!
    @IBOutlet weak var discountImage: UIView!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var productDescription: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var leftProductDescription: UILabel!
    
    @IBOutlet weak var stepperView: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var addProduct: UIButton!
    @IBOutlet weak var productCount: UIButton!
    @IBOutlet weak var removeProduct: UIButton!
    @IBOutlet weak var favBtn: UIButton!
    
    var delegate: ProductCellDelegate?
    
    var showDeleteButton = false
    
    var inCartCount: Int = 0 {
        didSet {
            if inCartCount == 0 {
                productCount.isHidden = true
                removeProduct.isHidden = true
                //deleteButton.isHidden = true
            } else {
                //deleteButton.isHidden = false
                productCount.isHidden = false
                removeProduct.isHidden = false
                productCount.setTitle("\(inCartCount)", for: .normal)
            }
            self.layoutIfNeeded()
        }
    }
    override func draw(_ rect: CGRect) {
        self.selectionStyle = .none
    }
    
    func showShadow() {
        stepperView.layer.shadowColor = UIColor.black.cgColor
        stepperView.layer.shadowOpacity = 0.3
        stepperView.layer.shadowOffset = CGSize(width: 0, height: 2)
        stepperView.layer.shadowRadius = 0.7
        stepperView.layer.cornerRadius = 4.2
    }
    
    @IBAction func addProduct(_ sender: UIButton) {
        delegate?.productCellAddButtonTapped(cell: self)
    }
    
    @IBAction func removeProduct(_ sender: UIButton) {
        delegate?.productCellDeleteButtonTapped(cell: self)
    }
    @IBAction func removeSubCategoryFromCart(_ sender: UIButton) {
        delegate?.productCellRemoveCategoryButtonTapped(cell: self)
    }
    
    @IBAction func favAction(_ sender: Any) {
        delegate?.productCellFavButtonTapped(cell: self, isfavStatus: true)
    }
    
    func updateValue(added: Bool) {
        if added {
            inCartCount += 1
        } else {
            inCartCount -= 1
        }
    }
    
    
    func setupDataForConfirmBooking(data: ProductCellModel) {
        self.brandName.text = data.brand_name
        self.leftProductDescription.text = "\(data.pack_size ?? "") " + "\(data.measuring_unit ?? "")"
        self.deleteButton.isHidden = !showDeleteButton
        
        self.discountImage.isHidden = true
        self.lblDiscount.isHidden = true
        if let productImg = data.productImage {
            let imageUrl = URL.get(from: productImg)
            self.productImage.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "bawiqPlaceholde"))
        } else {
            self.productImage.image = #imageLiteral(resourceName: "bawiqPlaceholde")
        }
        
        self.stepperView.isHidden = true
        
        if let discount = data.discount_percentage {
            if discount > 0 {
                discountImage.isHidden = false
                lblDiscount.isHidden = false
                lblDiscount.text = "\(data.discount_percentage ?? 0)%"
            } else {
                discountImage.isHidden = true
                lblDiscount.isHidden = true
            }
        }
        
        
        self.productName.text = data.productName
        self.productDescription.text = "\(data.pack_size ?? "") " + "\(data.measuring_unit ?? "")"
        if let price = data.productPrice {
            if let count = data.inCartCount {
                self.inCartCount = count
                self.productPrice.text =  "\(count) x " + "AED " + String(format: "%.2f", price)  //String(describing: price)
            } else {
                self.inCartCount = 0
            }
        }
        
        
    }
    func setupDataForSubCategoryProducts(data: ProductCellModel) {
        self.brandName.text = data.brand_name
        self.deleteButton.isHidden = !showDeleteButton
        if let productImg = data.productImage {
            let imageUrl = URL.get(from: productImg)
            self.productImage.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "bawiqPlaceholde"))
        } else {
            self.productImage.image = #imageLiteral(resourceName: "bawiqPlaceholde")
        }
        
        self.productName.text = data.productName
        
        if let discount = data.discount_percentage {
            if discount > 0 {
                discountImage.isHidden = false
                lblDiscount.isHidden = false
                lblDiscount.text = "\(data.discount_percentage ?? 0)%"
            } else {
                discountImage.isHidden = true
                lblDiscount.isHidden = true
            }
        }
        
        if let packSize = data.pack_size {
            self.productDescription.text = "\(packSize) " + "\(data.measuring_unit ?? "")"
        }
        
        self.leftProductDescription.text = "\(data.pack_size ?? "") " + "\(data.measuring_unit ?? "")"
        
        if let price = data.productPrice {
            self.productPrice.text = "AED " + String(format: "%.2f", price) //String(describing: price)
        }
        
        if let count = data.inCartCount, count > 0 {
            self.inCartCount = count
        } else {
            self.inCartCount = 0
        }
        
        if let count = data.is_fav, count == 1 {
            self.favBtn.setImage(#imageLiteral(resourceName: "fav"), for: .normal)
        } else {
            self.favBtn.setImage(#imageLiteral(resourceName: "unfav"), for: .normal)
        }

    }
    func setupData(data: ProductCellModel) {
        self.brandName.text = data.brand_name
        self.deleteButton.isHidden = !showDeleteButton
        if let productImg = data.productImage {
            let imageUrl = URL.get(from: productImg)
            self.productImage.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "bawiqPlaceholde"))
        } else {
            self.productImage.image = #imageLiteral(resourceName: "bawiqPlaceholde")
        }
       
        self.productName.text = data.productName
        
        if let discount = data.discount_percentage {
            if discount > 0 {
                discountImage.isHidden = false
                lblDiscount.isHidden = false
                lblDiscount.text = "\(data.discount_percentage ?? 0)%"
            } else {
                discountImage.isHidden = true
                lblDiscount.isHidden = true
            }
        }
        
        if let packSize = data.pack_size {
            self.productDescription.text = "\(packSize) " + "\(data.measuring_unit ?? "")"
        }
        
        self.leftProductDescription.text = "\(data.pack_size ?? "") " + "\(data.measuring_unit ?? "")"
        
        if let price = data.productPrice {
            if let count = data.inCartCount {
                self.inCartCount = count
                self.productPrice.text =  "\(count) x " + "AED " + String(format: "%.2f", price) //String(describing: price)
            } else {
                self.inCartCount = 0
            }
            
           // self.productPrice.text = "\(data.quantity)" + "AED " + String(describing: price)
        }
        
        if let count = data.inCartCount {
            self.inCartCount = count
        } else {
            self.inCartCount = 0
        }

    }
}



