//
//  AddCardViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 27/02/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit


class AddCardViewController: UIViewController,UITextFieldDelegate,UIWebViewDelegate {

    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var cardDetailsLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var cardNumberLabel: UILabel!
    
    @IBOutlet weak var cardNumberTextField1: UITextField!
    @IBOutlet weak var cardNumberTextField2: UITextField!
    @IBOutlet weak var cardNumberTextField3: UITextField!
    @IBOutlet weak var cardNumberTextField4: UITextField!
    
    @IBOutlet weak var CARDLABEL: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        giveUiAttributes()
        assShadowToView()
        giveFontToTextFields(textField:cardNumberTextField1)
        giveFontToTextFields(textField:cardNumberTextField2)
        giveFontToTextFields(textField:cardNumberTextField3)
        giveFontToTextFields(textField:cardNumberTextField4)
        giveTextToCardNumberLabel()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Singleton.sharedInstance.enableDisableIQKeyboard(enable: false  )
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Singleton.sharedInstance.enableDisableIQKeyboard(enable: true)
    }

    @IBAction func backButtonMenuAction(_ sender: UIButton) {
     let _ = self.navigationController?.popViewController(animated: true)
    }

    func giveUiAttributes(){
        //HEADING LABEL
        let firstAttributedText = NSMutableAttributedString(string: TEXT.ADD_NEW, attributes: [NSFontAttributeName:UIFont(name: FONT.light, size: FONT_SIZE.large)!])
        let secondAttributedText = NSMutableAttributedString(string: " " + TEXT.CARD_TEXT, attributes: [NSFontAttributeName:UIFont(name: FONT.regular, size: FONT_SIZE.large)!])
        firstAttributedText.append(secondAttributedText)
        headingLabel.attributedText = firstAttributedText
        cardDetailsLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.smallest)
        cardDetailsLabel.text = TEXT.CARD_DETAILS.capitalized
        
        cardNumberLabel.text =  TEXT.CARD_NUMBER
        cardNumberLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        cardNumberLabel.textColor = COLOR.ADD_NEW_CARD_HEADING_LABEL
        
        CARDLABEL.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)
        CARDLABEL.textColor = COLOR.ADD_NEW_CARD_HEADING_LABEL
        
    }
    
    func assShadowToView(){
        cardView.layer.shadowColor = UIColor.black.cgColor
        cardView.layer.shadowOpacity = 0.3
        cardView.layer.shadowOffset = CGSize(width: 0, height: 7)
        cardView.layer.shadowRadius = 6
        cardView.layer.cornerRadius = 4.3
    }
    
    func giveFontToTextFields(textField:UITextField){
        textField.font = UIFont(name: FONT.light, size: FONT_SIZE.large)
        textField.textColor = UIColor.black
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2) { 
            self.giveTextToCardNumberLabel()
        }
       
        if (textField.text?.count)! >= 4 && string == ""{
         return true
        }else if (textField.text?.count)! >= 4{
            if let nextTextField = self.view.viewWithTag(textField.tag + 100) as? UITextField{
                if (nextTextField.text?.count)! < 4{
                nextTextField.becomeFirstResponder()
                }else{
                    nextTextField.text = string
                    return false
                }
            }else{
                self.view.endEditing(true)
            }
        }else if textField.text?.count == 1 && string == ""{
            if let previousTextField = self.view.viewWithTag(textField.tag - 100) as? UITextField{
                textField.text = ""
                previousTextField.becomeFirstResponder()
                return false
            }
        }
        return true
    }

    
    func giveTextToCardNumberLabel()  {
        CARDLABEL.text = cardNumberTextField1.text! + String(repeating: "X", count: (4 - (cardNumberTextField1.text?.count)!)) + "   " + cardNumberTextField2.text! + String(repeating: "X", count: (4 - (cardNumberTextField2.text?.count)!)) + "   " + cardNumberTextField3.text! + String(repeating: "X", count: (4 - (cardNumberTextField3.text?.count)!)) + "   "  + cardNumberTextField4.text! + String(repeating: "X", count: (4 - (cardNumberTextField4.text?.count)!))
        
        let url : NSURL = NSURL(string: "http://stackoverflow.com/questions/12416469/how-to-launch-safari-and-open-url-from-ios-app")!
        
        if UIApplication.shared.canOpenURL(url as URL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: { (true) in
                    print("asdasd")
                })
            } else {
               // UIApplication.shared.open
            }
        }
}

    
     
}
