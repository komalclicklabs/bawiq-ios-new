//
//  OtpScreenViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 20/03/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class OtpScreenViewController: UIViewController,UITextFieldDelegate,ErrorDelegate {

    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var changeNumbeButton: UIButton!
    @IBOutlet weak var resendOtpButton: UIButton!
    @IBOutlet weak var otpTextField: UITextField!
    @IBOutlet weak var otpTextField2: UITextField!
    @IBOutlet weak var otpTextfiedl3: UITextField!
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var otpTextfield4: UITextField!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    var phoneNumberView : ChangePhoneNumberView?
    
    var errorMessageView:ErrorView!
    
    func getDeviceToken() -> String {
        if let deviceToken = UserDefaults.standard.value(forKey: USER_DEFAULT.deviceToken) {
            return deviceToken as! String
        }
        return "no device token"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        confirmButton.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
        resendOtpButton.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: UIControlState.normal)
        changeNumbeButton.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: UIControlState.normal)
        setTitleView()
        heading.text = ""
        phoneNumber.font = UIFont(name: FONT.regular, size: FONT_SIZE.medium)
        phoneNumber.text = Singleton.sharedInstance.vendorDetailInfo.phoneNo
        setTextFieldFont(textField: otpTextField)
        setTextFieldFont(textField: otpTextfiedl3)
        setTextFieldFont(textField: otpTextField2)
        setTextFieldFont(textField: otpTextfield4)
        changeNumbeButton.titleLabel?.font = UIFont(name: FONT.light, size: FONT_SIZE.small)
        resendOtpButton.titleLabel?.font = UIFont(name: FONT.light, size: FONT_SIZE.small)
        confirmButton.titleLabel?.font = UIFont(name: FONT.light, size: FONT_SIZE.small)
         assignUiColors()
         phoneNumberView = UINib(nibName: NIB_NAME.ChangePhoneNumberView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ChangePhoneNumberView
        
    }
    
    
    
 
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let nextTextField = self.view.viewWithTag(textField.tag + 1) as? UITextField
            let previousTextField = self.view.viewWithTag(textField.tag - 1) as? UITextField
            print(string)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
                      self.checkIfOtpisComplete()
                })
            guard let text = textField.text else { return true }
            if text.characters.count >= 1{
            if let _ = nextTextField   , string != "" {
            nextTextField!.becomeFirstResponder()
            nextTextField!.text=string
            }
            else if string == "" {
            if let _ = previousTextField {
            textField.text=""
            // let prev = previousTextField?.text!
            previousTextField?.becomeFirstResponder()
            return false
            }
            else {
            return true
            }
        
            }
            else {
            textField.resignFirstResponder()
            }
            }
            let newLength = text.characters.count + string.characters.count - range.length
            return newLength <= 1
    }
    func checkIfOtpisComplete(){
        let otp = (otpTextField.text! + otpTextField2.text! + otpTextfiedl3.text! + otpTextfield4.text!)
        if otp.characters.count == 4{
            self.confirmAction("")
        }
    }
    
    func showErrorMessage(error:String,isError:Bool) {
        if errorMessageView == nil {
            errorMessageView = UINib(nibName: NIB_NAME.errorView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
            errorMessageView.delegate = self
            errorMessageView.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: HEIGHT.errorMessageHeight)
            self.view.addSubview(errorMessageView)
            errorMessageView.setErrorMessage(message: error,isError: isError)
        }
    }

    func removeErrorView() {
        self.errorMessageView.removeFromSuperview()
        self.errorMessageView = nil
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
      // _ = self.navigationController?.popViewController(animated: true)
        Singleton.sharedInstance.logoutButtonAction()
    }

 
    func setTitleView() {
        let titleView = UINib(nibName: NIB_NAME.titleView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TitleView
        titleView.frame = CGRect(x: 0, y: HEIGHT.navigationHeight, width: self.view.frame.width, height: HEIGHT.titleHeight)
        titleView.backgroundColor = UIColor.clear
        self.view.addSubview(titleView)
        titleView.setTitle(title: TEXT.ENTER_OTP.capitalized, boldTitle: "")
    }
    
    func setTextFieldFont(textField:UITextField){
        textField.font = UIFont(name: FONT.regular, size: FONT_SIZE.medium)
    }

    @IBAction func resendOtpAction(_ sender: Any) {
        let param = [
        "access_token":"\(Singleton.sharedInstance.vendorDetailInfo.accessToken)",
        "device_token":"\(getDeviceToken())",
        "app_version":"\(APP_VERSION)",
        "app_device_type":"\(APP_DEVICE_TYPE)",
        "app_access_token":"\(Singleton.sharedInstance.vendorDetailInfo.appAccessToken!)",
        "user_id":"\(Singleton.sharedInstance.formDetailsInfo.user_id!)"
        ]
        print(param)
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.serverCall(apiName: "resend_otp", params: param as [String : AnyObject]?, httpMethod: "POST") { (isSuccess, response) in
         ActivityIndicator.sharedInstance.hideActivityIndicator()
            if isSuccess == true{
            print(response)
                if let message = response["message"] as? String{
                    self.showErrorMessage(error: message, isError: false    )
                }
        }else{
            if let message = response["message"] as? String{
                self.showErrorMessage(error: message, isError: true)
            }
        }
        }
        
        
        
    }

    @IBAction func changeNumberAction(_ sender: Any) {
        
        phoneNumberView?.frame = self.view.frame
        phoneNumberView?.contactField.textField.text = ""
        self.view.addSubview(phoneNumberView!)
        self.view.bringSubview(toFront: phoneNumberView!)
        self.phoneNumberView?.changeNumberAction = {(number) in
            print(number.replacingOccurrences(of: " ", with: ""))
            if Singleton.sharedInstance.validatePhoneNumber(phoneNumber: number.replacingOccurrences(of: " ", with: "")) == true{
                print("success")
                print(number.replacingOccurrences(of: " ", with: ""))
                self.view.endEditing(true)
                self.changePhoneNumber(number: number.replacingOccurrences(of: " ", with: ""))
            }else{
                self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PHONE_NUMBER, isError: true)
            }
        }
        
    }
    

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func confirmAction(_ sender: Any) {
        self.view.endEditing(true)
        if validaateOtp() == true{
        let param  = [
        "otp":"\((otpTextField.text! + otpTextField2.text! + otpTextfiedl3.text! + otpTextfield4.text!))",
        "access_token":"\(Singleton.sharedInstance.vendorDetailInfo.accessToken)",
        "device_token":"\(getDeviceToken())",
        "app_version":"\(APP_VERSION)",
        "app_device_type":"\(APP_DEVICE_TYPE)",
        "app_access_token":"\(Singleton.sharedInstance.vendorDetailInfo.appAccessToken!)",
        "user_id":"\(Singleton.sharedInstance.formDetailsInfo.user_id!)"
        ]
            ActivityIndicator.sharedInstance.showActivityIndicator()
            APIManager.sharedInstance.serverCall(apiName: "verify_otp", params: param as [String : AnyObject]?, httpMethod: "POST") { (isSuccess, response) in
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                print(response)

                if isSuccess == true{
                    if let message = response["message"] as? String{
                        self.showErrorMessage(error: message, isError: false    )
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() , execute: {
                        if Singleton.sharedInstance.checkForPendingAmount(owener: self) == false   {
                            
                            Singleton.sharedInstance.pushToHomeScreen()
                        }
                    })
                    }
                }else{
                    if let message = response["message"] as? String{
                        self.showErrorMessage(error: message, isError: true)
                    }
                }
            }
            
        }
    }
    
    func changePhoneNumber(number:String) {
        let param = [
        "access_token":Singleton.sharedInstance.vendorDetailInfo.accessToken!,
        "new_phone_no":number,
        "phone_no":Singleton.sharedInstance.vendorDetailInfo.phoneNo!.replacingOccurrences(of: " ", with: ""),
        "user_id":Singleton.sharedInstance.vendorDetailInfo.userId!,
        "device_token":APIManager.sharedInstance.getDeviceToken(),
        "app_version":APP_VERSION,
        "app_device_type":APP_DEVICE_TYPE,
        "app_access_token":Singleton.sharedInstance.vendorDetailInfo.appAccessToken!
        ] as [String : Any]
        print(param)
         ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.serverCall(apiName: "change_phone_number", params: param as [String : AnyObject]?, httpMethod: "POST") { (isSuccess, Response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if isSuccess == true{
               self.showErrorMessage(error: Response["message"] as! String, isError: false)
                self.phoneNumber.text = number
                self.phoneNumberView?.removeFromSuperview()
                Singleton.sharedInstance.vendorDetailInfo.phoneNo = number
            }else{
                self.showErrorMessage(error: Response["message"] as! String, isError: true )

            }
        }
    }
    
    
    func validaateOtp() -> Bool{
        let otp = (otpTextfield4.text! + otpTextField.text! + otpTextField2.text! + otpTextfiedl3.text!)
        if otp.characters.count == 4{
            return true
        }else{
            self.showErrorMessage(error: TEXT.otpErrorMessage, isError: true  )
            return false
        }
        return false

    }
    
    func assignUiColors(){
        self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        self.confirmButton.setTitleColor(COLOR.LOGIN_BUTTON_TITLE_COLOR, for: UIControlState.normal)
        otpTextField.textColor = COLOR.SPLASH_TEXT_COLOR
        otpTextfiedl3.textColor = COLOR.SPLASH_TEXT_COLOR
        otpTextField2.textColor = COLOR.SPLASH_TEXT_COLOR
        otpTextfield4.textColor = COLOR.SPLASH_TEXT_COLOR
        heading.textColor = COLOR.SPLASH_TEXT_COLOR
        phoneNumber.textColor = COLOR.SPLASH_TEXT_COLOR
        
        
        
    }
    
    
}

