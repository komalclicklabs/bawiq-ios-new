//
//  BQNotificationData.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 21/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQNotificationData: NSObject {

    var creation_datetime: String?
    var job_id: Int?
    var notification_status: Int?
    var notification_text: String?
    var notification_title: String?
    var user_id: Int?
    
    convenience init(param: [String: Any]) {
        self.init()
        
        if let categoryDescription = param["creation_datetime"] as? String {
            self.creation_datetime = self.utcToLocal(date: categoryDescription)
        }
    
        if let categoryDescription = param["job_id"] as? Int {
            self.job_id = categoryDescription
        }
        
        if let categoryDescription = param["notification_status"] as? Int {
            self.notification_status = categoryDescription
        }
        
        if let categoryDescription = param["notification_text"] as? String {
            self.notification_text = categoryDescription
        }
        if let categoryDescription = param["user_id"] as? Int {
            self.user_id = categoryDescription
        }
        
        if let categoryDescription = param["notification_title"] as? String {
            self.notification_title = categoryDescription
        }
    }
    
    func utcToLocal(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if let dt = dateFormatter.date(from: date) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = "dd MMM yyyy hh:mma" //09:00pm, 28 Sep 2017
            return dateFormatter.string(from: dt)
        }
        return ""
    }
    
    
}
