//
//  TaskReviewVC.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 11/30/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class TaskReviewVC: UIViewController,UITableViewDelegate,UITableViewDataSource,ErrorDelegate {

    @IBOutlet var createTaskButton: UIButton!
    @IBOutlet weak var tasksTable: UITableView!
    @IBOutlet weak var arrowButton: UIImageView!
    
    var errorMessageView:ErrorView!
    var titleView:TitleView!
    let model = TaskReviewModel()
    let defaultCreateButtonWidth:CGFloat = 52.0
     var pulsator : Pulsator! = nil
    var distance = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        arrowButton.image = #imageLiteral(resourceName: "arrow").withRenderingMode(.alwaysTemplate)
        arrowButton.tintColor = UIColor.white
        self.view.backgroundColor = COLOR.REVIEW_BACKGROUND_COLOR
        self.tasksTable.separatorStyle = .none
        self.tasksTable.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 50.0))
        self.tasksTable.register(UINib.init(nibName: NIB_NAME.taskReviewCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.taskReviewCellIdentifier)
        self.setTitleView()
        self.setCreateButton()
        print(Singleton.sharedInstance.createTaskDetail.latitude)
        print(Singleton.sharedInstance.createTaskDetail.longitude)
        print(Singleton.sharedInstance.createTaskDetail.jobPickupLatitude)
        print(Singleton.sharedInstance.createTaskDetail.jobPickupLongitude)
         
       // createTaskButton.inputView = addPulsating()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.arrowButton.superview?.isHidden = true
        let showTutorial =  Singleton.sharedInstance.isForTutorial()
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
                if showTutorial == true{
                    self.arrowButton.superview?.isHidden = false
                    self.arrowButton.superview?.alpha = 0
                    UIView.animate(withDuration: 0.8, animations: {
                        self.arrowButton.superview?.alpha = 1
                    }, completion: { (true) in
                        self.startArrowAnimation()
                    })
                }   else{
                    self.arrowButton.superview?.isHidden = true
                }
            })
    }
    
    
    //MARK:CREATE TASK BUTTON
    func setCreateButton() {
        self.createTaskButton.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
        self.createTaskButton.layer.cornerRadius = defaultCreateButtonWidth / 2
        self.createTaskButton.setShadow()
    }
    
    //MARK:TITLE VIEW
    func setTitleView() {
        titleView = UINib(nibName: NIB_NAME.titleView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TitleView
        titleView.frame = CGRect(x: 0, y: HEIGHT.titleHeight, width: self.view.frame.width, height: HEIGHT.titleHeight)
        titleView.backgroundColor = UIColor.clear
        self.view.addSubview(titleView)
        self.titleView.setTitle(title: TEXT.YOU_HAVE, boldTitle: TEXT.SET)
    }
    
 
    
    
    
    //MARK: UITABLEVIEW DELEGATE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.getNumberOfRowsForTaskReview()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        switch Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag {
        case PICKUP_DELIVERY.both.rawValue?:
            if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
                if indexPath.row == 1 {
                    return 60.0
                }
            } else {
                if indexPath.row == 0 {
                    return 60.0
                }
            }
        case PICKUP_DELIVERY.addDeliveryDetails.rawValue?:
            return model.getHeightForCell(index: indexPath.row) + 20
        default:
            break
        }
        return model.getHeightForCell(index: indexPath.row) + 20
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.taskReviewCellIdentifier) as! TaskReviewCell
        cell.setTaskReviewAsPerLayout(index: indexPath.row)
        cell.editButton.tag = indexPath.row
        cell.editButton.addTarget(self, action: #selector(self.editAction(sender:)), for: UIControlEvents.touchUpInside)
        cell.deleteButton.tag = indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(self.deleteAction(sender:)), for: UIControlEvents.touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag {
        case PICKUP_DELIVERY.both.rawValue?:
            if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
                if indexPath.row == 1 {
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.addDeliveryDetails), object: self)
                    _ = self.navigationController?.popViewController(animated: true)
                }
            } else {
                if indexPath.row == 0 {
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.addPickupDetails), object: self)
                    _ = self.navigationController?.popViewController(animated: true)
                }
            }
            break
        default:
            break
        }
    }
    
    //MARK: BUTTON ACTION
    func editAction(sender:UIButton) {
        switch Singleton.sharedInstance.formDetailsInfo.work_flow {
        case WORKFLOW.pickupDelivery.rawValue?:
            switch Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag {
            case PICKUP_DELIVERY.pickup.rawValue?:
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.editPickupTask), object: self)
                break
            case PICKUP_DELIVERY.delivery.rawValue?:
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.editDeliveryTask), object: self)
                break
            case PICKUP_DELIVERY.both.rawValue?:
                if sender.tag == 0 {
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.editPickupTask), object: self)
                } else {
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.editDeliveryTask), object: self)
                }
                break
            case PICKUP_DELIVERY.addDeliveryDetails.rawValue?:
                if sender.tag == 0 {
                    Singleton.sharedInstance.createTaskDetail.hasPickup = 1
                    Singleton.sharedInstance.createTaskDetail.hasDelivery = 0
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.editPickupTask), object: self)
                } else {
                     Singleton.sharedInstance.createTaskDetail.hasPickup = 0
                     Singleton.sharedInstance.createTaskDetail.hasDelivery = 1
                    NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.editDeliveryTask), object: self)
                }
                break
            default:
                break
            }
            break
        case WORKFLOW.appointment.rawValue?, WORKFLOW.fieldWorkforce.rawValue?:
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.editDeliveryTask), object: self)
            break
        default:
            break
        }
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func deleteAction(sender:UIButton) {
        
        Singleton.sharedInstance.showAlertWithOption(owner: self, title: TEXT.areYouSureYouWantToDelete, message: "", showRight: true, leftButtonAction: { 
            
        
        
        switch Singleton.sharedInstance.formDetailsInfo.work_flow {
        case WORKFLOW.pickupDelivery.rawValue?:
            switch Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag {
            case PICKUP_DELIVERY.addDeliveryDetails.rawValue?:
                Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag = PICKUP_DELIVERY.both.rawValue
                if sender.tag == 0 {
                    Singleton.sharedInstance.createTaskDetail.hasPickup = 0
                    Singleton.sharedInstance.createTaskDetail.hasDelivery = 1
                    Singleton.sharedInstance.createTaskDetail.hidePickupDelivery = false
                    Singleton.sharedInstance.deletePickupDetails()
                } else {
                    Singleton.sharedInstance.createTaskDetail.hasPickup = 1
                    Singleton.sharedInstance.createTaskDetail.hasDelivery = 0
                    Singleton.sharedInstance.createTaskDetail.hidePickupDelivery = false
                    Singleton.sharedInstance.deleteDelivaryDetails()
                }
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.updateModelTaskDetails), object: self)
                self.tasksTable.reloadData()
               _ = self.navigationController?.popViewController(animated: true)
                break
            default:
                
                
                
                Singleton.sharedInstance.deleteAllDetails()
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.updateModelTaskDetails), object: self)
                NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.editPickupTask), object: self)
                
               // if Singleton.sharedInstance.formDetailsInfo.isNLevel {
                    //Singleton.sharedInstance.formDetailsInfo.nLevelController?.deleteTask()
                    
                    for controller in self.navigationController!.viewControllers as Array {
                        
                        if controller is CheckoutViewController  {
                            (controller as! CheckoutViewController).creatTaskManager.setprefilType()
                           (controller as! CheckoutViewController).locationManager.startUpdatingLocation()
                            (controller as! CheckoutViewController).creatTaskManager.setUpPickUpDeliveryWorkFlow()
                            self.navigationController!.popToViewController(controller, animated: true)
                            return
                        }
                    }
                    
                    
                    _ = self.navigationController?.popToRootViewController(animated: true)
                    return
              //  }
                

                
                
                if Singleton.sharedInstance.formDetailsInfo.verticalType == .pdaf {
                    
                _ = self.navigationController?.popViewController(animated: true)
                }else{
                    if Singleton.sharedInstance.servicesTypeCategories.count > 1 && Singleton.sharedInstance.formDetailsInfo.verticalType == .multipleCategory {
                    for controller in self.navigationController!.viewControllers as Array {
                        if controller is LaundyProductsViewController  && Singleton.sharedInstance.formDetailsInfo.verticalType == .multipleCategory {
                            Singleton.sharedInstance.clearQuantityFromCategories()
                            self.navigationController!.popToViewController(controller, animated: true)
                        }
                    }
                    }else{
                        _ = self.navigationController?.popToRootViewController(animated: true)
                    }
                }
                break
            }
            break
        default:
            Singleton.sharedInstance.deleteAllDetails()
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.updateModelTaskDetails), object: self)
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.editPickupTask), object: self)
            for controller in self.navigationController!.viewControllers as Array {
            if controller is CheckoutViewController  {
                (controller as! CheckoutViewController).creatTaskManager.setprefilType()
                (controller as! CheckoutViewController).locationManager.startUpdatingLocation()
                (controller as! CheckoutViewController).creatTaskManager.setUpPickUpDeliveryWorkFlow()
                
            }
            }
            
            _ = self.navigationController?.popViewController(animated: true)
            break
        }
            }, rightButtonAction: nil, leftButtonTitle: TEXT.YES_ACTION, rightButtonTitle: TEXT.NO)
    }

    @IBAction func createTaskAction(_ sender: AnyObject) {
        createTaskButton.isEnabled = false
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) { 
            self.createTaskButton.isEnabled = true
        }
        
        var customField:CustomFieldDetails!
        var pickupCustomFieldTemplate = ""
        var customFieldTemplate = ""
        
        for i in 0..<Singleton.sharedInstance.createTaskDetail.pickupMetadata.count {
            //customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:i)
            customField = Singleton.sharedInstance.createTaskDetail.pickupMetadata[i] as! CustomFieldDetails
            pickupCustomFieldTemplate = customField.template_id!
            switch customField.data_type {
            case CUSTOM_FIELD_DATA_TYPE.image:
                for imageStatus in customField.imageUploadingStatus {
                    guard imageStatus == true else {
                        Singleton.sharedInstance.showAlert(ERROR_MESSAGE.IMAGE_UPLOADING)
                        return
                    }
                }
                break
            default:
                break
            }
        }
        
        
        
        for i in 0..<Singleton.sharedInstance.createTaskDetail.metaData.count {
            //customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:i)
            customField = Singleton.sharedInstance.createTaskDetail.metaData[i] as! CustomFieldDetails
            customFieldTemplate = customField.template_id!
            switch customField.data_type {
            case CUSTOM_FIELD_DATA_TYPE.image:
                for imageStatus in customField.imageUploadingStatus {
                    guard imageStatus == true else {
                        Singleton.sharedInstance.showAlert(ERROR_MESSAGE.IMAGE_UPLOADING)
                        return
                    }
                }
                break
            default:
                break
                
            }
        }
        
        
        if customField != nil {
            
            
        }
        
        
        let param = ["coordinates":
            [["lat" : "\(Singleton.sharedInstance.createTaskDetail.jobPickupLatitude)", "lng" : "\(Singleton.sharedInstance.createTaskDetail.jobPickupLongitude)"],["lat":"\(Singleton.sharedInstance.createTaskDetail.latitude)","lng":"\(Singleton.sharedInstance.createTaskDetail.longitude)"]]]
        //APIManager.sharedInstance.syncApiHit("get_price", params: <#T##[String : Any]#>)
//        let serialQueue = DispatchQueue(label: "queuename")
//        serialQueue.sync {
//            
//        
//            DispatchQueue.main.async {
//                ActivityIndicator.sharedInstance.showActivityIndicator()
//            }
//            APIManager.sharedInstance.serverCall(apiName: "get_price", params: param as [String : AnyObject]?, httpMethod: "POST", receivedResponse: { (isSuccess, response) in
//                DispatchQueue.main.async {
//                    ActivityIndicator.sharedInstance.hideActivityIndicator()
//                }
//                print(response)
//            })
//        }
//
        
        var hasPickup = Singleton.sharedInstance.createTaskDetail.hasPickup
        var hasDelivery = Singleton.sharedInstance.createTaskDetail.hasDelivery
        var pickupMetaData = [Any]()
        var metaData = [Any]()
        
        switch Singleton.sharedInstance.formDetailsInfo.work_flow! {
        case WORKFLOW.appointment.rawValue, WORKFLOW.fieldWorkforce.rawValue:
            Singleton.sharedInstance.createTaskDetail.jobPickupAddress = ""
            Singleton.sharedInstance.createTaskDetail.jobPickupLatitude = ""
            Singleton.sharedInstance.createTaskDetail.jobPickupLongitude = ""
            pickupCustomFieldTemplate = ""
            metaData = model.createMetaData()
            break
        default:
            if Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag == PICKUP_DELIVERY.addDeliveryDetails.rawValue {
                hasPickup = 1
                hasDelivery = 1
                metaData = model.createMetaData()
                pickupMetaData = model.createPickupMetaData()
                
                
                
            }else {//if Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag == PICKUP_DELIVERY.both.rawValue {
                if !((hasPickup == 1 && hasDelivery == 1)) && Singleton.sharedInstance.formDetailsInfo.forcePickupDelivery == 1 && Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag == PICKUP_DELIVERY.both.rawValue{
                    let alert = UIAlertController(title: "", message: TEXT.FORCE_PICKUP_DELIVERY_MESSAGE, preferredStyle: UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction(title: TEXT.OK_TEXT.capitalized, style: UIAlertActionStyle.default, handler: { (action) in
                        if hasDelivery == 0 {
                            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.addDeliveryDetails), object: self)
                            _ = self.navigationController?.popViewController(animated: true)
                        } else if hasPickup == 0 {
                        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.addPickupDetails), object: self)
                        _ = self.navigationController?.popViewController(animated: true)
                        }
                    })
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: nil)
                    return
                } else {
                    
                }
            }
            
            
            break
        }
        
        
        
        
        if Singleton.sharedInstance.formDetailsInfo.work_flow != WORKFLOW.pickupDelivery.rawValue {
            hasPickup = 0
            hasDelivery = 0
        }
        
        if APP_DEVICE_TYPE == allAppDeviceType.nexTask {
            var distanceIndex = 100
            var subTotalIndex = 200
            for i in 0..<pickupMetaData.count{
                if let data = pickupMetaData[i] as? [String:Any]{
                    if let labelValue = data["label"] as? String{
                        if labelValue == "distance"{
                            distanceIndex = i
                        }
                        if labelValue  == "subtotal"{
                         subTotalIndex = i
                        }
                    }
                }
                print(i)
            }
            
            if distanceIndex < pickupMetaData.count - 1 {
            pickupMetaData.remove(at: distanceIndex)
            pickupMetaData.append(["label":"distance","data":distance])
            }
            
            if subTotalIndex < pickupMetaData.count - 1 {
                pickupMetaData.remove(at: subTotalIndex)
                pickupMetaData.append(["label":"subtotal","data":Singleton.sharedInstance.subTotal])
            }
            if  Singleton.sharedInstance.formDetailsInfo.verticalType == .pdaf {
            customFieldTemplate = pickupCustomFieldTemplate
            metaData = pickupMetaData
            }
            
            print(pickupMetaData)
        }else {
            var subTotalIndex = 200
            for i in 0..<pickupMetaData.count{
                if let data = pickupMetaData[i] as? [String:Any]{
                    if let labelValue = data["label"] as? String{
                        if labelValue  == "subtotal"{
                            subTotalIndex = i
                        }
                    }
                }
                print(i)
            }
            
            if subTotalIndex < pickupMetaData.count {
                pickupMetaData.remove(at: subTotalIndex)
                pickupMetaData.append(["label":"subtotal","data":Singleton.sharedInstance.subTotal])
            }
            
            
            
            var subTotalIndexMeta = 200
            for i in 0..<metaData.count{
                if let data = metaData[i] as? [String:Any]{
                    if let labelValue = data["label"] as? String{
                        if labelValue  == "subtotal"{
                            subTotalIndexMeta = i
                        }
                    }
                }
                print(i)
            }
            
            if subTotalIndexMeta < metaData.count {
                metaData.remove(at: subTotalIndexMeta)
                metaData.append(["label":"subtotal","data":Singleton.sharedInstance.subTotal])
            }
            
            
            
        }
        if Singleton.sharedInstance.formDetailsInfo.toShowPayment == false  {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.createTaskHit(autoAssignment: Singleton.sharedInstance.formDetailsInfo.auto_assign!,
                                                customerAddress: Singleton.sharedInstance.createTaskDetail.customerAddress,
                                                customerEmail: Singleton.sharedInstance.createTaskDetail.customerEmail,
                                                customerPhone: Singleton.sharedInstance.createTaskDetail.customerPhone,
                                                customerUsername: Singleton.sharedInstance.createTaskDetail.customerUsername,
                                                latitude: Singleton.sharedInstance.createTaskDetail.latitude,
                                                longitude: Singleton.sharedInstance.createTaskDetail.longitude,
                                                jobDeliveryDatetime: Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime,
                                                hasDelivery: "\(hasDelivery)",
                                                hasPickup: "\(hasPickup)",
                                                jobDescription: Singleton.sharedInstance.createTaskDetail.jobDescription,
                                                jobPickupAddress: Singleton.sharedInstance.createTaskDetail.jobPickupAddress,
                                                jobPickupEmail: Singleton.sharedInstance.createTaskDetail.jobPickupEmail,
                                                jobPickupPhone: Singleton.sharedInstance.createTaskDetail.jobPickupPhone,
                                                jobPickupDateTime: Singleton.sharedInstance.createTaskDetail.jobPickupDateTime,
                                                jobPickupLatitude: Singleton.sharedInstance.createTaskDetail.jobPickupLatitude,
                                                jobPickupLongitude: Singleton.sharedInstance.createTaskDetail.jobPickupLongitude,
                                                jobPickupName: Singleton.sharedInstance.createTaskDetail.jobPickupName,
                                                domainName: Singleton.sharedInstance.formDetailsInfo.domain_name!,
                                                layoutType: Singleton.sharedInstance.formDetailsInfo.work_flow!,
                                                vendorId: Vendor.current!.id!,
                                                metadata:metaData,
                                                pickupMetadata:pickupMetaData,
                                                pickupCustomFieldTemplate:pickupCustomFieldTemplate, customFieldTemplate:customFieldTemplate)
        { (isSuccess, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
            }
            if isSuccess == true {
                self.didRecieveResponseAfterTaskCreation(response:response)
              
            }else{
               self.showErrorMessage(error: response["message"] as! String)
            }
        }
        }else{

            // IN CASE OF PAYMENT
           customeFuncForPricing(completionHandler: { (value, toAdd) in
            if toAdd == true{
            metaData = self.model.createMetaData(forCustom: "price", value: value)
            }
                print(pickupMetaData)
                ActivityIndicator.sharedInstance.showActivityIndicator()
                APIManager.sharedInstance.createTaskHit(url:API_NAME.getPayment,autoAssignment: Singleton.sharedInstance.formDetailsInfo.auto_assign!,
                                                        customerAddress: Singleton.sharedInstance.createTaskDetail.customerAddress,
                                                        customerEmail: Singleton.sharedInstance.createTaskDetail.customerEmail,
                                                        customerPhone: Singleton.sharedInstance.createTaskDetail.customerPhone,
                                                        customerUsername: Singleton.sharedInstance.createTaskDetail.customerUsername,
                                                        latitude: Singleton.sharedInstance.createTaskDetail.latitude,
                                                        longitude: Singleton.sharedInstance.createTaskDetail.longitude,
                                                        jobDeliveryDatetime: Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime,
                                                        hasDelivery: "\(hasDelivery)",
                                                        hasPickup: "\(hasPickup)",
                                                        jobDescription: Singleton.sharedInstance.createTaskDetail.jobDescription,
                                                        jobPickupAddress: Singleton.sharedInstance.createTaskDetail.jobPickupAddress,
                                                        jobPickupEmail: Singleton.sharedInstance.createTaskDetail.jobPickupEmail,
                                                        jobPickupPhone: Singleton.sharedInstance.createTaskDetail.jobPickupPhone,
                                                        jobPickupDateTime: Singleton.sharedInstance.createTaskDetail.jobPickupDateTime,
                                                        jobPickupLatitude: Singleton.sharedInstance.createTaskDetail.jobPickupLatitude,
                                                        jobPickupLongitude: Singleton.sharedInstance.createTaskDetail.jobPickupLongitude,
                                                        jobPickupName: Singleton.sharedInstance.createTaskDetail.jobPickupName,
                                                        domainName: Singleton.sharedInstance.formDetailsInfo.domain_name!,
                                                        layoutType: Singleton.sharedInstance.formDetailsInfo.work_flow!,
                                                        vendorId: Vendor.current!.id!,
                                                        metadata:metaData,
                                                        pickupMetadata:pickupMetaData,
                                                        pickupCustomFieldTemplate:pickupCustomFieldTemplate, customFieldTemplate:customFieldTemplate,withPayment:true,payment_method:"\(Singleton.sharedInstance.formDetailsInfo.payementMethod)")
                { (isSuccess, response) in
                    DispatchQueue.main.async {
                        ActivityIndicator.sharedInstance.hideActivityIndicator()
                    }
                    print(response)
                    if isSuccess == true {
                        //  if let currencyData
                        if let data = response["data"] as? [String:Any]{
                            if let value = data["currency"] as? [String:Any]{
                              guard let currencyValue = Currency(json: value) else {
                                print("ERROR -> Not able to parse currency")
                                return
                              }
                                if let totalCost = data["per_task_cost"] as? String{
                                    var vc : NewPaymentScreenViewController!
                                     vc = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.payment) as! NewPaymentScreenViewController
                                    vc?.amount = "\(currencyValue.symbol) \(totalCost)"
                                    vc?.initialAmmount = "\(totalCost)"
                                    
                                    vc?.afterSelecting = {(id,type,tip) in
                                        ActivityIndicator.sharedInstance.showActivityIndicator()
                                        APIManager.sharedInstance.createTaskHit(autoAssignment: Singleton.sharedInstance.formDetailsInfo.auto_assign!,
                                                                                customerAddress: Singleton.sharedInstance.createTaskDetail.customerAddress,
                                                                                customerEmail: Singleton.sharedInstance.createTaskDetail.customerEmail,
                                                                                customerPhone: Singleton.sharedInstance.createTaskDetail.customerPhone,
                                                                                customerUsername: Singleton.sharedInstance.createTaskDetail.customerUsername,
                                                                                latitude: Singleton.sharedInstance.createTaskDetail.latitude,
                                                                                longitude: Singleton.sharedInstance.createTaskDetail.longitude,
                                                                                jobDeliveryDatetime: Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime,
                                                                                hasDelivery: "\(hasDelivery)",
                                                                                hasPickup: "\(hasPickup)",
                                                                                jobDescription: Singleton.sharedInstance.createTaskDetail.jobDescription,
                                                                                jobPickupAddress: Singleton.sharedInstance.createTaskDetail.jobPickupAddress,
                                                                                jobPickupEmail: Singleton.sharedInstance.createTaskDetail.jobPickupEmail,
                                                                                jobPickupPhone: Singleton.sharedInstance.createTaskDetail.jobPickupPhone,
                                                                                jobPickupDateTime: Singleton.sharedInstance.createTaskDetail.jobPickupDateTime,
                                                                                jobPickupLatitude: Singleton.sharedInstance.createTaskDetail.jobPickupLatitude,
                                                                                jobPickupLongitude: Singleton.sharedInstance.createTaskDetail.jobPickupLongitude,
                                                                                jobPickupName: Singleton.sharedInstance.createTaskDetail.jobPickupName,
                                                                                domainName: Singleton.sharedInstance.formDetailsInfo.domain_name!,
                                                                                layoutType: Singleton.sharedInstance.formDetailsInfo.work_flow!,
                                                                                vendorId: Vendor.current!.id!,
                                                                                metadata:metaData,
                                                                                pickupMetadata:pickupMetaData,
                                                                                pickupCustomFieldTemplate:pickupCustomFieldTemplate, customFieldTemplate:customFieldTemplate,withPayment:true,ammount:"\(totalCost)",currency:currencyValue.id,payment_method:type,card_id:id,tipAmount:tip)
                                        { (isSuccess, response) in
                                            
                                            DispatchQueue.main.async {
                                                ActivityIndicator.sharedInstance.hideActivityIndicator()
                                            }
                                            
                                            print(response)
                                            
                                            if isSuccess == true{
                                                self.didRecieveResponseAfterTaskCreation(response:response)
                                            }else{
                                                _ = self.navigationController?.popViewController(animated: true)
                                                self.showErrorMessage(error: response["message"] as! String)
                                            }
                                            
                                            
                                        }
                                    }
                                    
                                    self.navigationController?.pushViewController(vc!, animated: true)    
                                }
                            }
                        }
                    }else{
                        self.showErrorMessage(error: response["message"] as! String)
                    }
                }
            })
            
        }
    }
    
    
    
    
    
    
    func customeFuncForPricing(completionHandler:@escaping ((_ value:String,_ toAdd:Bool)->Void))  {
        
        if APP_DEVICE_TYPE == "17" && Singleton.sharedInstance.formDetailsInfo.toShowPayment == true{
        let param = ["coordinates":
            [["lat" : "\(Singleton.sharedInstance.createTaskDetail.jobPickupLatitude)", "lng" : "\(Singleton.sharedInstance.createTaskDetail.jobPickupLongitude)"],["lat":"\(Singleton.sharedInstance.createTaskDetail.latitude)","lng":"\(Singleton.sharedInstance.createTaskDetail.longitude)"]]]
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.serverCall(baseURl: "",apiName: "get_price", params: param as [String : AnyObject]?, httpMethod: "POST") { (isSuccess, response) in
            print(response)
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if let data = response["data"] as? [String:Any]{
                if let value = data["price"] as? Int{
                   
                    completionHandler("\(value)", true)
                }
            }
            
        }
        }else{
         completionHandler("", false)
        }
    }
    
    
    
    //MARK: ERROR MESSAGE
    func showErrorMessage(error:String) {
        if errorMessageView == nil {
            errorMessageView = UINib(nibName: NIB_NAME.errorView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
            errorMessageView.delegate = self
            errorMessageView.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: HEIGHT.errorMessageHeight)
            self.view.addSubview(errorMessageView)
            errorMessageView.setErrorMessage(message: error,isError: true)
        }
    }
    
    //MARK: ERROR DELEGATE METHOD
    func removeErrorView() {
        self.errorMessageView = nil
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Arrow Animation For First time Tutorial
    func startArrowAnimation() {
        UIView.animate(withDuration: 0.8, delay: 0,
                       options: [.repeat, .autoreverse, .curveEaseOut],
                       animations: {
                        self.arrowButton.transform = CGAffineTransform(translationX: -22, y: 0)
            }, completion: nil)
    }
    
    func didRecieveResponseAfterTaskCreation(response:[String:Any]){
        // Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
        Singleton.sharedInstance.createTaskDetail = CreateTaskDetails(json: [:])
        Singleton.sharedInstance.resetMetaData()
        Singleton.sharedInstance.resetPickupMetaData()
        
        if Singleton.sharedInstance.formDetailsInfo.work_flow == WORKFLOW.pickupDelivery.rawValue {
            switch Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag {
            case PICKUP_DELIVERY.pickup.rawValue?:
                Singleton.sharedInstance.createTaskDetail.hasDelivery = 0
                Singleton.sharedInstance.createTaskDetail.hasPickup = 1
                break
            case PICKUP_DELIVERY.delivery.rawValue?:
                Singleton.sharedInstance.createTaskDetail.hasDelivery = 1
                Singleton.sharedInstance.createTaskDetail.hasPickup = 0
                break
            case PICKUP_DELIVERY.both.rawValue?:
                Singleton.sharedInstance.createTaskDetail.hasDelivery = 0
                Singleton.sharedInstance.createTaskDetail.hasPickup = 1
                break
            case PICKUP_DELIVERY.addDeliveryDetails.rawValue?:
                Singleton.sharedInstance.createTaskDetail.hasDelivery = 0
                Singleton.sharedInstance.createTaskDetail.hasPickup = 1
                Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag = PICKUP_DELIVERY.both.rawValue
                Singleton.sharedInstance.createTaskDetail.hidePickupDelivery = false
                break
            default:
                break
            }
        } else {
            Singleton.sharedInstance.createTaskDetail.hasDelivery = 1
            Singleton.sharedInstance.createTaskDetail.hasPickup = 0
        }
        let showTutorial =  Singleton.sharedInstance.isForTutorial()
        if showTutorial == false    {
            
            if let message = response["message"] as? String{
                 ErrorView.showWith(message: message, isErrorMessage: false, removed: nil)
            }
            
           
            
            NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.refreshHomeScreenAfterCreateTask), object: nil, userInfo: response)
            // NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: NOTIFICATION_OBSERVER.refreshHomeScreenAfterCreateTask), object: self)
         
         if Singleton.sharedInstance.formDetailsInfo.isNLevel {
            Singleton.sharedInstance.formDetailsInfo.nLevelController?.deleteTask()
            _ = self.navigationController?.popToRootViewController(animated: true)
            return
         }
         
            if Singleton.sharedInstance.servicesTypeCategories.count > 1{
            for controller in self.navigationController!.viewControllers as Array {
               
               
               
                if controller is CheckoutViewController  && Singleton.sharedInstance.formDetailsInfo.verticalType == .pdaf {
                  
                    self.navigationController!.popToViewController(controller, animated: true)
                    return
                }else if controller is LaundyProductsViewController  && Singleton.sharedInstance.formDetailsInfo.verticalType == .multipleCategory {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }else if controller is TaxiHomeScreenViewController  && Singleton.sharedInstance.formDetailsInfo.verticalType == .taxi {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }else{
                  //  _ = self.navigationController?.1(animated: true)
                }
            }
            }else{
              _ =  self.navigationController?.popToRootViewController(animated: true)
            }
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.getInTouch) as? GetInTouchController
            self.navigationController?.pushViewController(vc!, animated: true   )
        }
    }
    
    
    func stopArrowAnimation() {
        self.arrowButton.transform = CGAffineTransform.identity
        self.arrowButton.layer.removeAllAnimations()
    }
    
}
