//
//  BillBreakUpTableViewCell.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 16/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class BillBreakUpTableViewCell: UITableViewCell {

    
    @IBOutlet weak var valeuName: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var middleCollen: UILabel!
    @IBOutlet weak var upperHorizontalLine: UILabel!
    @IBOutlet weak var lowerHorizontalLine: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        valeuName.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle.customizeSize())
        valeuName.textColor = COLOR.SPLASH_TEXT_COLOR
        
        valueLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle.customizeSize())
        valueLabel.textColor  = COLOR.SPLASH_TEXT_COLOR
        
        upperHorizontalLine.text = ""
        lowerHorizontalLine.text = ""
        upperHorizontalLine.backgroundColor = COLOR.FILLED_IMAGE_COLOR.withAlphaComponent(0.2)
        lowerHorizontalLine.backgroundColor = COLOR.FILLED_IMAGE_COLOR.withAlphaComponent(0.2)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
