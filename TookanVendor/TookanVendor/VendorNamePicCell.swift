//
//  VendorNamePicCell.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 11/29/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
import Kingfisher

//MARK: - PROTOCOL TO UPDATE DATA IN CONTROLLER
protocol VendorNameImageChanged {
    func nameChanged(yourText:String)
    //func imageChanged(yourImage:UIImage)
    func editButtonTapped()
}

class VendorNamePicCell: UITableViewCell,UITextFieldDelegate {
   
   @IBOutlet var vendorImageView: UIImageView!
   @IBOutlet weak var editProfileButton: UIButton!
   var isEditting = false
   @IBOutlet weak var imagePickerButton: UIButton!
   var delegate: VendorNameImageChanged!
   @IBOutlet weak var cameraButton: UIButton!
   
   override func awakeFromNib() {
      super.awakeFromNib()
      
      
      cameraButton.backgroundColor = COLOR.ACTIVE_IMAGE_COLOR
      cameraButton.isHidden = !isEditting
      cameraButton.setCornerRadius(radius: 18)
      
      // imagePickerButton.setCornerRadius(radius: 43)
      // imagePickerButton.setBorder(borderColor: .darkGray)
      
    
      
      editProfileButton.isHidden = true
   }
   
   //MARK: - TEXTFIELD DELEGATE
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      return true
   }
   
   func textFieldDidEndEditing(_ textField: UITextField) {
      self.delegate.nameChanged(yourText: textField.text!)
   }
   
   //MARK: - UDPATE CELL IF EDITTING
   func editButtonAction() {
      isEditting = true
      cameraButton.isHidden = !isEditting
      delegate.editButtonTapped()
      
   }
   
   //MARK: - SETTING CELL WITH FIELD TYPE
   func setCellInfo(fieldType:FIELD_TYPE,row:Int,detailModel:ProfileDetails){
      cameraButton.isHidden = !isEditting
      print(detailModel.fullName)
    vendorImageView.setCornerRadius(radius: 75)
   
    vendorImageView.clipsToBounds = true
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3) {
         print("vendor image height \(self.vendorImageView.frame.height)")
        self.vendorImageView.setCornerRadius(radius: 75)
        self.layoutIfNeeded()
    }
      if detailModel.image != nil && isEditting == true {
         // self.imagePickerButton.setImage(detailModel.image, for: UIControlState.normal)
         self.vendorImageView.image = detailModel.image
      } else if detailModel.imageString.length > 0 {
         let imageUrl = URL(string: detailModel.imageString)
         self.vendorImageView.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "iconDefaultAvatar"))
      }else {
        self.vendorImageView.image = #imageLiteral(resourceName: "profilePlaceholder")
        
    }
   }
   
   override func setSelected(_ selected: Bool, animated: Bool) {
      super.setSelected(selected, animated: animated)
      // Configure the view for the selected state
   }
   
}
