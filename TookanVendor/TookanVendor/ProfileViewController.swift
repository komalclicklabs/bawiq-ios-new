//
//  ProfileViewController.swift
//  TookanVendor
//
//  Created by Vishal on 31/01/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController,ErrorDelegate, CountryPhoneCodePickerDelegate {
    
    func removeErrorView() {
        self.errorMessageView = nil
    }
    
    @IBOutlet weak var nationality: MKTextField!
    @IBOutlet weak var dobField: MKTextField!
    @IBOutlet weak var gennder: MKTextField!
    
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var name: MKTextField!
    @IBOutlet weak var email: MKTextField!
    @IBOutlet weak var phone: VSTextField!
    
    var pickOption = ["Male", "Female", "Other"]
    var navigationBar : NavigationView!
    var imageData: Data?
    var errorMessageView:ErrorView!
    var keyboardSize:CGSize = CGSize(width: 0.0, height: 0.0)
    
    @IBOutlet weak var editImagebutton: ImagePickerButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTextFields()
        setNavigationBar()
        
        shoWProfileImage(on: picture)
        
        editImagebutton.imageCallBack = { [weak self] (result) in
            switch result {
            case .success(let filePath):
                self?.setImage(path: filePath)
                break
            case .error(let error): break
                break
            }
        }
        
        // Do any additional setup after loading the view.
        
    }
    
    func shoWProfileImage(on image: UIImageView) {
        if let url = Vendor.current?.vendorImage {
            let imageUrl = URL.get(from: url)
            image.kf.setImage(with: imageUrl, placeholder: placeholdeImage)
        }
    }
    
    func configureTextFields() {
        self.gennder.placeHolderColor = .white
        self.dobField.placeHolderColor = .white
        self.nationality.placeHolderColor = .white
        
        phone.delegate = self
        name.text = Vendor.current?.username ?? ""
        email.text = Vendor.current?.email ?? ""
        gennder.text = Vendor.current?.gender ?? ""
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        gennder.inputView = pickerView
        
        dobField.text = Vendor.current?.dob ?? ""
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.maximumDate = Date()
        dobField.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(ProfileViewController.datePickerValueChanged), for: UIControlEvents.valueChanged)
        
        nationality.text = Vendor.current?.nationality ?? ""
        let countryPicker = CountryPicker()
        countryPicker.countryPhoneCodeDelegate = self
        nationality.inputView = countryPicker
        if let vendor = Vendor.current, vendor.phoneNo.count < 10 {
            phone.setFormatting("XX-XXX-XXXX", replacementChar: "X")
            phone.text = Vendor.current?.phoneNo.applyPatternOnNumbers(pattern: "##-###-####", replacmentCharacter: "#")
        } else {
            phone.setFormatting("XXX-XXX-XXXX", replacementChar: "X")
            phone.text = Vendor.current?.phoneNo.applyPatternOnNumbers(pattern: "###-###-####", replacmentCharacter: "#")
        }
        
            //Vendor.current?.phoneNo ?? ""
        
        name.placeHolderColor = .white
        email.placeHolderColor = .white
        //phone.placeHolderColor = .white
    
        phone.attributedPlaceholder = NSAttributedString(string: "Phone Number e.g. (052-993-3645)",
                                                                        attributes: [NSForegroundColorAttributeName: UIColor.white])
        
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        
        dateFormatter.timeStyle = DateFormatter.Style.none
        
        dobField.text = dateFormatter.string(from: sender.date)
        
    }
    
    func countryPhoneCodePicker(picker: CountryPicker, didSelectCountryCountryWithName name: String, countryCode: String, phoneCode: String) {
        self.nationality.text = name
    }
    
    //MARK: NAVIGATION BAR
    func setNavigationBar() {
        navigationBar = NavigationView.getNibFile(withHeight: 80,params:NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: TEXT.EditProfile, leftButtonImage: #imageLiteral(resourceName: "BackButtonWhite"), rightButtonImage: nil) , leftButtonAction: {
            _ = self.navigationController?.popViewController(animated: true)
        }, rightButtonAction: nil)
        navigationBar.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
    }
    
    func backAction() {
        self.navigationController?.popViewController(animated: true)
    }

    func setImage(path: String) {
        if let image = UIImage(contentsOfFile: path) {
            picture.image = image
            self.imageData = UIImageJPEGRepresentation(image, 0.5)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func updateProfileAction(_ sender: UIButton) {
        checkValidation()
    }
    
    func phoneNumberContainsCharacterOtherThanNumbers(phone: String) -> Bool {
        
      
        
        return phone.count != phone.stripOutUnwantedCharacters(charactersYouWant: "0123456789+-").count
    }
    
    func checkValidation() {
        
        var email = ""
        var name = ""
        var phone = self.phone.text
        
        if let _name = self.name.text {
            name = _name
        }
        
        if let _email = self.email.text?.trimText {
            email = _email
        }
        guard name.length > 0 else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_NAME, isError: true)
            return
        }
        
        if Singleton.sharedInstance.validateEmail(email) == false {
            email = ""
//            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_EMAIL, isError: true)
//            return
        }
        
        if let _phone = phone {
            if (!Singleton.sharedInstance.validatePhoneNumber(phoneNumber: _phone)) || (phoneNumberContainsCharacterOtherThanNumbers(phone: _phone) == true) {
//                self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PHONE_NUMBER, isError: true)
                phone = ""
            }
        } else {
            phone = ""
        }
        
        if phone == "" && email == "" {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PHONE_Email, isError: true)
            return
        }
        
        phone = phone?.replacingOccurrences(of: "-", with: "")
        
        var params = ["app_access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
                          "email": email,
//                          "phone_no": phone,
        "username": name] as [String: Any]
        if phone?.first == "0" {
            params["phone_no"] = phone?.dropFirst()
        } else {
            params["phone_no"] = phone
        }
        
        
        if self.gennder.text != "" {
            params["gender"] = gennder.text
        }
        
        if self.dobField.text != "" {
            params["dob"] = dobField.text
        }
        
        if self.nationality.text != "" {
            params["nationality"] = nationality.text
        }
        
        
        ActivityIndicator.sharedInstance.showActivityIndicator()
        
        
        
        APIManager.sharedInstance.editProfile(apiName: API_NAME.vendorUpdate, params: params as [String : AnyObject], httpMethod: HTTP_METHOD.POST, isImage: imageData != nil ? true : false, vendorImage: picture.image, imageKey: "vendor_image", isNeedToShowActivityIndictor: true) { (isSuccess, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSuccess {
                    if let data = response["data"] as? [String:AnyObject] {
                        Vendor.logInWith(data: data)
                        if Vendor.current!.isAccountVerified == "0" {
                            let storyboard = UIStoryboard(name: STORYBOARD_NAME.main, bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: STORYBOARD_ID.otpScreen) as? OtpScreenViewController
                            vc?.isComingUpdateProfile = true
                            self.navigationController?.pushViewController(vc!, animated: true)
                        } else {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }else{
                    self.showErrorMessage(error: response["message"] as? String ?? "", isError: true )
                }
            }
            
        }
    }
    //MARK: ERROR MESSAGE
    func showErrorMessage(error:String,isError:Bool) {
        if errorMessageView == nil {
            errorMessageView = UINib(nibName: NIB_NAME.errorView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
            errorMessageView.delegate = self
            errorMessageView.frame = CGRect(x: 0, y: self.view.frame.height - keyboardSize.height, width: self.view.frame.width, height: HEIGHT.errorMessageHeight)
            self.view.addSubview(errorMessageView)
            errorMessageView.setErrorMessage(message: error,isError: isError)
        }
    }
}

extension ProfileViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case phone:
            phone.setFormatting("XXX-XXX-XXXX", replacementChar: "X")
            if textField.text == "" {
                //            if string == "0" {
                //                phoneField.text = string
                //            } else {
                //                phoneField.text = "0" + string
                //            }
                phone.text = "0" + string
                return false
            }
            return true
        default:
            break
        }
        return true
    }
}

extension ProfileViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickOption[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        gennder.text = pickOption[row]
    }
    
}

