//
//  NLevelParentViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 14/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

protocol NLevelFlowDelegate: class {
   func nextButtonPressed(forElement element: LevelElement, atIndexInChildArray index: Int)
   func addQuantityButtonPressed(forElement element: LevelElement)
   func subtractQuantityButtonPressed(forElement element: LevelElement)
   func popViewController()
   func checkoutButtonPressed()
   func getQuantityOfProductWith(id: String) -> Int
   func getChildrenOf(element: LevelElement, inCellWithTag tag: Int, completion: ((Bool, Level?) -> Void)?)
}

class NLevelParentViewController: UIViewController {

    @IBOutlet weak var minimumValueLabelBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var minimumOrderLabel: UILabel!
    // MARK: - Properties
   private var titleString = ""
   private var imageUrlString = ""
   fileprivate var level: [LevelElement]!
   fileprivate var childrenLevelsOfLevel: [Level]? {
      didSet {
         cartButtonview.isHidden = !shouldCartBeVisible()
      }
   }
    let productFilterScreen = ProductFilterScreen.instanceProductFilterScreenFromNib()
    
   private var layoutType: LevelLayout!
    var selectedSortType = sortType.alphabetically
    var menuNavigationBar:MenuNavigationView?
    var childReloadClosure : (()->Void)?
   var cart: NLevelCart?
   var flowManager: NLevelNavigationHandler?
   
   var navBar: JugnooHeaderForCatalogue?
   var childviewController: UIViewController?
   
   var isNavBarHidden = false
   var isBackButtonHidden = false
   private var neverShowCartButton = false
   
   var numberOfElementsInCart = 0 {
      didSet {
         numberOfElementsInCartChanged()
      }
   }
   
   // MARK: - IBOutlets
   @IBOutlet weak var cartButton: UIButton!
   @IBOutlet weak var cartButtonview: UIView!
   @IBOutlet weak var cartCount: UILabel!
    @IBOutlet weak var cartPriceLabel: UILabel!
   
   // MARK: - View Life Cycle
    override func viewDidLoad() {
      super.viewDidLoad()
      setUiAttributsForMinOder()
      cartButtonview.backgroundColor = COLOR.THEME_FOREGROUND_COLOR

      setSortView()
      cartButtonview.isHidden = !shouldCartBeVisible()
      cartButtonview.setShadow()
      
      let vc = getViewControllerToSetAsAChild()
      setViewControllerAs(childViewController: vc, shouldSetFrame: layoutType.shouldSetFrame())
      
      view.bringSubview(toFront: cartButtonview)
      view.bringSubview(toFront: self.minimumOrderLabel)
      
      if !isNavBarHidden {
         setNavigationBar()
      }
    }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      numberOfElementsInCart = cart?.getCartCount() ?? 0
      UIApplication.shared.statusBarStyle = .default
      CustomLocationManager.shared.updateCoordinates(forced: false)
   }
   
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      
      navBar?.setFrame()
      
      if layoutType.shouldSetFrame() && childviewController != nil {
         setFrameOf(viewController: childviewController!)
      }
   }
   
   // MARK: - IBAction
   @IBAction func checkoutButtonPressed() {
      cart?.checkoutButtonPressed()
   }
   
   // MARK: - Methods
   fileprivate func numberOfElementsInCartChanged() {
    
    cartCount.font = UIFont(name: FONT.regular, size: FONT_SIZE.priceFontSize)
    cartCount.textColor = COLOR.LOGIN_BUTTON_TITLE_COLOR
    cartPriceLabel.text =  Singleton.sharedInstance.formDetailsInfo.currencyid + "\(NLevelFlowManager.subTotal)"
    cartPriceLabel.letterSpacing = 0.5
    cartPriceLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.priceFontSize)
    cartPriceLabel.textColor = COLOR.LOGIN_BUTTON_TITLE_COLOR
    cartCount.text =  "Checkout (\(numberOfElementsInCart.description) \(numberOfElementsInCart == 1 ? "Item" : "items"))"     //numberOfElementsInCart.description
    cartPriceLabel.layer.cornerRadius = 5
    cartPriceLabel.clipsToBounds = true
    cartCount.letterSpacing = 0.5
      cartCount.isHidden = numberOfElementsInCart == 0
      cartButtonview.isHidden = !shouldCartBeVisible()
    self.minimumOrderLabel.isHidden = !toShowMinimumValueLable()
   }
   
   fileprivate func shouldCartBeVisible() -> Bool {
      if neverShowCartButton {
         return false
      }
    if numberOfElementsInCart > 0 {
     minimumValueLabelBottomConstraint.constant = 50.0
     self.view.layoutIfNeeded()
     return true
    }else{
        minimumValueLabelBottomConstraint.constant = 0
        self.view.layoutIfNeeded()
        return false
    }
    
   }
    
    func toShowMinimumValueLable() -> Bool{
        return NLevelFlowManager.subTotal < Singleton.sharedInstance.formDetailsInfo.minimumOrderValue
           
    }
   
    private func setNavigationBar() {
        
        if menuHeaderType == 0 {
            navBar = JugnooHeaderForCatalogue.getNibFile(leftButtonAction: { [weak self] in
                self?.flowManager?.fallBackToPreviousLevelWith(currentLevel: self!.level.first!.level)
            })
            navBar?.rightButtonAction = {[weak self](action) in
                self?.showSortView()
            }
            navBar?.setUpImage(imageUrlString: self.imageUrlString)
            navBar?.backButton.isHidden = isBackButtonHidden
            navBar?.headingLabel.text = titleString
            view.addSubview(navBar!)
            
        } else {
            menuNavigationBar = MenuNavigationView.getNibFile(leftButtonAction: { [weak self] in
                self?.flowManager?.fallBackToPreviousLevelWith(currentLevel: self!.level.first!.level)
            })
            menuNavigationBar?.filterButton.isHidden = true
            menuNavigationBar?.searchButton.isHidden = true
            menuNavigationBar?.menuButton.isHidden = isBackButtonHidden
            menuNavigationBar?.titleLabel.text = titleString
            view.addSubview(menuNavigationBar!)
        }
    }
   
   private func getViewControllerToSetAsAChild() -> UIViewController {
      var viewControllerToSetAsChild: UIViewController
      
      switch layoutType! {
      case .banner:
         viewControllerToSetAsChild = BannerTableViewController.getWith(level: level, delegate: self)
      
      case .menu:
        viewControllerToSetAsChild = MenuViewController.getWith(level: level, delegate: self)
        if menuHeaderType == 0 {
            let height = view.frame.height - HEIGHT.jugnooHeaderHeight + 20 - heightOfCheckoutButton()
            viewControllerToSetAsChild.view.frame = CGRect(x: 0, y: HEIGHT.jugnooHeaderHeight - 20, width: SCREEN_SIZE.width, height: height)
        } else {
            let height = view.frame.height - HEIGHT.menuNavigationHeight + 20 - heightOfCheckoutButton()
            viewControllerToSetAsChild.view.frame = CGRect(x: 0, y: HEIGHT.menuNavigationHeight, width: SCREEN_SIZE.width, height: height)
        }
        viewControllerToSetAsChild.view.roundCorners(cornersToRound: [.topRight, .topLeft])
      case .list:
         viewControllerToSetAsChild = ListTableViewController.getWith(level: level, delegate: self)
      case .map:
         neverShowCartButton = true
         viewControllerToSetAsChild = UIViewController()
      }
      
      childviewController = viewControllerToSetAsChild
      return viewControllerToSetAsChild
      
   }
   
   private func setViewControllerAs(childViewController: UIViewController, shouldSetFrame: Bool = true) {
      addChildViewController(childViewController)
      
      if shouldSetFrame {
         setFrameOf(viewController: childViewController)
      }
      
      view.addSubview(childViewController.view)
      childViewController.didMove(toParentViewController: self)
   }
   
    private func setFrameOf(viewController: UIViewController) {
      
      let bottomInset = self.view.safeAreaInsetsForAllOS.bottom
        if menuHeaderType == 0 {
            let height = view.frame.height - HEIGHT.jugnooHeaderHeight + 20 - heightOfCheckoutButton() - bottomInset
            viewController.view.frame = CGRect(x: 0, y: HEIGHT.jugnooHeaderHeight - 20, width: SCREEN_SIZE.width, height: height)
        } else {
            let height = view.frame.height - HEIGHT.menuNavigationHeight + 20 - heightOfCheckoutButton() - bottomInset
            viewController.view.frame = CGRect(x: 0, y: HEIGHT.menuNavigationHeight - 20, width: SCREEN_SIZE.width, height: height)
        }
    }
   
   private func heightOfCheckoutButton() -> CGFloat {
      return shouldCartBeVisible() ? 75 : 25
   }
   

   
   // MARK: - Navigation
    class func getWith(layoutType: LevelLayout, level: Level, title: String,imageUrlString:String) -> NLevelParentViewController {
      let parentVC = findIn(storyboard: .nLevel, withIdentifier: "NLevelParentViewController") as! NLevelParentViewController
      parentVC.layoutType = layoutType
      parentVC.level = level
      parentVC.titleString = title
      parentVC.imageUrlString = imageUrlString
      return parentVC
   }
    
    func showSortView(){
        let animationDuration = 0.4
        if productFilterScreen.superview == nil {
            productFilterScreen.registerNib()
            
            productFilterScreen.sortScreenIsFrom = SortScreenOption.Menus.rawValue
            productFilterScreen.alpha = 0
            productFilterScreen.mainBackgroundView.backgroundColor = UIColor.clear
            productFilterScreen.heightOfBackgroundTableView.constant = 0
            productFilterScreen.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: SCREEN_SIZE.height)
            productFilterScreen.layoutIfNeeded()
            self.view.addSubview(productFilterScreen)
            //            self.insertSubview(productFilterScreen, belowSubview: sender)
            productFilterScreen.heightOfBackgroundTableView.constant = 150.0
            UIView.animate(withDuration: animationDuration, animations: {
                //self.layoutIfNeeded()
                // self.filterButton.alpha = 0
                self.productFilterScreen.alpha = 1
                self.productFilterScreen.mainBackgroundView.backgroundColor = UIColor.black.withAlphaComponent(0.6)
                self.productFilterScreen.layoutIfNeeded()
            }, completion: { (success) in
                self.productFilterScreen.tableViewOfFilter.reloadData()
            })
            
        }else {
            productFilterScreen.heightOfBackgroundTableView.constant = 0
            UIView.animate(withDuration: animationDuration, animations: {
                //self.filterButton.alpha = 1
                //self.layoutIfNeeded()
                self.productFilterScreen.alpha = 0
                self.productFilterScreen.mainBackgroundView.backgroundColor = UIColor.clear
                self.productFilterScreen.layoutIfNeeded()
            }, completion: { (success) in
                self.productFilterScreen.removeFromSuperview()
       })
        }
    }
    
    
   
   deinit {
      print("NLevel parent deintialized")
   }
    
    func setSortView(){
        self.productFilterScreen.onSortCellClick = {[weak self](sort) in
            print(sort)
            self?.sortProductsWith(type:sort)
            self?.selectedSortType = sort
            print("sortType")
        }
    }
    
    func sortProductsWith(type:sortType){
        if self.level.count > 0 && level[0] is Product{
            sortTask(forType: type, array: self.level! as! [Product])
        }else if self.childrenLevelsOfLevel != nil && self.childrenLevelsOfLevel!.count > 0   {
            for (num,i) in self.childrenLevelsOfLevel!.enumerated(){
                if i.count > 0 && i[0] is Product{
                    childrenLevelsOfLevel![num] = sortTask(forType: type, array: i as! [Product])
                }
            }
        }
        childReloadClosure == nil ? print("nil"): childReloadClosure!()
    }
    
    func sortTask(forType:sortType,array:[Product]) -> [Product]{
        var newArray = [Product]()
        
        switch forType {
        case .alphabetically:
            newArray = array.sorted{  $0.name.localizedCaseInsensitiveCompare($1.name) == ComparisonResult.orderedAscending }
        case .priceHigh:
            newArray = array.sorted{  $0.price > $1.price }
        default:
            newArray = array.sorted{  $0.price < $1.price }
            print("")
        }
        return newArray
    }
    
    func pushCustomizeArray(productObj:Product,completionAction : ((_ itemQunatity:Int)->Void)?){
        let customizationVC = UIStoryboard(name: STORYBOARD_NAME.nLevel, bundle: frameworkBundle).instantiateViewController(withIdentifier: "CustomizationViewController") as! CustomizationViewController
        
        customizationVC.menuDetailCustomizeObjectArray = productObj.custoMizableArray
        customizationVC.onAddButtonClick = completionAction
        customizationVC.selectedVarietyItemPrice = productObj.price
        customizationVC.productElement = productObj
        self.navigationController?.pushViewController(customizationVC, animated: true)
    }
    
    func setUiAttributsForMinOder(){
        minimumOrderLabel.backgroundColor = UIColor.darkGray
        minimumOrderLabel.textColor = UIColor.white
        minimumOrderLabel.font = UIFont(name: FONT.light, size: 12.2)
        minimumOrderLabel.text = "Minimum order amount \(Singleton.sharedInstance.formDetailsInfo.currencyid)\(Singleton.sharedInstance.formDetailsInfo.minimumOrderValue)"
    }
    
}

// MARK: - NLevelFlowDelegate
extension NLevelParentViewController: NLevelFlowDelegate {
    func popViewController() {
        flowManager?.fallBackToPreviousLevelWith(currentLevel: level.first!.level)
    }

   func getChildrenOf(element: LevelElement, inCellWithTag tag: Int, completion: ((Bool, Level?) -> Void)?) {
    
    
        navBar?.leftButton.isHidden = true
    
    
      guard let arrayOfChildLevels = childrenLevelsOfLevel else {
         childrenLevelsOfLevel = [Level].init(repeating: [], count: level.count)
         getChildrenOf(element: element, inCellWithTag: tag, completion: completion)
         return
      }
      
      let index = tag.getRowAndSection().row
      let childLevel = arrayOfChildLevels[index]
      
      guard childLevel.count > 0 else {
         flowManager?.getNonDummyChildrenOf(element: element, atChildIndex: index, showActivityIndicator: false) { [weak self] (success, levelElements) in
            if success {
                print("in child")
                if levelElements!.count > 0 && levelElements![0] is Product{
                    print("out child")
//                    self?.childrenLevelsOfLevel?[index] = (self?.sortTask(forType: (self?.selectedSortType)!, array: levelElements as! [Product]))!
//                    self?.childReloadClosure == nil ? print("nil"): self?.childReloadClosure!()
                    self?.childrenLevelsOfLevel?[index] = levelElements!
                    self?.childReloadClosure == nil ? print("nil"): self?.childReloadClosure!()
                }else{
                    self?.childrenLevelsOfLevel?[index] = levelElements!
                }
            }
            completion?(success, levelElements)
         }
         return
      }
      
      completion?(true, childLevel)
   }

   func getQuantityOfProductWith(id: String) -> Int {
      return cart!.getQuantityOfProductWith(id: id)
   }

   func addQuantityButtonPressed(forElement element: LevelElement) {
    
      if element is Product && (element as! Product).custoMizableArray.count > 0{
        pushCustomizeArray(productObj: element as! Product, completionAction: { [weak self] (itemCount) in
            print(itemCount)
            for _ in 0..<itemCount{
            self?.cart?.quantityAddedOf(element: element)
            }
            self?.numberOfElementsInCart = self?.cart?.getCartCount() ?? 0
        })
      }else{
        cart?.quantityAddedOf(element: element)
        numberOfElementsInCart = cart?.getCartCount() ?? 0
    }
    
   }
   
   func subtractQuantityButtonPressed(forElement element: LevelElement) {
    if element is Product {
        if (element as! Product).canRemoveWithoutCart() == false {
            ErrorView.showWith(message: "Not here! Please remove your items individually in the cart on the checkout screen.", removed: nil)
            return
        }
    }
    if element is Product{
    cart?.quantitySubtractedOf(element: element,custString:(element as! Product).getFirstValueOfCustomDictionary())
      numberOfElementsInCart = cart?.getCartCount() ?? 0
    }
   }
   
   func nextButtonPressed(forElement element: LevelElement, atIndexInChildArray index: Int) {
      guard !(element is Product) else {
         print("ERROR -> Cannot go further after product level")
         return
      }
      flowManager?.pushNextControllerWithChildrenOf(element: element, atChildIndex: index)
   }
}
