//
//  Catalogue.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 13/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

enum LevelLayout: Int {
   case list = 1
   case banner = 2
   case map = 3
   case menu = 4
    
    func shouldSetFrame() -> Bool {
        switch self {
        case .map:
            return false
        default:
            return true
        }
    }
}

enum CatalaogueError: LocalizedError {
   case levelIsEmpty
   var errorDescription: String? {
      return ERROR_MESSAGE.catalougeFetchingFailure
   }
}

public class Catalogue {
   
   // MARK: - Properties
   private var formId = ""
   private var levels = [Level]()
   
   private var numberOfLevels: Int {
      return levels.count
   }
   
   /// 1. [Int: [Int]] in this dictionary, 'key' denotes index of parent in parent level and [Int] denotes it's children in lower level, [Int] value of 'Int' in this will be index of children in their level. '[Int]' is also referred as child array in code.
   
   /// 2. At first level(first index of array) will be empty
   
   /// 3. At last level(last index of array) every value corrensponding to key will contain empty array as they will not have any children.
   private var levelsParentChildRelationTree = [[Int: [Int]]]()
   
   /// 'Key' of dictionary is 'Id' of products parent
   /// 'value' of dictionary is 'index' of parent in level array
   private var productParentsHashMap = [String: Int]()
   
   
   // MARK: - Intializer
   init(json: [[[String: Any]]]) throws {
      
      for (index, rawLevel) in json.enumerated() {
         guard rawLevel.count > 0 else {
            throw CatalaogueError.levelIsEmpty
         }
         let (level, hashMap) = LevelElement.getLevelFrom(json: rawLevel, levelIndex: index)
         levels.append(level)
         levelsParentChildRelationTree.append(hashMap)
      }
   }
   
   // MARK: - Methods
   func getChildrenOfElementAt(level: Int, withIndex parentIndex: Int, showActivityIndicator: Bool = true, completion: @escaping (Bool, [LevelElement]?) -> Void) {
      
      let levelOfChildren = level + 1
      
      guard !isAskingForProductLevel(level: levelOfChildren) else {
         if levels.count > 0 {
            
            let parent = levels[level][parentIndex]
            
            fetchProductWith(parentWithId: parent.id, atLevel: levelOfChildren, showActivityIndicator: showActivityIndicator, withParentIndex: parentIndex) { (success, products) in
               completion(success, products)
               return
            }
            return
         }
         fetchProductWith(parentWithId: nil, atLevel: level, showActivityIndicator: showActivityIndicator, withParentIndex: parentIndex) { (success, products) in
            completion(success, products)
            return
         }
         return
      }
      
      guard doChildrenExistOf(parentAtIndex: parentIndex, atLevel: level) else {
         completion(false, nil)
         print("Children do not exist")
         return
      }
      
      guard !isZerothLevel(level: levelOfChildren) else {
         completion(true, elementsAtZerothLevel())
         return
      }
      
      let indicesOfChildren = getIndicesOfChildrenOfParent(atIndex: parentIndex, inLevel: level)
      let elements = getElementsAt(level: levelOfChildren, withIndices: indicesOfChildren)
      completion(true, elements)
   }
   
   private func isAskingForProductLevel(level: Int) -> Bool {
      return numberOfLevels <= level
   }
   
   private func doChildrenExistOf(parentAtIndex parentIndex: Int, atLevel levelIndex: Int) -> Bool {
      let levelOfChildren = levelIndex + 1
      
      if isZerothLevel(level: levelOfChildren) && haveElementsAtZerothLevel() {
         return true
      }
      
      let hashMapOfChildLevel = levelsParentChildRelationTree[levelOfChildren]
    
      guard let indicesOfChildren = hashMapOfChildLevel[parentIndex], indicesOfChildren.count > 0 else {
         print("Error -> Children not found of element at index \(parentIndex) of level \(levelIndex)")
         return false
      }
      
      return true
   }
   
   private func haveElementsAtZerothLevel() -> Bool {
      return levels.count > 0 && levels[0].count > 0
   }
   
   private func isZerothLevel(level: Int) -> Bool {
      return level <= 0
   }
   
   private func elementsAtZerothLevel() -> Level? {
      return levels.first
   }
   
   private func getIndicesOfChildrenOfParent(atIndex: Int, inLevel levelIndex: Int) -> [Int] {
      let levelOfChildren = levelIndex + 1
      let hashMapOfChildLevel = levelsParentChildRelationTree[levelOfChildren]
      let indicesOfChildren = hashMapOfChildLevel[atIndex]
      return indicesOfChildren!
   }
   
   private func getElementsAt(level: Int, withIndices: [Int]) -> [LevelElement] {
      let levelOfWhichElementsAreRequired = levels[level]
      
      var elements = [LevelElement]()
      for index in withIndices {
         elements.append(levelOfWhichElementsAreRequired[index])
      }
      return elements
   }
   
   private func fetchProductWith(parentWithId parentID: String?, atLevel level: Int, showActivityIndicator: Bool, withParentIndex parentIndex: Int, completion: @escaping (Bool, [LevelElement]?) -> Void) {
      
      if parentID != nil {
         productParentsHashMap[parentID!] = parentIndex
      }

      Product.fetchProductsFromServerOf(parentWithid: parentID, andFormId: formId, atLevel: level, showActivityIndicator: showActivityIndicator) { (success, products) in
         guard success else {
            completion(false, nil)
            return
         }
         let productsAsLevelElements = products!.map {$0 as LevelElement}
         completion(success, productsAsLevelElements)
      }
   }
   
   func getTitleOf(level: Int, withParentIndex parentIndex: Int, parentId: String?) -> String {
      let parentLevel = level - 1
      
      let formName = Vendor.current == nil ? "Smoke & Grill" : Vendor.current?.isDemoUser() == true ? "Smoke & Grill" : StoreFrontManager.shared.companyName//"Smoke & Grill"
      if isZerothLevel(level: level) {
         return formName
      }
      

      if isAskingForProductLevel(level: level) && productParentsHashMap.count > 0 {
         let element = getParentOfProductWith(parentId: parentId, productLevel: level)!
         return element.name
      }
      
      let categoryName = levels[parentLevel][parentIndex].name
      
      if categoryName.isEmpty {
         return formName
      }
      return categoryName
   }
   
    
    
    func getImageOf(level: Int, withParentIndex parentIndex: Int, parentId: String?) -> String {
        let parentLevel = level - 1
        
        let formName = Singleton.sharedInstance.formDetailsInfo.logo
        if isZerothLevel(level: level) {
            if formName != nil {
            return formName!
            }else{
                return ""
            }
        }
        
        
        if isAskingForProductLevel(level: level) && productParentsHashMap.count > 0 {
            let element = getParentOfProductWith(parentId: parentId, productLevel: level)!
            return element.imageUrl != nil ? "\(element.imageUrl!)" : ""
        }
        
        let categoryName = levels[parentLevel][parentIndex].imageUrl != nil ? "\(levels[parentLevel][parentIndex].imageUrl!)" : ""
        
        if categoryName.isEmpty {
            return formName!
        }
        return categoryName
    }
    
    
    
    
   func getIndexOfElementInLevelWhosePositionInChildArrayIs(position: Int, level: Int, parentIndex: Int) -> Int {
      
      return levelsParentChildRelationTree[level][parentIndex]![position]
   }
   
   func getParentOfProductWith(parentId: String?, productLevel: Int) -> LevelElement? {
      guard let unwrappedId = parentId else {
         return nil
      }
      
      let indexOfparent = productParentsHashMap[unwrappedId]!
      let parentLevel = productLevel - 1
      
      return levels[parentLevel][indexOfparent]
   }
   
    static func getCatalogueOfFormWithDemo(formId: String, showActivityIndicator: Bool = true, showAlert: Bool = true,completion: ((Bool, Catalogue?, Error?) -> Void)?) {
        
        let params = getParamsToGetCatalogeLevels(formID: formId)
        
        HTTPClient.makeConcurrentConnectionWith(method: .POST, showAlertInDefaultCase: showAlert, showActivityIndicator: showActivityIndicator , para: params, extendedUrl: API_NAME.v2_getAppCatalogue) { (responseObject, error, _, statusCode) in
            
            parsingToGetAppCatalogue(responseObject: responseObject, error: error, formId: formId, statusCode: statusCode, completion: { (isSuccess, catalogue, error) in
                completion?(isSuccess, catalogue, error)
            })
            
        }
    }
   
   // MARK: - Server Hit
  public static func getCatalogueOfFormWith(formId: String, showActivityIndicator: Bool = true, showAlert: Bool = true,completion: ((Bool, Catalogue?, Error?) -> Void)?) {
      
      let params = getParamsToGetCatalogeLevels(formID: formId)
    
    
        print("getCatalogueOfFormWith")
    
    
    
    
      HTTPClient.makeConcurrentConnectionWith(method: .POST, showAlertInDefaultCase: showAlert, showActivityIndicator: showActivityIndicator , para: params, extendedUrl: API_NAME.getAppCatalogue) { (responseObject, error, _, statusCode) in
        parsingToGetAppCatalogue(responseObject: responseObject, error: error, formId: formId, statusCode: statusCode, completion: { (isSuccess, catalogue, error) in
            completion?(isSuccess, catalogue, error)
        })
      }
   }
   
    
    private class func parsingToGetAppCatalogue(responseObject: Any?, error: Error?,formId: String, statusCode: Int?, completion: ((Bool, Catalogue?, Error?) -> Void)?) {
        
        if statusCode == .some(STATUS_CODES.SHOW_DATA),
            let response = responseObject as? [String: Any],
            let rawLevels = response["data"] as? [[[String: Any]]] {
            do {
                let catalogue = try Catalogue.init(json: rawLevels)
                catalogue.formId = formId
                completion?(true, catalogue, error)
            }
            catch let error {
                completion?(false, nil, error)
            }
            
        } else {
            guard statusCode != STATUS_CODES.INVALID_ACCESS_TOKEN else {
                //error automatiacally handled
                return
            }
            completion?(false, nil, error)
        }
        
        
    }
    
    
   private static func getParamsToGetCatalogeLevels(formID: String) -> [String: Any] {
    
    var param = [String:Any]()
    
    param["api_key"] = Singleton.sharedInstance.formDetailsInfo.apiKey
    //param["reference_id"] = Merchant.shared!.vedorRefferenceId
    param["form_id"] = Singleton.sharedInstance.formDetailsInfo.form_id
    param["user_id"] = Singleton.sharedInstance.formDetailsInfo.user_id
    if Vendor.current == nil {
    param["app_device_type"] = originalDeviceType
    }else{
    param["app_device_type"] = APP_DEVICE_TYPE
    }
    
    print(param)
      return param
   }

}
