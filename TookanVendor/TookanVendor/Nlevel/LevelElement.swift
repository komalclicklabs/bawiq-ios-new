//
//  Level.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 14/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

typealias Level = [LevelElement]

enum ActionType: Int {
   case next = 3
   case singleAddition = 1
   case multipleAddition = 2
   case nextButButtonHidden = 4
   
   init(rawLayout: [String: Any]) {
      guard let rawButtons = rawLayout["buttons"] as? [[String: Any]],
         rawButtons.count > 0,
         let rawType = rawButtons.first?["type"] as? Int,
         let type = ActionType.init(rawValue: rawType) else {
            self = ActionType.singleAddition
            return
      }
      self = type
   }
}

class LevelElement {
   // MARK: - Properties
    var id = ""
   private(set) var parentId: String?
   
   var name = ""
   private(set) var description = ""
   private(set) var lines = Lines(rawLayout: [:])
   private(set) var imageUrl: URL?
   private(set) var imageSize: Int?
   private(set) var isDummy = false
   private(set) var parentIndexInPreviousLevel = 0
   private(set) var level = 0
   private(set) var childLayoutType = LevelLayout.list
   private(set) var layoutType = LevelLayout.list 
   
   var actionType = ActionType.nextButButtonHidden
   
   // MARK: - Intializer
   init?(json: [String: Any], level: Int) {
    
      guard let id = findIdIn(json: json), !id.isEmpty else {
         print("Catalogue/Product id not found")
         return nil
      }
      self.id = id
    
      
      self.name = (json["name"] as? String) ?? ""
      self.isDummy = (json["is_dummy"] as? Bool) ?? false
      self.parentIndexInPreviousLevel = (json["parent_index"] as? Int) ?? 0
      self.level = level
      self.description = (json["description"] as? String) ?? ""
      
      if let tempParentId = json["parent_category_id"] {
         self.parentId = "\(tempParentId)"
      }
      
      if let rawLayout = json["layout_data"] as? [String: Any] {
         self.lines = Lines(rawLayout: rawLayout)
         self.actionType = ActionType(rawLayout: rawLayout)
         
         if let rawImagesArray = rawLayout["images"] as? [[String: Any]],
            rawImagesArray.count > 0,
            let imageUrlString = rawImagesArray.first?["data"] as? String
            {
            self.imageUrl = URL.get(from: imageUrlString)
         }
        if let rawImagesArray = rawLayout["images"] as? [[String: Any]],
            rawImagesArray.count > 0,let imageSize = rawImagesArray.first?["size"] as? Int {
            self.imageSize = imageSize
        }
      }
      
      if let currentLayOutTypeRawValue = json["layout_type"] as? Int,
         let currentLayoutType = LevelLayout(rawValue: currentLayOutTypeRawValue) {
         self.layoutType = currentLayoutType
      }
      
      if let childLayoutTypeRawValue = json["child_layout_type"] as? Int, let childLayoutType =  LevelLayout(rawValue: childLayoutTypeRawValue) {
         self.childLayoutType = childLayoutType
      }
      
   }
   
   private func findIdIn(json: [String: Any]) -> String? {
      if let catlougeId = json["catalogue_id"] {
         return "\(catlougeId)"
      }
      
      if let productId = json["product_id"] {
         return "\(productId)"
      }
      
      return nil
   }
   
   // MARK: - Type Methods
   static func getLevelFrom(json: [[String: Any]], levelIndex: Int) -> (level: Level, hashingDict: [Int: [Int]]) {
      var level = [LevelElement]()
      var hashingDict = [Int: [Int]]()
      
      for (index, rawLevel) in json.enumerated() {
         guard let element = LevelElement(json: rawLevel, level: levelIndex) else {
            continue
         }
         level.append(element)
         
         let parentIndex = element.parentIndexInPreviousLevel
            
            if var childIndexes = hashingDict[parentIndex] {
               childIndexes.append(index)
               hashingDict[parentIndex] = childIndexes
            } else {
               hashingDict[parentIndex] = [index]
            }
      }
      return (level, hashingDict)
   }

}
