//
//  ListTableViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 13/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

protocol ActionButtonDelegate:class {
   func actionButtonPressed(type:ButtonPressedType, tag: Int)
}


class ListTableViewController: UITableViewController ,ActionButtonDelegate{
   
   // MARK: - Properties
   var level: [LevelElement]!
   private weak var delegate: NLevelFlowDelegate?
   
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      self.tableView.register(UINib(nibName: NIB_NAME.listCell, bundle: frameworkBundle), forCellReuseIdentifier: ListTableViewCell.identifier)
      
      tableView.tableFooterView = UIView()
      tableView.estimatedRowHeight = 110
      tableView.rowHeight = UITableViewAutomaticDimension
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      tableView.reloadData()
   }
   // MARK: - Table view data source
   override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return level.count
   }
   
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: ListTableViewCell.identifier, for: indexPath) as! ListTableViewCell
      
      let element = level[indexPath.row]
      var quantity = 0
      
      if element is Product {
         cell.selectionStyle = .none
      }
      
      if let product = element as? Product {
         quantity = delegate!.getQuantityOfProductWith(id: product.id)
         product.quantity = quantity
        if quantity > 0 {
            cell.isQuantityMoreThanZero = true
        }else{
            cell.isQuantityMoreThanZero = false
        }
      }
    cell.setCellWith(imageUrl: element.imageUrl, description: element.lines.getAttributedStringDescriptionForList(), actionType: element.actionType, quantity: quantity, imageSize: element.imageSize!)
 //     cell.setCellWith(imageUrl: element.imageUrl, description: element.lines.getAttributedStringDescription(), actionType: element.actionType, quantity: quantity)
      cell.delegate = self
      cell.tag = indexPath.generateTag()
      
      return cell
   }
   
   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
      let levelElement = level[indexPath.row]
      
      tableView.deselectRow(at: indexPath, animated: true)
      
      guard levelElement.actionType == .next || levelElement.actionType == .nextButButtonHidden else {
         return
      }
      
      delegate?.nextButtonPressed(forElement: levelElement, atIndexInChildArray: indexPath.row)
   }
   
   // MARK: - Navigation
   class func getWith(level: [LevelElement], delegate: NLevelFlowDelegate) -> ListTableViewController {
      let listVC = findIn(storyboard: .nLevel, withIdentifier: STORYBOARD_ID.listVC) as! ListTableViewController
      listVC.level = level
      listVC.delegate = delegate
      return listVC
   }
   
   // MARK: - ActionButtonDelegate
   func actionButtonPressed(type:ButtonPressedType, tag: Int) {

      let indexRow = tag.getRowAndSection().row
      let element = level[indexRow]
      switch type {
      case .Add:
         delegate?.addQuantityButtonPressed(forElement: element)
//        self.reloadRowAt(index: indexRow)
      case .Subtract:
         delegate?.subtractQuantityButtonPressed(forElement: element)
//        self.reloadRowAt(index: indexRow)
      case .Next:
         delegate?.nextButtonPressed(forElement: element, atIndexInChildArray: indexRow)
      }
//    let contentOffset = tableView.contentOffset
//    tableView.reloadData()
//    tableView.layoutIfNeeded()
//    tableView.setContentOffset(contentOffset, animated: false)
    UIView.transition(with: tableView, duration: 0.2, options: .transitionCrossDissolve, animations: {self.reloadRowAt(index: indexRow)}, completion: nil)

    
   }
   
   // MARK: - Methods
   private func reloadRowAt(index: Int) {
      let indexPath = IndexPath(row: index, section: 0)
      DispatchQueue.main.async { [weak self] in
         self?.tableView.reloadRows(at: [indexPath], with: .none)
      }
   }
}
