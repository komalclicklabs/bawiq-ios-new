//
//  ListTableViewCell.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 13/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
//import Kingfisher 

class ListTableViewCell: UITableViewCell {
   
    let defaultLeading:CGFloat = 82.0
   static let identifier = "ListTableViewCell"
   @IBOutlet weak var leadingConstraintOfDescription: NSLayoutConstraint!
    @IBOutlet weak var headingTitleRightConstraint: NSLayoutConstraint!

   @IBOutlet weak var trailingFromLeft: NSLayoutConstraint!
   @IBOutlet weak var blackOverlayOnImage: UIImageView!
   @IBOutlet weak var quatityLabel: UILabel!
   @IBOutlet weak var icon: UIImageView!
   @IBOutlet weak var descriptionLabel: UILabel!
   @IBOutlet weak var headingLabel: UILabel!
    
   weak var delegate : ActionButtonDelegate?
   var actionView2: NLevelActionView?
   var isQuantityMoreThanZero = true
   var actionTypeTemp = ActionType.singleAddition
   
   
    func setCellWith(imageUrl: URL?, description: [NSAttributedString], actionType: ActionType, quantity: Int,imageSize : Int = 1) {
    if  imageSize == 4 {
        icon.isHidden = true
        self.leadingConstraintOfDescription.constant = 15.0
    } else {
        icon.isHidden = false
        self.leadingConstraintOfDescription.constant = defaultLeading
        icon.kf.setImage(with: imageUrl, placeholder: placeholdeImage)
    }
     self.actionTypeTemp = actionType
      if actionView2 == nil {
         actionView2 = UINib(nibName: NIB_NAME.NLevelActionView, bundle: frameworkBundle).instantiate(withOwner: self, options: nil)[0] as? NLevelActionView
         actionView2?.translatesAutoresizingMaskIntoConstraints = false
         self.addSubview(actionView2!)
         addActionViewToCenterConstraint()
      }
      setNeedsLayout()
      layoutIfNeeded()
      
      let widthOfActionView = NLevelActionView.getWidthFor(action: actionType)
     
        trailingFromLeft.constant = 5
            headingTitleRightConstraint.constant = widthOfActionView + 3
            actionView2?.frame = CGRect(x: (actionView2?.frame.origin.x)!, y: (actionView2?.frame.origin.y)!, width: widthOfActionView, height: (actionView2?.frame.height)!)
        self.layoutIfNeeded()
      actionView2?.setupActionView(withType: actionType,isZero: quantity == 0) { [weak self](type) in
         self?.delegate?.actionButtonPressed(type: type, tag: self?.tag ?? 0)
      }
        actionView2?.setCountLabel(count: quantity)
      descriptionLabel.attributedText = description[1]
        headingLabel.attributedText = description[0]
   }
   
   private func handleQuantity(_ quantity: Int) {
      quatityLabel.isHidden = quantity == 0
      blackOverlayOnImage.isHidden = quantity == 0
      quatityLabel.text = quantity.description
   }
   
   private func addActionViewToCenterConstraint() {
      guard let actionView = actionView2 else {
         return
      }
      var centerConstraint = 0
      let widthOfActionView: CGFloat = NLevelActionView.getWidthFor(action: actionTypeTemp)
    print(widthOfActionView)
      let trailingSpace = NSLayoutConstraint(item: actionView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailingMargin, multiplier: 1, constant: 5)
    var centerVertically = NSLayoutConstraint()
    if actionTypeTemp == .multipleAddition || actionTypeTemp == .singleAddition {
         centerConstraint = 15
        centerVertically =  NSLayoutConstraint(item: actionView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: CGFloat(centerConstraint))
    }else{
         centerConstraint = 0
        centerVertically =  NSLayoutConstraint(item: actionView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
    }
    
    
//        if isQuantityMoreThanZero == true  {
            let width = NSLayoutConstraint(item: actionView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: widthOfActionView)
            
            self.addConstraints([trailingSpace, centerVertically, width])
//        }else{
//            let width = NSLayoutConstraint(item: actionView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: widthOfActionView-30)
//
//            self.addConstraints([trailingSpace, centerVertically, width])
//        }
    
    
    
      
   }
   
   
}
