//
//  BannerTableViewCell.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 13/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class BannerTableViewCell: UITableViewCell {
   
   static let identifier = "BannerTableViewCell"

   // MARK: - Properties
   weak var delegate : ActionButtonDelegate?
   var actionView: NLevelActionView?
   private var actionTypeTemp = ActionType.nextButButtonHidden
   
   // MARK: - IBOutlets
   @IBOutlet weak var blackOverlayOnImage: UIImageView!
   @IBOutlet weak var quatityLabel: UILabel!
   @IBOutlet weak var icon: UIImageView!
   @IBOutlet weak var descriptionLabel: UILabel!
   @IBOutlet weak var labelTrailingSpace: NSLayoutConstraint!
   
   override func layoutSubviews() {
      super.layoutSubviews()
      
      let widthOfActionView = NLevelActionView.getWidthFor(action: actionTypeTemp)

      actionView?.frame = CGRect(x: SCREEN_SIZE.width - (10+widthOfActionView), y: descriptionLabel.center.y, width: widthOfActionView, height: 50)
      actionView?.center.y = descriptionLabel.center.y
   }

   
   func setCellWith(imageUrl: URL?, description: NSAttributedString, actionType: ActionType, quantity: Int) {
      
      icon.kf.setImage(with: imageUrl, placeholder: placeholdeImage)

      
      let widthOfActionView = NLevelActionView.getWidthFor(action: actionType)
      self.actionTypeTemp = actionType
      
      if actionView == nil {
         actionView = UINib(nibName: NIB_NAME.NLevelActionView, bundle: frameworkBundle).instantiate(withOwner: self, options: nil)[0] as? NLevelActionView
         actionView?.frame = CGRect(x: SCREEN_SIZE.width - (10+widthOfActionView), y: 50, width: widthOfActionView, height: 50)
         self.addSubview(actionView!)
      }
      
      labelTrailingSpace.constant = widthOfActionView + 20
      
      actionView?.setupActionView(withType: actionType,isZero: quantity == 0) { [weak self](type) in
         print(type)
         self?.delegate?.actionButtonPressed(type: type, tag: self?.tag ?? 0)
      }
      
      self.bringSubview(toFront: actionView!)
      
      descriptionLabel.attributedText = description
      handleQuantity(quantity)
   }
   
   private func handleQuantity(_ quantity: Int) {
      quatityLabel.isHidden = quantity == 0
      blackOverlayOnImage.isHidden = quantity == 0
      
      quatityLabel.text = quantity.description
   }

}
