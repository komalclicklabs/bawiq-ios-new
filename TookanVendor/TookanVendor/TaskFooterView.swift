//
//  TaskFooterView.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 30/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

protocol FooterDelegate {
    func firstButtonAction()
    func secondButtonAction()
}

class TaskFooterView: UIView {

    @IBOutlet var createTaskButton: UIButton!
    @IBOutlet var reviewButton: UIButton!
    @IBOutlet var firstButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet var secondButtonWidthConstraint: NSLayoutConstraint!
    
    var delegate:FooterDelegate!
    let height:CGFloat = 50.0
    
    override func awakeFromNib() {
        /*----------- create task button ---------------*/
        self.createTaskButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        self.createTaskButton.setShadow()
        self.createTaskButton.layer.cornerRadius = height / 2
        
        /*----------- Review Button ---------------*/
        self.reviewButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        self.reviewButton.setShadow()
        self.reviewButton.layer.cornerRadius = height / 2
        
        self.setButtonsAsPerConfig()
    }
    
    func setButtonsAsPerConfig() {
        self.setResetAndSaveReviewButtons()
//        switch Singleton.sharedInstance.formDetailsInfo.work_flow {
//        case WORKFLOW.pickupDelivery.rawValue?:
//            switch Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag {
//            case PICKUP_DELIVERY.pickup.rawValue?:
//                self.setResetAndSaveReviewButtons()
//                break
//            case PICKUP_DELIVERY.delivery.rawValue?:
//                self.setResetAndSaveReviewButtons()
//                break
//            case PICKUP_DELIVERY.both.rawValue?:
//                self.setResetAndSaveReviewButtons()
//                break
//            case PICKUP_DELIVERY.addDeliveryDetails.rawValue?:
//                self.setBothAddDetailsAndReviewButtons()
//                self.setSkipDeliveryTaskDetails()
//                break
//            default:
//                break
//            }
//            break
//        case WORKFLOW.appointment.rawValue?:
//            self.setCreateTaskButton()
//            break
//        case WORKFLOW.fieldWorkforce.rawValue?:
//            self.setCreateTaskButton()
//            break
//        default:
//            break
//        }
    }

    func setCreateTaskButton() {
        self.reviewButton.setTitleColor(COLOR.TITLE_POSITIVE_COLOR, for: UIControlState.normal)
        self.reviewButton.backgroundColor = COLOR.BUTTON_BACKGROUND_POSITIVE_COLOR
        self.reviewButton.setImage(nil, for: UIControlState.normal)
        self.reviewButton.setTitle(TEXT.SAVE_REVIEW, for: UIControlState.normal)
        self.reviewButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.large)
        self.reviewButton.isHidden = false
        self.firstButtonWidthConstraint.constant = -self.reviewButton.frame.width
        self.secondButtonWidthConstraint.constant = (SCREEN_SIZE.width / 2) - 30
        self.setNeedsUpdateConstraints()

        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
            self.createTaskButton.isHidden = true
        }
    }
    
    func setBothAddDetailsAndReviewButtons() {
        /*----------------- Constraint -----------------*/
        self.firstButtonWidthConstraint.constant = (SCREEN_SIZE.width * 0.1) - 60
        self.secondButtonWidthConstraint.constant = (SCREEN_SIZE.width * 0.1) - 60
        self.createTaskButton.isHidden = false
        
        /*------------- Add Details ------------*/
        self.createTaskButton.setTitleColor(COLOR.TITLE_NEGATIVE_COLOR, for: UIControlState.normal)
        self.createTaskButton.backgroundColor = COLOR.BUTTON_BACKGROUND_NEGATIVE_COLOR
        self.createTaskButton.setImage(#imageLiteral(resourceName: "iconAddPlus").withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        self.createTaskButton.tintColor = COLOR.TITLE_NEGATIVE_COLOR
        self.createTaskButton.setTitle(TEXT.ADD_DELIVERY_DETAILS, for: UIControlState.normal)
        self.createTaskButton.titleLabel?.numberOfLines = 2
        self.createTaskButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        self.createTaskButton.layer.borderColor = COLOR.TITLE_NEGATIVE_COLOR.cgColor
        self.createTaskButton.layer.borderWidth = 1.5
        
        /*---------------- Save and Review -----------------*/
        self.reviewButton.setTitleColor(COLOR.TITLE_POSITIVE_COLOR, for: UIControlState.normal)
        self.reviewButton.backgroundColor = COLOR.BUTTON_BACKGROUND_POSITIVE_COLOR
        self.reviewButton.setImage(#imageLiteral(resourceName: "iconSaveTick").withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        self.reviewButton.tintColor = COLOR.TITLE_POSITIVE_COLOR
        self.reviewButton.setTitle(TEXT.SAVE_REVIEW, for: UIControlState.normal)
        //self.reviewButton.setTitle(TEXT.SAVE_REVIEW, for: UIControlState.normal)
        self.reviewButton.titleLabel?.numberOfLines = 2
        self.reviewButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        
        self.setNeedsUpdateConstraints()
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
    }
    
    func setSkipDeliveryTaskDetails() {
        self.createTaskButton.setImage(#imageLiteral(resourceName: "iconSkipDetails").withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        self.createTaskButton.tintColor = COLOR.TITLE_NEGATIVE_COLOR
        self.createTaskButton.setTitle(TEXT.SKIP_DELIVERY_DETAILS, for: UIControlState.normal)
    }
    
    func setResetAndSaveReviewButtons() {
        /*----------------- Constraint -----------------*/
        self.firstButtonWidthConstraint.constant = (SCREEN_SIZE.width * 0.1) - 60
        self.secondButtonWidthConstraint.constant = (SCREEN_SIZE.width * 0.1) - 60
        self.createTaskButton.isHidden = false
        
        /*------------- Add Details ------------*/
        self.createTaskButton.setTitleColor(COLOR.TITLE_NEGATIVE_COLOR, for: UIControlState.normal)
        self.createTaskButton.backgroundColor = COLOR.BUTTON_BACKGROUND_NEGATIVE_COLOR
        self.createTaskButton.setImage(#imageLiteral(resourceName: "iconReset").withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        self.createTaskButton.tintColor = COLOR.TITLE_NEGATIVE_COLOR
        self.createTaskButton.setTitle(TEXT.RESET, for: UIControlState.normal)
        self.createTaskButton.titleLabel?.numberOfLines = 2
        self.createTaskButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        self.createTaskButton.layer.borderColor = COLOR.TITLE_NEGATIVE_COLOR.cgColor
        self.createTaskButton.layer.borderWidth = 1.5
        
        /*---------------- Save and Review -----------------*/
        self.reviewButton.setTitleColor(COLOR.TITLE_POSITIVE_COLOR, for: UIControlState.normal)
        self.reviewButton.backgroundColor = COLOR.BUTTON_BACKGROUND_POSITIVE_COLOR
        self.reviewButton.setImage(#imageLiteral(resourceName: "iconSaveTick").withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        self.reviewButton.tintColor = COLOR.TITLE_POSITIVE_COLOR
        self.reviewButton.setTitle(TEXT.SAVE_REVIEW, for: UIControlState.normal)
        //self.reviewButton.setTitle(TEXT.SAVE_REVIEW, for: UIControlState.normal)
        self.reviewButton.titleLabel?.numberOfLines = 2
        self.reviewButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        
        self.setNeedsUpdateConstraints()
        UIView.animate(withDuration: 0.2) {
            self.layoutIfNeeded()
        }
    }
    
    
    
    @IBAction func createTaskAction(_ sender: AnyObject) {
        delegate.firstButtonAction()
    }
    
    @IBAction func reviewAction(_ sender: AnyObject) {
        delegate.secondButtonAction()
    }
}
