//
//  SignupController.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 17/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
enum RegisterType: Int {
    case individual = 0
    case family = 1
}
class SignupController: UIViewController, NavigationDelegate, PickerDelegate, CountryPhoneCodePickerDelegate {
    
    
    func setUserCurrentLocation() {
        
    }
    
    
    // MARK: - Properties
    var pickOption = ["Male", "Female", "Other"]
    let marginTitleAndTextFields: CGFloat =  42
    let marginTextFieldsAndSignUpStackView: CGFloat = 35
    var navigationBar: NavigationView!
    var pickerForDropdown: CustomDropdown!
    var countryCodeAndCountryName = [String: String]()
    var selectedLocale: String!
    var isFacebook = false
    var isInstagram = false
    var isGoogle = false
    var faceBookData = FacebookDetail()
    var instaGramData = InstaGramDetail()
    var email: String?
    var createTaskModel : CreateTaskModal? = CreateTaskModal()
    var shouldHideReferralField: Bool {
        return !AppConfiguration.current.isReferralRequired
    }
    
    @IBOutlet weak var showPwdBtn: UIButton!
    @IBOutlet weak var showConfirmPwdBtn: UIButton!
    
    var registerType: RegisterType?
    //MARK: - IBOutlets
    @IBOutlet weak var privacyPolicyBtn: UIButton!
    @IBOutlet weak var viewPasswordButton: UIButton!
    @IBOutlet weak var checkBoxButton: UIButton!
    @IBOutlet weak var termsAndConditionButton: UIButton!
    @IBOutlet weak var iAgreeToLabel: UILabel!
    @IBOutlet weak var checkboxView: UIView!
    @IBOutlet var privacyPolicyTextView: UITextView!
    @IBOutlet var signupButton: UIButton!
    @IBOutlet weak var nameField: MKTextField!
    @IBOutlet weak var emailField: MKTextField!
   // @IBOutlet weak var phoneField: VSTextField!
    @IBOutlet weak var phoneField: VSTextField!
    @IBOutlet weak var passwordField: MKTextField!
    @IBOutlet weak var confirmPasswordField: MKTextField!
    @IBOutlet weak var referalField: MKTextField!
    @IBOutlet weak var countryCodeField: MKTextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var haveAnAccountLabel: UILabel!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var distanceBetweenTopAndTitleLabel: NSLayoutConstraint!
    @IBOutlet weak var distanceBetweenTitleAndTextFields: NSLayoutConstraint!
    @IBOutlet weak var distanceBtwSignupStackViewAndTextFields: NSLayoutConstraint!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var applyTextField: MKTextField!
    @IBOutlet weak var loginButton: UIButton!
    var referralCode = ""
    
    @IBOutlet weak var genderField: MKTextField!
    @IBOutlet weak var dateOfBirthField: MKTextField!
    @IBOutlet weak var nationalityField: MKTextField!
    
    @IBOutlet weak var passwordImg: UIImageView!
    @IBOutlet weak var confirmPasswordImg: UIImageView!
    @IBOutlet weak var passwordStackView: UIStackView!
    @IBOutlet weak var confirmPasswordStackView: UIStackView!
    
    @IBOutlet weak var signupImage: UIImageView!
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        checkBoxButton.setBackgroundImage(UIImage(named: "iconCheckboxUnticked"), for: .normal)
        termsAndConditionButton.addTarget(self, action: #selector(SignupController.termsAndConditionAction), for: UIControlEvents.touchUpInside)
        privacyPolicyBtn.addTarget(self, action: #selector(SignupController.privacyPolicyAction), for: UIControlEvents.touchUpInside)
        self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        configureViews()
        checkBoxButton.isHidden = false
        termsAndConditionButton.isHidden = false
        iAgreeToLabel.isHidden = false
        
    }
    func applyButtonConfigure() {
        applyButton.clipsToBounds = true
        applyButton.layer.borderWidth = 1
        applyButton.layer.borderColor = UIColor.white.cgColor
        applyTextField.placeHolderColor = .white
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isFacebook == true {
            setUpFacebookData()
        }
        if isInstagram == true {
            setUpInstaData()
        }
        if isGoogle == true {
            setUpGoogleData()
        }
        passwordField.text = ""
        confirmPasswordField.text = ""
    }
    @IBAction func applyReferCodeAction(_ sender: Any) {
       
        
        let params = self.getParamsToDeleteLocation()
        ActivityIndicator.sharedInstance.showActivityIndicator()
        
        HTTPClient.makeConcurrentConnectionWith(method: .POST, para: params, extendedUrl: "apply_refferal_code") { (responseObject, error, _, statusCode) in
            
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            self.referralCode = ""
            print(responseObject ?? "")
            if statusCode == .some(STATUS_CODES.SHOW_MESSAGE) {
                if let json = responseObject as? [String: Any], let errMsg = json["message"] as? String {
                    self.showErrorMessage(error: errMsg)
                }
            }
            guard statusCode == .some(STATUS_CODES.SHOW_DATA) else {
                return
            }
            
            if let dataRes = responseObject as? [String:Any] {
                if let data = dataRes["data"] as? [String:Any], let refferal = data["refferal"] as? [String:Any], let refferal_code = refferal["refferal_code"] as? String {
                    self.referralCode = refferal_code
                    print(self.referralCode)
                }
            }
        }
    }
    
    @IBAction func showPwdAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        passwordField.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func showConfirmPwdAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        confirmPasswordField.isSecureTextEntry = !sender.isSelected
    }
    
    private func getParamsToDeleteLocation() -> [String: Any] {
        return [
            "refferal_code": applyTextField.text ?? ""
        ]
    }
    
    
    //MARK: - Configure Views
    private func configureViews() {
        self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        self.setNavigationBar()
        applyButtonConfigure()
        signupImage.image = registerType == RegisterType.family ? #imageLiteral(resourceName: "family-1") : #imageLiteral(resourceName: "indiviual-1")
              self.setTextFields()
        //      self.setSignupButton()
        //      self.setSignInButtonView()
        //      self.setPrivacyPolicyLabel()
        //      self.titleLabel.configureSignInTitleTypeWith(text: TEXT.SIGN_UP_TITLE)
    }
    
    func setPrivacyPolicyLabel() {
        if AppConfiguration.current.showTermsAndConditions == "1" {
            self.privacyPolicyTextView.isHidden = false
            self.privacyPolicyTextView.isUserInteractionEnabled = true
            self.privacyPolicyTextView.delegate = self
            let attributedString = self.getAttributedTotalText(title: TEXT.PROCEEDING_ACCOUNT, subTitle: TEXT.TERMS_OF_SERVICE)
            self.privacyPolicyTextView.attributedText = attributedString
        } else {
            self.privacyPolicyTextView.isHidden = true
        }
        
    }
    
    private func getAttributedTotalText(title:String,subTitle:String) -> NSMutableAttributedString {
        let attributedString = NSMutableAttributedString(string: "\(title) ", attributes: [NSForegroundColorAttributeName:JOB_STATUS_COLOR.CANCELLED, NSFontAttributeName:UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)!])
        var secondString = NSMutableAttributedString(string: "\(subTitle) ")
        attributedString.append(secondString)
        
        var linkAttributes = self.getLinkAttributes(url: AppConfiguration.current.tAndConditions)
        var foundRange = attributedString.mutableString.range(of: TEXT.TERMS_OF_SERVICE)
        attributedString.setAttributes(linkAttributes, range: foundRange)
        
        secondString = NSMutableAttributedString(string: "\(TEXT.AND) ", attributes: [NSForegroundColorAttributeName: JOB_STATUS_COLOR.CANCELLED, NSFontAttributeName:UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)!])
        attributedString.append(secondString)
        secondString = NSMutableAttributedString(string: "\(TEXT.PRIVACY_POLICY)")
        attributedString.append(secondString)
        
        linkAttributes = self.getLinkAttributes(url: AppConfiguration.current.tAndConditions)
        foundRange = attributedString.mutableString.range(of: TEXT.PRIVACY_POLICY)
        attributedString.setAttributes(linkAttributes, range: foundRange)
        
        return attributedString
    }
    
    func getLinkAttributes(url: String) -> [String : Any] {
        
        var attribute:[String : Any] = [NSLinkAttributeName: NSURL(string: url)!]
        attribute[NSForegroundColorAttributeName] = COLOR.THEME_FOREGROUND_COLOR
        attribute[NSFontAttributeName] = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)!
        return attribute
    }
    
    private func setTextFields() {
        // distanceBetweenTitleAndTextFields.constant = marginTitleAndTextFields * heightMultiplierForDifferentDevices
              setEmailField()
              setPasswordField()
        setGenderField()
        setDOBField()
        setupNationalityField()
              setNameField()
              setPhoneNumberField()
        //      setRefferalField()
              setCheckBoxView()
        //      setCountryCodeField()
    }
    
    private func setEmailField() {
         emailField.delegate = self
         emailField.placeHolderColor = .white
         emailField.bottomBorderColor = UIColor.white
      //  emailField.configureWith(text: email ?? "", placeholder: TEXT.YOUR_EMAIL)
    }
    
    private func setGenderField() {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        genderField.inputView = pickerView
        genderField.delegate = self
        genderField.placeHolderColor = .white
        genderField.bottomBorderColor = UIColor.white
        
        //  emailField.configureWith(text: email ?? "", placeholder: TEXT.YOUR_EMAIL)
    }
    
    private func setupNationalityField() {
        nationalityField.delegate = self
        nationalityField.placeHolderColor = .white
        nationalityField.bottomBorderColor = UIColor.white
        let countryPicker = CountryPicker()
        countryPicker.countryPhoneCodeDelegate = self
        nationalityField.inputView = countryPicker
    }
    
    func countryPhoneCodePicker(picker: CountryPicker, didSelectCountryCountryWithName name: String, countryCode: String, phoneCode: String) {
        self.nationalityField.text = name
    }
    
    private func setDOBField() {
        dateOfBirthField.delegate = self
        dateOfBirthField.placeHolderColor = .white
        dateOfBirthField.bottomBorderColor = UIColor.white
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.maximumDate = Date()
        dateOfBirthField.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(SignupController.datePickerValueChanged), for: UIControlEvents.valueChanged)
        //  emailField.configureWith(text: email ?? "", placeholder: TEXT.YOUR_EMAIL)
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        
        dateFormatter.timeStyle = DateFormatter.Style.none
        
        dateOfBirthField.text = dateFormatter.string(from: sender.date)
        
    }

    
    
    private func setPasswordField() {
        if isFacebook {
            showConfirmPwdBtn.isHidden = true
            showPwdBtn.isHidden = true
            passwordStackView.isHidden = true
            confirmPasswordStackView.isHidden = true
            passwordImg.isHidden = isFacebook
            passwordField.isHidden = isFacebook
            passwordField.placeHolderColor = .white
            passwordField.delegate = self
            confirmPasswordField.isHidden = isFacebook
            confirmPasswordImg.isHidden = isFacebook
            confirmPasswordField.placeHolderColor = .white
            confirmPasswordField.delegate = self
        }
        if isInstagram {
            showConfirmPwdBtn.isHidden = true
            showPwdBtn.isHidden = true
            passwordStackView.isHidden = true
            confirmPasswordStackView.isHidden = true
            passwordImg.isHidden = isFacebook
            passwordField.isHidden = isInstagram
            passwordField.placeHolderColor = .white
            passwordField.delegate = self
            confirmPasswordField.isHidden = isInstagram
            confirmPasswordImg.isHidden = isFacebook
            confirmPasswordField.placeHolderColor = .white
            confirmPasswordField.delegate = self
        }
        if isGoogle {
            showConfirmPwdBtn.isHidden = true
            showPwdBtn.isHidden = true
            passwordStackView.isHidden = true
            confirmPasswordStackView.isHidden = true
            passwordImg.isHidden = isGoogle
            passwordField.isHidden = isGoogle
            passwordField.placeHolderColor = .white
            passwordField.delegate = self
            confirmPasswordField.isHidden = isGoogle
            confirmPasswordImg.isHidden = isGoogle
            confirmPasswordField.placeHolderColor = .white
            confirmPasswordField.delegate = self
        }
        showPwdBtn.tintColor = passwordField.tintColor
        showPwdBtn.setImage(#imageLiteral(resourceName: "eyeActive").renderWithAlwaysTemplateMode(), for: .selected)
        showConfirmPwdBtn.tintColor = confirmPasswordField.tintColor
        showConfirmPwdBtn.setImage(#imageLiteral(resourceName: "eyeActive").renderWithAlwaysTemplateMode(), for: .selected)
      //  passwordField.configureWith(text: "", placeholder: TEXT.YOUR_PASSWORD)
//        viewPasswordButton.tintColor = passwordField.tintColor
//        viewPasswordButton.setImage(#imageLiteral(resourceName: "eyeActive").renderWithAlwaysTemplateMode(), for: .selected)
    }
    
    private func setNameField() {
        self.nameField.delegate = self
         nameField.placeHolderColor = .white
       // nameField.configureWith(text: "", placeholder: TEXT.YOUR_NAME)
    }
    
    private func setCountryCodeField() {
        countryCodeField.delegate = self
        let countryDialingCode = "+" + NSLocale.locale.getCurrentDialingCode()
        countryCodeField.configureWith(text: countryDialingCode, placeholder: "")
        countryCodeField.rightView = getViewWithDownArrowImage()
        countryCodeField.rightViewMode = .always
        
    }
    
    private func getViewWithDownArrowImage() -> UIView {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        let imageView = UIImageView.init(image: #imageLiteral(resourceName: "downArrow").renderWithAlwaysTemplateMode())
        imageView.tintColor = COLOR.SPLASH_TEXT_COLOR
        imageView.contentMode = .center
        view.frame = imageView.frame
        view.frame.size.width += 10
        view.addSubview(imageView)
        return view
    }
    
    private func setPhoneNumberField() {
        phoneField.delegate = self
        phoneField.setFormatting("XXX-XXX-XXXX", replacementChar: "X")
        phoneField.attributedPlaceholder = NSAttributedString(string: "Phone Number e.g. (052-993-3645)",
                                                         attributes: [NSForegroundColorAttributeName: UIColor.white])
      //   phoneField.placeHolderColor = .white
//        phoneField.configureWith(text: "", placeholder: TEXT.YOUR_CONTACT)
    }
    
    private func setRefferalField() {
        referalField.delegate = self
        referalField.configureWith(text: "", placeholder: TEXT.REFERRAL_CODE)
        referalField.isHidden = shouldHideReferralField
    }
    
    private func setSignupButton() {
        //      signupButton.configureNormalButtonWith(title: TEXT.SIGN_UP)
        //      distanceBtwSignupStackViewAndTextFields.constant = marginTextFieldsAndSignUpStackView * heightMultiplierForDifferentDevices
    }
    
    private func setCheckBoxView() {
        iAgreeToLabel.configureNewUserTypeLabelWith(text: TEXT.I_AGREE_TO)
        iAgreeToLabel.textColor = UIColor.white
        termsAndConditionButton.configureSmallButtonWith(title: TEXT.TERMS_CONDITIONS)
        termsAndConditionButton.backgroundColor = UIColor.clear
        termsAndConditionButton.setTitleColor(UIColor.white, for: UIControlState.normal)
        privacyPolicyBtn.configureSmallButtonWith(title: TEXT.AND_PRIVACY_POLICY)
        privacyPolicyBtn.backgroundColor = UIColor.clear
        privacyPolicyBtn.setTitleColor(UIColor.white, for: UIControlState.normal)
        //termsAndConditionButton.isHidden = AppConfiguration.current.showTermsAndConditions == "1" ? true : false
        termsAndConditionButton.isHidden = false
        checkboxView.isHidden = false
    }
    
    private func setSignInButtonView() {
        haveAnAccountLabel.configureNewUserTypeLabelWith(text: TEXT.HAVE_AN_ACCOUNT)
        signInButton.configureSmallButtonWith(title: TEXT.SIGN_IN)
    }
    
    //MARK: NAVIGATION BAR
    func setNavigationBar() {
        
        navigationBar = NavigationView.getNibFile(params:NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "Log in", title: "", leftButtonImage: #imageLiteral(resourceName: "back"), rightButtonImage: nil) , leftButtonAction: {[weak self] in
            _ = self?.navigationController?.popViewController(animated: true)
            
            }, rightButtonAction: {[weak self] in
                guard let navVC = self?.navigationController else {
                    print("Navigation controller of Splash not found")
                    return
                }
                LoginController.pushIn(navVC: navVC, withEmail: "")
        })
        
        self.view.addSubview(navigationBar)

        
        
//        navigationBar = NavigationView.getNibFile(leftButtonAction: { [weak self] in
//            self?.backAction()
//            }, rightButtonAction: nil)
//
        navigationBar.setBackgroundColor(color: UIColor.clear, andTintColor: COLOR.SPLASH_TEXT_COLOR)
        navigationBar.bottomLine.isHidden = true
//        self.view.addSubview(navigationBar)
       
    }
    
    //MARK: - IBAction
    func backAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signupAction(_ sender: AnyObject) {
        self.checkValidation()
    }
    
    @IBAction func viewPasswordButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        passwordField.isSecureTextEntry = !sender.isSelected
        confirmPasswordField.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func countryCodeButtonPressed() {
        showCountryCode()
    }
    
    @IBAction func signInButtonPressed(_ sender: UIButton) {
        if previousControllerIsLogInController() {
            _ = self.navigationController?.popViewController(animated: true)
        } else {
            guard let navVC = self.navigationController else {
                print("Navigation controller of Splash not found")
                return
            }
            LoginController.pushIn(navVC: navVC, withEmail: "")
        }
    }
    
    private func previousControllerIsLogInController() -> Bool {
        let previousController = self.navigationController?.getSecondLastController()
        return previousController is LoginController
    }
    
    //MARK: VALIDATION CHECK
    func checkValidation() {
        var email = (self.emailField.text?.trimText)!
        var phone = (self.phoneField.text?.trimText)!
        var password = ""
        var confirmPassword = ""
        
        let name = (self.nameField.text?.trimText)!
        
        guard name.length > 0 else {
            self.showErrorMessage(error: "Please enter name")
            return
        }
        
        var contact = "" // (self.countryCodeField.text)! + " " +
        
        if !Singleton.sharedInstance.validateEmail(email) {
            email = ""
        } else {
            email = (self.emailField.text?.trimText)!
        }

        if !Singleton.sharedInstance.validatePhoneNumber(phoneNumber: (self.phoneField.text?.trimText)!) && phoneNumberContainsCharacterOtherThanNumbers(phone:(self.phoneField.text?.trimText)!) == true {
            contact = ""
        } else {
            contact = (self.phoneField.text?.trimText)!
        }
        
//        if !Singleton.sharedInstance.validatePhoneNumber(phoneNumber: (self.emailField.text?.trimText)!) && phoneNumberContainsCharacterOtherThanNumbers(phone:(self.emailField.text?.trimText)!) == true {
//            contact = ""
//        } else {
//            contact = (self.emailField.text?.trimText)!
//        }
        
        
        if (email == "") {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PHONE_Email)
            return
        }
        
        if (contact == "") {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PHONE_Number)
            return
        }

        if !self.checkBoxButton.isSelected {
            self.showErrorMessage(error: ERROR_MESSAGE.ACCEPT_TERMS_POLICY)
            return
        }
        
        if isFacebook == false && isInstagram == false && isGoogle == false {
            password = (self.passwordField.text?.trimText)!
            guard Singleton.sharedInstance.validPassword(password:password) == true else {
                self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PASSWORD)
                return
            }
            
            confirmPassword = (self.confirmPasswordField.text?.trimText)!
            guard Singleton.sharedInstance.validPassword(password:confirmPassword) == true else {
                self.showErrorMessage(error: ERROR_MESSAGE.INVALID_CONFIRM_PASSWORD)
                return
            }
            if password != confirmPassword {
                self.showErrorMessage(error: ERROR_MESSAGE.PASSWORD_MISMATCH)
                return
            }
        }
        
        if isFacebook == false && isInstagram == false && isGoogle == false  {
            password = (self.passwordField.text?.trimText)!
            confirmPassword = (self.confirmPasswordField.text?.trimText)!
        } else {
            password = "123456"
            confirmPassword = "123456"
        }
        
        let gender = self.genderField.text ?? ""
        let dob = self.dateOfBirthField.text ?? ""
        let nationality = self.nationalityField.text ?? ""
        
        if isFacebook == true {
            self.serverRequest(email: email, password: password, name: name, contact: contact,isFacebook: true,id: faceBookData.id, social_login_token: faceBookData.fbToken, social_sign_up_type: faceBookData.socialSignUpType, gender: gender, dob: dob, nationality: nationality)
        } else if isInstagram == true {
            self.serverRequest(email: email, password: password, name: name, contact: contact,isFacebook: true, id: instaGramData.instaId, social_login_token: instaGramData.instaToken, social_sign_up_type: instaGramData.socialSignUpType, gender: gender, dob: dob, nationality: nationality)
        } else if isGoogle == true {
            self.serverRequest(email: email, password: password, name: name, contact: contact,isFacebook: true,id: faceBookData.id, social_login_token: faceBookData.fbToken, social_sign_up_type: faceBookData.socialSignUpType, gender: gender, dob: dob, nationality: nationality)
        } else {
            self.serverRequest(email: email, password: password, name: name, contact: contact, isFacebook: false, social_sign_up_type: 0, gender: gender, dob: dob, nationality: nationality)
        }
    }
    
    func phoneNumberContainsCharacterOtherThanNumbers(phone: String) -> Bool {
        
        return phone.count != phone.stripOutUnwantedCharacters(charactersYouWant: "0123456789+-").count
    }
    
    //MARK: SERVER REQUEST
    func serverRequest(email:String, password:String, name:String, contact:String,isFacebook : Bool, id:String = "", social_login_token: String = "", social_sign_up_type: Int, gender: String, dob: String, nationality: String) {
        
        self.view.endEditing(true)
        ActivityIndicator.sharedInstance.showActivityIndicator()
        //      getCountryCodeAndContinentCode { [weak self] (success, countryCode, continentCode) in
        //         guard success, self != nil else {
        //            print("Contry Code, Continent Code Not Found")
        //            ActivityIndicator.sharedInstance.hideActivityIndicator()
        //            return
        //         }
        
        if isFacebook == true {
            logEvent(label: "sign_up_facebook")
        } else if isInstagram == true {
            logEvent(label: "sign_up_instagram")
        } else if isGoogle == true {
            logEvent(label: "sign_up_google")
        } else{
            logEvent(label: "sign_up_email")
        }
        logEvent(label: "sign_up_taxi_app")
        
        
        APIManager.sharedInstance.signupRequest(name,
                                                phone: contact,
                                                email: email,
                                                password: password,
                                                isFacebook: isFacebook,
                                                id: id,
                                                refferalCode: self.referralCode,
                                                signUpType: self.registerType!,
                                                social_login_token: social_login_token,
                                                social_sign_up_type: social_sign_up_type,
                                                gender: gender,
                                                dob: dob, nationality: nationality) { (succeeded, response) in
            print(response)
            DispatchQueue.main.async(execute: { () -> Void in
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if succeeded == true {
                    UserDefaults.standard.set(false, forKey: "isGuestUser")
                    logEvent(label: "user_sign_up_success")
                    Singleton.sharedInstance.isSignedIn = true
                    self.signupButton.isEnabled = true
                    if let status = response["status"] as? Int {
                        switch(status) {
                        case STATUS_CODES.SHOW_DATA:
                            if let data = response["data"] as? [String: Any] {
                                Vendor.logInWith(data: data)
                                Vendor.current?.isFirstSignup = true
//                                if AppConfiguration.current.isDemoEnabled == true{
//                                    self.checkForPendingTask()
//                                } else {
                                    self.onSuccessFullSignUp()
//                                }
                            }
                        case STATUS_CODES.INVALID_ACCESS_TOKEN:
                            
                            Singleton.sharedInstance.showAlertWithOption(owner: self, title: "", message: response["message"] as! String!, showRight: false, leftButtonAction: {
                                print("")
                            }, rightButtonAction: nil, leftButtonTitle: "Ok", rightButtonTitle: "")
                            
                            break
                            
                        default:
                            self.showErrorMessage(error: response["message"] as! String!)
                            break
                        }
                    }
                } else {
                    self.signupButton.isEnabled = true
                    self.showErrorMessage(error: response["message"] as! String!)
                }
            })

        }
        //      }
    }
    
    
    func checkForPendingTask() {
        self.sendReferralCodeToServerIfPresent()
        if Singleton.sharedInstance.pendingTaskFromDemo != nil && Singleton.sharedInstance.pendingTaskFromDemo! == true {
            ActivityIndicator.sharedInstance.showActivityIndicator()
            //         self.createTaskModel?.apiHitForTask( id: "", type: "8", tip: "", response: Singleton.sharedInstance.createTaskDetail.responseOfGetPayment!, { (isSuccess, response) in
            //
            //         })
            
            APIManager.sharedInstance.serverCall(apiName: API_NAME.createTaskForSavedCart, params: Singleton.sharedInstance.createTaskDetail.setParmForDemoTask() as [String : AnyObject], httpMethod: "POST", receivedResponse: { ( isSuccess, response) in
                print(response)
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                Singleton.sharedInstance.pendingTaskFromDemo = false
                self.gotoExtraInfoVC()
            })
            
            
        } else {
            self.gotoExtraInfoVC()
        }
    }
    
    func makeParams(){
        
    }
    
    func gotoExtraInfoVC() {
        
        //      if let vc = storyboard.instantiateViewController(withIdentifier: STORYBOARD_ID.extraInfoVC) as? ExtraInfoVC {
        //         self.navigationController?.pushViewController(vc, animated: true)
        //      }
        let storyboard = UIStoryboard(name: STORYBOARD_NAME.demo, bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: STORYBOARD_ID.completionSignupVC) as? CompletionSignupVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func onSuccessFullSignUp(){
       
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.otpScreen) as? OtpScreenViewController {
            vc.registerType = self.registerType
            self.navigationController?.pushViewController(vc, animated: true   )
        }
//        Singleton.sharedInstance.checkForLogin(owener: self, startLoaderAnimation: {
//            ActivityIndicator.sharedInstance.showActivityIndicator()
//        }, stopLoaderAnimation: {
//            ActivityIndicator.sharedInstance.hideActivityIndicator()
//        })
//        self.sendReferralCodeToServerIfPresent()
    }
    
    
    private func sendReferralCodeToServerIfPresent() {
        guard let referralCode = referalField.text, referralCode.isEmpty == false else {
            print("No referral code present")
            return
        }
        logEvent(label: "referral_applied")
        Vendor.current!.sendReferralCodeToServer(referralCode: self.referalField.text!, completion: nil)
        
//        Vendor.current!.sendReferralCodeToServer(referralCode: self.referalField.text!, completion: { (success) in
//            if success {
//                ErrorView.showWith(message: "Referral code applied successfully.", removed: nil)
//            } else {
//                ErrorView.showWith(message: "Invalid referral code.", removed: nil)
//            }
//            
//        })
    }
    
    func getCountryCodeAndContinentCode(completion: @escaping (Bool, String?, String?) -> Void) {
        let serverUrl = "https://ip.tookanapp.com:8000/requestCountryCodeGeoIP2"
        
        HTTPClient.shared.makeSingletonConnectionWith(method: .GET, showActivityIndicator: false, baseUrl: serverUrl, extendedUrl: "") { (responseObject, _, _, statusCode) in
            guard let response = responseObject as? [String: Any],
                let data = response["data"] as? [String: Any],
                let countryCode = data["country_code"] as? String,
                let continentCode = data["continent_code"] as? String
                else {
                    completion(false, nil, nil)
                    return
            }
            
            completion(true, countryCode, continentCode)
        }
    }
    
    //MARK: COUNTRY CODE
    func showCountryCode() {
        self.view.endEditing(true)
        let countryArray = NSMutableArray()
        for locale in NSLocale.locales() {
            countryArray.add(locale.countryName)
            countryCodeAndCountryName[locale.countryName] = locale.countryCode
        }
        
        pickerForDropdown = UINib(nibName: NIB_NAME.customDropdown, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomDropdown
        pickerForDropdown.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        pickerForDropdown.valueArray =  countryArray
        pickerForDropdown.delegate = self
        self.view.addSubview(pickerForDropdown)
        pickerForDropdown.setPickerView(customFieldRow: 0)
    }
    
    //MARK: - Dropdown Picker Delegate
    func dismissPicker() {
        pickerForDropdown = nil
    }
    
    func selectedValueFromPicker(_ value: String, customFieldRowIndex:Int) {
        var code = ""
        if countryCodeAndCountryName[value] != nil {
            let locale = countryCodeAndCountryName[value]!
            self.selectedLocale = locale
            if dialingCode[selectedLocale] != nil {
                code = "+" + dialingCode[self.selectedLocale]!
            } else {
                code = "+1"
            }
            
        } else {
            code = "+"
        }
        pickerForDropdown = nil
        self.countryCodeField.text = code
    }
    
    //MARK: ERROR MESSAGE
    func showErrorMessage(error:String) {
        ErrorView.showWith(message: error, removed: nil)
    }
    
    @IBAction func checkBoxButtonAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            checkBoxButton.setBackgroundImage(UIImage(named: "checkboxOnCopy"), for: .normal)
        } else {
            checkBoxButton.setBackgroundImage(UIImage(named: "iconCheckboxUnticked"), for: .normal)
            checkBoxButton.setImage(nil, for: .normal)
        }
    }
    
    func setUpFacebookData()  {
        emailField.text = faceBookData.email
        emailField.isEnabled = faceBookData.email.isEmpty
        
        nameField.text = faceBookData.fullName
        viewPasswordButton.isHidden = true
    }
    
    func setUpInstaData() {
        emailField.text = ""
        viewPasswordButton.isHidden = true
    }
    
    func setUpGoogleData() {
        emailField.text = faceBookData.email
        emailField.isEnabled = faceBookData.email.isEmpty
        
        nameField.text = faceBookData.fullName
    }
    
    //MARK: - Navigation
    class func getWith(registerType: RegisterType?) -> SignupController {
        let signUpController = findIn(storyboard: .main, withIdentifier: STORYBOARD_ID.sigupController) as! SignupController
        signUpController.registerType = registerType
        return signUpController
    }
    
    class func pushIn(navVC: UINavigationController, registerType: RegisterType?) {
        let signUpController = getWith(registerType: registerType)
        navVC.pushViewController(signUpController, animated: true)
    }
    
    func pushInWithFacebookData(navVC: UINavigationController, registerType: RegisterType?, facebookDetails: FacebookDetail) {
        print(facebookDetails.email)
        print(facebookDetails.fullName)
        print(facebookDetails.id)
        
        self.faceBookData = facebookDetails
        
        let signUpController = getWithFacebook(registerType: registerType)
        navVC.pushViewController(signUpController, animated: true)
        self.isFacebook = true
        self.setUpFacebookData()
    }
    
    func getWithFacebook(registerType: RegisterType?) -> SignupController {
        let signUpController = findInWithFacebook(storyboard: .main, withIdentifier: STORYBOARD_ID.sigupController) as! SignupController
        signUpController.registerType = registerType
        return signUpController
    }
    
    
    func termsAndConditionAction(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "terms") as? TermsAndConsitionViewController
        vc?.urlString = "https://www.bawiq.com/terms-and-conditions/"
        self.present(vc!, animated: true, completion: nil)
    }
    
    func privacyPolicyAction(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "terms") as? TermsAndConsitionViewController
        vc?.urlString = "https://www.bawiq.com/privacy-policy/"
        self.present(vc!, animated: true, completion: nil)
    }
    
}

extension SignupController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameField:
            emailField.becomeFirstResponder()
        case emailField:
            showCountryCode()
        case phoneField:
            if isFacebook {
                passwordField.becomeFirstResponder()
            } else if !shouldHideReferralField {
                referalField.resignFirstResponder()
            } else {
                textField.resignFirstResponder()
            }
        case passwordField:
            if !shouldHideReferralField {
                referalField.becomeFirstResponder()
            } else {
                textField.resignFirstResponder()
            }
        case confirmPasswordField:
            if !shouldHideReferralField {
                referalField.becomeFirstResponder()
            } else {
                textField.resignFirstResponder()
            }
        case referalField:
            referalField.resignFirstResponder()
        default:
            break
        }
        return true
    }
  
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case phoneField:
            if textField.text == "" {
                //            if string == "0" {
                //                phoneField.text = string
                //            } else {
                //                phoneField.text = "0" + string
                //            }
                phoneField.text = "0" + string
                return false
            }
            return true
        default:
            break            
        }
        return true
    }
    
}


extension SignupController: UITextViewDelegate{
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        if UIApplication.shared.canOpenURL(URL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL, options: [:], completionHandler: { (success) in
                    print("Open url : \(success)")
                })
            }
        }
        return true
    }
}

extension SignupController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickOption[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genderField.text = pickOption[row]
    }

}
