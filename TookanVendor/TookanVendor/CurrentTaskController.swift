//
//  CurrentTaskController.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 22/12/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
import DGActivityIndicatorView

class CurrentTaskController: UIViewController, NavigationDelegate, UITableViewDelegate, UITableViewDataSource, ErrorDelegate {
    func setUserCurrentLocation() {
        
    }
    

  // MARK: - Properties
  var refreshControl = UIRefreshControl()
  let footerHeight:CGFloat = 50.0
  var activityIndicator:DGActivityIndicatorView!
  var limit = 0
  var backgroundView:UIView!
  var backgroundIndicator:DGActivityIndicatorView!
  var backgroundLabel:UILabel!
  var errorMessageView:ErrorView!
  
  private var allOrders = [OrderHistory]()
  
  // MARK: - IBoutlets
    @IBOutlet var currentTaskTable: UITableView!

  // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = COLOR.REVIEW_BACKGROUND_COLOR
        self.setNavigationBar()
       // self.setTitleView()
        self.setTableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        limit = 0
        if self.backgroundIndicator != nil {
            self.backgroundIndicator.startAnimating()
        }
        self.setTableBackgroundView(backgroundText: TEXT.LOADING)
       allOrders.removeAll()
        currentTaskTable.reloadData()
        self.getCurrentTaskList()
        self.setFooterView()
        NotificationCenter.default.addObserver(self, selector: #selector(CurrentTaskController.refreshBookings), name: NSNotification.Name(rawValue: NOTIFICATION_OBSERVER.updateOnPushNotification), object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_OBSERVER.updateOnPushNotification), object: nil )
    }
    
    func setTableView() {
        self.currentTaskTable.delegate = self
        self.currentTaskTable.dataSource = self
        self.currentTaskTable.backgroundColor = COLOR.REVIEW_BACKGROUND_COLOR
        self.currentTaskTable.rowHeight = UITableViewAutomaticDimension
        self.currentTaskTable.estimatedRowHeight = 490
        self.currentTaskTable.separatorStyle  =  .none
        self.currentTaskTable.register(UINib.init(nibName: NIB_NAME.currentTaskCell, bundle: Bundle.main), forCellReuseIdentifier: NIB_NAME.currentTaskCell)
        self.setTableBackgroundView(backgroundText: TEXT.LOADING)
        self.addingRefreshControl()
    }
    
    //MARK: NAVIGATION BAR
    func setNavigationBar() {
      
        let navi = NavigationView.getNibFile(withHeight: 80,params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: TEXT.CURRENT + " " + Singleton.sharedInstance.formDetailsInfo.call_tasks_as, leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"), rightButtonImage: nil)
            , leftButtonAction: {[weak self] in
                _ = self?.navigationController?.popViewController(animated: true)
        }, rightButtonAction: nil)
        
        
        navi.setBackgroundColor(color: COLOR.THEME_FOREGROUND_COLOR, andTintColor: COLOR.LOGIN_BUTTON_TITLE_COLOR)
        
        self.view.addSubview(navi)
        
 
        
        
    }
    
    //MARK: NAVIGATION DELEGATE METHODS
    func backAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:TITLE VIEW
    func setTitleView() {
        let titleView = UINib(nibName: NIB_NAME.titleView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TitleView
        titleView.frame = CGRect(x: 0, y: HEIGHT.navigationHeight, width: self.view.frame.width, height: HEIGHT.titleHeight)
        titleView.backgroundColor = UIColor.clear
        self.view.addSubview(titleView)
        titleView.setTitle(title: TEXT.CURRENT, boldTitle: Singleton.sharedInstance.formDetailsInfo.call_tasks_as)
    }
    
    //MARK: PULL TO REFRESH
    func addingRefreshControl() {
        refreshControl.backgroundColor = .clear
        refreshControl.tintColor = COLOR.THEME_FOREGROUND_COLOR
        refreshControl.attributedTitle = NSAttributedString(string: TEXT.PULL_TO_REFRESH)
        refreshControl.addTarget(self, action: #selector(OrderHistoryVC.pullRefresh), for: UIControlEvents.valueChanged)
        self.currentTaskTable.addSubview(refreshControl)
    }
    
    func pullRefresh() {
        limit = 0
        self.getCurrentTaskList()
    }
    
    func getCurrentTaskList() {
        let params = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,"vendor_id":String(Vendor.current!.id!),"app_device_type": APP_DEVICE_TYPE,"job_id":"","job_status":"\(JOB_STATUS.arrived.rawValue), \(JOB_STATUS.started.rawValue)", "limit":"\(limit)"] as [String : Any]
        APIManager.sharedInstance.serverCall(apiName: API_NAME.getOrderHistory, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) {
            [weak self](isSuccess, response) in
            if let weakSelf = self {
                DispatchQueue.main.async {
                    print(response)
                    if isSuccess {
                       weakSelf.allOrders.removeAll()
                        if weakSelf.limit == 0 {
                           weakSelf.allOrders = [OrderHistory]()
                        }
                      weakSelf.allOrders = OrderHistory.getOrderListFrom(json: response)
                        let orderLists = weakSelf.allOrders
                        var orderCount = 0
                        for i in 0..<orderLists.count {
                            orderCount = orderCount + orderLists[i].tasksOfOrder.count
                        }
                        weakSelf.currentTaskTable.reloadData()
                        weakSelf.limit = weakSelf.limit + orderCount
                        weakSelf.activityIndicator.stopAnimating()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            weakSelf.refreshControl.endRefreshing()
                        }
                        if weakSelf.allOrders.count == 0 {
                            weakSelf.setTableBackgroundView(backgroundText: TEXT.NO + " " + Singleton.sharedInstance.formDetailsInfo.call_tasks_as + " " + TEXT.FOUND)
                            if weakSelf.backgroundIndicator != nil {
                                weakSelf.backgroundIndicator.stopAnimating()
                            }
                        }
                    } else {
                        if weakSelf.limit == 0 {
                            weakSelf.allOrders = [OrderHistory]()
                        }
                        weakSelf.activityIndicator.stopAnimating()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                            weakSelf.refreshControl.endRefreshing()
                        }
                        weakSelf.currentTaskTable.reloadData()
                        if weakSelf.allOrders.count == 0 {
                            weakSelf.setTableBackgroundView(backgroundText: TEXT.NO + " " + Singleton.sharedInstance.formDetailsInfo.call_tasks_as + " " + TEXT.FOUND)
                            if weakSelf.backgroundIndicator != nil {
                                weakSelf.backgroundIndicator.stopAnimating()
                            }
                        }
                        if let message =  response["message"] as? String{
                            if message != "No data found."{
                                self?.showErrorMessage(error: response["message"] as! String, isError: true)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func sendRequestForTrackAgent(jobId:String, Button: UIButton) {
        Button.isEnabled = false
        let params = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,"job_id":jobId,"app_device_type": APP_DEVICE_TYPE,"request_type":"1"] as [String : Any]
        APIManager.sharedInstance.serverCall(apiName: API_NAME.trackAgent, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) {[weak self] (isSucceeded, response) in
             Button.isEnabled = true
            if isSucceeded == true{
            if let weakself = self {
                DispatchQueue.main.async {
                    print(response)
                    if let data = response["data"] as? [String:Any] {
                        if let sessionId = data["session_id"] as? String {
//                            var orderDetailVC : OrderDetailVC!
//                            orderDetailVC = weakself.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.orderDetailVC) as! OrderDetailVC
//                            orderDetailVC.sessionId = sessionId
//                            orderDetailVC.jobId = jobId
//                            orderDetailVC.forTracking = true
//                            weakself.navigationController?.pushViewController(orderDetailVC, animated: true)
                        }
                    }
                }
            }
            }else{
                self?.showErrorMessage(error: response["message"] as! String, isError: true)
            }
        }
    }
    
    //MARK: TRACK ACTION
    func trackAction(sender:UIButton) {
        let orderDetails = allOrders[sender.tag / 100].tasksOfOrder[sender.tag % 100] 
        print(orderDetails.job_id)
        self.sendRequestForTrackAgent(jobId: orderDetails.job_id,Button: sender)
    }
    
    //MARK: UITABLEVIEW DELEGATE
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if allOrders.count > 0 {
            self.currentTaskTable.backgroundView = nil
            if self.backgroundIndicator != nil {
                self.backgroundIndicator.stopAnimating()
            }
        }
        return allOrders.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == allOrders.count - 1 {
            guard limit == 0 || limit >= 10 else {
                return
            }
            if self.activityIndicator != nil {
                self.activityIndicator.startAnimating()
                self.getCurrentTaskList()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.currentTaskCell) as! CurrentTaskCell
        cell.backgroundColor = COLOR.REVIEW_BACKGROUND_COLOR
        
        if allOrders[indexPath.row].tasksOfOrder.count == 1 {
            cell.setSingleTaskCard(orderHistory: allOrders[indexPath.row].tasksOfOrder[0])
            cell.secondCardTrackingButton.tag = indexPath.row * 100
            cell.secondCardTrackingButton.addTarget(self, action: #selector(self.trackAction(sender:)), for: .touchUpInside)
        }else{
            cell.setTwoTaskCard(orderHistory: allOrders[indexPath.row])
            cell.secondCardTrackingButton.tag = indexPath.row * 100 + 1
            cell.secondCardTrackingButton.addTarget(self, action: #selector(self.trackAction(sender:)), for: .touchUpInside)
            cell.firstCardTrackingButton.tag = indexPath.row * 100
            cell.firstCardTrackingButton.addTarget(self, action: #selector(self.trackAction(sender:)), for: .touchUpInside)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }

    
    //MARK: TABLE BACKGROUND VIEW
    func setTableBackgroundView(backgroundText:String) {
        if self.backgroundView == nil {
            self.backgroundView = UIView()
            self.backgroundView.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: SCREEN_SIZE.height - 100)
            self.backgroundIndicator = DGActivityIndicatorView.init(type: DGActivityIndicatorAnimationType.ballTrianglePath, tintColor: COLOR.THEME_FOREGROUND_COLOR, size: 40.0)
            self.backgroundIndicator?.frame = CGRect(x: 0, y: self.backgroundView.frame.height/2 - 150, width: 50, height: 50)
            self.backgroundIndicator?.center = CGPoint(x: self.backgroundView.center.x, y: (self.backgroundIndicator?.center.y)!)
            self.backgroundIndicator?.clipsToBounds = true
            self.backgroundIndicator?.startAnimating()
            self.backgroundView.addSubview(self.backgroundIndicator!)
            
            
            self.backgroundLabel = UILabel(frame: CGRect(x: 0, y: (self.backgroundIndicator?.frame.origin.y)! + (self.backgroundIndicator?.frame.size.height)! + 20, width: self.backgroundView.frame.width, height: 50))
            self.backgroundLabel.textColor = COLOR.PLACEHOLDER_COLOR
            self.backgroundLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.large)
            self.backgroundLabel.textAlignment = NSTextAlignment.center
            self.backgroundLabel.text = backgroundText
            //label.text = TEXT.NO + " " + Singleton.sharedInstance.formDetailsInfo.call_tasks_as + " " + TEXT.FOUND
            self.backgroundView.addSubview(self.backgroundLabel)
        } else {
            self.backgroundLabel.text = backgroundText
        }
        self.currentTaskTable.backgroundView = self.backgroundView
    }
    
    //MARK: FOOTER VIEW
    func setFooterView() {
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: footerHeight)
        activityIndicator = DGActivityIndicatorView.init(type: DGActivityIndicatorAnimationType.ballTrianglePath, tintColor: COLOR.THEME_FOREGROUND_COLOR, size: 40.0)
        activityIndicator?.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityIndicator?.center = CGPoint(x: self.view.center.x, y: (activityIndicator?.center.y)!)
        activityIndicator?.clipsToBounds = true
        view.addSubview(activityIndicator)
        self.currentTaskTable.tableFooterView = view
    }
    
    //MARK: ERROR MESSAGE
    func showErrorMessage(error:String, isError:Bool) {
        if errorMessageView != nil {
            self.removeErrorView()
        }
        
        if errorMessageView == nil {
            errorMessageView = UINib(nibName: NIB_NAME.errorView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
            errorMessageView.delegate = self
            errorMessageView.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: HEIGHT.errorMessageHeight)
            self.view.addSubview(errorMessageView)
            errorMessageView.setErrorMessage(message: error, isError: isError)
        }
    }
    
    //MARK: ERROR DELEGATE METHOD
    func removeErrorView() {
        if self.errorMessageView != nil {
            self.errorMessageView = nil
        }
    }
    
    //MARK:REFRESH FUNCTION
    func refreshBookings(){
        limit = 0
        
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            self.getCurrentTaskList()
        }
        
    }
    
    deinit {
        print("current order deinit")
    }

}
