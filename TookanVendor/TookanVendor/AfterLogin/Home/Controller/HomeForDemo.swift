//
//  HomeForDemo.swift
//  TookanVendor
//
//  Created by CL-Macmini-110 on 10/11/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import Fugu
import MapKit
import Kingfisher

class HomeForDemo: UIViewController {

   //MARK:- Outlets
   @IBOutlet var companyName: UILabel!
   @IBOutlet var backgroundImageView: UIImageView!
   @IBOutlet var bottomCollectionView: UICollectionView!
   @IBOutlet var companyDescription: UILabel!
   @IBOutlet var orderOnline: UIButton!
   @IBOutlet var menuButton: UIButton!
   @IBOutlet var shareButton: UIButton!
   @IBOutlet var signupBackgroundView: UIView!
   @IBOutlet var completeDemoLabel: UILabel!
   @IBOutlet var signupNowButton: UIButton!
   @IBOutlet var topLine: UIView!
   @IBOutlet var heightOfSignupView: NSLayoutConstraint!
   
   //MARK:- Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      self.setBlurView()
      self.setCollectionView()
      self.setCompanyName()
      self.setDescription()
      self.setOnlineOrderButton()
      self.setCompleteDemoLabel()
      self.setSignUpButton()
      self.setSignupBackgroundView()
      self.shareButton.isHidden = true
      self.menuButton.isHidden = true
      self.topLine.backgroundColor = COLOR.LINE_COLOR
      if Vendor.current == nil {
         self.heightOfSignupView.constant = 40
         self.signupBackgroundView.isHidden = false
      } else {
         self.heightOfSignupView.constant = 0
         self.signupBackgroundView.isHidden = true
      }
    }

   //MARK:- Configure Views
   func setBlurView() {
    guard Vendor.current != nil else {
        self.backgroundImageView.image = #imageLiteral(resourceName: "HomeBackground")
        return
    }
    
    guard Vendor.current?.isDemoUser() == false else {
        self.backgroundImageView.image = #imageLiteral(resourceName: "HomeBackground")
        return
    }
    
    
//    self.backgroundImageView.kf.setImage(with: URL(string:Singleton.sharedInstance.formDetailsInfo.logo!), placeholder: #imageLiteral(resourceName: "splashBackground"))
      var blurEffectView: UIView!
      blurEffectView = UIView()
//      let blurEffectView = UIVisualEffectView(effect: blurEffect)
      blurEffectView.frame = self.backgroundImageView.bounds
      blurEffectView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
      blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      self.backgroundImageView.addSubview(blurEffectView)
   }
   
   func setCollectionView()  {
      self.bottomCollectionView.delegate = self
      self.bottomCollectionView.dataSource = self
      self.bottomCollectionView.register(UINib(nibName: NIB_NAME.homeBottomCell, bundle: nil), forCellWithReuseIdentifier: NIB_NAME.homeBottomCell)
   }
   
   func setCompanyName() {
    self.companyName.text = Vendor.current == nil ? "Smoke & Grill" : Vendor.current?.isDemoUser() == true ? "Smoke & Grill" : Singleton.sharedInstance.formDetailsInfo.formName//
      self.companyName.font = UIFont(name: FONT.semiBold, size: FONT_SIZE.maxFontSize)
    self.companyName.letterSpacing = 1
      self.companyName.textColor = UIColor.white
   }
   
   func setDescription() {
    self.companyDescription.text = Vendor.current == nil ? "Making people happy through food. Order with us to get kill all your hunger fits." : Vendor.current?.isDemoUser() == true ? "Making people happy through food. Order with us to get kill all your hunger fits." : Singleton.sharedInstance.formDetailsInfo.app_description//
      self.companyDescription.font = UIFont(name: FONT.regular, size: FONT_SIZE.priceFontSize)
      self.companyDescription.textColor = UIColor.white
   }
   
   func setOnlineOrderButton() {
      
      self.orderOnline.backgroundColor = COLOR.BRICK_COLOR
      self.orderOnline.setTitle(TEXT.ORDER_ONLINE_CAP, for: .normal)
      self.orderOnline.layer.cornerRadius = self.orderOnline.frame.height/2//getAspectRatioValue(value: )
      self.orderOnline.setTitleColor(UIColor.white, for: .normal)
   }
   
   func setSignupBackgroundView() {
      self.signupBackgroundView.backgroundColor = UIColor.white
   }
   
   func setCompleteDemoLabel() {
      self.completeDemoLabel.textColor = UIColor.black
      self.completeDemoLabel.text = TEXT.COMPLETE_DEMO
      self.completeDemoLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.priceFontSize)
   }
   
   func setSignUpButton() {
      self.signupNowButton.configureNormalButtonWith(title: "\(TEXT.SIGN_UP_CAP) \(TEXT.NOW_CAP)")
      self.signupNowButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)!
      signupNowButton.layer.cornerRadius = 4
   }
   
   //MARK:- IBAction
   @IBAction func orderAction(_ sender: Any) {
      
      self.sdkCalling()
   }
   
   @IBAction func signupAction(_ sender: Any) {
      
    SignupController.pushIn(navVC: self.navigationController!, registerType: nil)
//      if let parentVC = self.parent  as? GenricHomeWithSideMenuViewController {
//         parentVC.hideSideMenuView()
//      }
   }
}

//MARK: - UICollection View Delegate
extension HomeForDemo: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
   
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
      return UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
   }
   
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      return 3
   }
   
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      let width:CGFloat = collectionView.frame.width/3
    return CGSize(width: width, height : 70)
   }
   
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NIB_NAME.homeBottomCell, for: indexPath) as? HomeBottomCell else{
         fatalError("HomeBottomCell")
      }
      cell.rightLine.isHidden = false
      switch indexPath.item {
      case 0:
        let extractedExpr = #imageLiteral(resourceName: "ourMenuIcon")
        cell.topImageButton.setImage(extractedExpr, for: .normal)
         cell.bottomLabel.text = TEXT.OUR_MENU
      case 1:
         cell.topImageButton.setImage(#imageLiteral(resourceName: "Locateus"), for: .normal)
         cell.bottomLabel.text = TEXT.LOCATE_US
      default:
         cell.topImageButton.setImage(#imageLiteral(resourceName: "messageRedIcon"), for: .normal)
         cell.bottomLabel.text = TEXT.CONTACT_US
         cell.rightLine.isHidden = true
      }
      
      return cell
   }
   
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      switch indexPath.item {
      case 0:
         self.sdkCalling()
      case 1:
         self.openMapForPlace()
         break
      default:
         if Vendor.current == nil {
            FuguConfig.shared = FuguConfig.init(fullName: "" , email: "", phoneNumber: "", userUniqueKey: "", deviceToken: APIManager.sharedInstance.getDeviceToken())
         } else {
            FuguConfig.shared = FuguConfig.init(fullName: Vendor.current!.firstName! + " " + Vendor.current!.lastName! , email: Vendor.current!.email!, phoneNumber: Vendor.current!.phoneNo, userUniqueKey: "\(Vendor.current!.id)", deviceToken: APIManager.sharedInstance.getDeviceToken())
            FuguConfig.shared.appSecretKey = "\(AppConfiguration.current.FuguToken)"
         }
         FuguConfig.shared.appSecretKey = "\(AppConfiguration.current.FuguToken)"
         FuguConfig.shared.registerUserDetails()
         customizeFugu()
         FuguConfig.shared.pushChatsViewController(self.navigationController!)
         Singleton.sharedInstance.enableDisableIQKeyboard(enable: false)
      }
   }
   
   func openMapForPlace() {
      
      let lat = 30.7188978
      let lon = 76.81029809999995
      if (UIApplication.shared.canOpenURL(NSURL(string:"https://maps.google.com")! as URL ))
      {
         UIApplication.shared.openURL(NSURL(string: "https://maps.google.com/?q=@\(lat),\(lon)")! as URL)
      }
   }
   
   func sdkCalling() {
    Singleton.sharedInstance.enableDisableIQKeyboard(enable: true)
      StoreFrontManager.shared.startLoadingBlock = { (action) in
         ActivityIndicator.sharedInstance.showActivityIndicator()
      }
      
      StoreFrontManager.shared.stopLoadingBlock = { (action) in
         ActivityIndicator.sharedInstance.hideActivityIndicator()
      }
      StoreFrontManager.shared.changeAppDeviceType(deviceToken: APP_DEVICE_TYPE)
      let standardNav = self.navigationController
      StoreFrontManager.shared.completionHandler = {[weak self] (createTask) in
         self?.navigationController?.popToRootViewController(animated: false)
         if let vc = UIStoryboard(name: STORYBOARD_NAME.demo, bundle: Bundle.main).instantiateViewController(withIdentifier: STORYBOARD_ID.orderSuccessfullVC) as? OrderSuccessfullVC {
            
            Singleton.sharedInstance.createTaskDetail.paramForDemoTask = createTask
            Singleton.sharedInstance.pendingTaskFromDemo = true
            standardNav?.pushViewController(vc, animated: false)
            print(createTask)
         }
         
      }
    if Vendor.current == nil {
        
        ActivityIndicator.sharedInstance.showActivityIndicator()
        FormDetails.fetchOfferingsFromServerForDemo { (isSucceeded, error) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            guard isSucceeded == true else {
                print("OFFRINGS")
                return
            }
            StoreFrontManager.shared.setCompanyName(name: Singleton.sharedInstance.formDetailsInfo.formName)
            StoreFrontManager.shared.checkAuthentication(type:.catalogue, navigationController: self.navigationController!,isDemo:true) { (isAuthorized, response) in
                
                if isAuthorized == true {
                    
                } else {
                    print(response)
                }
            }
        }
    }else{
       
                    StoreFrontManager.shared.changeAppDeviceType(deviceToken:APP_DEVICE_TYPE)
                    StoreFrontManager.shared.completionHandler = nil
                    StoreFrontManager.shared.setCompanyName(name: Singleton.sharedInstance.formDetailsInfo.formName)
                    StoreFrontManager.shared.checkAuthentication(type: .catalogue, navigationController: self.navigationController!) { (isAuthorized, response) in

                        if isAuthorized == true {

                        } else {
                            print(response)
                        }
                    }
                }
    
    
   }
   
   //MARK:- Interating FuguChat
   private func customizeFugu(){
      FuguConfig.shared.backButtonImageIcon = #imageLiteral(resourceName: "iconBackTitleBar")
      FuguConfig.shared.themColor = COLOR.THEME_FOREGROUND_COLOR
      FuguConfig.shared.navigationTitleColor = COLOR.SPLASH_TEXT_COLOR
      FuguConfig.shared.navigationBackgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      FuguConfig.shared.navigationHeaderViewBackgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
   }
}
