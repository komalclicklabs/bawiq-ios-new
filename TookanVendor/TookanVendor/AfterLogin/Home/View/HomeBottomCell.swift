//
//  HomeBottomCell.swift
//  TookanVendor
//
//  Created by CL-Macmini-110 on 10/12/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class HomeBottomCell: UICollectionViewCell {

   //MARK:- Outlets
   @IBOutlet var topImageButton: UIButton!
   @IBOutlet var bottomLabel: UILabel!
   @IBOutlet var rightLine: UIView!
   
   //MARK:- Life Cycle
   override func awakeFromNib() {
      super.awakeFromNib()
      self.topImageButton.isUserInteractionEnabled = false
      self.rightLine.backgroundColor = COLOR.LINE_COLOR
      self.bottomLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
      self.bottomLabel.textColor = COLOR.TEXT_LIGHT_COLOR
    }

}
