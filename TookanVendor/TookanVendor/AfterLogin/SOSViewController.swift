//
//  SOSViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 01/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class SOSViewController: UIViewController {

   // MARK: - Properties
   private var isSosEnabled: Bool {
      return Vendor.current!.isSosEnabled
   }
   
   private var timer: Timer?
   private var timeRemaining = 5
   
   private let retryAfterTime: TimeInterval = 5
   private var retriesLeftInCaseOfApiHitFailure = 5
   
   var task: OrderDetails?

   // MARK: - IBOutlets
   @IBOutlet weak var blurView: UIVisualEffectView!
   
   @IBOutlet weak var backButton: UIButton!
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var timerView: UIView!
   @IBOutlet weak var timerLabel: UILabel!
   @IBOutlet weak var descriptionLabel: UILabel!
   @IBOutlet weak var requestSentLabel: UILabel!
   @IBOutlet weak var cancelButton: UIButton!
   @IBOutlet weak var cancelLabel: UILabel!

   @IBOutlet weak var emergencyImage: UIImageView!
   @IBOutlet weak var disableEmergencyModeButton: UIButton!
   
   @IBOutlet weak var bottomDistanceDisableButton: NSLayoutConstraint!
   

   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      
      configureViews()
      setViewToIntialState()
      
      if !isSosEnabled {
         startTimer()
      }
   }
   
   private func setViewToIntialState() {
      timerLabel.text = timeRemaining.description
      
      emergencyImage.isHidden = !isSosEnabled
      descriptionLabel.isHidden = isSosEnabled
      requestSentLabel.isHidden = !isSosEnabled
      timerView.isHidden = isSosEnabled
      cancelButton.isHidden = isSosEnabled
      cancelLabel.isHidden = isSosEnabled
      
      bottomDistanceDisableButton.constant = isSosEnabled ? 0 : -disableEmergencyModeButton.frame.height
   }

   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      UIApplication.shared.isStatusBarHidden = true
   }

   override func viewWillDisappear(_ animated: Bool) {
      super.viewWillDisappear(animated)
      removeTimer()
      UIApplication.shared.isStatusBarHidden = false
   }
   
   // MARK: - Configure Views
   private func configureViews() {
      configureTitleLabel()
      configureTimerLabel()
      configureDescriptionLabel()
      configureCancelLabel()
      configureDisableModeButton()
      configureRequestSentLabel()
   }
   
   private func configureTitleLabel() {
      titleLabel.configureHeadingTypeLabelWith(text: TEXT.EMERGENCY_SOS)
      titleLabel.textColor = UIColor.white
   }
   private func configureTimerLabel() {
      timerLabel.font = UIFont.init(name: FONT.regular, size: 65)
   }
   
   private func configureDescriptionLabel() {
      descriptionLabel.configureDescriptionTypeLabelWith(text: TEXT.EMERGENCY_NOTIFY_ADMIN + " " + APP_NAME)
      descriptionLabel.textColor = UIColor.white
   }
   
   private func configureCancelLabel() {
      cancelLabel.font = UIFont(name: FONT.light, size: 14)
   }
   
   private func configureDisableModeButton() {
      disableEmergencyModeButton.setTitle(TEXT.DISABLE_EMERGENECY, for: .normal)
      disableEmergencyModeButton.titleLabel?.font = UIFont(name: FONT.regular, size: 16)
   }
   
   private func configureRequestSentLabel() {
      self.requestSentLabel.configureDescriptionTypeLabelWith(text: TEXT.EMERGENCY_REQUEST_SENT)
      self.requestSentLabel.font = self.requestSentLabel.font.withSize(30)
      self.requestSentLabel.textColor = UIColor.white
   }

   
   // MARK: - Timer
   private func startTimer() {
      timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(handleTimerFiring(_:)), userInfo: nil, repeats: true)
   }

   @objc private func handleTimerFiring(_ timer: Timer) {
      timeRemaining -= 1
      timerLabel.text = timeRemaining.description
      if timeRemaining == 0 {
         remainigTimeFinished()
        logEvent(label: "sos_enabled")
      }
   }

   private func remainigTimeFinished() {
      removeTimer()
      sendSosHit()
   }

   private func removeTimer() {
      timer?.invalidate()
      timer = nil
   }
   
   private func startRetryTimer() {
    descriptionLabel.text = "Retrying".localized + "..."
      timer = Timer.scheduledTimer(timeInterval: retryAfterTime, target: self, selector: #selector(handleRetries), userInfo: nil, repeats: false)
   }
   
   @objc private func handleRetries() {
      guard retriesLeftInCaseOfApiHitFailure > 0 else {
         retriesEnded()
         return
      }
      retriesLeftInCaseOfApiHitFailure -= 1
      sendSosHit()
   }
   
   private func retriesEnded() {
      removeTimer()
      descriptionLabel.text = "Failed to contact SOS service."
   }
   

   // MARK: - IBaction
   @IBAction func backButtonPressed() {
      dismiss(animated: true, completion: nil)
   }
   
   @IBAction func cancelButtonPressed(_ sender: Any) {
      dismiss(animated: true, completion: nil)
   }

   @IBAction func disableEmergencyModePressed(_ sender: Any) {
      Vendor.current!.isSosEnabled = false
      dismiss(animated: true, completion: nil)
    logEvent(label: "sos_disabled")
   }

   // MARK: - Methods
   
   private func sendSosHit() {
      task?.sendSosCall { [weak self] success in
         if success {
            self?.sosHitSuccessful()
         } else {
            self?.sosHitFailed()
         }
      }
   }
   
   private func sosHitSuccessful() {
      removeTimer()
      Vendor.current!.isSosEnabled = true
      animateEmergencyImageToForground()
      cancelButton.hideSmoothly(completion: nil)
      cancelLabel.hideSmoothly(completion: nil)
      animateDisableButtonOnScreen()
      animateRequestlabelToForground()
   }
   
   private func sosHitFailed() {
      removeTimer()
      startRetryTimer()
   }
   
   private func animateEmergencyImageToForground() {
      
      UIView.animate(withDuration: 1) {
         self.timerView.isHidden = true
         self.timerLabel.isHidden = true
         self.emergencyImage.isHidden = false
      }

   }
   
   private func animateRequestlabelToForground() {
         self.requestSentLabel.isHidden = false
         self.descriptionLabel.isHidden = true
   }
   
   private func animateDisableButtonOnScreen() {
      self.bottomDistanceDisableButton.constant = 0

      UIView.animate(withDuration: 0.5) {
         self.view.layoutIfNeeded()
      }
   }
   
   deinit {
      print("SOS view controller deintialized")
   }
   
   // MARK: - Round Animation 
//   var circlePathLayer = CAShapeLayer()
//   var circleRadius: CGFloat = 64
//   
//   func configure() {
//      circlePathLayer.lineWidth = 2
//      circlePathLayer.fillColor = UIColor.clear.cgColor
//      circlePathLayer.strokeColor = UIColor.white.cgColor
//      circlePathLayer.frame = timerView.bounds
//      
//      timerView.layer.addSublayer(circlePathLayer)
//   }
//   
//   func getPath() -> UIBezierPath {
//      let path = UIBezierPath(ovalIn: timerView.bounds)
//      return path
//   }
//   
//   override func viewDidLayoutSubviews() {
//      super.viewDidLayoutSubviews()
//      circlePathLayer.path = getPath().cgPath
//   circlePathLayer.frame = timerView.bounds
//   }
   
   // MARK: - Navigation
   class func presentOn(viewController: UIViewController, withTask task: OrderDetails) {
      let sosVC = findIn(storyboard: .taxi, withIdentifier: STORYBOARD_ID.sos) as! SOSViewController
      sosVC.task = task
      viewController.present(sosVC, animated: true, completion: nil)
   }

   
   
}
