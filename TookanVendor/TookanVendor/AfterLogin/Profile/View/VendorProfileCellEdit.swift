//
//  VendorProfileCellEdit.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 08/08/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit


protocol ProfileFieldChanged {
    func profileValueChanged(fieldType:Int,yourText:String)
}


class VendorProfileCellEdit: UITableViewCell,UITextFieldDelegate {

    
    @IBOutlet weak var buttonWidth: NSLayoutConstraint!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var showPasswordButton: UIButton!
    @IBOutlet weak var bottomLine: UILabel!
    
    var isEditting =  false
    var delegate : ProfileFieldChanged!
    var fieldType = Int()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textField.delegate = self
        showPasswordButton.setImage(#imageLiteral(resourceName: "eyeInactive"), for: UIControlState.normal)
        showPasswordButton.setImage(#imageLiteral(resourceName: "eyeActive"), for: UIControlState.selected)
        buttonWidth.constant = 0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setUiAttributes(){
        label.font = UIFont(name: FONT.light, size: FONT_SIZE.smallest)
        label.textColor = UIColor(colorLiteralRed: 51/255, green: 51/255, blue: 51/255, alpha: 0.5)
        textField.font = UIFont(name: FONT.light, size: FONT_SIZE.priceFontSize)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    
    func setCellInfo(fieldType:FIELD_TYPE,row:Int,detailModel:ProfileDetails){
        
        self.fieldType = fieldType.hashValue
        setUiAttributes()
        self.textField.isSecureTextEntry = false
        self.textField.isUserInteractionEnabled = self.isEditting
        self.textField.textColor = COLOR.SPLASH_TEXT_COLOR
        self.bottomLine.isHidden = !isEditting
        switch fieldType {
            
            
        case .name:
            self.textField.text = detailModel.fullName.trimText
            label.text = TEXT.NAME
            setForName()
            self.textField.attributedPlaceholder = NSAttributedString(string: TEXT.NAME,
                                                                      attributes: [NSForegroundColorAttributeName:COLOR.SPLASH_TEXT_COLOR])
            buttonWidth.constant = 0
        case .email:
            self.textField.text = detailModel.email
            setForEmail()
            self.textField.isUserInteractionEnabled = false
            if self.isEditting == true{
                self.textField.textColor = self.label.textColor
            }else{
                
            }
            self.textField.attributedPlaceholder = NSAttributedString(string: TEXT.EMAIL,
                                                                      attributes: [NSForegroundColorAttributeName: COLOR.SPLASH_TEXT_COLOR])
            buttonWidth.constant = 0
            label.text = TEXT.EMAIL
        case .contact:
            //self.cellField.setViewForPhoneOrNot()
            self.textField.text = detailModel.phoneNumber
            label.text = TEXT.PHONE
            setUpForNumber()
            self.textField.attributedPlaceholder = NSAttributedString(string: TEXT.PHONE,
                                                                      attributes: [NSForegroundColorAttributeName: COLOR.SPLASH_TEXT_COLOR])
            buttonWidth.constant = 0
            
        case .password:
            
            self.textField.text =  "asdasdasdas"
            self.textField.isSecureTextEntry = true
            
            
            label.text = TEXT.PASSWORD
            buttonWidth.constant = 0
        case .changepassword:
            self.textField.text = detailModel.oldPassWord
            self.label.text = TEXT.CHANGE_OLD_PASS_PLACEHOLDER
            self.textField.isSecureTextEntry = true
             self.textField.attributedPlaceholder = NSAttributedString(string: TEXT.CHANGE_OLD_PASS_PLACEHOLDER,
                                                                   attributes: [NSForegroundColorAttributeName: COLOR.SPLASH_TEXT_COLOR])
            buttonWidth.constant = 30
        
        case .newPassword:
            self.textField.text = detailModel.newPassWord
            self.label.text = "New Password"
            self.textField.isSecureTextEntry = true
             self.textField.attributedPlaceholder = NSAttributedString(string: "New Password",
                                                                   attributes: [NSForegroundColorAttributeName: COLOR.SPLASH_TEXT_COLOR])
            buttonWidth.constant = 30
            
        default:
            print("")
            
        }

        
        
        
        
    }
    
    @IBAction func showPassWordAction(_ sender: UIButton) {
        
        self.textField.isSecureTextEntry = sender.isSelected
        sender.isSelected   = !sender.isSelected
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (self.fieldType == FIELD_TYPE.email.hashValue || self.fieldType == FIELD_TYPE.contact.hashValue)  && string == " "{
            return false
        }
        var textToSend = String()
        if string != ""{
            textToSend = textField.text! + string
        }else{
            textToSend = textField.text!
            textToSend.remove(at:textToSend.index(before:  textToSend.endIndex))
        }
        
        return true
    }
    
    
    
    func setUpForNumber(){
        self.textField.keyboardType = .numberPad
        self.textField.autocapitalizationType = UITextAutocapitalizationType.none
    }
    
    func setForEmail(){
        self.textField.keyboardType = .emailAddress
        self.textField.autocapitalizationType = UITextAutocapitalizationType.none
    }
    
    func setForName(){
        self.textField.keyboardType = .alphabet
        self.textField.autocapitalizationType = UITextAutocapitalizationType.words
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.delegate.profileValueChanged(fieldType: fieldType, yourText: textField.text!)
    }
    
    
    
}
