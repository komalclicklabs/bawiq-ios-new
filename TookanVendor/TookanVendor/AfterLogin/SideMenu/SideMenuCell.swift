//
//  SideMenuCell.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 12/14/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {
   
   //MARK: - IBOutlets
   @IBOutlet weak var cellBackGroundView: UIView!
   @IBOutlet weak var bottomLineTrailingConstraint: NSLayoutConstraint!
   @IBOutlet weak var bottomLine: UIView!
   @IBOutlet weak var rightImage: UIImageView!
   @IBOutlet weak var menuText: UILabel!
   
   //MARK: - View Life Cycle
   override func awakeFromNib() {
      super.awakeFromNib()
      
      configureViews()
      self.selectionStyle = .none
   }
   
   func configureViews() {
      cellBackGroundView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      bottomLine.backgroundColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.1)
      self.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      configureTitleLabel()
      configureRightImage()
   }
   
   func configureTitleLabel() {
      menuText.configureDescriptionTypeLabelWith(text: "")
   }
   
   func configureRightImage() {
      rightImage.tintColor = COLOR.SPLASH_TEXT_COLOR
   }
   
   override func setHighlighted(_ highlighted: Bool, animated: Bool) {
      
      if highlighted == true {
         self.cellBackGroundView.backgroundColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.1)
      } else {
         self.cellBackGroundView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      }
   }
   
   func setCellWith(title: String, icon: UIImage) {
      menuText.text = title
      rightImage.image = icon.renderWithAlwaysTemplateMode()
   }
   
   
}
