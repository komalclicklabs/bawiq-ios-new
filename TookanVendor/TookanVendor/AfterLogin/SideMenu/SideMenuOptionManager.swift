//
//  SideMenuController.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 01/09/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

class SideMenuOptionManager {
   
   // MARK: - Properties
   private(set) var sideMenuOptions: [SideMenuOption]
   
   // MARK: - Intializer
   init() {
      sideMenuOptions = []
      generateSideMenuOptions()
   }
   
   
   private func generateSideMenuOptions() {
      
      appendHomeOption()
      
      if Vendor.current == nil {
         appendPaymentOption()
      }
      
      if shouldShowPaymentsOption() {
         appendPaymentOption()
      }
      
      appendOrderOption()
      
      if shouldShowReferralsOption() {
         appendReferralsOption()
      }
      
      appendProfileOption()
      if shouldShowChatOption() {
         appendChatWithSupportOption()
      }
   //   appendLogoutOption()
   }
   
   private func appendHomeOption() {
      let home = SideMenuOption(title: TEXT.home, image: #imageLiteral(resourceName: "home"), type: .home)
      sideMenuOptions.append(home)
   }
   
   private func shouldShowPaymentsOption() -> Bool {
      return Singleton.sharedInstance.formDetailsInfo.toShowPayment && Singleton.sharedInstance.formDetailsInfo.paymentMethodArray.count > 1 
   }
   
   private func appendPaymentOption() {
      let paymentOption = SideMenuOption(title: TEXT.payments, image: #imageLiteral(resourceName: "payments"), type: .payments)
      sideMenuOptions.append(paymentOption)
   }
   
   private func appendOrderOption() {
      //let orderString = Singleton.sharedInstance.formDetailsInfo.call_tasks_as
      let orderOption = SideMenuOption(title: TEXT.ORDERS, image: #imageLiteral(resourceName: "orders"), type: .orders)
      sideMenuOptions.append(orderOption)
   }
   
   private func shouldShowReferralsOption() -> Bool {
      return AppConfiguration.current.isReferralRequired
   }
   
   private func appendReferralsOption() {
      let referralsOption = SideMenuOption(title: TEXT.referrals, image: #imageLiteral(resourceName: "referrals"), type: .referrals)
      sideMenuOptions.append(referralsOption)
   }
   
   private func appendProfileOption() {
      let profileOption = SideMenuOption(title: TEXT.profile, image: #imageLiteral(resourceName: "profile"), type: .profile)
      sideMenuOptions.append(profileOption)
   }
   
   private func shouldShowChatOption() -> Bool {
      return AppConfiguration.current.isFuguEnabled
   }
   
   private func appendChatWithSupportOption() {
      let chatText = TEXT.CHAT_SUPPORT//TEXT.chatWith + " " + APP_NAME
      let chatWithSupportOption = SideMenuOption(title: chatText, image: #imageLiteral(resourceName: "chatWithTookan"), type: .chatWithSupport)
      sideMenuOptions.append(chatWithSupportOption)
   }
   
   private func appendLogoutOption() {
      let logoutOption = SideMenuOption(title: TEXT.LOGOUT_BUTTON, image: #imageLiteral(resourceName: "logout"), type: .logout)
      sideMenuOptions.append(logoutOption)
   }
   
   // MARK: -
   
}
