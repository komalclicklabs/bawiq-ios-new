//
//  SideMenuOption.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 01/09/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

struct SideMenuOption {
   let title: String
   let image: UIImage
   let type: SideMenuOptionType
}

enum SideMenuOptionType {
   case home
   case payments
   case orders
   case referrals
   case profile
   case chatWithSupport
   case logout
}
