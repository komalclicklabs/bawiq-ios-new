//
//  DriverSelectTableViewCell.swift
//  TookanVendor
//
//  Created by Aditi on 7/19/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import Kingfisher

class SelectAgentTableViewCell: UITableViewCell {
   
   //MARK : IBOutlets
   @IBOutlet weak var imageDriver: UIImageView!
   @IBOutlet weak var titleLabel: UILabel!
   @IBOutlet weak var descriptionLabel: UILabel!
   @IBOutlet weak var imageSelect: UIImageView!
   
   
   // MARK: - View Life Cycle 
   override func awakeFromNib() {
      super.awakeFromNib()
      
      configureViews()
   }
   
   private func configureViews() {
      backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      
      titleLabel.font = UIFont(name: FONT.regular, size: 16)
      titleLabel.textColor = COLOR.SPLASH_TEXT_COLOR
      
      descriptionLabel.font = UIFont(name: FONT.light, size: 13)
      descriptionLabel.textColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.6)
   }
   
   // MARK: - Methods
   func setCellWith(imageUrl: URL?, title: String, description: String) {
      imageDriver.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "placeHolder"))
      titleLabel.text = title
      descriptionLabel.text = description
   }
   
}
