//
//  DriverSelectViewController.swift
//  TookanVendor
//
//  Created by Aditi on 7/19/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
class SelectAgentViewController: UIViewController {
   
   var arrayOfAgents = [Agent]()
   var selectedindex : Int = -1
   
   let callAgentAs = Singleton.sharedInstance.formDetailsInfo.callServiceProvidersAs
   private var titleString: String {
      return TEXT.select + " " + callAgentAs
   }

   var tags = ""
   var coordinates: (latitude: String, longitude: String)!
   var formId = ""
   
   var agentSuccessfullySelected: (() -> Void)?
   var navBar: NavigationView?
   
   //MARK : IBOutlets
   @IBOutlet weak var continueButton: UIButton!
   @IBOutlet weak var tableView: UITableView!
   @IBOutlet weak var errorLabel: UILabel!
   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()

      setViewsToIntialState()
      configureViews()
      setTableViewDataSource()
      fetchAgentsFromServer()
   }
   
   fileprivate func setViewsToIntialState() {
      errorLabel.isHidden = true
      tableView.isHidden = true
      continueButton.isHidden = true
   }
   
   fileprivate func setTableViewDataSource() {
      self.tableView.delegate = self
      self.tableView.dataSource = self
      tableView.registerCellWith(nibName: NIB_NAME.SelectAgentTableViewCell, reuseIdentifier: CELL_IDENTIFIER.SelectAgentCell)
   }
   
   fileprivate func configureViews() {
      view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      configureTableView()
      configureErrorLabel()
      configureContinueButton()
      setNavigationBar()
   }
   
   fileprivate func configureTableView() {
      tableView.tableFooterView = UIView()
      tableView.estimatedRowHeight = 65
      tableView.rowHeight = UITableViewAutomaticDimension
   }
   
   fileprivate func configureErrorLabel() {
      let errorMessage = TEXT.NO + " " + callAgentAs + " " + TEXT.FOUND
      self.errorLabel.configureDescriptionTypeLabelWith(text: errorMessage)
   }
   
   fileprivate func configureContinueButton() {
      continueButton.configureNormalButtonWith(title: TEXT.CONTINUE)
      continueButton.layer.cornerRadius = 0
   }
   
   //MARK : Button Actions
   @IBAction func continueButtonPressed(_ sender: Any) {
      guard arrayOfAgents.count != 0 else {
         fetchAgentsFromServer()
         return
      }
      
      guard selectedindex >= 0 else {
         let errorMessage = TEXT.PLEASE.capitalized  + " " + TEXT.selectA.lowercased() + " " + callAgentAs.lowercased() + " " + TEXT.toContinue
         Singleton.sharedInstance.showAlert(errorMessage)
         return
      }
      
      Singleton.sharedInstance.createTaskDetail.fleetId = arrayOfAgents[selectedindex].fleet_id
      
      agentSuccessfullySelected?()
//      DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.6) { [weak self]
//         self.navigationController?.popViewController(animated: true)
//      }
      
   }
   
   //MARK : Methods
   private func setNavigationBar() {
      let navParams = NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: titleString, leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"), rightButtonImage: nil)
      
      navBar = NavigationView.getNibFile(params: navParams, leftButtonAction: {
         _ = self.navigationController?.popViewController(animated: true)
      }, rightButtonAction: nil)
      //   navBar?.backButton.isHidden = isBackButtonHidden
      view.addSubview(navBar!)
   }
   
   func fetchAgentsFromServer() {
      Agent.fetchAgentsFromServer(formID: formId, latitude: coordinates.latitude.description, longitude: coordinates.longitude.description, tags: tags,completion: { [weak self] (success, agents) in
         self?.continueButton.isHidden = false
         
         guard success, let tempAgents = agents, tempAgents.count > 0 else {
            self?.tableView.isHidden = true
            self?.errorLabel.isHidden = false
            self?.continueButton.setTitle(TEXT.RETRY, for: .normal)
            return
         }
         
         self?.tableView.isHidden = false
         self?.continueButton.setTitle(TEXT.CONTINUE, for: .normal)
         self?.arrayOfAgents = tempAgents
         self?.tableView.reloadData()
      })
   }
   
   // MARK: - Navigation
   class func pushIn(navVC: UINavigationController, withTags tags: String, formId: String, latitude: String, longitude: String, successfullySelected: @escaping () -> Void) {
      let vc = findIn(storyboard: .afterLogin, withIdentifier: "SelectAgentViewController") as! SelectAgentViewController
      vc.tags = tags
      vc.formId = formId
      vc.coordinates = (latitude, longitude)
      vc.agentSuccessfullySelected = successfullySelected
      navVC.pushViewController(vc, animated: true)
   }
}

//MARK : UITableViewDelegate, UITableViewDataSource
extension SelectAgentViewController : UITableViewDelegate, UITableViewDataSource {
   
   func numberOfSections(in tableView: UITableView) -> Int {
      return 1
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return arrayOfAgents.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
      let cell = self.tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.SelectAgentCell, for: indexPath) as! SelectAgentTableViewCell
      let agent = arrayOfAgents[indexPath.row]
      
      let image = indexPath.row == selectedindex ? #imageLiteral(resourceName: "greenTick.png") : #imageLiteral(resourceName: "optionEmpty.png")
      cell.imageSelect.image = image
      
      let imageUrl = URL(string: agent.fleet_image)
      cell.setCellWith(imageUrl: imageUrl, title: agent.username, description: agent.tags)
      
      return cell
      
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
      selectedindex = indexPath.row
      tableView.reloadData()
   }
   
   
}



