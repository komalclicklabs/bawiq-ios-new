//
//  FavouriteAddress.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 14/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

enum favLocationType: String {
   case Home
   case Work
   case Other

   fileprivate func getFavLocationCategorizationType() -> Int {
      switch self {
      case .Home:
         return 0
      case .Work:
         return 1
      case .Other:
         return 2
      }
   }
   
   init(categorizationType: Int) {
      switch categorizationType {
      case 0:
         self = .Home
      case 1:
         self = .Work
      default:
         self = .Other
      }
   }
}

struct FavouriteAddress {
   
   private(set) var streetAddress = String()
   var favId = String()
   private(set) var latitude = String()
   private(set) var longitude = String()
   var placeId:String?
   
   var type: favLocationType
   var landmark = ""
   var flatNo = ""
   var label = ""
   
   var address: String {
      var tempAddress = ""
      
      if !flatNo.isEmpty {
         tempAddress += flatNo + ", "
      }
      if !landmark.isEmpty {
         tempAddress += landmark + ", "
      }
      tempAddress += streetAddress
      
      return tempAddress
   }
   
   init(json: [String: Any]) {
      if let value = json["address"] as? String {
         self.streetAddress = value
      }
      
      if let value = json["latitude"] as? String {
         self.latitude = value
      }else if let value = json["latitude"] as? Double {
         self.latitude = "\(value)"
      }
      
      if let value = json["longitude"] as? String {
         self.longitude = value
      }else if let value = json["longitude"] as? Double {
         self.longitude = "\(value)"
      }
      
      if let value = json["fav_id"] {
         self.favId = "\(value)"
      }
      
      if let value = json["name"] as? String {
         self.label = value
      }
      
      if let locationType = favLocationType(rawValue: self.label) {
         self.type = locationType
      } else {
         self.type = .Other
      }
      
      if let placeIdvalue = json["placeId"] as? String {
         self.placeId = placeIdvalue
      }
      
      self.flatNo = (json["house"] as? String) ?? ""
      self.landmark = (json["landmark"] as? String) ?? ""
   }
   
   init() {
   type = .Other
   }
   
   init(label: String, address: String, houseNo: String, landmark: String, lattitude: String, longitude: String, type: favLocationType, favId: String) {
      self.label = label
      self.streetAddress = address
      self.flatNo = houseNo
      self.landmark = landmark
      self.latitude = lattitude
      self.longitude = longitude
      self.type = type
      self.favId = favId
   }
   
   // MARK: - Methods
   func deleteFromServer(completion: @escaping (_ success: Bool) -> Void) {
      let params = getParamsToDeleteLocation()
       
      HTTPClient.makeConcurrentConnectionWith(method: .POST, para: params, extendedUrl: API_NAME.deleteFavLocation) { (responseObject, error, _, statusCode) in
         
         guard statusCode == .some(STATUS_CODES.SHOW_DATA) else {
            completion(false)
            return
         }
         
         completion(true)
      }
   }
   
   private func getParamsToDeleteLocation() -> [String: String] {
      return [
        "access_token": Vendor.current!.appAccessToken!,
        "fav_id": favId,
        "app_device_type": APP_DEVICE_TYPE,
        "reference_id": Vendor.current!.referenceId,
        "user_id" : "\(Singleton.sharedInstance.formDetailsInfo.user_id!)",
        "form_id" : Singleton.sharedInstance.formDetailsInfo.form_id!
      ]
   }
   
   func addAddressToFavLocations(completion: @escaping (_ success: Bool) -> Void) {
      
      let params = getParamsForSavingAddress()
      
      HTTPClient.makeConcurrentConnectionWith(method: .POST, para: params, extendedUrl: API_NAME.addFavLocation) { (responseObject, error, extendedUrl, statusCode) in
         
         guard error == nil,
            statusCode == .some(STATUS_CODES.SHOW_DATA) else {
               completion(false)
               return
         }
         completion(true)
      }
   }
   
   private func getParamsForSavingAddress() -> [String: Any] {
      let typename = type == .Other ? label : "\(type)"
      
      let param: [String: Any]  = [
        "access_token": Vendor.current!.appAccessToken!,
        "app_access_token": Vendor.current!.appAccessToken!,
        "address": streetAddress,
        "latitude": latitude,
        "longitude": longitude,
        "app_device_type": APP_DEVICE_TYPE,
        "name": typename,
        "house": flatNo,
        "landmark": landmark,
        "app_version": APP_VERSION,
        "loc_type": type.getFavLocationCategorizationType().description,
        "reference_id": Vendor.current!.referenceId,
        "user_id" : Singleton.sharedInstance.formDetailsInfo.user_id!,
        "form_id" : Singleton.sharedInstance.formDetailsInfo.form_id!
//         "loc_type": type.getFavLocationCategorizationType()
      ]
      
      return param
   }
   
   func sendEditionOfAddressToServer(completion: Completion) {
      let params = getParamsToEditAddress()
      
      
      HTTPClient.makeConcurrentConnectionWith(method: .POST, para: params, extendedUrl: API_NAME.editFavLocation) {  (_, error, _, statusCode) in
         guard error == nil,
            statusCode == .some(STATUS_CODES.SHOW_DATA) else {
               completion?(false)
               return
         }
         completion?(true)
      }
   }
   
   private func getParamsToEditAddress() -> [String: Any] {
      
      let typename = type == .Other ? label : "\(type)"
      
      let editBody: [String: Any] = [
        "address": streetAddress,
        "name": typename,
        "house": flatNo,
        "landmark": landmark,
        "longitude": longitude,
        "latitude": latitude,
        "loc_type": type.getFavLocationCategorizationType().description
//         "loc_type": type.getFavLocationCategorizationType()
      ]
      
      let params: [String: Any] = [
        "access_token": Vendor.current!.appAccessToken!,
        "app_access_token": Vendor.current!.appAccessToken!,
        "app_device_type": APP_DEVICE_TYPE,
        "user_id": Vendor.current!.userId!,
        "fav_id": favId,
        "edit_body": editBody,
        "reference_id": Vendor.current!.referenceId,
        "form_id" : Singleton.sharedInstance.formDetailsInfo.form_id!
      ]
      
      return params
      
   }
   
   // MARK: - Type Methods
   static func fetchAllFromServer(completion: @escaping (_ success: Bool, _ home: [FavouriteAddress], _ work: [FavouriteAddress], _ others: [FavouriteAddress]) -> Void) {
      
      let param = getParamsToFetchLocation()
      
      HTTPClient.makeConcurrentConnectionWith(method: .POST, para: param, extendedUrl: API_NAME.getFavouriteLocation) { (responseObject, error, _, statusCode) in
         
         var home = [FavouriteAddress]()
         var work = [FavouriteAddress]()
         var others = [FavouriteAddress]()
         
         guard statusCode == .some(STATUS_CODES.SHOW_DATA),
            let response = responseObject as? [String: Any],
            let data = response["data"] as? [String: Any],
            let rawFavLocations = data["favLocations"] as? [[String: Any]] else {
               completion(false, home, work, others)
               return
         }
         
         for rawLocation in rawFavLocations {
            let tempFavAddress = FavouriteAddress(json: rawLocation)
            
            switch tempFavAddress.type {
            case .Home:
               home.append(tempFavAddress)
            case .Work:
               work.append(tempFavAddress)
            case .Other:
               others.append(tempFavAddress)
            }
            
         }
         
         completion(true, home, work, others)
      }
      
   }
   
   private static func getParamsToFetchLocation() -> [String: Any] {
      return [
        "access_token":Vendor.current!.appAccessToken!,
        "app_device_type":APP_DEVICE_TYPE,
        "reference_id": Vendor.current!.referenceId,
        //"user_id" : Singleton.sharedInstance.formDetailsInfo.user_id!,
        //"form_id" : Singleton.sharedInstance.formDetailsInfo.form_id!
      ]
   }
}
