//
//  AddNewLocationViewController.swift
//  TookanVendor
//
//  Created by No One on 13/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import GooglePlaces

protocol FavouriteLocationManager: class  {
   func InFlowforSavingLocation() -> Bool
   func doesFavLocationForTypeAlreadyExist(type: favLocationType) -> Bool
   func getFavIdOf(loctionType: favLocationType) -> String?
   
   func addressSelectedWith(address: String, coordinate: CLLocationCoordinate2D)
   func newAddressSaved()
}

class AddNewLocationViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
   
   enum Section: Int {
      case searchResult = 0, home, work, other
      
      func getSectionHeaderHeight() -> CGFloat {
         switch  self {
         case .home:
            return 32
         default:
            return 0
         }
      }
      
      func getIcon() -> UIImage {
         switch self {
         case .home:
            return #imageLiteral(resourceName: "homeAddress").renderWithAlwaysTemplateMode()
         case .work:
            return #imageLiteral(resourceName: "workAddress").renderWithAlwaysTemplateMode()
         default:
            return #imageLiteral(resourceName: "otherAddress").renderWithAlwaysTemplateMode()
         }
      }
      
      func labelIfAddressNotAdded() -> String {
         switch self {
         case .home:
            return TEXT.HomeAddress
         case .other:
            return TEXT.addAnotherAddress
         case .work:
            return TEXT.WorkAddress
         case .searchResult:
            return ""
         }
      }
      
      func getLocationType() -> favLocationType {
         switch self {
         case .home:
            return .Home
         case .work:
            return .Work
         default:
            return .Other
         }
      }
   }
   
   // MARK: - Properties
   var searchResults = [FavouriteAddress]()
   
   var homeAddresses = [FavouriteAddress]()
   var workAdressess = [FavouriteAddress]()
   var otherAddressess = [FavouriteAddress]()
   
   let maximumNumberOfOtherAddress = 8
   
   fileprivate var isSelectingLocationForSaving = false
   
   //Completion handler for returning coordinates and address ot previous screen.
   var completionHandler:((_ coordinate:CLLocationCoordinate2D,_ address:String) -> Void)?
   
   //MARK: - IBOUTLETS
   @IBOutlet weak var errorLabel: UILabel!
   @IBOutlet weak var upperView: UIView!
   @IBOutlet weak var searchTextField: UITextField!
   @IBOutlet weak var backButton: UIButton!
   @IBOutlet weak var tableView: UITableView!
   @IBOutlet weak var selectFromMapButton: UIButton!

   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()

    self.tableView.isHidden = true
    
      searchTextField.backgroundColor = UIColor(colorLiteralRed: 246/255, green: 246/255, blue: 246/255, alpha: 1)
      searchTextField.setLeftPaddingPoints(10)
      searchTextField._cornerRadius = 2

      addShadowToView()
      
      searchTextField.placeholder = TEXT.searchYourAddress
      searchTextField.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
      
      tableView.separatorStyle = .none
      tableView.delegate = self
      tableView.dataSource = self
      searchTextField.delegate = self
      
      tableView.registerCellWith(nibName: NIB_NAME.addressListCell, reuseIdentifier: CELL_IDENTIFIER.addressCellIdentifier)
      tableView.registerCellWith(nibName: NIB_NAME.FavLocationCell, reuseIdentifier: CELL_IDENTIFIER.FavLocationCell)
      
      self.tableView.rowHeight = UITableViewAutomaticDimension
      self.tableView.estimatedRowHeight = 55
    
//      selectFromMapButton.configureNormalButtonWith(title: TEXT.selectFrommap)
    
      errorLabel.configureDescriptionTypeLabelWith(text: ERROR_MESSAGE.SEARCH_ADDRESS)
//    guard Vendor.current == nil else {
       // getAllSavedAddress()
//        return
//    }
    
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
//      searchTextField.becomeFirstResponder()
      
      UIApplication.shared.statusBarStyle = .default
      isSelectingLocationForSaving = false

   }
   
     //MARK: - UITABLE VIEW DATASOURCE
   func numberOfSections(in tableView: UITableView) -> Int {
      return 1
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
    return searchResults.count
    
//      switch Section(rawValue: section)! {
//      case .searchResult:
//         return searchResults.count
//      case .home:
//         return homeAddresses.count > 0 ? homeAddresses.count : 1
//      case .work:
//         return workAdressess.count > 0 ? workAdressess.count : 1
//      case .other:
//         return otherAddressess.count < maximumNumberOfOtherAddress ? otherAddressess.count + 1 : maximumNumberOfOtherAddress
//      }
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
    let addressCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.addressCellIdentifier) as! AddressListCell
    addressCell.selectionStyle = .none
    addressCell.addressTxtField.isHidden = true
    if indexPath.row < searchResults.count {
        addressCell.addressLabel.text = searchResults[indexPath.row].address
    }
    
    return addressCell
    
    
    //      let section = Section(rawValue: indexPath.section)!
//      switch section {
//      case .home, .work, .other:
//         return loadFavLocationCellFor(indexPath: indexPath)
//      case .searchResult:
//         let addressCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.addressCellIdentifier) as! AddressListCell
//         addressCell.selectionStyle = .none
//         if indexPath.row < searchResults.count {
//            addressCell.addressLabel.text = searchResults[indexPath.row].address
//         }
//
//         return addressCell
//      }
   }
   
   private func loadFavLocationCellFor(indexPath: IndexPath) -> FavLocationCell {
      let favLocationCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.FavLocationCell) as! FavLocationCell
      favLocationCell.selectionStyle = .none
      let section = Section(rawValue: indexPath.section)!
      
      favLocationCell.tag = indexPath.generateTag()
      let favAddress = getSavedAddressAt(section: indexPath.section, row: indexPath.row)
      
      favLocationCell.addButonAction = { [weak self] (type) in
         self?.addButtonPressedInCellWith(tag: favLocationCell.tag)
      }
      favLocationCell.showOptionsAction = { [weak self] (type) in
         self?.optionButtonPressedInCellWith(tag: favLocationCell.tag)
      }
      
      let icon = section.getIcon()
      let address = favAddress?.address ?? ""
      let label = favAddress?.label ?? section.labelIfAddressNotAdded()
      favLocationCell.setUpCellWith(icon: icon, address: address, label: label, isAddressSaved: favAddress != nil)
      
      return favLocationCell
   }
   
   private func getSavedAddressAt(section: Int, row: Int) -> FavouriteAddress? {
      let sectionName = Section(rawValue: section)!
      
      switch sectionName {
      case .home:
         return isHomeAddressSavedFor(row: row) ? homeAddresses[row] : nil
      case .work:
         return isWorkAdressSavedFor(row: row) ? workAdressess[row] : nil
      case .other:
         return isOtherAddressSavedFor(row: row) ? otherAddressess[row] : nil
      case .searchResult:
         return searchResults[row]
      }
   }
   
   fileprivate func isHomeAddressSavedFor(row: Int) -> Bool {
      return homeAddresses.count > row
   }
   
   fileprivate func isWorkAdressSavedFor(row: Int) -> Bool {
      return workAdressess.count > row
   }
   
   fileprivate func isOtherAddressSavedFor(row: Int) -> Bool {
      return otherAddressess.count > row
   }
   
   
   // MARK: - Table View Delegate
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
      let section = Section(rawValue: indexPath.section)!
      let row = indexPath.row
      
      switch section {
      case .searchResult:
         guard let placeid = searchResults[indexPath.row].placeId else {
            print("Place Id Not found")
            return
         }
         
         getPlaceDetailFrom(placeId: placeid, atRow: indexPath.row)
         return
      case .home:
         if isHomeAddressSavedFor(row: row) {
            let address = homeAddresses[row]
            let coordinate = getCoordinatedFor(address: address)
            completionHandler?(coordinate, address.address)
            self.navigationController?.popViewController(animated: true)
         } else {
            addButtonPressedInCellWith(tag: indexPath.generateTag())
         }
      case .work:
         if isWorkAdressSavedFor(row: row) {
            let address = workAdressess[row]
            let coordinate = getCoordinatedFor(address: address)
            completionHandler?(coordinate, address.address)
            self.navigationController?.popViewController(animated: true)

         } else {
            addButtonPressedInCellWith(tag: indexPath.generateTag())
         }
         
      case .other:
         if isOtherAddressSavedFor(row: row) {
            let address = otherAddressess[row]
            let coordinate = getCoordinatedFor(address: address)
            completionHandler?(coordinate, address.address)
            self.navigationController?.popViewController(animated: true)

         } else {
            addButtonPressedInCellWith(tag: indexPath.generateTag())
         }
      }
      
   }
   
   private func getCoordinatedFor(address: FavouriteAddress) -> CLLocationCoordinate2D {
      let latittude = Double(address.latitude)!
      let longitude = Double(address.longitude)!
      
      return CLLocationCoordinate2D(latitude: latittude, longitude: longitude)
   }
   
   private func getPlaceDetailFrom(placeId: String, atRow row: Int) {
      guard IJReachability.isConnectedToNetwork() else {
         ErrorView.showWith(message: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isErrorMessage: true, removed: nil)
         return
      }
      
      tableView.isUserInteractionEnabled = false
      GMSPlacesClient.shared().lookUpPlaceID(placeId) { [weak self] (place, error) in
         self?.tableView.isUserInteractionEnabled = true
         
         guard let placeUnwraaped = place, error == nil else {
            return
         }
         
         let address = self?.searchResults[row].address ?? ""
         
         self?.completionHandler?(placeUnwraaped.coordinate, address)
         self?.backButtonAction(UIButton())
      }

   }
   
   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      guard let sectionName = Section(rawValue: section), sectionName == .home else {
         return UIView()
      }
      
      let view = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: 32))
      
      let labelFrame = CGRect(x: 20, y: 0, width: view.bounds.width, height: view.bounds.height)
      let label = UILabel(frame: labelFrame)
      
      label.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
      label.text = TEXT.FavoritesText
      label.letterSpacing = 0.4
      label.backgroundColor = UIColor.clear
      view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      view.addSubview(label)
      
      return view
      
   }
   
   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      let sectionName = Section(rawValue: section)!
      
      return sectionName.getSectionHeaderHeight()
   }
   
   func scrollViewDidScroll(_ scrollView: UIScrollView) {
      self.searchTextField.resignFirstResponder()
   }
   
   //MARK: - UITEXTFIELD DELEGATE
   func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      var text = String()
      
      if string != ""{
         text = textField.text! + string
         
      }else{
         text = textField.text!
         text.remove(at: text.index(before:  text.endIndex))
      }
      print(text)
      
      APIManager.sharedInstance.autoCompleteSearch(text) { [weak self] (succeeded, response) in
         
         guard succeeded else {
            return
         }
         self?.searchResults.removeAll()
         if let arrayOfPredictions = response["predictions"] as? [[String: Any]] {
            for prediction in arrayOfPredictions {
               let response = [
                  "address": prediction["description"] as? String ?? "",
                  "placeId": prediction["place_id"] as? String ?? ""
               ]
               let gmsResult = FavouriteAddress(json: response)
               self?.searchResults.append(gmsResult)
            }
            self?.tableView.isHidden = false
            self?.tableView.reloadData()
         }
      }
      
      return true
   }
   
   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      textField.resignFirstResponder()
      return true
   }
   
   //MARK: - IBAction
   @IBAction func backButtonAction(_ sender: UIButton) {
      self.navigationController?.popViewController(animated: true)
   }
   
   @IBAction func selectFromMapButtonPressed(_ sender: UIButton) {
      guard !tableView.isHidden else {
         getAllSavedAddress()
         return
      }
      
      isSelectingLocationForSaving = false
      
      AddAddressFromMapViewController.pushViewControllerIn(navVC: self.navigationController!, manager: self, savingforType: .Other, locationToEdit: nil)
   }
   
   // MARK: - Methods
   func addShadowToView(){
      upperView.layer.shadowColor = UIColor.black.cgColor
      upperView.layer.shadowOpacity = 0.3
      upperView.layer.shadowOffset = CGSize(width: 0, height: 7)
      upperView.layer.shadowRadius = 6
   }
   
   func addButtonPressedInCellWith(tag: Int) {
      isSelectingLocationForSaving = true
      let sectionNumber = tag.getRowAndSection().section
      
      let section = Section(rawValue: sectionNumber)!
      let savingForType = section.getLocationType()
      
      AddAddressFromMapViewController.pushViewControllerIn(navVC: self.navigationController!, manager: self, savingforType: savingForType, locationToEdit: nil)
   }
   
   func optionButtonPressedInCellWith(tag: Int) {
      let (row, section) = tag.getRowAndSection()
      
      UIAlertController.showActionSheetWith(title: nil, message: nil, options: [TEXT.EDIT, TEXT.DELETE], actions: [ { [weak self] in
         self?.editAddressIn(section: section, atRow: row)
         }, { [weak self] in
            self?.deleteAddressIn(section: section, atRow: row)
         }
         ])
   }
   
   private func deleteAddressIn(section: Int, atRow row: Int) {
      let sectionName = Section(rawValue: section)!
      let location = getSavedAddressAt(section: section, row: row)
      
      location?.deleteFromServer { [weak self] success in
         guard success else {
            return
         }
         
         switch sectionName {
         case .home:
            self?.homeAddresses.remove(at: row)
         case .work:
            self?.workAdressess.remove(at: row)
         case .other:
            self?.otherAddressess.remove(at: row)
         default:
            break
         }
         
         self?.reload(section: sectionName)
      }
   }
   
   private func editAddressIn(section: Int, atRow row: Int) {
      let location = getSavedAddressAt(section: section, row: row)!
      
      isSelectingLocationForSaving = true
      
      AddAddressFromMapViewController.pushViewControllerIn(navVC: self.navigationController!, manager: self, savingforType: location.type, locationToEdit: location)
   }
   
   fileprivate func reload(section: Section) {
      let index = IndexSet.init(integer: section.rawValue)
      tableView.reloadSections(index, with: .automatic)
   }

   
   // MARK: - Server Hits
   func getAllSavedAddress() {
      FavouriteAddress.fetchAllFromServer { [weak self] (success, home, work, others) in
         self?.tableView.isHidden = !success
         let buttonTitle = success ? TEXT.selectFrommap : TEXT.RETRY
         self?.selectFromMapButton.setTitle(buttonTitle, for: .normal)

         guard success else {
            return
         }
         
         self?.homeAddresses = home
         self?.otherAddressess = others
         self?.workAdressess = work
         
         self?.tableView.reloadData()
      }
   }
   
   
}

extension AddNewLocationViewController: FavouriteLocationManager {
   func getFavIdOf(loctionType: favLocationType) -> String? {
      switch loctionType {
      case .Home:
         return homeAddresses.first?.favId
      case .Work:
         return workAdressess.first?.favId
      case .Other:
         return otherAddressess.first?.favId
      }
   }

   func addressSelectedWith(address: String, coordinate: CLLocationCoordinate2D) {
      completionHandler?(coordinate, address)
      self.navigationController?.popToViewController(self, animated: false)
      self.navigationController?.popViewController(animated: true)
   }
   
   func newAddressSaved() {
      getAllSavedAddress()
      self.navigationController?.popToViewController(self, animated: true)
   }

   func doesFavLocationForTypeAlreadyExist(type: favLocationType) -> Bool {
      switch type {
      case .Home:
         return isHomeAddressSavedFor(row: 0)
      case .Work: 
         return isWorkAdressSavedFor(row: 0)
      case .Other:
         return isOtherAddressSavedFor(row: maximumNumberOfOtherAddress - 1)
      }
   }
   
   func InFlowforSavingLocation() -> Bool {
      return isSelectingLocationForSaving
   }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
