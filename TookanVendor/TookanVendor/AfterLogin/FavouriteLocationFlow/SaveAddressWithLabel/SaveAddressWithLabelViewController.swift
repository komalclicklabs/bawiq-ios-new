//
//  SaveAddressWithLabelViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 24/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class SaveAddressWithLabelViewController: UIViewController {
   
   enum Section: Int {
      case label = 0, address, flatNo, landmark
      
      func getHeader() -> String {
         switch self {
         case .label:
            return TEXT.label
         case .address:
            return TEXT.address
         case .flatNo:
            return TEXT.flatNo
         case .landmark:
            return TEXT.landmark
         }
      }
   }
   
   enum SaveActions {
      case add
      case replace
      case edit
   }
   
   // MARK: - Properties
   var address = FavouriteAddress()
   var manager: FavouriteLocationManager?
   
   // MARK: - IBOutlets
   @IBOutlet weak var tableView: UITableView!
   @IBOutlet weak var homeButton: UIButton!
   @IBOutlet weak var workButton: UIButton!
   @IBOutlet weak var otherButton: UIButton!
   @IBOutlet weak var saveButton: UIButton!

   
   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      configureViews()
      setNavigationBar()
      
      tableView.dataSource = self
      tableView.estimatedRowHeight = 45
      tableView.rowHeight = UITableViewAutomaticDimension
      tableView.tableFooterView = UIView()
      
      updateUIAccordingToActionType()
   }
   
   fileprivate func updateUIAccordingToActionType() {
      let actionType = getActionToPerform()
      
      switch actionType {
      case .add, .replace:
         if capacityOfOtherLocationFilled() {
            otherButton.isHidden = true
         }
      case .edit:
         showOnlyButtonWhoseLocationIsBeingEdited()
         removeClickableEffectFromLabelButtons()
      }
   }
   
   fileprivate func capacityOfOtherLocationFilled() -> Bool {
      return manager?.doesFavLocationForTypeAlreadyExist(type: .Other) == true
   }
   
   fileprivate func showOnlyButtonWhoseLocationIsBeingEdited() {
      homeButton.isHidden = address.type != .Home
      otherButton.isHidden = address.type != .Other
      workButton.isHidden = address.type != .Work
   }
   
   fileprivate func removeClickableEffectFromLabelButtons() {
      homeButton.isUserInteractionEnabled = false
      otherButton.isUserInteractionEnabled = false
      workButton.isUserInteractionEnabled = false
   }
   
   fileprivate func setNavigationBar() {
      let navBarTexts = NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: TEXT.saveAddress, leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"), rightButtonImage: nil)
      let navBar = NavigationView.getNibFile(params: navBarTexts, leftButtonAction: { [weak self] in
         self?.backButtonPressed("")
      }, rightButtonAction: nil)
      view.addSubview(navBar)
   }
   
   // MARK: - IBAction
   @IBAction func homeButtonPressed(_ sender: UIButton) {
      guard address.type != .Home else {
         //alreadySelected
         return
      }
      
      if manager?.doesFavLocationForTypeAlreadyExist(type: .Home) == true {
         Singleton.sharedInstance.showAlertWithOption(owner: self, title: "", message: "Do you want to update existing Home address with new one?", showRight: true, leftButtonAction: { [weak self] _ in
            self?.changeAddressTypeTo(.Home)
            }, rightButtonAction: nil, leftButtonTitle: TEXT.YES_ACTION, rightButtonTitle: TEXT.NO)
      } else {
         self.changeAddressTypeTo(.Home)
      }
      
   }
   
   @IBAction func workButtonPressed(_ sender: UIButton) {
      guard address.type != .Work else {
         //alreadySelected
         return
      }
      
      if manager?.doesFavLocationForTypeAlreadyExist(type: .Work) == true {
         Singleton.sharedInstance.showAlertWithOption(owner: self, title: "", message: "Do you want to update existing Work address with new one?", showRight: true, leftButtonAction: { [weak self] _ in
            self?.changeAddressTypeTo(.Work)
            }, rightButtonAction: nil, leftButtonTitle: TEXT.YES_ACTION, rightButtonTitle: TEXT.NO)
      } else {
         self.changeAddressTypeTo(.Work)
      }
   }
   
   @IBAction func otherButtonPressed(_ sender: UIButton) {
      guard address.type != .Other else {
         //alreadySelected
         return
      }
      
      changeAddressTypeTo(.Other)
   }
   
   @IBAction func saveButtonPressed(_ sender: UIButton) {
      
      if address.label.isEmpty && address.type == .Other {
         Singleton.sharedInstance.showAlertWithOption(owner: self, message: "Please add a label for this address.", leftButtonAction: nil, leftButtonTitle: TEXT.OK_TEXT)
         return
      }
      
      let actionToPerform = getActionToPerform()
      
      switch actionToPerform {
      case .add:
         addAddress()
      case .edit:
         editAddress()
      case .replace:
         replaceAddress()
      }

   }
   
   fileprivate func getActionToPerform() -> SaveActions {
      
      guard address.favId.isEmpty else {
         return .edit
      }
      
      guard address.type != .Other else {
         return .add
      }
      
      if manager?.doesFavLocationForTypeAlreadyExist(type: address.type) == true {
         return .replace
      }
      
      return .add
   }
   
   fileprivate func addAddress() {
      address.addAddressToFavLocations() { [weak self] (success) in
         guard success else {
            return
         }
         
         self?.manager?.newAddressSaved()
      }
   }
   
   fileprivate func replaceAddress() {
      
      address.favId = manager!.getFavIdOf(loctionType: address.type)!
      
      address.sendEditionOfAddressToServer() { [weak self] (success) in
         guard success else {
            return
         }
         
         self?.manager?.newAddressSaved()
      }
   }
   
   fileprivate func editAddress() {
      address.sendEditionOfAddressToServer() { [weak self] (success) in
         guard success else {
            return
         }
         
         self?.manager?.newAddressSaved()
      }
   }
   
   @IBAction func backButtonPressed(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
   }

   
   // MARK: - Methods
   
   fileprivate func textChanegedOfCellWithTag(tag: Int, withNewText text: String) {
      let (_, section) = tag.getRowAndSection()
      let sectionType = Section(rawValue: section)!
      
      switch sectionType {
      case .label:
         address.label = text
      case .flatNo:
         address.flatNo = text
      case .landmark:
         address.landmark = text
      default:
         return
      }
      
   }
   
   fileprivate func changeAddressTypeTo(_ type: favLocationType) {
      address.type = type
      tableView.reloadData()
      
      setSelectedStateOfButton(type: type)
   }
   
   fileprivate func setSelectedStateOfButton(type: favLocationType) {
      configure(labelButton: workButton, forSelectedState: type == .Work)
      configure(labelButton: homeButton, forSelectedState: type == .Home)
      configure(labelButton: otherButton, forSelectedState: type == .Other)
   }
   
   // MARK: - Navigation
   static func pushIn(navVC: UINavigationController, manager: FavouriteLocationManager, address: FavouriteAddress) {
      let vc = findIn(storyboard: .favLocation, withIdentifier: "SaveAddressWithLabelViewController") as! SaveAddressWithLabelViewController
      vc.manager = manager
      vc.address = address
      navVC.pushViewController(vc, animated: true)
   }
}

// MARK: - UITableViewDataSource
extension SaveAddressWithLabelViewController: UITableViewDataSource {
   func numberOfSections(in tableView: UITableView) -> Int {
      return 4
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      let section = Section(rawValue: section)!
      
      switch section {
      case .label:
         return address.type == .Other ? 1 : 0
      default:
         return 1
      }
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: SaveAddressTableViewCell.identifier, for: indexPath) as! SaveAddressTableViewCell
      let section = Section(rawValue: indexPath.section)!
      
      cell.selectionStyle = .none
      cell.tag = indexPath.generateTag()
      cell.textChanged = { [weak self] newText in
         self?.textChangedInCellWith(tag: cell.tag, newString: newText)
      }
      
      cell.textViewChanged = { [weak self] textView in
         self?.textViewChangedInCell(textView)
      }
      
      let header = section.getHeader()
      
      switch section {
      case .label:
         cell.setCellWith(title: header, description: address.label, isDescriptionEditable: true)
      case .address:
         cell.setCellWith(title: header, description: address.streetAddress, isDescriptionEditable: false)
      case .flatNo:
         cell.setCellWith(title: header, description: address.flatNo, isDescriptionEditable: true)
      case .landmark:
         cell.setCellWith(title: header, description: address.landmark, isDescriptionEditable: true)
      }
      
      return cell
   }
   
   fileprivate func textChangedInCellWith(tag: Int, newString: String) {
      let section = tag.getRowAndSection().section
      let sectionType = Section(rawValue: section)!
      
      switch sectionType {
      case .label:
         address.label = newString
      case .flatNo:
         address.flatNo = newString
      case .landmark:
         address.landmark = newString
      default:
         break
      }
   }
   
   fileprivate func textViewChangedInCell(_ textView: UITextView) {
      let currentOffset = tableView.contentOffset
      UIView.setAnimationsEnabled(false)
      tableView.beginUpdates()
      tableView.endUpdates()
      UIView.setAnimationsEnabled(true)
      tableView.setContentOffset(currentOffset, animated: false)
   }
}

extension SaveAddressWithLabelViewController: UITableViewDelegate {
   
}

// MARK: - Confgure Views
extension SaveAddressWithLabelViewController {
   fileprivate func configureViews() {
      
      tableView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      
      configureHomeButton()
      configureWorkButton()
      configureOtherButton()
      configureSaveButton()
   }
   

   
   
   fileprivate func configureHomeButton() {
      configure(labelButton: homeButton, forSelectedState: address.type == .Home)
   }
   
   fileprivate func configureWorkButton() {
      configure(labelButton: workButton, forSelectedState: address.type == .Work)
   }
   
   fileprivate func configureOtherButton() {
      configure(labelButton: otherButton, forSelectedState: address.type == .Other)
   }
   
   fileprivate func configure(labelButton: UIButton, forSelectedState selected: Bool) {
      labelButton.titleLabel?.font = UIFont(name: FONT.regular, size: 15)
      
      let color = selected ? COLOR.THEME_FOREGROUND_COLOR : COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.5)
      labelButton.tintColor = color
      labelButton.setBorder(borderColor: color)
      labelButton._cornerRadius = 5
   }
   
   fileprivate func configureSaveButton() {
      saveButton.configureNormalButtonWith(title: TEXT.DONE)
   }
}


