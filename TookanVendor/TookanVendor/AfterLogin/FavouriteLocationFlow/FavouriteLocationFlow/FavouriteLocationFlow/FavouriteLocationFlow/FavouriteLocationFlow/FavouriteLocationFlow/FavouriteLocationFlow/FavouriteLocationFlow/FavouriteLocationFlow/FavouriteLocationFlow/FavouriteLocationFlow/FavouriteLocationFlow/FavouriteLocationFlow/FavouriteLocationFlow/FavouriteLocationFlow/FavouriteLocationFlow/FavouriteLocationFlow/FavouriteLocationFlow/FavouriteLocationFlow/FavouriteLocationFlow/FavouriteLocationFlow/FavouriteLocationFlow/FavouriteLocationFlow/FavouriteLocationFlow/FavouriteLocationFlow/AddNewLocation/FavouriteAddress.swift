//
//  FavouriteAddress.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 14/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

enum favLocationType: String {
   case Home
   case Work
   case Other
}

class FavouriteAddress {
   
   private(set) var address = String()
   private(set) var favId = String()
   private(set) var latitude = String()
   private(set) var longitude = String()
   private(set) var name = String()
   private(set) var type: favLocationType
   
   init(json: [String: Any]) {
      if let value = json["address"] as? String {
         self.address = value
      }
      
      if let value = json["latitude"] as? String {
         self.latitude = value
      }else if let value = json["latitude"] as? Double {
         self.latitude = "\(value)"
      }
      
      if let value = json["longitude"] as? String {
         self.longitude = value
      }else if let value = json["longitude"] as? Double {
         self.longitude = "\(value)"
      }
      
      if let value = json["fav_id"] as? String {
         self.favId = value
      }else if let value = json["fav_id"] as? Int {
         self.favId = "\(value)"
      }
      
      if let value = json["name"] as? String {
         self.name = value
      }
      
      if let locationType = favLocationType(rawValue: self.name) {
         self.type = locationType
      } else {
         self.type = .Other
      }
      
   }
   
   init() {
   type = .Other
   }
   
   // MARK: - Methods
   func deleteFromServer(completion: @escaping (_ success: Bool) -> Void) {
      let params = getParamsToDeleteLocation()
       
      HTTPClient.makeConcurrentConnectionWith(method: .POST, para: params, extendedUrl: API_NAME.deleteFavLocation) { (responseObject, error, _, statusCode) in
         
         guard statusCode == .some(STATUS_CODES.SHOW_DATA) else {
            completion(false)
            return
         }
         
         completion(true)
      }
   }
   
   private func getParamsToDeleteLocation() -> [String: String] {
      return [
         "access_token": Vendor.current!.appAccessToken!,
         "fav_id": favId,
         "app_device_type": APP_DEVICE_TYPE
      ]
   }
   
   
   // MARK: - Type Methods
   static func fetchAllFromServer(completion: @escaping (_ success: Bool, _ home: [FavouriteAddress], _ work: [FavouriteAddress], _ others: [FavouriteAddress]) -> Void) {
      
      let param = getParamsToFetchLocation()
      
      HTTPClient.makeConcurrentConnectionWith(method: .POST, para: param, extendedUrl: API_NAME.getFavouriteLocation) { (responseObject, error, _, statusCode) in
         
         var home = [FavouriteAddress]()
         var work = [FavouriteAddress]()
         var others = [FavouriteAddress]()
         
         guard statusCode == .some(STATUS_CODES.SHOW_DATA),
            let response = responseObject as? [String: Any],
            let data = response["data"] as? [String: Any],
            let rawFavLocations = data["favLocations"] as? [[String: Any]],
            rawFavLocations.count > 0 else {
               completion(false, home, work, others)
               return
         }
         
         for rawLocation in rawFavLocations {
            let tempFavAddress = FavouriteAddress(json: rawLocation)
            
            switch tempFavAddress.type {
            case .Home:
               home.append(tempFavAddress)
            case .Work:
               work.append(tempFavAddress)
            case .Other:
               others.append(tempFavAddress)
            }
            
         }
         
         completion(true, home, work, others)
      }
      
   }
   
   private static func getParamsToFetchLocation() -> [String: Any] {
      return [
         "access_token":Vendor.current!.appAccessToken!,
         "app_device_type":APP_DEVICE_TYPE
      ]
   }
}
