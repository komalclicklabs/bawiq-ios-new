//
//  BQWalletVC.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 25/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit
import TelrSDK
//import CASHU

class BQWalletVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var lblWalletAmount: UILabel!
    @IBOutlet weak var lblCurrency: UILabel!
    @IBOutlet weak var btnAddMoney: UIButton!
    @IBOutlet weak var txtWalletAmount: UITextField!
    @IBOutlet weak var btnAllMoney: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btn100: UIButton!
    @IBOutlet weak var btn200: UIButton!
    @IBOutlet weak var btn300: UIButton!
    var arrSavedAddress = [BQWalletModel]()
    var paymentRequest: PaymentRequest?
    @IBOutlet weak var viewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var waaletMiddleView: UIView!
    @IBOutlet weak var noTransactionLbl: UILabel!
    
    var isFromPaymentScreen: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.sendAccessTokenLoginHit), name: NSNotification.Name(rawValue: "updateWalletBalance"), object: nil)
        self.navigationController?.navigationBar.isHidden = true
        
        self.setupUIComponents()
        self.setNavigationBar()
        self.setupTableView()
        noTransactionLbl.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        sendAccessTokenLoginHit()
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        sendAccessTokenLoginHit()
    }
    
    @objc private func sendAccessTokenLoginHit() {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.loginRequestWithAccessToken({ (isSuccess, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if isSuccess == true {
                Singleton.sharedInstance.isSignedIn = true
                print(response)
                if let status = response["status"] as? Int {
                    switch(status) {
                    case STATUS_CODES.SHOW_DATA:
                        DispatchQueue.main.async(execute: { () -> Void in
                            if let data = response["data"] as? [String:Any] {
                                print("Success")
                                Vendor.logInWith(data: data)
                                if let amount = Vendor.current?.wallet_amount {
                                    self.lblWalletAmount.text = "AED " + "\(amount.roundTo(places: 2))"
                                }
                                
                            }
                        })
                        
                    default:
                        break
                    }
                }
                
            } else {
                print("Error")
            }
        })
    }

    private func setupUIComponents() {
        
        
        self.viewHeightConstraint.constant = 0.0
        waaletMiddleView.isHidden = true
        
        lblWalletAmount.text = "AED " + "\(Vendor.current?.wallet_amount ?? 0.0)"
        
        btn100.layer.cornerRadius = 4.8
        btn100.layer.borderColor = UIColor.gray.cgColor
        btn100.layer.borderWidth = 1.0
        
        btnAllMoney.layer.cornerRadius = 22.0
        
        btn200.layer.cornerRadius = 4.8
        btn200.layer.borderColor = UIColor.gray.cgColor
        btn200.layer.borderWidth = 1.0
        
        btn300.layer.cornerRadius = 4.8
        btn300.layer.borderColor = UIColor.gray.cgColor
        btn300.layer.borderWidth = 1.0
        
        btnAddMoney.layer.cornerRadius = 20.3
        btnAddMoney.layer.borderColor = UIColor.white.cgColor
        btnAddMoney.layer.borderWidth = 1.0
        
        
        lblCurrency.layer.cornerRadius = 4.3
        lblCurrency.layer.borderColor = UIColor.white.cgColor
        lblCurrency.layer.borderWidth = 1.0
    }
    private func setupTableView() {
        
        
        self.tblView.layer.cornerRadius = 5.0
        
        self.tblView.register(UINib(nibName: "BQWalletCell", bundle: nil), forCellReuseIdentifier: "BQWalletCell")
        
        self.getWalletTransactions()
    }
    
    func successFullOrderIDCreate(orderID: String) {
        
        if let walletAmount = txtWalletAmount.text {
            BQBookingModel.shared.walletMoney = walletAmount
            BQBookingModel.shared.transactionOrderID = orderID
            paymentRequest = self.preparePaymentRequest(orderID: orderID, walletAmount: walletAmount)
            performSegue(withIdentifier: "TelrSegue", sender: paymentRequest)
        } else {
            //Show error on banner
        }
    }
    
    @IBAction func addmOneyAction(_ sender: Any) {
        self.hideKeyBoard()
        UIView.animate(withDuration: 0.3, animations: {
            if self.btnAddMoney.isSelected == true {
                self.btnAddMoney.isSelected = false
                self.viewHeightConstraint.constant = 0.0
                self.waaletMiddleView.isHidden = true
                self.btnAddMoney.setTitle("ADD MONEY", for: .normal)
                
            } else {
                self.btnAddMoney.isSelected = true
                self.viewHeightConstraint.constant = 242.0
                self.waaletMiddleView.isHidden = false
                self.btnAddMoney.setTitle("CANCEL", for: .normal)
            }
        }) { (finished) in
            
        }
    }
    
    func getOrderIDForPayment() {
        self.getOrderID { (success, response) in
            if success {
                print(response as Any)
                if let dataRes = response {
                    if let job = dataRes["order"] as? [String: Any], let jobID = job["order_id"] as? String {
                        self.successFullOrderIDCreate(orderID: jobID)
                    }
                }
                
                
            }
        }
    }
    func getOrderID(receivedResponse: @escaping (_ succeeded: Bool, _ response:[String: Any]?) -> ()) {
        //get_order_id
        NetworkingHelper.sharedInstance.commonServerCall(apiName: "get_order_id",
                                                         params: [:],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                            DispatchQueue.main.async {
                                                                if succeeded {
                                                                    guard let data = response["data"] as? [String: Any] else {
                                                                        receivedResponse(false,nil)
                                                                        return
                                                                    }
                                                                    receivedResponse(true, data)
                                                                } else {
                                                                    receivedResponse(false,nil)
                                                                }
                                                            }
                                                            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? TelrController{
            UserDefaults.standard.set(true, forKey: "isComingFromWallet")
            destination.paymentRequest = paymentRequest!
        }
    }
    
    private func preparePaymentRequest(orderID: String, walletAmount: String) -> PaymentRequest {
        
//        let paymentReq = PaymentRequest()
//        paymentReq.key = "VPbSB@pX6L^QfJfL"
//        paymentReq.store = "20528"
//        paymentReq.appId = "123456789"
//        paymentReq.appName = "Bawiq"
//        paymentReq.appUser = "123456"
//        paymentReq.appVersion = "0.0.1"
////        paymentReq.transTest = "1"   // DEV/TEST
//        paymentReq.transTest = "0"     // CLIENT
//        paymentReq.transType = "sale"
//        paymentReq.transClass = "paypage"
//        paymentReq.transCartid = orderID //String(arc4random())
//        paymentReq.transDesc = "Bawiq Wallet Recharge"
//        paymentReq.transCurrency = "AED"
//        paymentReq.transAmount = walletAmount
//        paymentReq.billingEmail = Vendor.current?.email ?? ""
//        paymentReq.billingFName = Vendor.current?.firstName ?? ""
//        paymentReq.billingLName = Vendor.current?.username ?? ""
//        paymentReq.billingTitle = "Mr"
//        paymentReq.city = BQBookingModel.shared.addressLine1 ?? ""
//        paymentReq.country = "United Arab Emirates"
//        paymentReq.region = BQBookingModel.shared.addressLine2 ?? ""
//        paymentReq.address = BQBookingModel.shared.addressLine2 ?? ""
//        return paymentReq
        
        
        let paymentReq = PaymentRequest()
        paymentReq.key = "VPbSB@pX6L^QfJfL"
        paymentReq.store = "20528"
        paymentReq.appId = "123456789"
        paymentReq.appName = "Bawiq"
        paymentReq.appUser = "123456"
        paymentReq.appVersion = "0.0.1"
//        paymentReq.transTest = "1"   // DEV/TEST
        paymentReq.transTest = "0"     // CLIENT
        paymentReq.transType = "sale"
        paymentReq.transClass = "paypage"
        paymentReq.transCartid = orderID //String(arc4random())
        paymentReq.transDesc = "Bawiq Wallet Recharge"
        paymentReq.transCurrency = "AED"
        paymentReq.transAmount = walletAmount
        paymentReq.billingEmail = Vendor.current?.email ?? ""
        paymentReq.billingFName = Vendor.current?.firstName ?? ""
        paymentReq.billingLName = Vendor.current?.username ?? ""
        paymentReq.billingTitle = "Mr"
        paymentReq.city = BQBookingModel.shared.addressLine1 ?? ""
        paymentReq.country = "United Arab Emirates"
        paymentReq.region = BQBookingModel.shared.addressLine2 ?? ""
        paymentReq.address = BQBookingModel.shared.addressLine2 ?? ""
        return paymentReq
        
        
//        let paymentReq = PaymentRequest()
//        paymentReq.key = "M4M5q^55fbT-SdLN"
//        paymentReq.store = "20294"
//        paymentReq.appId = "123456789"
//        paymentReq.appName = "Bawiq"
//        paymentReq.appUser = "123456"
//        paymentReq.appVersion = "0.0.1"
//        paymentReq.transTest = "1"
//        paymentReq.transType = "sale"
//        paymentReq.transClass = "paypage"
//        paymentReq.transCartid = orderID //String(arc4random())
//        paymentReq.transDesc = "Bawiq Wallet"
//        paymentReq.transCurrency = "AED"
//        paymentReq.transAmount = walletAmount
//        paymentReq.billingEmail = Vendor.current?.email ?? ""
//        paymentReq.billingFName = Vendor.current?.firstName ?? ""
//        paymentReq.billingLName = Vendor.current?.username ?? ""
//        paymentReq.billingTitle = "Mr"
//        paymentReq.city = BQBookingModel.shared.addressLine1 ?? ""
//        paymentReq.country = "United Arab Emirates"
//        paymentReq.region = BQBookingModel.shared.addressLine2 ?? ""
//        paymentReq.address = BQBookingModel.shared.addressLine2 ?? ""
//        return paymentReq
    }
    
    @IBAction func addWalletAction(_ sender: Any) {
//        self.addCashUMoney()
        self.hideKeyBoard()
        if let walletAmount = txtWalletAmount.text?.length {
            if walletAmount > 0 {
                self.getOrderIDForPayment()
            } else {
                ErrorView.showWith(message: "Please enter amount.", removed: nil)
            }
        }
    }
    
    func hideKeyBoard() {
        self.txtWalletAmount.resignFirstResponder()
    }
    
    @IBAction func btn300Action(_ sender: Any) {
        self.hideKeyBoard()
        self.changeButton100UIAsPerAmount(is100Selected: false, is200Selected: false, is300Selected: true)
        txtWalletAmount.text = "300"
    }
    @IBAction func btn200Action(_ sender: Any) {
        self.hideKeyBoard()
        self.changeButton100UIAsPerAmount(is100Selected: false, is200Selected: true, is300Selected: false)
        txtWalletAmount.text = "200"
    }
    
    func changeButton100UIAsPerAmount(is100Selected: Bool, is200Selected: Bool, is300Selected: Bool) {
        
        if is300Selected {
            btn300.backgroundColor = COLOR.App_Red_COLOR
            btn300.setTitleColor(.white, for: .normal)
            
            btn200.backgroundColor = .white
            btn200.setTitleColor(.darkGray, for: .normal)
            
            btn100.backgroundColor = .white
            btn100.setTitleColor(.darkGray, for: .normal)
        } else if is200Selected {
            btn200.backgroundColor = COLOR.App_Red_COLOR
            btn200.setTitleColor(.white, for: .normal)
            
            btn300.backgroundColor = .white
            btn300.setTitleColor(.darkGray, for: .normal)
            
            btn100.backgroundColor = .white
            btn100.setTitleColor(.darkGray, for: .normal)
        } else if is100Selected  {
            btn100.backgroundColor = COLOR.App_Red_COLOR
            btn100.setTitleColor(.white, for: .normal)
            
            btn200.backgroundColor = .white
            btn200.setTitleColor(.darkGray, for: .normal)
            
            btn300.backgroundColor = .white
            btn300.setTitleColor(.darkGray, for: .normal)
        } else {
            btn100.backgroundColor = .white
            btn100.setTitleColor(.darkGray, for: .normal)
            
            btn200.backgroundColor = .white
            btn200.setTitleColor(.darkGray, for: .normal)
            
            btn300.backgroundColor = .white
            btn300.setTitleColor(.darkGray, for: .normal)
        }
        
    }
    
    @IBAction func btn100Action(_ sender: Any) {
        self.hideKeyBoard()
        self.changeButton100UIAsPerAmount(is100Selected: true, is200Selected: false, is300Selected: false)
        txtWalletAmount.text = "100"
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let txtString = textField.text else {
            return false
        }
        let maxLength = 7
        let currentString: NSString = txtString as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if newString.length <= maxLength {
            let finalString = (txtString as NSString).replacingCharacters(in: range, with: string)
            if finalString == "100" {
                self.changeButton100UIAsPerAmount(is100Selected: true, is200Selected: false, is300Selected: false)
            } else if finalString == "200" {
                self.changeButton100UIAsPerAmount(is100Selected: false, is200Selected: true, is300Selected: false)
            } else if finalString == "300" {
                self.changeButton100UIAsPerAmount(is100Selected: false, is200Selected: false, is300Selected: true)
            } else {
                self.changeButton100UIAsPerAmount(is100Selected: false, is200Selected: false, is300Selected: false)
            }
            return true
        }
        return false
    }
    
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        var backImage = UIImage()
        if isFromPaymentScreen == true {
            backImage = #imageLiteral(resourceName: "BackButtonWhite")
        } else {
            backImage = #imageLiteral(resourceName: "menu")
        }
        
        //Use for showing two right bar buttons on navigation bar
        
        let navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "My Payments", leftButtonImage: backImage, rightButtonImage: nil, secondRightButtonImage: nil)
            , leftButtonAction: {[weak self] in
                self?.backAction()
            }, rightButtonAction: nil,
               rightButtonSecondAction: nil
        )
        navigationBar.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
        
    }
    
    func backAction() {
        if isFromPaymentScreen {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.presentLeftMenuViewController()
        }
    }
    
    func getWalletTransactions() {
        
        let params = getParamsToDeleteLocation()
       ActivityIndicator.sharedInstance.showActivityIndicator()
       
        HTTPClient.makeConcurrentConnectionWith(method: .POST, para: params, extendedUrl: "get_wallet_transactions") { (responseObject, error, _, statusCode) in
            
            ActivityIndicator.sharedInstance.hideActivityIndicator()
    
            
            guard statusCode == .some(STATUS_CODES.SHOW_DATA) else {
                return
            }
            print(responseObject ?? "")
            if let dataRes = responseObject as? [String:Any] {
                if let data = dataRes["data"] as? [String:Any] {
                    if let favLocations = data["walletTransaction"] as? [[String: Any]] {
                        print(favLocations)
                        
                        for item in favLocations {
                            let rollData = BQWalletModel.init(param: item)
                            self.arrSavedAddress.append(rollData)
                        }
                        
                        if self.arrSavedAddress.count > 0 {
                            self.tblView.delegate = self
                            self.tblView.dataSource = self
                            //self.lblNoDataFound.isHidden = true
                            self.tblView.isHidden = false
                            self.tblView.reloadData()
                        } else {
                            //self.lblNoDataFound.isHidden = false
                            self.tblView.isHidden = true
                        }
                    } else {
                        //self.lblNoDataFound.isHidden = false
                        self.tblView.isHidden = true
                    }
                    print(data)
                }
            }
        }
    }
    
    private func getParamsToDeleteLocation() -> [String: Any] {
        return [
            "app_access_token": Vendor.current!.appAccessToken!
        ]
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrSavedAddress.count > 0 {
            noTransactionLbl.isHidden = true
            return arrSavedAddress.count
        } else {
            noTransactionLbl.isHidden = false
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BQWalletCell", for: indexPath) as? BQWalletCell else {
            return UITableViewCell()
        }
         let cellDataModel: BQWalletModel = self.arrSavedAddress[indexPath.row]
        
        cell.lblTitle.text = cellDataModel.transaction_type //cellDataModel.transaction_mode
        cell.lblDate.text = cellDataModel.transaction_time
        cell.lblAmount.text = "AED \(cellDataModel.transaction_amount ?? 0.0)"
        return cell
    }
}

extension BQWalletVC {
    func addCashUMoney() {
        
//        //******* Setup CASHU Configurations
//
//        let cashuConfigurations : CASHUConfigurations = CASHUConfigurations()
//        //The Merchant ID that you have received from CASHU
//        cashuConfigurations.clientID = "CLIENT_ID_SAND-D1A45B3A6DB6A27D2331DCF4089E6135"
//            //"CLIENT_ID_SAND-0989D3FBC3FC7A2457090E3C4EDB7827" //"Your Merchant ID"
//        //MERCHANT REFERENCE SHOULD BE UNIQUE, THIS IS TOTALY USED BY YOU
//        cashuConfigurations.merchantReference = "cu\(Date().timeIntervalSince1970)"
//        cashuConfigurations.environment = .dev
////        cashuConfigurations.sdkToken
//        cashuConfigurations.delegate = self
//
//        // Initializing Product Details
////        let productDetails : ProductDetails = ProductDetails()
////        productDetails.currency = .usd
////        productDetails.productName = ""
////        productDetails.price = 0.0
////        cashuConfigurations.productDetails = productDetails
//        cashuConfigurations.productDetails.currency = .aed
//        cashuConfigurations.productDetails.productName = "Bawiq Wallet Recharge"
//
//        cashuConfigurations.productDetails.price = 10
//        cashuConfigurations.presentingMethod = .push
//        cashuConfigurations.language = .english
//        cashuConfigurations.isLoggingEnabled = true
//        //**************
//
//        CASHUServices.initiateProductPaymentInParent(self, configurations: cashuConfigurations)
        
    }
}

//extension BQWalletVC: CASHUServicesDelegate {
//
//    func didFinishPaymentSuccessfullyWithReferenceID(referenceID: String, productDetails: CASHU.ProductDetails) {
//        self.navigationController?.popViewController(animated: true)
//    }
//
//    func didFailPaymentWithReferenceID(referenceID: String, productDetails: CASHU.ProductDetails) {
//
//    }
//}
//
