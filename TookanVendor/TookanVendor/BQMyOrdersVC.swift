//
//  BQMyOrdersVC.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 10/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQMyOrdersVC: UIViewController, CAPSPageMenuDelegate {

    var controllerArray: [UIViewController] = []
    var pageMenu : CAPSPageMenu?
    var navigationBar:NavigationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupPageMenu()
        self.setNavigationBar()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Setup Page menu
    func setupPageMenu() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "MyOrders", bundle: nil)
        if let ongoingController = mainStoryboard.instantiateViewController(withIdentifier: "BQAllOrdersVC") as? BQAllOrdersVC {
            ongoingController.title = "All"
            ongoingController.currentViewController = self
            controllerArray.append(ongoingController)
        }
        
        if let pastController = mainStoryboard.instantiateViewController(withIdentifier: "BQUpcomingOrderVC") as? BQUpcomingOrderVC {
            pastController.title = "Incoming"
            pastController.currentViewController = self
            controllerArray.append(pastController)
        }
        
        if let upcomingController = mainStoryboard.instantiateViewController(withIdentifier: "BQPastOrdersVC") as? BQPastOrdersVC {
            upcomingController.title = "Past"
            upcomingController.currentViewController = self
            controllerArray.append(upcomingController)
        }
        
        let parameters: [CAPSPageMenuOption] = [
            .menuItemSeparatorWidth(1.0),
            .useMenuLikeSegmentedControl(true),
            .menuItemSeparatorPercentageHeight(0.5)
        ]
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x:0, y:100, width:self.view.frame.width, height:self.view.frame.height), pageMenuOptions: parameters)
        pageMenu?.delegate = self
        
        pageMenu?.view.layer.shadowColor = UIColor.lightGray.cgColor
        pageMenu?.view.layer.shadowOpacity = 1
        pageMenu?.view.layer.shadowOffset = CGSize.zero
        pageMenu?.view.layer.shadowRadius = 2
        
        self.view.addSubview(pageMenu!.view)
    }
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        //Use for showing two right bar buttons on navigation bar
        
        navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "My Orders", leftButtonImage: #imageLiteral(resourceName: "menu"), rightButtonImage: nil, secondRightButtonImage: nil)
            , leftButtonAction: {[weak self] in
                self?.presentLeftMenuViewController()
            }, rightButtonAction: nil,
               rightButtonSecondAction: nil
        )
        navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
    }
}

extension BQMyOrdersVC {
    func didMoveToPage(_ controller: UIViewController, index: Int) {
        print("did move to page")
        pageMenu?.moveSelectionIndicator(index)
        //        var color : UIColor = UIColor(red: 18.0/255.0, green: 150.0/255.0, blue: 225.0/255.0, alpha: 1.0)
        //        var navColor : UIColor = UIColor(red: 17.0/255.0, green: 64.0/255.0, blue: 107.0/255.0, alpha: 1.0)
        //
        //        if index == 1 {
        //            color = UIColor.orangeColor()
        //            navColor = color
        //        } else if index == 2 {
        //            color = UIColor.grayColor()
        //            navColor = color
        //        } else if index == 3 {
        //            color = UIColor.purpleColor()
        //            navColor = color
        //        }
        //
        //        UIView.animateWithDuration(0.5, animations: { () -> Void in
        //            self.navigationController!.navigationBar.barTintColor = navColor
        //        }) { (completed) -> Void in
        //            println("did fade")
        //        }
    }
    
    func willMoveToPage(_ controller: UIViewController, index: Int) {
        print("will move to page")
        
        //        var color : UIColor = UIColor(red: 18.0/255.0, green: 150.0/255.0, blue: 225.0/255.0, alpha: 1.0)
        //        var navColor : UIColor = UIColor(red: 17.0/255.0, green: 64.0/255.0, blue: 107.0/255.0, alpha: 1.0)
        //
        //        if index == 1 {
        //            color = UIColor.orangeColor()
        //            navColor = color
        //        } else if index == 2 {
        //            color = UIColor.grayColor()
        //            navColor = color
        //        } else if index == 3 {
        //            color = UIColor.purpleColor()
        //            navColor = color
        //        }
        //
        //        UIView.animateWithDuration(0.5, animations: { () -> Void in
        //            self.navigationController!.navigationBar.barTintColor = navColor
        //        }) { (completed) -> Void in
        //            println("did fade")
        //        }
    }
}
