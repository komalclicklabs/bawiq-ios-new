//
//  CustomizationViewController.swift
//  Jugnoo Autos
//
//  Created by Sourabh on 12/01/17.
//  Copyright © 2017 Socomo Technologies. All rights reserved.
//

import UIKit

// RestaurantDetail-Customization
protocol UpdateTableViewDelegate:class {
    func reloadTableCell()
}

class CustomizationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UpdateQuantityDelegate {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet var navigationHeaderView: UIView!
    weak var delegate:UpdateTableViewDelegate!
    @IBOutlet weak var headerLabel: UILabel!
    
    @IBOutlet weak var itemsListTableView: UITableView!
    
    @IBOutlet weak var addToCartLabel: UILabel!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var cartPriceLabel: UILabel!
    var menuDetailCustomizeObjectArray:[MenuCustomizableObjects]!
    
    
    var navigationBar: NavigationView!
    var productElement : Product?
    var selectedVarietyItemID:String!
    var selectedVarietyItemName:String!
    var selectedVarietyItemDescription:String!
    var selectedVarietyItemPrice:Double!
    var selectedVarietyItemTaxes:[AnyObject]!
    var isSelectedVarietyItemVeg:Bool!
    var selectedCustomizeID:String!
    var onAddButtonClick : ((_ itemQuantity:Int)->Void)?
//    var selectedSection:Int!
//    var selectedRow:Int!
    
    var customizeIDArray = [String]()
    
    var customizeOptionIDArray = [String]()
    var customizeNameArray = [String]()
    var customizePriceArray = [Double]()
    var additionalCostArray = [Double]()
    var selectionArray = [[Bool]]()
    
    var totalPrice = Double()
    var itemQuantity:Int = 1
    weak var menuDelegate: MenuViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavBar()
        setCartCountLable()
        addToCartButton.titleLabel?.font = UIFont(name: FONT.light, size: 17.0)
        cartPriceLabel.font = UIFont(name: FONT.light, size: 17.0)
        headerLabel.text = "Customize Item"
        headerLabel.font = UIFont(name: FONT.regular, size:16.0)
        backButton.setImage(backButtonImage!.renderWithAlwaysTemplateMode(), for: UIControlState.normal)
        backButton.tintColor = UIColor.white
        navigationHeaderView.layoutIfNeeded()
//        CommonFunctions.shareCommonMethods().createShadow(of: navigationHeaderView)
        navigationHeaderView.setShadow()
        navigationHeaderView.backgroundColor = UIColor.white
        navigationHeaderView.isHidden = true
//        CommonFunctions.shareCommonMethods().googleAnalyticsScreenTracker(withScreenName: Menus_Customize_Item)
        self.itemsListTableView.backgroundColor = .white
        self.itemsListTableView.separatorColor = UIColor.lightGray
        self.itemsListTableView.estimatedRowHeight = 100
        self.itemsListTableView.rowHeight = UITableViewAutomaticDimension
//        self.itemsListTableView.layer.shadowOffset = CGSize(width: 5,height: 5)
        self.itemsListTableView.tableFooterView = UIView(frame: CGRect.zero)
        self.itemsListTableView.layer.borderColor = UIColor.lightGray.cgColor
        self.itemsListTableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: itemsListTableView.bounds.size.width, height: 10.0))
        
//        roundingCorners(view: self.itemsListTableView, upper: true, bottom: false)
        
        totalPrice = selectedVarietyItemPrice
        
        self.itemsListTableView.layer.masksToBounds = true
        
        self.itemsListTableView.register(UINib(nibName: "CutomizationHeaderTableViewCell", bundle:frameworkBundle), forCellReuseIdentifier: "CustomizeItemCells")
        self.itemsListTableView.register(UINib(nibName: "CustomizeItemsDetailTableViewCell", bundle: frameworkBundle), forCellReuseIdentifier: "CustomizeItemsDetailCell")
        self.itemsListTableView.register(UINib(nibName: NIB_NAME.listCell, bundle: frameworkBundle), forCellReuseIdentifier: ListTableViewCell.identifier)
        
        for i in 0..<menuDetailCustomizeObjectArray.count {
            var tempSelectionArray = [Bool]()
            
            for j in 0..<menuDetailCustomizeObjectArray[i].customizeSubOptionArray.count {
                
                if menuDetailCustomizeObjectArray[i].isCheckBox == false {
                    if j == 0 {
                        tempSelectionArray.append(true)
                        totalPrice += menuDetailCustomizeObjectArray[i].customizeSubOptionArray[j].customizePrice
                    }
                }
                
                tempSelectionArray.append(false)
                
//                if menuDetailCustomizeObjectArray[i].customizeSubOptionArray[j].isDefault == true {
//                    tempSelectionArray.append(true)
//                    totalPrice += menuDetailCustomizeObjectArray[i].customizeSubOptionArray[j].customizePrice
//                }
//                else {
//                    tempSelectionArray.append(false)
//                }
                
            }
            self.selectionArray.append(tempSelectionArray)
        }
        
        cartPriceLabel.text = "\(Singleton.sharedInstance.formDetailsInfo.currencyid) \( Singleton.sharedInstance.convertPriceToString(value: totalPrice))"
        addToCartLabel.text = TEXT.addToCart
//        CommonFunctions.shareCommonMethods().createShadow(of: addToCartButton)
        addToCartButton.backgroundColor = nil
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.itemsListTableView.delegate = self
        self.itemsListTableView.dataSource = self
        
        self.itemsListTableView.reloadData()
        
    }
    
  override  func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       // self.addToCartButton.setGradientColorFrom()
    self.addToCartButton.backgroundColor = UIColor.clear
    }
    
    // MARK: - TableView DataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return menuDetailCustomizeObjectArray.count+1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        }
        else {
            return menuDetailCustomizeObjectArray[section-1].customizeSubOptionArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ListTableViewCell.identifier, for: indexPath) as! ListTableViewCell
            
            cell.delegate = self
            cell.tag = indexPath.generateTag()
            cell.selectionStyle = .none
            guard let element = productElement else {
                return cell
            }
            
            let quantity = itemQuantity
            cell.setCellWith(imageUrl: element.imageUrl, description: element.lines.getAttributedStringDescriptionForList(), actionType: element.actionType, quantity: quantity,imageSize: element.imageSize ?? 1)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomizeItemsDetailCell") as! CustomizeItemsDetailTableViewCell
            cell.selectionStyle = .none
            var selectedImage = checked
            
            var deSelectedImage = checkboxEmpty
            
            
            if menuDetailCustomizeObjectArray[indexPath.section-1].isCheckBox == true {
                selectedImage = checked
                deSelectedImage = checkboxEmpty
                
            }
            cell.topBgContraint.constant = 0
            cell.bottomBgContraint.constant = 0
            
            let numberOfRows = self.tableView(tableView, numberOfRowsInSection: indexPath.section)
            cell.clipsToBounds = true
            if selectionArray[indexPath.section-1].count - 1 == indexPath.row {
                cell.seperatorLine.isHidden = false
            } else {
                cell.seperatorLine.isHidden = true
            }
            cell.seperatorLine.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1.0)
            if numberOfRows == 1 {
              //  cell.cornerRect = .allCorners
              //  cell.bgView._cornerRadius = 3
             //   cell.bgView.setShadow()
             //   cell.topBgContraint.constant = 2
             //   cell.bottomBgContraint.constant = 2
                cell.seperatorLine.isHidden = false
            }else {
                if indexPath.row == 0 {
                    cell.cornerRect = [.topLeft , .topRight]
                    cell.topBgContraint.constant = 2
                } else if indexPath.row == numberOfRows - 1 {
                    //cell.seperatorLine.backgroundColor = UIColor.clear
                    cell.cornerRect = [.bottomLeft , .bottomRight]
                    cell.bottomBgContraint.constant = 2
                }
                else {
                    cell.cornerRect = []
                }
            }
//            cell.itemSelectionButton.tintColor = COLOR.THEME_FOREGROUND_COLOR
            if selectionArray[indexPath.section-1][indexPath.row] == true {
                cell.itemSelectionButton.setImage(selectedImage, for: .normal)
                cell.itemNameLabel.font = UIFont(name: FONT.semiBold, size: 14)
            }
            else {
                cell.itemSelectionButton.setImage(deSelectedImage, for: .normal)
                cell.itemNameLabel.font = UIFont(name: FONT.light, size: 14)
            }
            
            cell.itemNameLabel.textColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1.0)
            cell.itemNameLabel.text = menuDetailCustomizeObjectArray[indexPath.section-1].customizeSubOptionArray[indexPath.row].customizeOptionName
            cell.itemNameLabel.letterSpacing = 0.2
            
            cell.itemCostLabel.textColor = UIColor(red: 92/255, green: 92/255, blue: 92/255, alpha: 1.0)
            cell.itemCostLabel.font = UIFont(name: FONT.semiBold, size: 14)
            if menuDetailCustomizeObjectArray[indexPath.section-1].customizeSubOptionArray[indexPath.row].customizePrice > 0 {
                cell.itemCostLabel.text = "\(Singleton.sharedInstance.formDetailsInfo.currencyid) \(Singleton.sharedInstance.convertPriceToString(value: menuDetailCustomizeObjectArray[indexPath.section-1].customizeSubOptionArray[indexPath.row].customizePrice))"
            }
            else {
                cell.itemCostLabel.text = ""
            }
            cell.itemCostLabel.letterSpacing = 0.2
            return cell
        }
        
    }
    
    // MARK: - TableView Delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section > 0 {
            
            if menuDetailCustomizeObjectArray[indexPath.section-1].isCheckBox == true {
                
                if selectionArray[indexPath.section-1][indexPath.row] == false {
                    totalPrice += menuDetailCustomizeObjectArray[indexPath.section-1].customizeSubOptionArray[indexPath.row].customizePrice
                    selectionArray[indexPath.section-1][indexPath.row] = true
                }
                else {
                    selectionArray[indexPath.section-1][indexPath.row] = false
                    
                    totalPrice -= menuDetailCustomizeObjectArray[indexPath.section-1].customizeSubOptionArray[indexPath.row].customizePrice
                }
            }
            else {
                
                var ifItemAlreadySelected = false
                
                for i in 0..<selectionArray[indexPath.section-1].count {
                    
                    if i == indexPath.row {
                        
//                        ifItemAlreadySelected = selectionArray[indexPath.section-1][indexPath.row]
                    }
                    if selectionArray[indexPath.section-1][i] == true {
                        totalPrice -= menuDetailCustomizeObjectArray[indexPath.section-1].customizeSubOptionArray[i].customizePrice
                    }
                    selectionArray[indexPath.section-1][i] = false
                }
                
                if ifItemAlreadySelected == false {
                    selectionArray[indexPath.section-1][indexPath.row] = true
                    totalPrice += menuDetailCustomizeObjectArray[indexPath.section-1].customizeSubOptionArray[indexPath.row].customizePrice
                }
            }
            cartPriceLabel.text = "\(Singleton.sharedInstance.formDetailsInfo.currencyid) \( Singleton.sharedInstance.convertPriceToString(value: Double(itemQuantity)*totalPrice))"
            let section = IndexSet(integer: indexPath.section)
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 0))
        if section == 0 {
        }else {
            tableHeaderView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 30)
            tableHeaderView.tag = section
            tableHeaderView.backgroundColor = UIColor.clear
            
            let headerLbl:UILabel = UILabel(frame: CGRect(x: 15, y: 0, width: tableView.frame.width-20, height: 20))
            headerLbl.backgroundColor = .clear
            headerLbl.textAlignment = .left
            headerLbl.textColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1.0)
            headerLbl.font = UIFont(name: FONT.semiBold, size: 12)
            if menuDetailCustomizeObjectArray[section-1].isCheckBox == false {
                headerLbl.text = "\(menuDetailCustomizeObjectArray[section-1].customizeName.uppercased()) (Required)"
            }
            else {
                headerLbl.text = "\(menuDetailCustomizeObjectArray[section-1].customizeName.uppercased()) (Optional)"
            }
            headerLbl.minimumScaleFactor = 0.5
            headerLbl.numberOfLines = 0
            headerLbl.letterSpacing = 1.0
            tableHeaderView.addSubview(headerLbl)
        }
        return tableHeaderView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        return 40
    }
  
    // MARK: - UpdateSelection Delegate
    
    func updateSelection(customizeOptionIDArray:[String], customizePriceArray:[Double], additionalCostArray:[Double]) {
        
        self.customizeOptionIDArray = customizeOptionIDArray
        self.customizePriceArray = customizePriceArray
        self.additionalCostArray = additionalCostArray
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Update Quantity Delegate
    
    func updateQuantity(value:Int) {
        
        if value == 0 {
//            CommonFunctions.shareCommonMethods().getNavigationControllerOfCurrentlySelectedTab().popViewController(animated: true)
//           55 CommonFunctions.shareCommonMethods().popViewController(ofSelectedTab: true)
        }
        else {
            itemQuantity = value
            
            cartPriceLabel.text = " \(Singleton.sharedInstance.formDetailsInfo.currencyid) \( Singleton.sharedInstance.convertPriceToString(value: Double(itemQuantity)*totalPrice))"
        }
        
        
    }
    // MARK: - Update Cart for Checkout
    
    func addProductForCart() {
        
        var index = 0
        var isItemAlreadySelected = false
        
        if RestaurantMenuObject.sharedInstance.selectedOrderObjectArray.map({$0.productVarietyID}).contains(selectedVarietyItemID) {
            
            for i in 0..<RestaurantMenuObject.sharedInstance.selectedOrderObjectArray.count {
                
                isItemAlreadySelected = false
                
                if RestaurantMenuObject.sharedInstance.selectedOrderObjectArray[i].productVarietyID == selectedVarietyItemID {
                    
                    if self.customizeOptionIDArray.count == RestaurantMenuObject.sharedInstance.selectedOrderObjectArray[i].customizeOptionIDArray.count {
                        
                        isItemAlreadySelected = true
                        
                        for j in 0..<self.customizeOptionIDArray.count {
                            
                            if !RestaurantMenuObject.sharedInstance.selectedOrderObjectArray[i].customizeOptionIDArray.contains(self.customizeOptionIDArray[j]) {
                                
                                isItemAlreadySelected = false
                                break
                            }
                        }
                    }
                }
                
                if isItemAlreadySelected == true {
                    
                    index = i
                    break
                }
            }
            
            // Check if Customization is same
            
            if isItemAlreadySelected {
                
//                let index = RestaurantMenuObject.sharedInstance.selectedOrderObjectArray.map({$0.productVarietyID}).index(of: selectedVarietyItemID)!
                
                let selectedOrderObject = RestaurantMenuObject.sharedInstance.selectedOrderObjectArray[index]
                
                selectedOrderObject.quantity = selectedOrderObject.quantity+itemQuantity
                
                RestaurantMenuObject.sharedInstance.selectedOrderObjectArray[index] = selectedOrderObject
                
//                saveMenuCartData()
            }
            else{
                
                self.insertNewItem()
            }
        }
        else {
            
            self.insertNewItem()
        }
    }
    
    func insertNewItem() {
        
        
//        let selectedOrderObject = SelectedOrderObject(productVarietyID: selectedVarietyItemID,
//                                                      productVarietyName:selectedVarietyItemName,
//                                                      productVarietyPrice: selectedVarietyItemPrice,
//                                                      quantity: itemQuantity,
//                                                      customizeIDArray: customizeIDArray,
//                                                      customizeOptionIDArray: customizeOptionIDArray,
//                                                      customizeNameArray:customizeNameArray,
//                                                      customizePriceArray: customizePriceArray,
//                                                      additionalCostArray: additionalCostArray,
//                                                      productVarietyTaxes: selectedVarietyItemTaxes,
//                                                      isProductVeg:isSelectedVarietyItemVeg)
//        
//        RestaurantMenuObject.sharedInstance.selectedOrderObjectArray.append(selectedOrderObject)
//        
//        saveMenuCartData()
        
    }
    
    @IBAction func backClick(_ sender: Any) {
      //  CommonFunctions.shareCommonMethods().ga(withCategory: Category_Menus, action: Action_Customize_Item, label:Label_Back_Button_Clicked , value: nil)
//        CommonFunctions.shareCommonMethods().getNavigationControllerOfCurrentlySelectedTab().popViewController(animated: true)
      //4  CommonFunctions.shareCommonMethods().popViewController(ofSelectedTab: true)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func cancelAction(_ sender: Any) {
        
        delegate.reloadTableCell()
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func OkAction(_ sender: Any) {
//        self.insertNewItem() // Add new element everytime
        
        customizeIDArray.removeAll()
        
        customizeOptionIDArray.removeAll()
        customizeNameArray.removeAll()
        customizePriceArray.removeAll()
        additionalCostArray.removeAll()
        print(selectionArray)
        var custObjectArray = [(id:String, customizeOptionName:String ,isDefault:Bool,customizePrice:Double, additionalCost:Double)]()
       
        self.navigationController?.popViewController(animated: true)
        var selectedString = ""
        var extraPrice = Double()
        
        for i in 0..<selectionArray.count {
            var selectedCusts = ""
            for j in 0..<selectionArray[i].count {
                
                if selectionArray[i][j] == true {
                    
                    selectedCusts.append(menuDetailCustomizeObjectArray[i].customizeSubOptionArray[j].customizeOptionName + " ")
                    customizeOptionIDArray.append(menuDetailCustomizeObjectArray[i].customizeSubOptionArray[j].id)
                    custObjectArray.append(menuDetailCustomizeObjectArray[i].customizeSubOptionArray[j])
                    extraPrice = extraPrice + menuDetailCustomizeObjectArray[i].customizeSubOptionArray[j].customizePrice
                }
            }
            if selectedCusts != "" {
            selectedString = selectedString + "\( menuDetailCustomizeObjectArray[i].customizeName ) : \(selectedCusts)  \n"
            }
        }
        
        let data = SelectedCustomProductData(quantity:  self.itemQuantity, arrayOfId: custObjectArray,additionalCost:extraPrice)
//        productElement?.quantity =   (productElement?.quantity ?? 0) + self.itemQuantity - 1
        productElement?.addCustomizationData(data: data, key: selectedString)
        print(self.itemQuantity)
         onAddButtonClick == nil ? print("nil") : onAddButtonClick!(self.itemQuantity)
        
        
        
//
//                    customizeIDArray.append("\(menuDetailCustomizeObjectArray[i].customizeID)")
//                    
//                    customizeOptionIDArray.append(menuDetailCustomizeObjectArray[i].customizeSubOptionArray[j].id)
//                    customizeNameArray.append(menuDetailCustomizeObjectArray[i].customizeSubOptionArray[j].customizeOptionName)
//                    customizePriceArray.append(menuDetailCustomizeObjectArray[i].customizeSubOptionArray[j].customizePrice)
//                    additionalCostArray.append(menuDetailCustomizeObjectArray[i].customizeSubOptionArray[j].additionalCost)
//                }
//            }
//        }
//        
//        self.addProductForCart()
//        delegate.reloadTableCell()
        
//        CommonFunctions.shareCommonMethods().ga(withCategory: Category_Menus, action: Action_Customize_Item, label:Label_Add_to_Cart_Clicked , value: nil)
        
//        CommonFunctions.shareCommonMethods().getNavigationControllerOfCurrentlySelectedTab().popViewController(animated: true)
       // 44 CommonFunctions.shareCommonMethods().popViewController(ofSelectedTab: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setNavBar(){
        navigationBar = NavigationView.getNibFile(params:NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "Customize Item", leftButtonImage: #imageLiteral(resourceName: "back"), rightButtonImage: nil) , leftButtonAction: {[weak self] in
            _ = self?.navigationController?.popViewController(animated: true)
            
            }, rightButtonAction: nil)
        self.view.addSubview(navigationBar)
        
        
        
        
    }
    func setCartCountLable(){
        cartPriceLabel.text =  Singleton.sharedInstance.formDetailsInfo.currencyid + "\(NLevelFlowManager.subTotal)"
        cartPriceLabel.letterSpacing = 0.5
        cartPriceLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.priceFontSize)
        cartPriceLabel.textColor = COLOR.LOGIN_BUTTON_TITLE_COLOR
        cartPriceLabel.layer.cornerRadius = 5
        cartPriceLabel.clipsToBounds = true
        addToCartLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.priceFontSize)
        addToCartLabel.textColor = COLOR.LOGIN_BUTTON_TITLE_COLOR
        cartPriceLabel.superview?.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
    }
    
    
    
}

class MenuCustomizableObjects {
    
    var customizeID = Int()
    var customizeName = String()
    var isCheckBox = Bool()
    var customizeSubOptionArray = [(id:String, customizeOptionName:String ,isDefault:Bool,customizePrice:Double, additionalCost:Double)]()
}

// MARK: - ActionButtonDelegate
extension CustomizationViewController: ActionButtonDelegate {
    func actionButtonPressed(type:ButtonPressedType, tag: Int) {
        switch type {
        case .Add:
            self.itemQuantity = itemQuantity + 1
            self.itemsListTableView.reloadData()
        default:
            self.itemQuantity = itemQuantity - 1
            if self.itemQuantity == 0{
                self.navigationController?.popViewController(animated: true )
            }
            self.itemsListTableView.reloadData()
        }
        cartPriceLabel.text = "\(Singleton.sharedInstance.formDetailsInfo.currencyid) \( Singleton.sharedInstance.convertPriceToString(value: totalPrice*Double(itemQuantity)))"
        
    }
    
    
    
}
