//
//  CustomizeItemsDetailTableViewCell.swift
//  Jugnoo Autos
//
//  Created by Sourabh on 27/12/16.
//  Copyright © 2016 Socomo Technologies. All rights reserved.
//

import UIKit

class CustomizeItemsDetailTableViewCell: UITableViewCell {

    // CustomizeItemsDetailCell
    @IBOutlet weak var bottomBgContraint: NSLayoutConstraint!
    @IBOutlet weak var topBgContraint: NSLayoutConstraint!
    
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var bgView: UIView!
    var cornerRect:UIRectCorner = []
    
    @IBOutlet weak var seperatorLine: UILabel!
    @IBOutlet weak var itemSelectionButton: UIButton!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var itemCostLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        
        bgImageView.layoutIfNeeded()
        bgView.layoutIfNeeded()
        
        if bgImageView.layer.sublayers != nil {
            if (bgImageView.layer.sublayers?.count)! > 0 {
                for i in 0 ..< bgImageView.layer.sublayers!.count {
                    if let subView = bgImageView.layer.sublayers![i] as? CAShapeLayer, subView.isKind(of: CAShapeLayer.self) {
                        bgImageView.layer.sublayers!.remove(at: i)
                        break
                    }
                }
            }
        }
        
        if cornerRect.isEmpty == false {
            self.bgView._cornerRadius = 3
        } else {
            self.bgView._cornerRadius = 0
        }
        
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) { 
            let bezierPath = UIBezierPath(roundedRect: self.bgImageView.bounds, byRoundingCorners: self.cornerRect, cornerRadii: CGSize(width: 3, height: 3))
            self.bgImageView.setShadow()
//       55     CommonFunctions.shareCommonMethods().createShadow(of: self.bgImageView, bezierPath: bezierPath, shadowOpacity: 0.5, shadowRadius: 1.0, shadowColor: UIColor.gray.cgColor)
        }
        
       
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
