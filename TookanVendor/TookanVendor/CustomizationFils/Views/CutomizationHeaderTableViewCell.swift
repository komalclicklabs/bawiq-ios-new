//
//  CutomizationHeaderTableViewCell.swift
//  Jugnoo Autos
//
//  Created by Sourabh on 26/12/16.
//  Copyright © 2016 Socomo Technologies. All rights reserved.
//

import UIKit

protocol UpdateQuantityDelegate:class {
    
    func updateQuantity(value:Int)
}
class CutomizationHeaderTableViewCell: UITableViewCell {

    @IBOutlet var iconBgImageView: UIImageView!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var headerTextLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var bottomBgContraint: NSLayoutConstraint!
    @IBOutlet weak var topBgContraint: NSLayoutConstraint!
    
    var selectedSection:Int!
    var quantity:Int!
    var cornerRect:UIRectCorner = []
    
    weak var updateQuantityDelegate:UpdateQuantityDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
//     55   self.plusButton.setImage(#imageLiteral(resourceName: "ic_plus_gradient"), for: .normal)
//        self.minusButton.setImage(#imageLiteral(resourceName: "ic_Minus_gradient"), for: .normal)
        // Initialization code
        
        self.plusButton.setImage(iconAddImage, for: UIControlState.normal)
    }
    
    override func layoutSubviews() {
        
        bgImageView.layoutIfNeeded()
        bgView.layoutIfNeeded()
        
        if bgImageView.layer.sublayers != nil {
            if (bgImageView.layer.sublayers?.count)! > 0 {
                for i in 0 ..< bgImageView.layer.sublayers!.count {
                    if let subView = bgImageView.layer.sublayers![i] as? CAShapeLayer, subView.isKind(of: CAShapeLayer.self) {
                        bgImageView.layer.sublayers!.remove(at: i)
                        break
                    }
                }
            }
        }
        
//   55     if cornerRect.isEmpty == false {
//             Singleton.sharedInstance.roundCorners(cornerRect, radius: 3, stroke: nil, with: bgView)
//        } else {
//            Singleton.sharedInstance.roundCorners(.allCorners, radius: 0, stroke: nil, with: bgView)
//        }
        
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) { 
            let bezierPath = UIBezierPath(roundedRect: self.bgImageView.bounds, byRoundingCorners: self.cornerRect, cornerRadii: CGSize(width: 3, height: 3))
            
//    55        CommonFunctions.shareCommonMethods().createShadow(of: self.bgImageView, bezierPath: bezierPath, shadowOpacity: 0.5, shadowRadius: 1.0, shadowColor: UIColor.gray.cgColor)
        }
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func minusProductClick(_ sender: Any) {
        
        if quantity > 0 {
            
//            CommonFunctions.shareCommonMethods().ga(withCategory: Category_Menus, action: Action_Customize_Item, label:Label_Item_Decreased , value: nil)
            quantity = quantity - 1
        }
        
        updateQuantityDelegate?.updateQuantity(value: quantity)
        self.updateAddPlusMinusButtons(quantity: quantity)
    }
    
    @IBAction func plusProductClick(_ sender: Any) {
        
//        CommonFunctions.shareCommonMethods().ga(withCategory: Category_Menus, action: Action_Customize_Item, label:Label_Item_Increased , value: nil)
        
        quantity = quantity + 1
        
        updateQuantityDelegate?.updateQuantity(value: quantity)
        self.updateAddPlusMinusButtons(quantity: quantity)
    }
   
    func updateAddPlusMinusButtons(quantity:Int) {
        
        self.minusButton.isHidden = false
        self.plusButton.isHidden = false
        self.quantityLabel.text = "\(quantity)"
    }
    
}
