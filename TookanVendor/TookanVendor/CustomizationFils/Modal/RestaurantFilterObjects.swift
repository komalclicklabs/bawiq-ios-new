//
//  RestaurantFilterObjects.swift
//  Jugnoo Autos
//
//  Created by Sourabh on 28/12/16.
//  Copyright © 2016 Socomo Technologies. All rights reserved.
//

import Foundation

class RestaurantFilterObjects {
    
    var availableCuisinesArray = [String]()
    
    var availableDeliveryTimeArray = [Double]()
    var availablePaymentModeArray = [Double]()
    var availableDistanceArray = [Double]()
    
    var isAvailableArray = [Bool]()
    var isClosedArray = [Bool]()
    var availableMinOrderArray = [Double]()
    
    var availablePackingChargesArray = [Double]()
    var availablePopularityArray = [Double]()
    var availablePriceRangeArray = [Double]()
    
    var availableRestaurantPopularityArray = [Double]()
    var availableServiceTaxArray = [Double]()
    var availableVATArray = [Double]()
    
    // MARK: - Parse Filter Json
    
    func parseRestaurantFilterJson(json:[String:Any]) -> Bool {
        
//        self = RestaurantFilterObjects()
        
        if let availablePaymentModeArray = json["applicable_payment_mode"] as? [Double] {
            
            self.availablePaymentModeArray = availablePaymentModeArray
        }
        if let restaurantCuisines = json["cuisines"] as? [String] {
            
            self.availableCuisinesArray = restaurantCuisines
        }
        
        if let availableMinOrderArray = json["minimum_order_amount"] as? [Double] {
            
            self.availableMinOrderArray = availableMinOrderArray
        }
        if let availableDeliveryTimeArray = json["delivery_time"] as? [Double] {
            
            self.availableDeliveryTimeArray = availableDeliveryTimeArray
        }
        if let availableDistanceArray = json["distance"] as? [Double] {
            
            self.availableDistanceArray = availableDistanceArray
        }
        
        if let isAvailableArray = json["is_available"] as? [Bool] {
            
            self.isAvailableArray = isAvailableArray
        }
        if let isClosedArray = json["is_closed"] as? [Bool] {
            
            self.isClosedArray = isClosedArray
        }
        
        if let availablePackingChargesArray = json["packing_charges"] as? [Double] {
            
            self.availablePackingChargesArray = availablePackingChargesArray
        }
        if let availablePopularityArray = json["popularity"] as? [Double] {
            
            self.availablePopularityArray = availablePopularityArray
        }
        if let availablePriceRangeArray = json["price_range"] as? [Double] {
            
            self.availablePriceRangeArray = availablePriceRangeArray
        }
        if let availableRestaurantPopularityArray = json["restaurant_popularity"] as? [Double] {
            
            self.availableRestaurantPopularityArray = availableRestaurantPopularityArray
        }
        if let availableServiceTaxArray = json["service_tax"] as? [Double] {
            
            self.availableServiceTaxArray = availableServiceTaxArray
        }
        if let availableVATArray = json["value_added_tax"] as? [Double] {
            
            self.availableVATArray = availableVATArray
        }
        
        return true
    }
}
