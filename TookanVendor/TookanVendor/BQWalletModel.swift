//
//  BQWalletModel.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 25/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQWalletModel: NSObject {
    var transaction_amount: Double?
    var transaction_id: Int?
    var transaction_mode: String?
    var transaction_order_id: Int?
    var transaction_ref_id: Int?
    var transaction_time: String?
    var transaction_type: String?
    var vendor_id: Int?
    var walletMessage: String?
    
    convenience init(param: [String: Any]) {
        self.init()
        
        if let categoryDescription = param["transaction_amount"] as? Double {
            self.transaction_amount = categoryDescription
        }
        
        if let categoryDescription = param["transaction_type"] as? Int {
            if categoryDescription == 1 {
                self.transaction_type = "Credits Rewarded"
            } else if categoryDescription == 2 {
                self.transaction_type = "Cashback Received"
            } else if categoryDescription == 3 {
                self.transaction_type = "Money added successfully"
            } else if categoryDescription == 5 {
                self.transaction_type = "Refund"
            } else {
                self.transaction_type = "Paid by BWallet"
            }
           // self.transaction_type = categoryDescription
        }
        
        if let categoryDescription = param["transaction_time"] as? String {
            self.transaction_time = self.utcToLocal(date: categoryDescription)
        }
        
        if let categoryDescription = param["transaction_ref_id"] as? Int {
            self.transaction_ref_id = categoryDescription
        }
        
        if let categoryDescription = param["transaction_order_id"] as? Int {
            self.transaction_order_id = categoryDescription
        }
        
        if let categoryDescription = param["transaction_mode"] as? String {
            
            //transaction Mode : 1: CREDITED, 2: DEBITED
            if categoryDescription == "1" {
                self.transaction_mode = "Credits Rewarded"
            } else {
                self.transaction_mode = "Order Transaction"
            }
            
        }
        
        if let categoryDescription = param["transaction_id"] as? Int {
            self.transaction_id = categoryDescription
        }
        
        if let categoryDescription = param["transaction_amount"] as? Double {
            self.transaction_amount = categoryDescription
        }
    }
    
    func utcToLocal(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        //12/01/17 9:30 PM
        if let dt = dateFormatter.date(from: date) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = "dd/MM/yyyy hh:mm a" //09:00pm, 28 Sep 2017
            return dateFormatter.string(from: dt)
        }
        return ""
    }
}
