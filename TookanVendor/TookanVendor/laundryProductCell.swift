//
//  laundryProductCell.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 25/04/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class laundryProductCell: UITableViewCell {

    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var productQuantityLabel: UILabel!
    
    var ProductDetails = CarTypeModal(json: [String : Any]())
    var productQuantity = 0
    weak var delegate : LaundryButtonTapped?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        background.backgroundColor = COLOR.popUpColor
        // Initialization code
        productName.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
        productName.textColor = COLOR.SPLASH_TEXT_COLOR
        priceLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.carTimeLabelSize)
        priceLabel.textColor = COLOR.THEME_FOREGROUND_COLOR
        
        productQuantityLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.extraLarge)
        productQuantityLabel.textColor = COLOR.SPLASH_BACKGROUND_COLOR
        productQuantityLabel.backgroundColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.85)
        productQuantityLabel.isHidden = true
        
        productImage.backgroundColor = UIColor.clear
        
        background.layer.shadowColor = UIColor.black.cgColor
        background.layer.shadowOpacity = 0.3
        background.layer.shadowOffset = CGSize(width: 0, height: 2.2)
        background.layer.shadowRadius = 2.5
        productQuantityLabel.layer.cornerRadius = 5
        productQuantityLabel.clipsToBounds = true
        background.layer.cornerRadius = 5
        productImage.clipsToBounds = true
        productImage.layer.cornerRadius = 5
        self.backgroundColor = UIColor.clear
        
        addButton.setTitle(TEXT.addString, for: UIControlState.normal)
        addButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)
        addButton.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: UIControlState.normal)
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func addButtonAction(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            print("1")
            productQuantity = productQuantity + 1
            if productQuantity == 1{
                sender.setTitle("", for: UIControlState.normal)
                minusButton.setTitle("", for: UIControlState.normal)
                sender.setImage(UIImage(named:"Add"), for: UIControlState.normal)
                minusButton.setImage(#imageLiteral(resourceName: "removeButton"), for: UIControlState.normal)
            }else{
                
            }
        default:
            if productQuantity == 1{
              productQuantity = productQuantity - 1
                sender.setTitle("", for: UIControlState.normal)
                addButton.setTitle(TEXT.addString, for: UIControlState.normal)
                sender.setImage(UIImage(named:""), for: UIControlState.normal)
                addButton.setImage(UIImage(named:""), for: UIControlState.normal)
            }else if productQuantity > 1{
                productQuantity = productQuantity - 1
            }else{
                 productQuantity = 0
            }
            print("2")
        }
        
        showQuantityText(quantity: productQuantity)
        
        
    }
    
    
    func updateQuantity(data:CarTypeModal){
        self.ProductDetails = data
        self.productQuantity = ProductDetails.quantity
        showQuantityText(quantity: ProductDetails.quantity)
    }

    func showQuantityText(quantity:Int){
        self.ProductDetails.quantity = quantity
        if quantity > 0{
            productQuantityLabel.text = "\(quantity)"
            productQuantityLabel.isHidden = false
            addButton.setTitle("", for: UIControlState.normal)
            minusButton.setTitle("", for: UIControlState.normal)
            addButton.setImage(UIImage(named:"Add"), for: UIControlState.normal)
            minusButton.setImage(#imageLiteral(resourceName: "removeButton"), for: UIControlState.normal)
        }else{
            minusButton.setTitle("", for: UIControlState.normal)
            addButton.setTitle(TEXT.addString, for: UIControlState.normal)
            minusButton.setImage(UIImage(named:""), for: UIControlState.normal)
            addButton.setImage(UIImage(named:""), for: UIControlState.normal)
             productQuantityLabel.isHidden = true
        }
        delegate?.plusOrMinusButtonTapped()
    }
    
}
