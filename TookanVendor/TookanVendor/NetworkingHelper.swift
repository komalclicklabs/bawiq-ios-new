//
//  Networking Helper.swift
//  Tookan
//
//  Created by Click Labs on 7/7/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit



class NetworkingHelper: NSObject {
    
    static let sharedInstance = NetworkingHelper()
    
    func autoCompleteSearch(_ searchKeyword:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any])->()) {
        
        let formattedSearchKey = searchKeyword.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let urlString = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(formattedSearchKey)&radius=500&key=\(KEYS.GOOGLE_BROWSER_KEY)"
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        HTTPClient.shared.makeSingletonConnectionWith(method: .GET, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: false, para: nil, baseUrl: urlString, extendedUrl: "") { (responseObject, _, _, statusCode) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            
            guard statusCode == .some(STATUS_CODES.SHOW_DATA),
                let response = responseObject as? [String: Any] else {
                    receivedResponse(false, [:])
                    return
            }
            receivedResponse(true, response)
        }
        
    }
    
    
    
    func commonServerCall(apiName:String,params: [String : AnyObject]?,httpMethod:String,receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        
       
        
        if IJReachability.isConnectedToNetwork() == true {
            sendRequestToServer(apiName, params: params! , httpMethod: httpMethod, isZipped:false) { (succeeded:Bool, response:[String:Any]) -> () in
                DispatchQueue.main.async {
                    print("commonServerCall: \(apiName, response)")
                    if(succeeded){
                        if let status = response["status"] as? Int {
                            switch(status) {
                            case STATUS_CODES.SHOW_DATA, STATUS_CODES.UNAUTHORIZED_FOR_AVAILABILITY:
                                receivedResponse(true, response)
                                break
                                
                            case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                if let message = response["message"] as? String {
                                    receivedResponse(false, ["data":response["data"] as! [String:Any],"status":status, "message":message])
                                } else {
                                    receivedResponse(false, ["data":response["data"] as! [String:Any],"status":status, "message":ERROR_MESSAGE.INVALID_ACCESS_TOKEN])
                                }
                                break
                                
                            case STATUS_CODES.SHOW_MESSAGE:
                                if let message = response["message"] as? String {
                                    receivedResponse(false, ["status":status, "message":message])
                                } else {
                                    receivedResponse(false, ["status":status, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                                }
                                break
                                
                            default:
                                if let message = response["message"] as? String {
                                    receivedResponse(false, ["status":status, "message":message])
                                } else {
                                    receivedResponse(false, ["status":status, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                                }
                                break
                            }
                        } else {
                            receivedResponse(false, ["status":0,"message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                        }
                    } else {
                        receivedResponse(false, ["status":0, "message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                    }
                }
            }
        } else {
            receivedResponse(false, ["status":0, "message":ERROR_MESSAGE.NO_INTERNET_CONNECTION])
        }
    }
    
    
    func uploadImageToServer(_ refImage:UIImage, receivedResponse:@escaping (_ succeeded:Bool,_ response:[String:Any])-> ()) {
        if IJReachability.isConnectedToNetwork() == true {
            uploadingMultipleTask("upload_reference_images", params: [:], isImage: true, imageData: [refImage], imageKey: "ref_image", receivedResponse: { (succeeded, response) -> () in
                print(response)
                DispatchQueue.main.async {
                    
                    
                    if(succeeded == true) {
                        switch(response["status"] as! Int) {
                        case STATUS_CODES.SHOW_DATA:
                            DispatchQueue.main.async(execute: { () -> Void in
                                receivedResponse(true, response)
                            })
                            break
                        case STATUS_CODES.SHOW_MESSAGE:
                            //Singleton.sharedInstance.showAlert(response["message"] as! String!)
                            receivedResponse(false, ["message":response["message"] as! String!])
                            break
                            
                        default:
                            //  Singleton.sharedInstance.showAlert(response["message"] as! String!)
                            receivedResponse(false, ["message":response["message"] as! String!])
                        }
                    } else {
                        // Singleton.sharedInstance.showAlert(ERROR_MESSAGE.SERVER_NOT_RESPONDING)
                        receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                    }
                }
            })
            
        } else {
            // Singleton.sharedInstance.showAlert(ERROR_MESSAGE.NO_INTERNET_CONNECTION)
            receivedResponse(false, ["message":ERROR_MESSAGE.NO_INTERNET_CONNECTION])
        }
    }
    
    
    func checkoutHit(metaData:[Any],pickUpMetaData:[Any],pickUpMetaDataName:String,metaDataTemplateName:String,hasPickup:String,hasDelivery:String, _ receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()){
        self.createTaskHit(autoAssignment: Singleton.sharedInstance.formDetailsInfo.auto_assign!,
                                                customerAddress: Singleton.sharedInstance.createTaskDetail.customerAddress,
                                                customerEmail: Singleton.sharedInstance.createTaskDetail.customerEmail,
                                                customerPhone: Singleton.sharedInstance.createTaskDetail.customerPhone,
                                                customerUsername: Singleton.sharedInstance.createTaskDetail.customerUsername,
                                                latitude: Singleton.sharedInstance.createTaskDetail.latitude,
                                                longitude: Singleton.sharedInstance.createTaskDetail.longitude,
                                                jobDeliveryDatetime: Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime,
                                                hasDelivery: hasDelivery,
                                                hasPickup: hasPickup,
                                                jobDescription: Singleton.sharedInstance.createTaskDetail.jobDescription,
                                                jobPickupAddress: Singleton.sharedInstance.createTaskDetail.jobPickupAddress,
                                                jobPickupEmail: Singleton.sharedInstance.createTaskDetail.jobPickupEmail,
                                                jobPickupPhone: Singleton.sharedInstance.createTaskDetail.jobPickupPhone,
                                                jobPickupDateTime: Singleton.sharedInstance.createTaskDetail.jobPickupDateTime,
                                                jobPickupLatitude: Singleton.sharedInstance.createTaskDetail.jobPickupLatitude,
                                                jobPickupLongitude: Singleton.sharedInstance.createTaskDetail.jobPickupLongitude,
                                                jobPickupName: Singleton.sharedInstance.createTaskDetail.jobPickupName,
                                                domainName: Singleton.sharedInstance.formDetailsInfo.domain_name!,
                                                layoutType: Singleton.sharedInstance.formDetailsInfo.work_flow!,
                                                vendorId: Vendor.current!.id!,
                                                metadata:metaData,
                                                pickupMetadata:pickUpMetaData,
                                                pickupCustomFieldTemplate:pickUpMetaDataName, customFieldTemplate:metaDataTemplateName,isDemo:Vendor.current == nil)
        { (isSuccess, response) in
            receivedResponse(isSuccess, response)
        }
    }
    
    
    
    
    func getPayment(metaData:[Any],pickUpMetaData:[Any],pickUpMetaDataName:String,metaDataTemplateName:String,hasPickup:String,hasDelivery:String, _ receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()){
//        ActivityIndicator.sharedInstance.showActivityIndicator()
        self.createTaskHit(url:API_NAME.getPayment,autoAssignment: Singleton.sharedInstance.formDetailsInfo.auto_assign!,
                                                customerAddress: Singleton.sharedInstance.createTaskDetail.customerAddress,
                                                customerEmail: Singleton.sharedInstance.createTaskDetail.customerEmail,
                                                customerPhone: Singleton.sharedInstance.createTaskDetail.customerPhone,
                                                customerUsername: Singleton.sharedInstance.createTaskDetail.customerUsername,
                                                latitude: Singleton.sharedInstance.createTaskDetail.latitude,
                                                longitude: Singleton.sharedInstance.createTaskDetail.longitude,
                                                jobDeliveryDatetime: Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime,
                                                hasDelivery: "\(hasDelivery)",
            hasPickup: "\(hasPickup)",
            jobDescription: Singleton.sharedInstance.createTaskDetail.jobDescription,
            jobPickupAddress: Singleton.sharedInstance.createTaskDetail.jobPickupAddress,
            jobPickupEmail: Singleton.sharedInstance.createTaskDetail.jobPickupEmail,
            jobPickupPhone: Singleton.sharedInstance.createTaskDetail.jobPickupPhone,
            jobPickupDateTime: Singleton.sharedInstance.createTaskDetail.jobPickupDateTime,
            jobPickupLatitude: Singleton.sharedInstance.createTaskDetail.jobPickupLatitude,
            jobPickupLongitude: Singleton.sharedInstance.createTaskDetail.jobPickupLongitude,
            jobPickupName: Singleton.sharedInstance.createTaskDetail.jobPickupName,
            domainName: Singleton.sharedInstance.formDetailsInfo.domain_name!,
            layoutType: Singleton.sharedInstance.formDetailsInfo.work_flow!,
            vendorId: 22,
            metadata:metaData,
            pickupMetadata:pickUpMetaData,
            pickupCustomFieldTemplate:pickUpMetaDataName,
            customFieldTemplate:metaDataTemplateName,
            withPayment:true,
            payment_method:"\(Singleton.sharedInstance.formDetailsInfo.payementMethod)")
        { (isSuccess, response) in
            receivedResponse(isSuccess, response)
        }
    }
    
    func createTaskHit(url:String = API_NAME.createTask ,autoAssignment:Int, customerAddress:String, customerEmail:String, customerPhone:String, customerUsername:String, latitude:String, longitude:String, jobDeliveryDatetime:String, hasDelivery:String, hasPickup:String, jobDescription:String, jobPickupAddress:String, jobPickupEmail:String, jobPickupPhone:String, jobPickupDateTime:String, jobPickupLatitude:String, jobPickupLongitude:String, jobPickupName:String,domainName:String, layoutType:Int,vendorId:Int, metadata:[Any], pickupMetadata:[Any],pickupCustomFieldTemplate:String, customFieldTemplate:String,withPayment:Bool = false,ammount:String = "0",currency:String = "",payment_method:String = "",card_id:String = "",tags:String = "",tipAmount : String = "0",tipType:String = Singleton.sharedInstance.formDetailsInfo.tipDetails.type.rawValue,isDemo : Bool = false,productsJson:[[String:Any]] = [[String:Any]](), _ receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        var params = [String:Any]()
        params = [
            "access_token":Vendor.current == nil ? "" : Vendor.current!.appAccessToken!,
            "app_access_token":Vendor.current == nil ? "" : Vendor.current!.appAccessToken!,
            "app_version":APP_VERSION,
            "device_token":"",
            "auto_assignment":"\(autoAssignment)",
            "customer_address":customerAddress,
            "customer_email":customerEmail,
            "customer_phone":customerPhone,
            "customer_username":customerUsername,
            "job_delivery_datetime":jobDeliveryDatetime,
            "latitude":latitude,
            "longitude":longitude,
            "has_delivery":"\(hasDelivery)",
            "has_pickup":"\(hasPickup)",
            "job_description":jobDescription,
            "job_pickup_address":jobPickupAddress,
            "job_pickup_email":jobPickupEmail,
            "job_pickup_phone":jobPickupPhone,
            "job_pickup_datetime":jobPickupDateTime,
            "job_pickup_latitude":jobPickupLatitude,
            "job_pickup_longitude":jobPickupLongitude,
            "job_pickup_name":jobPickupName,
            "domain_name":domainName,
            "layout_type":"\(layoutType)",
            "app_device_type":APP_DEVICE_TYPE,
            "vendor_id":"\(vendorId)",
            "timezone":Singleton.sharedInstance.getTimeZone(),
            "meta_data":metadata,
            "pickup_meta_data":pickupMetadata,
            "pickup_custom_field_template":pickupCustomFieldTemplate,
            "custom_field_template":customFieldTemplate,
            "user_id" : Singleton.sharedInstance.formDetailsInfo.user_id!,
            "form_id" : Singleton.sharedInstance.formDetailsInfo.form_id!,
            "vertical" : Singleton.sharedInstance.formDetailsInfo.verticalType.rawValue,
            "tip_amount":tipAmount,
            "tip_type" : tipType,
            "reference_id": Vendor.current == nil ?  AppConfiguration.current.currentReferenceId : Vendor.current!.referenceId,
            "api_key" : Singleton.sharedInstance.formDetailsInfo.apiKey,
            "products" : productsJson
        ]
        let fleetId = Singleton.sharedInstance.createTaskDetail.fleetId
        if !fleetId.isEmpty {
            params["fleet_id"] = fleetId
        }
        
        if withPayment == true{
            params["amount"] = ammount
            params["currency_id"] = currency
            params["payment_method"] = payment_method
            params["card_id"] = card_id
            params["access_id"] = Singleton.sharedInstance.promoAccessId
            params["promo_id"] = Singleton.sharedInstance.promoId
        }
        
        if Singleton.sharedInstance.formDetailsInfo.verticalType == .taxi {
            params["tags"] = tags
        }
        if isDemo == true{
         Singleton.sharedInstance.createTaskDetail.createTaskParamsToSend = params
            receivedResponse(true, ["message":"Order created successfully"])
        }else{
        print(params)
        if IJReachability.isConnectedToNetwork() == true {
            sendRequestToServer(url, params: params as [String : AnyObject], httpMethod: "POST", isZipped:false) { (succeeded:Bool, response:[String:Any]) -> () in
                DispatchQueue.main.async {
                    print(response)
                    if(succeeded){
                        if let status = response["status"] as? Int {
                            switch(status) {
                            case STATUS_CODES.SHOW_DATA:
                                DispatchQueue.main.async(execute: { () -> Void in
                                    receivedResponse(true, response)
                                })
                                break
                                
                            case STATUS_CODES.INVALID_ACCESS_TOKEN:
//                                Singleton.sharedInstance.logoutButtonAction()
//                                Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
                                receivedResponse(false, response)
                                
                            case STATUS_CODES.SHOW_MESSAGE:
                                receivedResponse(false, response)
                                break
                                
                            default:
                                receivedResponse(false, response)
                            }
                        } else {
                            receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                        }
                    } else {
                        receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                    }
                }
            }
        } else {
            receivedResponse(false, ["message":ERROR_MESSAGE.NO_INTERNET_CONNECTION])
        }
    }
    
    
}

}

