//
//  BQStoreStatusVC.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 10/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

protocol BQStoreStatusVCDelegate: class {
    func thanksForPromoCodeApplying()
    func startChatWithDriver()
    func startCallWithDrivr()
}


class BQStoreStatusVC: UIViewController {

    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var imgStoreAvtar: UIImageView!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblDelayedMessage: UILabel!
    @IBOutlet weak var btnThanks: UIButton!
    
    weak var delegate: BQStoreStatusVCDelegate?
    
    var isShowingPromoCodeView = false
    var isShowOrderDelayView = false
    var isShowDriverAssignedView = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isShowingPromoCodeView {
            self.updatePromoCodeUI()
            
        }
        
        if isShowOrderDelayView {
            self.updateOrderDelayViewUI()
            
        }
        
        if isShowDriverAssignedView {
            self.updateDriverAssignedUI()
        }
    }

    private func updatePromoCodeUI() {
        //1.
        //Promo Code
        //Has Been Applied
        //Thanks
        self.btnCall.isHidden = true
        self.btnMessage.isHidden = true
        self.imgStoreAvtar.isHidden = true
        
        self.btnThanks.isHidden = false
        self.btnThanks.layer.cornerRadius = 26.0
        
        lblStatus.text = "Promo Code"
        lblDelayedMessage.text = "Has Been Applied"
    }
    
    private func updateOrderDelayViewUI() {
        //2.
        //Please Wait
        //Your order has been slightly delayed...
        self.btnCall.isHidden = true
        self.btnMessage.isHidden = true
        self.imgStoreAvtar.isHidden = true
        lblDelayedMessage.isHidden = false
        
        self.btnThanks.isHidden = true
        lblStatus.text = "Please Wait"
        lblDelayedMessage.text = "Your order has been slightly delayed..."
    }
    
    private func updateDriverAssignedUI() {
        //3.
        //Store Assigned
        //Call button
        //Message Button
        
        self.btnCall.isHidden = false
        self.btnMessage.isHidden = false
        self.imgStoreAvtar.isHidden = false
        self.lblDelayedMessage.isHidden = true
        self.btnThanks.isHidden = true
        
        lblStatus.text = "Store Assigned"
        lblDelayedMessage.text = "Your order has been slightly delayed..."
    }
    
    @IBAction func messageButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func crossButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func callButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func thanksButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
