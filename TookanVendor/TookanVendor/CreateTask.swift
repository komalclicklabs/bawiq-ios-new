//
//  PickupDeliveryDetailVC.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 11/24/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
import AVFoundation

class PickupDeliveryDetailVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate, DescriptionDelegate, FooterDelegate, PickerDelegate, ErrorDelegate, PickupDeliveryDelegate, ScannerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, ImageCustomDelegate, ImagePreviewDelegate {

    @IBOutlet var skipReviewButton: UIButton!
    @IBOutlet var tableTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var pickupDeliveryBar: UIView!
    //@IBOutlet var pickupDeliveryBarCollectionView: UICollectionView!
    
    var cellInfoArray = [CUSTOM_FIELD_TYPE]()
    var addressRowHeight  :CGFloat = 0.0
    let footerHeight:CGFloat = 80.0
    var descriptonText:String = ""
    var locationAlert:UIAlertController!
    var pickerForDropdown:CustomDropdown!
    var countryCodeAndCountryName = [String:String]()
    var selectedLocale:String!
    var keyboardSize:CGSize = CGSize(width: 0.0, height: 0.0)
    var errorMessageView:ErrorView!
    var footerView:TaskFooterView!
    var topConstraintValueDefault:CGFloat = 10.0
    var topConstraintValueWithPickupDeliveryBar:CGFloat = 60.0
    var collectionCellWidth:CGFloat = 30.0
    var isPickupTask:Bool = false
    var model = CreateTaskModel()
    var scanView:ScanView!
    var imagePicker:UIImagePickerController!
    var imagesPreview:ImagesPreview!
    var fixedFieldCounts = 0
    let skipReviewButtonHeight:CGFloat = 30.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 60
        self.tableView.separatorStyle = .none
        
        self.tableTopConstraint.constant = topConstraintValueDefault
        self.pickupDeliveryBar.isHidden = true
        registerCellsWithTableView()
        
        switch Singleton.sharedInstance.formDetailsInfo.work_flow {
        case WORKFLOW.pickupDelivery.rawValue?:
            cellInfoArray = [CUSTOM_FIELD_TYPE.address, .option, .description,.contact, .email, .phone, .date]
            switch Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag {
            case PICKUP_DELIVERY.pickup.rawValue?:
                Singleton.sharedInstance.createTaskDetail.hasPickup = 1
                Singleton.sharedInstance.createTaskDetail.hasDelivery = 0
                break
            case PICKUP_DELIVERY.delivery.rawValue?:
                Singleton.sharedInstance.createTaskDetail.hasPickup = 0
                Singleton.sharedInstance.createTaskDetail.hasDelivery = 1
                break
            case PICKUP_DELIVERY.both.rawValue?:
                Singleton.sharedInstance.createTaskDetail.hasPickup = 1
                Singleton.sharedInstance.createTaskDetail.hasDelivery = 0
                break
             default:
                break
            }
        break
        case WORKFLOW.appointment.rawValue?, WORKFLOW.fieldWorkforce.rawValue?:
            cellInfoArray = [CUSTOM_FIELD_TYPE.address, .description,.contact, .email, .phone, .startDate, .endDate]
            Singleton.sharedInstance.createTaskDetail.hasPickup = 0
            Singleton.sharedInstance.createTaskDetail.hasDelivery = 1
            break
        default:
            break
        }
        self.fixedFieldCounts = cellInfoArray.count
        Singleton.sharedInstance.resetMetaData()
        Singleton.sharedInstance.resetPickupMetaData()
//        Singleton.sharedInstance.createTaskDetail.pickupMetadata = Singleton.sharedInstance.customFieldDetails
//        Singleton.sharedInstance.createTaskDetail.metaData = Singleton.sharedInstance.customFieldDetails
        
        cellInfoArray = model.setupCustomFieldsInArray(cellArray: cellInfoArray)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(cellInfoArray[0])
        NotificationCenter.default.addObserver(self, selector: #selector(self.locationServiceDisabledAlert), name: NSNotification.Name(rawValue: NOTIFICATION_OBSERVER.locationServicesDisabled), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.setFooterView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
       // self.tableView.contentOffset = CGPoint(x: 0, y: 77)
        print("viewDidAppear")
        self.setPickupDeliveryBar()
    }
    

    //MARK: Pickup/Delivery Bar
    func setPickupDeliveryBar() {
        self.pickupDeliveryBar.layer.shadowOffset = CGSize(width: 2, height: 3)
        self.pickupDeliveryBar.layer.shadowOpacity = 0.7
        self.pickupDeliveryBar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        self.pickupDeliveryBar.layer.shadowRadius = 3
        
        /*------------ Skip review button --------------*/
        self.skipReviewButton.backgroundColor = COLOR.SPLASH_TEXT_COLOR
        self.skipReviewButton.setTitleColor(UIColor.white, for: .normal)
        self.skipReviewButton.setTitle(TEXT.SKIP_TO_REVIEW, for: .normal)
        self.skipReviewButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        self.skipReviewButton.layer.cornerRadius = self.skipReviewButtonHeight / 2
        self.skipReviewButton.addTarget(self, action: #selector(self.skipToReviewAction), for: .touchUpInside)
    }
    
    func registerCellsWithTableView() {
        self.tableView.register(UINib.init(nibName: NIB_NAME.ADDRESS_NIB, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.ADDRESSCELL)
        self.tableView.register(UINib.init(nibName: NIB_NAME.PICKUPDELIVERYOPTION_NIB, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.PICKUPDELIVERYOPTIONCELL)
        self.tableView.register(UINib.init(nibName: NIB_NAME.DESCRIPTION_NIB, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.DESCRIPTION_CELL)
        self.tableView.register(UINib.init(nibName: NIB_NAME.customFieldCell, bundle: Bundle.main), forCellReuseIdentifier: NIB_NAME.customFieldCell)
        self.tableView.register(UINib.init(nibName: NIB_NAME.imageCustomCell, bundle: Bundle.main), forCellReuseIdentifier: NIB_NAME.imageCustomCell)
        //self.pickupDeliveryBarCollectionView.register(UINib(nibName: NIB_NAME.pickupDeliveryBarCell, bundle: nil), forCellWithReuseIdentifier: NIB_NAME.pickupDeliveryBarCell)
        //self.pickupDeliveryBarCollectionView.delegate = self
        //self.pickupDeliveryBarCollectionView.dataSource = self
    }
    
    
    //MARK: UITableView Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellInfoArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row >= self.fixedFieldCounts{
        let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
            if  customField.app_side!.rawValue == 2  {
            return 0
        }
        }
        
        switch cellInfoArray[indexPath.row] {
        case .description:
            return UITableViewAutomaticDimension
        case .contact,.date,.dateCustomField, .datePast, .dateFuture, .dateTime,.dateTimeFuture,.dateTimePast, .startDate, .endDate:
            return UITableViewAutomaticDimension
        case .email, .phone, .image:
            return UITableViewAutomaticDimension
        case .address:
            if let parent = self.parent as? HomeVC{
//                if parent.bottomViewExpanded == false{
//                    return 0
//                }else{
                    return UITableViewAutomaticDimension
//                }
            }
            return UITableViewAutomaticDimension
        case .option:
            switch Singleton.sharedInstance.formDetailsInfo.work_flow {
            case WORKFLOW.pickupDelivery.rawValue?:
                if Singleton.sharedInstance.createTaskDetail.hidePickupDelivery == true {
                    return 0.0
                } else {
                    return 130.0
                }
            
            case WORKFLOW.appointment.rawValue?, WORKFLOW.fieldWorkforce.rawValue?:
                return 0.0
            default:
                return 130.0
            }
        case .text, .number, .emailCustomField, .url, .telephone, .barcode, .dropDown:
            return UITableViewAutomaticDimension
        case .checkbox :
            return 40
        default:
            return 0.0
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            if let parentVC = self.parent  as? HomeVC {
              //  parentVC.openSearchViewWithExpandView()
            }
        default:
            print("didSelectRowAt create task")
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     print(cellInfoArray)
        
        switch cellInfoArray[indexPath.row] {
            
            
            
            
        case CUSTOM_FIELD_TYPE.address:
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.ADDRESSCELL) as! AddressViewCell
        
            //if cellInfoArray[indexPath.row]
            
            cell.setAddress()
            cell.selectionStyle = .none
            return cell
            
        case CUSTOM_FIELD_TYPE.option:
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.PICKUPDELIVERYOPTIONCELL) as! PickUpOptionCell
            cell.selectionStyle = .none
            cell.delegate = self
            cell.setOptionsAsPerConfig()
            return cell
       
        case CUSTOM_FIELD_TYPE.description:
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.DESCRIPTION_CELL) as! DescriptionCell
            cell.fieldType = .description
            cell.descriptionTextView.isEditable = true
            cell.setDescriptionView()
            cell.delegate = self
            cell.selectionStyle = .none
            cell.rightButton.isHidden = false
            return cell
            
        case CUSTOM_FIELD_TYPE.contact:
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.customFieldCell) as! CustomFieldCell
            cell.setNameField()
            cell.customField.isEnabled = true
            cell.selectionStyle = .none
            return cell
            
        case CUSTOM_FIELD_TYPE.email:
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.customFieldCell) as! CustomFieldCell
            cell.setEmailField()
            cell.customField.isEnabled = true
            cell.selectionStyle = .none
            return cell
            
        case CUSTOM_FIELD_TYPE.phone:
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.customFieldCell) as! CustomFieldCell
            cell.setContactField()
            cell.selectionStyle = .none
            cell.customField.isEnabled = true
            cell.countryCodeButton.tag = indexPath.row - self.fixedFieldCounts
            cell.dropdownIcon.tag = indexPath.row - self.fixedFieldCounts
            cell.countryCodeButton.addTarget(self, action: #selector(self.showCountryCode(sender:)), for: UIControlEvents.touchUpInside)
            cell.dropdownIcon.addTarget(self, action: #selector(self.showCountryCode(sender:)), for: UIControlEvents.touchUpInside)
            return cell

        case CUSTOM_FIELD_TYPE.date, .dateCustomField, .datePast, .dateFuture, .dateTime,.dateTimeFuture,.dateTimePast:
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.customFieldCell) as! CustomFieldCell
           
            cell.selectionStyle = .none
            cell.customField.tag = indexPath.row - self.fixedFieldCounts
            switch cellInfoArray[indexPath.row] {
            case .date:
                cell.fieldType = FIELD_TYPE.date
                cell.datePicker.minimumDate = Date()
                cell.datePicker.maximumDate = nil
                cell.datePicker.datePickerMode = .dateAndTime
                cell.datePicker.date = Date()
                
                cell.setDatePicker(label: "", text: "")
                break
            case .dateCustomField:
                cell.fieldType = FIELD_TYPE.dateCustomField
                cell.datePicker.datePickerMode = .date
                cell.datePicker.date = Date()
                cell.datePicker.maximumDate = nil
                cell.datePicker.minimumDate = nil
                let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
                cell.setDatePicker(label: customField.label!.replacingOccurrences(of: "_", with: " "), text: customField.data!)
                break
            case .datePast:
                cell.fieldType = FIELD_TYPE.datePast
                cell.datePicker.datePickerMode = .date
                cell.datePicker.date = Date()
                cell.datePicker.maximumDate = Date()
                cell.datePicker.minimumDate = nil
                let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
                cell.setDatePicker(label: customField.label!.replacingOccurrences(of: "_", with: " "), text: customField.data!)

                break
            case .dateFuture:
                cell.fieldType = FIELD_TYPE.dateFuture
                cell.datePicker.minimumDate = Date()
                cell.datePicker.maximumDate = nil
                cell.datePicker.datePickerMode = .date
                cell.datePicker.date = Date()
                let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
                cell.setDatePicker(label: customField.label!.replacingOccurrences(of: "_", with: " "), text: customField.data!)

                break
            case .dateTime:
                cell.fieldType = FIELD_TYPE.dateTime
                cell.datePicker.datePickerMode = .dateAndTime
                cell.datePicker.date = Date()
                cell.datePicker.maximumDate = nil
                cell.datePicker.minimumDate = nil
                let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
                cell.setDatePicker(label: customField.label!.replacingOccurrences(of: "_", with: " "), text: customField.data!)
                break
            case .dateTimePast:
                cell.fieldType = FIELD_TYPE.dateTimePast
                cell.datePicker.maximumDate = Date()
                cell.datePicker.minimumDate = nil
                cell.datePicker.datePickerMode = .dateAndTime
                cell.datePicker.date = Date()
                let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
                cell.setDatePicker(label: customField.label!.replacingOccurrences(of: "_", with: " "), text: customField.data!)

                break
            case .dateTimeFuture:
                cell.fieldType = FIELD_TYPE.dateTimeFuture
                cell.datePicker.minimumDate = Date()
                cell.datePicker.maximumDate = nil
                cell.datePicker.datePickerMode = .dateAndTime
                cell.datePicker.date = Date()
                let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
                cell.setDatePicker(label: customField.label!.replacingOccurrences(of: "_", with: " "), text: customField.data!)
                break
            default:
                break
            }
            if indexPath.row >= self.fixedFieldCounts{
             let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
                if customField.app_side!.rawValue == 0  || customField.app_side!.rawValue == 2  {
                cell.customField.isEnabled = false
            }else{
                 cell.customField.isEnabled = true
            }
            }else{
               cell.customField.isEnabled = true
            }
             cell.rightButton.isHidden = true
            return cell
            
        case CUSTOM_FIELD_TYPE.startDate:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.customFieldCell) as! CustomFieldCell
            cell.rightButton.isHidden = false
            cell.customField.tag = indexPath.row - self.fixedFieldCounts
            cell.datePicker.minimumDate = Date()
            cell.datePicker.maximumDate = nil
            cell.fieldType = FIELD_TYPE.startDate
            cell.datePicker.datePickerMode = .dateAndTime
            cell.datePicker.date = Date()
            cell.setDatePicker(label: "", text: "")
            cell.selectionStyle = .none
            if indexPath.row >= self.fixedFieldCounts{
            let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
                if customField.app_side!.rawValue == 0  || customField.app_side!.rawValue == 2  {
                cell.customField.isEnabled = false
            }else{
                cell.customField.isEnabled = true
            }
        }else{
                cell.customField.isEnabled = true
        }
         
            return cell
            
        case CUSTOM_FIELD_TYPE.endDate:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.customFieldCell) as! CustomFieldCell
          cell.rightButton.isHidden = false
            cell.customField.tag = indexPath.row - self.fixedFieldCounts
            cell.fieldType = FIELD_TYPE.endDate
            cell.datePicker.minimumDate = Date()
            cell.datePicker.maximumDate = nil
            cell.datePicker.datePickerMode = .dateAndTime
            cell.datePicker.date = Date()
            cell.setDatePicker(label: "", text: "")
            cell.selectionStyle = .none
          
            if indexPath.row >= self.fixedFieldCounts{
                let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
                if customField.app_side!.rawValue == 0  || customField.app_side!.rawValue == 2  {
                    cell.customField.isEnabled = false
                }else{
                    cell.customField.isEnabled = true
                }
            }else{
                cell.customField.isEnabled = true
            }
            
            return cell

        case CUSTOM_FIELD_TYPE.image:
            let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.imageCustomCell) as! ImageCustomCell
            cell.imageCollectionView.tag = indexPath.row - self.fixedFieldCounts
            cell.selectionStyle = .none
            cell.delegate = self
            let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
            let images = customField.data?.jsonObjectArray
            cell.setImageCollectionView(label: customField.label!.replacingOccurrences(of: "_", with: " "), imageArray: images!)
            cell.addImageButton.tag = indexPath.row - self.fixedFieldCounts
            cell.addImageButton.addTarget(self, action: #selector(self.openImagePicker(sender:)), for: UIControlEvents.touchUpInside)
            
            if customField.app_side!.rawValue == 0  || customField.app_side!.rawValue == 2  {
                cell.isForEditing = false
            }else{
                cell.isForEditing = true
            }
            return cell
            
        case .text, .number, .emailCustomField, .url, .telephone, .barcode, .checkbox, .dropDown:
            let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.DESCRIPTION_CELL) as! DescriptionCell
            
            cell.descriptionTextView.tag = indexPath.row - self.fixedFieldCounts
            cell.delegate = self
            cell.selectionStyle = .none
            var customField:CustomFieldDetails!
            switch cellInfoArray[indexPath.row] {
                
            case .text:
                cell.fieldType = .text
                customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
                if customField.label! == "Task_Details" {
                 customField.data = Singleton.sharedInstance.selectedData
                }
                cell.setTextCustomField(label: customField.label!.replacingOccurrences(of: "_", with: " "), text: customField.data!)
                break
            case .number:
                cell.fieldType = .number
                customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
                cell.setNumberCustomField(label: customField.label!.replacingOccurrences(of: "_", with: " "), text: customField.data!)
                break
            case .emailCustomField:
                cell.fieldType = .emailCustomField
                customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
                cell.setEmailCustomField(label:customField.label!.replacingOccurrences(of: "_", with: " "), text: customField.data!)
                break
            case .url:
                cell.fieldType = .url
                customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
                cell.setUrlCustomField(label:customField.label!.replacingOccurrences(of: "_", with: " "), text: customField.data!)
                break
            case .telephone:
                cell.fieldType = .telephone
                customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
                cell.setTelephoneCustomField(label:customField.label!.replacingOccurrences(of: "_", with: " "), text: customField.data!)
                break
            case .barcode:
                cell.fieldType = .barcode
                customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
                cell.setBarcodeCustomField(label:customField.label!.replacingOccurrences(of: "_", with: " "), text: customField.data!)
                break
            case .checkbox:
                cell.fieldType = .checkbox
                customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
                cell.setCheckboxCustomField(label:customField.label!.replacingOccurrences(of: "_", with: " "), text: customField.data!)
                break
            case .dropDown:
                cell.fieldType = .dropDown
                customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:indexPath.row - self.fixedFieldCounts)
                print(customField.input!)
                if customField.data == customField.input {
                    cell.setDropdownCustomField(label:customField.label!.replacingOccurrences(of: "_", with: " "), text: "", dropDownValues: customField.input!)
                } else {
                    cell.setDropdownCustomField(label:customField.label!.replacingOccurrences(of: "_", with: " "), text: customField.data!, dropDownValues: customField.input!)
                }
                break
            default:
                break
            }
            if customField.app_side!.rawValue == 0  || customField.app_side!.rawValue == 2  {
                cell.descriptionTextView.isEditable = false
            }else{
                cell.descriptionTextView.isEditable = true
            }
            
             cell.rightButton.isHidden = true
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func textviewDidBegin() {
        if let parentVC = self.parent  as? HomeVC {
//            parentVC.exapandViewFromDescriptionDidbegin()
        }
    }
    
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showCountryCode(sender:UIButton) {
        self.view.endEditing(true)
        let countryArray = NSMutableArray()
        for locale in NSLocale.locales() {
            countryArray.add(locale.countryName)
            countryCodeAndCountryName[locale.countryName] = locale.countryCode
        }

        pickerForDropdown = UINib(nibName: NIB_NAME.customDropdown, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomDropdown
        pickerForDropdown.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        pickerForDropdown.valueArray =  countryArray
        pickerForDropdown.delegate = self
        self.view.addSubview(pickerForDropdown)
        pickerForDropdown.setPickerView(customFieldRow: sender.tag)
    }
    
    //MARK: Dropdown Picker Delegate
    func dismissPicker() {
        pickerForDropdown = nil
    }
    
    func selectedValueFromPicker(_ value: String, customFieldRowIndex: Int) {
        var code = ""
        if countryCodeAndCountryName[value] != nil {
            let locale = countryCodeAndCountryName[value]!
            self.selectedLocale = locale
            if dialingCode[selectedLocale] != nil {
                code = "+" + dialingCode[selectedLocale]!
            } else {
                code = "+1"
            }
        } else {
            code = "+"
        }
        pickerForDropdown = nil
        if let cell = self.tableView.cellForRow(at: NSIndexPath(row: customFieldRowIndex + self.fixedFieldCounts, section: 0) as IndexPath) as? CustomFieldCell {
            cell.countryCodeButton.setTitle(code, for: UIControlState.normal)
            let contactNumber = code + " " + cell.customField.text!
            switch Singleton.sharedInstance.getTaskTypeTosetDetailsForCreateTask() {
            case PICKUP_DELIVERY.pickup:
                Singleton.sharedInstance.createTaskDetail.jobPickupPhone = contactNumber
                break
            case PICKUP_DELIVERY.delivery:
                Singleton.sharedInstance.createTaskDetail.customerPhone = contactNumber
                break
            case PICKUP_DELIVERY.both:
                Singleton.sharedInstance.createTaskDetail.jobPickupPhone = contactNumber
                Singleton.sharedInstance.createTaskDetail.customerPhone = contactNumber
                break
            case PICKUP_DELIVERY.addDeliveryDetails:
                if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
                    Singleton.sharedInstance.createTaskDetail.jobPickupPhone = contactNumber
                } else if Singleton.sharedInstance.createTaskDetail.hasDelivery == 1 {
                    Singleton.sharedInstance.createTaskDetail.customerPhone = contactNumber
                }
                break
            }
            cell.countryCodeButton.updateConstraintsIfNeeded()
        }
    }

    //MARK: FOOTER VIEW
    func setFooterView() {
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: footerHeight)
        footerView = UINib(nibName: NIB_NAME.taskFooterView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TaskFooterView
        footerView.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: footerHeight)
        footerView.delegate = self
        view.addSubview(footerView)
        let showTutorial =  Singleton.sharedInstance.isForTutorial()
        footerView.createTaskButton.clipsToBounds = false   
        if showTutorial == true{
       
        footerView.createTaskButton.isEnabled = false
        }else{
            footerView.createTaskButton.isEnabled = true
        }
        self.tableView.tableFooterView = view
    }
    
    func updateFooterView() {
        guard footerView != nil else {
            return
        }
        self.footerView.setButtonsAsPerConfig()
    }
    
    //MARK: VALIDATION CHECK
    func isValidData() -> Bool {
        self.view.endEditing(true)
        var phoneNumber:String!
        var address:String!
        var jobDate:String!
        var email:String!
        var latitude:String!
        var longitude:String!
        var deliveryPhoneNumber:String!
        var deliveryAddress:String!
        var deliveryJobDate:String!
        var deliveryEmail:String!
        var deliveryLatitude:String!
        var deliveryLongitude:String!
        var startDate:String!
        var endDate:String!
        var isWorkflowAppointmentFos = false
        var isItPickupAndDeliveryTask = false
        
        switch Singleton.sharedInstance.formDetailsInfo.work_flow {
        case WORKFLOW.pickupDelivery.rawValue?:
            switch Singleton.sharedInstance.getTaskTypeTosetDetailsForCreateTask() {
            case PICKUP_DELIVERY.pickup:
                phoneNumber = Singleton.sharedInstance.createTaskDetail.jobPickupPhone
                address = Singleton.sharedInstance.createTaskDetail.jobPickupAddress
                jobDate = Singleton.sharedInstance.createTaskDetail.jobPickupDateTime
                email = Singleton.sharedInstance.createTaskDetail.jobPickupEmail
                latitude = Singleton.sharedInstance.createTaskDetail.jobPickupLatitude
                longitude = Singleton.sharedInstance.createTaskDetail.jobPickupLongitude
                
                if checkForReqired(ForPickUp: true).showError == true {
                    self.showErrorMessage(error: checkForReqired(ForPickUp: true).forLabel + " Field is mandatory to fill")
                    return false
                }
                
                break
            case PICKUP_DELIVERY.delivery:
                phoneNumber = Singleton.sharedInstance.createTaskDetail.customerPhone
                address = Singleton.sharedInstance.createTaskDetail.customerAddress
                jobDate = Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime
                email = Singleton.sharedInstance.createTaskDetail.customerEmail
                latitude = Singleton.sharedInstance.createTaskDetail.latitude
                longitude = Singleton.sharedInstance.createTaskDetail.longitude
                if checkForReqired(ForPickUp: false).showError == true {
                    self.showErrorMessage(error: checkForReqired(ForPickUp: false).forLabel + " Field is mandatory to fill")
                    return false
                }
                break
            case PICKUP_DELIVERY.both:
                if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
                    phoneNumber = Singleton.sharedInstance.createTaskDetail.jobPickupPhone
                    address = Singleton.sharedInstance.createTaskDetail.jobPickupAddress
                    jobDate = Singleton.sharedInstance.createTaskDetail.jobPickupDateTime
                    email = Singleton.sharedInstance.createTaskDetail.jobPickupEmail
                    latitude = Singleton.sharedInstance.createTaskDetail.jobPickupLatitude
                    longitude = Singleton.sharedInstance.createTaskDetail.jobPickupLongitude
                    
                    if checkForReqired(ForPickUp: true).showError == true {
                        self.showErrorMessage(error: checkForReqired(ForPickUp: true).forLabel + " Field is mandatory to fill")
                        return false
                    }
                    
                } else {
                    phoneNumber = Singleton.sharedInstance.createTaskDetail.customerPhone
                    address = Singleton.sharedInstance.createTaskDetail.customerAddress
                    jobDate = Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime
                    email = Singleton.sharedInstance.createTaskDetail.customerEmail
                    latitude = Singleton.sharedInstance.createTaskDetail.latitude
                    longitude = Singleton.sharedInstance.createTaskDetail.longitude
                    if checkForReqired(ForPickUp: false).showError == true {
                        self.showErrorMessage(error: checkForReqired(ForPickUp: false).forLabel + " Field is mandatory to fill")
                        return false
                    }
                    
                }
                break
            case PICKUP_DELIVERY.addDeliveryDetails:
                phoneNumber = Singleton.sharedInstance.createTaskDetail.jobPickupPhone
                address = Singleton.sharedInstance.createTaskDetail.jobPickupAddress
                jobDate = Singleton.sharedInstance.createTaskDetail.jobPickupDateTime
                email = Singleton.sharedInstance.createTaskDetail.jobPickupEmail
                latitude = Singleton.sharedInstance.createTaskDetail.jobPickupLatitude
                longitude = Singleton.sharedInstance.createTaskDetail.jobPickupLongitude
                
                deliveryPhoneNumber = Singleton.sharedInstance.createTaskDetail.customerPhone
                deliveryAddress = Singleton.sharedInstance.createTaskDetail.customerAddress
                deliveryJobDate = Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime
                deliveryEmail = Singleton.sharedInstance.createTaskDetail.customerEmail
                deliveryLatitude = Singleton.sharedInstance.createTaskDetail.latitude
                deliveryLongitude = Singleton.sharedInstance.createTaskDetail.longitude
                isItPickupAndDeliveryTask = true
                print(Singleton.sharedInstance.createTaskDetail.hasPickup)
                print(Singleton.sharedInstance.createTaskDetail.hasDelivery)
                
                if Singleton.sharedInstance.createTaskDetail.hasPickup == 1{
                    if checkForReqired(ForPickUp: true).showError == true {
                        self.showErrorMessage(error: checkForReqired(ForPickUp: true).forLabel + " Field is mandatory to fill")
                        return false
                    }
                }else{
                    if checkForReqired(ForPickUp: false).showError == true {
                        self.showErrorMessage(error: checkForReqired(ForPickUp: false).forLabel + " Field is mandatory to fill")
                        return false
                    }
                }
                
                
                
                break
            }
            
        case WORKFLOW.appointment.rawValue?, WORKFLOW.fieldWorkforce.rawValue?:
            phoneNumber = Singleton.sharedInstance.createTaskDetail.customerPhone
            address = Singleton.sharedInstance.createTaskDetail.customerAddress
            jobDate = Singleton.sharedInstance.createTaskDetail.jobPickupDateTime
            startDate = Singleton.sharedInstance.createTaskDetail.jobPickupDateTime
            endDate = Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime
            email = Singleton.sharedInstance.createTaskDetail.customerEmail
            latitude = Singleton.sharedInstance.createTaskDetail.latitude
            longitude = Singleton.sharedInstance.createTaskDetail.longitude
            
           
            
            isWorkflowAppointmentFos = true
            if checkForReqired(ForPickUp: false ).showError == true {
                self.showErrorMessage(error: checkForReqired(ForPickUp: false).forLabel + " Field is mandatory to fill")
                return false
            }

            break
        default:
            break
        }
        
        guard  Singleton.sharedInstance.validatePhoneNumber(phoneNumber: phoneNumber) == true else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PHONE_NUMBER)
            return false
        }
        
        guard  Singleton.sharedInstance.validateAddress(address: address, latitude: latitude, longitude: longitude) == true  else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_ADDRESS)
            return false
        }
        
        if email.length > 0 {
            guard Singleton.sharedInstance.validateEmail(email) == true else {
                self.showErrorMessage(error: ERROR_MESSAGE.INVALID_EMAIL)
                return false
            }
        }
        
        if Singleton.sharedInstance.createTaskDetail.pickupMetadata.count > 0 {
            
            for customFieldRow in Singleton.sharedInstance.createTaskDetail.pickupMetadata {
                let customField = customFieldRow as! CustomFieldDetails
                switch customField.data_type {
                case CUSTOM_FIELD_DATA_TYPE.url:
                    if (customField.data?.length)! > 0 {
                        guard Singleton.sharedInstance.verifyUrl(customField.data!) == true else {
                            self.showErrorMessage(error: ERROR_MESSAGE.PLEASE_ENTER_VALID + customField.label!.replacingOccurrences(of: "_", with: " "))
                            return false
                        }
                    }
                    break
                case CUSTOM_FIELD_DATA_TYPE.telephone:
                    if (customField.data?.length)! > 0 {
                        guard Singleton.sharedInstance.validatePhoneNumber(phoneNumber: customField.data!) == true else {
                            self.showErrorMessage(error: ERROR_MESSAGE.PLEASE_ENTER_VALID + customField.label!.replacingOccurrences(of: "_", with: " ").replacingOccurrences(of: "_", with: " "))
                            return false
                        }
                    }
                    
                case CUSTOM_FIELD_DATA_TYPE.email:
                    if (customField.data?.length)! > 0 {
                        guard Singleton.sharedInstance.validateEmail(customField.data!) == true else {
                            self.showErrorMessage(error: ERROR_MESSAGE.PLEASE_ENTER_VALID + customField.label!.replacingOccurrences(of: "_", with: " ").replacingOccurrences(of: "_", with: " "))
                            return false
                        }
                    }
                    break
                default:
                    break
                }
            }
        }
        
        if Singleton.sharedInstance.createTaskDetail.metaData.count > 0 {
            for customFieldRow in Singleton.sharedInstance.createTaskDetail.metaData {
                let customField = customFieldRow as! CustomFieldDetails
                switch customField.data_type {
                case CUSTOM_FIELD_DATA_TYPE.url:
                    if (customField.data?.length)! > 0 {
                        guard Singleton.sharedInstance.verifyUrl(customField.data!) == true else {
                            self.showErrorMessage(error: ERROR_MESSAGE.PLEASE_ENTER_VALID + customField.label!.replacingOccurrences(of: "_", with: " "))
                            return false
                        }
                    }
                    break
                case CUSTOM_FIELD_DATA_TYPE.telephone:
                    if (customField.data?.length)! > 0 {
                        guard Singleton.sharedInstance.validatePhoneNumber(phoneNumber: customField.data!) == true else {
                            self.showErrorMessage(error: ERROR_MESSAGE.PLEASE_ENTER_VALID + customField.label!.replacingOccurrences(of: "_", with: " "))
                            return false
                        }
                    }
                    
                case CUSTOM_FIELD_DATA_TYPE.email:
                    if (customField.data?.length)! > 0 {
                        guard Singleton.sharedInstance.validateEmail(customField.data!) == true else {
                            self.showErrorMessage(error: ERROR_MESSAGE.PLEASE_ENTER_VALID + customField.label!.replacingOccurrences(of: "_", with: " "))
                            return false
                        }
                    }
                    break
                default:
                    break
                }
            }
        }
        
        if APP_DEVICE_TYPE == allAppDeviceType.nexTask && Singleton.sharedInstance.formDetailsInfo.verticalType == .pdaf {
        var packageType = String()
            var quantity = "0"
            
            for i in Singleton.sharedInstance.createTaskDetail.pickupMetadata{
                if let data = i as? CustomFieldDetails{
                    if data.label == NexTaskVariable.package_Type{
                        packageType = data.data!
                    }else if data.label == NexTaskVariable.Quantity{
                        quantity = data.data!
                    }
                    
                }
            }
            
            print(packageType)
            print(quantity)
            if quantity != ""{
            let checkForQuantity = checkForPackageAndQuantity(quantity: Int(quantity)!, packageType: packageType)
            
            
        if checkForQuantity == false    {
            return checkForQuantity
        }
            }
        
        }
        
    
    
        
        
        
        
        
        
        if isWorkflowAppointmentFos == true {
            guard startDate.trimText.isEmpty == false else {
                self.showErrorMessage(error: ERROR_MESSAGE.ENTER_START_TIME)
                return false
            }
            
            guard endDate.trimText.isEmpty == false else {
                self.showErrorMessage(error: ERROR_MESSAGE.ENTER_END_TIME)
                return false
            }
            
            let dateFormatter = DateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"//"MM/dd/yyyy HH:mm"
            let startDateTime = dateFormatter.date(from: startDate)
            let endDateTime = dateFormatter.date(from: endDate)
            let timeInterval = Int((endDateTime?.timeIntervalSince(startDateTime!))!)
            guard Int((startDateTime?.timeIntervalSince(Date()))!) > 0 else {
                self.showErrorMessage(error: ERROR_MESSAGE.INVALID_START_TIME)
                return false
            }
                
            guard timeInterval > 60 else {
                self.showErrorMessage(error: ERROR_MESSAGE.INVALID_END_TIME)
                return false
            }
        } else {
            guard  Singleton.sharedInstance.validateDate(jobDate: jobDate) == true else {
                self.showErrorMessage(error: ERROR_MESSAGE.INVALID_DATETIME)
                return false
            }
            
            if isItPickupAndDeliveryTask == true {
                guard  Singleton.sharedInstance.validatePhoneNumber(phoneNumber: deliveryPhoneNumber) == true else {
                    self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PHONE_NUMBER)
                    return false
                }
                
                guard  Singleton.sharedInstance.validateAddress(address: deliveryAddress, latitude: deliveryLatitude, longitude: deliveryLongitude)  else {
                    self.showErrorMessage(error: ERROR_MESSAGE.INVALID_ADDRESS)
                    return false
                }
                
                guard  Singleton.sharedInstance.comparePickupAndDeliveryDate(pickupDate: jobDate, deliveryDate: deliveryJobDate) == true else {
                    self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PICKUP_DELIVERY_TIME)
                    return false
                }
                
                if deliveryEmail.length > 0 {
                    guard Singleton.sharedInstance.validateEmail(deliveryEmail) == true else {
                        self.showErrorMessage(error: ERROR_MESSAGE.INVALID_EMAIL)
                        return false
                    }
                }
                
                
            }
        }
        return true
    }
    
    
    func checkForPackageAndQuantity(quantity:Int,packageType:String) -> Bool {
        switch packageType {
        case "Small":
            print(quantity)
            if quantity >= 1 && quantity <= 5{
                return true
            }else{
                self.showErrorMessage(error: "Small package type should only have 1 to 5 packages")
                    return false
            }
        case "Medium":
            print(quantity)
            
            if quantity >= 6 && quantity <= 11{
                return true
            }else{
                self.showErrorMessage(error: "Medium package type should only have 6 to 11 packages")
                return false
            }
            
        case "Large":
            if quantity >= 12 {
                return true
            }else{
                self.showErrorMessage(error: "large package type should only have more than 12 packages")
                return false
            }
        default:
            print(quantity)
        }
        
        return true
        
    }
    
    
    
    //MARK: SET FOR DELIVERY TASK
    func setForDeliveryDetails() {
        self.isPickupTask = false
        Singleton.sharedInstance.setAddDelivaryDetails()
        self.updateCellInfoArray()
        self.tableView.reloadData()
        self.updateFooterView()
        //self.updatePickupDeliveryOption(isPickupTask: isPickupTask)
    }
    
    //MARK: SET FOR PICKUP TASK
    func setForPickupDetails() {
        self.isPickupTask = true
        Singleton.sharedInstance.setAddPickupDetails()
        self.updateCellInfoArray()
        self.tableView.reloadData()
        self.updateFooterView()
        //self.updatePickupDeliveryOption(isPickupTask: isPickupTask)
    }
    
    //MARK: EDIT PICKUP TASK
    func editTableWithPickupTask() {
        self.isPickupTask = true
        self.updateCellInfoArray()
        self.tableView.reloadData()
        self.updateFooterView()
       // self.updatePickupDeliveryOption(isPickupTask: isPickupTask)
    }
    
    //MARK: EDIT DELIVERY TASK
    func editTableWithDeliveryTask(){
        self.isPickupTask = false
        self.updateCellInfoArray()
        self.tableView.reloadData()
        self.updateFooterView()
       // self.updatePickupDeliveryOption(isPickupTask: isPickupTask)
    }
    
    //MARK: RELOAD AFTER CREATE TASK
    func reloadAfterCreateTask() {
        self.tableTopConstraint.constant = self.topConstraintValueDefault
        self.pickupDeliveryBar.isHidden = true
        self.isPickupTask = true
        self.updateCellInfoArray()
        self.tableView.reloadData()
        //self.updateFooterView()
        //self.updatePickupDeliveryOption(isPickupTask: isPickupTask)
    }
    
    //MARK: UPDATE MODEL TASK DETAIL
    func updateModelTaskDetail() {
        self.model.saveCurrentTaskDetails()
    }
    
    //MARK: ERROR MESSAGE
    func showErrorMessage(error:String) {
        if errorMessageView == nil {
            errorMessageView = UINib(nibName: NIB_NAME.errorView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
            errorMessageView.delegate = self
            errorMessageView.frame = CGRect(x: 0, y: self.view.frame.height - keyboardSize.height, width: self.view.frame.width, height: HEIGHT.errorMessageHeight)
            self.view.addSubview(errorMessageView)
            errorMessageView.setErrorMessage(message: error,isError: true)
        }
    }
    
    //MARK: ERROR DELEGATE METHOD
    func removeErrorView() {
        self.errorMessageView = nil
    }
    
    
    //MARK: DESCRIPTION DELEGATE
    func updateDescriptionCellHeight(height: CGFloat, descriptionText:String) {
        UIView.setAnimationsEnabled(false) // Disable animations
        self.tableView.beginUpdates()
        self.tableView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    func showBarcodePopup(cell:DescriptionCell) {
        let alertController = UIAlertController(title: TEXT.BARCODE, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let scanButton = UIAlertAction(title: TEXT.SCAN, style: UIAlertActionStyle.default){
            UIAlertAction in
            if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized {
                self.startScanning(index: cell.descriptionTextView.tag)
            } else {
                AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                    if granted == true {
                        self.startScanning(index: cell.descriptionTextView.tag)
                    } else {
                        Singleton.sharedInstance.showAlert(TEXT.CAMERA_NOT_ACCESSIBLE)
                    }
                });
            }
        }
        let manualButton = UIAlertAction(title: TEXT.MANUAL, style: UIAlertActionStyle.default){
            UIAlertAction in
            cell.scannerFlag = false
            cell.descriptionTextView.becomeFirstResponder()
        }
        
        let cancelAction = UIAlertAction(title: TEXT.CANCEL_ALERT, style: UIAlertActionStyle.cancel){
            UIAlertAction in
        }
        
        alertController.addAction(scanButton)
        alertController.addAction(manualButton)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func startScanning(index:Int) {
//        if let parentVC = self.parent  as? HomeVC {
//            UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
//                parentVC.arrowButton.transform = CGAffineTransform.identity
//                parentVC.settingCreateTaskInteractions(interactionType: false)
//                //self.pickUpDetail.tableView.contentOffset = CGPoint(x: 0, y: tableRect.size.height)
//               // parentVC.mergingView.isHidden = true
//              //  parentVC.searchBackView.isHidden = false
//                parentVC.bottomContainerTopConstraint.constant = parentVC.view.frame.height //-self.visibleBottomViewHeight
//                parentVC.navigationBar.transform = CGAffineTransform(translationX: 0, y: 0)
//                parentVC.view.layoutIfNeeded()
//                }, completion: { finished in
//                    //parentVC.pickUpDetail.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: true)
//                    self.scanView = ScanView(frame: parentVC.view.frame)
//                    self.scanView.tag = index
//                    self.scanView.delegate = self
//                    parentVC.view.addSubview(self.scanView)
//                    self.scanView.setScanView(index: index)
//            })
//
//        }
    }

    //MARK: Scanner Delegate
    func decodedOutput(_ value: String, index:Int) {
        print("DecodedOutput = \(value)")
        if let cell = tableView.cellForRow(at: IndexPath(row: index + self.fixedFieldCounts, section: 0)) as? DescriptionCell {
            cell.descriptionTextView.text! = value
            if value.length > 0 {
                cell.placeholder.isHidden = true
            } else {
                cell.placeholder.isHidden = false
            }
            let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:index)
            customField.data = value
            self.tableView.reloadRows(at: [IndexPath(row: index + self.fixedFieldCounts, section: 0)], with: UITableViewRowAnimation.none)
        }
    }
    
    func dismissScannerView() {
//        if let parentVC = self.parent  as? HomeVC {
////            UIView.animate(withDuration: 0.01, delay: 0.3, options: UIViewAnimationOptions.curveEaseInOut, animations: {
////                parentVC.bottomContainerTopConstraint.constant = parentVC.view.frame.height
////                parentVC.view.layoutIfNeeded()
////                }, completion: { finished in
//                   // parentVC.pickUpDetail.tableView.contentOffset = CGPoint(x: 0, y: 0)
//                    UIView.animate(withDuration: 0.8, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 2.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
//                        parentVC.bottomContainerTopConstraint.constant = -SCREEN_SIZE.height + parentVC.topMarginForExpandedBottomView
//                        parentVC.arrowButton.transform = CGAffineTransform(rotationAngle: -CGFloat.pi)
//                        parentVC.navigationBar.transform = CGAffineTransform(translationX: 0, y: -parentVC.navigationViewheight)
//                        parentVC.view.layoutIfNeeded()
//                        }, completion: { finished in
//                           // parentVC.settingCreateTaskInteractions(interactionType: true)
//                            //parentVC.view.bringSubview(toFront: self.arrowButton)
//                            //parentVC.mergingView.isHidden = true
//                           // parentVC.pickUpDetail.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: true)
//                    })
//            //})
//        }
        print("dismiss")
    }

    

    
    func locationServiceDisabledAlert(){
        if(locationAlert == nil) {
            locationAlert = UIAlertController(
                title: "Background Location Access Disabled".localized,
                message: "To proceed further, Please open App's Settings and set Location access to 'Always'.".localized,
                preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel".localized, style: .cancel, handler: { (action) in
                self.locationAlert = nil
            })
            locationAlert.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "Open Settings".localized, style: .default) { (action) in
                if let url = URL(string:UIApplicationOpenSettingsURLString) {
                    UIApplication.shared.openURL(url)
                    self.locationAlert = nil
                }
            }
            locationAlert.addAction(openAction)
            self.present(locationAlert, animated: true, completion: nil)
        }
    }
    
    //MARK: Keyboard Functions
    func keyboardWillShow(_ notification : Foundation.Notification){
        //let info: NSDictionary = (notification as NSNotification).userInfo!
        let value: NSValue = (notification as NSNotification).userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue//info.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        keyboardSize = value.cgRectValue.size
    }
    
    func keyboardWillHide(_ notification: Foundation.Notification) {
        keyboardSize = CGSize(width: 0.0, height: 0.0)
    }

    //MARK: CELL INFO ARRAY
    func updateCellInfoArray() {
        switch Singleton.sharedInstance.formDetailsInfo.work_flow {
        case WORKFLOW.pickupDelivery.rawValue?:
            cellInfoArray = [CUSTOM_FIELD_TYPE.address, .option, .description,.contact, .email, .phone, .date]
            break
        case WORKFLOW.appointment.rawValue?, WORKFLOW.fieldWorkforce.rawValue?:
            cellInfoArray = [CUSTOM_FIELD_TYPE.address, .description,.contact, .email, .phone, .startDate, .endDate]
            break
        default:
            break
        }
        cellInfoArray = model.setupCustomFieldsInArray(cellArray: cellInfoArray)
    }
    
    //MARK: PICKUP DELIVERY DELEGATE
    func pickupDeliveryLayoutType() {
        self.updateCellInfoArray()
      //  let contentOffset = self.tableView.contentOffset
        self.tableView.reloadData()
      //  self.tableView.contentOffset = contentOffset
//       self.tableView.reloadRows(at: [IndexPath(row: self.cellInfoArray.index(of: CUSTOM_FIELD_TYPE.date)!, section: 0)], with: UITableViewRowAnimation.none)
//        let contentOffset = self.tableView.contentOffset
//        UIView.performWithoutAnimation {
//            self.tableView.beginUpdates()
//            self.tableView.reloadData()
//            self.tableView.contentOffset = contentOffset
//            self.tableView.endUpdates()
//        }
        //self.updateFooterView()
    }
    
//    //MARK: PICKUP DELIVERY TOP BAR DELEGATE
//    func updatePickupDeliveryOption(isPickupTask:Bool) {
//        self.isPickupTask = isPickupTask
//
//        if self.isPickupTask == true {
//            Singleton.sharedInstance.createTaskDetail.hasPickup = 1
//            Singleton.sharedInstance.createTaskDetail.hasDelivery = 0
//        } else {
//            Singleton.sharedInstance.createTaskDetail.hasPickup = 0
//            Singleton.sharedInstance.createTaskDetail.hasDelivery = 1
//        }
//        self.pickupDeliveryBarCollectionView.reloadData()
//        self.tableView.reloadData()
//    }
    
    //MARK: FOOTER DELEGATE
    func secondButtonAction() {
        //MARK: CREATE TASK
        guard isValidData() == true else {
            return
        }
        
        
        print(Singleton.sharedInstance.createTaskDetail.pickupMetadata)
        
       
        
        
         getCoreLocation(completionHandler: { (value) in
            Singleton.sharedInstance.distanceBetweenPickUpDelivery = value
        if let parentHome = self.parent as? HomeVC {
            self.updateModelTaskDetail()
            print(self.isPickupTask)
           // parentHome.gotoReviewScreen()
        }
        })
    }
    
    func firstButtonAction() {
        switch Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag {
        case PICKUP_DELIVERY.addDeliveryDetails.rawValue?:
            if isPickupTask == true {
                Singleton.sharedInstance.resetPickupDetails()
            } else {
                Singleton.sharedInstance.resetDeliveryDetails()
            }
            break
        default:
            Singleton.sharedInstance.deleteAllDetails()
            break
        }
        self.tableView.reloadData()
    }
    
    func skipToReviewAction() {
        
        self.model.setSkipData()
        if let parentHome = self.parent as? HomeVC {
            print(isPickupTask)
           // parentHome.goToReviewWithoutHit()
        }
    }
    //MARK: - IMAGE PICKER TO CHANGE IMAGE
    func openImagePicker(sender:UIButton) {
        let alertController = UIAlertController(title: "", message: TEXT.CHOOSE_IMAGE, preferredStyle: .actionSheet)
        let galleryAction = UIAlertAction(title: TEXT.GALLERY, style: .default, handler: { (action) -> Void in
            self.openCameraOrGallery(imagePickerType: .photoLibrary, index:sender.tag)
        })
        let cameraAction = UIAlertAction(title: TEXT.CAMERA, style: .default, handler: { (action) -> Void in
            self.openCameraOrGallery(imagePickerType: .camera, index:sender.tag)
        })
        let cancelAction = UIAlertAction(title: TEXT.CANCEL_ALERT, style: .cancel, handler: { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(galleryAction)
        alertController.addAction(cameraAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func openCameraOrGallery(imagePickerType:UIImagePickerControllerSourceType, index:Int){
        if imagePicker == nil {
            self.imagePicker = UIImagePickerController()
        }
        self.imagePicker.view.tag = index
        if UIImagePickerController.isSourceTypeAvailable(imagePickerType){
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = imagePickerType
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        } else {
            switch (imagePickerType){
            case .camera:
                Singleton.sharedInstance.showAlert(TEXT.CAMERA_NOT_ACCESSIBLE)
            case .photoLibrary:
                Singleton.sharedInstance.showAlert(TEXT.GALLERY_NOT_ACCESSIBLE)
            default:
                break
            }
        }
        
    }
    
    
    func checkForReqired(ForPickUp:Bool)->(showError:Bool,forLabel:String){
        switch ForPickUp {
        case true:
            for i in Singleton.sharedInstance.createTaskDetail.pickupMetadata{
                if let data = i as? CustomFieldDetails{
                    if data.data_type != CUSTOM_FIELD_DATA_TYPE.dropDown{
                        
                        if data.data == "" && data.required == 1{
                            return (true,data.label!.replacingOccurrences(of: "_", with: " "))
                        }
                    }
                        
                    else{
                        if data.data == data.input && data.required == 1{
                            return (true,data.label!.replacingOccurrences(of: "_", with: " "))
                        }
                        
                    }
                }
            }
            
        default:
            for i in Singleton.sharedInstance.createTaskDetail.metaData{
                if let data = i as? CustomFieldDetails{
                    if data.data_type != CUSTOM_FIELD_DATA_TYPE.dropDown{
                        
                        if data.data == "" && data.required == 1{
                            return (true,data.label!.replacingOccurrences(of: "_", with: " "))
                        }
                    }
                        
                    else{
                        if data.data == data.input && data.required == 1{
                            return (true,data.label!.replacingOccurrences(of: "_", with: " "))
                        }
                        
                    }
                }
            }
            break
            
        }
        return (false  ,"")
    }
    
    
    
    //MARK: - IMAGE PICKER CONTROLLER DELEGATES
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: { finished in
            self.imagePicker = nil
        })
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:picker.view.tag)
            customField.imageUploadingStatus.append(false)
            var images = customField.data?.jsonObjectArray
            let currentDateTime = Singleton.sharedInstance.convertDateToString()
            let imagePath = Singleton.sharedInstance.saveImageToDocumentDirectory("\(currentDateTime).png", image: pickedImage)
            images?.append(imagePath)
            customField.data = images?.jsonString
            self.uploadingImages(pickedImage, index: (images?.count)! - 1, imagePath: imagePath, tag:picker.view.tag, customField: customField)
            self.tableView.reloadRows(at: [IndexPath(row: picker.view.tag + self.fixedFieldCounts, section: 0)], with: UITableViewRowAnimation.automatic)
        }
        self.dismiss(animated: true, completion: { finished in
            self.imagePicker = nil
        })
    }
    
    func uploadingImages(_ image:UIImage, index:Int, imagePath:String, tag:Int, customField:CustomFieldDetails) {
        APIManager.sharedInstance.uploadImageToServer(image) { (succeeded, response) -> () in
            DispatchQueue.main.async(execute: { () -> Void in
                if(succeeded == true) {
                    if let responseData = response["data"] as? NSDictionary {
                        if let refImage = responseData["ref_image"] as? String {
                            print(refImage)
                           // let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:tag)
                            var images = customField.data?.jsonObjectArray
                            if index < (images?.count)! {
                                customField.imageUploadingStatus[index] = true
                                images?[index] = refImage
                                customField.data = images?.jsonString
                                Singleton.sharedInstance.deleteImageFromDocumentDirectory(imagePath)
                                Singleton.sharedInstance.allImagesCache.setObject(image, forKey: "\(refImage)" as NSString)
                                if (tag + self.fixedFieldCounts) < self.cellInfoArray.count {
                                    self.tableView.reloadRows(at: [IndexPath(row: tag + self.fixedFieldCounts, section: 0)], with: UITableViewRowAnimation.automatic)
                                }
                            }
                        }
                    }
                } else {
                    let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:tag)
                    var images = customField.data?.jsonObjectArray
                    images?.remove(at: index)
                    customField.imageUploadingStatus.remove(at: index)
                    customField.data = images?.jsonString
                    Singleton.sharedInstance.deleteImageFromDocumentDirectory(imagePath)
                    if (tag + self.fixedFieldCounts) < self.cellInfoArray.count {
                        self.tableView.reloadRows(at: [IndexPath(row: tag + self.fixedFieldCounts, section: 0)], with: UITableViewRowAnimation.automatic)
                    }
                    self.showErrorMessage(error: response["message"] as! String)
                }
            })
        }
    }
    
    //MARK: ImagePreviewDelegate Methods
    func showImagesPreview(_ indexItem:Int, imagesArray:[Any],customFieldRow: Int,headerTypeOfCell:headerType) {
        if(imagesPreview == nil) {
            if let parentVC = self.parent  as? HomeVC {
                imagesPreview = UINib(nibName: NIB_NAME.imagesPreview, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ImagesPreview
                imagesPreview.frame = UIScreen.main.bounds
                imagesPreview.imageCollectionView.frame = CGRect(x: imagesPreview.imageCollectionView.frame.origin.x, y: imagesPreview.imageCollectionView.frame.origin.y, width: self.view.frame.width, height: imagesPreview.imageCollectionView.frame.height)
                imagesPreview.delegate = self
                imagesPreview.imageArray = imagesArray
                imagesPreview.backgroundColor = UIColor.clear
                self.imagesPreview.setCollectionViewWithPage(indexItem,customFieldRow:customFieldRow)
                parentVC.view.addSubview(imagesPreview)
                
                imagesPreview.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
                UIView.animate(withDuration: 0.35, delay: 0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
                    self.imagesPreview.transform = CGAffineTransform.identity
                    }, completion: { finished in
                        self.imagesPreview.backgroundColor = UIColor.white
                })
            }
         }
    }
    
    func dismissImagePreview(customFieldRowIndex:Int) {
        imagesPreview.backgroundColor = UIColor.clear
        UIView.animate(withDuration: 0.35, delay: 0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.imagesPreview.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
            }, completion: { finished in
                self.imagesPreview.removeFromSuperview()
                self.imagesPreview = nil
                self.tableView.reloadRows(at: [IndexPath(row: customFieldRowIndex + self.fixedFieldCounts, section: 0)], with: UITableViewRowAnimation.automatic)
        })
    }
    
    func deleteImageActionFromImages(_ index: Int, customFieldRowIndex:Int,header:headerType) {
        let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:customFieldRowIndex)
        var images = customField.data?.jsonObjectArray
        Singleton.sharedInstance.allImagesCache.removeObject(forKey: images?[index] as! NSString)
        Singleton.sharedInstance.deleteImageFromDocumentDirectory(images?[index] as! String)
        images?.remove(at: index)
        customField.data = images?.jsonString
        self.imagesPreview.confirmationDoneForDeletion()
    }
    
    func getCoreLocation(completionHandler:@escaping (_ value:String)->Void){
        var url = String()
        if !(Singleton.sharedInstance.createTaskDetail.latitude == Singleton.sharedInstance.createTaskDetail.jobPickupLatitude && Singleton.sharedInstance.createTaskDetail.longitude == Singleton.sharedInstance.createTaskDetail.jobPickupLongitude ) && Singleton.sharedInstance.createTaskDetail.longitude != "" && Singleton.sharedInstance.formDetailsInfo.verticalType == .pdaf && APP_DEVICE_TYPE == allAppDeviceType.nexTask{
            
        url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(Singleton.sharedInstance.createTaskDetail.latitude),\(Singleton.sharedInstance.createTaskDetail.longitude)&destination=\(Singleton.sharedInstance.createTaskDetail.jobPickupLatitude),\(Singleton.sharedInstance.createTaskDetail.jobPickupLongitude)&key=AIzaSyDdeHntIgISeD7kZt6DEC6sftKFhnXs4W0"
            
            }else if Singleton.sharedInstance.formDetailsInfo.verticalType == .multipleCategory && APP_DEVICE_TYPE == allAppDeviceType.nexTask{
                
                let metaDataLatLong = Singleton.sharedInstance.getDefaultLatLong()
                
         url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(metaDataLatLong.lat),\(metaDataLatLong.long)&destination=\(Singleton.sharedInstance.createTaskDetail.jobPickupLatitude),\(Singleton.sharedInstance.createTaskDetail.jobPickupLongitude)&key=AIzaSyDdeHntIgISeD7kZt6DEC6sftKFhnXs4W0"
            }else{
                completionHandler("\(0)")
                return
            }
            
        print(url)
        
//        print("https://maps.googleapis.com/maps/api/directions/json?origin=\(Singleton.sharedInstance.createTaskDetail.latitude),\(Singleton.sharedInstance.createTaskDetail.longitude)&destination=\(Singleton.sharedInstance.createTaskDetail.jobPickupLatitude),\(Singleton.sharedInstance.createTaskDetail.jobPickupLongitude)")
        if IJReachability.isConnectedToNetwork() == true{
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.showActivityIndicator()
            }
            sendRequestToServer(baseUrl: url, "", params: [String:AnyObject]() , httpMethod: "POST", isZipped: false) { (isSuccess, Response) in
                DispatchQueue.main.async {
                    ActivityIndicator.sharedInstance.hideActivityIndicator()
                }
                print(Response)
                if let routesValue = Response["routes"] as? [Any]{
                    if  routesValue.count > 0{
                        if let routeData = routesValue[0] as? [String:Any]{
                            if let legsArray = routeData["legs"] as? [Any]{
                                print(legsArray)
                                if legsArray.count > 0{
                                    if let legsData = legsArray[0] as? [String:Any]{
                                        if let distanceData = legsData["distance"] as? [String:Any]{
                                            print(distanceData)
                                            if let distanceValue = distanceData["value"] as? Double{
                                                print(distanceValue)
                                                // self.getEta(value: distanceValue)
                                                completionHandler("\(distanceValue * 0.000621)")
                                            }else if let distanceValue = distanceData["value"] as? String{
                                                // self.getEta(value: Double(distanceValue)!)
                                                completionHandler("\(Double(distanceValue)! * 0.000621)")
                                                
                                                
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        //self.getEta(value: 1.0)
                        //                    self.showErrorMessage(error: "unable to fetch the distance between pick up and ")
                        DispatchQueue.main.async {
                            self.showErrorMessage(error: "unable to fetch the distance between pick up and drop off location")
                        }
                    }
                }
            }
            print(time)
            
        }else{
            self.showErrorMessage(error:ERROR_MESSAGE.NO_INTERNET_CONNECTION )
        }
        }

  


}





