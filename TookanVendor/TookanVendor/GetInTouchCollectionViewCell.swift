//
//  GetInTouchCollectionViewCell.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 09/01/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class GetInTouchCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var getInTouchLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        headingLabel.text = TEXT.TUTORIALHEADING
        headingLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.medium)
        contentLabel.text = TEXT.TUTORIALCONTENTDEL
        contentLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.smallest)
        getInTouchLabel.text = TEXT.GET_IN_TOUCH
        getInTouchLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.medium)
        
    }
    
    
    
    
    
    
    
    
}
