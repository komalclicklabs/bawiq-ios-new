//
//  MenuNavigationView.swift
//  StoreFront
//
//  Created by cl-macmini-45 on 04/10/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class MenuNavigationView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!
    private var menuAction: (() -> Void)?
    
    class func getNibFile(withHeight:CGFloat = HEIGHT.menuNavigationHeight , leftButtonAction: (() -> Void)?) -> MenuNavigationView {
        let navBar = frameworkBundle.loadNibNamed(NIB_NAME.menuNavigationView, owner: self, options: nil)?.first as! MenuNavigationView
        navBar.setFrame(withHeight: withHeight)
        navBar.setButtonActions(leftButtonAction: leftButtonAction)
        //navBar.setUiAttributes()
        return navBar
    }
    
    func setFrame(withHeight:CGFloat = HEIGHT.jugnooHeaderHeight) {
//        self.setShadow()
        frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: withHeight)
    }
    
    private func setButtonActions(leftButtonAction: (() -> Void)?) {
        self.menuAction = leftButtonAction
    }
    
    @IBAction func menuButtonAction(_ sender: UIButton) {
        self.menuAction?()
    }
    
    @IBAction func searchButtonAction(_ sender: UIButton) {
        //self.leftButtonAction?()
    }
    
    @IBAction func filterButtonAction(_ sender: UIButton) {
        //self.leftButtonAction?()
    }
}
