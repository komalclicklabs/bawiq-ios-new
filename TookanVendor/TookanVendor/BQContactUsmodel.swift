//
//  BQContactUsmodel.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 18/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQContactUsmodel: NSObject {

    var contact_address: String?
    var contact_email: String?
    var contact_lat: String?
    var contact_long: String?
    var contact_number: String?
    var creation_datetime: String?
    var user_id: Int?
    var website_link: String?
    
    convenience init(param: [String: Any]) {
        self.init()
       
        if let categoryDescription = param["website_link"] as? String {
            self.website_link = categoryDescription
        }
        if let categoryDescription = param["user_id"] as? Int {
            self.user_id = categoryDescription
        }
        if let categoryDescription = param["creation_datetime"] as? String {
            self.creation_datetime = categoryDescription
        }
        if let categoryDescription = param["contact_number"] as? String {
            self.contact_number = categoryDescription
        }
        if let categoryDescription = param["contact_long"] as? String {
            self.contact_long = categoryDescription
        }
        if let categoryDescription = param["contact_lat"] as? String {
            self.contact_lat = categoryDescription
        }
        if let categoryDescription = param["contact_email"] as? String {
            self.contact_email = categoryDescription
        }
        if let categoryDescription = param["contact_address"] as? String {
            self.contact_address = categoryDescription
        }
        
    }
    
    
    
}
