//
//  ForgotPassVC.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 11/23/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class ForgotPassVC: UIViewController,NavigationDelegate,CustomFieldDelegate,ErrorDelegate {
    
    let buttonHeight:CGFloat = 50.0
    var navigationBar:NavigationView!
    var titleView:TitleView!
    var emailField:CustomTextField!
    var errorMessageView:ErrorView!
    let topMarginOfTextField:CGFloat = 20.0
    var keyboardSize:CGSize = CGSize(width: 0.0, height: 0.0)
    @IBOutlet weak var enterEmailAddressLabel: UILabel!
    @IBOutlet weak var sendLinkButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        self.setNavigationBar()
        self.setTitleView()
        self.view.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
        enterEmailAddressLabel.setLabelWithFontColorText(yourtext: TEXT.FORGOT_ENTER_EMAIL_FOR_LINK_LABEL, yourColor: UIColor.darkGray, fontSize: 14.5, fontName: FONT.light)
        self.setSendLinkButtonButton()
    }
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        setEmailField()
        
        self.animateTextField()
    }
    
    func setSendLinkButtonButton() {
        self.sendLinkButton.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
        self.sendLinkButton.setCornerRadius(radius: buttonHeight / 2)
        self.sendLinkButton.setShadow()
        self.sendLinkButton.setAttributedTitle(Singleton.sharedInstance.setAttributeTitleWithOrWithoutArrow(title: TEXT.SEND_LINK_BUTTON, isArrow: false, yourImage: IMAGE.iconSigninSigninpage), for: .normal)
    }
    
    //MARK: - ANIMATE TEXTFIELDS
    func animateTextField() {
        Singleton.sharedInstance.animateToIdentity(view: self.emailField, delayTime: 0.4)
    }
    
    //MARK: - CHECK VALIDATIONS
    func checkValidation() {
        let email = (self.emailField.textField.text?.trimText)!
        guard Singleton.sharedInstance.validateEmail(email) == true else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_EMAIL,isError: true)
            return
        }
        let forgotParams = [ "email": email, "login_type":"1", "app_device_type":APP_DEVICE_TYPE ]
        self.serverRequest(yourParams: forgotParams as [String : AnyObject])
    }
    
    //MARK: - SEND LINK ACTION
    @IBAction func sendLinkAction(_ sender: UIButton) {
        checkValidation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    //MARK: NAVIGATION BAR
    func setNavigationBar() {
        navigationBar = UINib(nibName: NIB_NAME.navigation, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! NavigationView
        navigationBar.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: HEIGHT.navigationHeight)
        navigationBar.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        navigationBar.delegate = self
        self.view.addSubview(navigationBar)
    }
    //MARK: NAVIGATION DELEGATE METHODS
    func backAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    //MARK:TITLE VIEW
    func setTitleView() {
        titleView = UINib(nibName: NIB_NAME.titleView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TitleView
        titleView.frame = CGRect(x: 0, y: HEIGHT.navigationHeight, width: self.view.frame.width, height: HEIGHT.titleHeight)
        titleView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        self.view.addSubview(titleView)
        self.titleView.setTitle(title: TEXT.FORGOT_PASS_TITLE_, boldTitle: TEXT.FORGOT_PASS_BOLD)
    }
    func setEmailField() {
        self.emailField = UINib(nibName: NIB_NAME.customTextField, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomTextField
        self.emailField.delegate = self
        self.emailField.fieldType = FIELD_TYPE.email
        self.emailField.frame = CGRect(x: 0, y: 200, width: self.view.frame.width, height: HEIGHT.textFieldHeight)
        self.view.addSubview(self.emailField)
        self.emailField.setImageForDifferentStates(inactive: IMAGE.iconEmailInActive, placeholderText: TEXT.YOUR_EMAIL, isPasswordField: false, unlock: nil)
        self.emailField.alpha = 0.0
        self.emailField.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
        self.emailField.textField.keyboardType = UIKeyboardType.emailAddress
        self.emailField.textField.returnKeyType = UIReturnKeyType.next
    }
    
    //MARK: SERVER REQUEST
    func serverRequest(yourParams:[String:AnyObject]) {
        
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.serverCall(apiName: API_NAME.forgotPass, params: yourParams, httpMethod: HTTP_METHOD.POST) { (isSuccess, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSuccess {
                    self.emailField.textField.text = ""
                   // Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
                    self.showErrorMessage(error: response["message"] as? String ?? "", isError: false)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
                        _ = self.navigationController?.popViewController(animated: true)
                    })
                }else{
                self.showErrorMessage(error: response["message"] as? String ?? "",isError: true)
                }
            }
        }
    }
    
    
    //MARK: ERROR MESSAGE
    func showErrorMessage(error:String,isError:Bool) {
        if errorMessageView == nil {
            errorMessageView = UINib(nibName: NIB_NAME.errorView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
            errorMessageView.delegate = self
            errorMessageView.frame = CGRect(x: 0, y: self.view.frame.height - keyboardSize.height, width: self.view.frame.width, height: HEIGHT.errorMessageHeight)
            self.view.addSubview(errorMessageView)
            errorMessageView.setErrorMessage(message: error,isError: isError)
        }
    }
    
    //MARK: ERROR DELEGATE METHOD
    func removeErrorView() {
        self.errorMessageView.removeFromSuperview()
        self.errorMessageView = nil
    }
    
    
    //MARK: Keyboard Functions
    func keyboardWillShow(_ notification : Foundation.Notification){
        //let info: NSDictionary = (notification as NSNotification).userInfo!
        let value: NSValue = (notification as NSNotification).userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue//info.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        keyboardSize = value.cgRectValue.size
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func customFieldShouldReturn(fieldType: FIELD_TYPE) {
        
    }
    
    func keyboardWillHide(_ notification: Foundation.Notification) {
        keyboardSize = CGSize(width: 0.0, height: 0.0)
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
