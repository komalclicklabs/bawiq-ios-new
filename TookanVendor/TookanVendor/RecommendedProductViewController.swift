//
//  RecommendedProductViewController.swift
//  TookanVendor
//
//  Created by Harshit Parikh on 14/09/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class RecommendedProductViewController: UIViewController, NavigationDelegate {

    var isPullToRefreshActive: Bool = false
    var refreshControl = UIRefreshControl()
    fileprivate var skip: Int = 0
    fileprivate var limit: Int = 10
    var isPaginationRequired: Bool = false
    var products = [Products]()
    fileprivate var itemCellView: ProductCellView?
    fileprivate var itemSelectedIndex: Int = -1
    var navigationBar: NavigationView!
    var isSponseredProduct: Bool = false
    var categorySelected: Category?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noProductFoundLbl: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var totalAmountLbl: UILabel!
    @IBOutlet weak var cartBottomView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noProductFoundLbl.isHidden = true
        self.setNavigationBar()
        self.setupTableView()
        self.addPulltoRefresh()
    }
    
    @IBAction func nextBtnAction(_ sender: Any) {
        gotoCart()
    }
    
    private func setNavigationBar() {
        //Use for showing two right bar buttons on navigation bar
        
        if isSponseredProduct {
            guard let category = self.categorySelected, let subcategoryName = category.subCategoryName else {
                return
            }
            navigationBar = NavigationView.getNibFile(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: subcategoryName, leftButtonImage: #imageLiteral(resourceName: "BackButtonWhite"), rightButtonImage: #imageLiteral(resourceName: "cartButton"))
                , leftButtonAction: {[weak self] in
                    self?.backAction()
                    //self?.navigationController?.popViewController(animated: true)
                }, rightButtonAction: {[weak self] in
                    self?.gotoCart()
            })
            
            navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
            navigationBar.delegate = self
            self.view.addSubview(navigationBar)
        } else {
            navigationBar = NavigationView.getNibFile(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "Would you also like to buy ?", leftButtonImage: #imageLiteral(resourceName: "BackButtonWhite"), rightButtonImage: #imageLiteral(resourceName: "cartButton"))
                , leftButtonAction: {[weak self] in
                    self?.backAction()
                    //self?.navigationController?.popViewController(animated: true)
                }, rightButtonAction: {[weak self] in
                    self?.gotoCart()
            })
            
            navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
            navigationBar.delegate = self
            self.view.addSubview(navigationBar)
        }
    }
    
    func backAction() {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is SSASideMenu {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func setUserCurrentLocation() {
        print("setUserCurrentLocation")
    }
    
    
    func searchText(text: String) {
        print(text)
    }
    
    private func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "ProductCell", bundle: nil), forCellReuseIdentifier: "ProductCell")
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 200
        if isSponseredProduct {
            guard let category = self.categorySelected else {
                return
            }
            self.getProducts(category: category, { (success, products, paginationRequired) in
                if success {
                    guard let _products = products else {
                        return
                    }
                    self.isPaginationRequired = paginationRequired
                    if !paginationRequired {
                        self.products = _products
                    } else {
                        self.products += _products
                    }
                    //self.products = _products
                    self.tableView.reloadData()
                }
                
            })
        } else {
            self.getAllRecommendedProducts()
        }
    }
    private func addPulltoRefresh() {
        //Add pullto refresh
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = .white
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refreshControl)
    }
    
    @objc func pullToRefresh() {
        isPullToRefreshActive = true
        self.products.removeAll()
        self.reset()
        if isSponseredProduct {
            guard let category = self.categorySelected else {
                return
            }
            self.getProducts(category: category, { (success, products, paginationRequired) in
                if success {
                    guard let _products = products else {
                        return
                    }
                    self.isPaginationRequired = paginationRequired
                    if !paginationRequired {
                        self.products = _products
                    } else {
                        self.products += _products
                    }
                    //self.products = _products
                    self.tableView.reloadData()
                }
                
            })
        } else {
            self.getAllRecommendedProducts()
        }
        
        
    }
    
    func reset() {
        skip = 0
        limit = 10
    }
    
    func paginationHit() {
        if self.isPaginationRequired == true {
            skip += 10
            if isSponseredProduct {
                guard let category = self.categorySelected else {
                    return
                }
                self.getProducts(category: category, { (success, products, paginationRequired) in
                    if success {
                        guard let _products = products else {
                            return
                        }
                        self.isPaginationRequired = paginationRequired
                        if !paginationRequired {
                            self.products = _products
                        } else {
                            self.products += _products
                        }
                        //self.products = _products
                        self.tableView.reloadData()
                    }
                    
                })
            } else {
                self.getAllRecommendedProducts()
            }
        }
    }
    
    func getAllRecommendedProducts() {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        self.getRecommendedProducts(limit: limit, skip: skip) { (success, products, paginationRequired) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if success {
                    guard let _products = products else {
                        return
                    }
                    self.isPaginationRequired = paginationRequired
                    if !paginationRequired {
                        self.products = _products
                    } else {
                        self.products += _products
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
func getRecommendedProducts(limit: Int = 10, skip: Int = 0, _ receivedResponse: @escaping (_ succeeded: Bool, _ response:[Products]?, _ paginationRequired: Bool) -> ()){
    guard let lat = BQBookingModel.shared.latitude else {
        return
    }
    guard let long = BQBookingModel.shared.longitude else {
        return
    }
    
    let params: [String : Any] = [
        "limit": limit,
        "skip": skip,
        "app_access_token": Vendor.current!.appAccessToken!,
        "latitude": "\(lat)",
        "longitude": "\(long)"
        // ,"product_id": "27584"
    ]
    
    
    print("Get recommended products param: ", params)
    
    NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getRecommendedProducts,
                                                     params: params as [String : AnyObject],
                                                     httpMethod: "POST") { (succeeded, response) in
                                                        
                                                        if self.refreshControl.isRefreshing {
                                                            self.refreshControl.endRefreshing()
                                                        }
                                                        
                                                        DispatchQueue.main.async {
                                                            if succeeded {
                                                                guard let data = response["data"] as? [String: Any], let products = data["data"] as? [[String: Any]] else {
                                                                    receivedResponse(false,nil, false)
                                                                    return
                                                                }
                                                                guard let recommendedCount = data["count"] as? Int else {
                                                                    return
                                                                }
                                                                                                                                    guard let totalPrice = data["total_price"] as? Double else {
                                                                                                                                        return
                                                                                                                                    }
                                                                                                                                    self.totalAmountLbl.text = String(format: "%.2f", totalPrice)
                                                                let stringJson = products.jsonString
                                                                let productsData = stringJson.data(using: .utf8)
                                                                do {
                                                                    let productsArray = try JSONDecoder().decode([Products].self, from: productsData!)
                                                                    self.isPaginationRequired =  recommendedCount > productsArray.count
                                                                    receivedResponse(true, productsArray,self.isPaginationRequired)
                                                                    
                                                                } catch {
                                                                    print(error.localizedDescription)
                                                                    receivedResponse(false,nil,false)
                                                                }
                                                            } else {
                                                                receivedResponse(false,nil,false)
                                                            }
                                                        }
        }
                                                        
    }
    
    func getProducts(category: Category,limit: Int = 10, skip: Int = 0, _ receivedResponse: @escaping (_ succeeded: Bool, _ response:[Products]?, _ paginationRequired: Bool) -> ()){
        if let lat = BQBookingModel.shared.latitude, let long = BQBookingModel.shared.longitude {
            var params = [String: Any]()
            let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
            
            if !isGuestUser {
                params["access_token"] = Vendor.current!.appAccessToken!
            }
            params["user_type"] = 0
            params["limit"] = limit
            params["skip"] = skip
            params["category_id"] = category.id
            params["sub_category_id"] = category.subCategoryId
            params["lattitude"] = lat
            params["longitude"] = long
            
            print("Get products param: ", params)
            
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getProducts,
                                                             params: params as [String : AnyObject],
                                                             httpMethod: "POST") { (succeeded, response) in
                                                                if self.refreshControl.isRefreshing {
                                                                    self.refreshControl.endRefreshing()
                                                                }
                                                                DispatchQueue.main.async {
                                                                    if succeeded {
                                                                        guard let data = response["data"] as? [String: Any], let products = data["data"] as? [[String: Any]] else {
                                                                            receivedResponse(false,nil, false)
                                                                            return
                                                                        }
                                                                        guard let productCount = data["iTotalRecords"] as? Int else {
                                                                            return
                                                                        }
                                                                        guard let totalPrice = data["total_price"] as? Double else {
                                                                            return
                                                                        }
                                                                        self.totalAmountLbl.text = String(format: "%.2f", totalPrice)
                                                                        let stringJson = products.jsonString
                                                                        let productsData = stringJson.data(using: .utf8)
                                                                        do {
                                                                            let productsArray = try JSONDecoder().decode([Products].self, from: productsData!)
                                                                            self.isPaginationRequired =  productCount > productsArray.count
                                                                            receivedResponse(true, productsArray,self.isPaginationRequired)

                                                                        } catch {
                                                                            print(error.localizedDescription)
                                                                            receivedResponse(false,nil,false)
                                                                        }
                                                                    } else {
                                                                        receivedResponse(false,nil,false)
                                                                    }
                                                                }
                                                                
            }
        }
    }
    
    
}


extension RecommendedProductViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.products.count > 0 {
            self.noProductFoundLbl.isHidden = true
            return self.products.count
        } else {
            self.noProductFoundLbl.isHidden = false
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as? ProductCell else {
            return UITableViewCell()
        }
        
        let cellDataModel: Products = self.products[indexPath.row]
        cell.leftProductDescription.isHidden = true
        cell.productDescription.isHidden = false
        cell.favBtn.isHidden = false
        cell.delegate = self
        cell.contentView.backgroundColor = .clear
        cell.stepperView.isHidden = false
        cell.deleteButton.isHidden = true
        cell.discountImage.isHidden = true
        cell.setupDataForSubCategoryProducts(data: cellDataModel)
        
        return cell
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if (tableView.contentOffset.y + tableView.frame.size.height) >= tableView.contentSize.height - 20 {
            self.paginationHit()
        }
    }
    
}

extension RecommendedProductViewController: ProductCellDelegate {
    func productCellAddButtonTapped(cell: ProductCell) {
        if let index = tableView.indexPath(for: cell) {
            self.updateView(view: cell, index: index.row, add: true, dobString: "")
        }
    }
    
    func productCellDeleteButtonTapped(cell: ProductCell) {
        if let index = tableView.indexPath(for: cell) {
            self.updateView(view: cell, index: index.row, add: false, dobString: "")
        }
    }
    
    func productCellRemoveCategoryButtonTapped(cell: ProductCell) {
        
    }
    
    func productCellFavButtonTapped(cell: ProductCell, isfavStatus: Bool) {
        if let index = tableView.indexPath(for: cell) {
            self.markFavUnfavProduct(view: cell, index: index.row)
        }
    }
}

extension RecommendedProductViewController: BQSelectDateTimeVCDelegate {
    
    func markFavUnfavProduct(view: ProductCellView, index: Int) {
        let product = self.products[index]
        ActivityIndicator.sharedInstance.showActivityIndicator()
        product.markFavUnfavApi { (success, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if success {
                if let status = self.products[index].is_fav, status == 1 {
                    self.products[index].is_fav = 0
                } else {
                    self.products[index].is_fav = 1
                }
                self.tableView.reloadData()
//                self.getAllRecommendedProducts()
            }
        }
    }
    
    func updateView(view: ProductCellView, index: Int, add: Bool, dobString: String = "") {
        if add {
            addProductToCart(product: products[index],view: view, index: index, dobString: dobString)
        } else {
            deleteProductFromCart(product: products[index],view: view, index: index)
        }
    }
    
    
    func addProductToCart(product: Products, view: ProductCellView, index: Int, dobString: String = "") {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        product.addProduct(dobString: dobString) { (success, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if success {
                view.updateValue(added: true)
                if let count = self.products[index].inCartCount {
                    self.products[index].inCartCount = count + 1
                }
                
                if let totalPrice = response!["total_price"] as? Double, let cartCount = response!["cart_count"] as? Int {
                    print(totalPrice, cartCount)
                    self.refreshCell(at: index, isAdded: true, cartCount: cartCount, totalPrice: Float(totalPrice))
                }
            } else {
                if let data = response!["message"] as? String {
                    self.selectDOB()
                }
            }
            
        }
    }
    
    func deleteProductFromCart(product: Products, view: ProductCellView, index: Int) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        product.deleteProduct { (success, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if success {
                view.updateValue(added: false)
                if let count = self.products[index].inCartCount {
                    self.products[index].inCartCount = count - 1
                }
                if let totalPrice = response!["total_price"] as? Double, let cartCount = response!["cart_count"] as? Int {
                    print(totalPrice, cartCount)
                    self.refreshCell(at: index, isAdded: false, cartCount: cartCount, totalPrice: Float(totalPrice))
                }
            }
        }
    }
    
    func refreshCell(at index: Int, isAdded: Bool, cartCount: Int, totalPrice: Float) {
        
        
        if cartCount > 0 {
            nextBtn.isUserInteractionEnabled = true
            cartBottomView.isHidden = false
        } else {
            nextBtn.isUserInteractionEnabled = false
            cartBottomView.isHidden = false
        }
        
        self.totalAmountLbl.text = String(format: "%.2f", totalPrice)
        //String(totalPrice)
        //        if subCategoryTableView != nil {
        //            subCategoryTableView.reloadData()
        //        }
        
        //        let indexPath = IndexPath(row: index, section: 0)
        //        subCategoryTableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func selectDOB() {
        let selectDateVC = BQSelectDateTimeVC(nibName: "BQSelectDateTimeVC", bundle: nil)
        selectDateVC.modalPresentationStyle = .overCurrentContext
        if let vc = UIApplication.shared.keyWindow?.topMostWindowController {
            selectDateVC.delegate = self
            selectDateVC.isFromAddItem = true
            selectDateVC.messageStr = "You must be 21 years above to add this product."
            self.present(selectDateVC, animated: true, completion: nil)
        }
    }
    
    func bookingDate(date: String) {
        if self.itemSelectedIndex > -1 {
            if let view = self.itemCellView {
                self.updateView(view: view, index: self.itemSelectedIndex, add: true, dobString: date)
                print("Your age is greater than 21", date)
            }
        }
    }
    
    func showError(message: String) {
        Singleton.sharedInstance.showAlert(message)
    }
    
    fileprivate func gotoCart() {
        
        if isSponseredProduct {
            self.getRecommendedProducts(limit: 10, skip: 0) { (success, products, false) in
                DispatchQueue.main.async {
                    ActivityIndicator.sharedInstance.hideActivityIndicator()
                    if success {
                        if let result = products {
                            if result.count > 0 {
                                if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "RecommendedProductViewController") as? RecommendedProductViewController {
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            } else {
                                if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartSegmentViewController") as? CartSegmentViewController {
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        } else {
                            if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartSegmentViewController") as? CartSegmentViewController {
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    } else {
                        if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartSegmentViewController") as? CartSegmentViewController {
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            }
        } else {
            if self.cartBottomView.isHidden {
                if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQNoItemsInCartViewVC") as? BQNoItemsInCartViewVC {
                    self.navigationController?.present(vc, animated: true, completion: nil)
                }
            } else {
                if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartSegmentViewController") as? CartSegmentViewController {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
        }
    }
}

