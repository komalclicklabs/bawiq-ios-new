//
//  Product.swift
//  TookanVendor
//
//  Created by Vishal on 14/02/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import Foundation

struct Banners {
    var banner_id: Int?
    var category_id: Int?
    var category_image: String?
    var is_default: Bool = false
    
    init(data: [String: Any]) {
        if let bannerId = data["banner_id"] as? Int {
            self.banner_id = bannerId
        }
        if let bannerId = data["category_id"] as? Int {
            self.category_id = bannerId
        }
        if let bannerId = data["category_image"] as? String {
            self.category_image = bannerId
        }
        if let is_default = data["is_default"] as? Bool {
            self.is_default = is_default
        }
    }
}

//struct ProductsModel: Decodable {
//    var count: Int
//    var allProductsData: [Products]
//    
//    var isPaginationRequired: Bool {
//        return count > allProductsData.count
//    }
//    
//    private enum CodingKeys: String, CodingKey {
//        case count = "iTotalRecords"
//        case allProductsData = "data"
//    }
//}


struct Products: Codable, ProductCellModel {
    var discount_percentage: Int?
    var discounted_price: Float?
    let measuring_unit: String?
    let pack_size: String?
    let product_id : Int?
    let product_code_no : String?
    let barcode_no : String?
    let productImage : String?
    let productName : String?
    let productDescription : String?
    let category_id : Int?
    let sub_category_id : Int?
    let productPrice : Double?
    let is_enabled : Int?
    let is_deleted : Int?
    let is_above_21 : Int?
    //let measuring_unit : String?
    let brand_name : String?
    //let pack_size : String?
    let sub_category_name : String?
    let sub_category_image : String?
    let sub_category_description : String?
    let category_name : String?
    let category_description : String?
    let category_image : String?
    var inCartCount: Int? = 0
    var is_fav: Int? = 0
//    static var shared = Products()
//    var productCount: Int  = 0
    
    enum CodingKeys: String, CodingKey {
        case product_id = "product_id"
        case product_code_no = "product_code_no"
        case barcode_no = "barcode_no"
        case productImage = "image_url"
        case productName = "name"
        case productDescription = "description"
        case category_id = "category_id"
        case sub_category_id = "sub_category_id"
        case productPrice = "price"
        case is_enabled = "is_enabled"
        case is_deleted = "is_deleted"
        case is_above_21 = "is_above_21"
        case measuring_unit = "measuring_unit"
        case brand_name = "brand_name"
        case pack_size = "pack_size"
        case sub_category_name = "sub_category_name"
        case sub_category_image = "sub_category_image"
        case sub_category_description = "sub_category_description"
        case category_name = "category_name"
        case category_description = "category_description"
        case category_image = "category_image"
        case inCartCount = "quantity" //"inCartCount"
        case discount_percentage = "discount_percentage"
        case discounted_price = "discounted_price"
        case is_fav = "is_fav"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        product_id = try values.decodeIfPresent(Int.self, forKey: .product_id)
        product_code_no = try values.decodeIfPresent(String.self, forKey: .product_code_no)
        barcode_no = try values.decodeIfPresent(String.self, forKey: .barcode_no)
        productImage = try values.decodeIfPresent(String.self, forKey: .productImage)
        productName = try values.decodeIfPresent(String.self, forKey: .productName)
        productDescription = try values.decodeIfPresent(String.self, forKey: .productDescription)
        category_id = try values.decodeIfPresent(Int.self, forKey: .category_id)
        sub_category_id = try values.decodeIfPresent(Int.self, forKey: .sub_category_id)
        productPrice = try values.decodeIfPresent(Double.self, forKey: .productPrice)
        is_enabled = try values.decodeIfPresent(Int.self, forKey: .is_enabled)
        is_deleted = try values.decodeIfPresent(Int.self, forKey: .is_deleted)
        is_above_21 = try values.decodeIfPresent(Int.self, forKey: .is_above_21)
        measuring_unit = try values.decodeIfPresent(String.self, forKey: .measuring_unit)
        discount_percentage = try values.decodeIfPresent(Int.self, forKey: .discount_percentage)
        brand_name = try values.decodeIfPresent(String.self, forKey: .brand_name)
        pack_size = try values.decodeIfPresent(String.self, forKey: .pack_size)
        sub_category_name = try values.decodeIfPresent(String.self, forKey: .sub_category_name)
        sub_category_image = try values.decodeIfPresent(String.self, forKey: .sub_category_image)
        sub_category_description = try values.decodeIfPresent(String.self, forKey: .sub_category_description)
        category_name = try values.decodeIfPresent(String.self, forKey: .category_name)
        category_description = try values.decodeIfPresent(String.self, forKey: .category_description)
        category_image = try values.decodeIfPresent(String.self, forKey: .category_image)
        inCartCount = try values.decodeIfPresent(Int.self, forKey: .inCartCount)
        discounted_price = try values.decodeIfPresent(Float.self, forKey: .discounted_price)
        is_fav = try values.decodeIfPresent(Int.self, forKey: .is_fav)
    }
    
    
    func addProduct(dobString: String = "", receivedResponse: @escaping (_ succeeded: Bool, _ response:[String: Any]?) -> ()){
        guard let lat = BQBookingModel.shared.latitude else {
            return
        }
        guard let long = BQBookingModel.shared.longitude else {
            return
        }
        var params: [String : Any] = [
            "product_id": self.product_id!,
            "app_access_token": Vendor.current!.appAccessToken!,
            "lattitude": lat,
            "longitude": long]
        if dobString != "" {
            params["birthDate"] = dobString
        }
        print("Add product params", params)
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.addProduct,
                                                         params: params as [String : AnyObject],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                            DispatchQueue.main.async {
                                                                if succeeded {
                                                                    guard let data = response["data"] as? [String: Any] else {
                                                                        receivedResponse(false,nil)
                                                                        return
                                                                    }
                                                                    receivedResponse(true, data)
                                                                } else {
                                                                    receivedResponse(false,response)
                                                                }
                                                            }
                                                            
        }
    }
    
    func deleteProduct(receivedResponse: @escaping (_ succeeded: Bool, _ response:[String: Any]?) -> ()){
        let params: [String : Any] = [
            "product_id": self.product_id!,
            "app_access_token": Vendor.current!.appAccessToken!]
        
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.deleteProduct,
                                                         params: params as [String : AnyObject],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                            DispatchQueue.main.async {
                                                                if succeeded {
                                                                    guard let data = response["data"] as? [String: Any] else {
                                                                        receivedResponse(false,nil)
                                                                        return
                                                                    }
                                                                    receivedResponse(true, data)
                                                                } else {
                                                                    receivedResponse(false,nil)
                                                                }
                                                            }
        }
    }
    
    func markFavUnfavApi(receivedResponse: @escaping (_ succeeded: Bool, _ response:[String: Any]?) -> ()){
        var favValue = Int()
        if let status = self.is_fav, status == 1 {
           favValue = 0
        } else {
            favValue = 1
        }
        let params: [String : Any] = [
            "product_id": "\(self.product_id!)",
            "app_access_token": Vendor.current!.appAccessToken!,
            "is_fav": favValue]
        
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.favUnfavProduct,
                                                         params: params as [String : AnyObject],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                            DispatchQueue.main.async {
                                                                if succeeded {
                                                                    guard let data = response["data"] as? [String: Any] else {
                                                                        receivedResponse(false,nil)
                                                                        return
                                                                    }
                                                                    receivedResponse(true, data)
                                                                } else {
                                                                    receivedResponse(false,nil)
                                                                }
                                                            }
        }
    }
}


struct CartProducts: Decodable, ProductCellModel {
    var is_fav: Int? = 0
    var productPrice: Double?
    var brand_name: String?
    var pack_size: String?
    var cartId: Int?
    var discount_percentage: Int?
    var productImage: String?
    var productDescription: String?
    var is_above_21: Int?
    var measuring_unit: String?
    var product_id: Int?
    var productName: String?
    var inCartCount: Int? 
    var discounted_price: Float?
    
    enum CodingKeys: String, CodingKey {
        case brand_name = "brand_name"
        case cartId = "cart_id"
        case pack_size = "pack_size"
        case productDescription = "description"
        case discount_percentage = "discount_percentage"
        case discounted_price = "discounted_price"
        case productImage = "image_url"
        case is_above_21 = "is_above_21"
        case measuring_unit = "measuring_unit"
        case product_id = "product_id"
        case productName = "product_name"
        case inCartCount = "quantity"
        case totalPrice = "total_price"
        case productPrice = "unit_price"
        case is_fav = "is_fav"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cartId = try values.decodeIfPresent(Int.self, forKey: .cartId)
        pack_size = try values.decodeIfPresent(String.self, forKey: .pack_size)
        productImage = try values.decodeIfPresent(String.self, forKey: .productImage)
        productDescription = try values.decodeIfPresent(String.self, forKey: .productDescription)
        productPrice = try values.decodeIfPresent(Double.self, forKey: .productPrice)
        discount_percentage = try values.decodeIfPresent(Int.self, forKey: .discount_percentage)
        discounted_price = try values.decodeIfPresent(Float.self, forKey: .discounted_price)
        brand_name = try values.decodeIfPresent(String.self, forKey: .brand_name)
        is_above_21 = try values.decodeIfPresent(Int.self, forKey: .is_above_21)
        measuring_unit = try values.decodeIfPresent(String.self, forKey: .measuring_unit)
        product_id = try values.decodeIfPresent(Int.self, forKey: .product_id)
        productName = try values.decodeIfPresent(String.self, forKey: .productName)
        inCartCount = try values.decodeIfPresent(Int.self, forKey: .inCartCount)
        is_fav = try values.decodeIfPresent(Int.self, forKey: .is_fav)
    }
    
    func addProduct(receivedResponse: @escaping (_ succeeded: Bool, _ response:[String: Any]?) -> ()) {
        guard let lat = BQBookingModel.shared.latitude else {
            return
        }
        guard let long = BQBookingModel.shared.longitude else {
            return
        }
        var params: [String : Any] = [
            "product_id": self.product_id!,
            "app_access_token": Vendor.current!.appAccessToken!,
            "lattitude": lat,
            "longitude": long]
        
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.addProduct,
                                                         params: params as [String : AnyObject],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                            DispatchQueue.main.async {
                                                                if succeeded {
                                                                    guard let data = response["data"] as? [String: Any] else {
                                                                        receivedResponse(false,nil)
                                                                        return
                                                                    }
                                                                    receivedResponse(true, data)
                                                                } else {
                                                                    receivedResponse(false,nil)
                                                                }
                                                            }
                                                            
        }
    }
    
    func removeCategoryFormTheCart(receivedResponse: @escaping (_ succeeded: Bool, _ response:[String: Any]?) -> ()) {
        let params: [String : Any] = [
            "product_id": self.product_id!,
            "app_access_token": Vendor.current!.appAccessToken!,
            "deleteAll": 1]
        
        NetworkingHelper.sharedInstance.commonServerCall(apiName: "vendor_delete_cart_items",
                                                         params: params as [String : AnyObject],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                            DispatchQueue.main.async {
                                                                if succeeded {
                                                                    guard let data = response["data"] as? [String: Any] else {
                                                                        receivedResponse(false,nil)
                                                                        return
                                                                    }
                                                                    receivedResponse(true, data)
                                                                } else {
                                                                    receivedResponse(false,nil)
                                                                }
                                                            }
                                                            
        }
    }
    
    func deleteProduct(receivedResponse: @escaping (_ succeeded: Bool, _ response:[String: Any]?) -> ()){
        let params: [String : Any] = [
            "product_id": self.product_id!,
            "app_access_token": Vendor.current!.appAccessToken!]
        
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.deleteProduct,
                                                         params: params as [String : AnyObject],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                            DispatchQueue.main.async {
                                                                if succeeded {
                                                                    guard let data = response["data"] as? [String: Any] else {
                                                                        receivedResponse(false,nil)
                                                                        return
                                                                    }
                                                                    receivedResponse(true, data)
                                                                } else {
                                                                    receivedResponse(false,nil)
                                                                }
                                                            }
                                                            
        }
    }
    
    
    
    
    
    
    
    
    
    static func getCartProducts(receivedResponse: @escaping (_ succeeded: Bool, _ response:[CartProducts]?, _ totalPrice: Float?) -> ()) {
        guard let lat = BQBookingModel.shared.latitude else {
            return
        }
        guard let long = BQBookingModel.shared.longitude else {
            return
        }
        let params: [String : Any] = [
            "app_access_token": Vendor.current!.appAccessToken!,
            "latitude": lat,
            "longitude": long]
        
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getProductscart,
                                                         params: params as [String : AnyObject],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                            DispatchQueue.main.async {
                                                                if succeeded {
                                                                    
                                                                    guard let data = response["data"] as? [String: Any], let products = data["data"] as? [[String: Any]] else {
                                                                        receivedResponse(false,nil, 0)
                                                                        return
                                                                    }
                                                                    let stringJson = products.jsonString
                                                                    let productsData = stringJson.data(using: .utf8)
                                                                    do {
                                                                        let productsArray = try JSONDecoder().decode([CartProducts].self, from: productsData!)
                                                                        if let totalPrice = data["total_price"] as? Double {
                                                                            receivedResponse(true, productsArray, Float(totalPrice))

                                                                        }
                                                                                                                                    } catch {
                                                                        print(error.localizedDescription)
                                                                        receivedResponse(false,nil, 0)
                                                                    }
                                                                } else {
                                                                    receivedResponse(false,nil, 0)
                                                                }
                                                            }
                                                            
        }
    }
}
