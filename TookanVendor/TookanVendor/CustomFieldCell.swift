//
//  CustomFieldCell.swift
//  
//
//  Created by cl-macmini-45 on 28/11/16.
//
//

import UIKit

class CustomFieldCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet var validStatus: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var customField: UITextField!
    @IBOutlet var countryCodeButton: UIButton!
    @IBOutlet var underline: UIView!
    @IBOutlet var rightButton: UIButton!
    @IBOutlet var customFieldTopConstraint: NSLayoutConstraint!
    @IBOutlet var dropdownIcon: UIButton!
    @IBOutlet var leadingConstraint: NSLayoutConstraint!
    
    
    var fieldType:FIELD_TYPE!
    var inactiveImage:UIImage!
   // var activeImage:UIImage!
   // var filledImage:UIImage!
    var unlockImage:UIImage!
    
    let datePicker  = UIDatePicker()
    let cellOriginY:CGFloat = 26.0
    let defaultTopConstraint:CGFloat = 37.0
    let topConstraintWithoutTitle:CGFloat = 12.0
    let defaultLeftConstraint:CGFloat = -50.0
    let leftConstraintWithCountryCode:CGFloat = 15.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        /*---------- header ---------------*/
        titleLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        titleLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        
        /*---------- underline ----------------*/
        underline.backgroundColor = COLOR.SPLASH_LINE_COLOR

        /*----------- Text field --------------*/
        customField.textColor = COLOR.SPLASH_TEXT_COLOR
        customField.font = UIFont(name: FONT.ultraLight, size: FONT_SIZE.large)
        customField.delegate = self
        customField.tintColor = COLOR.THEME_FOREGROUND_COLOR
        
        /*------------ Country Code -------------*/
        countryCodeButton.titleLabel?.font = UIFont(name: FONT.ultraLight, size: FONT_SIZE.large)
        countryCodeButton.setTitleColor(COLOR.SPLASH_TEXT_COLOR, for: UIControlState.normal)
        let code:String!
        let selectedLocale = Singleton.sharedInstance.currentDialingCode().uppercased()
        if(selectedLocale.length > 0) {
            if dialingCode[selectedLocale] != nil {
                code = "+" + dialingCode[selectedLocale]!
            } else {
                code = "+1"
            }
        } else {
            code = "+1"
        }
        countryCodeButton.setTitle(code, for: UIControlState.normal)
    }


    func setNameField() {
        /*------------ Custom Field --------------*/
        self.titleLabel.text = TEXT.CONTACT
        self.leadingConstraint.constant = defaultLeftConstraint
        self.customFieldTopConstraint.constant = defaultTopConstraint
        self.titleLabel.isHidden = false
        self.countryCodeButton.isHidden = true
        self.dropdownIcon.isHidden = true
        self.fieldType = FIELD_TYPE.name
        self.setImageForDifferentStates(inactive: IMAGE.iconNameInactive, placeholderText: TEXT.NAME, isPasswordField: false, unlock: nil)
        self.customField.keyboardType = UIKeyboardType.default
        self.customField.returnKeyType = UIReturnKeyType.done
        self.customField.autocapitalizationType = .words
        self.customField.inputView = nil
        
        switch Singleton.sharedInstance.getTaskTypeTosetDetailsForCreateTask() {
        case PICKUP_DELIVERY.pickup:
            self.customField.text = Singleton.sharedInstance.createTaskDetail.jobPickupName
            break
        case PICKUP_DELIVERY.delivery:
            self.customField.text = Singleton.sharedInstance.createTaskDetail.customerUsername
            break
        case PICKUP_DELIVERY.both:
            self.customField.text = Singleton.sharedInstance.createTaskDetail.jobPickupName
            break
        case PICKUP_DELIVERY.addDeliveryDetails:
            if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
                self.customField.text = Singleton.sharedInstance.createTaskDetail.jobPickupName
            } else if Singleton.sharedInstance.createTaskDetail.hasDelivery == 1 {
                self.customField.text = Singleton.sharedInstance.createTaskDetail.customerUsername
            }
            break
        }
    }
   
    func setEmailField() {
        self.leadingConstraint.constant = defaultLeftConstraint
        self.customFieldTopConstraint.constant = topConstraintWithoutTitle
        self.countryCodeButton.isHidden = true
        self.dropdownIcon.isHidden = true
        self.titleLabel.isHidden = true
        self.fieldType = FIELD_TYPE.email
        self.setImageForDifferentStates(inactive: IMAGE.iconEmailInActive, placeholderText: TEXT.EMAIL, isPasswordField: false, unlock: nil)
        self.customField.keyboardType = UIKeyboardType.emailAddress
        self.customField.returnKeyType = UIReturnKeyType.done
        self.customField.inputView = nil
        
        
        switch Singleton.sharedInstance.getTaskTypeTosetDetailsForCreateTask() {
        case PICKUP_DELIVERY.pickup:
            self.customField.text = Singleton.sharedInstance.createTaskDetail.jobPickupEmail
            break
        case PICKUP_DELIVERY.delivery:
            self.customField.text = Singleton.sharedInstance.createTaskDetail.customerEmail
            break
        case PICKUP_DELIVERY.both:
            self.customField.text = Singleton.sharedInstance.createTaskDetail.jobPickupEmail
            break
        case PICKUP_DELIVERY.addDeliveryDetails:
            if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
                self.customField.text = Singleton.sharedInstance.createTaskDetail.jobPickupEmail
            } else if Singleton.sharedInstance.createTaskDetail.hasDelivery == 1 {
                self.customField.text = Singleton.sharedInstance.createTaskDetail.customerEmail
            }
            break
        }
        
        if self.customField.text?.length == 0 {
            self.setValidStatusHide()
        }

    }
    
    func setContactField() {
        self.leadingConstraint.constant = leftConstraintWithCountryCode
        self.customFieldTopConstraint.constant = topConstraintWithoutTitle
        self.titleLabel.isHidden = true
        self.countryCodeButton.isHidden = false
        self.dropdownIcon.isHidden = false
        self.fieldType = FIELD_TYPE.contact
        self.setImageForDifferentStates(inactive: IMAGE.iconContactUnfilled, placeholderText: TEXT.CONTACT_NUMBER, isPasswordField: false, unlock: nil)
        self.customField.keyboardType = UIKeyboardType.numberPad
        self.customField.returnKeyType = UIReturnKeyType.done
        self.customField.inputView = nil
        var contactNumber = ""
        
        
        switch Singleton.sharedInstance.getTaskTypeTosetDetailsForCreateTask() {
        case PICKUP_DELIVERY.pickup:
            contactNumber = Singleton.sharedInstance.createTaskDetail.jobPickupPhone
            break
        case PICKUP_DELIVERY.delivery:
            contactNumber = Singleton.sharedInstance.createTaskDetail.customerPhone
            break
        case PICKUP_DELIVERY.both:
            contactNumber = Singleton.sharedInstance.createTaskDetail.jobPickupPhone
            break
        case PICKUP_DELIVERY.addDeliveryDetails:
            if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
                contactNumber = Singleton.sharedInstance.createTaskDetail.jobPickupPhone
            } else if Singleton.sharedInstance.createTaskDetail.hasDelivery == 1 {
                contactNumber = Singleton.sharedInstance.createTaskDetail.customerPhone
            }
            break
        }
        
        if contactNumber.length > 1 {
            let code = contactNumber.components(separatedBy: " ")[0]
            countryCodeButton.setTitle(code, for: UIControlState.normal)
            let number = contactNumber.components(separatedBy: " ")[1]
            self.customField.text = number
        } else {
            let code:String!
            let selectedLocale = Singleton.sharedInstance.currentDialingCode().uppercased()
            if(selectedLocale.length > 0) {
                if dialingCode[selectedLocale] != nil {
                    code = "+" + dialingCode[selectedLocale]!
                } else {
                    code = "+1"
                }
            } else {
               code = "+1"
            }
            countryCodeButton.setTitle(code, for: UIControlState.normal)
            self.customField.text = ""
        }
    }
    
    func setDatePicker(label:String, text:String) {
        self.leadingConstraint.constant = defaultLeftConstraint
        self.customFieldTopConstraint.constant = defaultTopConstraint
        self.countryCodeButton.isHidden = true
        self.dropdownIcon.isHidden = true
        self.titleLabel.isHidden = false
        self.customField.inputView = datePicker
        self.customField.clearButtonMode = .never
        self.datePicker.addTarget(self, action: #selector(self.datePickerChanged(sender:)), for: .valueChanged)
        self.customField.text = ""
        
        
        switch fieldType! {
        case .date, .startDate, .endDate:
            switch Singleton.sharedInstance.getTaskTypeTosetDetailsForCreateTask() {
            case PICKUP_DELIVERY.pickup:
                self.customField.text = Singleton.sharedInstance.createTaskDetail.jobPickupDateTime.dateFromString(withFormat: "dd MMM yyyy, hh:mm a")
                break
            case PICKUP_DELIVERY.delivery:
                switch self.fieldType! {
                case FIELD_TYPE.date:
                    self.customField.text = Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime.dateFromString(withFormat: "dd MMM yyyy, hh:mm a")
                    break
                case FIELD_TYPE.startDate:
                    self.customField.text = Singleton.sharedInstance.createTaskDetail.jobPickupDateTime.dateFromString(withFormat: "dd MMM yyyy, hh:mm a")
                    break
                case FIELD_TYPE.endDate:
                    self.customField.text = Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime.dateFromString(withFormat: "dd MMM yyyy, hh:mm a")
                    break
                default:
                    break
                }
                
                break
            case PICKUP_DELIVERY.both:
                self.customField.text = Singleton.sharedInstance.createTaskDetail.jobPickupDateTime.dateFromString(withFormat: "dd MMM yyyy, hh:mm a")
                break
            case PICKUP_DELIVERY.addDeliveryDetails:
                if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
                    self.customField.text = Singleton.sharedInstance.createTaskDetail.jobPickupDateTime.dateFromString(withFormat: "dd MMM yyyy, hh:mm a")
                } else if Singleton.sharedInstance.createTaskDetail.hasDelivery == 1 {
                    self.customField.text = Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime.dateFromString(withFormat: "dd MMM yyyy, hh:mm a")
                }
                break
            }
            
            switch self.fieldType! {
            case FIELD_TYPE.date:
                if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
                    self.titleLabel.text = TEXT.PICK_BEFORE
                } else {
                    self.titleLabel.text = TEXT.DELIVER_BEFORE
                }
                break
            case FIELD_TYPE.startDate:
                self.titleLabel.text = TEXT.START_TIME
                break
            case FIELD_TYPE.endDate:
                self.titleLabel.text = TEXT.END_TIME
                break
            default:
                break
            }
            break
        
        case .dateCustomField, .dateFuture, .datePast, .dateTime, .dateTimePast, .dateTimeFuture:
            self.titleLabel.text = label
            self.customField.text = text
            self.customField.attributedPlaceholder = NSAttributedString(string: TEXT.ENTER + " " + label, attributes: [NSForegroundColorAttributeName: COLOR.PLACEHOLDER_COLOR])
            break
        default:
            break
        }
        
        self.setImageForDifferentStates(inactive: IMAGE.iconPickupBeforeInactive, placeholderText: TEXT.ENTER_DATE, isPasswordField: false, unlock: nil)
    }
    
    
    func datePickerChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!

        switch fieldType! {
        case .date, .startDate, .endDate:
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            switch Singleton.sharedInstance.getTaskTypeTosetDetailsForCreateTask() {
            case PICKUP_DELIVERY.pickup:
                Singleton.sharedInstance.createTaskDetail.jobPickupDateTime = dateFormatter.string(from: sender.date)
                break
            case PICKUP_DELIVERY.delivery:
                switch self.fieldType! {
                case FIELD_TYPE.date:
                    Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime = dateFormatter.string(from: sender.date)
                    break
                case FIELD_TYPE.startDate:
                    Singleton.sharedInstance.createTaskDetail.jobPickupDateTime = dateFormatter.string(from: sender.date)
                    break
                case FIELD_TYPE.endDate:
                    Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime = dateFormatter.string(from: sender.date)
                    break
                default:
                    break
                }
                break
            case PICKUP_DELIVERY.both:
                Singleton.sharedInstance.createTaskDetail.jobPickupDateTime = dateFormatter.string(from: sender.date)
                Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime = dateFormatter.string(from: sender.date)
                break
            case PICKUP_DELIVERY.addDeliveryDetails:
                if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
                    Singleton.sharedInstance.createTaskDetail.jobPickupDateTime = dateFormatter.string(from: sender.date)
                } else if Singleton.sharedInstance.createTaskDetail.hasDelivery == 1 {
                    Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime = dateFormatter.string(from: sender.date)
                }
                break
            }
            
            // change to a readable time format and change to local time zone
            dateFormatter.dateFormat = "dd MMM yyyy, hh:mm a"
            break
            
        case .dateCustomField:
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:self.customField.tag)
            customField.data = dateFormatter.string(from: sender.date)
            break
        case .dateFuture:
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:self.customField.tag)
            customField.data = dateFormatter.string(from: sender.date)
            break
        case .datePast:
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:self.customField.tag)
            customField.data = dateFormatter.string(from: sender.date)
            break
        case .dateTime:
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:self.customField.tag)
            customField.data = dateFormatter.string(from: sender.date)
            break
        case .dateTimePast:
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:self.customField.tag)
            customField.data = dateFormatter.string(from: sender.date)
            break
        case .dateTimeFuture:
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:self.customField.tag)
            customField.data = dateFormatter.string(from: sender.date)
            break
        default:
            break
        }

        dateFormatter.timeZone = NSTimeZone.local
        let timeStamp = dateFormatter.string(from: sender.date)
        self.customField.text = timeStamp
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setImageForDifferentStates(inactive:UIImage, placeholderText:String, isPasswordField:Bool, unlock:UIImage?) {
        inactiveImage = inactive
       // activeImage = active
       // filledImage = filled
        self.customField.attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSForegroundColorAttributeName: COLOR.PLACEHOLDER_COLOR])
       
        self.rightButton.setImage(self.inactiveImage.withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        if (self.customField.text?.length)! > 0 {
            setActiveInActiveStatus(yourState: ACTIVE_INACTIVE_STATES.filled)
        } else {
            setActiveInActiveStatus(yourState: ACTIVE_INACTIVE_STATES.inActive)
        }
        
        if isPasswordField == true {
            self.customField.isSecureTextEntry = true
            if unlock != nil {
                self.unlockImage = unlock
            }
        }
    }
    
    func setActiveInActiveStatus(yourState:String) {
        switch yourState {
        case ACTIVE_INACTIVE_STATES.active:
            self.rightButton.tintColor = COLOR.ACTIVE_IMAGE_COLOR
        case ACTIVE_INACTIVE_STATES.inActive:
            self.rightButton.tintColor = COLOR.IN_ACTIVE_IMAGE_COLOR
        case ACTIVE_INACTIVE_STATES.filled:
            self.rightButton.tintColor = COLOR.FILLED_IMAGE_COLOR
        default :
            self.rightButton.tintColor = COLOR.IN_ACTIVE_IMAGE_COLOR
        }
    }
    
    //MARK: UITEXTFIELD DELEGATE
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.validStatus.isHidden = true
        self.shrinkUnderLine()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.showInactiveUnderline()
        self.setCreateTaskDetailsValue(updatedText: (textField.text?.trimText)!)
        switch self.fieldType! {
        case .date, .startDate, .endDate, .dateCustomField, .datePast, .dateFuture, .dateTime, .dateTimePast, .dateTimeFuture:
            self.datePickerChanged(sender: self.datePicker)
            break
        default:
            break
        }

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedText = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        
        
            switch self.fieldType! {
            case .contact:
                if updatedText.length > 12 {
                    return false
                }
                print("")
            default:
                print("")
            }
            
        
        
        self.setCreateTaskDetailsValue(updatedText: updatedText.trimText)
        return true
    }
    
    
    func setCreateTaskDetailsValue(updatedText:String) {
        switch fieldType! {
        case FIELD_TYPE.name:
            switch Singleton.sharedInstance.getTaskTypeTosetDetailsForCreateTask() {
            case PICKUP_DELIVERY.pickup:
                Singleton.sharedInstance.createTaskDetail.jobPickupName = updatedText
                break
            case PICKUP_DELIVERY.delivery:
                Singleton.sharedInstance.createTaskDetail.customerUsername = updatedText
                break
            case PICKUP_DELIVERY.both:
                Singleton.sharedInstance.createTaskDetail.jobPickupName = updatedText
                Singleton.sharedInstance.createTaskDetail.customerUsername = updatedText
                break
            case PICKUP_DELIVERY.addDeliveryDetails:
                if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
                    Singleton.sharedInstance.createTaskDetail.jobPickupName = updatedText
                } else if Singleton.sharedInstance.createTaskDetail.hasDelivery == 1 {
                    Singleton.sharedInstance.createTaskDetail.customerUsername = updatedText
                }
                break
            }
            break
            
        case FIELD_TYPE.email:
            switch Singleton.sharedInstance.getTaskTypeTosetDetailsForCreateTask() {
            case PICKUP_DELIVERY.pickup:
                Singleton.sharedInstance.createTaskDetail.jobPickupEmail = updatedText
                break
            case PICKUP_DELIVERY.delivery:
                Singleton.sharedInstance.createTaskDetail.customerEmail = updatedText
                break
            case PICKUP_DELIVERY.both:
                Singleton.sharedInstance.createTaskDetail.jobPickupEmail = updatedText
                Singleton.sharedInstance.createTaskDetail.customerEmail = updatedText
                break
            case PICKUP_DELIVERY.addDeliveryDetails:
                if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
                   Singleton.sharedInstance.createTaskDetail.jobPickupEmail = updatedText
                } else if Singleton.sharedInstance.createTaskDetail.hasDelivery == 1 {
                    Singleton.sharedInstance.createTaskDetail.customerEmail = updatedText
                }
                break
            }
            break
            
        case FIELD_TYPE.contact:
            let contactNumber = (self.countryCodeButton.titleLabel?.text)! + " " + (updatedText)

            switch Singleton.sharedInstance.getTaskTypeTosetDetailsForCreateTask() {
            case PICKUP_DELIVERY.pickup:
                Singleton.sharedInstance.createTaskDetail.jobPickupPhone = contactNumber
                break
            case PICKUP_DELIVERY.delivery:
                Singleton.sharedInstance.createTaskDetail.customerPhone = contactNumber
                break
            case PICKUP_DELIVERY.both:
                Singleton.sharedInstance.createTaskDetail.jobPickupPhone = contactNumber
                Singleton.sharedInstance.createTaskDetail.customerPhone = contactNumber
                break
            case PICKUP_DELIVERY.addDeliveryDetails:
                if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
                    Singleton.sharedInstance.createTaskDetail.jobPickupPhone = contactNumber
                } else if Singleton.sharedInstance.createTaskDetail.hasDelivery == 1 {
                   Singleton.sharedInstance.createTaskDetail.customerPhone = contactNumber
                }
                break
            }

            break
            
        case FIELD_TYPE.date:
            break
        default:
            break
        }
    }
    
    
    func shrinkUnderLine() {
        UIView.animate(withDuration: 0.2, animations: {
            self.underline.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            }, completion: { finished in
                self.showActiveUnderLine()
        })
    }
    
    func showActiveUnderLine() {
        UIView.animate(withDuration: 0.2, animations: {
            self.underline.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
            self.setActiveInActiveStatus(yourState:ACTIVE_INACTIVE_STATES.active)
            self.underline.transform = CGAffineTransform.identity
            }, completion: { finished in
                
        })
    }
    
    func showInactiveUnderline() {
        self.underline.backgroundColor = COLOR.SPLASH_LINE_COLOR
        if self.customField.text?.length == 0 {
            self.rightButton.setImage(self.inactiveImage, for: UIControlState.normal)
        } else {
            switch fieldType! {
            case .email:
                if Singleton.sharedInstance.validateEmail((self.customField.text?.trimText)!) == true {
                    self.setSuccessStatus()
                } else {
                    self.setFailureStatus()
                }
                self.setActiveInActiveStatus(yourState: ACTIVE_INACTIVE_STATES.filled)
                break
            default:
                self.setActiveInActiveStatus(yourState: ACTIVE_INACTIVE_STATES.filled)
                break
            }
            
        }
        self.underline.transform = CGAffineTransform.identity
    }
    
    func setSuccessStatus() {
        self.validStatus.image = IMAGE.successIcon
        self.validStatus.isHidden = false
    }
    
    func setFailureStatus() {
        self.validStatus.image = IMAGE.failureIcon
        self.validStatus.isHidden = false
    }
    
    func setValidStatusHide() {
        self.validStatus.isHidden = true
    }
    
    
    @IBAction func rightButtonAction(_ sender: Any) {
        self.customField.becomeFirstResponder()
    }

    
}
