//
//  WorkFlowOptionsCell.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 24/04/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class WorkFlowOptionsCell: UITableViewCell {
    @IBOutlet weak var serviceImage: UIImageView!
    @IBOutlet weak var serviceName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        serviceName.font = UIFont(name: FONT.regular, size: FONT_SIZE.serviceNameFontSize)      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
