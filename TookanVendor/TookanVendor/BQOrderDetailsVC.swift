//
//  BQOrderDetailsVC.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 10/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit
import MessageUI

enum ServiceIsNotStarted: Int {
    case address = 0
    case specialInstruction
    case payment
    static let count = payment.rawValue + 1
}

enum ServiceIsAccepted: Int {
    case address = 0
    case specialInstruction
    case payment
    case storeDetails
    static let count = storeDetails.rawValue + 1
}


class BQOrderDetailsVC: UIViewController, MFMessageComposeViewControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var topBgView: UIView!
    @IBOutlet weak var lblAssigning: UILabel!
    @IBOutlet weak var lblGroceryPrepared: UILabel!
    @IBOutlet weak var lblGroceryDispatched: UILabel!
    @IBOutlet weak var lblAccepted: UILabel!
    @IBOutlet weak var lblDelivered: UILabel!
    @IBOutlet weak var statusCircle1: UIView!
    @IBOutlet weak var statusCircle2: UIView!
    @IBOutlet weak var statusCircle3: UIView!
    @IBOutlet weak var statusCircle4: UIView!
    @IBOutlet weak var statusCircle5: UIView!
    @IBOutlet weak var statusLine1: UIView!
    @IBOutlet weak var statusLine2: UIView!
    @IBOutlet weak var statusLine3: UIView!
    @IBOutlet weak var statusLine4: UIView!
    var navigationBar:NavigationView!
    
    @IBOutlet weak var btnCancelOrder: UIButton!
    @IBOutlet weak var btnViewItems: UIButton!
    @IBOutlet weak var btnViewItems2: UIButton!
    @IBOutlet weak var topViewHeightConstraints: NSLayoutConstraint!
    
    private var mailComposeViewController: MFMailComposeViewController!
    var arrSavedAddress = [BQOrderListModel]()
    
    var orderID: Int!
    var isComingFromOrderCreation = false
    var isPullToRefreshActive: Bool = false
    var refreshControl = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setupTable()
        self.setNavigationBar()
        self.addPulltoRefresh()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.arrSavedAddress.removeAll()
        self.getJobDetailsById()
    }
    
    private func setupUIComponents() {
        
        self.statusCircle1.layer.cornerRadius = statusCircle1.frame.size.height/2
        self.statusCircle2.layer.cornerRadius = statusCircle1.frame.size.height/2
        self.statusCircle3.layer.cornerRadius = statusCircle1.frame.size.height/2
        self.statusCircle4.layer.cornerRadius = statusCircle1.frame.size.height/2
        self.statusCircle5.layer.cornerRadius = statusCircle1.frame.size.height/2
        
        self.statusCircle1.backgroundColor = COLOR.App_Red_COLOR
        self.lblAssigning.textColor = COLOR.App_Green_COLOR
        
        if self.arrSavedAddress.count > 0 {
            let cellDataModel: BQOrderListModel = self.arrSavedAddress[0]
            
            if cellDataModel.job_status == OrderStatus.Unassigned {
                
            }
            
            if cellDataModel.job_status == OrderStatus.Accepted {
                self.statusLine1.backgroundColor = COLOR.App_Green_COLOR
                self.statusCircle2.backgroundColor = COLOR.App_Red_COLOR
                self.lblAccepted.textColor = COLOR.App_Green_COLOR
                self.statusLine1.backgroundColor = COLOR.App_Green_COLOR
            }
            if cellDataModel.job_status == OrderStatus.Started {
                self.statusCircle2.backgroundColor = COLOR.App_Red_COLOR
                self.lblAccepted.textColor = COLOR.App_Green_COLOR
                self.statusCircle3.backgroundColor = COLOR.App_Red_COLOR
                self.lblGroceryPrepared.textColor = COLOR.App_Green_COLOR
                self.statusLine1.backgroundColor = COLOR.App_Green_COLOR
                self.statusLine2.backgroundColor = COLOR.App_Green_COLOR
                
            }
            if cellDataModel.job_status == OrderStatus.OnTheWay {
                self.statusCircle2.backgroundColor = COLOR.App_Red_COLOR
                self.lblAccepted.textColor = COLOR.App_Green_COLOR
                self.statusCircle3.backgroundColor = COLOR.App_Red_COLOR
                self.lblGroceryPrepared.textColor = COLOR.App_Green_COLOR
                self.statusCircle4.backgroundColor = COLOR.App_Red_COLOR
                self.lblGroceryDispatched.textColor = COLOR.App_Green_COLOR
                self.statusLine1.backgroundColor = COLOR.App_Green_COLOR
                self.statusLine2.backgroundColor = COLOR.App_Green_COLOR
                self.statusLine3.backgroundColor = COLOR.App_Green_COLOR
            }
            if cellDataModel.job_status == OrderStatus.Delivered {
                self.statusCircle2.backgroundColor = COLOR.App_Red_COLOR
                self.lblAccepted.textColor = COLOR.App_Green_COLOR
                self.statusCircle3.backgroundColor = COLOR.App_Red_COLOR
                self.lblGroceryPrepared.textColor = COLOR.App_Green_COLOR
                self.statusCircle4.backgroundColor = COLOR.App_Red_COLOR
                self.lblGroceryDispatched.textColor = COLOR.App_Green_COLOR
                self.statusCircle5.backgroundColor = COLOR.App_Red_COLOR
                self.lblDelivered.textColor = COLOR.App_Green_COLOR
                self.statusLine1.backgroundColor = COLOR.App_Green_COLOR
                self.statusLine2.backgroundColor = COLOR.App_Green_COLOR
                self.statusLine3.backgroundColor = COLOR.App_Green_COLOR
                self.statusLine4.backgroundColor = COLOR.App_Green_COLOR
            }
            if cellDataModel.job_status == OrderStatus.Cancelled {
                
            }
            self.btnViewItems.layer.cornerRadius = 22
            self.btnViewItems2.layer.cornerRadius = 22
            self.btnCancelOrder.layer.cornerRadius = 22
        }
        
    }
    
    private func addPulltoRefresh() {
        //Add pullto refresh
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor(red: 246/255, green: 246/255, blue: 246/255, alpha: 1.0)
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        self.tblView.addSubview(refreshControl)
    }
    
    @objc func pullToRefresh() {
        isPullToRefreshActive = true
        self.arrSavedAddress.removeAll()
        self.getJobDetailsById()
        
    }
    
    private func setupTable() {
        self.tblView.register(UINib(nibName: "DeliveryAddressCell", bundle: nil), forCellReuseIdentifier: "DeliveryAddressCell")
        self.tblView.register(UINib(nibName: "BQPaymentDetailCell", bundle: nil), forCellReuseIdentifier: "BQPaymentDetailCell")
        self.tblView.register(UINib(nibName: "BQStoreDetailsCell", bundle: nil), forCellReuseIdentifier: "BQStoreDetailsCell")
        self.tblView.register(UINib(nibName: "SpecialInstructionTableViewCell", bundle: nil), forCellReuseIdentifier: "SpecialInstructionTableViewCell")
    }
    
    
    
    private func getJobDetailsById() {
        let params = getParamsToDeleteLocation()
        if isPullToRefreshActive == false {
            ActivityIndicator.sharedInstance.showActivityIndicator()
        }
        
        HTTPClient.makeConcurrentConnectionWith(method: .POST, para: params, extendedUrl: "get_job_details_by_id") { (responseObject, error, _, statusCode) in
            
            
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            self.isPullToRefreshActive = false
            guard statusCode == .some(STATUS_CODES.SHOW_DATA) else {
                return
            }
            print(responseObject ?? "")
            if let dataRes = responseObject as? [String:Any] {
                if let data = dataRes["data"] as? [String:Any] {
                    if let favLocations = data["jobs"] as? [[String: Any]] {
                        print(favLocations)
                        
                        for item in favLocations {
                            let rollData = BQOrderListModel.init(param: item)
                            
                            self.topViewHeightConstraints.constant = 120.0
                            self.topBgView.isHidden = false
                            
                            if rollData.job_status == OrderStatus.Delivered || rollData.job_status == OrderStatus.Accepted || rollData.job_status == OrderStatus.OnTheWay || rollData.job_status == OrderStatus.Started {
                                self.btnViewItems.isHidden = true
                                self.btnCancelOrder.isHidden = true
                                self.btnViewItems2.isHidden = false
                            } else if rollData.job_status == OrderStatus.Cancelled {
                                self.btnViewItems.isHidden = true
                                self.btnCancelOrder.isHidden = true
                                self.btnViewItems2.isHidden = false
                                self.topViewHeightConstraints.constant = 20.0
                                self.topBgView.isHidden = true
                            } else {
                                self.btnViewItems.isHidden = false
                                self.btnCancelOrder.isHidden = false
                                self.btnViewItems2.isHidden = true
                                
                            }
                            self.arrSavedAddress.append(rollData)
                        }
                        self.setupUIComponents()
                        self.tblView.delegate = self
                        self.tblView.dataSource = self
                        self.tblView.reloadData()
                        
                    }
                    print(data)
                    
                }
            }
        }
    }
    
    private func getParamsToDeleteLocation() -> [String: Any] {
        return [
            "app_access_token": Vendor.current!.appAccessToken!,
            "job_id": "\(self.orderID ?? 0)"
        ]
    }
    
    
    private func cancelOrder() {
        
        if let cellDataModel: BQOrderListModel = self.arrSavedAddress[0] {
            if let vc = UIStoryboard(name: "MyOrders", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQOrderCancellationReasonVC") as? BQOrderCancellationReasonVC {
                vc.orderID = cellDataModel.category_id
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    @IBAction func cancelOrderButtonAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Message", message: "Are you sure you want to cancel this order ?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .destructive, handler: { (action) -> Void in
            self.cancelOrder()
        })
        let noAction = UIAlertAction(title: "No", style: .cancel, handler: { (action) -> Void in
            //self.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
//        alertController.view.tintColor = UIColor.red
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func viewItemsbuttonAction(_ sender: Any) {
        let cellDataModel: BQOrderListModel = self.arrSavedAddress[0]
        if let vc = UIStoryboard(name: "MyOrders", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQViewItemsVC") as? BQViewItemsVC {
            vc.orderListDataModel = cellDataModel
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        //Use for showing two right bar buttons on navigation bar
        
        navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "#\(orderID ?? 0)", leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"), rightButtonImage: nil, secondRightButtonImage: nil)
            , leftButtonAction: {[weak self] in
                self?.backAction()
            }, rightButtonAction: nil,
               rightButtonSecondAction: nil
        )
        navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
    }
    
    // MARK: - IBAction
    func backAction() {
        if isComingFromOrderCreation {
            Singleton.sharedInstance.pushToHomeScreen() {
            
            }
            return
        }
        self.navigationController?.popViewController(animated: true)
    }
}

extension BQOrderDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.arrSavedAddress.count > 0 {
            let cellDataModel: BQOrderListModel = self.arrSavedAddress[0]
            if cellDataModel.job_description != "" {
                if cellDataModel.job_status == OrderStatus.Accepted || cellDataModel.job_status == OrderStatus.OnTheWay || cellDataModel.job_status == OrderStatus.Delivered {
                    if section == 0 {
                        return "Address Details"
                    } else if section == 1 {
                        return "Special Instructions"
                    } else if section == 2 {
                        return "Payment Details"
                    }
                    return "Store Details"
                }
                if section == 0 {
                    return "Address Details"
                } else if section == 1 {
                    return "Special Instructions"
                }
                return "Payment Details"
            } else {
                if cellDataModel.job_status == OrderStatus.Accepted || cellDataModel.job_status == OrderStatus.OnTheWay || cellDataModel.job_status == OrderStatus.Delivered {
                    if section == 0 {
                        return "Address Details"
                    } else if section == 1 {
                        return "Payment Details"
                    }
                    return "Store Details"
                }
                if section == 0 {
                    return "Address Details"
                }
                return "Payment Details"
            }
        }
        return ""
        
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1.0)
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let cellDataModel: BQOrderListModel = self.arrSavedAddress[indexPath.row] {
            if cellDataModel.job_description != "" {
                if indexPath.section == 0 {
                    return UITableViewAutomaticDimension
                } else if indexPath.section == 1 {
                    return 80
                } else if indexPath.section == 2 {
                    return 135
                }
            } else {
                if indexPath.section == 0 {
                    return UITableViewAutomaticDimension
                } else if indexPath.section == 1 {
                    return 135
                }
            }
            return 80
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let cellDataModel: BQOrderListModel = self.arrSavedAddress[0] {
            if self.arrSavedAddress.count > 0 {
                //            let cellDataModel: BQOrderListModel = self.arrSavedAddress[0]
                if cellDataModel.job_status == OrderStatus.Accepted || cellDataModel.job_status == OrderStatus.Accepted || cellDataModel.job_status == OrderStatus.OnTheWay || cellDataModel.job_status == OrderStatus.Delivered {
                    if cellDataModel.job_description != "" {
                        return ServiceIsAccepted.count
                    } else {
                        return ServiceIsAccepted.count - 1
                    }
                }
            }
            if cellDataModel.job_description != "" {
                return ServiceIsNotStarted.count
            } else {
                return ServiceIsNotStarted.count - 1
            }
            return ServiceIsNotStarted.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSavedAddress.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.arrSavedAddress.count > 0 {
            if let cellDataModel: BQOrderListModel = self.arrSavedAddress[indexPath.row] {
                if cellDataModel.job_status == OrderStatus.Accepted || cellDataModel.job_status == OrderStatus.OnTheWay || cellDataModel.job_status == OrderStatus.Started || cellDataModel.job_status == OrderStatus.Delivered {
                    if cellDataModel.job_description != "" {
                        if indexPath.section == 0 {
                            return self.createDeliveryaddressCell(indexPath: indexPath, cellDataModel: cellDataModel)
                        } else if indexPath.section == 1 {
                            return self.createSpecialInstructionCell(indexPath: indexPath, cellDataModel: cellDataModel)
                        } else if indexPath.section == 2 {
                            return self.createPaymentCell(indexPath: indexPath, cellDataModel: cellDataModel)
                        } else if indexPath.section == 3 {
                            return self.createStoreDetailsCell(indexPath: indexPath, cellDataModel: cellDataModel)
                        }
                    } else {
                        if indexPath.section == 0 {
                            return self.createDeliveryaddressCell(indexPath: indexPath, cellDataModel: cellDataModel)
                        } else if indexPath.section == 1 {
                            return self.createPaymentCell(indexPath: indexPath, cellDataModel: cellDataModel)
                        } else if indexPath.section == 2 {
                            return self.createStoreDetailsCell(indexPath: indexPath, cellDataModel: cellDataModel)
                        }
                    }
                }
                if indexPath.section == 0 {
                    return self.createDeliveryaddressCell(indexPath: indexPath, cellDataModel: cellDataModel)
                } else if indexPath.section == 1 {
                    if cellDataModel.job_description != "" {
                        return self.createSpecialInstructionCell(indexPath: indexPath, cellDataModel: cellDataModel)
                    } else {
                        return self.createPaymentCell(indexPath: indexPath, cellDataModel: cellDataModel)
                    }
                }
                return self.createPaymentCell(indexPath: indexPath, cellDataModel: cellDataModel)
            }
        }
        return UITableViewCell()
    }
    
    func createDeliveryaddressCell(indexPath: IndexPath, cellDataModel: BQOrderListModel) -> UITableViewCell {
        guard let cell = tblView.dequeueReusableCell(withIdentifier: "DeliveryAddressCell", for: indexPath) as? DeliveryAddressCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        cell.title.text = ""
        cell.btnDelete.isHidden = true
        cell.setupDataForBookingDetails(orderModel: cellDataModel)
        return cell
    }
    
    func createSpecialInstructionCell(indexPath: IndexPath, cellDataModel: BQOrderListModel) -> UITableViewCell {
        guard let cell = tblView.dequeueReusableCell(withIdentifier: "SpecialInstructionTableViewCell", for: indexPath) as? SpecialInstructionTableViewCell else {
            return UITableViewCell()
        }
        cell.specialInstructionTxtView.text = cellDataModel.job_description ?? ""
        return cell
    }
    
    func createPaymentCell(indexPath: IndexPath, cellDataModel: BQOrderListModel) -> UITableViewCell {
        guard let cell = tblView.dequeueReusableCell(withIdentifier: "BQPaymentDetailCell", for: indexPath) as? BQPaymentDetailCell else {
            return UITableViewCell()
        }
        cell.bgView.backgroundColor = .white
        cell.setupDataForBookingPaymentDetails(orderModel: cellDataModel)
        return cell
    }
    
    func createStoreDetailsCell(indexPath: IndexPath, cellDataModel: BQOrderListModel) -> UITableViewCell {
        guard let cell = tblView.dequeueReusableCell(withIdentifier: "BQStoreDetailsCell", for: indexPath) as? BQStoreDetailsCell else {
            return UITableViewCell()
        }
        
        cell.lblName.text = cellDataModel.store_name
        cell.lblDescription.text = "All type of grocery material"
        
        cell.btnCall.isHidden = false
        cell.btnMessage.isHidden = false
        
        if cellDataModel.job_status == OrderStatus.Delivered {
            cell.btnFeedback.isHidden = false
            cell.btnCall.isHidden = true
            cell.btnMessage.isHidden = true
            
            if cellDataModel.is_customer_rated == true {
                cell.btnFeedback.isHidden = true
                cell.bgRatingView.isHidden = false
                if cellDataModel.fleet_rating == 1 {
                    cell.imgRated1.image = #imageLiteral(resourceName: "rated")
                } else if cellDataModel.fleet_rating == 2 {
                    cell.imgRated1.image = #imageLiteral(resourceName: "rated")
                    cell.imgRated2.image = #imageLiteral(resourceName: "rated")
                } else if cellDataModel.fleet_rating == 3 {
                    cell.imgRated1.image = #imageLiteral(resourceName: "rated")
                    cell.imgRated2.image = #imageLiteral(resourceName: "rated")
                    cell.imgRated3.image = #imageLiteral(resourceName: "rated")
                } else if cellDataModel.fleet_rating == 4 {
                    cell.imgRated1.image = #imageLiteral(resourceName: "rated")
                    cell.imgRated2.image = #imageLiteral(resourceName: "rated")
                    cell.imgRated3.image = #imageLiteral(resourceName: "rated")
                    cell.imgRated4.image = #imageLiteral(resourceName: "rated")
                } else if cellDataModel.fleet_rating == 5 {
                    cell.imgRated1.image = #imageLiteral(resourceName: "rated")
                    cell.imgRated2.image = #imageLiteral(resourceName: "rated")
                    cell.imgRated3.image = #imageLiteral(resourceName: "rated")
                    cell.imgRated4.image = #imageLiteral(resourceName: "rated")
                    cell.imgRated5.image = #imageLiteral(resourceName: "rated")
                }
            } else {
                cell.btnFeedback.isHidden = false
                cell.bgRatingView.isHidden = true
            }
        } else {
            cell.btnFeedback.isHidden = true
            cell.bgRatingView.isHidden = true
        }
        
        cell.delegate = self
        cell.bgView.backgroundColor = .white
        return cell
    }
}

extension BQOrderDetailsVC: BQStoreStatusVCDelegate, BQStoreDetailsCellDelegate {
    func feedbackToDriver() {
        if let cellDataModel: BQOrderListModel = self.arrSavedAddress[0] {
            if let vc = UIStoryboard(name: "MyOrders", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQFeedbackReviewVC") as? BQFeedbackReviewVC {
                vc.job_id = cellDataModel.category_id
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func thanksForPromoCodeApplying() {
        print("thanksForPromoCodeApplying")
    }
    
    func startChatWithDriver() {
        print("startChatWithDriver")
    }
    
    func startCallWithDrivr() {
        print("startCallWithDrivr")
    }
    
    func callBtnAction() {
        if let cellDataModel: BQOrderListModel = self.arrSavedAddress[0] {
            var phone = cellDataModel.store_phone
            phone = phone?.replacingOccurrences(of: "[ |()-]", with: "", options: [.regularExpression])
            print(phone as Any)
            if let mobile = phone {
                if let url = URL(string: "tel://\(mobile ?? "")"), UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        }
    }
    
    func messageBtnAction() {
        if let cellDataModel: BQOrderListModel = self.arrSavedAddress[0] {
            if let mobile = cellDataModel.store_phone {
                // let phoneNumberStr = code + mobile
                self.sendMessage(phoneNoStr: mobile)
            }
        }
        
    }
    
    func sendMessage(phoneNoStr: String) {
        if MFMessageComposeViewController.canSendText() {
            let controller = MFMessageComposeViewController()
            controller.body = ""
            controller.recipients = [phoneNoStr]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
}





