//
//  FilterOptionTableViewCell.swift
//  Jugnoo Autos
//
//  Created by Gagandeep  on 20/01/17.
//  Copyright © 2017 Socomo Technologies. All rights reserved.
//

import UIKit

class FilterOptionTableViewCell: UITableViewCell {

    @IBOutlet var bgView: UIView!
    @IBOutlet var tickIconImageView: UIImageView!
    @IBOutlet var filterLabel: UILabel!
    @IBOutlet var horizontalLineView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tickIconImageView.isHidden = true
        tickIconImageView.image = greenTick
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var isSelectedSortType : Bool{
        get{
            return tickIconImageView.isHidden
        }
        set(newValue){
            
            self.tickIconImageView.isHidden = !newValue
        }
    }
    
    
}
