//
//  Constant.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 15/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit


let SCREEN_SIZE = UIScreen.main.bounds

let frameworkBundle = Bundle.main
let placeholdeImage  = UIImage(named: "bawiqPlaceholde", in: frameworkBundle, compatibleWith: nil)
let homeAddressImage = UIImage(named: "homeAddress", in: frameworkBundle, compatibleWith: nil)
let workAddress = UIImage(named: "workAddress", in: frameworkBundle, compatibleWith: nil)
let otherAddress = UIImage(named: "otherAddress", in: frameworkBundle, compatibleWith: nil)
let deleteAddress = UIImage(named: "deleteAddress", in: frameworkBundle, compatibleWith: nil)
let editAddress = UIImage(named: "addressOptions", in: frameworkBundle, compatibleWith: nil)
let backButtonImage = UIImage(named: "back", in: frameworkBundle, compatibleWith: nil)
let iconCash = UIImage(named: "iconCash", in: frameworkBundle, compatibleWith: nil)
let optionEmpty = UIImage(named: "optionEmpty", in: frameworkBundle, compatibleWith: nil)
let iconTip = UIImage(named: "iconTip", in: frameworkBundle, compatibleWith: nil)
let iconPromo = UIImage(named: "iconPromocode", in: frameworkBundle, compatibleWith: nil)
let iconBill = UIImage(named: "iconBill", in: frameworkBundle, compatibleWith: nil)
let iconPayments = UIImage(named: "iconPayments", in: frameworkBundle, compatibleWith: nil)
let greenTick = UIImage(named: "greenTick", in: frameworkBundle, compatibleWith: nil)
let sampleCard = UIImage(named: "sampleCard", in: frameworkBundle, compatibleWith: nil)
let downArrow = UIImage(named: "downArrow", in: frameworkBundle, compatibleWith: nil)
let iconDropOff = UIImage(named: "iconDropOff", in: frameworkBundle, compatibleWith: nil)
let iconAddImage = UIImage(named: "iconAddImage", in: frameworkBundle, compatibleWith: nil)
let pickupHeading = UIImage(named:"pickupHeading", in: frameworkBundle, compatibleWith: nil)
let cartHeading = UIImage(named: "cartHeading", in: frameworkBundle, compatibleWith: nil)
let taskDescription = UIImage(named: "taskDescription", in: frameworkBundle, compatibleWith: nil)
let checkboxEmpty = UIImage(named: "checkboxEmpty", in: frameworkBundle, compatibleWith: nil)
let checked = UIImage(named: "checked", in: frameworkBundle, compatibleWith: nil)


struct KEYS {
    static let GOOGLE_MAPS_API_KEY = "AIzaSyBzg-m9ObRbX2k0IqYTMif7sfM6ESPV2vY"
    //"AIzaSyAN7K_l1Rp1oe8H8vZdgeeQ2_Koz4qqB8g"
    //"AIzaSyA6DAFH-kh8nX0AptXPzOqkgKpKurZzGAQ" //"AIzaSyANAZCixPIRWWEpkKeU5436ETBGP1DMtsQ"
    static let GOOGLE_BROWSER_KEY = "AIzaSyBzg-m9ObRbX2k0IqYTMif7sfM6ESPV2vY"
    //"AIzaSyAN7K_l1Rp1oe8H8vZdgeeQ2_Koz4qqB8g"
    // "AIzaSyA6DAFH-kh8nX0AptXPzOqkgKpKurZzGAQ" //"AIzaSyANAZCixPIRWWEpkKeU5436ETBGP1DMtsQ"
    
    static let GOOGLE_SIGNIN_KEY = "1036414707735-gpe653ahgnr3r6qfq7hbhrp8abm8c5uk.apps.googleusercontent.com"
    //"1036414707735-7m6fk85ob92j41uppf3bnvpifakr80p0.apps.googleusercontent.com"
    //"1036414707735-6nc6rcotfsm53crjsv9d8aebct01euhm.apps.googleusercontent.com"
    //"1036414707735-ukj93sd1f6ia2l4m6v23barbide8hnol.apps.googleusercontent.com"
    //"1002754809441-ajjqgvbni6h16r7scg385ucj4ao8s4jd.apps.googleusercontent.com" //"1045228771954-qlks0effc4k6i37ht102loo4in24j20b.apps.googleusercontent.com"
    
    //test key AIzaSyAjgywNKN0-aZJxQInL_li6KcIuufFFzCg samneet.kharbanda@tookanapp.com
    //
    // live build - AIzaSyBO3akrG8JrkiscfP16HKEH1OFHUTexHs8
    //live build key AIzaSyBRWwez5rUKbEj6T3RIILx7Qi8e3vC8e84
    
//new key  AIzaSyBO4Glwu9mcYOfJTJvJPU06y7VOgYLv5v0

}

//MARK: FONT
struct FONT {
    static let regular = "Montserrat-Regular"
    static let bold = "Montserrat-Bold"
    static let light = "Montserrat-Light"
    static let ultraLight = "Montserrat-UltraLight"
   static let semiBold = "Montserrat-SemiBold"
}

//MARK: API NAME
struct API_NAME {
    
    static let verifyOtp = "marketplace_vendor_verify_otp"
    static let resendOtp = "marketplace_vendor_resend_otp"
    
    static let addFamilyMember = "marketplace_vendor_add_family_member"
    static let updateFamilyMember = "marketplace_vendor_update_family_member"
    static let getFamilyMember = "marketplace_vendor_get_family_member"
    static let deleteFamilyMember = "marketplace_vendor_delete_family_member"
   
    static let getCategories = "get_categories"
    static let getSubCategories = "get_sub_categories"
    static let getAllBanners = "get_banners_by_customer"
    static let getProducts = "get_products"
    static let getRecommendedProducts = "get_customer_recommended_products"
    
    
    static let addProduct = "vendor_add_product_to_cart"
    static let getProductscart = "vendor_get_cart_items"
    static let deleteProduct = "vendor_delete_cart_items"
    static let favUnfavProduct = "mark_unamrk_fav_product"
    
    
    static let getFavLocations = "get_fav_location"
    static let deleetFavLocation = "delete_fav_location"
    
    
    static let getAppCatalogue = "get_app_catalogue"
    static let authenticate_both = "authenticate_merchant_and_vendor"
    static let fetchAppConfiguration = "fetch_app_configuration"
    static let checkEmailRegistered = "check_email_exists"
    static let signup = "marketplace_vendor_signup"
    static let sendReferral = "apply_referral"
    static let loginEmail = "marketplace_vendor_login"
    static let loginAccessToken = "marketplace_vendor_login_via_access_token"
    static let forgotPass = "user_forgot_password"
    static let changePhoneNumber = "change_phone"
    static let createTask = "create_task_via_vendor"//POST /change_vendor_password
    static let vendorUpdate = "marketplace_vendor_update"
    //POST /edit_vendor_profile
    static let editProfile = "edit_vendor_profile"
    static let getOrderHistory = "get_order_history"
    static let trackAgent = "track_agent"
    static let fetchCards = "fetch_merchant_cards"
    static let getPayment = "send_payment_for_task"
    static let getProductsForCategory = "get_products_for_category"
    static let getFavouriteLocation = "get_fav_location"
    static let deleteFavLocation = "delete_fav_location"
    static let addFavLocation = "add_fav_location"
    static let getAgents = "get_service_providers"
    static let editFavLocation = "edit_fav_location"
    static let assignPromo = "assign_coupon"
    static let getSuperCategories = "get_super_categories"
    static let v2_getSuperCategories = "get_dummy_user_details_for_demo"
    static let v2_getAppCatalogue = "get_app_catalogue"
    static let v2_getProductsForCategory = "get_products_for_category"
    static let v2_get_bill_breakdown = "get_bill_breakdown"
    static let company_details_after_signup = "set_company_details_after_signup"
    static let getMerchantApiKeys = "get_merchant_api_keys"
    static let createTaskForSavedCart =  "create_task_via_vendor_and_save_cart_details"
    static let uploadImageForReference = "upload_reference_images"
}

struct HTTP_METHOD {
    static let POST = "POST"
    static let GET = "GET"
    static let PUT = "PUT"
}

//MARK: STATUS CODE
struct STATUS_CODES{
    static let INVALID_ACCESS_TOKEN = 101
    static let BAD_REQUEST = 400
    static let UNAUTHORIZED_ACCESS = 401
    static let ERROR_IN_EXECUTION = 500
    static let SHOW_ERROR_MESSAGE = 304
    static let NOT_FOUND_MESSAGE = 404
    static let SHOW_MESSAGE = 201
    static let UNAUTHORIZED_FOR_AVAILABILITY = 210
    static let SHOW_DATA = 200
    static let SLOW_INTERNET_CONNECTION = 999
    static let DELETED_TASK = 501
}

//MARK:ERROR MESSAGE
struct ERROR_MESSAGE {
    static let INVALID_ACCESS_TOKEN = "Invalid Access Token".localized
    static let NO_INTERNET_CONNECTION = "Unable to connect with the server. Check your internet connection and try again.".localized
    static let SERVER_NOT_RESPONDING = "Something went wrong while connecting to server!".localized
    static let INVALID_EMAIL = "Please enter a correct email address".localized
    static let INVALID_PASSWORD = "Please enter Password.".localized
    static let INVALID_CONFIRM_PASSWORD = "Please retype your Password.".localized
    static let PASSWORD_MISMATCH = "Password mismatch".localized
//    static let INVALID_PASSWORD = "This password is too short".localized
    static let OLD_NEW_PASSWORD = "Old password & new password can not be same.".localized
    static let PassWord_DosentMatch = "New password & confirm password doesen't match."
    static let INVALID_PHONE_Email = "Please enter valid Email".localized
    static let INVALID_PHONE_Number = "Please enter valid phone number".localized
//    static let INVALID_PHONE_Email = "Your Email or Phone number is invalid".localized
    static let INVALID_PHONE_NUMBER = "Your Phone number is invalid".localized
    static let INVALID_ADDRESS = "Please enter address".localized
    static let ACCEPT_TERMS_POLICY = "Please accept terms and policy to proceed".localized
    static let INVALID_DATETIME = "Datetime must be greater than current time".localized
    static let invalidStart = "Invalid start time.".localized
    static let invalidEnd = "Invalid end time.".localized
    static let INVALID_PICKUP_DELIVERY_TIME = "Delivery time must be greater than Pickup time".localized
    static let INVALID_NAME = "Please enter name".localized
    static let ENTER_START_TIME = "Please enter start time".localized
    static let ENTER_END_TIME = "Please enter end time".localized
    static let INVALID_START_TIME = "Start time must be greater than current time".localized
    static let INVALID_END_TIME = "End time must be greater than start time".localized
    
    static let INVALID_COUNTRY_CODE = "Please select your country code first.".localized
    static let INVALID_COMPANY = "Please enter your company name first.".localized
    static let IMAGE_UPLOADING = "Please wait Image is uploading...".localized
    static let PLEASE_ENTER_VALID = "Please enter valid ".localized
    static let WHATSAPP_NOT_FOUND = "You don't have Whatsapp in your device.".localized
    static let SEARCH_ADDRESS = "Search Address".localized
    
    
    
   //10/7/2017
   static let catalougeFetchingFailure = "Unable to fetch Catalogue".localized
   static let unableToFetchProducts = "No Products found".localized
    static let ENTER_COMPANY_SIZE = "Please enter company size".localized
   static let NO_CATALOGUE_FOUND = "No Catalogue Found".localized
    static let INVALID_Ammount = "Please enter valid ammount".localized
}

//MARK: USER DEFAULT
struct USER_DEFAULT {
    static let selectedServer = "selectedServer"
    static let accessToken = "accessToken"
    static let appaccessToken = "appaccessToken"
    static let selectedLocale = "locale"
    static let deviceToken = "deviceToken"
    static let emailAddress = "emailAddress"
    static let phoneNumber = "phoneNumber"
    static let companyName = "companyName"
    static let address = "address"
    static let vendorName = "vendorName"
    static let vendorImage = "vendorImage"
    static let updatingLocationPathArray = "updatingPathLocationArray"
    static let isFirstTimeSignUp = "isFirstTimeSignUp"
}

//MARK: NIB NAME
struct NIB_NAME {
    static let menuNavigationView = "MenuNavigationView"
    static let JugnooHeaderForCatalogue = "JugnooHeaderForCatalogue"
    static let welcomeView = "WelcomeView"
    static let socialSectionView = "SocialSectionView"
    static let navigation = "NavigationView"
    static let titleView = "TitleView"
    static let customTextField = "CustomTextField"
    static let errorView = "ErrorView"
    static let ADDRESS_NIB = "AddressViewCell"
    static let PICKUPDELIVERYOPTION_NIB = "PickUpOptionCell"
    static let CONTACT_NAME_NIB = "ContactViewCell"
    static let EMAIL_PHONE_NIB = "EmailPhoneCell"
    static let DESCRIPTION_NIB = "DescriptionCell"
    static let customFieldCell = "CustomFieldCell"
    static let vendroProfileCell = "VendorProfileCell"
    static let vendorNamePicCell = "VendorNamePicCell"
    static let taskReviewCell = "TaskReviewCell"
    static let addressListCell = "AddressListCell"
    static let taskFooterView = "TaskFooterView"
    static let customDropdown = "CustomDropdown"
    static let pickupDeliveryBarCell = "PickupDeliveryBarCell"
    static let orderHistory = "OrderHistoryCell"
    static let sideMenuCell = "SideMenuCell"
    //OrderIDCallInfoCell
    static let orderIDCallInfoCell = "OrderIDCallInfoCell"
    //HeaderView
    static let orderDetailCustomFieldAndActivityCell = "OrderDetailCustomFieldAndActivityCell"
    static let imageCustomCell = "ImageCustomCell"
    static let imageCollectionCell = "ImageCollectionCell"
    static let imagePreviewCell = "ImagePreviewCell"
    static let noInternetConnectionView = "NoInternetConnectionView"
    static let imagesPreview = "ImagesPreview"
    static let currentTaskCell = "CurrentTaskCell"
    static let pushNotificationView = "PushNotificationView"
    static let tutorialCell = "TutorialCell"
    static let PayementMethodCell = "PayementMethodCell"
    static let TaxiCarTypes = "TaxiCarTypes"
    static let PleaseWaitView = "PleaseWaitView"
    static let scheduleViewId:String  = "bookingDateView"
    static let ChangePhoneNumberView = "ChangePhoneNumberView"
    static let WorkFlowOptionsCell = "WorkFlowOptionsCell"
    static let laundryProductCell = "laundryProductCell"
    static let cartViewCell = "cartViewCell"
   static let checkoutCartCell = "CartCell"
    static let CardsCell = "CardsCell"
    static let PaymentHeaders = "PaymentHeaders"
    static let promoCell = "PromoCell"
    static let BillBreakupCell = "BillBreakupCell"
    static let AddNewPromoCell = "AddNewPromoCell"
    static let TipCell = "TipCell"
    static let infoPopUpView = "InfoPopUpView"
    static let NLevelActionView = "NLevelActionView"
    static let menuTableCollectionViewCell = "MenuTableCollectionViewCell"
    static let menuHeader = "MenuHeaderCell"
   static let listCell = "ListTableViewCell"
    static let CheckoutHeaderViews = "CheckoutHeaderViews"
    static let TaskDescriptionCell = "TaskDescriptionCell"
    static let FavLocationCell = "FavLocationCell"
    static let CustomFTextField = "CustomFTextField"
    static let AddressCustomeField = "AddressCustomeField"
    static let phoneNumberCell = "phoneNumberCell"
    static let DateCustomField = "DateCustomField"
    static let CheckBoxCustomField = "CheckBoxCustomField"
    static let DropDownCell = "DropDownCell"
    static let TextFieldForUnEditing = "TextFieldForUnEditing"
   static let checkoutSubtotalCell = "SubtotalTableViewCell"
    static let SideMenuProfileCell = "SideMenuProfileCell"
    static let ImgesCustomField = "ImgesCustomField"
     static let SelectAgentTableViewCell = "SelectAgentTableViewCell"
    static let VendorProfileCellEdit = "vendorProfileCellEdit"
    static let extraInfoCell = "ExtraInfoCell"
   static let homeBottomCell = "HomeBottomCell"
    
    static let orderHistoryViewCell = "OrderHistoryViewCell"
    static let orderStatusCell = "OrderStatusCell"
    static let orderSummaryCell = "OrderSummaryCell"
    
    static let addressCell = "AddressCell"
    static let profilePictureCell = "ProfilePictureCell"
}


//struct SIDEMENU_OPTION {
//    //["Create Taks","Order History","Track Current Tasks","My Profile"]
//    
//    static let userName = "userName"
//    static let createTask = "createTask"
//    static let orderHistory =  TEXT.ALL
//    static let trackCurrent = "Current".localized
//    static let myProfile = "Profile".localized
//    static let PAYMENT_OPTION = "Payments".localized
//    static let logout = "Logout".localized
//    static let Referral = "Referrals".localized
//
//
//}


struct ACTIVE_INACTIVE_STATES {
    static let active = "active"
    static let inActive = "inActive"
    static let filled = "filled"
}

//MARK: STORYBOARD
struct STORYBOARD_NAME {
    static let afterLogin = StoryBoard.afterLogin.rawValue //Appointment,Deliveries
    static let main = StoryBoard.main.rawValue //Common(Appointment,Deliveries,Taxi)
    static let taxiStoryBoardId = StoryBoard.taxi.rawValue //Taxi
    static let demo = StoryBoard.demo.rawValue
    static let nLevel = StoryBoard.nLevel.rawValue
}

enum StoryBoard: String {
  case afterLogin = "AfterLogin"
  case main = "Main"
    case myorders = "MyOrders"
  case taxi = "TaxiAfterLogIn"
   case nLevel = "NLevel"
   case favLocation = "FavouriteLocation"
    case demo = "Demo"
}

//MARK: STORYBOARD
struct STORYBOARD_ID {
    //AfterLoginNavigation
    static let afterLoginNavigation = "AfterLoginNavigation"
    static let splashNavigation = "SplashNavigation"
    static let loginController = "LoginController"
    static let sigupController = "SignupController"
    static let homeController = "HomeController"
    static let reviewController = "TaskReviewVC"
    static let changePassVC = "ChangePassVC"
    static let orderHistoryVC = "OrderHistoryVC"
    static let vendorProfileVC = "VendorProfileVC"
    static let orderDetailVC = "OrderDetailVC"
    static let currentTaskController = "CurrentTaskController"
    static let tutorialScreen = "tutorial"
    static let getInTouch = "getInTouch"
    static let addCardVC = "addCard"
    static let addNewPayment = "addNewPayment"
    static let allPayment = "allPayment"
    static let viewCard = "viewCard"
    static let addCardWeb = "addCardWeb"
    static let allCardsAfterBooking = "allCardsAfterBooking"
    static let otpScreen = "otp"
    static let taxiHome = "taxiHome"
    static let RateVc = "rateVc"
    static let servicesOptions = "servicesOptions"
    static let laundryController = "laundryController"
    static let cartController = "cartController"
    static let refer = "refer"
    static let payment = "payment"
   static let sos = "SOSViewController"
    static let menuView = "menuView"
   static let listVC = "ListTableViewController"
    static let addLocation = "addLocation"
    static let checkoutScreen = "checkoutScreen"
    static let completionSignupVC = "CompletionSignupVC"
    static let orderSuccessfullVC = "OrderSuccessfullVC"
    static let extraInfoVC = "ExtraInfoVC"
    static let welcomeVC = "WelcomeVC"
    static let splashController = "SplashController"
   static let homeForDemo = "HomeForDemo"
    static let orderDetail = "OrderDetail"
}

//MARK: HEIGHT
struct HEIGHT {
    static let navigationHeight:CGFloat = 80.0
    static let titleHeight:CGFloat = 40.0
    static let textFieldHeight:CGFloat = 60.0
    static let errorMessageHeight:CGFloat = 43.0
    static let jugnooHeaderHeight:CGFloat = 131.1
    static let menuNavigationHeight:CGFloat = 64.0
}

let heightMultiplierForDifferentDevices: CGFloat = { () -> CGFloat in
  let screenHeightInDesigns: CGFloat = 667
  return SCREEN_SIZE.height/screenHeightInDesigns
}()

//MARK: IMAGES
struct IMAGE {
    static let iconEmailInActive = UIImage(named: "iconEmailInactive")!
    //static let iconEmailActive = UIImage(named: "iconEmailActive")!
    //static let iconEmailFilled = UIImage(named: "iconEmailFilled")!
   // static let iconPasswordActive = UIImage(named: "iconPasswordActive")!
    static let iconPasswordActiveShown = UIImage(named: "iconPasswordActiveShown")!
  //  static let iconPasswordFilled = UIImage(named: "iconPasswordFilled")!
    static let iconPasswordInactive = UIImage(named: "iconPasswordInactive")!
    static let iconSigninWelcome = UIImage(named: "iconSigninWelcome")!
    static let iconSigninSigninpage = UIImage(named: "iconSigninButton")!
   // static let iconContactActive = UIImage(named: "iconContactActive")!
   // static let iconContactFilled = UIImage(named: "iconContactFilled")!
    static let iconContactUnfilled = UIImage(named: "iconContactUnfilled")!
   // static let iconNameActive = UIImage(named: "iconNameActive")!
   // static let iconNameFilled = UIImage(named: "iconNameFilled")!
    static let iconNameInactive = UIImage(named: "iconNameInactive")!
    static let successIcon = UIImage(named: "success")!
    static let failureIcon = UIImage(named: "failure")!
    //static let iconImageActive = UIImage(named: "iconImageActive")!
    //static let iconImageFilled = UIImage(named: "iconImageFilled")!
    static let iconImageInactive = UIImage(named: "iconImageInactive")!
    static let iconImageChangePass = UIImage(named: "iconChangePassword")!
    static let iconImageLogout = UIImage(named: "iconContact")!
    static let iconImageCompany = UIImage(named: "iconProfileCompanyFilled")!
    static let iconImageAddress = UIImage(named: "iconProfileAddressFilled")!
    static let iconImageProfilePhone = UIImage(named: "iconProfileContactFilled")!
    static let iconEditProfile = UIImage(named: "iconEditProfile")!
    static let iconDefaultAvatar = UIImage(named: "iconDefaultAvatar")!
    static let iconAddPlus = UIImage(named: "iconAddPlus")!
    static let iconSaveTick = UIImage(named: "iconSaveTick")!
    static let iconProfileMailFilled = UIImage(named: "iconProfileMailFilled")!
    //static let iconPickupBeforeFilled = UIImage(named: "iconPickupBeforeFilled")!
    static let iconPickupBeforeInactive = UIImage(named: "iconPickupBeforeInactive")!
    //static let iconPickupBeforeActive = UIImage(named: "iconPickupBeforeActive")!
    static let iconSkipDetails = UIImage(named: "iconSkipDetails")!
    static let iconPattern = UIImage(named: "iconPattern")!
    static let iconDeleteRow = UIImage(named: "iconDeleteRow")!
    static let iconEditRow = UIImage(named: "iconEditRow")!
    static let iconDeleteRedRow = UIImage(named: "iconDeleteRedRow")!
    static let iconAddLocationReview = UIImage(named: "iconAddLocationReview")!
    static let iconReset = UIImage(named: "iconReset")!
    static let iconTextInactive = UIImage(named: "iconTextInactive")!
    static let iconNumberInactive = UIImage(named: "iconNumberInactive")!
    static let iconScanInactive = UIImage(named: "iconScanInactive")!
    static let iconCheckboxTicked = UIImage(named: "iconCheckboxTicked")!
    static let iconCheckboxUnticked = UIImage(named: "iconCheckboxUnticked")!
    static let iconDropdownClosed = UIImage(named: "iconDropdownClosed")!
    static let iconDropdownOpen = UIImage(named: "iconDropdownOpen")!
    static let iconMenu = UIImage(named: "iconMenu")!
    static let iconTrackOrder = UIImage(named: "iconTrackOrder")!
    static let iconEndLocationTrack = UIImage(named: "iconEndLocationTrack")!
    static let iconStartLocationTrack = UIImage(named: "iconStartLocationTrack")!
    static let iconTravellingDot = UIImage(named: "iconTravellingDot")!
    static let iconBackTitleBar = UIImage(named: "iconBackTitleBar")!
    static let tutorialScreenPd1 = UIImage(named:"tutorialPd1")
    static let tutorialScreenPd2 = UIImage(named:"tutorialPd2")
    static let tutorialScreenPd3 = UIImage(named:"tutorialPd3")
    static let tutorialScreenAP1 = UIImage(named:"tutorialAp1")
    static let tutorialScreenAp2 = UIImage(named:"tutorialAp2")
    static let tutorialScreenAP3 = UIImage(named:"tutorialAp3")
    static let tickImage = UIImage(named:"iconCreated")
    
}

//MARK: - TABLECELL IDENTIFIERS
struct CELL_IDENTIFIER {
    static let ADDRESSCELL = "AddressCell"
    static let PICKUPDELIVERYOPTIONCELL = "PickUpDeliveryOptionCell"
    static let CONTACT_NAME_CELL = "ContactNameCell"
    static let EMAIL_PHONE_CELL = "EmailPhoneCell"
    static let DESCRIPTION_CELL = "DescriptionCell"
    static let vendorProfileIdentifier = "VendorProfileCell"
    static let vendorNamePicCellIdentifier = "vendorNamePicCellIdentifier"
    //TaskReviewCellIdentifier
    static let taskReviewCellIdentifier = "TaskReviewCellIdentifier"
    //AddressCellIdentifier
    static let addressCellIdentifier = "AddressCellIdentifier"
    static let orderHistoryCellIdentifier = "OrderHistoryCell"
    static let sideMenuCellIdentifier = "SideMenuCellIdentifier"
    //OrderIDCallInfoCell
    static let orderIDCallInfoCell = "OrderIDCallInfoCell"
    //OrderDetailCustomFieldAndActivityCell
    static let orderDetailCustomFieldAndActivityCell = "OrderDetailCustomFieldAndActivityCell"
    static let payementMethod = "payementMethod"
    static let carTypeItem = "carTypeItem"
    static let servcesCell = "servcesCell"
    static let laundryProductCell = "LaundyCell"
    static let cartCell = "cartCell"
   static let checkoutCartCell = "CheckoutCartCell"
    static let cardCell = "cardCell"
    static let promoCell = "promoCell"
    static let billCell = "billCell"
    static let addNewPromo = "addNewPromo"
    static let tip = "tip"
    static let menuTableCollectionViewCell = "menuTableCollectionViewCell"
    static let menuHeader = "MenuHeaderViewCell"
    static let FavLocationCell = "FavLocationCell"
    static let taskDescription = "taskDescription"
    static let customTextField = "customeTextField"
    static let AddressCustomeField = "addressTextField"
    static let phoneCode = "phoneCode"
    static let dateCustomField = "dateCustomField"
    static let custCheckBox = "custCheckBox"
    static let DropDownCell = "DropDownCell"
    static let UnEdit = "UnEdit"
    static let checkoutSubtotalCell = "SubtotalTableViewCell"
    static let sideMenuProfile = "sideMenuProfile"
    static let ImageCustom = "ImageCustom"
    static let SelectAgentCell = "SelectAgentCell"
    static let VendorProfileCell = "VendorProfileCellNew"
    static let extraInfoCell = "ExtraInfoCell"
    static let orderHistoryViewCell = "OrderHistoryViewCell"

}


//MARK: - NOTIFICATION OBSERVER
struct NOTIFICATION_OBSERVER {
    static let PICKUP_OPTION_DETAIL_ENABLE = "PICKUP_OPTION_DETAIL_ENABLE"
    static let PICKUP_OPTION_DETAIL_DISABLE = "PICKUP_OPTION_DETAIL_DISABLE"
    static let hideProfielView = "hideProfielView"
    static let locationServicesDisabled = "locationDisabled"
    static let currentLocationFetched = "currentLocation"
    static let addDeliveryDetails = "addDeliveryDetails"
    static let addPickupDetails = "addPickupDetails"
    static let editPickupTask = "editPickupTask"
    static let editDeliveryTask = "editDeliveryTask"
    static let refreshHomeScreenAfterCreateTask = "refreshAfterCreateTask"
    static let updateModelTaskDetails = "updateModelTaskDetails"
    static let updatePath = "updatePath"
    static let stopTracking = "stopTracking"
    static let updateOnPushNotification = "pushNotification"
    static let showLowerView = "showLowerView"
}

//MARK: CUSTOM FIELD TYPE
enum CUSTOM_FIELD_TYPE {
    case address
    case option
    case contact
    case email
    case phone
    case description
    case date
    case startDate
    case endDate
    case image
    case dropDown
    case text
    case number
    case dateCustomField
    case checkbox
    case telephone
    case emailCustomField
    case url
    case checklist
    case dateFuture
    case datePast
    case dateTime
    case dateTimeFuture
    case dateTimePast
    case table
    case barcode
    case driverInfo
}

struct CUSTOM_FIELD_DATA_TYPE {
    static let dropDown = "Dropdown"
    static let text = "Text"
    static let number = "Number"
    static let image = "Image"
    static let date = "Date"
    static let checkbox = "Checkbox"
    static let telephone = "Telephone"
    static let email = "Email"
    static let url = "URL"
    static let checklist = "Checklist"
    static let dateFuture = "Date-Future"
    static let datePast = "Date-Past"
    static let dateTime = "Date-Time"
    static let dateTimeFuture = "Datetime-Future"
    static let dateTimePast = "Datetime-Past"
    static let table = "Table"
    static let barcode = "Barcode"
}

let paymentTypeDict = [
"2":"Card",
"4":"Card",
"8":"Cash"
]

//MARK: FIELD TYPE
enum FIELD_TYPE {
    case email
    case password
    case newPassword
    case name
    case contact
    case date
    case startDate
    case endDate
    case description
    case image
    case companyName
    case changepassword
    case logout
    case address
    case trackTask
    case orderHistory
    case dateCustomField
    case dateFuture
    case datePast
    case dateTime
    case dateTimeFuture
    case dateTimePast
    case comment
}

//MARK: WORKFLOW
enum WORKFLOW: Int {
    case pickupDelivery = 0
    case appointment
    case fieldWorkforce
    case Taxi
}

//MARK: PICKUP/DELIVERY
enum PICKUP_DELIVERY:Int {
    case pickup = 0
    case delivery
    case both
    case addDeliveryDetails
}

//MARK: JOB_TYPE
enum JOB_TYPE: Int {
    case pickup = 0
    case delivery = 1
    case fieldWorkforce = 2
    case appointment = 3
    case both = 10
   
   
   func getPrefix() -> String {
      switch self {
      case .pickup:
         return "P"
      case .delivery:
         return "D"
      default:
         return ""
      }
   }
}

//MARK: JOB STATUS
enum JOB_STATUS: Int {
    case assigned = 0
    case started = 1
    case successful = 2
    case failed = 3
    case arrived = 4
    case partial = 5
    case unAssigned = 6
    case accepted = 7 // Acknowledged
    case declined = 8
    case canceled = 9
    case ignored = 11
    case deleted = 10
    case noStatus = 12
   
   func getTextAndColor(vertical: Vertical = Singleton.sharedInstance.formDetailsInfo.verticalType) -> (text: String, color: UIColor) {
      
      switch self {
      case .assigned:
         return (TEXT.ASSIGNED_STATUS, JOB_STATUS_COLOR.ASSIGNED)
      case .started:
        
            return (TEXT.STARTED_STATUS , JOB_STATUS_COLOR.STARTED)
        
      case .successful:
         if vertical == .taxi {
            return (TEXT.completed , JOB_STATUS_COLOR.SUCCESSFUL)
         }else{
            return (TEXT.SUCCESSFUL_STATUS,JOB_STATUS_COLOR.SUCCESSFUL)
         }
      case .failed:
        
         return (TEXT.FAILED_STATUS,JOB_STATUS_COLOR.FAILED)
        
      case .arrived:
        
            return (TEXT.ARRIVED_STATUS, JOB_STATUS_COLOR.INPROGRESS)
        
    case .partial:
        
         return (TEXT.PARTIAL_STATUS,JOB_STATUS_COLOR.INPROGRESS)
        
      case .unAssigned:
         if vertical == .taxi {
            return (TEXT.scheduled, JOB_STATUS_COLOR.SCHEDULED)
         } else {
            return (TEXT.UNASSIGNED_STATUS, JOB_STATUS_COLOR.UNASSIGNED)
         }
      case .accepted:
         return (TEXT.ACCEPTED_STATUS,JOB_STATUS_COLOR.ACCEPTED)
      case .declined:
         return (TEXT.DECLINED_STATUS,JOB_STATUS_COLOR.DECLINED)
      case .canceled:
         return (TEXT.CANCELED_STATUS, JOB_STATUS_COLOR.CANCELLED)
      case .ignored:
         return (TEXT.IGNORED_STATUS, JOB_STATUS_COLOR.UNASSIGNED)
      case .deleted:
         return ("Deleted", UIColor.red)
      case .noStatus:
        return ("", UIColor.clear)
        
      }
   }
   
   func shouldHideSosButton() -> Bool {
      switch self {
      case .started, .arrived:
         return false
      default:
         return true
      }
   }
    
    func shouldShowCallButton() -> Bool {
        switch self {
        case .successful, .canceled, .declined, .failed, .unAssigned:
            return false
        default:
            return true
        }
    }
    
    func shouldStopTracking() -> Bool {
        
        switch self {
        case .successful, .canceled, .failed:
            return true
        default:
            return false
        }
    }
   
   func processingOftaskStopped() -> Bool {
    
         return false
    
   }
   
}



//let dialingCode = ["AC":"247"]
let dialingCode = ["AC":"247","BD": "880", "BE": "32", "BF": "226", "BG": "359", "BA": "387", "BB": "1-246", "WF": "681", "BL": "590", "BM": "1-441", "BN": "673", "BO": "591", "BH": "973", "BI": "257", "BJ": "229", "BT": "975", "JM": "1-876", "BV": "", "BW": "267", "WS": "685", "BQ": "599", "BR": "55", "BS": "1-242", "JE": "44-1534", "BY": "375", "BZ": "501", "RU": "7", "RW": "250", "RS": "381", "TL": "670", "RE": "262", "TM": "993", "TJ": "992", "RO": "40", "TK": "690", "GW": "245", "GU": "1-671", "GT": "502", "GS": "500 ", "GR": "30", "GQ": "240", "GP": "590", "JP": "81", "GY": "592", "GG": "44-1481", "GF": "594", "GE": "995", "GD": "1-473", "GB": "44", "GA": "241", "SV": "503", "GN": "224", "GM": "220", "GL": "299", "GI": "350", "GH": "233", "OM": "968", "TN": "216", "JO": "962", "HR": "385", "HT": "509", "HU": "36", "HK": "852", "HN": "504", "HM": " ", "VE": "58", "PR": "1-787 and 1-939", "PS": "970", "PW": "680", "PT": "351", "SJ": "47", "PY": "595", "IQ": "964", "PA": "507", "PF": "689", "PG": "675", "PE": "51", "PK": "92", "PH": "63", "PN": "870", "PL": "48", "PM": "508", "ZM": "260", "EH": "212", "EE": "372", "EG": "20", "ZA": "27", "EC": "593", "IT": "39", "VN": "84", "SB": "677", "ET": "251", "SO": "252", "ZW": "263", "SA": "966", "ES": "34", "ER": "291", "ME": "382", "MD": "373", "MG": "261", "MF": "590", "MA": "212", "MC": "377", "UZ": "998", "MM": "95", "ML": "223", "MO": "853", "MN": "976", "MH": "692", "MK": "389", "MU": "230", "MT": "356", "MW": "265", "MV": "960", "MQ": "596", "MP": "1-670", "MS": "1-664", "MR": "222", "IM": "44-1624", "UG": "256", "TZ": "255", "MY": "60", "MX": "52", "IL": "972", "FR": "33", "IO": "246", "SH": "290", "FI": "358", "FJ": "679", "FK": "500", "FM": "691", "FO": "298", "NI": "505", "NL": "31", "NO": "47", "NA": "264", "VU": "678", "NC": "687", "NE": "227", "NF": "672", "NG": "234", "NZ": "64", "NP": "977", "NR": "674", "NU": "683", "CK": "682", "XK": "", "CI": "225", "CH": "41", "CO": "57", "CN": "86", "CM": "237", "CL": "56","CHL":"56", "CC": "61", "CA": "1", "CG": "242", "CF": "236", "CD": "243", "CZ": "420", "CY": "357", "CX": "61", "CR": "506", "CW": "599", "CV": "238", "CU": "53", "SZ": "268", "SY": "963", "SX": "599", "KG": "996", "KE": "254", "SS": "211", "SR": "597", "KI": "686", "KH": "855", "KN": "1-869", "KM": "269", "ST": "239", "SK": "421", "KR": "82", "SI": "386", "KP": "850", "KW": "965", "SN": "221", "SM": "378", "SL": "232", "SC": "248", "KZ": "7", "KY": "1-345", "SG": "65", "SE": "46", "SD": "249", "DO": "1-809 and 1-829", "DM": "1-767", "DJ": "253", "DK": "45", "VG": "1-284", "DE": "49", "YE": "967", "DZ": "213", "US": "1", "UY": "598", "YT": "262", "UM": "1", "LB": "961", "LC": "1-758", "LA": "856", "TV": "688", "TW": "886", "TT": "1-868", "TR": "90", "LK": "94", "LI": "423", "LV": "371", "TO": "676", "LT": "370", "LU": "352", "LR": "231", "LS": "266", "TH": "66", "TF": "", "TG": "228", "TD": "235", "TC": "1-649", "LY": "218", "VA": "379", "VC": "1-784", "AE": "971", "AD": "376", "AG": "1-268", "AF": "93", "AI": "1-264", "VI": "1-340", "IS": "354", "IR": "98", "AM": "374", "AL": "355", "AO": "244", "AQ": "", "AS": "1-684", "AR": "54", "AU": "61", "AT": "43", "AW": "297", "IN": "91", "AX": "358-18", "AZ": "994", "IE": "353", "ID": "62", "UA": "380", "QA": "974", "MZ": "258"]

