//
//  ImageCollectionCell.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 16/12/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class ImageCollectionCell: UICollectionViewCell {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var transLayer: UIView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imageView.layer.cornerRadius = 4.0
        self.imageView.layer.borderColor = COLOR.SPLASH_LINE_COLOR.cgColor
        self.imageView.layer.borderWidth = 0.5
        transLayer.isHidden = true
        self.transLayer.clipsToBounds = true
        self.imageView.clipsToBounds = true 
    }

}
