//
//  BQCancelReasons.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 06/06/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQCancelReasons: NSObject {
    var isSelected = false
    var reasonName = "Default Resons"
    
    convenience init(param: [String: Any]) {
        self.init()
    
        if let categoryDescription = param["reasonName"] as? String {
            self.reasonName = categoryDescription
        }
    }
}
