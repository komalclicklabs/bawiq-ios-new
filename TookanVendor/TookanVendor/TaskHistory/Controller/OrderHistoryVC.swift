//
//  OrderHistoryVC.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 12/13/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class OrderHistoryVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
   // MARK: - Properties
   var navigationBar: NavigationView!
   var refreshControl = UIRefreshControl()
   let footerHeight: CGFloat = 50.0
   
   var shouldFetchMoreOrders = true
   var limit = 0
   
   var allOrders = [OrderHistory]()
   
   // MARK: - IBOutlets
   @IBOutlet weak var orderTableView: UITableView!
   @IBOutlet weak var errorLabel: UILabel!
   @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

   // MARK: - View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
    orderTableView.separatorStyle = .none
      setNavigationBar()
      configureViews()
      setTableViewRowHeight()
      registerCellsForOrderTableView()
      addingRefreshControl()
   }
   
   private func configureViews() {
      self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      configureTableView()
      configureActivityIndicator()
   }
   
   private func configureTableView() {
      orderTableView.separatorColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.1)
      orderTableView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
   }
   
   private func configureActivityIndicator() {
      activityIndicator.activityIndicatorViewStyle = .whiteLarge
      activityIndicator.color = COLOR.THEME_FOREGROUND_COLOR
   }
   
   private func setTableViewRowHeight() {
      self.orderTableView.rowHeight = UITableViewAutomaticDimension
      self.orderTableView.estimatedRowHeight = 100
   }
   
   private func registerCellsForOrderTableView() {
      
      orderTableView.register( UINib(nibName: NIB_NAME.orderHistoryViewCell, bundle: frameworkBundle), forCellReuseIdentifier: CELL_IDENTIFIER.orderHistoryViewCell)
       orderTableView.register( UINib(nibName: NIB_NAME.orderHistory, bundle: frameworkBundle), forCellReuseIdentifier:  CELL_IDENTIFIER.orderHistoryCellIdentifier)
     

   }
   
   private func addingRefreshControl() {
      refreshControl.backgroundColor = .clear
      refreshControl.tintColor = COLOR.THEME_FOREGROUND_COLOR
      refreshControl.attributedTitle = NSAttributedString(string: TEXT.PULL_TO_REFRESH)
      refreshControl.addTarget(self, action: #selector(OrderHistoryVC.pullRefresh), for: UIControlEvents.valueChanged)
      self.orderTableView.addSubview(refreshControl)
   }
   
   override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      UIApplication.shared.statusBarStyle = .default

      fetchTopTenOrdersFromServer(shouldShowActivityIndicator: true)
      
      NotificationCenter.default.addObserver(self, selector: #selector(OrderHistoryVC.refreshBookings), name: NSNotification.Name(rawValue: NOTIFICATION_OBSERVER.updateOnPushNotification), object: nil)
   }
   
   override func viewWillDisappear(_ animated: Bool) {
      super.viewWillDisappear(animated)
      
      NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_OBSERVER.updateOnPushNotification), object: nil )
   }
   
   // MARK: - IBAction
   func backAction() {
      _ = self.navigationController?.popViewController(animated: true)
   }
   
   // MARK: - Methods

   func pullRefresh() {
      fetchTopTenOrdersFromServer(shouldShowActivityIndicator: false)
   }
   
   
   //MARK: - UITableView Delegate
   func numberOfSections(in tableView: UITableView) -> Int {
      return 1
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return allOrders.count
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
      if let orderHistoryCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.orderHistoryViewCell) as? OrderHistoryViewCell {

         print(indexPath.row)
         print(allOrders[indexPath.row])
         let order = allOrders[indexPath.row]
         orderHistoryCell.setCellWith(order: order)
         return orderHistoryCell
         
      }
      
      return UITableViewCell()
   }
   
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
   // TODO: Refactor
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      var orderDetailVC : OrderDetail!
      var jobStatus: JOB_STATUS
      var jobID:String!
      orderDetailVC = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.orderDetail) as! OrderDetail
      if allOrders[indexPath.row].tasksOfOrder.count >= 2 {
         if allOrders[indexPath.row].tasksOfOrder[0].job_type == JOB_TYPE.pickup {
            jobID = allOrders[indexPath.row].tasksOfOrder[0].job_id
            orderDetailVC.jobId = allOrders[indexPath.row].tasksOfOrder[0].job_id
            jobStatus = allOrders[indexPath.row].tasksOfOrder[0].job_status!
         } else {
            jobID = allOrders[indexPath.row].tasksOfOrder[1].job_id
            orderDetailVC.jobId = allOrders[indexPath.row].tasksOfOrder[1].job_id
            jobStatus = allOrders[indexPath.row].tasksOfOrder[1].job_status!
         }
      } else {
         jobID = allOrders[indexPath.row].tasksOfOrder[0].job_id
         orderDetailVC.jobId = allOrders[indexPath.row].tasksOfOrder[0].job_id
        if allOrders[indexPath.row].tasksOfOrder[0].job_status != nil {
         jobStatus = allOrders[indexPath.row].tasksOfOrder[0].job_status!
        }else{
           jobStatus = JOB_STATUS.unAssigned
        }
      }
      
      if Singleton.sharedInstance.formDetailsInfo.verticalType == .taxi && [JOB_STATUS.arrived , JOB_STATUS.started, JOB_STATUS.accepted].contains(jobStatus) == true {
         
         let params = [
            "access_token": UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,
            "job_id": orderDetailVC.jobId,
            "app_device_type": APP_DEVICE_TYPE,
            "request_type": "1"
            ] as [String : Any]
         print(params)
         //ActivityIndicator.sharedInstance.showActivityIndicator()
         
         NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.trackAgent, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) { [weak self] (isSucceeded, response) in
//            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if isSucceeded == true{
               if let weakself = self {
                  DispatchQueue.main.async {
                     print(response)
                     if let data = response["data"] as? [String:Any] {
                        if let sessionId = data["session_id"] as? String {
                           weakself.apiHitForOrderHistory(isDirect:false, sessionId: sessionId, orderDetailVC : orderDetailVC, jobID: jobID)
                        }
                     }
                  }
               }
            }else{
               if let message =  response["message"] as? String{
                  if message != "No data found."{
                     ErrorView.showWith(message: message, removed: nil)
                  }else{
                     if let weakself = self {
                        DispatchQueue.main.async {
                           print(response)
                           if let data = response["data"] as? [String:Any] {
                              if let sessionId = data["session_id"] as? String {
                                 weakself.apiHitForOrderHistory(isDirect:false, sessionId: sessionId,orderDetailVC : orderDetailVC, jobID:jobID)
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      } else {
         self.apiHitForOrderHistory(isDirect:true, sessionId: "",orderDetailVC : orderDetailVC, jobID:jobID)
      }
   }
   
   func apiHitForOrderHistory(isDirect: Bool, sessionId:String, orderDetailVC : OrderDetail,jobID:String) {
      
      var orderParams = [String: Any]()
      orderParams["reference_id"] = Vendor.current?.referenceId
      orderParams["form_id"] = Singleton.sharedInstance.formDetailsInfo.form_id!
      orderParams["user_id"] = Vendor.current!.userId
      orderParams["app_device_type"] = APP_DEVICE_TYPE
      orderParams["job_id"] = jobID
      ActivityIndicator.sharedInstance.showActivityIndicator()
    
      HTTPClient.makeConcurrentConnectionWith(method: .POST, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: true, para: orderParams, extendedUrl: API_NAME.getOrderHistory) { (response, error, _, statusCode) in
         print(response ?? "")
         DispatchQueue.main.async {
            
            guard statusCode == .some(STATUS_CODES.SHOW_DATA) else {
               ErrorView.showWith(message: (error?.localizedDescription)!, isErrorMessage: true, removed: nil)
               return
            }
            if let dataRes = response as? [String:Any] {
               if let data = dataRes["data"] as? [[String:Any]] {
                  for item in data {
                     orderDetailVC.orderDetails = OrderDetails(json: item)
                     
                  }
               }
            }
            if isDirect == false {
               orderDetailVC.sessionId = sessionId
               orderDetailVC.forTracking = true
            }
            self.navigationController?.pushViewController(orderDetailVC, animated: true)
         }
      }
   }
   
   
   
   
   // MARK: - Pagination
   private func fetchTopTenOrdersFromServer(shouldShowActivityIndicator: Bool) {
      
      if shouldShowActivityIndicator {
         showActivityIndicator()
      }
      
      OrderHistory.fetchNextTenFromServerAfter(index: 0) { [weak self] (success, error, orders) in
         self?.stopActivityIndicator()
         guard success else {
            print(orders)
            self?.limit = 0
            if error?.localizedDescription.lowercased() == "no data found." {
               self?.errorLabel.text = TEXT.NO + " " + "orders" + " " + TEXT.FOUND
            } else {
                
                
               self?.errorLabel.text = error?.localizedDescription
            }
            return
         }
        if orders?.count == 0{
            self?.errorLabel.text = TEXT.NO + " " + "orders" + " " + TEXT.FOUND
            self?.orderTableView.isHidden = true
        }else{
        
         self?.limit = 10
         self?.allOrders.removeAll()
         self?.orderTableView.isHidden = false
         self?.allOrders = orders!
         self?.orderTableView.reloadData()
        }
      }
   }
   
   private func showActivityIndicator() {
      activityIndicator.isHidden = false
      errorLabel.text = TEXT.LOADING
      orderTableView.isHidden = true
      activityIndicator.startAnimating()
   }
   
   private func stopActivityIndicator() {
      activityIndicator.stopAnimating()
      activityIndicator.isHidden = true
      
      self.refreshControl.endRefreshing()
   }
   
   private func lastCellIsVisible() {
      guard shouldFetchMoreOrders else {
         return
      }
      
      addActivityIndicatorAtTableViewFooter()
      fetchNextTenElementsFromAlreadyFetchedElements() { [weak self] _ in
         self?.orderTableView.tableFooterView = UIView()
      }
   }
   
   private func addActivityIndicatorAtTableViewFooter() {
      let footerView = getFooterView()
      let activityIndicator = getActivityIndicatorToAddInTableViewFooter()
      
      footerView.addSubview(activityIndicator)
      activityIndicator.startAnimating()
      
      orderTableView.tableFooterView = footerView
   }
   
   private func getActivityIndicatorToAddInTableViewFooter() -> UIActivityIndicatorView {
      let activityIndicator = UIActivityIndicatorView()
      activityIndicator.activityIndicatorViewStyle = .gray
      activityIndicator.color = COLOR.THEME_FOREGROUND_COLOR
      
      let height = activityIndicator.frame.height
      let width = activityIndicator.frame.width
      
      let centerX = (orderTableView.frame.width/2) - (width/2)
      let centerY = (footerHeight/2) - (height/2)
      
      activityIndicator.frame.origin = CGPoint(x: centerX, y: centerY)
      return activityIndicator
   }
   
   private func getFooterView() -> UIView {
      let footerView = UIView()
      footerView.backgroundColor = UIColor.clear
      
      footerView.frame = CGRect(x: 0, y: 0, width: orderTableView.frame.width, height: footerHeight)
      return footerView
   }
   
   private func fetchNextTenElementsFromAlreadyFetchedElements(completion: Completion) {
      OrderHistory.fetchNextTenFromServerAfter(index: limit) { [weak self] (success, error, orders) in
         guard success else {
            completion?(false)
            return
         }
         
         self?.limit += 10
         completion?(true)
         self?.allOrders += orders!
         self?.orderTableView.reloadData()
      }
   }
   

   
   
   //MARK: - NAVIGATION BAR
   func setNavigationBar() {
      
      let navi = NavigationView.getNibFile(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "Orders", leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"), rightButtonImage: nil)
         , leftButtonAction: {[weak self] in
            self?.backAction()
         }, rightButtonAction: nil)
      
            
      self.view.addSubview(navi)
   }
   
   func refreshBookings() {
      DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) { [weak self] in
         self?.fetchTopTenOrdersFromServer(shouldShowActivityIndicator: false)
      }
      
   }
}
