//
//  OrderHistoryModels.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 12/14/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import Foundation
import GoogleMaps


class OrderDetails: NSObject {
   
   let serverDateFormat = "yyyy-MM-dd'T'HH:mm:ss.000Z"
   let displayDateFormat = "dd, MMM yyyy, hh:mm a"
    
    var job_id = ""
    var job_type = JOB_TYPE.pickup
    var job_time = ""
    var job_status: JOB_STATUS?
    var has_delivery : Int?
    var has_pickup : Int?
    var job_description = ""
    var job_address = ""
    var job_pickup_address = ""
    var job_pickup_datetime = ""
    var job_delivery_datetime = ""
    var pickup_delivery_relationship = ""
    var fleet_name = ""
    var job_pickup_name = ""
    var customer_username = ""
    var customer_phone = ""
    var job_pickup_phone = ""
    var customer_email = ""
    var job_pickup_email = ""
    var toShowPickUpOrDeliveryView = Bool()
    var totalTime = ""
    var totalDistance = ""
    var fleetImage = String()
    var fleetVehicleData = String()
    var fleetPhone = String()
    var jobHash = String()
    var isCustomerRated = String()
    var fleetLicense = String()
    var vertical = Vertical.pdaf
    var formId = String()
   var orderId = String()
   var productDetails = [ProductDetails]()
   var taskHistory = [TaskHistory]()
   var sectionsOfStatusCell = [OrderDetailStatus]()
   var creationDateTime = String()
    var link_Id = ""
    var endPointLatLong:CLLocationCoordinate2D?
    var fleetLatest:CLLocationCoordinate2D?
    var dropOffPoint:CLLocationCoordinate2D?
   
   var phoneNumberCallUrl: URL? {
      return URL(string: "tel://\(fleetPhone.replacingOccurrences(of: " ", with: ""))")
   }
    
    
    init(json: [String:Any]) {
        
        print("\(json)")
        print(vehicleNotes)
        
        if let value = json["form_id"] as? String{
            self.formId = value
        }else if let value = json["form_id"] as? Int{
            self.formId = "\(value)"
        }
      
      if let value = json["order_id"] as? String{
         self.orderId = value
      }else if let value = json["order_id"] as? Int{
         self.orderId = "\(value)"
      }
        
        if let value = json["creation_datetime"] as? String{
            self.creationDateTime = value
        }else if let value = json["creation_datetime"] as? Int{
            self.creationDateTime = "\(value)"
        }
        if let value = json["is_customer_rated"] as? String{
            self.isCustomerRated = "\(value)"
        }else if let value = json["is_customer_rated"] as? Int{
            self.isCustomerRated = "\(value)"
        }
        
        fleetVehicleData = ""
        for i in vehicleNotes{
            if let value = json[i] as? String{
                if i == vehicleNotes[0]{
                     self.fleetVehicleData =  value
                }else{
                     self.fleetVehicleData = fleetVehicleData + " " + value
                }
               
            }else if let value = json[i] as? Int{
                if i == vehicleNotes[0]{
                    self.fleetVehicleData =  "\(value)"
                }else{
                   self.fleetVehicleData = fleetVehicleData + " " + "\(value)"
                }
                
                
            }
        }
        
        if let value = json["fleet_phone"] as? String{
            self.fleetPhone = value
        }else if let value = json["fleet_phone"] as? Int{
            self.fleetPhone = "\(value)"
        }
        
         if let jobId = json["job_id"] as? String {
            self.job_id = jobId
         }else   if let jobId = json["job_id"] as? Int {
            self.job_id = "\(jobId)"
        }
        
        if let jobType = json["job_type"] as? NSNumber {
            self.job_type = JOB_TYPE(rawValue: jobType.intValue)!
        }
        
        if let linkTaks = json["link_task"] as? [String:Any]{
            if [JOB_TYPE.delivery,JOB_TYPE.pickup,JOB_TYPE.both].contains(self.job_type) == true{
                if let linkJob_id = linkTaks["job_id"] as? String{
                    toShowPickUpOrDeliveryView = true
                    link_Id = linkJob_id
                } else if let linkJob_id = linkTaks["job_id"] as? Int{
                    toShowPickUpOrDeliveryView = true
                    link_Id = "\(linkJob_id)"
                } else {
                    toShowPickUpOrDeliveryView = false
                }
            }else{
                toShowPickUpOrDeliveryView = false
            }
        }else{
            toShowPickUpOrDeliveryView = false
        }
        
        if let jobTime = json["job_time"] as? String {
            self.job_time = jobTime
        }
        
      if let jobStatusRawValue = json["job_status"] as? Int, let jobStatus = JOB_STATUS(rawValue: jobStatusRawValue) {
            self.job_status = jobStatus
            print(jobStatusRawValue)
      }else{
         self.job_status = JOB_STATUS(rawValue: 12)
        }
        
        if let showStatus  = json["show_status"] as? Int{
            
            if showStatus == 0{
                 self.job_status = JOB_STATUS(rawValue: 12)
            }
            
        }else if let showStatus = json["show_status"] as? String{
            
            if showStatus == "0"{
                 self.job_status = JOB_STATUS(rawValue: 12)
            }
        }
        
          if let hasDelivery = json["has_delivery"] as? Int {
            self.has_delivery = hasDelivery
        }
        
         if let hasPickup = json["has_pickup"] as? Int {
            self.has_pickup = hasPickup
        }
        
        if let jobDescription = json["job_description"] as? String {
            self.job_description = jobDescription
        }
        
        if let jobAddress = json["job_address"] as? String {
            self.job_address = jobAddress.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        
        if let job_pickupAddress = json["job_pickup_address"] as? String {
            self.job_pickup_address = job_pickupAddress.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
        if let job_pickupDatetime = json["job_pickup_datetime"] as? String {
            self.job_pickup_datetime = job_pickupDatetime
            print(job_pickupDatetime)
        }
        if let job_deliveryDatetime = json["job_delivery_datetime"] as? String {
            self.job_delivery_datetime = job_deliveryDatetime
            print(job_deliveryDatetime)
        }
        
        if let pickup_delivery_relationship_value = json["pickup_delivery_relationship"] as? String {
            self.pickup_delivery_relationship = pickup_delivery_relationship_value
        }
        
        if let fleetName = json["fleet_name"] as? String {
            self.fleet_name = fleetName
        }
        if let jobPickupName = json["job_pickup_name"] as? String {
            self.job_pickup_name = jobPickupName
        }
        
        if let customerUsername = json["customer_username"] as? String {
            self.customer_username = customerUsername
        }
        
        if let value = json["customer_phone"] as? String {
            self.customer_phone = value
        }
        
        if let value = json["job_pickup_phone"] as? String {
            self.job_pickup_phone = value
        }
        
        if let value = json["job_pickup_email"] as? String {
            self.job_pickup_email = value
        }
        
        if let value = json["customer_email"] as? String {
            self.customer_email = value
        }
        if let total_distance_travelled = json["total_distance_travelled"] as? String{
            totalDistance = total_distance_travelled
        }
        if let value = json["vertical"] {
            self.vertical = Vertical(rawValue: "\(value)")!
        }
        
        if let value = json["job_vertical"] {
            self.vertical = Vertical(rawValue: "\(value)")!
        }
        
        
        if let total_time_spent_at_task_till_completion = json["total_time_spent_at_task_till_completion"] as? String{
        totalTime = total_time_spent_at_task_till_completion
        }
        
        if job_type == JOB_TYPE.pickup{
        if let job_pickup_latitude = json["job_pickup_latitude"] as? Double{
            if let job_pickup_longitude = json["job_pickup_longitude"] as? Double{
                endPointLatLong = CLLocationCoordinate2D(latitude: CLLocationDegrees(job_pickup_latitude), longitude:  CLLocationDegrees(job_pickup_longitude))
            }
        }
        if let job_pickup_latitude = json["job_pickup_latitude"] as? String{
            if let job_pickup_longitude = json["job_pickup_longitude"] as? String{
                endPointLatLong = CLLocationCoordinate2D(latitude: CLLocationDegrees(job_pickup_latitude)!, longitude:  CLLocationDegrees(job_pickup_longitude)!)
            }
        }
        }else{
            if let job_pickup_latitude = json["job_latitude"] as? Double{
                if let job_pickup_longitude = json["job_longitude"] as? Double{
                    endPointLatLong = CLLocationCoordinate2D(latitude: CLLocationDegrees(job_pickup_latitude), longitude:  CLLocationDegrees(job_pickup_longitude))
                }
            }
            if let job_pickup_latitude = json["job_latitude"] as? String{
                if let job_pickup_longitude = json["job_longitude"] as? String{
                    endPointLatLong = CLLocationCoordinate2D(latitude: CLLocationDegrees(job_pickup_latitude)!, longitude:  CLLocationDegrees(job_pickup_longitude)!)
                }
            }
        }
        if let  fleet_latitude = json["fleet_latitude"] as? Double{
            if let fleet_longitude = json["fleet_longitude"] as? Double{
                fleetLatest = CLLocationCoordinate2D(latitude: CLLocationDegrees(fleet_latitude), longitude:  CLLocationDegrees(fleet_longitude))
            }
        }
        if let  fleet_latitude = json["fleet_latitude"] as? String{
            if let fleet_longitude = json["fleet_longitude"] as? String{
                fleetLatest = CLLocationCoordinate2D(latitude: CLLocationDegrees(fleet_latitude)!, longitude:  CLLocationDegrees(fleet_longitude)!)
            }
        }
      
        if let value = json["fleet_image"] as? String{
            self.fleetImage = value
        }
      
        if let value = json["job_hash"] as? String{
            self.jobHash = value
        }
        
        if let value = json["fleet_license"] as? String{
            self.fleetLicense = value
        }
      
      if let value = json["orderDetails"] as? [[String:Any]]{
         self.productDetails.removeAll()
         for currentProduct in value {
            if let product = currentProduct["product"] as? [String: Any]{
               let prod = ProductDetails(json: product)
               self.productDetails.append(prod)
            }
            
         }
      }
      
      if let value = json["task_history"] as? [[String:Any]]{
         self.taskHistory.removeAll()
         for currentHistory in value {
            let history = TaskHistory(json: currentHistory)
            self.taskHistory.append(history)
         }
      }
      
      
    }
   
   
   // MARK: - Methods
   func shouldHideSosButton() -> Bool {
      if vertical == .taxi {
         return job_status?.shouldHideSosButton() ?? true
      }
      return true
   }
    
   func sendSosCall(completion: Completion) {
      let params = getParamsforSosCall()
      HTTPClient.makeConcurrentConnectionWith(method: .POST, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: true, para: params, extendedUrl: "send_email_to_admin") { (responseObject, _, _, statusCode) in
         if statusCode == .some(STATUS_CODES.SHOW_DATA) {
            completion?(true)
         } else {
            completion?(false)
         }
      }
   }
   
   private func getParamsforSosCall() -> [String: Any] {
      return [
         "access_token" : Vendor.current!.appAccessToken!,
         "device_token" : "",
         "app_version" : APP_VERSION,
         "app_device_type" : APP_DEVICE_TYPE,
         "app_access_token" : Vendor.current!.appAccessToken!,
         "job_id" : job_id,
         "user_id" : Vendor.current!.userId!.description
      ]
   }
   
   private func getPickupDateStringToDisplay() -> String {
      return job_pickup_datetime.datefromYourInputFormatToOutputFormat(input: serverDateFormat, output: displayDateFormat)
   }
   
   private func getDeliveryDateStringToDisplay() -> String {
      return job_delivery_datetime.datefromYourInputFormatToOutputFormat(input: serverDateFormat, output: displayDateFormat)
   }
   
   func getAddress() -> String {
      switch job_type {
      case .pickup:
         return job_pickup_address
      default:
         return job_address
      }
   }
   
   func getDate() -> String {
      switch job_type {
      case .delivery:
         return getDeliveryDateStringToDisplay()
      case .appointment:
         return getPickupDateStringToDisplay() + " - " + getDeliveryDateStringToDisplay()
      default:
         return getPickupDateStringToDisplay()
      }
   }
   
   func getPrefixForJobType() -> String {
      return job_type.getPrefix()
   }
   
   
   func setStatusSections(orderDetails: OrderDetails) {
    if self.job_status != nil && self.job_status != JOB_STATUS(rawValue: 12){
         self.sectionsOfStatusCell.append(.status)
      }
      
      if let _ = orderDetails.taskHistory[0].creationDatetime, orderDetails.taskHistory[0].creationDatetime != "" {
         self.sectionsOfStatusCell.append(.orderTime)
      }
      
      if orderDetails.job_pickup_datetime != "" {
       //  self.sectionsOfStatusCell.append(.pickupTime)
      }
      
      if orderDetails.job_pickup_address != "" {
         //self.sectionsOfStatusCell.append(.pickupFrom)
      }
      
      if orderDetails.job_delivery_datetime != "" {
       //  self.sectionsOfStatusCell.append(.deliveryTime)
      }
      
      if orderDetails.job_address != "" {
         self.sectionsOfStatusCell.append(.deliveryTo)
      }
   }
   
}


class OrderHistory: NSObject {
    
    var tasksOfOrder = [OrderDetails]()
    
    init(json: [[String:Any]]) {
        
        for order in json {
            let singleOrderFullDetail = OrderDetails(json: order)
            tasksOfOrder.append(singleOrderFullDetail)
        }
    }
   
   // MARK: - Methods
   func hasTasks() -> Bool {
      return tasksOfOrder.count > 0
   }
   
   func hasSingleTask() -> Bool {
      return tasksOfOrder.count == 1
   }
   
   func getLatestStatusAndVertical() -> (status: JOB_STATUS, vertical: Vertical) {
      for task in tasksOfOrder {
        if task.job_status != nil {
         if !task.job_status!.processingOftaskStopped() {
            return (task.job_status!, task.vertical)
         }
        }
        return (JOB_STATUS.unAssigned, task.vertical)
      }
      
      let lastTask = tasksOfOrder.last!
      return (lastTask.job_status!, lastTask.vertical)
   }
   
   // MARK: - Type Methods
   
   class func fetchNextTenFromServerAfter(index: Int, completion: @escaping (_ success: Bool, _ error: Error?, [OrderHistory]?) -> Void) {
      
      let params = getParamsForFetchingOrdersAfter(index: index)
      
      HTTPClient.shared.makeSingletonConnectionWith(method: .POST, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: false, para: params, extendedUrl: API_NAME.getOrderHistory) { (responseObject, error, _, statusCode) in
         
         guard let response = responseObject as? [String: Any],
            statusCode == STATUS_CODES.SHOW_DATA else {
               completion(false, error, nil)
               return
         }
         
         let orders = getOrderListFrom(json: response)
         completion(true, nil, orders)
      }
   }
   
   private class func getParamsForFetchingOrdersAfter(index: Int) -> [String: Any] {
//      let orderParams: [String: Any] = [
//         "access_token": Vendor.current!.appAccessToken!,
//         "app_access_token" : Vendor.current!.appAccessToken!,
//         "vendor_id": Vendor.current!.id!.description,
//         "app_device_type": APP_DEVICE_TYPE,
//         "job_id":"",
//         "limit":"\(index)"
//      ]
      var orderParams = [String: Any]()
      orderParams["reference_id"] = Vendor.current!.referenceId
      orderParams["form_id"] = Singleton.sharedInstance.formDetailsInfo.form_id!
      orderParams["user_id"] = Vendor.current!.userId
      orderParams["app_device_type"] = APP_DEVICE_TYPE
      orderParams["limit"] = "\(index)"
//      {
//         "api_key": "d471d212df85bbc051a6d0a0b383b3276541a65026fdb133a9872cb61d659bb2",
//         "reference_id": "1",
//         "form_id": 520,
//         "user_id": 1717,
//         "parent_category_id":5524,
//         "app_device_type": 882
//      }
      print(orderParams)
      return orderParams
   }
   
   class func getOrderListFrom(json: [String: Any]) -> [OrderHistory] {
    
    var orderList = [OrderHistory]()
    
    if let dataArray = json["data"]  as? [[[String: Any]]] {
      for temp in dataArray {
        let singleOrderFullDetail = OrderHistory(json: temp)
        orderList.append(singleOrderFullDetail)
      }
    }
    
    return orderList
  }
  
}

class TaskHistory:NSObject {
   
   var creationDatetime:String!
   var stateDescription:String!
   var type:String!
   
   init(json:[String:Any]) {
      
      if let value = json["creation_datetime"] as? String {
         self.creationDatetime = value
      } else  if let value = json["created_at"] as? String {
         self.creationDatetime = value
      }else{
         self.creationDatetime  = ""
    }
    
      
      if let value = json["description"] as? String,
         let typeValue = json["type"] as? String {
         if typeValue == "state_changed" {
            self.stateDescription = value
         } else {
            self.stateDescription = typeValue
         }
      } else {
         self.stateDescription = "-"
      }
      
      
      
      if let typeValue = json["type"] as? String {
         self.type = typeValue
      } else {
         self.type = "-"
      }
      
   }
}


