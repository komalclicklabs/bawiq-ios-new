//
//  ImagesCustomTableViewCell.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 14/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class ImagesCustomTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
   
   // MARK: - IBOutlets
   @IBOutlet var headerLabel: UILabel!
   @IBOutlet var imageCollectionView: UICollectionView!
   @IBOutlet var bottomLine: UIView!
   
   @IBOutlet var imageCollectionHeightConstraint: NSLayoutConstraint!
   @IBOutlet weak var buttonAndCollectionViewConstraint: NSLayoutConstraint!
   @IBOutlet weak var labelAndCollectionViewConstraint: NSLayoutConstraint!
   
   // MARK: - Properties
   var onAddbuttonTap: (() ->Void)?
   var imageTappedAtIndex: ((_ index: Int) -> Void)?
   
   var isForEditing:Bool?
   var selfHeaderType:headerType?
   let imageCollecitonHeight:CGFloat = 76.0
   var imageCollectionArray = [Any]()
   weak var delegate:ImageCustomDelegate!
   
   // MARK: - View Life Cycle
   override func awakeFromNib() {
      super.awakeFromNib()
      /*--------- Header Label -------------*/
      headerLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.smallest)
      headerLabel.textColor = UIColor.black.withAlphaComponent(0.6)
      /*--------- UnderLine ------------*/
      self.bottomLine.backgroundColor = COLOR.SPLASH_LINE_COLOR
      
      /*------------- ImageCollectionView ------------*/
      // self.imageCollectionView.backgroundColor = COLOR.SPLASH_LINE_COLOR
      self.imageCollectionView.delegate = self
      self.imageCollectionView.dataSource = self
      self.imageCollectionView.register(UINib(nibName: NIB_NAME.imageCollectionCell, bundle: nil), forCellWithReuseIdentifier: NIB_NAME.imageCollectionCell)
      /*-------------- icon -------------*/
      
      selectionStyle = .none
   }
   
   func setImageCollectionView(label:String, imageArray:[Any]) {
      self.imageCollectionArray = imageArray
      self.headerLabel.text = label

      self.imageCollectionHeightConstraint.constant = self.imageCollecitonHeight
      self.bottomLine.isHidden = false
      
      self.imageCollectionView.reloadData()
   }
   
   
   //MARK: - UICOLLECTIONVIEW DELEGATE
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NIB_NAME.imageCollectionCell, for: indexPath) as! ImageCollectionCell
      if indexPath.row < imageCollectionArray.count {
         if Singleton.sharedInstance.isFileExistAtPath(self.imageCollectionArray[indexPath.item] as! String) == true {
            getImageFromDocumentDirectory(self.imageCollectionArray[indexPath.item] as! String, imageView: cell.imageView, placeHolderImage: #imageLiteral(resourceName: "iconImageInactive"), contentMode: UIViewContentMode.scaleAspectFill)
            cell.transLayer.isHidden = false
            cell.activityIndicator.startAnimating()
         } else {
            cell.transLayer.isHidden = true
            cell.activityIndicator.stopAnimating()
            let url = URL(string: self.imageCollectionArray[indexPath.item] as! String)
            cell.imageView.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "iconImageInactive"))
         }
         
      }else{
         cell.imageView.image = #imageLiteral(resourceName: "iconAddImage")
         cell.transLayer.isHidden = true
      }
      let width = (SCREEN_SIZE.width - 30) / 4
      
      
      
      cell.imageView.layer.borderColor = COLOR.SPLASH_LINE_COLOR.cgColor
      cell.imageView.layer.borderWidth = 0
      
      cell.transLayer.layer.borderColor = COLOR.SPLASH_LINE_COLOR.cgColor
      cell.transLayer.layer.borderWidth = 0
      
      
      if width < imageCollecitonHeight {
         cell.imageView.layer.cornerRadius = (width)/2 - 2
         cell.transLayer.layer.cornerRadius = (width)/2 - 2
      }else{
         cell.imageView.layer.cornerRadius = (imageCollecitonHeight)/2 - 2
         cell.transLayer.layer.cornerRadius = (imageCollecitonHeight)/2 - 2
      }
      
      return cell
   }
   
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      if isForEditing == false{
         return imageCollectionArray.count
      }else{
         return imageCollectionArray.count + 1
      }
   }
   
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
      return UIEdgeInsets(top:0, left:0, bottom: 0, right: 0)
   }
   
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
      let width = (SCREEN_SIZE.width - 30) / 4
      if width < imageCollecitonHeight {
         return CGSize(width: width, height: width)
      }
      return CGSize(width: imageCollecitonHeight, height: imageCollecitonHeight)
   }
   
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      if indexPath.row >= imageCollectionArray.count{
         self.onAddbuttonTap?()
      }else{
         delegate?.showImagesPreview(indexPath.item, imagesArray: self.imageCollectionArray,customFieldRow: collectionView.tag, headerTypeOfCell: selfHeaderType!)
         imageTappedAtIndex?(indexPath.item)
      }
   }
   

   
   func loadImageAsynchronously(_ imageURL:String, index:Int, placeholderImage:UIImage) -> UIImage {
      if let cachedImage = Singleton.sharedInstance.allImagesCache.object(forKey: "\(imageURL)" as NSString) {             //image caching
         return cachedImage
      } else {
         // The image isn't cached, download the img data
         // We should perform this in a background thread
         let url = URL(string: imageURL as String)
         let request: Foundation.URLRequest = Foundation.URLRequest(url: url!)
         let mainQueue = OperationQueue.main
         
         NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
            
            
            DispatchQueue.main.async {
               
               
               
               if error == nil {
                  // Convert the downloaded data in to a UIImage object
                  let image = UIImage(data: data!)
                  // Store the image in to our cache
                  // Update the cell
                  DispatchQueue.main.async(execute: {
                     if(image != nil) {
                        Singleton.sharedInstance.allImagesCache.setObject(image!, forKey: "\(request.url!.absoluteString)" as NSString)
                        if index < self.imageCollectionArray.count {
                           self.imageCollectionView.reloadItems(at: [IndexPath(item: index, section: 0)])
                        }
                     }
                  })
               }
               else {
                  print("Error")
               }
            }
         })
      }
      
      return placeholderImage
   }
   
}
