//
//  ServicesOptionsViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 24/04/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import IQKeyboardManager
import Kingfisher

class ServicesOptionsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
  
    let serviceNameArray = ["Pick-up & Delivery","Laundry Services"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: NIB_NAME.WorkFlowOptionsCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.servcesCell)
        tableView.separatorStyle = .none
      setNavigationBar()
      tableView.estimatedRowHeight = 198
      tableView.rowHeight = UITableViewAutomaticDimension
    }
    
   func configureViews() {
      self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      self.tableView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
   }
   
   func setNavigationBar() {
      let navBarparams = NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: TEXT.pickAOffering, leftButtonImage: nil, rightButtonImage: nil)
      
      let navbar = NavigationView.getNibFile(params: navBarparams, leftButtonAction: nil, rightButtonAction: nil)
      view.addSubview(navbar)
   }
   
    
    override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
        Singleton.sharedInstance.enableDisableIQKeyboard(enable: true)
    }
    
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
      let cell = tableView.dequeueReusableCell(withIdentifier:  CELL_IDENTIFIER.servcesCell, for: indexPath) as? WorkFlowOptionsCell
      
      cell?.selectionStyle = .none
      cell?.serviceImage.image = #imageLiteral(resourceName: "placeHolder")
      let imageUrlString = Singleton.sharedInstance.WorkFlowFormDetailsOptions[indexPath.row].logo ?? ""
      let url = URL(string: imageUrlString)
      cell?.serviceImage.kf.setImage(with: url, placeholder: #imageLiteral(resourceName: "placeHolder"))
      
      cell?.serviceName.textColor = COLOR.SPLASH_TEXT_COLOR
      cell?.serviceName.text = Singleton.sharedInstance.WorkFlowFormDetailsOptions[indexPath.row].formName
      cell?.serviceImage.clipsToBounds = true
      
      return cell!
   }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Singleton.sharedInstance.WorkFlowFormDetailsOptions.count
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
//        IQKeyboardManager.shared().isEnabled = true
//      let formDetails  = Singleton.sharedInstance.WorkFlowFormDetailsOptions[indexPath.row]
//        Singleton.sharedInstance.customFieldOptionsPickup = Singleton.sharedInstance.servicesOptionPickUpDetails[indexPath.row]
//
//        Singleton.sharedInstance.customFieldOptionsDelivery = Singleton.sharedInstance.servicesOptionsDeliveryDetails[indexPath.row]
//        Singleton.sharedInstance.formDetailsInfo = formDetails
//        Singleton.sharedInstance.typeCategories = Singleton.sharedInstance.servicesTypeCategories[indexPath.row]
//
//
//        DispatchQueue.main.async {
//
//         if formDetails.isNLevel {
//
//            let formId =  Singleton.sharedInstance.formDetailsInfo.form_id!
//            if Vendor.current == nil {
//                Catalogue.getCatalogueOfFormWithDemo(formId: formId, completion: { (success, catalogue, _) in
//                    guard success else {
//                        print("Catalouge API failed")
//
//                        return
//                    }
//                    Singleton.sharedInstance.formDetailsInfo.nLevelController = NLevelFlowManager(navigationController: self.navigationController!, catalogue: catalogue!)
//                    Singleton.sharedInstance.formDetailsInfo.nLevelController?.startFlow()
//                })
//            } else {
//                Catalogue.getCatalogueOfFormWith(formId: formId) { (success, catalogue, _) in
//
//                    guard success else {
//                        print("Catalouge API failed")
//
//                        return
//                    }
//                    Singleton.sharedInstance.formDetailsInfo.nLevelController = NLevelFlowManager(navigationController: self.navigationController!, catalogue: catalogue!)
//                    Singleton.sharedInstance.formDetailsInfo.nLevelController?.startFlow()
//
//                }
//            }
//
//            return
//         }
//
//
//        switch formDetails.verticalType {
//        case .taxi:
//            let vc = UIStoryboard(name: STORYBOARD_NAME.taxiStoryBoardId, bundle: Bundle.main).instantiateViewController(withIdentifier: STORYBOARD_ID.taxiHome) as? TaxiHomeScreenViewController
//           // vc.is
//           vc?.tohidebackButton = false
//
//            if Singleton.sharedInstance.typeCategories.count != 0{
//            self.navigationController?.pushViewController(vc!, animated: true)
//            }else{
//                ErrorView.showWith(message: "No Categories found.", isErrorMessage: true, removed: nil)
//            }
//
//        case .multipleCategory:
//
//            let vc = UIStoryboard(name: STORYBOARD_NAME.afterLogin, bundle: Bundle.main).instantiateViewController(withIdentifier: STORYBOARD_ID.laundryController) as? LaundyProductsViewController
//            // vc.is
//            self.navigationController?.pushViewController(vc!, animated: true)
//
//        default:
//            let vc = UIViewController.findIn(storyboard: .nLevel, withIdentifier: "checkoutScreen") as! CheckoutViewController
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
//        }
      
      StoreFrontManager.shared.startLoadingBlock = { (action) in
         ActivityIndicator.sharedInstance.showActivityIndicator()
      }
      
      StoreFrontManager.shared.stopLoadingBlock = { (action) in
         ActivityIndicator.sharedInstance.hideActivityIndicator()
      }
      let param = ["app_access_token" : Vendor.current!.appAccessToken! ,"app_device_type":APP_DEVICE_TYPE]
      print(param)
      ActivityIndicator.sharedInstance.showActivityIndicator()
      APIManager.sharedInstance.serverCall(apiName: API_NAME.getMerchantApiKeys, params: param as [String : AnyObject], httpMethod: "POST") { (isSuccess, response) in
         print(response)
         ActivityIndicator.sharedInstance.hideActivityIndicator()
         if isSuccess == true {
            if let data = response["data"] as? [String:Any]{
               
             MerchantRefModal.current = MerchantRefModal(json:data)
               StoreFrontManager.shared.changeAppDeviceType(deviceToken:APP_DEVICE_TYPE)
               StoreFrontManager.shared.completionHandler = nil
                StoreFrontManager.shared.setCompanyName(name: Singleton.sharedInstance.formDetailsInfo.formName)
//               StoreFrontManager.shared.checkAuthentication(merchantKey:MerchantRefModal.current!.merchantKey, referenceId: MerchantRefModal.current!.merchantId, vedorRefferenceId: Vendor.current!.referenceId, email: Vendor.current!.email!, phone: Vendor.current!.phoneNo, name: (Vendor.current!.firstName! + Vendor.current!.lastName!),type: .catalogue, navigationController: self.navigationController!) { (isAuthorized, response) in
//                  
//                  if isAuthorized == true {
//                     
//                  } else {
//                     print(response)
//                  }
//               }
            }
         }else{
            ErrorView.showWith(message: response["message"] as! String, removed: nil)
         }
      }
         
      
      }
      
      

   
    
    

}
