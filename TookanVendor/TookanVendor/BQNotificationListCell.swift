//
//  BQNotificationListCell.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 21/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQNotificationListCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
