//
//  BQSearchProductsVC.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 31/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit
import IQKeyboardManager

class BQSearchProductsVC: UIViewController, SearchCategoryView, UITextFieldDelegate {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var lblNoDataFount: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var cartBottomView: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    
    var products = [Products]()
    var presenter: SearchCategoryPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.setNavigationBar()
        self.setUpPresenter()
        
        btnCancel.isHidden = true
        txtSearch.delegate = self
        IQKeyboardManager.shared().isEnableAutoToolbar = false
       
        // txtSearch.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        // Do any additional setup after loading the view.
    }
//
//    func doneButtonClicked(_ sender: Any) {
//        //your code when clicked on done
//        self.txtSearch.resignFirstResponder()
//        self.presenter.getProductsForSubCategory(searchString: self.txtSearch.text ?? "")
//
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.txtSearch.resignFirstResponder()
        self.presenter.getProductsForSubCategory(searchString: self.txtSearch.text ?? "")
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let txtString = textField.text else {
            return false
        }
        let finalString = (txtString as NSString).replacingCharacters(in: range, with: string)
        if finalString.length > 0 {
            btnCancel.isHidden = false
        } else {
            btnCancel.isHidden = true
        }
        
        return true
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        txtSearch.text = ""
    }
    
    @IBAction func checkoutButtonAction(_ sender: UIButton) {
        if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartSegmentViewController") as? CartSegmentViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        IQKeyboardManager.shared().isEnableAutoToolbar = true
    }
    
    private func setupTableView() {
        
        cartBottomView.isHidden = true
        self.tblView.isHidden = true
        self.lblNoDataFount.isHidden = false
        
        self.searchView.layer.cornerRadius = 20.0
        self.searchView.layer.borderWidth = 1.0
        self.searchView.layer.borderColor = UIColor.lightGray.cgColor
        
        self.tblView.register(UINib(nibName: "ProductCell", bundle: nil), forCellReuseIdentifier: "ProductCell")
        self.tblView.rowHeight = UITableViewAutomaticDimension
        self.tblView.estimatedRowHeight = 200
        self.tblView.delegate = self
        self.tblView.dataSource = self
    }
    
    func reloadProductTable() {
        if presenter.numberOfItemsInTable() == 0 {
            self.tblView.isHidden = true
            self.lblNoDataFount.isHidden = false
            self.lblNoDataFount.text = "No Product Found."
        } else {
            self.tblView.isHidden = false
            self.lblNoDataFount.isHidden = true
            self.tblView.reloadData()
        }
        
    }
    func refreshCell(at index: Int, isAdded: Bool, cartCount: Int, totalPrice: Float) {
        if cartCount > 0 {
            cartBottomView.isHidden = false
        } else {
            cartBottomView.isHidden = true
        }
        self.lblTotalAmount.text = String(totalPrice)
    }
    private func setUpPresenter() {
        presenter = SearchCategoryImplementation(withView: self)
    }
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        //Use for showing two right bar buttons on navigation bar
        
        let navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "Search", leftButtonImage: #imageLiteral(resourceName: "BackButtonWhite"), rightButtonImage: nil, secondRightButtonImage: nil)
            , leftButtonAction: {[weak self] in
                self?.navigationController?.popViewController(animated: true)
            }, rightButtonAction: nil,
               rightButtonSecondAction: nil
        )
        navigationBar.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
        
    }
}
extension BQSearchProductsVC: UITableViewDelegate, UITableViewDataSource {
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 110
//    }
//    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfItemsInTable()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as? ProductCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        cell.deleteButton.isHidden = true
        cell.favBtn.isHidden = false
        presenter.setupDataTable(view: cell, index: indexPath.row)
       // presenter.setupDataTableForConfirmBooking(view: cell, index: indexPath.row)
        return cell
    }
}
extension BQSearchProductsVC: ProductCellDelegate {
    func productCellFavButtonTapped(cell: ProductCell, isfavStatus: Bool) {
        let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
        if isGuestUser {
            self.showGuestUserError()
        } else {
            if let index = tblView.indexPath(for: cell) {
                presenter.markFavUnfavProduct(view: cell, index: index.row)
            }
        }
    }
    
    func productCellAddButtonTapped(cell: ProductCell) {
        let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
        if isGuestUser {
            self.showGuestUserError()
        } else {
            if let index = tblView.indexPath(for: cell) {
                presenter.updateView(view: cell, index: index.row, add: true)
            }
        }
    }
    func productCellRemoveCategoryButtonTapped(cell: ProductCell) {
        print("productCellRemoveCategoryButtonTapped")
    }
    func productCellDeleteButtonTapped(cell: ProductCell) {
        let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
        if isGuestUser {
            self.showGuestUserError()
        } else {
            if let index = tblView.indexPath(for: cell) {
                presenter.updateView(view: cell, index: index.row, add: false)
            }
        }
    }
    
    func showGuestUserError() {
        let alertController = UIAlertController(title: "", message: "Please login to proceed further.", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .destructive, handler: { (action) -> Void in
            Singleton.sharedInstance.logoutButtonAction()
        })
        let noAction = UIAlertAction(title: "No", style: .cancel, handler: { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        //        alertController.view.tintColor = UIColor.red
        self.present(alertController, animated: true, completion: nil)
    }
    
}



protocol SearchCategoryView: class  {
    func reloadProductTable()
    func refreshCell(at index: Int, isAdded: Bool, cartCount: Int, totalPrice: Float)
}



protocol SearchCategoryPresenter {
//    var  noOfSections: Int {get}
//    func noOfItemsInSection(section: Int) -> Int
//    func identifierForSection(indexpath: IndexPath) -> String
//    func cellForRowProduct(indexpath: IndexPath) -> Category
//    func setupData(view: SubCategoryCellView, index: Int)
//    func getSubCategories(category: Category)
//    func didSelectItemAt(index: IndexPath)
    func getProductsForSubCategory(searchString: String)
    func numberOfItemsInTable() -> Int
    func setupDataTable(view: ProductCellView, index: Int)
    func updateView(view: ProductCellView, index: Int, add: Bool)
    func markFavUnfavProduct(view: ProductCellView, index: Int)
}

class SearchCategoryImplementation: SearchCategoryPresenter {
    
    weak var view: SearchCategoryView?
    var products = [Products]()
    var selectedIndex = 0
    
    init(withView view: SearchCategoryView) {
        self.view = view
       // self.getProductsForSubCategory(searchString: "")
    }
    
    func numberOfItemsInTable() -> Int {
        return products.count
    }
    func setupDataTable(view: ProductCellView, index: Int) {
        let product = products[index]
        view.setupDataForSubCategoryProducts(data: product)//(data: product)
    }
    func getProductsForSubCategory(searchString: String) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        Category.getCategoriesWithSearch(searchString: searchString) { (success, products)  in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if success {
                    guard let _products = products else {
                        return
                    }
                    self.products = _products
                    self.view?.reloadProductTable()
                } else {
                    self.products.removeAll()
                    self.view?.reloadProductTable()
                    ErrorView.showWith(message: "Sorry!! We are not serving in this area", removed: nil)
                }
            }
        }
    }
    
    func updateView(view: ProductCellView, index: Int, add: Bool) {
        if add {
            addProductToCart(product: products[index],view: view, index: index)
        } else {
            deleteProductFromCart(product: products[index],view: view, index: index)
        }
        
    }
    func addProductToCart(product: Products, view: ProductCellView, index: Int) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        product.addProduct { (success, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if success {
                view.updateValue(added: true)
                if let totalPrice = response!["total_price"] as? Double, let cartCount = response!["cart_count"] as? Int {
                    print(totalPrice, cartCount)
                    self.view?.refreshCell(at: index, isAdded: true, cartCount: cartCount, totalPrice: Float(totalPrice))
                }
                
                
            }
            
        }
    }
    //MARK: ERROR MESSAGE
    func showErrorMessage(error:String) {
        ErrorView.showWith(message: error, removed: nil)
    }
    
    func deleteProductFromCart(product: Products, view: ProductCellView, index: Int) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        product.deleteProduct { (success, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if success {
                view.updateValue(added: false)
                if let totalPrice = response!["total_price"] as? Double, let cartCount = response!["cart_count"] as? Int {
                    print(totalPrice, cartCount)
                    self.view?.refreshCell(at: index, isAdded: false, cartCount: cartCount, totalPrice: Float(totalPrice))
                }
            }
        }
    }
    
    func markFavUnfavProduct(view: ProductCellView, index: Int) {
        let product = self.products[index]
        ActivityIndicator.sharedInstance.showActivityIndicator()
        product.markFavUnfavApi { (success, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if success {
                if let status = self.products[index].is_fav, status == 1 {
                    self.products[index].is_fav = 0
                } else {
                    self.products[index].is_fav = 1
                }
                self.view?.reloadProductTable()
            }
        }
    }
}

protocol SearchCategoryCellView {
    func setupData(subCategory: String, underlineViewHidden: Bool)
}

class SearchProductListImplementation: ProductListPresenter {
    var products = [Products]() {
        didSet {
            view?.reloadTable()
        }
    }
    weak var view: ProductListCollectionView?
    
    init(withView view: ProductListCollectionView, products: [Products]) {
        self.view = view
        self.products = products
    }
    
    var noOfSections: Int {
        return 1
    }
    
    func noOfItemsInSection(section: Int) -> Int {
        return products.count
    }
    
    func identifierForSection(indexpath: IndexPath) -> String {
        return "ProductCell"
    }
    
    func setupData(view: SubCategoryBannerCellView, index: Int) {
        
    }
}

