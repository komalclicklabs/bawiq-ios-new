//
//  VendorProfileCell.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 11/29/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

//MARK: - PROTOCOL TO UPDATE DATA IN CONTROLLER


class VendorProfileCell: UITableViewCell,CustomFieldDelegate,ProfileProtocol {
    
    @IBOutlet weak var cellField: CustomTextField!
    var isEditting =  false
    var delegate : ProfileFieldChanged!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cellField = UINib(nibName: NIB_NAME.customTextField, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomTextField
        self.cellField.delegate = self
        self.cellField.profileDelegate = self
        self.cellField.frame = CGRect(x: 0, y:  15
            , width: self.frame.width, height: 30)
        self.addSubview(self.cellField)
        self.cellField.setImageForDifferentStates(inactive: IMAGE.iconEmailInActive, placeholderText: TEXT.YOUR_EMAIL, isPasswordField: false, unlock: nil)
        //self.emailField.alpha = 0.0
        //self.emailField.transform = CGAffineTransform(translationX: self.frame.width, y: 0)
        self.cellField.textField.keyboardType = UIKeyboardType.default
        self.cellField.textField.returnKeyType = UIReturnKeyType.done
        self.backgroundColor = .clear
    }
    
    
    //MARK: - SETTING CELL WITH FIELD TYPE
    func setCellInfo(fieldType:FIELD_TYPE,row:Int,detailModel:ProfileDetails){
        self.cellField.bottomLineBottomConstraint.constant = 0
        self.cellField.fieldType = fieldType
        self.layoutIfNeeded()
        switch fieldType {
        case .email:
            self.cellField.textField.isUserInteractionEnabled = false
            self.cellField.textField.text = detailModel.email
            self.cellField.setImageForDifferentStates(inactive: IMAGE.iconProfileMailFilled, placeholderText: TEXT.YOUR_EMAIL, isPasswordField: false, unlock: nil)
        case .contact:
            //self.cellField.setViewForPhoneOrNot()
            self.cellField.isUserInteractionEnabled = isEditting
            self.cellField.countryCodeButton.isUserInteractionEnabled = isEditting
            self.cellField.countryCodeButton.setTitle(detailModel.countryCode, for: .normal)
            self.cellField.textField.text = detailModel.phoneNumber
            self.cellField.setImageForDifferentStates(inactive: IMAGE.iconImageProfilePhone, placeholderText: TEXT.PHONE, isPasswordField: false, unlock: nil)
            
        case .companyName:
            self.cellField.textField.text = detailModel.company
            self.cellField.isUserInteractionEnabled = isEditting
            self.cellField.setImageForDifferentStates(inactive: IMAGE.iconImageCompany, placeholderText: TEXT.COMPANY_PLACEHOLDER, isPasswordField: false, unlock: nil)
            
        case .address:
            self.cellField.textField.text = detailModel.address
            self.cellField.isUserInteractionEnabled = isEditting
            self.cellField.setImageForDifferentStates(inactive: IMAGE.iconImageAddress, placeholderText: TEXT.ADDRESS_PLACEHODLER, isPasswordField: false, unlock: nil)
            
        case .changepassword:
            self.cellField.textField.text = TEXT.CHANGE_PASS_TITLE_REGULAR + " " + TEXT.CHANGE_PASS_TITLE_BOLD
            self.cellField.textField.isUserInteractionEnabled = false
            self.cellField.setImageForDifferentStates(inactive: IMAGE.iconImageChangePass, placeholderText: TEXT.YOUR_EMAIL, isPasswordField: false, unlock: nil)
            
        case .trackTask:
            self.cellField.textField.text = TEXT.TRACK + " " + Singleton.sharedInstance.formDetailsInfo.call_tasks_as //TEXT.TRACK_YOUR_ORDER
            self.cellField.textField.isUserInteractionEnabled = false
            self.cellField.setImageForDifferentStates(inactive: IMAGE.iconImageChangePass, placeholderText: TEXT.YOUR_EMAIL, isPasswordField: false, unlock: nil)
            
        case .orderHistory:
            self.cellField.textField.text = Singleton.sharedInstance.formDetailsInfo.call_tasks_as + " " + TEXT.HISTORY
            self.cellField.textField.isUserInteractionEnabled = false
            self.cellField.setImageForDifferentStates(inactive: IMAGE.iconImageChangePass, placeholderText: TEXT.YOUR_EMAIL, isPasswordField: false, unlock: nil)


            
        case .logout:
            self.cellField.textField.text = TEXT.LOGOUT_BUTTON
            self.cellField.textField.isUserInteractionEnabled = false
            self.cellField.setImageForDifferentStates(inactive: IMAGE.iconImageLogout, placeholderText: TEXT.YOUR_EMAIL, isPasswordField: false, unlock: nil)
            
        default:
            print("")
            
        }
    }
    
    //MARK: - TEXTFIELD DIDEND UPDATE
    func fieldEndUpdate(fieldType: Int, yourText: String) {
        self.delegate.profileValueChanged(fieldType: fieldType, yourText: self.cellField.textField.text!)
    }
    
    //MARK: - TEXTFIELD SHOULD RETURN
    func customFieldShouldReturn(fieldType: FIELD_TYPE) {
        print(fieldType)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
