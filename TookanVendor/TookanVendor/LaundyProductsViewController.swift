//
//  LaundyProductsViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 25/04/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

protocol LaundryButtonTapped:class  {
    func plusOrMinusButtonTapped()
}


class LaundyProductsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,ErrorDelegate,LaundryButtonTapped{

    @IBOutlet weak var laundryProductTableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var iconTick: UIButton!
    var errorMessageView:ErrorView!
    
    var toHideBackButton:Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitleView()
        self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        
      self.automaticallyAdjustsScrollViewInsets = false
        self.navigationController?.automaticallyAdjustsScrollViewInsets = false
        laundryProductTableView.register(UINib(nibName: NIB_NAME.laundryProductCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.laundryProductCell)
        laundryProductTableView.separatorStyle = .none
        iconTick.setImage(#imageLiteral(resourceName: "iconSaveTick"), for: UIControlState.normal)
        iconTick.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
        iconTick.layer.cornerRadius = 51.1/2
        iconTick.clipsToBounds = true
        if toHideBackButton == nil  {
            toHideBackButton = false    
        }
        self.backButton.isHidden = toHideBackButton!
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshHomeScreenAfterCreateTask(successData:)), name: NSNotification.Name(rawValue: NOTIFICATION_OBSERVER.refreshHomeScreenAfterCreateTask), object: nil)
        self.laundryProductTableView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        
        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(_ animated: Bool) {
        laundryProductTableView.reloadData()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        Singleton.sharedInstance.clearQuantityFromCategories()
        
        Singleton.sharedInstance.createTaskDetail = CreateTaskDetails(json: [:])
        Singleton.sharedInstance.resetMetaData()
        Singleton.sharedInstance.resetPickupMetaData()
        
        Singleton.sharedInstance.createTaskDetail.jobDescription = ""
      _ = self.navigationController?.popViewController(animated: true)
        
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:  CELL_IDENTIFIER.laundryProductCell, for: indexPath) as? laundryProductCell
        cell?.delegate = self
        cell?.selectionStyle = .none
        cell?.productName.text = Singleton.sharedInstance.typeCategories[indexPath.row].carTypeName
        cell?.priceLabel.text = Singleton.sharedInstance.formDetailsInfo.currencyid  + " " + "\(Double(Singleton.sharedInstance.typeCategories[indexPath.row].baseFare)!)"
        cell?.productImage.image = #imageLiteral(resourceName: "placeHolder")
        
        cell?.productImage.loadImageAsynchronously(Singleton.sharedInstance.typeCategories[indexPath.row].carImageString)
        cell?.updateQuantity(data: Singleton.sharedInstance.typeCategories[indexPath.row])
        return cell!
    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Singleton.sharedInstance.typeCategories.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 67
    }
    
    func setTitleView() {
        let titleView = UINib(nibName: NIB_NAME.titleView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TitleView
        titleView.frame = CGRect(x: 0, y: HEIGHT.navigationHeight, width: self.view.frame.width, height: HEIGHT.titleHeight)
        titleView.backgroundColor = UIColor.clear
        self.view.addSubview(titleView)
        titleView.setTitle(title: TEXT.addClothesToBas, boldTitle: TEXT.basket)
    }
    
    @IBAction func iconTikAction(_ sender: UIButton) {
        if Singleton.sharedInstance.checkIfQauntityIsMoreThanZero() == false    {
            self.showErrorMessage(error: TEXT.PleaseSelectOneItem, isError: true)
        }else{
            //self.showErrorMessage(error: "Success", isError: false)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.cartController) as? CartViewViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        }
    }
    
    func showErrorMessage(error:String,isError:Bool) {
        if errorMessageView == nil {
            errorMessageView = UINib(nibName: NIB_NAME.errorView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
            errorMessageView.delegate = self
            errorMessageView.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: HEIGHT.errorMessageHeight)
            self.view.addSubview(errorMessageView)
            errorMessageView.setErrorMessage(message: error,isError: isError)
        }
    }
    
    func removeErrorView() {
        self.errorMessageView = nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        footerView.backgroundColor = UIColor.white
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func refreshHomeScreenAfterCreateTask(successData:Notification){
        Singleton.sharedInstance.clearQuantityFromCategories()
        self.laundryProductTableView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if let message = successData.userInfo?["message"] as? String{
                self.showErrorMessage(error: message, isError: false)
            }
        }
    }
    
    func plusOrMinusButtonTapped() {
        if Singleton.sharedInstance.checkIfQauntityIsMoreThanZero() == false    {
            self.hideOrShowButtonThroughAnimation(toHide: true)
        }else{
            self.hideOrShowButtonThroughAnimation(toHide: false)
        }
    }
    
    func hideOrShowButtonThroughAnimation(toHide:Bool){
        switch toHide {
        case true:
            UIView.animate(withDuration: 0.2, animations: {
                self.iconTick.alpha = 0
            }, completion: { (true) in
                self.iconTick.isHidden = true
            })
        default:
            self.iconTick.isHidden = false
            UIView.animate(withDuration: 0.2, animations: { 
                self.iconTick.alpha = 1
            })
        }
        
    }
    

}
