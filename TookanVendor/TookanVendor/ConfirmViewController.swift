//
//  ConfirmViewController.swift
//  TookanVendor
//
//  Created by Vishal on 15/02/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class ConfirmViewController: UIViewController {

    @IBOutlet weak var btnOK: UIButton!
    var navigationBar:NavigationView!
    var orderID: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar()
        self.setupUICompenents()
        sendAccessTokenLoginHit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let cartSegmentVc = self.parent as? CartSegmentViewController {
            cartSegmentVc.navigationBar.titleLabel.text = "Payment Summary"
        }
    }
    
    private func sendAccessTokenLoginHit() {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.loginRequestWithAccessToken({ (isSuccess, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if isSuccess == true {
                Singleton.sharedInstance.isSignedIn = true
                print(response)
                if let status = response["status"] as? Int {
                    switch(status) {
                    case STATUS_CODES.SHOW_DATA:
                        DispatchQueue.main.async(execute: { () -> Void in
                            if let data = response["data"] as? [String:Any] {
                                print("Success")
                                Vendor.logInWith(data: data)
                                
                            }
                        })
                        
                    default:
                        break
                    }
                }
                
            } else {
                print("Error")
            }
        })
    }


    private func setupUICompenents() {
        btnOK.layer.cornerRadius = 24
    }
    
    @IBAction func okButtonAction(_ sender: Any) {
        
        if orderID == nil {
            
            
            if let vc = UIStoryboard(name: "MyOrders", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQWalletVC") as? BQWalletVC {
                
                sideMenuViewController?.contentViewController = vc
            }
            
            
//            let storyboard = UIStoryboard(name: "MyOrders", bundle: nil)
//            if let nextViewController = storyboard.instantiateViewController(withIdentifier: "BQWalletVC") as? BQWalletVC {
//                
//                self.navigationController?.pushViewController(nextViewController, animated: true)
//            }
            
            
//            if let vc = UIStoryboard(name: "MyOrders", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQWalletVC") as? BQWalletVC {
//                
//                if let walletMoney = BQBookingModel.shared.walletMoney {
//                    Vendor.current?.wallet_amount = Float(Float((Vendor.current?.wallet_amount)!) + Float(walletMoney)!)
//                }
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
        } else {
            if let vc = UIStoryboard(name: "MyOrders", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQOrderDetailsVC") as? BQOrderDetailsVC {
                vc.orderID = orderID
                vc.isComingFromOrderCreation = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        //Use for showing two right bar buttons on navigation bar
//        
        self.navigationController?.navigationBar.isHidden = true
        
        navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "Confirm", leftButtonImage: nil, rightButtonImage: nil, secondRightButtonImage: nil)
            , leftButtonAction: nil,
              rightButtonAction: nil,
               rightButtonSecondAction: nil
        )
        navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
    }
}
