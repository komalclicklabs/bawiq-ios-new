//
//  TutorialCollectionViewCell.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 07/01/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class TutorialCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
}
