//
//  BQAddCommentCell.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 06/06/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit
protocol BQAddCommentCellDelegate: class {
    func cancelComment(textReview: String)
}

class BQAddCommentCell: UITableViewCell, UITextViewDelegate {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtView: UITextView!
    @IBOutlet weak var bgView: UIView!
    weak var delegate: BQAddCommentCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    class func cellIdentifier() -> String {
        return String(describing: BQAddCommentCell.self)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Please input your reviews..." {
            textView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Please input your reviews..."
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let txtString = textView.text else {
            return false
        }
        let finalString = (txtString as NSString).replacingCharacters(in: range, with: text)
        self.delegate?.cancelComment(textReview: finalString)
        
        return true
    }
    
}
