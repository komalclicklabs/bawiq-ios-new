//
//  FamilyMemberTableViewCell.swift
//  TookanVendor
//
//  Created by Vishal on 30/01/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

protocol FamilyMemberTableViewCellDelegate {
    func deleteFamilyMember()
}

class FamilyMemberTableViewCell: UITableViewCell {

    @IBOutlet weak var backgroundViewCell: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var walletLimit: UILabel!
    @IBOutlet weak var phone: UILabel!
    var delegate: FamilyMemberTableViewCellDelegate?
    
    var data: FamilyMember? {
        didSet {
            guard let _data = data else {
                return
            }
            self.name.text = _data.username
            self.email.text = _data.email
            self.walletLimit.text = "AED " + String(_data.walletLimit)
            self.phone.text = _data.phoneNo
            self.backgroundViewCell.setShadow()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
//        backgroundView?.border(color: COLOR.shadowColorNew)
        // Initialization code
    }
    
    @IBAction func deleteMemberAction(_ sender: UIButton) {
        if let _delegate = delegate {
            _delegate.deleteFamilyMember()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
