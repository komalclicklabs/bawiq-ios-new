//
//  BQComplainCell.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 16/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

protocol BQComplainCellDelegate: class {
    func selectedComplainRating(isSelected: Bool, rating: String)
}


class BQComplainCell: UITableViewCell {

    @IBOutlet weak var btnComplain5: UIButton!
    @IBOutlet weak var btnComplain4: UIButton!
    @IBOutlet weak var btnComplain3: UIButton!
    @IBOutlet weak var btnComplain2: UIButton!
    @IBOutlet weak var btnComplain1: UIButton!
    @IBOutlet weak var bgView: UIView!
    weak var delegate: BQComplainCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        btnComplain1.layer.borderColor = UIColor.gray.cgColor
        btnComplain1.layer.borderWidth = 1.0
        btnComplain1.layer.cornerRadius = 15.0
        
        btnComplain2.layer.borderColor = UIColor.gray.cgColor
        btnComplain2.layer.borderWidth = 1.0
        btnComplain2.layer.cornerRadius = 15.0
        
        btnComplain3.layer.borderColor = UIColor.gray.cgColor
        btnComplain3.layer.borderWidth = 1.0
        btnComplain3.layer.cornerRadius = 15.0
        
        btnComplain4.layer.borderColor = UIColor.gray.cgColor
        btnComplain4.layer.borderWidth = 1.0
        btnComplain4.layer.cornerRadius = 15.0
        
        btnComplain5.layer.borderColor = UIColor.gray.cgColor
        btnComplain5.layer.borderWidth = 1.0
        btnComplain5.layer.cornerRadius = 15.0
        
    }
    @IBAction func complainButton1Pressed(_ sender: UIButton) {
        if !btnComplain1.isSelected {
            btnComplain1.backgroundColor = COLOR.App_Red_COLOR
            btnComplain1.setTitleColor(.white, for: .normal)
            btnComplain1.isSelected = true
        } else {
            btnComplain1.setTitleColor(.gray, for: .normal)
            btnComplain1.backgroundColor = .white
            btnComplain1.setTitleColor(.gray, for: .normal)
            btnComplain1.isSelected = false
        }
        delegate?.selectedComplainRating(isSelected:sender.isSelected, rating: btnComplain1.titleLabel?.text ?? "")
    }
    @IBAction func complainButton2Pressed(_ sender: UIButton) {
        if !btnComplain2.isSelected {
            btnComplain2.backgroundColor = COLOR.App_Red_COLOR
            btnComplain2.setTitleColor(.white, for: .normal)
            btnComplain2.isSelected = true
        } else {
            btnComplain2.backgroundColor = .white
            btnComplain2.setTitleColor(.gray, for: .normal)
            btnComplain2.isSelected = false
        }
        delegate?.selectedComplainRating(isSelected:sender.isSelected, rating: btnComplain2.titleLabel?.text ?? "")
    }
    @IBAction func complainButton3Pressed(_ sender: UIButton) {
        if !btnComplain3.isSelected {
            btnComplain3.backgroundColor = COLOR.App_Red_COLOR
            btnComplain3.setTitleColor(.white, for: .normal)
            btnComplain3.isSelected = true
        } else {
            btnComplain3.backgroundColor = .white
            btnComplain3.setTitleColor(.gray, for: .normal)
            btnComplain3.isSelected = false
        }
        delegate?.selectedComplainRating(isSelected:sender.isSelected, rating: btnComplain3.titleLabel?.text ?? "")
    }
    @IBAction func complainButton4Pressed(_ sender: UIButton) {
        if !btnComplain4.isSelected {
            btnComplain4.backgroundColor = COLOR.App_Red_COLOR
            btnComplain4.setTitleColor(.white, for: .normal)
            btnComplain4.isSelected = true
        } else {
            btnComplain4.backgroundColor = .white
            btnComplain4.setTitleColor(.gray, for: .normal)
            btnComplain4.isSelected = false
        }
        delegate?.selectedComplainRating(isSelected:sender.isSelected, rating: btnComplain4.titleLabel?.text ?? "")
    }
    @IBAction func complainButton5Pressed(_ sender: UIButton) {
        if !btnComplain5.isSelected {
            btnComplain5.backgroundColor = COLOR.App_Red_COLOR
            btnComplain5.setTitleColor(.white, for: .normal)
            btnComplain5.isSelected = true
        } else {
            btnComplain5.backgroundColor = .white
            btnComplain5.setTitleColor(.gray, for: .normal)
            btnComplain5.isSelected = false
        }
        delegate?.selectedComplainRating(isSelected:sender.isSelected, rating: btnComplain5.titleLabel?.text ?? "")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
