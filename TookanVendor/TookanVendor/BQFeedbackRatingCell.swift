//
//  BQFeedbackRatingCell.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 14/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

protocol BQFeedbackRatingCellDelegate: class {
    func ratingProvideToDriver(textReview: String, ratings: Int)
    func ratingCountForServiceProvider(ratings: Int)
}


class BQFeedbackRatingCell: UITableViewCell {

    weak var delegate: BQFeedbackRatingCellDelegate?
    @IBOutlet weak var btnStar1: UIButton!
    @IBOutlet weak var btnStar2: UIButton!
    @IBOutlet weak var btnStar3: UIButton!
    @IBOutlet weak var btnStar4: UIButton!
    @IBOutlet weak var btnStar5: UIButton!
    @IBOutlet weak var lblStatus: UILabel!
    
    var rating = 1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func ratingButtonspressed(_ sender: Any) {
        switch (sender as AnyObject).tag {
        case 1:
            btnStar1.setImage(#imageLiteral(resourceName: "rated"), for: .normal)
            btnStar2.setImage(#imageLiteral(resourceName: "unRated"), for: .normal)
            btnStar3.setImage(#imageLiteral(resourceName: "unRated"), for: .normal)
            btnStar4.setImage(#imageLiteral(resourceName: "unRated"), for: .normal)
            btnStar5.setImage(#imageLiteral(resourceName: "unRated"), for: .normal)
            lblStatus.text = "Worse"
        case 2:
            btnStar1.setImage(#imageLiteral(resourceName: "rated"), for: .normal)
            btnStar2.setImage(#imageLiteral(resourceName: "rated"), for: .normal)
            btnStar3.setImage(#imageLiteral(resourceName: "unRated"), for: .normal)
            btnStar4.setImage(#imageLiteral(resourceName: "unRated"), for: .normal)
            btnStar5.setImage(#imageLiteral(resourceName: "unRated"), for: .normal)
            lblStatus.text = "Bad"
        case 3:
            btnStar1.setImage(#imageLiteral(resourceName: "rated"), for: .normal)
            btnStar2.setImage(#imageLiteral(resourceName: "rated"), for: .normal)
            btnStar3.setImage(#imageLiteral(resourceName: "rated"), for: .normal)
            btnStar4.setImage(#imageLiteral(resourceName: "unRated"), for: .normal)
            btnStar5.setImage(#imageLiteral(resourceName: "unRated"), for: .normal)
            lblStatus.text = "Good"
        case 4:
            btnStar1.setImage(#imageLiteral(resourceName: "rated"), for: .normal)
            btnStar2.setImage(#imageLiteral(resourceName: "rated"), for: .normal)
            btnStar3.setImage(#imageLiteral(resourceName: "rated"), for: .normal)
            btnStar4.setImage(#imageLiteral(resourceName: "rated"), for: .normal)
            btnStar5.setImage(#imageLiteral(resourceName: "unRated"), for: .normal)
            lblStatus.text = "Very Good"
        case 5:
            btnStar1.setImage(#imageLiteral(resourceName: "rated"), for: .normal)
            btnStar2.setImage(#imageLiteral(resourceName: "rated"), for: .normal)
            btnStar3.setImage(#imageLiteral(resourceName: "rated"), for: .normal)
            btnStar4.setImage(#imageLiteral(resourceName: "rated"), for: .normal)
            btnStar5.setImage(#imageLiteral(resourceName: "rated"), for: .normal)
            lblStatus.text = "Awesome"
        default:
            print("btnStar1")
        }
        rating = (sender as AnyObject).tag
        self.delegate?.ratingCountForServiceProvider(ratings: (sender as AnyObject).tag)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
