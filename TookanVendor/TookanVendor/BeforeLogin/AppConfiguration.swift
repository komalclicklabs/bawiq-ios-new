//
//  AppConfiguration.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 29/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation


class AppConfiguration {
  
  private(set) static var current = AppConfiguration()
  
   // MARK: - Properties
   
  private(set) var user_id = ""
  private(set) var appDeviceType = ""
  private(set) var contactEmail = ""
  private(set) var contactNumber = ""
  private(set) var FuguToken = ""
  fileprivate var appVersionAtBackend = 0
  fileprivate var isForcedUpdate = false
  var currencyid = ""
  private(set) var appUrl = ""
  
  var vat = "0"
  var serviceTax = "0"
  var formId = ""
  
  private(set) var isLoginViaEmailRequired = "1" // not coming from backend yet
  private(set) var isFacebookRequired = "0"
  private(set) var isGoogleRequired = "0"
  private(set) var isOtpRequired = "0"
  private(set) var isPromoRequired = "0"
  private(set) var isReferralRequired = false
  private(set) var isCreditEnabled = "0"
  private(set) var show_payment_screen = "0"
  private(set) var showTermsAndConditions = "0"
  private(set) var isFuguEnabled = false
  private(set) var tAndConditions = TERMS_OF_SERVICE_LINK
  var isDemoEnabled = false
  let currentApiKey = DEMO_API_KEY.beauty
  var currentReferenceId = "\(DEMO_REFRENCE_ID.beauty)"
    
  var isSocialLoginAvailable: Bool {
    return isGoogleRequired == "1" || isFacebookRequired == "1"
  }
  
  private(set) var referral = Referral()
   
   // MARK: - Intializer
  private init() {
  }
  
  private init(json: [String: Any]) {
    print(json)
    if let id = json["user_id"] as? NSNumber {
        self.user_id = "\(id)"
    } else if let id = json["user_id"] as? String{
        self.user_id = id
    }
    
    if let value = json["t_n_c"] as? String{
        self.tAndConditions = value
    }
    
    if let value = json["show_t_n_c"] as? String{
        self.showTermsAndConditions = value
    }else if let value = json["show_t_n_c"] as? Int{
        self.showTermsAndConditions = "\(value)"
    }
    
    if let vat = json["vat"] {
      self.vat = "\(vat)"
    }
    if let serviceTax = json["service_tax"] {
      self.serviceTax = "\(serviceTax)"
    }
    
    self.appUrl = (json["app_url"] as? String) ?? ""
    self.isForcedUpdate = (json["is_force"] as? Bool) ?? false
    if let appVersion = json["app_version"] {
      self.appVersionAtBackend = Int("\(appVersion)") ?? 0
    }

    if let value = json["form_id"] as? String{
        self.formId = value
    }else if let value = json["form_id"] as? Int{
        self.formId = "\(value)"
    }
    
    if let value = json["payment_settings"] as? [Any]{
        print(value)
        
        for i in value{
            if let data = i as? [String:Any]{
                if let symbol = data["symbol"] as? String{
                    self.currencyid = symbol
                }
            }
        }
    }
    
    
    self.isFacebookRequired  = convertBoolTypeVariablesToString(withKey: "is_facebook_required", inDict: json)
    self.isGoogleRequired    = convertBoolTypeVariablesToString(withKey: "isGoogleRequired", inDict: json)
    self.isOtpRequired       = convertBoolTypeVariablesToString(withKey: "is_otp_required", inDict: json)
    self.isPromoRequired     = convertBoolTypeVariablesToString(withKey: "is_promo_required", inDict: json)
    self.isReferralRequired  = (json["is_referral_required"] as? Bool) ?? false
    self.isCreditEnabled     = convertBoolTypeVariablesToString(withKey: "is_credit_enabled", inDict: json)
    self.isFuguEnabled       = (json["is_fugu_chat_enabled"] as? Bool) ?? false
    self.FuguToken           = convertBoolTypeVariablesToString(withKey: "fugu_chat_token", inDict: json)
    self.referral            = Referral(fromJson: json)
    
  }
  
  private func convertBoolTypeVariablesToString(withKey key: String, inDict dict: [String: Any]) -> String {
    if let value = dict[key] {
      return "\(value)"
    } else {
      return "0"
    }
  }
  
  
  // MARK: - Configuration
  class func fetchFromServer(completion: @escaping (Bool, Error?) -> Void) {
    
    let params = getParamsTofetchAppConfigurations()
    
    HTTPClient.makeConcurrentConnectionWith(method: .POST, showAlert: false, showAlertInDefaultCase: false, showActivityIndicator: false, para: params, extendedUrl: API_NAME.fetchAppConfiguration) { (responseObject, error, extendedUrl, statusCode) in
      
      guard error == nil, let response = responseObject as? [String: Any],  let data = response["data"] as? [String: Any] else {
        completion(false, error)
        return
      }
        
      current = AppConfiguration(json: data)
      completion(true, nil)
      
    }
    
  }
  
  private class func getParamsTofetchAppConfigurations() -> [String: Any] {
    let param: [String: Any] = [
//      "device_token": getDeviceToken(),
 //     "app_version":APP_VERSION,
//        "ios_app_version":APP_VERSION,
      "app_device_type": 1 //APP_DEVICE_TYPE
    ]
    return param
  }
  
  private class func getDeviceToken() -> String {
    return APIManager.sharedInstance.getDeviceToken()
  }
  
  private func isAccessTokenAvailable() -> Bool {
    return UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) != nil
  }
   
  
}



//MARK: - App Versioning
extension AppConfiguration {
  enum AppUpdate {
    case optional
    case forced
    case none
    
    func getUpdateMessage() -> String {
      switch self {
      case .optional:
        return TEXT.APP_UPDATE_NORMAL
      case .forced:
        return TEXT.APP_UPDATE_FORCED
      default:
        return ""
      }
    }
  }
  
  func getAppUpdateType() -> AppUpdate {
    //MARK:- vishalvirodhia: removed for now
   // return .none
    
    if isAppVersionLowerThanLatestLiveVersion() {
      return isForcedUpdate ? .forced : .optional
    }
    
    return .none
  }
  
  private func isAppVersionLowerThanLatestLiveVersion() -> Bool {
    return APP_VERSION < appVersionAtBackend
  }
  
}


