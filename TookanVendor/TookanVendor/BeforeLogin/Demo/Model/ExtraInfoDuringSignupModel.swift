//
//  extraInfoModel.swift
//  TookanVendor
//
//  Created by CL-Macmini-110 on 9/25/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation


class ExtraInfoDuringSignup: NSObject {
   // MARK:- Propreties
   var companyName: String? = ""
   var teamSize: String? = ""
   var companyAddress: String? = ""
   var companyLatitude: Double? = 0.0
   var companyLongitude: Double? = 0.0
   
   // MARK:- Server Hit
   func sendExtraInfoDataToServer(receivedResponse:@escaping (_ success: Bool) -> Void) {
      var params : [String:Any] = ["app_device_type": APP_DEVICE_TYPE]
      params["app_access_token"] = Vendor.current?.appAccessToken
      params["company_latitude"] = self.companyLatitude
      params["company_longitude"] = self.companyLongitude
      params["company_name"] = self.companyName
      params["company_address"] = self.companyAddress
      params["use_app"] = "I have a pen, I have an apple"
      params["team_size"] = self.teamSize
      params["business_vertical"] = "Pickup & Delivery"
      
      HTTPClient.makeMultiPartRequestWith(method: .POST, para: params, extendedUrl: API_NAME.company_details_after_signup) {
         (responseObject, error , _, statusCode) in
         
         guard statusCode == STATUS_CODES.SHOW_DATA,
         responseObject is [String: Any] else {
            receivedResponse(false)
            return
         }
         receivedResponse(true)
      }
   }
   
}
