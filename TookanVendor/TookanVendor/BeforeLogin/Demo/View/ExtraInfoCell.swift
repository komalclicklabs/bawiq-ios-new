//
//  ExtraInfoCell.swift
//  TookanVendor
//
//  Created by CL-Macmini-110 on 9/21/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

protocol ExtraInfoCellDelegate {
   func textViewBeginEditing(_ textView: UITextView)
   func textViewChanged(_ textView: UITextView)
}

class ExtraInfoCell: UITableViewCell {
   // MARK:- Outlet
   @IBOutlet var extraInfoTextView: UITextView!
   @IBOutlet var bottomView: UIView!
   @IBOutlet var placeHolderLabel: UILabel!
   
   var delegate: ExtraInfoCellDelegate!
   // MARK:- View life cycle
   override func awakeFromNib() {
      super.awakeFromNib()
      self.setBottomView()
      self.setExtraInfoTextView()
      self.setPlaceHolderLabel()
   }
   
   // MARK:- Setting Views
   func setExtraInfoTextView() {
      self.extraInfoTextView.textColor = UIColor.black
      self.extraInfoTextView.font = UIFont(name: FONT.light, size: FONT_SIZE.priceFontSize)
      self.extraInfoTextView.delegate = self
   }
   
   func setBottomView() {
      self.bottomView.backgroundColor = COLOR.SPLASH_LINE_COLOR
   }
   
   func setPlaceHolderLabel() {
      self.placeHolderLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.priceFontSize)
      self.placeHolderLabel.textColor = COLOR.SUBTITLE_LIGHT_COLOR
      self.placeHolderLabel.letterSpacing = 0.5
   }
   
   func setCompanyNameField(companyName: String) {
      self.placeHolderLabel.text = "\(TEXT.BUSSINESS) / \(TEXT.COMPANY_PLACEHOLDER) \(TEXT.NAME)"
      self.extraInfoTextView.text = companyName
      self.extraInfoTextView.isUserInteractionEnabled = true
   }
   
   func setCompanyAddressField(companyAddress: String) {
      self.placeHolderLabel.text = "\(TEXT.BUSSINESS) / \(TEXT.COMPANY_PLACEHOLDER) \(TEXT.address)"
      self.extraInfoTextView.text = companyAddress
      self.extraInfoTextView.isUserInteractionEnabled = false
   }
   
   func setTeamSizeField(teamSize: String) {
      self.placeHolderLabel.text = TEXT.TEAM_SIZE
      self.extraInfoTextView.text = teamSize
      self.extraInfoTextView.isUserInteractionEnabled = true
   }
}


//MARK:- TextViewDelegate
extension ExtraInfoCell: UITextViewDelegate {
   
   func textViewDidBeginEditing(_ textView: UITextView) {
      
      self.delegate.textViewBeginEditing(textView)
   }
   
   func textViewDidChange(_ textView: UITextView) {

      self.delegate.textViewChanged(textView)
      if textView.text.isEmpty == true {
         self.placeHolderLabel.isHidden = false
      } else {
         self.placeHolderLabel.isHidden = true
      }
      textView.sizeToFit()

   }
}
