//
//  WelcomeVC.swift
//  TookanVendor
//
//  Created by CL-Macmini-110 on 9/22/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import DGActivityIndicatorView

class WelcomeVC: UIViewController {
   // MARK:- Outlets
   @IBOutlet var tryDemoButton: UIButton!
   @IBOutlet var signupButton: UIButton!
   @IBOutlet var logoImage: UIImageView!
   @IBOutlet var welcomeLabel: UILabel!
   @IBOutlet var centerimageView: UIImageView!
   
   // MARK:- View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      self.fetchAppConfigFromServer()
      self.setLogoImage()
      self.setSignupButton()
      self.setWelcomeLabel()
      self.setTryDemoButton()
      self.setCenterImageView()
   }
   
   override func viewWillAppear(_ animated: Bool) {
      self.navigationController?.isNavigationBarHidden = true
   }
   
   // MARK:- Setting Views
   private func setTryDemoButton() {
      self.tryDemoButton.configureNormalButtonWithOppositeColorThemeWith(title: TEXT.TRY_DEMO)
      self.tryDemoButton.isExclusiveTouch = true
   }
   
   private func setSignupButton() {
      self.signupButton.configureNormalButtonWith(title: "\(TEXT.SIGN_UP) / \(TEXT.SIGN_IN)")
   }
   
   private func setLogoImage() {
      self.logoImage.image = #imageLiteral(resourceName: "logoTookanSplashGraphic")
   }
   
   private func setCenterImageView() {
      self.centerimageView.image = #imageLiteral(resourceName: "FoodDeliveryLogo")
      
   }
   
   private func setWelcomeLabel() {
      self.welcomeLabel.attributedText = self.getAttributedTotalText(title: "\(TEXT.welcome) \(TEXT.TO)", subTitle: APP_NAME)
      self.welcomeLabel.numberOfLines = 0
   }
   
   // MARK: - Setting Attributed Text
   private func getAttributedTotalText(title:String,subTitle:String) -> NSMutableAttributedString {
      let attributedString = NSMutableAttributedString(string: "\(title)\n", attributes: [NSForegroundColorAttributeName:COLOR.SPLASH_TEXT_COLOR, NSFontAttributeName:UIFont(name: FONT.light, size: FONT_SIZE.large)!])
      
      let secondString = NSMutableAttributedString(string: "\(subTitle)", attributes: [NSForegroundColorAttributeName:COLOR.SPLASH_TEXT_COLOR, NSFontAttributeName:UIFont(name: FONT.regular, size: FONT_SIZE.extraLarge)!])
      attributedString.append(secondString)
      return attributedString
   }
   
   // MARK: - IBAction
   @IBAction func tryDemoAction(_ sender: Any) {
      
      if let vc = UIViewController.findIn(storyboard: .afterLogin, withIdentifier: "GenricHomeWithSideMenuViewController") as? GenricHomeWithSideMenuViewController {
         self.navigationController?.pushViewController(vc, animated: true)
         
//         let appWindow = (UIApplication.shared.delegate  as! AppDelegate).window
//         appWindow?.rootViewController = vc
      }
      
      
   }
   
   @IBAction func signupAction(_ sender: Any) {
      let storyboard = UIStoryboard(name: STORYBOARD_NAME.main, bundle: Bundle.main)
      let completion = storyboard.instantiateViewController(withIdentifier: STORYBOARD_ID.splashController) as! SplashController
      self.navigationController?.pushViewController(completion, animated: true)
   }
   
   // MARK: - Server Hit
   private func fetchAppConfigFromServer() {
      ActivityIndicator.sharedInstance.showActivityIndicator()
      AppConfiguration.fetchFromServer { [weak self] (success, error) in
         guard success else {
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            UIAlertController.showWith(message: error?.localizedDescription ?? ERROR_MESSAGE.SERVER_NOT_RESPONDING, title: nil, buttonTitle: TEXT.RETRY) {
               self?.fetchAppConfigFromServer()
            }
            return
         }
         self?.handleAppVersioningCheck {
            if self?.isAccessTokenAvailable() == .some(true) {
               self?.sendAccessTokenLoginHit()
               return
            }
            ActivityIndicator.sharedInstance.hideActivityIndicator()
         }
      }
   }
   
   private func sendAccessTokenLoginHit() {
      ActivityIndicator.sharedInstance.showActivityIndicator()
      APIManager.sharedInstance.loginRequestWithAccessToken({ (isSuccess, response) in
         ActivityIndicator.sharedInstance.hideActivityIndicator()
         if isSuccess == true {
            Singleton.sharedInstance.isSignedIn = true
            print(response)
            if let status = response["status"] as? Int {
               switch(status) {
               case STATUS_CODES.SHOW_DATA:
                  DispatchQueue.main.async(execute: { () -> Void in
                     if let data = response["data"] as? [String:Any] {
                        Vendor.logInWith(data: data)
                        Singleton.sharedInstance.checkForLogin(owener: self, startLoaderAnimation: {
                           ActivityIndicator.sharedInstance.showActivityIndicator()
                        }, stopLoaderAnimation: {
                           ActivityIndicator.sharedInstance.hideActivityIndicator()
                        })
                     }
                  })
                  
               default:
                  break
               }
            }
            
         } else {
            Singleton.sharedInstance.enableDisableIQKeyboard(enable: false)
            if (response["status"] as? Int) != nil {
               self.showErrorMessage(error: response["message"] as! String)
            } else {
               Singleton.sharedInstance.showAlertWithOption(owner: self, message: response["message"] as! String, leftButtonAction: {
                  self.sendAccessTokenLoginHit()
               }, leftButtonTitle: TEXT.RETRY)
            }
            
         }
      })
   }
   
   func serverHitForCatalogue() {
      var formId = ""
      if Singleton.sharedInstance.WorkFlowFormDetailsOptions.count > 0{
         formId = Singleton.sharedInstance.WorkFlowFormDetailsOptions[0].form_id!
      }
      
      if Singleton.sharedInstance.WorkFlowFormDetailsOptions.count == 1{
         ActivityIndicator.sharedInstance.showActivityIndicator()
         Catalogue.getCatalogueOfFormWithDemo(formId: formId) { (success, catalogue, error) in
            print("CATALOGUE")
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            guard success else {
               self.tryDemoButton.isUserInteractionEnabled = true
               ErrorView.showWith(message: error?.localizedDescription ?? "", isErrorMessage: true, removed: nil)
               return
            }

            guard catalogue != nil else{
               ErrorView.showWith(message: ERROR_MESSAGE.NO_CATALOGUE_FOUND, isErrorMessage: true, removed: nil)
               return
            }

            Singleton.sharedInstance.instantiateWorkFlowFor(catalogue: catalogue!, navVC: Singleton.sharedInstance.getAfterLoginMainNavigationController())
         }
      } else {
         ActivityIndicator.sharedInstance.hideActivityIndicator()
         self.tryDemoButton.isUserInteractionEnabled = true
         if Singleton.sharedInstance.WorkFlowFormDetailsOptions.count > 1 {
            if let vc = UIViewController.findIn(storyboard: .main, withIdentifier: STORYBOARD_ID.servicesOptions) as? ServicesOptionsViewController{
               self.navigationController?.pushViewController(vc, animated: true)
            }
         }
      }
   }
   
   
   //MARK: - ERROR MESSAGE
   private func showErrorMessage(error:String) {
      ErrorView.showWith(message: error, removed: nil)
   }
   
   private func isAccessTokenAvailable() -> Bool {
      return UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) != nil
   }
   
   //MARK: - AppVersioning
   private func handleAppVersioningCheck(completion: @escaping () -> Void) {
      let updateType = AppConfiguration.current.getAppUpdateType()
      
      guard updateType != .none else {
         completion()
         return
      }
      
      let appVersioningView = loadAppVersioningView(cancelAction: completion)
      set(appVersioningView: appVersioningView, forUpdateType: updateType)
      
      appVersioningView.addViewToWindowWithAnimation()
   }
   
   private func loadAppVersioningView(cancelAction: @escaping () -> Void) -> ChangePhoneNumberView {
      let intialTexts = ChangePhoneNumberView.PopupParams()
      
      let appVersionView = ChangePhoneNumberView.getWithHeading(texts: intialTexts, successAction: {
         guard let appURl = URL(string: AppConfiguration.current.appUrl) else {
            return
         }
         UIApplication.shared.openURL(appURl)}, cancelAction: cancelAction)
      
      return appVersionView
   }
   
   private func set(appVersioningView: ChangePhoneNumberView, forUpdateType updateType: AppConfiguration.AppUpdate) {
      let texts = ChangePhoneNumberView.PopupParams(heading: updateType.getUpdateMessage(), textFieldPlaceholder: "", textFieldtext: "", removeAlertButtonTitle: TEXT.CANCEL, saveButtonAlertTitle: TEXT.DOWNLOAD)
      
      appVersioningView.setViewWithNew(texts: texts)
      
      switch updateType {
      case .forced:
         appVersioningView.cancelButton.isHidden = true
      default:
         break
      }
   }
}
