//
//  OrderSuccessfull.swift
//  TookanVendor
//
//  Created by CL-Macmini-110 on 9/21/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import Fugu

class OrderSuccessfullVC: UIViewController {
   // MARK:- Outlets
   @IBOutlet var similarAppsLabel: UILabel!
   @IBOutlet var tickImageView: UIImageView!
   @IBOutlet var contactUsButton: UIButton!
   @IBOutlet var successLabel: UILabel!
   @IBOutlet var signUpButton: UIButton!
   @IBOutlet var centerImageView: UIImageView!
   @IBOutlet var closeButton: UIButton!
   var createTaskModel = CreateTaskModal()
   // MARK:- View Life cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      self.setLogoImageView()
      self.setSuccessLabel()
      self.setSimilarAppsLabel()
      self.setContactUsButton()
      self.setSignupButton()
      self.setCenterImageView()
      self.setCloseButton()
   }
   
   //MARK:- Setting Views
   private func setLogoImageView() {
      self.tickImageView.image = #imageLiteral(resourceName: "greenTick")
   }
   
   private func setSuccessLabel() {
      self.successLabel.text = TEXT.VISIT_DASHBOARD
      self.successLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
      self.successLabel.textColor = JOB_STATUS_COLOR.CANCELLED
      self.successLabel.letterSpacing = 0.5
      self.successLabel.numberOfLines = 0
   }
   
   private func setSimilarAppsLabel() {
      self.similarAppsLabel.text = TEXT.SIMILAR_APPS
      self.similarAppsLabel.textColor = COLOR.SPLASH_TEXT_COLOR
      self.similarAppsLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
      self.similarAppsLabel.numberOfLines = 0
   }
   
   private func setContactUsButton() {
      guard Vendor.current == nil else {
         self.contactUsButton.configureNormalButtonWith(title: TEXT.CONTACT_US)
         return
      }
      self.contactUsButton.configureNormalButtonWithOppositeColorThemeWith(title: TEXT.CONTACT_US)
   }
   
   private func setSignupButton() {
      guard Vendor.current == nil else {
         self.signUpButton.isHidden = true
         return
      }
      self.signUpButton.isHidden = false
      self.signUpButton.configureNormalButtonWith(title: "\(TEXT.SIGN_UP) / \(TEXT.SIGN_IN)")
   }
   
   private func setCenterImageView() {
      self.centerImageView.image = #imageLiteral(resourceName: "FoodDeliveryLogo")
   }
   
   private func setCloseButton() {
      self.closeButton.setImage(#imageLiteral(resourceName: "close").renderWithAlwaysTemplateMode(), for: .normal)
      self.closeButton.tintColor = COLOR.SIGNIN_TITLE_TYPE_LABEL
   }
   
   //MARK:- IBAction
   @IBAction func contactUsAction(_ sender: Any) {
      if Vendor.current == nil {
         FuguConfig.shared = FuguConfig.init(fullName: "" , email: "", phoneNumber: "", userUniqueKey: "", deviceToken: APIManager.sharedInstance.getDeviceToken())
      } else {
         FuguConfig.shared  = FuguConfig.init(fullName: Vendor.current!.firstName! + " " + Vendor.current!.lastName! , email: Vendor.current!.email!, phoneNumber: Vendor.current!.phoneNo, userUniqueKey: "\(Vendor.current!.id!)", deviceToken: APIManager.sharedInstance.getDeviceToken())
      }
      FuguConfig.shared.appSecretKey = "\(AppConfiguration.current.FuguToken)"
      FuguConfig.shared.registerUserDetails()
      customizeFugu()
      FuguConfig.shared.pushChatsViewController(self.navigationController!)
      Singleton.sharedInstance.enableDisableIQKeyboard(enable: false)
   }
   
   @IBAction func signUpAction(_ sender: Any) {
      let storyboard = UIStoryboard(name: STORYBOARD_NAME.main, bundle: Bundle.main)
      let completion = storyboard.instantiateViewController(withIdentifier: STORYBOARD_ID.splashController) as! SplashController
      self.navigationController?.pushViewController(completion, animated: true)
   }
   
   @IBAction func closeButtonAction(_ sender: Any) {
      self.createTaskModel.afterTaskCreation()
      self.navigationController?.popToRootViewController(animated: true)
   }
   
   //MARK:- Interating FuguChat
   private func customizeFugu(){
      FuguConfig.shared.backButtonImageIcon = #imageLiteral(resourceName: "iconBackTitleBar")
      FuguConfig.shared.themColor = COLOR.THEME_FOREGROUND_COLOR
      FuguConfig.shared.navigationTitleColor = COLOR.SPLASH_TEXT_COLOR
      FuguConfig.shared.navigationBackgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
      FuguConfig.shared.navigationHeaderViewBackgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
   }
   
}
