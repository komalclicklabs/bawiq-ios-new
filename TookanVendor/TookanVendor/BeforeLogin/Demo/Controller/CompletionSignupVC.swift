//
//  CompletionSignup.swift
//  TookanVendor
//
//  Created by CL-Macmini-110 on 9/21/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class CompletionSignupVC: UIViewController {
   // MARK:- Outlets
   @IBOutlet var tickImageView: UIImageView!
   @IBOutlet var exploringLabel: UILabel!
   @IBOutlet var infoLabel: UILabel!
   @IBOutlet var proceedWithDemoButton: UIButton!
   @IBOutlet var calendlyButton: UIButton!
   // MARK:- View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      self.setImage()
      self.setProceedWithDemoButton()
      self.setExploringLabel()
      self.setInfoLabel()
      self.setCalendelyButton()
   }
   
   //MARK:- Setting Views
   private func setImage() {
      self.tickImageView.image = #imageLiteral(resourceName: "greenTick")
   }
   
   private func setCalendelyButton() {
      self.calendlyButton.setTitle(TEXT.CALENDLY_CALL, for: .normal)
      self.calendlyButton.backgroundColor = UIColor.clear
      self.calendlyButton.titleLabel?.letterSpacing = 0.5
      self.calendlyButton.isHidden = true
   }
   
   private func setProceedWithDemoButton() {
      self.proceedWithDemoButton.configureNormalButtonWith(title: TEXT.PROCEED_WITH_DEMO)
   }
   
   private func setExploringLabel(){
      self.exploringLabel.attributedText = self.getAttributedTotalText(title: "\(TEXT.AWESOME)!", subTitle: "\(TEXT.EXPLORING)!")
   }
   
   private func setInfoLabel() {
      self.infoLabel.text = TEXT.CREDENTIAL_EMAILD
      self.infoLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
      self.infoLabel.textColor = JOB_STATUS_COLOR.CANCELLED
      self.infoLabel.numberOfLines = 0
      self.infoLabel.letterSpacing = 0.5
   }
   
   //MARK:- Setting AttributedText
   private func getAttributedTotalText(title:String,subTitle:String) -> NSMutableAttributedString {
      let attributedString = NSMutableAttributedString(string: "\(title)\n", attributes: [NSForegroundColorAttributeName:COLOR.SPLASH_TEXT_COLOR, NSFontAttributeName:UIFont(name: FONT.light, size: FONT_SIZE.large)!])
      
      let secondString = NSMutableAttributedString(string: "\(subTitle)", attributes: [NSForegroundColorAttributeName:COLOR.SPLASH_TEXT_COLOR, NSFontAttributeName:UIFont(name: FONT.regular, size: FONT_SIZE.extraLarge)!])
      attributedString.append(secondString)
      return attributedString
   }
   
   //MARK:- IBAction
   @IBAction func proceedWithDemoAction(_ sender: Any) {
      
      Singleton.sharedInstance.checkForLogin(owener: self, startLoaderAnimation: {
         ActivityIndicator.sharedInstance.showActivityIndicator()
      }, stopLoaderAnimation: {
         ActivityIndicator.sharedInstance.hideActivityIndicator()
      })
   }
   
   @IBAction func calendlyAction(_ sender: Any) {
      WebViewVC.openWebViewVC(webUrl: CALENDELY_LINK, title: TEXT.CALENDLY, vc: self)
   }
}
