//
//  ExtraInfo.swift
//  TookanVendor
//
//  Created by CL-Macmini-110 on 9/21/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import CoreLocation
enum Organization: Int {
   case name
   case teamSize
   case address
}

class ExtraInfoVC: UIViewController {
   // MARK:- Outlets
   @IBOutlet var welcomeLabel: UILabel!
   @IBOutlet var infoTableView: UITableView!
   @IBOutlet var saveButton: UIButton!
   @IBOutlet var manageLabel: UILabel!
   @IBOutlet var skipButton: UIButton!
   // MARK:- Propreties
   var currentName = Vendor.current?.firstName ?? ""
   var extraInfoDuringSignup = ExtraInfoDuringSignup()
   var picker = UIPickerView()
   let numberOfTeamsArray = ["", TEXT.ZERO_FIVE, TEXT.SIX_TWENTY, TEXT.TWENTYONE_FIFTY, TEXT.FIFTYONE_HUNDRED, TEXT.HUNDREDONE_TWOHUNDREDFIFTY, TEXT.MORE_TWOFIFTY]
   var indexPathForMap = IndexPath()
   
   // MARK:- View Life Cycle
   override func viewDidLoad() {
      super.viewDidLoad()
      self.setWelcomeLabel()
      self.setSaveButton()
      self.setSkipButton()
      self.setManageLabel()
      self.setTableView()
      self.setPickerView()
   }
   
   //MARK: - Setting Views
   private func setWelcomeLabel() {
      self.welcomeLabel.attributedText = self.getAttributedTotalText(title: "\(TEXT.welcome), \(self.currentName)!", subTitle: TEXT.LET_US_KNOW_YOU)
      self.welcomeLabel.numberOfLines = 0
   }
   
   private func setPickerView(){
      self.picker.delegate = self
      self.picker.dataSource = self
   }
   
   private func setSaveButton() {
      self.saveButton.configureNormalButtonWith(title: "\(TEXT.SAVE) & \(TEXT.CONTINUE)")
      self.saveButton.titleLabel?.letterSpacing = 1.0
   }
   
   private func setSkipButton() {
      self.skipButton.setTitle(TEXT.SKIP, for: .normal)
      self.skipButton.backgroundColor = UIColor.clear
      self.skipButton.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: .normal)
      self.skipButton.titleLabel?.letterSpacing = 0.5
   }
   
   private func setManageLabel() {
      self.manageLabel.text = TEXT.MANAGE_INFO_LATER
      self.manageLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
      self.manageLabel.textColor = JOB_STATUS_COLOR.CANCELLED
      self.manageLabel.letterSpacing = 0.5
   }
   
   private func setTableView() {
      self.infoTableView.register(UINib(nibName: NIB_NAME.extraInfoCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.extraInfoCell)
      self.infoTableView.dataSource = self
      self.infoTableView.delegate = self
      
   }
   
   //MARK: - Setting AttributedText
   private func getAttributedTotalText(title:String,subTitle:String) -> NSMutableAttributedString {
      let attributedString = NSMutableAttributedString(string: "\(title)\n", attributes: [NSForegroundColorAttributeName:JOB_STATUS_COLOR.CANCELLED, NSFontAttributeName:UIFont(name: FONT.light, size: FONT_SIZE.superLarge)!])
      
      let secondString = NSMutableAttributedString(string: "\(subTitle)", attributes: [NSForegroundColorAttributeName:COLOR.SUBTITLE_LIGHT_COLOR, NSFontAttributeName:UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)!])
      attributedString.append(secondString)
      return attributedString
   }
   
   //MARK: - IBAction
   @IBAction func saveAction(_ sender: Any) {
      
      guard self.mandatoryCheck() == true else {
         return
      }
      
      self.extraInfoDuringSignup.sendExtraInfoDataToServer { (isSuccess) in
         if isSuccess == true {
            self.gotoCompletionSignup()
         }
      }
   }
   
   @IBAction func skipAction(_ sender: Any) {
      self.gotoCompletionSignup()
   }
   
   func mandatoryCheck() -> Bool {
      
      guard self.extraInfoDuringSignup.companyName != "" else {
         ErrorView.showWith(message: ERROR_MESSAGE.INVALID_COMPANY, isErrorMessage: true, removed: nil)
         return false
      }
      
      guard self.extraInfoDuringSignup.teamSize != "" else {
         ErrorView.showWith(message: ERROR_MESSAGE.ENTER_COMPANY_SIZE, isErrorMessage: true, removed: nil)
         return false
      }
      
      guard self.extraInfoDuringSignup.companyAddress != "" else {
         ErrorView.showWith(message: ERROR_MESSAGE.INVALID_ADDRESS, isErrorMessage: true, removed: nil)
         return false
      }
      
      return true
   }
   
   func gotoCompletionSignup() {
      if let vc = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.completionSignupVC) as? CompletionSignupVC {
         self.navigationController?.pushViewController(vc, animated: true)
      }
   }
}

//MARK:- Table View Delegate
extension ExtraInfoVC: UITableViewDelegate, UITableViewDataSource{
   
   func numberOfSections(in tableView: UITableView) -> Int {
      return 1
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return 3
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      guard let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.extraInfoCell) as? ExtraInfoCell else {
         fatalError(" ExtraInfoCell not found")
      }
      self.infoTableView.setNeedsUpdateConstraints()
      self.infoTableView.layoutIfNeeded()
      cell.extraInfoTextView.tag = indexPath.row
      cell.delegate = self
      let field = Organization(rawValue: indexPath.row)!
      switch field {
      case .name:
         cell.setCompanyNameField(companyName: self.extraInfoDuringSignup.companyName ?? "")
      case .teamSize:
         cell.setTeamSizeField(teamSize: self.extraInfoDuringSignup.teamSize ?? "")
         cell.extraInfoTextView.inputView = picker
      case .address:
         cell.setCompanyAddressField(companyAddress: self.extraInfoDuringSignup.companyAddress ?? "")
         if self.extraInfoDuringSignup.companyAddress?.isEmpty == true{
            cell.placeHolderLabel.isHidden = false
         } else {
            cell.placeHolderLabel.isHidden = true
         }
      }
      return cell
   }
   
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let field = Organization(rawValue: indexPath.row)!
      switch field {
      case .address:
         let indexPath = IndexPath(row: indexPath.row, section: 0)
         self.view.endEditing(true)
         AddAddressFromMapViewController.pushViewControllerIn(navVC: self.navigationController!, manager: self, savingforType: .Other, locationToEdit: nil)
         self.indexPathForMap = indexPath

      default:
         break
      }
   }
   
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return UITableViewAutomaticDimension
   }
   
   func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
      return 60.0
   }
}

//MARK:- TextViewDelegate
extension ExtraInfoVC: ExtraInfoCellDelegate {
   
   func textViewBeginEditing(_ textView: UITextView) {
      
      let field = Organization(rawValue: textView.tag)!
      switch field {
      case .teamSize:
         textView.inputView = picker
         self.picker.tag = textView.tag
      default:
         break
      }
   }
   
   func textViewChanged(_ textView: UITextView) {

      self.infoTableView.beginUpdates()
      self.infoTableView.endUpdates()
      
      let field = Organization(rawValue: textView.tag)!
      switch field {
      case .name:
         self.extraInfoDuringSignup.companyName = textView.text
      case .teamSize:
         break
      case .address:
         self.extraInfoDuringSignup.companyAddress = textView.text
      }
   }
}

//MARK:- PickerView Delegate
extension ExtraInfoVC: UIPickerViewDelegate,UIPickerViewDataSource {
   
   func numberOfComponents(in pickerView: UIPickerView) -> Int {
      return 1
   }
   
   func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
      return numberOfTeamsArray.count
   }
   
   func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
      return numberOfTeamsArray[row]
   }
   
   func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
      if let cell = infoTableView.cellForRow(at: IndexPath(row: self.picker.tag, section: 0)) as? ExtraInfoCell {
         cell.extraInfoTextView.text = numberOfTeamsArray[row]
         self.extraInfoDuringSignup.teamSize = numberOfTeamsArray[row]
         if cell.extraInfoTextView.text.isEmpty == true {
            cell.placeHolderLabel.isHidden = false
         } else {
            cell.placeHolderLabel.isHidden = true
         }
      }
   }
}


extension ExtraInfoVC: FavouriteLocationManager {
   func getFavIdOf(loctionType: favLocationType) -> String? {
      return nil
   }
   
   func addressSelectedWith(address: String, coordinate: CLLocationCoordinate2D) {
      self.navigationController?.popViewController(animated: true)
      self.extraInfoDuringSignup.companyAddress = address
      self.extraInfoDuringSignup.companyLatitude = coordinate.latitude
      self.extraInfoDuringSignup.companyLongitude = coordinate.longitude
      self.infoTableView.reloadRows(at: [self.indexPathForMap], with: UITableViewRowAnimation.none)
   }
   
   func newAddressSaved() {
   }
   
   func doesFavLocationForTypeAlreadyExist(type: favLocationType) -> Bool {
      return false
   }
   
   func InFlowforSavingLocation() -> Bool {
      return false
   }
}
