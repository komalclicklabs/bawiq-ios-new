//
//  OtpScreenViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 20/03/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class OtpScreenViewController: UIViewController,UITextFieldDelegate {
    
    //MARK: - Properties
    var changeNumberAlert: ChangePhoneNumberView?
    
    var enteredOTP: String {
        var otp = ""
        for otpField in otpTextFields {
            otp += otpField.text!
        }
        return otp
    }
    
    var registerType: RegisterType?
    
    // MARK: - IBoutlets
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var changeNumbeButton: UIButton!
    @IBOutlet weak var resendOtpButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var otpTextFieldStack: UIStackView!
    @IBOutlet weak var changeNumberBtn: UIButton!
    
    @IBOutlet var otpTextFields: [MKTextField]!
    @IBOutlet weak var descriptionLabel: UILabel!
    var otpSent = false
    @IBOutlet weak var titleLabel: UILabel!
    
    var isComingFromLogin = false
    var isComingUpdateProfile = false
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureViews()
    }
    
    private func configureViews() {
        //    view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        //    titleLabel.configureSignInTitleTypeWith(text: TEXT.VERIFY_MOBILE)
        //    descriptionLabel.configureNewUserTypeLabelWith(text: TEXT.OTP_DESCRIPTION)
        //    configureTextFields()
        //    resendOtpButton.configureSmallButtonWith(title: TEXT.RESEND_OTP)
        //    changeNumbeButton.configureSmallButtonWith(title: TEXT.CHANGE_NUMBER)
        //    confirmButton.configureNormalButtonWith(title: TEXT.VERIFY)
        configureBackButton()
    }
    
    private func configureTextFields() {
        otpTextFields.forEach { textField in
           // if isComingFromLogin {
            
            //}
            textField.configureWith(text: "", placeholder: "")
            textField.delegate = self
            textField.font = textField.font?.withSize(30)
        }
    }
    
    private func configureBackButton() {
       // backButton.isHidden = !isComingFromLogin
        backButton.setImage(#imageLiteral(resourceName: "iconBackTitleBar").renderWithAlwaysTemplateMode(), for: .normal)
        backButton.tintColor = COLOR.SPLASH_TEXT_COLOR
    }
    
    // MARK: - TextField Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let nextTextField = self.view.viewWithTag(textField.tag + 1) as? UITextField
        let previousTextField = self.view.viewWithTag(textField.tag - 1) as? UITextField
        
        if backSpaceIsPressedIfReplacementStringIs(string) && previousTextField != nil {
            textField.text = ""
            previousTextField?.becomeFirstResponder()
            return false
        }
        
        if nextTextField == nil && textField.text!.count > 0 {
            textField.resignFirstResponder()
            return false
        }
        
        guard shouldWriteInNext(textField: nextTextField, WithTextInCurrentField: textField.text!, newText: string) else {
            return true
        }
        
        nextTextField!.text = string
        
        if nextTextFieldIsLastTextField(textField: nextTextField!) && isValidOTP() {
            DispatchQueue.main.async {
                textField.resignFirstResponder()
            }
          //  verifyOTP()
            return false
        } else {
            nextTextField?.becomeFirstResponder()
        }
        
        return false
        
    }
    
    private func backSpaceIsPressedIfReplacementStringIs(_ string: String) -> Bool {
        return string == ""
    }
    
    private func shouldWriteInNext(textField: UITextField?, WithTextInCurrentField currentText: String, newText: String) -> Bool {
        return textField != nil && newText != "" && currentText.count >= 1
    }
    
    private func nextTextFieldIsLastTextField(textField: UITextField) -> Bool {
        let lastTextField = otpTextFields.last! as UITextField
        return textField == lastTextField
    }
    
    private func isValidOTP() -> Bool {
        return enteredOTP.count == 4
    }
    
    // MARK: - IBAction
    @IBAction func backButtonAction(_ sender: Any) {
        if isComingUpdateProfile {
            Singleton.sharedInstance.logoutButtonAction()
        } else {
            self.navigationController?.popViewController(animated: true)
        }
        
//        if isComingFromLogin {
//            self.navigationController?.popViewController(animated: true)
//        } else {
//            Singleton.sharedInstance.logoutButtonAction()
//        }
    }
    
    
    @IBAction func changeNumberBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: STORYBOARD_NAME.main, bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "ForgotPassVC") as? ForgotPassVC {
            vc.isForChangeNumber = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func resendOtpAction(_ sender: Any) {
        self.emptyOtpTextFields()
        logEvent(label: "otp_resend")
        
        let param = [
            //      "access_token":"\(Vendor.current!.appAccessToken!)",
            //      "device_token":APIManager.sharedInstance.getDeviceToken(),
            //      "app_version":"\(APP_VERSION)",
            //      "app_device_type":"\(APP_DEVICE_TYPE)",
            "app_access_token":"\(Vendor.current!.appAccessToken!)"
            //      "user_id":"\(Singleton.sharedInstance.formDetailsInfo.user_id!)"
        ]
        print(param)
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.serverCall(apiName: API_NAME.resendOtp, params: param as [String : AnyObject]?, httpMethod: "POST") { (isSuccess, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if isSuccess == true{
                print(response)
                if let message = response["message"] as? String{
                    self.showErrorMessage(error: message, isError: false    )
                }
            }else{
                if let message = response["message"] as? String{
                    self.showErrorMessage(error: message, isError: true)
                }
            }
        }
        
        
        
    }
    
    @IBAction func changeNumberAction(_ sender: Any) {
        
        changeNumberAlert = ChangePhoneNumberView.loadForChangingNumber { [weak self] (countryCode, phoneNumber) in
            guard Singleton.sharedInstance.validatePhoneNumber(phoneNumber: phoneNumber) else {
                self?.showErrorMessage(error: ERROR_MESSAGE.INVALID_PHONE_NUMBER, isError: true)
                return
            }
            
            self?.changePhoneNumber(number: countryCode + " " + phoneNumber)
        }
        
    }
    
    @IBAction func confirmAction(_ sender: Any) {
        self.view.endEditing(true)
//        if showErrorMessageIfOTPIsInvalid() == true {
        if enteredOTP.isEmpty {
            self.showErrorMessage(error: "Please enter OTP", isError: true)
        } else {
            verifyOTP()
        }
        
//        }
    }
    
    private func verifyOTP() {
        let param  = [
            "otpCode": enteredOTP,
            "app_access_token": Vendor.current!.appAccessToken!
        ]
        
        ActivityIndicator.sharedInstance.showActivityIndicator()
        
        APIManager.sharedInstance.serverCall(apiName: API_NAME.verifyOtp, params: param as [String : AnyObject]?, httpMethod: "POST") { (isSuccess, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            print(response)
            
            if isSuccess == true{
                logEvent(label: "otp_verify_success")
                if let message = response["message"] as? String{
                    self.showErrorMessage(error: message, isError: false)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() , execute: {
                        Vendor.current!.is_phone_verified = "1"
                        Vendor.current?.isFirstSignup = true
                        if Vendor.current?.signUpType == SignUptype.individual.rawValue{
                            Singleton.sharedInstance.pushToHomeScreen() {
                            }
                        } else {
                            let storyboard = UIStoryboard(name: STORYBOARD_NAME.main, bundle: nil)
                            if let vc = storyboard.instantiateViewController(withIdentifier: "AddFamilyMemberViewController") as? AddFamilyMemberViewController {
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    })
                }
            }else{
                logEvent(label: "otp_verify_failure")
                if let message = response["message"] as? String{
                    self.showErrorMessage(error: message, isError: true)
                }
            }
        }
        
        
    }
    
    func changePhoneNumber(number:String) {
        logEvent(label: "otp_change_number")
        let param = [
            "access_token":Vendor.current!.accessToken!,
            "new_phone_no":number,
            "phone_no":Vendor.current!.phoneNo,
            "user_id":Vendor.current!.userId!,
            "device_token":APIManager.sharedInstance.getDeviceToken(),
            "app_version":APP_VERSION,
            "app_device_type":APP_DEVICE_TYPE,
            "app_access_token":Vendor.current!.appAccessToken!
            ] as [String : Any]
        print(param)
        changeNumberAlert?.endEditing(true)
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.serverCall(apiName: "change_phone_number", params: param as [String : AnyObject]?, httpMethod: "POST") { [weak self] (isSuccess, Response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if isSuccess == true{
                self?.showErrorMessage(error: Response["message"] as! String, isError: false)
                Vendor.current!.phoneNo = number
                self?.emptyOtpTextFields()
                
                self?.changeNumberAlert?.removeByAnimating()
            }else{
                self?.showErrorMessage(error: Response["message"] as! String, isError: true )
                
            }
        }
    }
    
    // MARK: - Methods
    
    func showErrorMessage(error:String,isError:Bool) {
        ErrorView.showWith(message: error, isErrorMessage: isError, removed: nil)
    }
    
    func showErrorMessageIfOTPIsInvalid() -> Bool {
        guard isValidOTP() else {
            self.showErrorMessage(error: TEXT.otpErrorMessage, isError: true  )
            return false
        }
        return true
    }
    
    private func emptyOtpTextFields() {
        otpTextFields.forEach() { (textField) in
            textField.text = ""
        }
    }
    
}

