//
//  FacebookDetail.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 17/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

struct FacebookDetail {
  var id = ""
  var email = ""
  var firstName = ""
  var lastName = ""
  var fbToken = ""
  var socialSignUpType = 0
    
  var fullName: String {
    return firstName + " " + lastName
  }
  
//  init(json: [String: Any]) {
//    if let email = json["email"] as? String {
//      self.email = email
//    }
//
//    if let name = json["name"] as? String {
//      self.name = name
//    }
//
//    if let id = json["fbId"] as? String {
//      self.id = id
//    }
//  }
    
}

struct InstaGramDetail {
    var instaId = ""
    var instaToken = ""
    var socialSignUpType = 0
}
