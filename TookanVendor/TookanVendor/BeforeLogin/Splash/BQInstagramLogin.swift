//
//  BQInstagramLogin.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 13/06/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

struct InstaAPI {
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    static let INSTAGRAM_CLIENT_ID = "e1acd64c17d948989053aecd0eb265ae"
    static let INSTAGRAM_CLIENTSERCRET = "f4d2eaf1e63d40d18bd3ee75ff23c9a2"
    static let INSTAGRAM_REDIRECT_URI = "https://www.bawiq.com/"
    static let INSTAGRAM_ACCESS_TOKEN =  "https://api.instagram.com/oauth/access_token"
}

class BQInstagramLogin: UIViewController, UIWebViewDelegate {

    var registerType: RegisterType = .individual
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadInstagramURL()
    }
    
    
    func loadInstagramURL() {
        
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=code&DEBUG=True", arguments: [InstaAPI.INSTAGRAM_AUTHURL, InstaAPI.INSTAGRAM_CLIENT_ID, InstaAPI.INSTAGRAM_REDIRECT_URI])
        let urlRequest = URLRequest.init(url: URL.init(string: authURL)!)
        self.webView.delegate = self
        webView.loadRequest(urlRequest)
    
    }
    // MARK: - IBAction
    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request:URLRequest, navigationType: UIWebViewNavigationType) -> Bool{
        return checkRequestForCallbackURL(request: request)
    }
    func checkRequestForCallbackURL(request: URLRequest) -> Bool {
        let requestURLString = (request.url?.absoluteString)! as String
        print(">>>>>>>>>>", requestURLString)
        if requestURLString.hasPrefix(InstaAPI.INSTAGRAM_REDIRECT_URI) {
            let range: Range<String.Index> = requestURLString.range(of: "?code=")!
            handleAuth(authToken: requestURLString.substring(from: range.upperBound))
            return false;
        }
        return true
    }
    
    func handleAuth(authToken: String) {
        print("Instagram authentication token ==", authToken)
        self.loadInstagramURLForToken(token: authToken)
    }
    
    func loadInstagramURLForToken(token: String) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        let authURL = String(format: "client_id=%@&client_secret=%@&redirect_uri=%@&grant_type=authorization_code&code=%@", arguments: [InstaAPI.INSTAGRAM_CLIENT_ID, InstaAPI.INSTAGRAM_CLIENTSERCRET, InstaAPI.INSTAGRAM_REDIRECT_URI, token])
        
        //let url = URL(string: "http://www.thisismylink.com/postName.php")!
        var request = URLRequest(url: URL(string: InstaAPI.INSTAGRAM_ACCESS_TOKEN)!)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        var dicParam = [String: Any]()
        dicParam["client_id"] = InstaAPI.INSTAGRAM_CLIENT_ID
        dicParam["client_secret"] = InstaAPI.INSTAGRAM_CLIENT_ID
        dicParam["redirect_uri"] = InstaAPI.INSTAGRAM_CLIENT_ID
        dicParam["grant_type"] = "authorization_code"
        dicParam["code"] = token
        
        //et postString = "client_id=13&name=Jack"
        request.httpBody = authURL.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(responseString)")
            do {
                if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    print(json)
                    
                    guard let instaGramAccessToken = json["access_token"] as? String else {
                        return
                    }
                    print("Insta access token ===== ", instaGramAccessToken)
                    guard let userDetail = json["user"] as? [String: Any], let userSocialId = userDetail["id"] as? String else {
                        return
                    }
                    print("Insta social id and email ==", userSocialId)
                    self.socialLoginWithParameters(email: "", socialSignupType: 3, instaId: userSocialId, instaToken: instaGramAccessToken)
                }
            } catch let jsonError {
                print(jsonError.localizedDescription)
            }
        }
        task.resume()
    }
    
    
    func socialLoginWithParameters(email: String, socialSignupType: Int, instaId: String, instaToken: String) {
        APIManager.sharedInstance.loginRequest(email, password: "", social_sign_up_type: socialSignupType, social_login_id: instaId, social_login_token: instaToken) { (succeeded, response) in
            print(response)
            DispatchQueue.main.async(execute: { () -> Void in
                DispatchQueue.main.async {
                    ActivityIndicator.sharedInstance.hideActivityIndicator()
                    if succeeded == true {
                        logEvent(label: "user_sign_in_via_instagram")
                        if let status = response["status"] as? Int {
                            switch(status) {
                            case STATUS_CODES.SHOW_DATA:
                                if let data = response["data"] as? [String:Any] {
                                    Vendor.logInWith(data: data)
                                    Singleton.sharedInstance.checkForLogin(owener: self, startLoaderAnimation: {
                                        ActivityIndicator.sharedInstance.showActivityIndicator()
                                    }, stopLoaderAnimation: {
                                        ActivityIndicator.sharedInstance.hideActivityIndicator()
                                    })
                                }
                                break
                            case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                Singleton.sharedInstance.showAlertWithOption(owner: self, title: "", message: response["message"] as! String!, showRight: false, leftButtonAction: {
                                    print("")
                                    self.navigationController?.popToRootViewController(animated: true)
                                }, rightButtonAction: nil, leftButtonTitle: "Ok", rightButtonTitle: "")
                                break

                            default:
                                Singleton.sharedInstance.showAlert(response["message"] as! String!)
                                break
                            }
                        }
                    } else {

                        var instaDetails = InstaGramDetail()
                     //   instaDetails.firstName = "\(firstName)" + "\(lasteName)"
                     //   instaDetails.email = email
                        instaDetails.instaId = instaId
                        instaDetails.instaToken = instaToken
                        instaDetails.socialSignUpType = socialSignupType
                        self.goToRegisterWithInstagram(instaDetails: instaDetails)

                        //self.showErrorMessage(error: response["message"] as! String!)
                    }
                }
            })
        }
    }
    
    
    func goToRegisterWithInstagram(instaDetails: InstaGramDetail) {
       // showHideSelectionView(show: false)
        
        guard let navVC = self.navigationController else {
            print("Navigation controller of Splash not found")
            return
        }
        if let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignupController") as? SignupController {
            vc.isInstagram = true
            vc.instaGramData = instaDetails
            vc.registerType = registerType
            //            vc.pushInWithFacebookData(navVC: navVC, registerType: registerType, facebookDetails: facebookDetails)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        //SignupController.pushIn(navVC: navVC, registerType: registerType)
    }
    
}
