//
//  SplashController.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 15/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
import IQKeyboardManager
import DGActivityIndicatorView
import GoogleSignIn


typealias Completion = ((Bool) -> Void)?
enum ActionfamilyIndividual: Int {
    
    case close = 6
    case family = 7
    case individual = 8
}



class SplashController: UIViewController, SocialActionDelegate, GIDSignInUIDelegate, GIDSignInDelegate {
    
    
    // MARK: - IBOutlets
    @IBOutlet var splashLogoImage: UIImageView!
    @IBOutlet weak var deviceTypeButton: UIButton!
    @IBOutlet weak var splashTextImage: UIImageView!
    @IBOutlet var segmentControl: UISegmentedControl!
    @IBOutlet weak var welcomeBackLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailTextField: MKTextField!
    
    @IBOutlet weak var viewWithEmailField: UIView!
    @IBOutlet weak var viewWithLogos: UIView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet var backButton: UIButton!
//    @IBOutlet var btnGoogle: GIDSignInButton!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var findStoresBtn: UIButton!
    
    //constraints
    @IBOutlet weak var verticalDistanceTitleAndLogo: NSLayoutConstraint!
    @IBOutlet weak var verticleDistanceTitleAndEmail: NSLayoutConstraint!
    
    // MARK: - Properties
    var errorMessageView: ErrorView!
    var socialSectionView: SocialSectionView!
    var activityIndicator: DGActivityIndicatorView!
    //var fromDemo = true
    
    var registerType: RegisterType = .individual
    
    //MARK:- vishalvirodhia: bawiq
    @IBOutlet weak var familyIndividualSelectionView: UIView!
    enum ActionTag: Int {
        case login = 1
        case register = 2
        case facebook = 3
        case gPlus = 4
        case instagram = 5
    }
   
    @IBAction func bawiqCustomAction(_ sender: UIButton) {
        switch ActionTag(rawValue: sender.tag) {
        case .login?:
            goToLogin()
            break
        case .register?:
            showHideSelectionView(show: true)
            break
        case .facebook?:
            facebookEventAction(socialSignupType: 1)
            break
        case .gPlus?:
            googlePlusEventAction()
            break
        case .instagram?:
            instagramEventAction()
            break
        case .none:
            break
        }
    }
    
    @IBAction func findStoreBtnAction(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "isGuestUser")
        Singleton.sharedInstance.pushToHomeScreen() {
            
        }
    }
    
    @IBAction func familyIndividualSelectionActions(_ sender: UIButton) {
        switch ActionfamilyIndividual(rawValue: sender.tag) {
        case .close?:
            showHideSelectionView(show: false)
            break
        case .family?:
            self.registerType = .family
            goToRegister()
            break
        case .individual?:
            self.registerType = .individual
            goToRegister()
            break
        case .none:
            break
        }
        
        
    }
    
    func showHideSelectionView(show: Bool) {
        if show {
            self.view.bringSubview(toFront: familyIndividualSelectionView)
            familyIndividualSelectionView.isHidden = false
            familyIndividualSelectionView.showSmoothly(completion: { (show) in
                
            })
        } else {
            self.view.sendSubview(toBack: familyIndividualSelectionView)
             familyIndividualSelectionView.isHidden = false
            familyIndividualSelectionView.hideSmoothly(completion: { (bool) in
                
            })
        }
    }
    func accessTokenLogin() {
        if self.isAccessTokenAvailable() == .some(true) {
            self.sendAccessTokenLoginHit()
            return
        }
    }
    
    func goToLogin() {
        guard let navVC = self.navigationController else {
            print("Navigation controller of Splash not found")
            return
        }
        LoginController.pushIn(navVC: navVC, withEmail: "")
    }
    func goToRegister() {
        showHideSelectionView(show: false)
        
        guard let navVC = self.navigationController else {
            print("Navigation controller of Splash not found")
            return
        }
        SignupController.pushIn(navVC: navVC, registerType: registerType)
    }
    
    func goToRegisterWithFacebook(facebookDetails: FacebookDetail) {
        showHideSelectionView(show: false)
        
        guard let navVC = self.navigationController else {
            print("Navigation controller of Splash not found")
            return
        }
        if let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SignupController") as? SignupController {
            if facebookDetails.socialSignUpType == 2 {
                vc.isGoogle = true
            } else {
                vc.isFacebook = true
            }
            
            vc.faceBookData = facebookDetails
            vc.registerType = registerType
//            vc.pushInWithFacebookData(navVC: navVC, registerType: registerType, facebookDetails: facebookDetails)
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        //SignupController.pushIn(navVC: navVC, registerType: registerType)
    }
    
    
    // MARK: - View Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.settingDemoApp()
        
       // self.accessTokenLogin()
        fetchAppConfigFromServer()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        self.backButton.setImage(#imageLiteral(resourceName: "iconBackTitleBar").withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: .normal)
        self.backButton.tintColor = UIColor.black
        setHeightConstraintsConstant()
        deviceTypeButton.setTitle("Device type \(APP_DEVICE_TYPE)", for: UIControlState.normal)
        setActivityIndicator()
        configureViews()
     
        deviceTypeButton.isHidden = APP_STORE.value == 1 ? true : false
        Singleton.sharedInstance.enableDisableIQKeyboard(enable: true)
        
        /*----------------- It is only for Testing purpose -----------*/
        switch(UserDefaults.standard.value(forKey: USER_DEFAULT.selectedServer) as! String) {
        case SERVER.dev:
            segmentControl.selectedSegmentIndex = 0
            break
        case SERVER.test:
            segmentControl.selectedSegmentIndex = 1
            break
        case SERVER.live:
            segmentControl.selectedSegmentIndex = 2
            break
        case SERVER.beta:
            segmentControl.selectedSegmentIndex = 3
            break
        default:
            break
        }
        
        if(APP_STORE.value == 1) {
            segmentControl.isHidden = true
        } else { 
            segmentControl.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarStyle = .default
    }
    
    func settingDemoApp() {
        
        if (AppConfiguration.current.isDemoEnabled == true) {
            self.welcomeBackLabel.isHidden = true
            self.backButton.isHidden = false
        } else {
            self.welcomeBackLabel.isHidden = false
            self.backButton.isHidden = true
        }
        
    }
    
    private func setHeightConstraintsConstant() {
        verticalDistanceTitleAndLogo.constant = 85 * heightMultiplierForDifferentDevices
        verticleDistanceTitleAndEmail.constant = 20 * heightMultiplierForDifferentDevices
    }
    
    private func configureViews() {
        self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        configureTitleLabel()
        configureEmailField()
       // configureContinueButton()
        configureWelcomeBackLabel()
    }
    
    private func configureWelcomeBackLabel() {
        let text = Vendor.hasSignedUpBefore() ? TEXT.welcomeBack : TEXT.welcome
        welcomeBackLabel.configureSignInTitleTypeWith(text: text)
        welcomeBackLabel.setAlphaToZero()
    }
    
    private func configureTitleLabel() {
        titleLabel.text = TEXT.enterEmailToContinue
        titleLabel.font = UIFont(name: FONT.ultraLight, size: 24.0)
        titleLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        titleLabel.setAlphaToZero()
    }
    
    private func configureEmailField() {
        emailTextField.configureWith(text: "", placeholder: TEXT.YOUR_EMAIL)
        emailTextField.setAlphaToZero()
        emailTextField.delegate = self
    }
    
    private func configureContinueButton() {
//         continueButton.configureSmallButtonWith(title: TEXT.CONTINUE)
//        continueButton.setImage(#imageLiteral(resourceName: "iconArrow").renderWithAlwaysTemplateMode(), for: UIControlState.normal)
//         continueButton.tintColor = COLOR.THEME_FOREGROUND_COLOR
//         continueButton.setAlphaToZero()
    }
    
    //MARK : -
    @IBAction func segmentValueChanged(_ sender: Any) {
        switch(segmentControl.selectedSegmentIndex) {
        case 0:
            UserDefaults.standard.setValue(SERVER.dev, forKey: USER_DEFAULT.selectedServer)
            break
        case 1:
            UserDefaults.standard.setValue(SERVER.test, forKey: USER_DEFAULT.selectedServer)
            break
        case 2:
            UserDefaults.standard.setValue(SERVER.live, forKey: USER_DEFAULT.selectedServer)
            break
        case 3:
            UserDefaults.standard.setValue(SERVER.beta, forKey: USER_DEFAULT.selectedServer)
            break
        default:
            break
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setActivityIndicator() {
        activityIndicator = DGActivityIndicatorView.init(type: DGActivityIndicatorAnimationType.ballTrianglePath, tintColor: COLOR.THEME_FOREGROUND_COLOR, size: 40.0)
        activityIndicator?.frame = CGRect(x: 0, y: SCREEN_SIZE.height - 150, width: 70, height: 70)
        activityIndicator?.center = CGPoint(x: self.view.center.x, y: (activityIndicator?.center.y)!)
        activityIndicator?.clipsToBounds = true
    }
    
    func sendAccessTokenLoginHit() {
        
        activityIndicator?.startAnimating()
        if activityIndicator != nil {
            self.view.addSubview(activityIndicator!)
        }
        APIManager.sharedInstance.loginRequestWithAccessToken({ (isSuccess, response) in
            if isSuccess == true {
                Singleton.sharedInstance.isSignedIn = true
                print(response)
                if let status = response["status"] as? Int {
                    switch(status) {
                    case STATUS_CODES.SHOW_DATA:
                        DispatchQueue.main.async(execute: { () -> Void in
                            if let data = response["data"] as? [String:Any] {
                                Vendor.logInWith(data: data)
                                Singleton.sharedInstance.checkForLogin(owener: self, startLoaderAnimation: {
                                    self.activityIndicator?.startAnimating()
                                    self.view.addSubview(self.activityIndicator!)
                                }, stopLoaderAnimation: {
                                    self.activityIndicator?.stopAnimating()
                                })
                            }
                        })
                        
                    default:
                        break
                    }
                }
                self.activityIndicator?.stopAnimating()
            } else {
                self.activityIndicator?.stopAnimating()
                Singleton.sharedInstance.enableDisableIQKeyboard(enable: false)
                if (response["status"] as? Int) != nil {
                    self.showErrorMessage(error: response["message"] as! String)
                }
                else{
                    Singleton.sharedInstance.showAlertWithOption(owner: self, message: response["message"] as! String, leftButtonAction: {
                        self.sendAccessTokenLoginHit()
                    }, leftButtonTitle: "Retry")
                }
                
            }
        })
    }
    
    //MARK: -
    func getDeviceToken() -> String {
        if let deviceToken = UserDefaults.standard.value(forKey: USER_DEFAULT.deviceToken) {
            return deviceToken as! String
        }
        return "no device token"
    }
    
    // MARK: - IBAction
    @IBAction func continueButtonPressed() {
       //Push To Home page without login ask for login before craeting any order booking.
        //After Login
//        if let vc = UIStoryboard(name: "AfterLogin", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeController") as? HomeVC {
//            self.navigationController?.pushViewController(vc, animated: true)
//        }
        
    }
    
    //MARK: - ERROR MESSAGE
    func showErrorMessage(error:String) {
        ErrorView.showWith(message: error, removed: nil)
    }
    
    // MARK: - Animations
    private func startViewSetupAnimations() {
        // vishalvirodhia bawiq
        return
        self.translatingAutoresizingMaskIntoConstraintsForSplashLogo()
        
        splashTextImage.hideSmoothly(completion: nil)
        
        animateAppSplashLogo { [weak self] completed in
            guard completed else {
                return
            }
            self?.setupViewAccordingToConfig()
        }
        
    }
    
    private func translatingAutoresizingMaskIntoConstraintsForSplashLogo() {
        viewWithLogos.translatesAutoresizingMaskIntoConstraints = true
        self.splashLogoImage.translatesAutoresizingMaskIntoConstraints = true
        self.splashTextImage.translatesAutoresizingMaskIntoConstraints = true
    }
    
    private func animateAppSplashLogo(completion: Completion) {
        
        let minYForSplashLogo = caculateTopMarginForSplashLogo()
        let newSize = calculateNewSizeOfViewWithLogos()
        let newCenterXForViewWithLogos = SCREEN_SIZE.width/2
        let newCenterXForSplashLogoImage = SCREEN_SIZE.width/2
        var timeDuration = 0.0
        if (AppConfiguration.current.isDemoEnabled == false){
            timeDuration = 1.0
        }
        
        UIView.animate(withDuration: timeDuration, animations: { [weak self] in
            self?.viewWithLogos.frame.origin.y = minYForSplashLogo
                        self?.splashLogoImage.frame.size = newSize
                        self?.viewWithLogos.frame.size = newSize
            self?.viewWithLogos.center.x = newCenterXForViewWithLogos
            self?.splashLogoImage.center.x = newCenterXForSplashLogoImage
            }, completion: completion)
    }
    
    private func caculateTopMarginForSplashLogo() -> CGFloat {
        return 90 * heightMultiplierForDifferentDevices
    }
    
    private func calculateNewSizeOfViewWithLogos() -> CGSize {
        let newWidth = splashLogoImage.frame.width //* 0.7
        let newHeight = splashLogoImage.frame.height //* 0.7
        
        return CGSize(width: newWidth, height: newHeight)
    }
    
    private func setupViewAccordingToConfig() {
        
        welcomeBackLabel.showSmoothly(completion: nil)
        titleLabel.showSmoothly(completion: nil)
        continueButton.showSmoothly(completion: nil)
        
        if shouldShowEmailOnBoardingSection() {
            self.loadEmailOnBoardingSection()
        }
        if shouldShowSocialOnboardingSection() {
            self.loadSocialOnboardingSection()
        }
    }
    
    // MARK: - Configuration
    func fetchAppConfigFromServer() {
        
        if (AppConfiguration.current.isDemoEnabled == false) {
           // startAnimatingIndicator()
           // self.activityIndicator.startAnimating()
            AppConfiguration.fetchFromServer { [weak self] (success, error) in
                //self?.activityIndicator?.removeFromSuperview()
             //   self?.activityIndicator.stopAnimating()
                guard success else {
                    UIAlertController.showWith(message: error?.localizedDescription ?? ERROR_MESSAGE.SERVER_NOT_RESPONDING, title: nil, buttonTitle: TEXT.RETRY) {
                        self?.fetchAppConfigFromServer()
                    }
                    return
                }
                
                self?.handleAppVersioningCheck {
                    
                    if self?.isAccessTokenAvailable() == .some(true) {
                        self?.sendAccessTokenLoginHit()
                        return
                    }
                    self?.startViewSetupAnimations()
                }
            }
        } else {
            self.handleAppVersioningCheck {
                self.startViewSetupAnimations()
            }
        }
    }
    
    private func isAccessTokenAvailable() -> Bool {
        return UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) != nil
    }
    
    private func shouldShowEmailOnBoardingSection() -> Bool {
        return AppConfiguration.current.isLoginViaEmailRequired == "1"
    }
    
    private func shouldShowSocialOnboardingSection() -> Bool {
        return AppConfiguration.current.isSocialLoginAvailable
    }
    
    // MARK: - OnBoarding Sections
    private func loadEmailOnBoardingSection() {
        emailTextField.showSmoothly(completion: nil)
    }
    
    private func loadSocialOnboardingSection() {
        
        let frame = calculateFrameForSocialSectionView()
        socialSectionView = SocialSectionView.loadWith(frame: frame)
        
        self.socialSectionView.delegate = self
        self.view.addSubview(socialSectionView)
        socialSectionView.showSmoothly(completion: nil)
    }
    
    private func calculateFrameForSocialSectionView() -> CGRect {
        
        let width = SCREEN_SIZE.width
        let height: CGFloat = 200 * heightMultiplierForDifferentDevices
        let originX: CGFloat = 0
        var originY: CGFloat = 0
        
        if shouldShowEmailOnBoardingSection() {
            originY = SCREEN_SIZE.height - height
        } else {
            originY = viewWithEmailField.frame.maxY
        }
        
        return CGRect(x: originX, y: originY, width: width, height: height)
    }
    
    //MARK: - AppVersioning
    func handleAppVersioningCheck(completion: @escaping () -> Void) {
        
        
        let updateType = AppConfiguration.current.getAppUpdateType()
        
        guard updateType != .none else {
            completion()
            return
        }
        
        let appVersioningView = loadAppVersioningView(cancelAction: completion)
        set(appVersioningView: appVersioningView, forUpdateType: updateType)
        
        appVersioningView.addViewToWindowWithAnimation()
    }
    
    private func loadAppVersioningView(cancelAction: @escaping () -> Void) -> ChangePhoneNumberView {
        let intialTexts = ChangePhoneNumberView.PopupParams()
        
        let appVersionView = ChangePhoneNumberView.getWithHeading(texts: intialTexts, successAction: {
            guard let appURl = URL(string: AppConfiguration.current.appUrl) else {
                return
            }
            UIApplication.shared.openURL(appURl)}, cancelAction: cancelAction)
        
        return appVersionView
    }
    
    private func set(appVersioningView: ChangePhoneNumberView, forUpdateType updateType: AppConfiguration.AppUpdate) {
        let texts = ChangePhoneNumberView.PopupParams(heading: updateType.getUpdateMessage(), textFieldPlaceholder: "", textFieldtext: "", removeAlertButtonTitle: TEXT.CANCEL, saveButtonAlertTitle: TEXT.APP_UPDATE_TEXT)
        
        appVersioningView.setViewWithNew(texts: texts)
        
        switch updateType {
        case .forced:
            appVersioningView.cancelButton.isHidden = true
        default:
            break
        }
    }
    
    //MARK: - Common Methods
    func startAnimatingIndicator() {
        self.activityIndicator?.startAnimating()
        //self.view.addSubview(self.activityIndicator!)
    }
    
    //MARK: - SOCIAL SECTION DELEGATE / Methods
    func facebookEventAction(socialSignupType: Int) {
        print("facebook")
        APIManager.sharedInstance.CommonFunctionForFaceBookLogin(viewControler: self, fbDataOnSucess: { (firstName, lastName, email, fbId, fbToken) in
            
            ActivityIndicator.sharedInstance.showActivityIndicator()
            self.socialLoginWithParameters(firstName: firstName, lasteName: lastName, email: email, password: "", socialSignupType: 1, fbId: fbId, fbToken: fbToken)
            
        }) { (error) in
            print(error)
        }
        
    }
    
    
    func socialLoginWithParameters(firstName: String, lasteName: String, email: String, password: String, socialSignupType: Int, fbId: String, fbToken: String) {
        APIManager.sharedInstance.loginRequest(email, password: "", social_sign_up_type: socialSignupType, social_login_id: fbId, social_login_token: fbToken) { (succeeded, response) in
            print(response)
            DispatchQueue.main.async(execute: { () -> Void in
                DispatchQueue.main.async {
                    ActivityIndicator.sharedInstance.hideActivityIndicator()
                    if succeeded == true {
                        logEvent(label: "user_sign_in_via_facebook")
                        if let status = response["status"] as? Int {
                            switch(status) {
                            case STATUS_CODES.SHOW_DATA:
                                if let data = response["data"] as? [String:Any] {
                                    Vendor.logInWith(data: data)
                                    Singleton.sharedInstance.checkForLogin(owener: self, startLoaderAnimation: {
                                        ActivityIndicator.sharedInstance.showActivityIndicator()
                                    }, stopLoaderAnimation: {
                                        ActivityIndicator.sharedInstance.hideActivityIndicator()
                                    })
                                }
                                break
                            case STATUS_CODES.INVALID_ACCESS_TOKEN:
                                Singleton.sharedInstance.showAlertWithOption(owner: self, title: "", message: response["message"] as! String!, showRight: false, leftButtonAction: {
                                    print("")
                                    self.navigationController?.popToRootViewController(animated: true)
                                }, rightButtonAction: nil, leftButtonTitle: "Ok", rightButtonTitle: "")
                                break
                                
                            default:
                                Singleton.sharedInstance.showAlert(response["message"] as! String!)
                                break
                            }
                        }
                    } else {
                        
                        var facebookDetails = FacebookDetail()
                        facebookDetails.firstName = "\(firstName)" + "\(lasteName)"
                        facebookDetails.email = email
                        facebookDetails.id = fbId
                        facebookDetails.fbToken = fbToken
                        facebookDetails.socialSignUpType = socialSignupType
                        self.goToRegisterWithFacebook(facebookDetails: facebookDetails)
                        
                        //self.showErrorMessage(error: response["message"] as! String!)
                    }
                }
            })
        }
    }
    
    
    
    
    func googlePlusEventAction() {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
            // Perform any operations on signed in user here.
            print(user.profile.name)
            if let accessToken = GIDSignIn.sharedInstance().currentUser.authentication.accessToken {
                ActivityIndicator.sharedInstance.showActivityIndicator()
                self.socialLoginWithParameters(firstName: user.profile.name, lasteName: "", email: user.profile.email, password: "", socialSignupType: 2, fbId: user.userID, fbToken: user.authentication.idToken)
            }
        } else {
            print("\(error.localizedDescription)")
        }
    }

    // Implement these methods only if the GIDSignInUIDelegate is not a subclass of
    // UIViewController.
    
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    @nonobjc func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        activityIndicator.stopAnimating()
//        myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    @nonobjc func signIn(signIn: GIDSignIn!,
                presentViewController viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    @nonobjc func signIn(signIn: GIDSignIn!,
                dismissViewController viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func instagramEventAction() {
        
        if let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQInstagramLogin") as? BQInstagramLogin {
            self.navigationController?.pushViewController(vc, animated: true)
        }
     //
    }
    
    @IBAction func changeDeviceType(_ sender: UIButton) {
        var customTipView : ChangePhoneNumberView?
        customTipView  =  ChangePhoneNumberView.loadWithSingleTextField(texts: ChangePhoneNumberView.PopupParams.init(heading: "", textFieldPlaceholder: "Device Type", textFieldtext: "", removeAlertButtonTitle: "Cancel", saveButtonAlertTitle: "Add"), keyboardType: .decimalPad, newTextSaved:  { (DeviceType) in
            UserDefaults.standard.set("\(DeviceType)", forKey: "deviceType")
            self.deviceTypeButton.setTitle("Device type \(APP_DEVICE_TYPE)", for: UIControlState.normal)
            customTipView?.removeByAnimating()
        })
    }
    
}

extension SplashController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

