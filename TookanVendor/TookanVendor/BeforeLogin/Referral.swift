//
//  Referral.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 29/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

struct Referral {
  var generationType = ""
  var shareMessage = ""
  var fbMessage = ""
  var description = ""
  var details = ""
  var image: String?
  
  init() {
  }
  
  init(fromJson json: [String: Any]) {
    if let generationType = json["referral_generation_type"] {
      self.generationType = "\(generationType)"
    }
    
    if let shareMessage = json["referral_share_message"] as? String {
      self.shareMessage = shareMessage
    }
    
    if let fbMessage = json["referral_fb_message"] as? String {
      self.fbMessage = fbMessage
    }
    
    if let description = json["referral_description"] as? String {
      self.description = description
    }
    
    if let details = json["referral_details"] as? String {
      self.details = details
    }
    
    if let image = json["referral_image"] as? String {
      self.image = image
    }
  }
}
