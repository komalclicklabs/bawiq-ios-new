//
//  ForgotPassVC.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 11/23/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class ForgotPassVC: UIViewController, NavigationDelegate {
    func setUserCurrentLocation() {
        
    }
    
    
    //MARK: - Properties
    var navigationBar:NavigationView!
    var titleView:TitleView!
    let topMarginOfTextField:CGFloat =  118 * heightMultiplierForDifferentDevices
    let marginForgotPasswordAndDescLabel: CGFloat = 10 * heightMultiplierForDifferentDevices
    let marginDescLabelAndEmailField: CGFloat = 93 * heightMultiplierForDifferentDevices
    let marginMailFieldAndSendLinkButton = 44 * heightMultiplierForDifferentDevices
    var isForChangeNumber = false
    
    // MARK: - IBOutlets
    @IBOutlet weak var enterEmailAddressLabel: UILabel!
    @IBOutlet weak var sendLinkButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailField: VSTextField!
    @IBOutlet weak var resentBtn: UIButton!
    
    @IBOutlet weak var distanceTopForgotPassword: NSLayoutConstraint!
    
    
    @IBAction func resendBtnAction(_ sender: Any) {
        self.checkValidation()
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureViews()
        resentBtn.isUserInteractionEnabled = true
        
    }
    
    //MARK: - ConfigureViews
    private func configureViews() {
        self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
       // self.setNavigationBar()
        setEmailField()
          setDescLabel()
        seTitleLabel()
        if isForChangeNumber {
            resentBtn.isHidden = true
        }
        // setSendLinkButton()
    }
    
    private func setEmailField() {
        emailField.delegate = self
//        emailField.placeHolderColor = .white
        emailField.textColor = .white
        if isForChangeNumber {
            emailField.delegate = self
//            emailField.configureWith(text: "", placeholder: "Phone Number")
            emailField.setFormatting("XXX-XXX-XXXX", replacementChar: "X")
            emailField.attributedPlaceholder = NSAttributedString(string: "Phone Number e.g. (052-993-3645)",
                                                                  attributes: [NSForegroundColorAttributeName: UIColor.white])
            
        } else {
//             emailField.configureWith(text: "", placeholder: "Email / Phone Number")
            emailField.attributedPlaceholder = NSAttributedString(string: "Email / Phone Number",
                                                                  attributes: [NSForegroundColorAttributeName: UIColor.white])
        }
        // emailField.configureWith(text: "", placeholder: TEXT.YOUR_EMAIL)
    }
    
    private func setDescLabel() {
        if isForChangeNumber {
            enterEmailAddressLabel.text = "Enter your new number"
        } else {
            enterEmailAddressLabel.text = "Enter your email/phone number to recover your password"
//            enterEmailAddressLabel.configureNewUserTypeLabelWith(text: TEXT.FORGOT_ENTER_EMAIL_FOR_LINK_LABEL)
        }
        
    }
    
    private func seTitleLabel() {
        if isForChangeNumber {
            titleLabel.configureSignInTitleTypeWith(text: "Change Number")
        } else {
            titleLabel.configureSignInTitleTypeWith(text: TEXT.FORGOT_PASSWORD)
        }
        
    }
    
    private func setSendLinkButton() {
        sendLinkButton.configureNormalButtonWith(title: TEXT.SEND_LINK_BUTTON)
    }
    
    func setSendLinkButtonButton() {
        self.sendLinkButton.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
        self.sendLinkButton.setShadow()
        self.sendLinkButton.setAttributedTitle(Singleton.sharedInstance.setAttributeTitleWithOrWithoutArrow(title: TEXT.SEND_LINK_BUTTON, isArrow: false, yourImage: IMAGE.iconSigninSigninpage), for: .normal)
    }
    
    func phoneNumberContainsCharacterOtherThanNumbers() -> Bool {
        let phoneNo = emailField.text!
        
        return phoneNo.count != phoneNo.stripOutUnwantedCharacters(charactersYouWant: "0123456789+-").count
    }
    
    //MARK: - CHECK VALIDATIONS
    func checkValidation() {
        var email = (self.emailField.text?.trimText)!
        var contact = email
        if !Singleton.sharedInstance.validateEmail(email) {
            email = ""
        }
        if !Singleton.sharedInstance.validatePhoneNumber(phoneNumber: contact) && phoneNumberContainsCharacterOtherThanNumbers() == true {
            contact = ""
        }
        if (email == "") && (contact == "") {
            if isForChangeNumber {
                self.showErrorMessage(error: "Please enter mobile number.", isError: true)
            } else {
                self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PHONE_Email, isError: true)
            }
            
            return
        }
        var forgotParams = ["userType":"0"]
        if email != "" {
            forgotParams["email"] = email
        }
        
        if contact != "" {
            forgotParams["phone_no"] = contact
        }
        self.serverRequest(yourParams: forgotParams as [String : AnyObject])
    }
    
    //MARK: - SEND LINK ACTION
    @IBAction func sendLinkAction(_ sender: UIButton) {
        if isForChangeNumber {
            var params = [String: Any]()
//            params["change_phone"] = self.emailField.text
            params["phone_no"] = self.emailField.text
            params["app_access_token"] = Vendor.current!.appAccessToken!
            params["user_type"] = "0"
            self.changeNumber(yourParams: params as [String : AnyObject])
        } else if otpSent {
            backAction()
        } else {
            emailField.resignFirstResponder()
            checkValidation()
        }
    }
    
    //MARK: NAVIGATION BAR
    func setNavigationBar() {
        let navigationBar = NavigationView.getNibFile(leftButtonAction: {
            _ = self.navigationController?.popViewController(animated: true)
        }, rightButtonAction: nil)
        navigationBar.backgroundColor = UIColor.clear
        navigationBar.bottomLine.isHidden = true
        navigationBar.backButton.tintColor = COLOR.SPLASH_TEXT_COLOR
        navigationBar.titleLabel.text = TEXT.FORGOT_PASSWORD
        self.view.addSubview(navigationBar)
    }
    
    //MARK: NAVIGATION DELEGATE METHODS
    func backAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK: SERVER REQUEST
    func serverRequest(yourParams:[String:AnyObject]) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.serverCall(apiName: API_NAME.forgotPass, params: yourParams, httpMethod: HTTP_METHOD.POST) { (isSuccess, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSuccess {
                    self.emailField.text = ""
                    // Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
                    self.showErrorMessage(error: response["message"] as? String ?? "", isError: false)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
                        self.showSuccess()
                        //                        _ = self.navigationController?.popViewController(animated: true)
                    })
                }else{
                    self.showErrorMessage(error: response["message"] as? String ?? "",isError: true)
                }
            }
        }
    }
    
    func changeNumber(yourParams:[String:AnyObject]) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.serverCall(apiName: API_NAME.changePhoneNumber, params: yourParams, httpMethod: HTTP_METHOD.POST) { (isSuccess, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSuccess {
                    if let data = response["data"] as? [[String: Any]] {
                        Vendor.logInWith(data: data[0])
                        Vendor.current?.isFirstSignup = true
                        //                                if AppConfiguration.current.isDemoEnabled == true{
                        //                                    self.checkForPendingTask()
                        //                                } else {
                        
                        //                                }
                        self.onSuccessFullSignUp()
                    }
                    self.emailField.text = ""
                    // Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
                    self.showErrorMessage(error: response["message"] as? String ?? "", isError: false)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1, execute: {
//                        self.showSuccess()
                        //                        _ = self.navigationController?.popViewController(animated: true)
                    })
                }else{
                    self.showErrorMessage(error: response["message"] as? String ?? "",isError: true)
                }
            }
        }
    }
    
    func onSuccessFullSignUp(){
        
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.otpScreen) as? OtpScreenViewController {
            self.navigationController?.pushViewController(vc, animated: true   )
        }
        //        Singleton.sharedInstance.checkForLogin(owener: self, startLoaderAnimation: {
        //            ActivityIndicator.sharedInstance.showActivityIndicator()
        //        }, stopLoaderAnimation: {
        //            ActivityIndicator.sharedInstance.hideActivityIndicator()
        //        })
        //        self.sendReferralCodeToServerIfPresent()
    }
    
    
    @IBOutlet weak var emailTextFieldheight: NSLayoutConstraint!
    var otpSent = false
    
    func showSuccess() {
        emailTextFieldheight.constant = 0
        enterEmailAddressLabel.text = "Mobile Number has been updated successfully."
        otpSent = true
        sendLinkButton.setTitle("OK", for: .normal)
        
    }
    
    
    
    //MARK: ERROR MESSAGE
    func showErrorMessage(error:String,isError:Bool) {
        ErrorView.showWith(message: error, isErrorMessage: isError, removed: nil)
    }
    
}

extension ForgotPassVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case emailField:
            if self.isForChangeNumber { 
                if textField.text == "" {
                    emailField.text = "0" + string
                    return false
                }
                return true
            }
            return true
        default:
            break
        }
        return true
    }
    
}
