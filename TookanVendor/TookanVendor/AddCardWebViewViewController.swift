//
//  AddCardWebViewViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 28/02/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class AddCardWebViewViewController: UIViewController,UIWebViewDelegate {



   // MARK: - Proeprties
    var completionHandler : (()->Void)?
   
   // MARK: - IBOutlets
   @IBOutlet weak var webView: UIWebView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
      
      loadRequestForAddCardScreen()
    }
   
   func loadRequestForAddCardScreen() {
      let urlToLoad = getAddCardPageUrl()
      
      let requestObj = URLRequest(url: urlToLoad)
      webView.loadRequest(requestObj)
   }
   
   func getAddCardPageUrl() -> URL {
      let serverUrl = HTTPClient.baseUrl
      
      let appAccessToken = Vendor.current!.appAccessToken!
      let customerEmail = Vendor.current!.email!
      let pageAddressWithQueryString = "add_cards_view?access_token=\(appAccessToken)&app_device_type=\(APP_DEVICE_TYPE)&client_email=\(customerEmail)"
      
      let pageUrlString = serverUrl + pageAddressWithQueryString
      
      return URL(string: pageUrlString)!
   }
   
   // MARK: - Web View Delegate
   func webViewDidStartLoad(_ webView: UIWebView) {
      UIApplication.shared.isNetworkActivityIndicatorVisible = true
   }
   
   func webViewDidFinishLoad(_ webView: UIWebView) {
      UIApplication.shared.isNetworkActivityIndicatorVisible = false
   }
   
    
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if request.url?.absoluteString.contains("error") == true {
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: { [weak self] in
                self?.dismissANdStopActivityIndicator()
            })
            return true
        }else if request.url?.absoluteString.contains("success") == true{
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5, execute: {
               UIApplication.shared.isNetworkActivityIndicatorVisible = false
                 self.dismiss(animated: true, completion: { 
                    if self.completionHandler != nil{
                        self.completionHandler!()
                    }
                 })
            })
            
            return true
        }
        return true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print(error)
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
      dismissANdStopActivityIndicator()
    }
   
   func dismissANdStopActivityIndicator() {
      UIApplication.shared.isNetworkActivityIndicatorVisible = false
      self.dismiss(animated: true, completion: nil)
   }
   
    
   
}
