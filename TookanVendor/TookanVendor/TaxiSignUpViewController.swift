//
//  TaxiSignUpViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 15/03/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class TaxiSignUpViewController: UIViewController,CustomFieldDelegate {

    var emailField:CustomTextField!
    var passwordField:CustomTextField!
    var nameField:CustomTextField!
    var contactField:CustomTextField!
    
    @IBOutlet weak var upperView: UIView!
    @IBOutlet weak var nameTextFieldView: UIView!
    @IBOutlet weak var numberTextFieldView: UIView!
    @IBOutlet weak var emailTextFieldView: UIView!
    @IBOutlet weak var passwordTextFieldView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitleView()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        setContactField()
        setEmailField()
        setNameField()
        setPasswordField()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setEmailField() {
        self.emailField = UINib(nibName: NIB_NAME.customTextField, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomTextField
        self.emailField.delegate = self
        self.emailField.textField.becomeFirstResponder()
        self.emailField.fieldType = FIELD_TYPE.email
        self.emailField.frame = emailTextFieldView.frame
        self.upperView.addSubview(self.emailField)
        self.upperView.bringSubview(toFront: emailField)
        self.emailField.alpha = 1
        //self.emailField.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
        self.emailField.setImageForDifferentStates(inactive: IMAGE.iconEmailInActive, placeholderText: TEXT.YOUR_EMAIL, isPasswordField: false, unlock: nil)
        self.emailField.textField.keyboardType = UIKeyboardType.emailAddress
        self.emailField.textField.returnKeyType = UIReturnKeyType.next
        
    }
    
    func setPasswordField() {
        self.passwordField = UINib(nibName: NIB_NAME.customTextField, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomTextField
        self.passwordField.delegate = self
        self.passwordField.fieldType = FIELD_TYPE.password
        self.passwordField.frame = passwordTextFieldView.frame
        self.upperView.addSubview(self.passwordField)
        self.passwordField.alpha = 1
        
        self.passwordField.setImageForDifferentStates(inactive: IMAGE.iconPasswordInactive, placeholderText: TEXT.YOUR_PASSWORD, isPasswordField: true, unlock: IMAGE.iconPasswordActiveShown)
        self.passwordField.textField.keyboardType = UIKeyboardType.default
        self.passwordField.textField.returnKeyType = UIReturnKeyType.next
    }
    
    func setNameField() {
        self.nameField = UINib(nibName: NIB_NAME.customTextField, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomTextField
        self.nameField.delegate = self
        self.nameField.fieldType = FIELD_TYPE.name
        self.nameField.frame = nameTextFieldView.frame
        self.upperView.addSubview(self.nameField)
        self.nameField.alpha = 1
       // self.nameField.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
        self.nameField.setImageForDifferentStates(inactive: IMAGE.iconNameInactive, placeholderText: TEXT.YOUR_NAME, isPasswordField: false, unlock: nil)
        self.nameField.textField.keyboardType = UIKeyboardType.default
        self.nameField.textField.returnKeyType = UIReturnKeyType.next
        self.nameField.textField.autocapitalizationType = .words
    }
    
    func setContactField() {
        self.contactField = UINib(nibName: NIB_NAME.customTextField, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! CustomTextField
        self.contactField.delegate = self
        self.contactField.fieldType = FIELD_TYPE.contact
        self.contactField.frame = numberTextFieldView.frame
        self.upperView.addSubview(self.contactField)
        self.contactField.alpha = 1
        //self.contactField.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
        self.contactField.setImageForDifferentStates(inactive: IMAGE.iconContactUnfilled, placeholderText: TEXT.YOUR_CONTACT, isPasswordField: false, unlock: nil)
        self.contactField.textField.keyboardType = UIKeyboardType.numberPad
        self.contactField.textField.returnKeyType = UIReturnKeyType.done
        self.contactField.setViewForPhoneOrNot()
       // self.contactField.countryCodeButton.addTarget(self, action: #selector(self.showCountryCode), for: UIControlEvents.touchUpInside)
    }
    
    func customFieldShouldReturn(fieldType: FIELD_TYPE) {
        print(fieldType)
    }
    
    
    func setTitleView() {
        let titleView = UINib(nibName: NIB_NAME.titleView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TitleView
        titleView.frame = CGRect(x: 0, y: HEIGHT.navigationHeight, width: self.view.frame.width, height: HEIGHT.titleHeight)
        titleView.backgroundColor = UIColor.clear
        self.view.addSubview(titleView)
        titleView.setTitle(title: TEXT.SIGN_UP+" "+TEXT.WITH, boldTitle: APP_NAME)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
