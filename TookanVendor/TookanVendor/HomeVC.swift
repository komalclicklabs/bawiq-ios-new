//
//  HomeVC.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 11/23/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import QuartzCore


class HomeVC: UIViewController, NavigationDelegate, HomeView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, locationManagerCustomDelegate {
    
    @IBOutlet weak var continueShopiingTxt: UILabel!
    @IBOutlet weak var sorryTxt: UILabel!
    @IBOutlet weak var notAvailableView: UIView!
    @IBOutlet weak var bannerPageController: UIPageControl!
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    @IBOutlet weak var productCollectionView: UICollectionView!
    @IBOutlet weak var cartBottomView: UIView!
    @IBOutlet weak var collectionHeightConstraint: NSLayoutConstraint!
    
    var presenter: HomePresenter!
    var navigationBar:NavigationView!
    var locationManager = CustomLocationManager.shared
    let colectionCellWidthConstant: CGFloat = 10
    var productsTotalPrice: Float = 0
    var isSearchingEnable = false
    var timer: Timer?
    var bannerScrollCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.bannerPageController.currentPage = 0
        notAvailableView.isHidden = true
        continueShopiingTxt.isHidden = true
        sorryTxt.isHidden = true
        self.setUpPresenter()
        getAllBannerData()
        self.initializeView()
        
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.6) {[weak self] in
//            self?.locationManager.delegate = self
//            self?.locationManager.startUpdatingLocation()
//        }
        
        self.locationManager.delegate = self
        self.locationManager.startUpdatingLocation()
        
        NotificationCenter.default.addObserver(self, selector: #selector(setUserLocationFromMap), name: NSNotification.Name(rawValue: "AddUserLocation"), object: nil)
        
        if let email = Vendor.current?.email {
            BQBookingModel.shared.email = email
        }
        
        if let phone = Vendor.current?.phoneNo {
            BQBookingModel.shared.phone = phone
        }
        BQBookingModel.shared.userName = Vendor.current?.username
        
    }
    
    func getAllBannerData() {
        self.presenter.getAllBanners()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            self.bannerCollectionView.reloadData()
        })
    }
    
    func setUserLocationFromMap(notification: NSNotification) {
       print("setUserLocationFromMap")
        if let userAddress = notification.object as? GMSReverseGeocodeResponse {
            // do something with your object
            print(userAddress)
            
            if (userAddress.results()?.count)! > 0{
                if userAddress.results()?[0].lines?.count != 0 {
                    if let lines = userAddress.firstResult()?.lines {
                        
                        //Deepak App is getting crashed after getting any nil value in address
                        if let thoroughfare = userAddress.results()?[0].thoroughfare {
                            BQBookingModel.shared.addressLine1 = thoroughfare
                        }
                        
                        if let postalCode = userAddress.results()?[0].postalCode {
                            BQBookingModel.shared.addressLine3 = postalCode
                        }
                        
                        
                        var middleAddress = ""
                        if let subLocality = userAddress.results()?[0].subLocality {
                            middleAddress = subLocality
                        }
                        if let locality = userAddress.results()?[0].locality {
                            if middleAddress.length != 0 {
                                middleAddress = middleAddress + ", " + locality
                            } else {
                                middleAddress = locality
                            }
                        }
                        if let country = userAddress.results()?[0].country {
                            if middleAddress.length != 0 {
                                middleAddress = middleAddress + ", " + country
                            } else {
                                middleAddress = country
                            }
                        }
                        BQBookingModel.shared.addressLine2 = middleAddress
                        
                        BQBookingModel.shared.latitude = userAddress.results()?[0].coordinate.latitude
                        BQBookingModel.shared.longitude = userAddress.results()?[0].coordinate.longitude
                        self.navigationBar.titleLabel.text = "Delivering to" + "\n" + lines.joined(separator: " ")
//                        self.navigationBar.titleLabel.text = "Current Location" + "\n" + lines.joined(separator: " ")
                        
                    }
                }
            }
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            self.timer?.invalidate()
            self.startTimer()
        })
        self.presenter.getCategories(limit: 50, skip: 0)
        print("viewWillAppear: A")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer?.invalidate()
    }
    
    func didUpdateLocation(withCoordinates: CLLocationCoordinate2D) {
        
        locationManager.reverseGeocodeCoordinateWithbrokenAddress(coordinate: withCoordinates, completionHandler: { (address, response) in
            
            
            if  response != nil {
                if (response?.results()?.count)! > 0 {
                    if response?.results()?[0].lines?.count != 0 {
                        if let lines = response?.firstResult()?.lines {
                            
                            //Deepak App is getting crashed after getting any nil value in address
                            if let thoroughfare = response?.results()?[0].thoroughfare {
                                BQBookingModel.shared.addressLine1 = thoroughfare
                            }
                            
                            if let postalCode = response?.results()?[0].postalCode {
                                BQBookingModel.shared.addressLine3 = postalCode
                            }
                            
                            
                            var middleAddress = ""
                            if let subLocality = response?.results()?[0].subLocality {
                                middleAddress = subLocality
                            }
                            if let locality = response?.results()?[0].locality {
                                if middleAddress.length != 0 {
                                    middleAddress = middleAddress + ", " + locality
                                } else {
                                    middleAddress = locality
                                }
                            }
                            if let country = response?.results()?[0].country {
                                if middleAddress.length != 0 {
                                    middleAddress = middleAddress + ", " + country
                                } else {
                                    middleAddress = country
                                }
                            }
                            BQBookingModel.shared.addressLine2 = middleAddress
                            BQBookingModel.shared.latitude = response?.results()?[0].coordinate.latitude
                            BQBookingModel.shared.longitude = response?.results()?[0].coordinate.longitude
                            self.presenter.getCategories(limit: 50, skip: 0)
                            
                        }
                    }
                }
            }
            self.navigationBar.titleLabel.numberOfLines = 3
            self.navigationBar.titleLabel.font = UIFont.systemFont(ofSize: 14.0)
            self.navigationBar.titleLabel.text = "Delivering to" + "\n" + address
//            self.navigationBar.titleLabel.text = "Current Location" + "\n" + address
        })
        self.locationManager.stopUpdatingLocation()
    }
    
    
    
    private func setUpPresenter() {
        presenter = HomePresenterImplementation(withView: self)
    }
    
    private func initializeView() {
        self.setNavigationBar()
        self.setupCollectionViews()
        UIApplication.shared.statusBarStyle = .default
    }
    
    private func locationTrackingEnable() {
        //  LocationTracker.sharedInstance.setLocationUpdate()
    }
    
    func backAction() {
        
    }
    
    //Push to map for selecting location:
    func setUserCurrentLocation() {
        print("setUserCurrentLocation")
        if let vc = UIStoryboard(name: "Booking", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQUserSavedLocationsVC") as? BQUserSavedLocationsVC {
            vc.isComingFromHome = true
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 2436, 2688, 1792:
                let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
                if isGuestUser {
                    //Use for showing two right bar buttons on navigation bar
                    navigationBar = NavigationView.getNibFileForTwoRightButtons(withHeight: 105, params: NavigationView.NavigationViewParams(leftButtonTitle: "",rightButtonTitle: "", title: "Home", leftButtonImage: #imageLiteral(resourceName: "menu"), rightButtonImage: #imageLiteral(resourceName: "searchWhite"), secondRightButtonImage: nil), leftButtonAction: {[weak self] in
                        if self?.isSearchingEnable == true {
                            //self?.navigationBar.txtSearchField.text = ""
                            //self?.navigationBar.searchView.isHidden = true
                            self?.navigationBar.backButton.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
                            self?.isSearchingEnable = false
                            self?.presenter.getCategories(limit: 50, skip: 0)
                        } else {
                            self?.presentLeftMenuViewController()
                        }
                        
                        }, rightButtonAction: {[weak self] in
                            self?.gotoSearch()
                            //self?.gotoCart()
                        }, rightButtonSecondAction: {[weak self] in
                            //self?.gotoSearch()
                    })
                    
                    navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
                    navigationBar.delegate = self
                    self.view.addSubview(navigationBar)
                } else {
                    //Use for showing two right bar buttons on navigation bar
                    navigationBar = NavigationView.getNibFileForTwoRightButtons(withHeight: 105, params: NavigationView.NavigationViewParams(leftButtonTitle: "",rightButtonTitle: "", title: "Home", leftButtonImage: #imageLiteral(resourceName: "menu"), rightButtonImage: #imageLiteral(resourceName: "cartButton"), secondRightButtonImage: #imageLiteral(resourceName: "searchWhite")), leftButtonAction: {[weak self] in
                        if self?.isSearchingEnable == true {
                            //self?.navigationBar.txtSearchField.text = ""
                            //self?.navigationBar.searchView.isHidden = true
                            self?.navigationBar.backButton.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
                            self?.isSearchingEnable = false
                            self?.presenter.getCategories(limit: 50, skip: 0)
                        } else {
                            self?.presentLeftMenuViewController()
                        }
                        
                        }, rightButtonAction: {[weak self] in
                            self?.gotoCart()
                        }, rightButtonSecondAction: {[weak self] in
                            self?.gotoSearch()
                    })
                    
                    navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
                    navigationBar.delegate = self
                    self.view.addSubview(navigationBar)
                }
            default:
                let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
                if isGuestUser {
                    //Use for showing two right bar buttons on navigation bar
                    navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "",rightButtonTitle: "", title: "Home", leftButtonImage: #imageLiteral(resourceName: "menu"), rightButtonImage: #imageLiteral(resourceName: "searchWhite"), secondRightButtonImage: nil), leftButtonAction: {[weak self] in
                        if self?.isSearchingEnable == true {
                            //self?.navigationBar.txtSearchField.text = ""
                            //self?.navigationBar.searchView.isHidden = true
                            self?.navigationBar.backButton.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
                            self?.isSearchingEnable = false
                            self?.presenter.getCategories(limit: 50, skip: 0)
                        } else {
                            self?.presentLeftMenuViewController()
                        }
                        
                        }, rightButtonAction: {[weak self] in
                            self?.gotoSearch()
                            //self?.gotoCart()
                        }, rightButtonSecondAction: {[weak self] in
                            //self?.gotoSearch()
                    })
                    
                    navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
                    navigationBar.delegate = self
                    self.view.addSubview(navigationBar)
                } else {
                    //Use for showing two right bar buttons on navigation bar
                    navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "",rightButtonTitle: "", title: "Home", leftButtonImage: #imageLiteral(resourceName: "menu"), rightButtonImage: #imageLiteral(resourceName: "cartButton"), secondRightButtonImage: #imageLiteral(resourceName: "searchWhite")), leftButtonAction: {[weak self] in
                        if self?.isSearchingEnable == true {
                            //self?.navigationBar.txtSearchField.text = ""
                            //self?.navigationBar.searchView.isHidden = true
                            self?.navigationBar.backButton.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
                            self?.isSearchingEnable = false
                            self?.presenter.getCategories(limit: 50, skip: 0)
                        } else {
                            self?.presentLeftMenuViewController()
                        }
                        
                        }, rightButtonAction: {[weak self] in
                            self?.gotoCart()
                        }, rightButtonSecondAction: {[weak self] in
                            self?.gotoSearch()
                    })
                    
                    navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
                    navigationBar.delegate = self
                    self.view.addSubview(navigationBar)
                }
            }
        }
        
        
        
    }
    
    func searchText(text: String) {
        print(text)
        if text.length == 0 {
            self.presenter.getCategories(limit: 50, skip: 0)
        } else {
            self.presenter.getCategoriesWithSearch(searchString: text) { (success, response) in
                if success {
                    //self.presenter.categories = response
                }
            }
        }
        
    }
    
    fileprivate func gotoSearch() {
        print("gotoSearch")
        
        if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQSearchProductsVC") as? BQSearchProductsVC {
            self.navigationController?.pushViewController(vc, animated: true)
        }
//        if navigationBar.searchView.isHidden {
//            navigationBar.backButton.setImage(#imageLiteral(resourceName: "BackButtonWhite"), for: .normal)
//            navigationBar.searchView.isHidden = false
//            self.isSearchingEnable = true
//        } else {
//            navigationBar.searchView.isHidden = true
//            navigationBar.backButton.setImage(#imageLiteral(resourceName: "menu"), for: .normal)
//            self.isSearchingEnable = false
//        }
        
    }
    
    fileprivate func gotoCart() {
        
        if productsTotalPrice > 0 {
            if BQBookingModel.shared.latitude != nil {
                if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartSegmentViewController") as? CartSegmentViewController {
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            } else {
                    Singleton.sharedInstance.showAlert("Allow Bawiq to access this device's location?")
            }
        } else {
            if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQNoItemsInCartViewVC") as? BQNoItemsInCartViewVC {
                self.navigationController?.present(vc, animated: true, completion: nil)
            }
        }
    }
    
    private func setupCollectionViews() {
        bannerCollectionView.delegate       =   self
        productCollectionView.delegate      =   self
        bannerCollectionView.dataSource     =   self
        productCollectionView.dataSource    =   self
    }
    
    func showPopUpPromotions() {
        if let vc = UIStoryboard(name: "AfterLogin", bundle: Bundle.main).instantiateViewController(withIdentifier: "PopupPromotionVC") as? PopupPromotionVC {
            vc.providesPresentationContextTransitionStyle = true
            vc.definesPresentationContext = true
            vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func reloadCollection(type: CollectionType, totalAmount: Float) {
        
        if presenter.noOfItemsInProducts > 0 {
            continueShopiingTxt.isHidden = true
            sorryTxt.isHidden = true
        } else {
            continueShopiingTxt.isHidden = false
            sorryTxt.isHidden = false
        }
        
        switch type {
        case .banner:
            DispatchQueue.main.async {
                self.bannerCollectionView.reloadData()
            }
            break
        case .product:
            self.productCollectionView.reloadData()
            break
        }
       productsTotalPrice = totalAmount
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == bannerCollectionView {
            bannerPageController.numberOfPages = presenter.noOfItemsInBanner
            return presenter.noOfItemsInBanner
//            bannerPageController.numberOfPages = presenter.noOfItemsInProducts
//            return presenter.noOfItemsInProducts
        }
        if presenter.noOfItemsInProducts > 0 {
            notAvailableView.isHidden = true
            self.navigationBar.rightButton.isHidden = false
            self.navigationBar.rightButton2.isHidden = false
            return presenter.noOfItemsInProducts
        } else {
            notAvailableView.isHidden = false
            self.navigationBar.rightButton.isHidden = true
            self.navigationBar.rightButton2.isHidden = true
        }
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == bannerCollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: presenter.bannerCellIdentifier, for: indexPath) as? BannerCell else {
                return UICollectionViewCell()
            }
            presenter.setupBannerData(view: cell, index: indexPath.row)
            return cell
        } else if collectionView == productCollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: presenter.productCellIdentifier, for: indexPath) as? ProductCollectionCell else {
                return UICollectionViewCell()
            }
            let category = presenter.cellForRowProduct(indexpath: indexPath)
            if category.is_sponsered {
                cell.featuredLabel.isHidden = false
            } else {
                cell.featuredLabel.isHidden = true
            }
            presenter.setupData(view: cell, index: indexPath.row)
            return cell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == bannerCollectionView {
            return CGSize(width: collectionView.bounds.size.width,
                          height: collectionView.bounds.size.height)
        } else if collectionView == productCollectionView {
            return CGSize(width: productCollectionView.frame.width/2,
                          height: productCollectionView.frame.width/2)
        } else {
            return CGSize.zero
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if BQBookingModel.shared.latitude != nil {
            let storyBoardNlev = UIStoryboard(name: "Booking", bundle: nil)
            if let menuView = storyBoardNlev.instantiateViewController(withIdentifier: "SubCategoryViewController") as? SubCategoryViewController {
                //BQFavoriteAddressModel
                //GMSReverseGeocodeResponse
                if collectionView == bannerCollectionView {
                    let banner = presenter.getBannerForIndexPath(index: indexPath.item)
                    if banner.is_default {
                        print("Default banner")
                    } else {
                        if let category = presenter.cellForRowBanners(indexpath: indexPath) {
                            menuView.seletedCategory = category
                            self.navigationController?.pushViewController(menuView, animated: true)
                        }
                    }
                    
                } else {
                    let sponseredCategory = presenter.cellForRowProduct(indexpath: indexPath).is_sponsered
                    if sponseredCategory {
                        if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "RecommendedProductViewController") as? RecommendedProductViewController {
                            vc.isSponseredProduct = true
                            vc.categorySelected = presenter.cellForRowProduct(indexpath: indexPath)
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    } else {
                        menuView.seletedCategory = presenter.cellForRowProduct(indexpath: indexPath)
                        self.navigationController?.pushViewController(menuView, animated: true)
                    }
                }
            }
        } else {
           // Singleton.sharedInstance.showAlert("Allow Bawiq to access this device's location?")
            let alertController = UIAlertController (title: "", message: "Allow Bawiq to access this device's location?", preferredStyle: .alert)
            
            let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                }
            }
            alertController.addAction(settingsAction)
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
            alertController.addAction(cancelAction)
            
            present(alertController, animated: true, completion: nil)
        }
    }
    
    /**
     Scroll to Next Cell
     */
    func scrollToNextCell(){
        
        if let collectionView = bannerCollectionView {
            if self.presenter.noOfItemsInBanner > 0 {
                if bannerScrollCount == self.presenter.noOfItemsInBanner - 1 {
                    let nextIndexpath = IndexPath(item: 0, section: 0)
                    collectionView.scrollToItem(at: nextIndexpath, at: .centeredHorizontally, animated: true)
                    bannerScrollCount = 0
                    self.bannerPageController.currentPage = bannerScrollCount
                } else {
                    let nextIndexpath = IndexPath(item: bannerScrollCount + 1, section: 0)
                    collectionView.scrollToItem(at: nextIndexpath, at: .centeredHorizontally, animated: true)
                    bannerScrollCount = bannerScrollCount + 1
                    self.bannerPageController.currentPage = bannerScrollCount
                }
            }
        }
        
    }
    
    /**
     Invokes Timer to start Automatic Animation with repeat enabled
     */
    func startTimer() {
        if self.presenter.noOfItemsInBanner > 1 {
            timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.scrollToNextCell), userInfo: nil, repeats: true)
        }
    }

}
extension HomeVC: BQUserSavedLocationsVCDelegate {
    func selectedAddress(address: BQFavoriteAddressModel) {
        if let locType = address.locType {
            
            
            //Deepak App is getting crashed after getting any nil value in address
            if let landmark = address.landmark {
                BQBookingModel.shared.addressLine1 = landmark
            }
            
            if let postalCode = address.postal_code {
                BQBookingModel.shared.addressLine3 = postalCode
            }
            
            if let area = address.address {
                BQBookingModel.shared.addressLine2 = area
            }
            
            if let apartmentNo = address.building_number {
                BQBookingModel.shared.apartmentNo = apartmentNo
            }
            
            if let floorNo = address.floor_number {
                BQBookingModel.shared.floorNo = floorNo
            }
            
            if let doorNo = address.door_number {
                BQBookingModel.shared.doorNo = doorNo
            }
            
            if let streetNo = address.street_number {
                BQBookingModel.shared.streetNo = streetNo
            }
            
            BQBookingModel.shared.latitude = Double("\(address.latitude ?? "")")
            BQBookingModel.shared.longitude = Double("\(address.longitude ?? "")")
            self.navigationBar.titleLabel.text = "Delivering to" + "\n" + locType
//            self.navigationBar.titleLabel.text = "Current Location" + "\n" + locType
        }
        
    }
}

enum CollectionType {
    case banner
    case product
}

protocol HomeView: class  {
    func reloadCollection(type: CollectionType, totalAmount: Float)
    func showPopUpPromotions()
}

protocol HomePresenter {
    var noOfItemsInProducts: Int {get}
    var noOfItemsInBanner: Int {get}
    func cellForRowProduct(indexpath: IndexPath) -> Category
    func cellForRowBanners(indexpath: IndexPath) -> Category?
    func getBannerForIndexPath(index: Int) -> Banners
    func getCategories(limit: Int, skip: Int)
    var bannerCellIdentifier: String {get}
    var productCellIdentifier: String {get}
    func setupData(view: ProductCollectionCellView, index: Int)
    func setupBannerData(view: BannerCollectionCellView, index: Int)
    func getCategoriesWithSearch(searchString: String, _ receivedResponse: @escaping (_ succeeded: Bool, _ response:[Category]?) -> ())
    func getAllBanners()
    
}

class HomePresenterImplementation: HomePresenter {
    
    weak var view: HomeView?
    var banners = [Banners]() //[BannerProduct]()
    var categories = [Category]()
    init(withView view: HomeView) {
        self.view = view
    }
    
    var bannerCellIdentifier: String {
        return "bannercell"
    }
    
    var productCellIdentifier: String {
        return "productCell"
    }
    
    var noOfItemsInProducts: Int {
        return categories.count
    }
    
    var noOfItemsInBanner: Int {
        return banners.count
    }
    
    
    func cellForRowProduct(indexpath: IndexPath) -> Category {
        return categories[indexpath.row]
    }
    
    func cellForRowBanners(indexpath: IndexPath) -> Category? {
        let bannerCategoryId = banners[indexpath.row].category_id
        for item in categories where bannerCategoryId == item.id {
            return item
        }
        return nil
    }
    
    func getBannerForIndexPath(index: Int) -> Banners {
        return banners[index]
    }
    
    func setupData(view: ProductCollectionCellView, index: Int) {
        let category = categories[index]
        let model = ProductCollectionCellImplemention(data: category)
        view.setupData(model: model)
    }
    
    func setupBannerData(view: BannerCollectionCellView, index: Int) {
        let category = banners[index]
        let model = BannerCollectionCellImplemention(data: category)
        view.setupBannerData(model: model)
        
//        let category = categories[index]
//        let model = ProductCollectionCellImplemention(data: category)
//        view.setupData(model: model)
    }
    
    func getAllBanners() {
        
        Category.getAllBannerData({ (success, response) in
            if success {
                guard let bannersArray = response else {
                    return
                }
                self.banners.removeAll()
                self.banners = bannersArray
               // self.view?.reloadCollection(type: .banner, totalAmount: 0.0)
            }
        })
    }
    
    func getCategoriesWithSearch(searchString: String, _ receivedResponse: @escaping (_ succeeded: Bool, _ response:[Category]?) -> ()){
        
        
        Category.getCategoriesWithSearch(searchString: searchString) { (success, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if success {
                guard let categoryArray = response else {
                    return
                }
                self.categories.removeAll()
               // self.categories = categoryArray
                
                //if let price = totalPrice {
                    self.view?.reloadCollection(type: .product, totalAmount: 0.0)
            //    }
            }
        }
    }
    
    func getCategories(limit: Int = 10, skip: Int = 0) {
        
        Category.getCategories(limit: limit, skip: skip) { (success, response, totalPrice) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if success {
                self.categories.removeAll()
                self.view?.reloadCollection(type: .banner, totalAmount: 0.0)
                guard let categoryArray = response else {
                    return
                }
                self.categories = categoryArray
                
                if let price = totalPrice {
                    self.view?.reloadCollection(type: .product, totalAmount: price)
                }
            } else {
                self.categories.removeAll()
                self.view?.reloadCollection(type: .product, totalAmount: 0.0)
                ErrorView.showWith(message: "Sorry!! We are not serving in this area", removed: nil)
            }
            if let popupPromotions = Vendor.current?.promotions {
                self.view?.showPopUpPromotions()
            }
        }
    }
}


struct BannerProduct {
    var id: Int?
    var imageUrl: String?
    
}

class Category {
    var id: Int?
    var imageUrl: String?
    var categoryName: String?
    var categoryDescription:  String?
    var isBlocked: Bool?
    var creationDate: String?
    
    var subCategoryId: Int?
    var subCategoryName: String?
    var subCategoryImage: String?
    var subCategoryDescription: String?
    
    var is_sponsered: Bool
    
    var isPaginationRequired: Bool = false
    
    init(with json:[String: Any]) {
        self.id = json["category_id"] as? Int
        self.categoryDescription = json["category_description"] as? String
        self.imageUrl = json["category_image"] as? String
        self.categoryName = json["category_name"] as? String
        self.creationDate = json["creation_datetime"] as? String
        self.isBlocked = json["is_blocked"] as? Bool
        self.subCategoryId = json["sub_category_id"] as? Int
        self.subCategoryName = json["sub_category_name"] as? String
        self.subCategoryImage = json["sub_category_image"] as? String
        self.categoryDescription = json["sub_category_description"] as? String
        if let is_sponsered = json["is_sponsored"] as? Bool {
            self.is_sponsered = is_sponsered
        } else {
            self.is_sponsered = false
        }
    }
    
    class func getCategories(limit: Int = 10, skip: Int = 0, _ receivedResponse: @escaping (_ succeeded: Bool, _ response:[Category]?, _ totalPrice: Float?) -> ()) {
        var params = [String: Any]()
        let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
        params["user_type"] = 0
        params["limit"] = limit
        params["skip"] = skip
        if !isGuestUser {
            params["access_token"] = Vendor.current!.appAccessToken!
        }
        if let lat = BQBookingModel.shared.latitude, let long = BQBookingModel.shared.longitude {
            params["lattitude"] = lat
            params["longitude"] = long
            ActivityIndicator.sharedInstance.showActivityIndicator()
            //ActivityIndicator.sharedInstance.hideActivityIndicator()
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getCategories,
                                                             params: params as [String : AnyObject],
                                                             httpMethod: "POST") { (succeeded, response) in
                                                                DispatchQueue.main.async {
                                                                    if succeeded {
                                                                        guard let data = response["data"] as? [String: Any], let category = data["data"] as? [[String: Any]] else {
                                                                            receivedResponse(false,nil, 0.0)
                                                                            return
                                                                        }
                                                                        let categoryArray = category.map({Category(with: $0)})
                                                                        if let totalPrice = data["total_price"] as? Double {
                                                                            receivedResponse(true, categoryArray, Float(totalPrice))
                                                                        }
                                                                    } else {
                                                                        //                                                                    if let errorMessage = response["message"] as? String {
                                                                        //                                                                        ErrorView.showWith(message: errorMessage, isErrorMessage: true, removed: {
                                                                        //                                                                            receivedResponse(false,nil, 0.0)
                                                                        //                                                                        })
                                                                        //                                                                    }
                                                                        
                                                                        receivedResponse(false,nil, 0.0)
                                                                        //                                                                    if let errorMessage = response["message"] as? String {
                                                                        //                                                                        let alertController = UIAlertController(title: "", message: errorMessage, preferredStyle: .alert)
                                                                        //                                                                        let yesAction = UIAlertAction(title: "Ok", style: .destructive, handler: { (action) -> Void in
                                                                        //                                                                            receivedResponse(false,nil, 0.0)
                                                                        //                                                                        })
                                                                        //
                                                                        //                                                                        alertController.addAction(yesAction)
                                                                        //                                                                        self.present(alertController, animated: true, completion: nil)
                                                                        //
                                                                        //                                                                    }
                                                                        
                                                                    }
                                                                }
            }
            
        }
    }
    
    class func getCategoriesWithSearch(searchString: String, _ receivedResponse: @escaping (_ succeeded: Bool, _ response:[Products]?) -> ()){
        if let lat = BQBookingModel.shared.latitude, let long = BQBookingModel.shared.longitude {
            var params = [String: Any]()
            let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
            
            if !isGuestUser {
                params["access_token"] = Vendor.current!.appAccessToken!
            }
            
            params["user_type"] = 0
            params["limit"] = 50
            params["skip"] = 0
            params["searchCriteria"] = searchString
            params["lattitude"] = lat
            params["longitude"] = long
            
            //        let params: [String : Any] = [
            //            "user_type":0,
            //            "limit": 50,
            //            "skip": 0,
            //            "access_token": Vendor.current!.appAccessToken!,
            //            "searchCriteria": searchString,
            //            "lattitude": lat,
            //            "longitude": long]
            ActivityIndicator.sharedInstance.showActivityIndicator()
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getProducts,
                                                             params: params as [String : AnyObject],
                                                             httpMethod: "POST") { (succeeded, response) in
                                                                DispatchQueue.main.async {
                                                                    if succeeded {
                                                                        guard let data = response["data"] as? [String: Any], let products = data["data"] as? [[String: Any]] else {
                                                                            receivedResponse(false,nil)
                                                                            return
                                                                        }
                                                                        let stringJson = products.jsonString
                                                                        let productsData = stringJson.data(using: .utf8)
                                                                        do {
                                                                            let productsArray = try JSONDecoder().decode([Products].self, from: productsData!)
                                                                            receivedResponse(true, productsArray)
                                                                        } catch {
                                                                            print(error.localizedDescription)
                                                                            receivedResponse(false,nil)
                                                                        }
                                                                    } else {
                                                                        receivedResponse(false,nil)
                                                                    }
                                                                }
            }
        }
    }
    
    
    func getSubCategories(limit: Int = 10, skip: Int = 0, _ receivedResponse: @escaping (_ succeeded: Bool, _ response:[Category]?, _ totalPrice: Float?) -> ()){
        if let lat = BQBookingModel.shared.latitude, let long = BQBookingModel.shared.longitude {
            var params = [String: Any]()
            let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
            
            if !isGuestUser {
                params["access_token"] = Vendor.current!.appAccessToken!
            }
            
            params["user_type"] = 0
            params["limit"] = limit
            params["skip"] = skip
            params["category_id"] = self.id!
            params["lattitude"] = lat
            params["longitude"] = long
            
            //        let params: [String : Any] = [
            //            "user_type":0,
            //            "limit": limit,
            //            "skip": skip,
            //            "access_token": Vendor.current!.appAccessToken!,
            //            "category_id": self.id!,
            //            "lattitude": lat,
            //            "longitude": long]
            
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getSubCategories,
                                                             params: params as [String : AnyObject],
                                                             httpMethod: "POST") { (succeeded, response) in
                                                                DispatchQueue.main.async {
                                                                    if succeeded {
                                                                        guard let data = response["data"] as? [String: Any], let category = data["data"] as? [[String: Any]] else {
                                                                            receivedResponse(false,nil,0.0)
                                                                            return
                                                                        }
                                                                        let categoryArray = category.map({Category(with: $0)})
                                                                        if let totalPrice = data["total_price"] as? Double {
                                                                            receivedResponse(true, categoryArray, Float(totalPrice))
                                                                        }
                                                                    } else {
                                                                        receivedResponse(false,nil,0.0)
                                                                    }
                                                                }
                                                                
            }
        }
    }
    
    
    
    func getProducts(category: Category,limit: Int = 10, skip: Int = 0, _ receivedResponse: @escaping (_ succeeded: Bool, _ response:[Products]?, _ paginationRequired: Bool) -> ()){
        if let lat = BQBookingModel.shared.latitude, let long = BQBookingModel.shared.longitude {
            var params = [String: Any]()
            let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
            
            if !isGuestUser {
                params["access_token"] = Vendor.current!.appAccessToken!
            }
            
            params["user_type"] = 0
            params["limit"] = limit
            params["skip"] = skip
            params["category_id"] = self.id!
            params["sub_category_id"] = self.subCategoryId!
            params["lattitude"] = lat
            params["longitude"] = long
            
            //        let params: [String : Any] = [
            //            "user_type":0,
            //            "limit": limit,
            //            "skip": skip,
            //            "access_token": Vendor.current!.appAccessToken!,
            //            "category_id": category.id!,
            //            "sub_category_id": self.subCategoryId!,
            //            "lattitude": lat,
            //            "longitude": long]
            
            print("Get products param: ", params)
            
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getProducts,
                                                             params: params as [String : AnyObject],
                                                             httpMethod: "POST") { (succeeded, response) in
                                                                DispatchQueue.main.async {
                                                                    if succeeded {
                                                                        guard let data = response["data"] as? [String: Any], let products = data["data"] as? [[String: Any]] else {
                                                                            receivedResponse(false,nil, false)
                                                                            return
                                                                        }
                                                                        guard let productCount = data["iTotalRecords"] as? Int else {
                                                                            return
                                                                        }
                                                                        let stringJson = products.jsonString
                                                                        let productsData = stringJson.data(using: .utf8)
                                                                        do {
                                                                            let productsArray = try JSONDecoder().decode([Products].self, from: productsData!)
                                                                            self.isPaginationRequired =  productCount > productsArray.count
                                                                            receivedResponse(true, productsArray,self.isPaginationRequired)
                                                                            
                                                                            
                                                                            //                                                                        let productsArray = try JSONDecoder().decode([Products].self, from: productsData!)
                                                                            //                                                                        receivedResponse(true, productsArray)
                                                                        } catch {
                                                                            print(error.localizedDescription)
                                                                            receivedResponse(false,nil,false)
                                                                        }
                                                                    } else {
                                                                        receivedResponse(false,nil,false)
                                                                    }
                                                                }
                                                                
            }
        }
    }
    
    class func getAllBannerData(_ receivedResponse: @escaping (_ succeeded: Bool, _ response:[Banners]?) -> ()) {
        
        var params = [String: Any]()
        let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")

        if !isGuestUser {
            params["app_access_token"] = Vendor.current!.appAccessToken!
            //            let params: [String : Any] = [
            //                "app_access_token": Vendor.current!.appAccessToken!]
        }
        ActivityIndicator.sharedInstance.showActivityIndicator()
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getAllBanners,
                                                         params: params as [String : AnyObject],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                            DispatchQueue.main.async {
                                                                if succeeded {
                                                                    guard let json = response["data"] as? [String: Any] else {
                                                                        //, let category = data["data"] as? [[String: Any]] else {
                                                                        receivedResponse(false,nil)
                                                                        return
                                                                    }
                                                                    guard let category = json["banners"] as? [[String: Any]] else {
                                                                        //, let category = data["data"] as? [[String: Any]] else {
                                                                        receivedResponse(false,nil)
                                                                        return
                                                                    }
                                                                    var bannerArray = [Banners]()
                                                                    for item in category {
                                                                        let bannerData = Banners(data: item)
                                                                        bannerArray.append(bannerData)
                                                                    }
                                                                    receivedResponse(true, bannerArray)
                                                                } else {
                                                                    receivedResponse(false,nil)
                                                                }
                                                            }
                                                            
        }
    }
}

struct BannerCollectionCellImplemention: BannerCollectionCellModel {
    let categoryImage: String
    
//    init(data: BannerProduct) {
//        if let image = data.imageUrl {
//            categoryImage = image
//        } else {
//            categoryImage = ""
//        }
//    }
    init(data: Banners) {
        if let image = data.category_image {
            categoryImage = image
        } else {
            categoryImage = ""
        }
    }
}

protocol BannerCollectionCellView {
    func setupBannerData(model: BannerCollectionCellModel)
   // func setupData(model: ProductCollectionCellModel)
}

protocol BannerCollectionCellModel {
    var categoryImage: String {get}
}

class BannerCell: UICollectionViewCell, BannerCollectionCellView {
    
    @IBOutlet weak var bannerImageView: UIImageView! {
        didSet {
            bannerImageView.contentMode = .scaleToFill
            bannerImageView.clipsToBounds = true
        }
    }

    
//    func setupData(model: ProductCollectionCellModel) {
//        let imageUrl = URL.get(from: model.productImage)
////        self.bannerImageView.kf.setImage(with: imageUrl)
//        self.bannerImageView.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "baLogo"))
//    }
    func setupBannerData(model: BannerCollectionCellModel) {
        let imageUrl = URL.get(from: model.categoryImage)
        self.bannerImageView.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "logoTookanSplashGraphic"))
    }
    
}

protocol ProductCollectionCellModel {
    var productImage: String {get}
    var productName: String {get}
}

struct ProductCollectionCellImplemention: ProductCollectionCellModel {
    let productImage: String
    let productName: String
    
    init(data: Category) {
        if data.is_sponsered {
            if let name = data.subCategoryName {
                productName = name
            } else {
                productName = "Unknwon"
            }
            if let image = data.subCategoryImage {
                productImage = image
            } else {
                productImage = ""
            }
        } else {
            if let name = data.categoryName {
                productName = name
            } else {
                productName = "Unknwon"
            }
            if let image = data.imageUrl {
                productImage = image
            } else {
                productImage = ""
            }
        }
        
    }
}

protocol ProductCollectionCellView {
    func setupData(model: ProductCollectionCellModel)
}

class ProductCollectionCell: UICollectionViewCell, ProductCollectionCellView {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var featuredLabel: UILabel!
    
    var productId: Int?
    
    
    func setupData(model: ProductCollectionCellModel) {
        let imageUrl = URL.get(from: model.productImage)
        self.productImage.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "bawiqPlaceholde"))
        self.productName.text = model.productName.capitalizingFirstLetter()
    }
}
