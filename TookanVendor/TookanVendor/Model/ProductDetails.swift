//
//  ProductDetails.swift
//  TookanVendor
//
//  Created by CL-Macmini-110 on 10/5/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

class ProductDetails: NSObject {
   var orderItemID: Int? = 0
   var jobID : Int? = 0
   var productID : Int? = 0
   var customerID : Int? = 0
   var unitPrice : Int? = 0
   var quantity : Int? = 0
   var totalPrice : Int? = 0
   var customizationLinking : String? = ""
   var status : Int? = 0
   var createdAt : String? = ""
   var updateAt: String? = ""
   var productName: String? = ""
   
   
   
   init(json: [String: Any]) {
      
      if let value = json["order_item_id"] as? String {
         if let intValue = Int(value){
            self.orderItemID = intValue
         }
      } else if let value = json["order_item_id"] as? NSNumber {
         self.orderItemID = value as? Int
      }
      
      if let value = json["job_id"] as? String {
         if let intValue = Int(value){
            self.jobID = intValue
         }
      } else if let value = json["job_id"] as? NSNumber {
         self.jobID = value as? Int
      }
      
      if let value = json["product_id"] as? String {
         if let intValue = Int(value){
            self.productID = intValue
         }
      } else if let value = json["product_id"] as? NSNumber {
         self.productID = value as? Int
      }
      
      if let value = json["cust_id"] as? String {
         if let intValue = Int(value){
            self.customerID = intValue
         }
      } else if let value = json["cust_id"] as? NSNumber {
         self.customerID = value as? Int
      }
      
      if let value = json["unit_price"] as? String {
         if let intValue = Int(value){
            self.unitPrice = intValue
         }
      } else if let value = json["unit_price"] as? NSNumber {
         self.unitPrice = value as? Int
      }
      
      if let value = json["quantity"] as? String {
         if let intValue = Int(value){
            self.quantity = intValue
         }
      } else if let value = json["quantity"] as? NSNumber {
         self.quantity = value as? Int
      }
      
      if let value = json["customization_linking"] as? String {
         self.customizationLinking = value
      }
      
      if let value = json["status"] as? String {
         if let intValue = Int(value){
            self.status = Int(intValue)
         }
      } else if let value = json["status"] as? NSNumber {
         self.status = value as? Int
      }
      
      if let value = json["created_at"] as? String {
         self.createdAt = value
      }
      
      if let value = json["updated_at"] as? String {
         self.updateAt = value
      }
      
      
      if let value = json["total_price"] as? String {
         if let intValue = Int(value){
            self.totalPrice = Int(intValue)
         }
      } else if let value = json["total_price"] as? NSNumber {
         self.totalPrice = value as? Int
      }
      
      
      if let value = json["product_name"] as? String {
         self.productName = value
      }
      
   }
}
