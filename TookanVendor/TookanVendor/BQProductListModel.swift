//
//  BQProductListModel.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 12/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQProductListModel: NSObject {
    var brand_name: String?
    var category_id: Int?
    var category_name: String?
    var order_item_id: Int?
    var product_description: String?
    var product_id: Int?
    var product_name: String?
    var quantity: Int?
    var sub_category_id: Int?
    var sub_category_name: String?
    var productImage = ""
    var total_price: Float?
    var unit_price: Double?
    var is_blocked: Bool?
    var pack_size: String?
    var measuring_unit: String?
    var product_image_url = ""
    var discount_percentage: Double?
    var discounted_price: Double?
    
    convenience init(param: [String: Any]) {
        self.init()
       
        if let brand_name = param["brand_name"] as? String {
            self.brand_name = brand_name
        }
        if let isBlocked = param["discount_percentage"] as? Double {
            self.discount_percentage = isBlocked
        }
        if let isBlocked = param["discounted_price"] as? Double {
            self.discounted_price = isBlocked
        }
        
        if let isBlocked = param["product_image_url"] as? String {
            self.product_image_url = isBlocked
        }
        
        if let isBlocked = param["pack_size"] as? String {
            self.pack_size = isBlocked
        }
        if let isBlocked = param["measuring_unit"] as? String {
            self.measuring_unit = isBlocked
        }
        
        if let isBlocked = param["productImage"] as? String {
            self.productImage = isBlocked
        }
        
        if let isBlocked = param["is_blocked"] as? Bool {
            self.is_blocked = isBlocked
        }
        
        if let isBlocked = param["unit_price"] as? Double {
            self.unit_price = isBlocked
        }
        
        if let isBlocked = param["total_price"] as? Float {
            self.total_price = isBlocked
        }
        
        if let categoryDescription = param["category_id"] as? Int {
            self.category_id = categoryDescription
        }
        
        if let categoryId = param["category_name"] as? String {
            self.category_name = categoryId
        }
        
        if let categoryImage = param["order_item_id"] as? Int {
            self.order_item_id = categoryImage
        }
        if let categoryName = param["product_description"] as? String {
            self.product_description = categoryName
        }
        
        if let creationDatetime = param["product_id"] as? Int {
            self.product_id = creationDatetime
        }
        
        if let isBlocked = param["product_name"] as? String {
            self.product_name = isBlocked
        }
        
        if let isBlocked = param["quantity"] as? Int {
            self.quantity = isBlocked
        }
        if let isBlocked = param["sub_category_id"] as? Int {
            self.sub_category_id = isBlocked
        }
        if let isBlocked = param["sub_category_name"] as? String {
            self.sub_category_name = isBlocked
        }
    }
    
    
}
