//
//  OrderDetail.swift
//  TookanVendor
//
//  Created by CL-Macmini-110 on 10/5/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

enum OrderSectionFields: Int{
   case status
   case itemSummary
   case billSummary
   case paymentSummary
}

enum OrderDetailStatus: Int {
   case status
   case orderTime
   case pickupTime
   case pickupFrom
   case deliveryTime
   case deliveryTo
}


class OrderDetail: UIViewController {
   
   //MARK:- Outlets
   @IBOutlet var orderDetailTable: UITableView!
   @IBOutlet var navBackground: UIView!
   @IBOutlet var titleLabel: UILabel!
   @IBOutlet var backButton: UIButton!
   
   //MARK:- properties
   let backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1.0)
   var jobId:String!
   var forTracking : Bool?
   var sessionId:String!
   var orderDetails:OrderDetails!
   let headerHeight:CGFloat = 50.0
   let serverDateFormat = "yyyy-MM-dd'T'HH:mm:ss.000Z"
   let pickupServerDateFormat = "MM/dd/yyyy hh:mm a"
   let displayDateFormat = "dd, MMM yyyy, hh:mm a"
   
   //MARK:- View Life Cycle
   override func viewDidLoad() {
    orderDetailTable.bounces = false
      super.viewDidLoad()
      self.setTableView()
      self.setNavigationBar()
      self.view.backgroundColor = UIColor(red: 239/255, green: 239/255, blue: 239/255, alpha: 1.0)
   }
   //MARK:- Setting Views
   func setTableView()  {
      self.orderDetailTable.dataSource = self
      self.orderDetailTable.delegate = self
      self.orderDetailTable.register(UINib(nibName: NIB_NAME.orderStatusCell, bundle: frameworkBundle), forCellReuseIdentifier: NIB_NAME.orderStatusCell)
      self.orderDetailTable.register(UINib(nibName: NIB_NAME.orderSummaryCell, bundle: frameworkBundle), forCellReuseIdentifier: NIB_NAME.orderSummaryCell)
      self.orderDetailTable.backgroundColor = backgroundColor
      self.orderDetails.setStatusSections(orderDetails: orderDetails)
   }
   
   func setNavigationBar() {
      self.navBackground.dropShadow(backgroundColor: UIColor.white, shadowColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0.5), opacity: 0.7, offSet: CGSize(width: 2, height: 3), radius: 4.0)
      self.setTitleLabel()
      self.setBackButton()
   }
   
   func setBackButton() {
      self.backButton.setImage(backButtonImage, for: .normal)
      self.backButton.tintColor = UIColor.black
   }
   
   func setTitleLabel() {
      self.titleLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.priceFontSize)
      self.titleLabel.text = "\(TEXT.ORDER_CAP.capitalized) #\(self.jobId ?? "")"
   }
   
   //MARK:- IBAction
   @IBAction func backAction(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
   }
   
   //MARK:- Setting Last Row of section
   func settingLastRow(cell: OrderSummaryCell, index: Int) {
      if index == orderDetails.productDetails.count-1 {
         cell.outerView.dropShadow(backgroundColor: UIColor.white, shadowColor: UIColor(red: 0, green: 0, blue: 0, alpha: 0.5), opacity: 0.7, offSet: CGSize(width: 2, height: 3), radius: 4.0)
      } else {
         cell.outerView.dropShadow(backgroundColor: UIColor.white, shadowColor: nil, opacity: 0, offSet: CGSize(width: 0, height: 0), radius: 0)
         
      }
   }
}

//MARK:- Table View Delegate
extension OrderDetail: UITableViewDelegate, UITableViewDataSource {
   func numberOfSections(in tableView: UITableView) -> Int {
      return 2
   }
   
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      let type = OrderSectionFields(rawValue: section)
      switch type {
      case .status?:
         return self.orderDetails.sectionsOfStatusCell.count
      case .itemSummary?:
         return orderDetails.productDetails.count
      case .billSummary?:
         return 0
      case .paymentSummary?:
         return 0
      default:
         return 0
      }
   }
   
   func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let sectionType = OrderSectionFields(rawValue: indexPath.section)
      switch sectionType {
      case .status?:
         if let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.orderStatusCell) as? OrderStatusCell{
            cell.backgroundColor = backgroundColor
            cell.rightLabel.textColor = UIColor.black
            switch self.orderDetails.sectionsOfStatusCell[indexPath.row] {
            case .status:
               cell.leftLabel.text = "\(TEXT.STATUS_CAP):"
               let temp = orderDetails.job_status!.getTextAndColor(vertical: orderDetails.vertical)
               cell.rightLabel.text = temp.text
               cell.rightLabel.textColor = temp.color
            case .orderTime:
               cell.leftLabel.text = "\(TEXT.ORDER_TIME_CAP):"
               cell.rightLabel.text = orderDetails.taskHistory[0].creationDatetime.convertDateFromUTCtoLocal(input: serverDateFormat, output: displayDateFormat,toConvert:false)
            case .pickupTime:
               cell.leftLabel.text = "\(TEXT.PICKUP_TIME_CAP):"
               cell.rightLabel.text = orderDetails.job_pickup_datetime.datefromYourInputFormatToOutputFormat(input: pickupServerDateFormat, output: displayDateFormat)
            case .pickupFrom:
               cell.leftLabel.text = "\(TEXT.deliverTo):"
               cell.rightLabel.text = orderDetails.job_pickup_address
            case .deliveryTime:
               cell.leftLabel.text = "\(TEXT.DELIVERY_TIME_CAP):"
               cell.rightLabel.text = orderDetails.job_delivery_datetime
            case .deliveryTo:
               cell.leftLabel.text = "\(TEXT.DELIVERY_TO_CAP):"
               cell.rightLabel.text = orderDetails.job_address
            }
            if indexPath.row == self.orderDetails.sectionsOfStatusCell.count-1 {
               cell.bottomLine.isHidden = true
            } else {
               cell.bottomLine.isHidden = false
            }
            return cell
         }
         
      case .itemSummary?:
         guard let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.orderSummaryCell) as? OrderSummaryCell else {
            fatalError("OrderSummaryCell not found")
         }
         cell.backgroundColor = UIColor.white
         let name = "\(self.orderDetails.productDetails[indexPath.row].productName ?? "xyz")"
         let quantity = "\(self.orderDetails.productDetails[indexPath.row].unitPrice ?? 0)  x  \(self.orderDetails.productDetails[indexPath.row].quantity ?? 0)"
         cell.leftLabel.text = "\(name) \n $\(quantity)"
         cell.rightLabel.text = "$\(self.orderDetails.productDetails[indexPath.row].totalPrice ?? 0)"
         self.settingLastRow(cell: cell, index: indexPath.row)
         if indexPath.row == self.orderDetails.productDetails.count - 1{
         cell.dashedHorizontalLineView.isHidden = true
         }else{
            cell.dashedHorizontalLineView.isHidden = false
         }
         return cell
         
      case .billSummary?:
         guard let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.orderSummaryCell) as? OrderSummaryCell else {
            fatalError("OrderSummaryCell not found")
         }
         cell.backgroundColor = UIColor.white
         let type = OrderDetailStatus(rawValue: indexPath.row)
         switch type {
         case .status?:
            cell.leftLabel.text = "\(TEXT.STATUS):\n 2*3"
         case .orderTime?:
            cell.leftLabel.text = "\(TEXT.ORDER_TIME_CAP):\n 2*3"
         case .deliveryTime?:
            cell.leftLabel.text = "\(TEXT.DELIVERY_TIME_CAP):\n 2*3"
         case .deliveryTo?:
            cell.leftLabel.text = "\(TEXT.DELIVERY_TO_CAP):\n 2*3"
            
         default:
            break
         }
         self.settingLastRow(cell: cell, index: indexPath.row)
         return cell
         
      case .paymentSummary?:
         guard let cell = tableView.dequeueReusableCell(withIdentifier: NIB_NAME.orderSummaryCell) as? OrderSummaryCell else {
            fatalError("OrderSummaryCell not found")
         }
         cell.backgroundColor = UIColor.white
         let type = OrderDetailStatus(rawValue: indexPath.row)
         switch type {
         case .status?:
            cell.leftLabel.text = "\(TEXT.STATUS):\n 2*3"
         case .orderTime?:
            cell.leftLabel.text = "\(TEXT.ORDER_TIME_CAP):\n 2*3"
         case .deliveryTime?:
            cell.leftLabel.text = "\(TEXT.DELIVERY_TIME_CAP):\n 2*3"
         case .deliveryTo?:
            cell.leftLabel.text = "\(TEXT.DELIVERY_TO_CAP):\n 2*3"
            
         default:
            break
         }
         self.settingLastRow(cell: cell, index: indexPath.row)
         return cell
         
         
      default:
         break
      }
      return UITableViewCell()
   }
   
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return UITableViewAutomaticDimension
   }
   
   func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      let sectionType = OrderSectionFields(rawValue: section)
      switch sectionType {
      case .status?:
         return 0.01
      case .itemSummary?:
         guard self.orderDetails.productDetails.count > 0 else {
            return 0.01
         }
         return headerHeight
      case .billSummary?:
         guard self.orderDetails.productDetails.count > 0 else {
            return 0.01
         }
         return headerHeight
      case .paymentSummary?:
         guard self.orderDetails.productDetails.count > 0 else {
            return 0.01
         }
         return headerHeight
      default:
         return headerHeight
      }
   }

   func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      let headerView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.orderDetailTable.frame.width, height: headerHeight))
      headerView.backgroundColor = UIColor.white
      
      let bottomLine = UIView(frame: CGRect(x: 10, y: 49, width: self.orderDetailTable.frame.width - 20.0, height: 1))
      bottomLine.backgroundColor = COLOR.IN_ACTIVE_IMAGE_COLOR
      let label = UILabel(frame: CGRect(x: 10.0, y: 0.0, width: self.orderDetailTable.frame.width, height: headerHeight))
      label.textColor = COLOR.textColorSlightLight
      label.font = UIFont(name: FONT.regular, size: FONT_SIZE.medium)
      let sectionType = OrderSectionFields(rawValue: section)
      switch sectionType {
      case .status?:
         headerView.backgroundColor = backgroundColor
         
      case .itemSummary?:
         label.text = TEXT.ITEM_SUMMARY_CAP
         if self.orderDetails.productDetails.count > 0 {
            headerView.addSubview(label)
            headerView.addSubview(bottomLine)
         }
      case .billSummary?:
         label.text = TEXT.BILL_SUMMARY_CAP
         headerView.addSubview(label)
         headerView.addSubview(bottomLine)
      case .paymentSummary?:
         label.text = TEXT.PAYMENT_SUMMARY_CAP
         headerView.addSubview(label)
         headerView.addSubview(bottomLine)
      default:
         break
      }
      return headerView
   }
   func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
      return 20.0
   }
   
   func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
      let footerView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.orderDetailTable.frame.width, height: self.orderDetailTable.frame.height))
      footerView.backgroundColor = backgroundColor
      return footerView
   }
}
