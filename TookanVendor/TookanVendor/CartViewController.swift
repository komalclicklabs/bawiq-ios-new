//
//  CartController.swift
//  TookanVendor
//
//  Created by Vishal on 15/02/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class CartViewController: UIViewController, CartView, UITextViewDelegate {
    func errorInbookingCreation(message: String) {
        
    }
    
  

    @IBOutlet weak var productTable: UITableView!
    @IBOutlet weak var specialInstruction: UITextView!
//    @IBOutlet weak var addNewItemView: UIView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var cartBottomView: UIView!
//    @IBOutlet weak var btnAddItem: UIButton!
    
    var presenter: CartPresenter!
    var products = [Products]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpPresenter()
        self.setUpTable()
        
    }
    private func setUpPresenter() {
        presenter = CartPresenterImplementation(withView: self)
    }
    
    private func setUpTable() {
        self.productTable.register(UINib(nibName: "ProductCell", bundle: nil), forCellReuseIdentifier: "ProductCell")
        self.productTable.register(UINib(nibName: "AddNewItemTableViewCell", bundle: nil), forCellReuseIdentifier: "AddNewItemTableViewCell")
        //btnAddItem.layer.cornerRadius = 5
        self.productTable.delegate = self
        self.productTable.dataSource = self
        self.productTable.rowHeight = UITableViewAutomaticDimension
        self.productTable.estimatedRowHeight = 200
    }
    
    
    @IBAction func addNewItemAction(_ sender: UIButton) {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is SSASideMenu {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    
    @IBAction func checkout(_ sender: UIButton) {
        if let parent = self.parent as? CartSegmentViewController {
            parent.switchController(from: CartController.cart)
            
        }
        
    }
    
    func reloadTable() {
         self.productTable.reloadData()
    }
    
    func refreshCell(at index: Int, isAdded: Bool, cartCount: Int, totalPrice: Float) {
        
        if cartCount > 0 {
            cartBottomView.isHidden = false
        } else {
            cartBottomView.isHidden = true
        }
        BQBookingModel.shared.totalProducts = cartCount
        BQBookingModel.shared.totalAmount = String(totalPrice)
        self.lblPrice.text = String(totalPrice)
        
        if totalPrice == 0 {
            if let navController = self.navigationController {
                if let viewControllers: [UIViewController] = navController.viewControllers {
                    for aViewController in viewControllers {
                        if aViewController is SSASideMenu {
                            self.navigationController!.popToViewController(aViewController, animated: true)
                        }
                    }
                }
            }
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Add a note to your order" {
            textView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Add a note to your order"
            BQBookingModel.shared.specialInstruction = nil
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let txtString = textView.text else {
            return false
        }
        let finalString = (txtString as NSString).replacingCharacters(in: range, with: text)
        BQBookingModel.shared.specialInstruction = finalString
        
        return true
    }
    
    
}

extension CartViewController: UITableViewDelegate, UITableViewDataSource {
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let view = addNewItemView
//        view?.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50)
//        return view
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfItemsInTable() + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == presenter.numberOfItemsInTable() {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewItemTableViewCell", for: indexPath) as? AddNewItemTableViewCell else {
                return UITableViewCell()
            }
            cell.delegate = self
            return cell
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as? ProductCell else {
            return UITableViewCell()
        }
        cell.leftProductDescription.isHidden = true
        cell.productDescription.isHidden = false
        cell.deleteButton.isHidden = false
        cell.favBtn.isHidden = true
        cell.delegate = self
        cell.discountImage.isHidden = true
        cell.lblDiscount.isHidden = true
        presenter.setupDataTable(view: cell, index: indexPath.row)
        cell.deleteButton.isHidden = false
        return cell
    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 50
//    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 90
//    }
}

extension CartViewController: ProductCellDelegate, AddNewItemProtocol {
    
    func addNewItemAction() {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is SSASideMenu {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func productCellFavButtonTapped(cell: ProductCell, isfavStatus: Bool) {
        
    }
    
    func productCellAddButtonTapped(cell: ProductCell) {
        if let index = productTable.indexPath(for: cell) {
            presenter.updateView(view: cell, index: index.row, add: true)
        }
    }
    func productCellRemoveCategoryButtonTapped(cell: ProductCell) {
        let alertController = UIAlertController(title: "Message", message: "Are you sure you want to delete this item from cart ?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            if let index = self.productTable.indexPath(for: cell) {
                self.presenter.removeCategoryFromTheCart(view: cell, index: index.row, add: true)
            }
        })
        let noAction = UIAlertAction(title: "No", style: .destructive, handler: { (action) -> Void in
            //self.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        //        alertController.view.tintColor = UIColor.red
        self.present(alertController, animated: true, completion: nil)
        
        print("productCellRemoveCategoryButtonTapped")
    }
    
    func productCellDeleteButtonTapped(cell: ProductCell) {
//        let alertController = UIAlertController(title: "Message", message: "Are you sure you want to delete this item from cart ?", preferredStyle: .alert)
//        let yesAction = UIAlertAction(title: "Yes", style: .destructive, handler: { (action) -> Void in
//            if let index = self.productTable.indexPath(for: cell) {
//                self.presenter.updateView(view: cell, index: index.row, add: false)
//            }
//        })
//        let noAction = UIAlertAction(title: "No", style: .cancel, handler: { (action) -> Void in
//            //self.dismiss(animated: true, completion: nil)
//        })
//        alertController.addAction(yesAction)
//        alertController.addAction(noAction)
//        //        alertController.view.tintColor = UIColor.red
//        self.present(alertController, animated: true, completion: nil)
        if let index = self.productTable.indexPath(for: cell) {
            self.presenter.updateView(view: cell, index: index.row, add: false)
        }
    }

}
