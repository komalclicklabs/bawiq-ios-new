//
//  ProfilePictureCell.swift
//  TookanVendor
//
//  Created by Vishal on 31/01/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

protocol ProfilePictureCellDelegate {
    func editProfileAction()
}
class ProfilePictureCell: UITableViewCell {

    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var emailAddress: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var gender: UILabel!
    @IBOutlet weak var phoneNumberTxtField: VSTextField!
    @IBOutlet weak var nationality: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var editScreen: UIButton!
    var delegate: ProfilePictureCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        editProfile.layer.borderWidth = 1
        editProfile.layer.borderColor = UIColor.white.cgColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBOutlet weak var editProfile: UIButton!
    
    @IBAction func editProfileAction(_ sender: UIButton) {
        if let _delegate = delegate {
            _delegate.editProfileAction()
        }
    }
    
}
