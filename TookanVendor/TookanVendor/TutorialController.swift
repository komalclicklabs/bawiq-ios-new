//
//  TutorialController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 07/01/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class TutorialController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    @IBOutlet weak var tutorialCollectionView: UICollectionView!
    @IBOutlet weak var pageControll: UIPageControl!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    
    let arrayOfImagesPd = [IMAGE.tutorialScreenPd1,IMAGE.tutorialScreenPd2,IMAGE.tutorialScreenPd3]
    let arrayOfImagesAp = [IMAGE.tutorialScreenAP1,IMAGE.tutorialScreenAp2,IMAGE.tutorialScreenAP3]
    override func viewDidLoad() {
        super.viewDidLoad()
        nextButton.setTitle(TEXT.NEXT, for: UIControlState.normal)
        nextButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        skipButton.setTitle(TEXT.SKIP, for: UIControlState.normal)
        skipButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NIB_NAME.tutorialCell, for: indexPath) as! TutorialCollectionViewCell
        if APP_DEVICE_TYPE == "5"{
            cell.cellImage.image = arrayOfImagesPd[indexPath.row]
        }else if APP_DEVICE_TYPE == "1" {
            cell.cellImage.image = arrayOfImagesAp[indexPath.row]
        }else{
             cell.cellImage.image = arrayOfImagesPd[indexPath.row]
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: tutorialCollectionView.frame.width, height: tutorialCollectionView.frame.height)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        print(center)
        if let ip = tutorialCollectionView.indexPathForItem(at: center) {
            self.pageControll.currentPage = ip.row
            if ip.row == 2{
                nextButton.setTitle(TEXT.DONE , for: UIControlState.normal)
                nextButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
            }else{
                nextButton.setTitle(TEXT.NEXT, for: UIControlState.normal)
                nextButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
            }
            print(ip.row)
        }
    }
  
    @IBAction func nextButtonAction(_ sender: AnyObject) {
        if pageControll.currentPage != 2{
            tutorialCollectionView.scrollToItem(at: IndexPath(row: pageControll.currentPage + 1, section: 0), at: UICollectionViewScrollPosition.centeredHorizontally, animated: true )
        }else{
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: NOTIFICATION_OBSERVER.showLowerView), object: nil)
            }
        }
    }
    
    @IBAction func skipButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue:  NOTIFICATION_OBSERVER.showLowerView), object: nil)
        }
    }
    

}
