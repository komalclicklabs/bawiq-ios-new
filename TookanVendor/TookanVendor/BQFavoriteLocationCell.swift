//
//  BQFavoriteLocationCell.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 07/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

protocol BQFavoriteLocationCellDelegate: class {
    func deleteAddressFromAccount(index: Int)
    func editAddress(cell: UITableViewCell)
}

class BQFavoriteLocationCell: UITableViewCell {
    @IBOutlet weak var btnEditAddress: UIButton!
    @IBOutlet weak var lblAddress4: UILabel!
    @IBOutlet weak var lblAddress3: UILabel!
    @IBOutlet weak var lblAddress2: UILabel!
    @IBOutlet weak var lblAddress1: UILabel!
    @IBOutlet weak var lblAddressType: UILabel!
    @IBOutlet weak var locIcon: UIImageView!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var floorNo: UILabel!
    @IBOutlet weak var doorNo: UILabel!
    @IBOutlet weak var streetNo: UILabel!
    
    
    weak var delegate: BQFavoriteLocationCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnEditAddress.isHidden = false
        bgView.layer.cornerRadius = 15.0
        
    }
    @IBAction func deleteButtonAction(_ sender: UIButton) {
        self.delegate?.deleteAddressFromAccount(index: sender.tag)
        
    }
    
    @IBAction func editAddressAction(_ sender: Any) {
        self.delegate?.editAddress(cell: self)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
