//
//  AddressListCell.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 11/30/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

protocol AddressCellDelegate: class {
    func shouldChange(text: String, string: String, cell: UITableViewCell) -> Bool
}

class AddressListCell: UITableViewCell {

    @IBOutlet weak var addressTxtField: UITextField!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var imageIcon: UIImageView!
    
    weak var delegate: AddressCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addressTxtField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension AddressListCell: UITextFieldDelegate {
    // MARK: - Textfield delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let txtString = textField.text else {
            return false
        }
        let finalString = (txtString as NSString).replacingCharacters(in: range, with: string)
        return self.delegate?.shouldChange(text: finalString, string: string, cell: self) ?? false
    }
}
