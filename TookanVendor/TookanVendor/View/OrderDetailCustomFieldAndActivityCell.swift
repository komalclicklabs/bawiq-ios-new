//
//  OrderDetailCustomFieldAndActivityCell.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 12/16/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class OrderDetailCustomFieldAndActivityCell: UITableViewCell {

    let widthForCircleToShow: CGFloat = 25.0
    let leadingOfUpperToShowCircle: CGFloat = 15.0

    @IBOutlet weak var bottomLabelBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var upperHeaderTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var upperHeaderLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var circleWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var dottedLineView: UIView!
    @IBOutlet weak var cellInfoLabel: UILabel!
    @IBOutlet weak var upperheaderLabel: UILabel!
    @IBOutlet weak var circleStatusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dottedLineView.backgroundColor = UIColor(patternImage:#imageLiteral(resourceName: "iconPattern"))
        
        /******* circle status label *****////
        self.circleStatusLabel.setCornerRadius(radius: 12.5)
        self.circleStatusLabel.setBorder(borderColor:COLOR.PLACEHOLDER_COLOR)
        circleStatusLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        circleStatusLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        // Initialization code
        
        self.backView.backgroundColor = COLOR.popUpColor
        self.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
    }
    
    func setCellForCustomField(row:Int) {
        
        if row == 0 {
            self.upperHeaderTopConstraint.constant = 15
            self.bottomLabelBottomConstraint.constant = 15

        }else{
        self.upperHeaderTopConstraint.constant = 0
        self.bottomLabelBottomConstraint.constant = 15
        }
        
        /*****- upper header label ****/
        self.upperheaderLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        self.upperheaderLabel.textColor = COLOR.PLACEHOLDER_COLOR
        self.upperheaderLabel.text = "Name"

        /*****- lower  label ****/

        self.cellInfoLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
        self.cellInfoLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        self.cellInfoLabel.text = "Dinesh"

        
        showOrHideStatusCircle(hide: true)
    }
    
    func setCellForActivityTimeLine(isHeaderCell:Bool,isLastCell:Bool, creationTime:String, stateDescription:String) {
        
        if isHeaderCell == true {
            /*****- upper header label ****/
            self.upperheaderLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
            self.upperheaderLabel.textColor = COLOR.PLACEHOLDER_COLOR
            self.upperheaderLabel.text = TEXT.HISTORY
            self.cellInfoLabel.text = ""
            self.upperHeaderTopConstraint.constant = 15
            self.bottomLabelBottomConstraint.constant = 5
            showOrHideStatusCircle(hide: true)
        } else {
            self.upperHeaderTopConstraint.constant = 0
            self.bottomLabelBottomConstraint.constant = 15
            /*****- upper header label ****/
            self.upperheaderLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
            self.upperheaderLabel.textColor = COLOR.SPLASH_TEXT_COLOR
            self.upperheaderLabel.text = stateDescription
            /*****- lower  label ****/
            self.cellInfoLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.small)
            self.cellInfoLabel.textColor = COLOR.SPLASH_TEXT_COLOR
            self.cellInfoLabel.text = creationTime.convertDateFromUTCtoLocal(input: "yyyy-MM-dd'T'HH:mm:ss.000Z", output: "dd MMM yyyy, hh:mm a")
            showOrHideStatusCircle(hide: false)
            let index = stateDescription.index(stateDescription.startIndex, offsetBy: 1)
            self.circleStatusLabel.text = stateDescription.substring(to: index)
            if isLastCell == true {
                self.dottedLineView.isHidden = true
            }else{
                self.dottedLineView.isHidden = false
            }
        }
    }
    
    func showOrHideStatusCircle(hide:Bool){
        
        if hide == true {
        self.dottedLineView.isHidden = true
        self.upperHeaderLeadingConstraint.constant = 0
        self.circleWidthConstraint.constant = 0
        self.layoutIfNeeded()
        }else{
            self.dottedLineView.isHidden = false
            self.upperHeaderLeadingConstraint.constant = leadingOfUpperToShowCircle
            self.circleWidthConstraint.constant = widthForCircleToShow
            self.layoutIfNeeded()
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
