//
//  BQContactUsVC.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 18/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit
import MessageUI

class BQContactUsVC: UIViewController {
    @IBOutlet weak var tblView: UITableView!
    var contactModel = BQContactUsmodel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar()
        self.setupTableView()
    }
    
    private func setupTableView() {
        self.tblView.layer.cornerRadius = 5.0
        
        self.tblView.register(UINib(nibName: "BQContactUsCell", bundle: nil), forCellReuseIdentifier: "BQContactUsCell")
        self.getContactUsDetails()
        
    }
    
    private func getContactUsDetails() {
        
        HTTPClient.makeConcurrentConnectionWith(method: .GET, para: nil, extendedUrl: "get_contact_details") { (responseObject, error, _, statusCode) in
            
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            
            guard statusCode == .some(STATUS_CODES.SHOW_DATA) else {
                return
            }
            print(responseObject ?? "")
            if let dataRes = responseObject as? [String:Any] {
                if let data = dataRes["data"] as? [String: Any], let contactUS = data["contactUs"] as? [String: Any] {
                    print(data)
                    
                    self.contactModel = BQContactUsmodel.init(param: contactUS)
                    self.tblView.reloadData()
                }
            }
        }
    }
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        //Use for showing two right bar buttons on navigation bar
        
        let navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "Contact Us", leftButtonImage: #imageLiteral(resourceName: "menu"), rightButtonImage: nil, secondRightButtonImage: nil)
            , leftButtonAction: {[weak self] in
                self?.presentLeftMenuViewController()
            }, rightButtonAction: nil,
               rightButtonSecondAction: nil
        )
        navigationBar.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
        
    }
}

extension BQContactUsVC: UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 146
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BQContactUsCell", for: indexPath) as? BQContactUsCell else {
            return UITableViewCell()
        }
        if indexPath.row == 0 {
            cell.lblTitle.text = "Contact Us"
            cell.lblDescription.text = contactModel.contact_number
            cell.imgIcon.image = #imageLiteral(resourceName: "contactCall")
        } else if indexPath.row == 1 {
            cell.lblTitle.text = "Email Us"
            cell.lblDescription.text = contactModel.contact_email
            cell.imgIcon.image = #imageLiteral(resourceName: "contactMail")
        } else if indexPath.row == 2 {
            cell.lblTitle.text = "Website"
            cell.lblDescription.text = contactModel.website_link
            cell.imgIcon.image = #imageLiteral(resourceName: "contactLogo")
        } else if indexPath.row == 3 {
            cell.lblTitle.text = "Message Us"
            cell.lblDescription.text = contactModel.contact_number
            cell.imgIcon.image = #imageLiteral(resourceName: "messageRedIcon")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            var phone = contactModel.contact_number
            phone = phone?.replacingOccurrences(of: "[ |()-]", with: "", options: [.regularExpression])
            print(phone as Any)
            if let url = URL(string: "tel://\(phone ?? "")"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        case 1:
            sendEmail(text: contactModel.contact_email!)
        case 2:
            self.openWebSiteURl(url: "http://\(contactModel.website_link ?? "")")
//            if let url = URL(string: "\(contactModel.website_link ?? "")") {
//                if #available(iOS 10.0, *) {
//                    UIApplication.shared.open(url, options: [:])
//                } else {
//                    UIApplication.shared.openURL(url)
//                }
//            }
        case 3:
            if (MFMessageComposeViewController.canSendText()) {
                let controller = MFMessageComposeViewController()
                controller.body = ""
                guard let number = contactModel.contact_number else {
                    return
                }
                controller.recipients = [number]
                controller.messageComposeDelegate = self
                self.present(controller, animated: true, completion: nil)
            }
        default:
            break
        }
        
    }
    
    func openWebSiteURl(url: String) {
        guard let url = URL(string: url) else {
            return //be safe
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    func sendEmail(text: String) {
        let mailComposeViewController = configuredMailComposeViewController(text: text)
        if MFMailComposeViewController.canSendMail()
        {
            self.present(mailComposeViewController, animated: true, completion: nil)
        }
    }
    func configuredMailComposeViewController(text: String) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setToRecipients([text])
        mailComposerVC.setSubject("")
        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
    
}
