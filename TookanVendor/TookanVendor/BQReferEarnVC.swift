//
//  BQReferEarnVC.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 14/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQReferEarnVC: UIViewController {
    
    var navigationBar:NavigationView!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var bgPromoCode: UIView!
    @IBOutlet weak var tapToApplyAction: UILabel!
    @IBOutlet weak var lblPromoCode: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var shareText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar()
        self.setupUICompenents()
        
        shareText.text = "Promotional Vouchers to be available soon."
        
        // To be changed when client wants refer and earn functionality
        btnOK.isHidden = true
        bgPromoCode.isHidden = true
        imgIcon.isHidden = true
        tapToApplyAction.isHidden = true
        lblPromoCode.isHidden = true
    
    }
    
    @IBAction func tapToApplyAction(_ sender: Any) {
        
        // text to share
        //let text = "This is some text that I want to share."
        
        // set up activity view controller
        
        
    }
    
    private func setupUICompenents() {
        btnOK.layer.cornerRadius = 24
        
        bgPromoCode.layer.borderColor = UIColor.white.cgColor
        bgPromoCode.layer.borderWidth = 1.0
        bgPromoCode.layer.cornerRadius = 10.0
        
//        let yourViewBorder = CAShapeLayer()
//        yourViewBorder.strokeColor = UIColor.white.cgColor
//        yourViewBorder.lineDashPattern = [7, 1]
//        yourViewBorder.frame = CGRect(x: 0, y: 0, width: bgPromoCode.frame.width - 50, height: bgPromoCode.frame.height)//bgPromoCode.frame
//        yourViewBorder.fillColor = nil//UIColor.white.cgColor
//        //yourViewBorder.path = UIBezierPath(rect: CGRect(x: 0, y: 0, width: bgPromoCode.frame.width, height: bgPromoCode.frame.height)).cgPath
//        yourViewBorder.path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: bgPromoCode.frame.width - 50, height: bgPromoCode.frame.height), cornerRadius: 15).cgPath
//        yourViewBorder.cornerRadius = 15.0
//        self.bgPromoCode.layer.addSublayer(yourViewBorder)
//        //self.bgPromoCode.layer.masksToBounds = true
//        self.imgIcon.bringSubview(toFront: self.bgPromoCode)
        
        if let referCode = Vendor.current!.refferal_code {
            lblPromoCode.text = referCode
        } else {
            
        }
        
        
        
    }
    @IBAction func okButtonAction(_ sender: Any) {
        if let code = lblPromoCode.text {
            let textToShare = ["Download Bawiq now and use my referral code \(code) to get AED 10 credited to your b wallet."]
            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        
        navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "Promotional Vouchers", leftButtonImage: #imageLiteral(resourceName: "menu"), rightButtonImage: nil, secondRightButtonImage: nil)
            , leftButtonAction: {[weak self] in
                self?.presentLeftMenuViewController()
            }, rightButtonAction: nil,
               rightButtonSecondAction: nil
        )
        
        // To be changed when client wants refer and earn functionality
        
//        navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "Refer & Earn", leftButtonImage: #imageLiteral(resourceName: "menu"), rightButtonImage: nil, secondRightButtonImage: nil)
//            , leftButtonAction: {[weak self] in
//                self?.presentLeftMenuViewController()
//            }, rightButtonAction: nil,
//               rightButtonSecondAction: nil
//        )
        navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
        
    }
}
