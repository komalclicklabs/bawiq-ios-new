//
//  BQFeedbackReviewVC.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 14/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQFeedbackReviewVC: UIViewController {

    var navigationBar:NavigationView!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnSave: UIButton!
    var selectedRating = 1
    var userComplainText: String?
    var job_id: Int?
    var arrComplaints = [String]()
    var customerComment: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setNavigationBar()
        self.setupUIComponents()
        self.setupTableView()
    }
    
    @IBAction func saveFunctionAction(_ sender: UIButton) {
        let params = getParamsToDeleteLocation()
        ActivityIndicator.sharedInstance.showActivityIndicator()
        HTTPClient.makeConcurrentConnectionWith(method: .POST, para: params, extendedUrl: "rate_job") { (responseObject, error, _, statusCode) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            guard statusCode == .some(STATUS_CODES.SHOW_DATA) else {
                return
            }
            print(responseObject ?? "")
            if let dataRes = responseObject as? [String:Any] {
                if let data = dataRes["data"] as? [String:Any] {
                    print(data)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    private func getParamsToDeleteLocation() -> [String: Any] {
        
        var dicRequest = [String: Any]()
        dicRequest["app_access_token"] = Vendor.current!.appAccessToken
        dicRequest["job_id"] = job_id
        dicRequest["customer_rating"] = selectedRating
        if selectedRating < 3 {
            dicRequest["text_of_complain"] = userComplainText
        }
        
        dicRequest["customer_comment"] = customerComment
        return dicRequest
    }
    
    
    
    private func setupUIComponents() {
        self.btnSave.layer.cornerRadius = 26.0
    }
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        //Use for showing two right bar buttons on navigation bar
        
        navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "Rate Service", leftButtonImage: #imageLiteral(resourceName: "BackButtonWhite"), rightButtonImage: nil, secondRightButtonImage: nil)
            , leftButtonAction: {[weak self] in
                self?.navigationController?.popViewController(animated: true)
            }, rightButtonAction: nil,
               rightButtonSecondAction: nil
        )
        navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
    }


    private func setupTableView() {
        self.tblView.register(UINib(nibName: "BQFeedbackRatingCell", bundle: nil), forCellReuseIdentifier: "BQFeedbackRatingCell")
        self.tblView.register(UINib(nibName: "BQFeedbackImageCell", bundle: nil), forCellReuseIdentifier: "BQFeedbackImageCell")
        self.tblView.register(UINib(nibName: "BQFeedBackCommentCell", bundle: nil), forCellReuseIdentifier: "BQFeedBackCommentCell")
        self.tblView.register(UINib(nibName: "BQComplainCell", bundle: nil), forCellReuseIdentifier: "BQComplainCell")
        self.tblView.delegate = self
        self.tblView.dataSource = self
    }
}

extension BQFeedbackReviewVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 140
        } else if indexPath.row == 1 {
            return 100
        } else if indexPath.row == 2 {
            if selectedRating > 2 {
                return 0
            }
            return 133
        }
        
        return 117
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "BQFeedbackRatingCell", for: indexPath) as? BQFeedbackRatingCell else {
                return UITableViewCell()
            }
            cell.delegate = self
            return cell
        } else if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "BQFeedbackImageCell", for: indexPath) as? BQFeedbackImageCell else {
                return UITableViewCell()
            }
            return cell
        } else if indexPath.row == 2 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "BQComplainCell", for: indexPath) as? BQComplainCell else {
                return UITableViewCell()
            }
            cell.delegate = self
            if selectedRating > 2 {
                cell.bgView.isHidden = true
            } else {
                cell.bgView.isHidden = false
            }
            return cell
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BQFeedBackCommentCell", for: indexPath) as? BQFeedBackCommentCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        return cell
    }
}

extension BQFeedbackReviewVC: BQComplainCellDelegate {
    func selectedComplainRating(isSelected: Bool, rating: String) {
        if isSelected == true {
            arrComplaints.append(rating)
        } else {
            arrComplaints = arrComplaints.filter{$0 != rating}
        }
        userComplainText = arrComplaints.joined(separator: ", ")
    }
}

extension BQFeedbackReviewVC: BQFeedbackRatingCellDelegate, BQFeedBackCommentCellDelegate {
    func feedbackComment(textReview: String) {
        print(textReview)
        customerComment = textReview
    }
    
    func ratingProvideToDriver(textReview: String, ratings: Int) {
        
    }
    
    func ratingCountForServiceProvider(ratings: Int) {
        print(ratings)
        selectedRating = ratings
        tblView.beginUpdates()
        tblView.reloadData()
        tblView.endUpdates()
    }
    
    
}



