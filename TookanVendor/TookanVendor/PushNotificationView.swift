//
//  PushNotificationView.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 23/12/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class PushNotificationView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var notificationMessage: UILabel!
    @IBOutlet var appIcon: UIImageView!
    @IBOutlet var line: UILabel!
    
    var action:(()->Void)?
    
    static var notiFicationView : PushNotificationView?
    
    override func awakeFromNib() {
        notificationMessage.textColor = COLOR.SPLASH_TEXT_COLOR
        titleLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        notificationMessage.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
        titleLabel.font = UIFont(name: FONT.ultraLight, size: FONT_SIZE.medium)
        titleLabel.text = APP_NAME
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 10
        self.layer.borderColor = COLOR.SPLASH_LINE_COLOR.cgColor
        self.layer.borderWidth = 0.5

        self.appIcon.layer.cornerRadius = 4.0
        self.appIcon.layer.masksToBounds = true
        self.line.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        
        
    }
    
    
    //MARK: STATIC FUNCTION TO SHOW THE NOTIFICATION POPUP
    
    static func showPush( message:String, actionOnClick:@escaping ()->Void) -> PushNotificationView{
        UIApplication.shared.setStatusBarHidden(true, with: .fade)
        let viewToBeloaded = Bundle.main.loadNibNamed(NIB_NAME.pushNotificationView, owner: self, options: nil)?[0] as? PushNotificationView
        let size = message.heightWithConstrainedWidth(width: SCREEN_SIZE.width - 60, font:  UIFont(name: FONT.light, size: FONT_SIZE.medium)!)
        let titleSize = APP_NAME.heightWithConstrainedWidth(width: SCREEN_SIZE.width - 60, font:  UIFont(name: FONT.light, size: FONT_SIZE.medium)!)
        viewToBeloaded?.alpha = 0
        viewToBeloaded!.frame = CGRect(x: 20, y: -(size.height + titleSize.height + 40), width: (UIApplication.shared.keyWindow?.frame.width)! - 60, height: size.height + titleSize.height + 40)
        viewToBeloaded?.notificationMessage.text = message
        viewToBeloaded?.action = actionOnClick
        return viewToBeloaded!
    }

    // FUNCTION TO HIDE THE POPUP
    func hideNotification(){
        UIView.animate(withDuration: 0.5, animations: {
            self.alpha = 0
            self.frame = CGRect(x: 20, y: -60, width: (UIApplication.shared.keyWindow?.frame.width)! - 40, height: self.frame.height)
            }) { (true) in
                UIApplication.shared.setStatusBarHidden(false, with: .fade)
                self.removeFromSuperview()
        }
    }
    
    @IBAction func tapAction(_ sender: AnyObject) {
        print("notificationTaped")
        action!()
        hideNotification()
    }
    
    @IBAction func cancelNotificationButton(_ sender: UIButton) {
        hideNotification()
    }
    
}
