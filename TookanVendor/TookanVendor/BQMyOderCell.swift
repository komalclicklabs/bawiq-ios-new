//
//  BQMyOderCell.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 10/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit
import QuartzCore

protocol BQMyOderCellDelegate: class {
    func showOrderDetails(senderTag: Int)
}

class BQMyOderCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblItemnameAndQuantity: UILabel!
    @IBOutlet weak var lblOderID: UILabel!
    @IBOutlet weak var btnViewDetails: UIButton!
    weak var delegate: BQMyOderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        lblStatus.layer.masksToBounds = true
        lblStatus.layer.cornerRadius = 15.0
    }

    @IBAction func showOrderDetailsAction(_ sender: UIButton) {
        self.delegate?.showOrderDetails(senderTag: sender.tag)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
