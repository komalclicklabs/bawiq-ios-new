//
//  BQWalletCell.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 25/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQWalletCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
