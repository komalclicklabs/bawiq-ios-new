//
//  BQUserSavedLocationsVC.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 07/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit


protocol BQUserSavedLocationsVCDelegate: class {
    func selectedAddress(address: BQFavoriteAddressModel)
}



class BQUserSavedLocationsVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnAddNewLocation: UIButton!
    @IBOutlet weak var btnSearchLocation: UIButton!
    @IBOutlet weak var bgSearchButton: UIView!
    @IBOutlet weak var noLocationFoundLbl: UILabel!
    
    var navigationBar:NavigationView!
    var arrSavedAddress = [BQFavoriteAddressModel]()
    var isComingFromHome = false
    var isComingFromCart = false
    var isComingFromProfile = false
    
    weak var delegate: BQUserSavedLocationsVCDelegate? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.registerTableViewCells()
        self.setupUIProperties()
        self.setNavigationBar()
        noLocationFoundLbl.isHidden = false
        
        self.tblView.delegate = self
        self.tblView.dataSource = self
        
        
        if isComingFromHome == true {
            bgSearchButton.isHidden = false
        } else if isComingFromCart == true {
            bgSearchButton.isHidden = true
        } else if isComingFromProfile == true {
            bgSearchButton.isHidden = false
        }
        
        tblView.estimatedRowHeight = 110
        tblView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.arrSavedAddress.removeAll()
        let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
        if !isGuestUser {
            self.getSavedLocations()
        }
    }
    
    
    private func setupUIProperties() {
        //SetUp UI
        self.bgSearchButton.isHidden = true
        btnAddNewLocation.isHidden = false
//        if self.isComingFromHome == true {
//            self.bgSearchButton.isHidden = false
//            btnAddNewLocation.isHidden = true
//        } else if isComingFromCart == true {
//            self.bgSearchButton.isHidden = true
//            btnAddNewLocation.isHidden = false
//        }
        
        btnSearchLocation.layer.cornerRadius = 12.0
        btnSearchLocation.layer.borderColor = UIColor.lightGray.cgColor
        btnSearchLocation.layer.borderWidth = 1.0
    }
    
    
    
    private func registerTableViewCells() {
        tblView.registerCellWith(nibName: "BQFavoriteLocationCell", reuseIdentifier: "BQFavoriteLocationCell")
    }
    
    
    func deleteFavoriteLocation(addressModel: BQFavoriteAddressModel) {
        
        let alertController = UIAlertController(title: "Message", message: "Are you sure you want to delete this saved location ?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .destructive, handler: { (action) -> Void in
            
            var params = [String: Any]()
            let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
            if !isGuestUser {
                params = self.getParamsToDeleteLocation()
                params["fav_id"] = addressModel.fav_id
            }
            
            //delete_fav_location
            //app_access_token
            //fav_id
            
            ActivityIndicator.sharedInstance.showActivityIndicator()
            HTTPClient.makeConcurrentConnectionWith(method: .POST, para: params, extendedUrl: "delete_fav_location") { (responseObject, error, _, statusCode) in
                
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                
                guard statusCode == .some(STATUS_CODES.SHOW_DATA) else {
                    return
                }
                print(responseObject ?? "")
                if let dataRes = responseObject as? [String:Any] {
                    self.getSavedLocations()
                }
            }

        })
        let noAction = UIAlertAction(title: "No", style: .cancel, handler: { (action) -> Void in
            //self.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    private func getSavedLocations() {
        arrSavedAddress.removeAll()
        var params = [String: Any]()
        let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
        if !isGuestUser {
            params = getParamsToDeleteLocation()
        }
    
        ActivityIndicator.sharedInstance.showActivityIndicator()
        HTTPClient.makeConcurrentConnectionWith(method: .POST, para: params, extendedUrl: "get_fav_location") { (responseObject, error, _, statusCode) in
            
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            
            guard statusCode == .some(STATUS_CODES.SHOW_DATA) else {
                return
            }
            print(responseObject ?? "")
            if let dataRes = responseObject as? [String:Any] {
                if let data = dataRes["data"] as? [String:Any] {
                    if let favLocations = data["favLocations"] as? [[String: Any]] {
                        print(favLocations)
                        
                        for item in favLocations {
                            let rollData = BQFavoriteAddressModel.init(param: item)
                            self.arrSavedAddress.append(rollData)
                        }
                        self.tblView.reloadData()
                        
                    }
                    print(data)
                    
                }
            }
        }
    }
    
    // MARK: - Methods
    
    private func getParamsToDeleteLocation() -> [String: Any] {
        return [
            "app_access_token": Vendor.current!.appAccessToken!
        ]
    }
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        //Use for showing two right bar buttons on navigation bar
        
        var backImage = UIImage()
        if isComingFromCart == true ||  isComingFromHome == true || isComingFromProfile == true {
            backImage = #imageLiteral(resourceName: "BackButtonWhite")
        } else {
            backImage = #imageLiteral(resourceName: "menu")
        }
        
        navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "My Saved Location(s)", leftButtonImage: backImage, rightButtonImage: nil, secondRightButtonImage: nil)
            , leftButtonAction: {[weak self] in
                self?.backAction()
            }, rightButtonAction: nil,
               rightButtonSecondAction: nil
        )
        navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
    }
    
    @IBAction func searchButtonPressed(_ sender: UIButton) {
        if let vc = UIStoryboard(name: "NLevel", bundle: Bundle.main).instantiateViewController(withIdentifier: "mapViewNlevel") as? MapViewController {
            if isComingFromCart == true {
                vc.isComingFromCart = true
            } else if isComingFromHome == true {
                vc.isComingFromHome = true
            } else if isComingFromProfile == true {
                vc.isComingFromProfile = true
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func addNewlocation(_ sender: UIButton) {
        if let vc = UIStoryboard(name: "NLevel", bundle: Bundle.main).instantiateViewController(withIdentifier: "mapViewNlevel") as? MapViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // MARK: - IBAction
    func backAction() {
        if isComingFromCart || isComingFromHome || isComingFromProfile {
            self.navigationController?.popViewController(animated: true)
        } else {
            sideMenuViewController?.presentLeftMenuViewController()
        }
    }
}


extension BQUserSavedLocationsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrSavedAddress.count > 0 {
            if self.isComingFromHome == true {
                self.bgSearchButton.isHidden = false
//                btnAddNewLocation.isHidden = true
            } else if isComingFromCart == true {
                self.bgSearchButton.isHidden = true
//                btnAddNewLocation.isHidden = false
            } else if isComingFromProfile == true {
                self.bgSearchButton.isHidden = false
//                btnAddNewLocation.isHidden = false
            }
            noLocationFoundLbl.isHidden = true
            return arrSavedAddress.count
        } else {
            if self.isComingFromHome == true {
                self.bgSearchButton.isHidden = true
//                btnAddNewLocation.isHidden = false
            } else if isComingFromCart == true {
                self.bgSearchButton.isHidden = true
//                btnAddNewLocation.isHidden = false
            } else if isComingFromProfile == true {
                self.bgSearchButton.isHidden = true
//                btnAddNewLocation.isHidden = false
            }
            noLocationFoundLbl.isHidden = false
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let addressCell = tableView.dequeueReusableCell(withIdentifier: "BQFavoriteLocationCell") as! BQFavoriteLocationCell
        
        let cellDataModel: BQFavoriteAddressModel = self.arrSavedAddress[indexPath.row]
        
        var tempAddString = [String]()
        let buildingNo = cellDataModel.building_number ?? ""
        let floorNo = cellDataModel.floor_number ?? ""
        let doorNo = cellDataModel.door_number ?? ""
        let streetNo = cellDataModel.street_number ?? ""
        if !buildingNo.isEmpty {
            tempAddString.append(buildingNo)
            BQBookingModel.shared.apartmentNo = buildingNo
            addressCell.lblAddress4.text = "Building No. : " + buildingNo  //additionalAddressString
        } else {
            addressCell.lblAddress4.isHidden = true
        }
        if !floorNo.isEmpty {
            tempAddString.append(floorNo)
            BQBookingModel.shared.floorNo = floorNo
            addressCell.floorNo.text = "Floor No. : " + floorNo
        } else {
            addressCell.floorNo.isHidden = true
        }
        if !doorNo.isEmpty {
            tempAddString.append(doorNo)
            BQBookingModel.shared.doorNo = doorNo
            addressCell.doorNo.text = "Door No. : " + doorNo
        } else {
            addressCell.doorNo.isHidden = true
        }
        if !streetNo.isEmpty {
            tempAddString.append(streetNo)
            BQBookingModel.shared.streetNo = streetNo
            addressCell.streetNo.text = "Street No. " + streetNo
        } else {
            addressCell.streetNo.isHidden = true
        }
        
        let additionalAddressString = tempAddString.joined(separator: "\n")
        BQBookingModel.shared.addressLine4 = additionalAddressString
        
        if let landmark = cellDataModel.landmark {
            addressCell.lblAddress1.text = "Landmark : " + landmark
        } else {
            addressCell.lblAddress1.isHidden = true
        }
        
        if let area = cellDataModel.address {
            addressCell.lblAddress2.text = "Area : " +  area
        } else {
            addressCell.lblAddress2.isHidden = true
        }
        
        if let zipCode = cellDataModel.postal_code {
            addressCell.lblAddress3.text = "Zip Code : " + zipCode
        } else {
            addressCell.lblAddress3.isHidden = true
        }
        
        addressCell.lblAddressType.text = cellDataModel.locType
        if cellDataModel.locType == "Home" {
            addressCell.locIcon.image = #imageLiteral(resourceName: "homeRed")
        } else if cellDataModel.locType == "Work" {
            addressCell.locIcon.image = #imageLiteral(resourceName: "workRed")
        } else {
            addressCell.locIcon.image = #imageLiteral(resourceName: "pinRed")
        }
        
        addressCell.btnDelete.tag = indexPath.row
        addressCell.delegate = self
        return addressCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cellDataModel: BQFavoriteAddressModel = self.arrSavedAddress[indexPath.row]
        self.delegate?.selectedAddress(address: cellDataModel)
        self.navigationController?.popViewController(animated: true)
    }
}


extension BQUserSavedLocationsVC: BQFavoriteLocationCellDelegate {
    func deleteAddressFromAccount(index: Int) {
        print("deleteAddressFromAccount")
        if let cellDataModel: BQFavoriteAddressModel = self.arrSavedAddress[index] {
             self.deleteFavoriteLocation(addressModel: cellDataModel)
        }
       
    }
    
    
    func editAddress(cell: UITableViewCell) {
        if let cellIndex = self.tblView.indexPath(for: cell) {
            print("======", cellIndex.row)
            if let vc = UIStoryboard(name: "Booking", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQAddUserLocation") as? BQAddUserLocation {
                let cellDataModel: BQFavoriteAddressModel = self.arrSavedAddress[cellIndex.row]
                vc.buildingNumber = cellDataModel.building_number ?? ""
                vc.floorNumber = cellDataModel.floor_number ?? ""
                vc.doorNumber = cellDataModel.door_number ?? ""
                vc.streetNumber = cellDataModel.street_number ?? ""
                vc.landmark = cellDataModel.landmark ?? ""
                vc.addressPart1 = cellDataModel.address ?? ""
                vc.addressPart3 = cellDataModel.postal_code ?? ""
                if let lat = cellDataModel.latitude,let long = cellDataModel.longitude {
                    vc.latitude = Double(lat)!
                    vc.longitude = Double(long)!
                }
                if let locType = cellDataModel.locType {
                    if locType == "Home" {
                        vc.locType = 0
                    } else if locType == "Work" {
                        vc.locType = 1
                    } else {
                        vc.locType = 2
                    }
                }
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}


