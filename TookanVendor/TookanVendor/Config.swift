//
//  Config.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 15/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class Config: NSObject {
    
}
//com.product.taxi
//com.clicklabs.TookanVendor

//1 FOR YES / 0 FOR NO


let APP_NAME = "Bawiq"
let CALENDELY_LINK = "https://www.apple.com"
let TERMS_OF_SERVICE_LINK = "http://yelo.red/privacy-policy/"
let PRIVACY_POLICY_LINK = "http://yelo.red/privacy-policy/"
let menuHeaderType = 1 //1 For simple top bar //0 for topBarWithImage
var APP_DEVICE_TYPE: String {
    
    if UserDefaults.standard.value(forKey: "deviceType") != nil && !isWhiteLabel {
        return UserDefaults.standard.value(forKey: "deviceType") as! String
    } else {
        return "1"
    }
}
let originalDeviceType = "1"
let isWhiteLabel = true


/* #################
 1 for enterprise build
 3 for Appointment build
 5 for Deliveries build
 ################# */

let APP_VERSION = 104
let vehicleNotes = ["fleet_vehicle_color", "fleet_vehicle_description", "fleet_license"]


struct allAppDeviceType{
    static var nexTask = "69"
}

struct NexTaskVariable{
    static var package_Type: String = "Package_Type"
    static var Quantity: String = "Quantity"
    static var Distance: String = "Distance"
}

struct SERVER {
    static let dev = "http://dev-api.bawiq.in/"
    //"http://13.127.32.146:3001/"//"https://api.yelo.red/"//"https://api.yelo.red/"//"https://beta-api.yelo.red/"//"https://test-api.yelo.red/"//"https://api.yelo.red/"//"https://api.yelo.red/"//"http://test.tookanapp.com:8033/"
    static let test = "http://13.126.65.71:3002/"//"https://api.yelo.red/"//"https://api.yelo.red/"//"https://beta-api.yelo.red/"//"https://test-api.yelo.red/"//"https://api.yelo.red/"//"https://test-api.yelo.red/"//"https://api.yelo.red/"//"https://api.yelo.red/"//"http://test-api.yelo.red/"//"http://test.tookanapp.com:8033/"
    static let live = "https://api.bawiq.com/"
    //"http://13.127.32.146:3001/"//"http://13.127.32.146:3001"//"https://api.yelo.red/"//"https://api.yelo.red/"//"https://beta-api.yelo.red/"//"https://test-api.yelo.red/"//"https://api.yelo.red/"//"https://test-api.yelo.red/"//"https://api.yelo.red/"//"https://api.yelo.red/"//"http://test-api.yelo.red/"//"https://api.tookanapp.com/"
    static let beta = "http://13.127.32.146:3001/"//"https://api.yelo.red/"//"https://api.yelo.red/"//"https://beta-api.yelo.red/"//"https://test-api.yelo.red/"//"https://api.yelo.red/"//"https://test-api.yelo.red/"//"https://api.yelo.red/"//"https://api.yelo.red/"//"http://test-api.yelo.red/"//"https://node1-api.tookanapp.com:444/"
}

//struct SERVER {
//    static let dev = "http://13.126.65.71:3002/"//"https://api.yelo.red/"//"https://api.yelo.red/"//"https://beta-api.yelo.red/"//"https://test-api.yelo.red/"//"https://api.yelo.red/"//"https://api.yelo.red/"//"http://test.tookanapp.com:8033/"
//    static let test = "http://13.126.65.71:3002/"//"https://api.yelo.red/"//"https://api.yelo.red/"//"https://beta-api.yelo.red/"//"https://test-api.yelo.red/"//"https://api.yelo.red/"//"https://test-api.yelo.red/"//"https://api.yelo.red/"//"https://api.yelo.red/"//"http://test-api.yelo.red/"//"http://test.tookanapp.com:8033/"
//    static let live = "http://13.126.65.71:3002/"//"http://13.127.32.146:3001"//"https://api.yelo.red/"//"https://api.yelo.red/"//"https://beta-api.yelo.red/"//"https://test-api.yelo.red/"//"https://api.yelo.red/"//"https://test-api.yelo.red/"//"https://api.yelo.red/"//"https://api.yelo.red/"//"http://test-api.yelo.red/"//"https://api.tookanapp.com/"
//    static let beta = "http://13.126.65.71:3002/"//"https://api.yelo.red/"//"https://api.yelo.red/"//"https://beta-api.yelo.red/"//"https://test-api.yelo.red/"//"https://api.yelo.red/"//"https://test-api.yelo.red/"//"https://api.yelo.red/"//"https://api.yelo.red/"//"http://test-api.yelo.red/"//"https://node1-api.tookanapp.com:444/"
//}


struct DEMO_API_KEY {
    static let taxi = "d471d212df85bbc051a6d0a0b383b3276541a65026fdb133a9872cb61d659bb2"
    static let beauty = "6d25ffa97b321a596b46f23a2b24bc1deca632f339808a3ae0004456dbcef85c"
}

struct DEMO_REFRENCE_ID {
    static let taxi = 1
    static let beauty = 1
}

struct APP_STORE {
    static let value = 1
}

let defaultMapType: mapType = mapType.normal

//MARK: FONT SIZE
struct FONT_SIZE {
    static let smallest:CGFloat = 12.0
    static let small:CGFloat = 13.0
    static let medium:CGFloat = 15.0
    static let large:CGFloat = 19.0
    static let buttonTitle:CGFloat = 14.0
    static let extraLarge:CGFloat = 22.0
    static let extraSmall : CGFloat = 7.8
    static let carTypeFontSize: CGFloat = 11
    static let carTimeLabelSize :CGFloat = 10
    static let serviceNameFontSize : CGFloat = 17.0
    static let priceFontSize : CGFloat = 16.0
    static let textfieldSize: CGFloat = 18
    static let superLarge: CGFloat = 27.0
   static let maxFontSize: CGFloat = 36.0
}


struct JOB_STATUS_COLOR {
    static let UNASSIGNED =  UIColor(red: 186/255, green: 186/255, blue: 186/255, alpha: 1.0)
    static let ASSIGNED =  UIColor(red: 245/255, green: 169/255, blue: 70/255, alpha: 1.0)
    static let ACCEPTED =  UIColor(red: 186/255, green: 104/255, blue: 200/255, alpha: 1.0)
    static let STARTED =  UIColor(red: 70/255, green: 149/255, blue: 245/255, alpha: 1.0)
    static let INPROGRESS =  UIColor(red: 22/255, green: 107/255, blue: 211/255, alpha: 1.0)
    static let SUCCESSFUL =  UIColor(red: 102/255, green: 208/255, blue: 42/255, alpha: 1.0)
    static let FAILED =  UIColor(red: 226/255, green: 87/255, blue: 76/255, alpha: 1.0)
    static let DECLINED =  UIColor(red: 203/255, green: 53/255, blue: 41/255, alpha: 1.0)
    static let CANCELLED =  UIColor(red: 154/255, green: 154/255, blue: 154/255, alpha: 1.0)
    static let SCHEDULED = UIColor(red: 245/255, green: 169/255, blue: 70/255, alpha: 1.0)
}

//COLOR
struct COLOR {
    static let THEME_FOREGROUND_COLOR = UIColor(red: 225/255, green: 61/255, blue: 54/255, alpha: 1.0)
    
    static let TITLE_POSITIVE_COLOR = UIColor.white
    static let TITLE_NEGATIVE_COLOR = UIColor(red: 62/255, green: 89/255, blue: 165/255, alpha: 1.0)
    static let BUTTON_BACKGROUND_POSITIVE_COLOR = UIColor(red: 62/255, green: 89/255, blue: 165/255, alpha: 1.0)
    static let BUTTON_BACKGROUND_NEGATIVE_COLOR = UIColor.white
    static let ACTIVE_IMAGE_COLOR =  UIColor(red: 225/255, green: 61/255, blue: 54/255, alpha: 1.0)
    static let shadowColorNew = UIColor(red: 117/255, green: 117/255, blue: 117/255, alpha: 0.22)

    
    //Screen BackGround color
    static let SPLASH_BACKGROUND_COLOR = UIColor.white
    
    //Button Title COLOR
    static let LOGIN_BUTTON_TITLE_COLOR = UIColor.white
    
    //Title Underline color
    static let TITLE_UNDER_LINE_COLOR = UIColor(red: 225/255, green: 61/255, blue: 54/255, alpha: 1.0)
    
    //TEXTColor
    static let App_Green_COLOR = UIColor(red: 13/255, green: 172/255, blue: 104/255, alpha: 1.0)
    static let App_Red_COLOR = UIColor(red: 216/255, green: 33/255, blue: 38/255, alpha: 1.0)
    static let SPLASH_TEXT_COLOR = UIColor(red: 71/255, green: 71/255, blue: 71/255, alpha: 1.0)
    static let SIGNIN_TITLE_TYPE_LABEL = UIColor(red: 144/255, green: 144/255, blue: 144/255, alpha: 1)
    
    //PopUpColor
    static let popUpColor = UIColor.white
    
    static let seperatorColor = UIColor.black.withAlphaComponent(0.6)
    static let IN_ACTIVE_IMAGE_COLOR =  UIColor(red: 178/255, green: 178/255, blue: 178/255, alpha: 1.0)
    static let FILLED_IMAGE_COLOR =  UIColor(red: 71/255, green: 71/255, blue: 71/255, alpha: 1.0)
    static let TRANS_COLOR = UIColor(red: 0, green: 0.0, blue: 0.0, alpha: 0.5)
    static let SPLASH_LINE_COLOR = UIColor(red: 219/255, green: 219/255, blue: 219/255, alpha: 1.0)
    static let PLACEHOLDER_COLOR = UIColor(red: 178/255, green: 178/255, blue: 178/255, alpha: 1.0)
    static let ERROR_COLOR = UIColor(red: 226/255, green: 87/255, blue: 76/255, alpha: 0.9)
    static let SUCCESS_COLOR = UIColor(red: 102/255, green: 208/255, blue: 42/255, alpha: 0.9)
    static let REVIEW_BACKGROUND_COLOR = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1.0)
    static let DELETE_COLOR = UIColor(red: 226/255, green: 87/255, blue: 76/255, alpha: 1.0)
    static let NOTIFICATION_BACKGROUND_COLOR = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
    static let ADD_NEW_CARD_HEADING_LABEL = UIColor(colorLiteralRed: 146/255, green: 146/255, blue: 146/255, alpha: 1)
    static let border_color = UIColor(colorLiteralRed: 219/255, green: 219/255, blue: 219/255, alpha: 0.5)
    static let skipButtonColor = UIColor(colorLiteralRed: 52/255, green: 56/255, blue: 57/255, alpha: 1)
    static let cancelColor = UIColor(red: 148/255, green: 148/255, blue: 148/255, alpha: 1.0)
    static let refferHeadingColor = UIColor(red: 144/255, green: 144/255, blue: 144/255, alpha: 1.0)
   static let shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.05)
   
   static let paymentGreenColor = UIColor(red: 130/255, green: 197/255, blue: 72/255, alpha: 1)
    
    static let SUBTITLE_LIGHT_COLOR = UIColor(red: 133/255, green: 133/255, blue: 133/255, alpha: 1.0)
   
   static let LINE_COLOR = UIColor(red: 235/256, green: 235/256, blue: 235/256, alpha: 1.0)
   static let TEXT_LIGHT_COLOR = UIColor(red: 89/256, green: 89/256, blue: 104/256, alpha: 1.0)
   static let BRICK_COLOR = UIColor(red: 225/256, green: 61/256, blue: 54/256, alpha: 1.0)
    static let textColorSlightLight = UIColor(red: 89/255, green: 89/255, blue: 104/255, alpha: 1.0)
    
    
    //Order Status Color
    static let App_Status_On_The_Way_Color = UIColor(red: 126/255, green: 46/255, blue: 31/255, alpha: 1.0)
    static let App_Status_Accepted_Color = UIColor(red: 94/255, green: 94/255, blue: 94/255, alpha: 1.0)
    static let App_Status_Started_Color = UIColor(red: 0/255, green: 0/255, blue: 176/255, alpha: 1.0)
    static let App_Status_Unassigned_Color = UIColor(red: 232/255, green: 148/255, blue: 46/255, alpha: 1.0)
    
}

struct OrderStatus {
    static let Unassigned = "ASSIGNING"
    static let Accepted = "ACCEPTED"
    static let Started = "PREPARING"
    static let OnTheWay = "ONROUTE"
    static let Delivered = "DELIVERED"
    static let Cancelled = "CANCELLED"
}


enum mapType {
    case white
    case black
    case normal
}
