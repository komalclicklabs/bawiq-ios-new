//
//  AddNewPromoCodeTableViewCell.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 16/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class AddNewPromoCodeTableViewCell: UITableViewCell,UITextFieldDelegate {

    @IBOutlet weak var topLabel: UILabel!
   
    override func awakeFromNib() {
        super.awakeFromNib()
    
        self.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        
        topLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle.customizeSize())
        topLabel.textColor = COLOR.THEME_FOREGROUND_COLOR
        self.selectionStyle = .none
    }

}
