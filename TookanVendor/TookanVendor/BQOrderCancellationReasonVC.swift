//
//  BQOrderCancellationReasonVC.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 06/06/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQOrderCancellationReasonVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnCancelOrder: UIButton!
    var cancelString = ""
    var arrCancelReasons = [BQCancelReasons]()
    
    
    var orderID: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar()
        self.getCancelOrderOptions()
        btnCancelOrder.layer.cornerRadius = 22.0
        
        self.tblView.register(UINib(nibName: BQSingleSelectionCell.cellIdentifier(), bundle: nil), forCellReuseIdentifier: BQSingleSelectionCell.cellIdentifier())
        self.tblView.register(UINib(nibName: BQAddCommentCell.cellIdentifier(), bundle: nil), forCellReuseIdentifier: BQAddCommentCell.cellIdentifier())
        self.tblView.delegate = self
        self.tblView.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func viewTermsBtnAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: STORYBOARD_NAME.main, bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: "terms") as? TermsAndConsitionViewController
        vc?.urlString = "https://www.bawiq.com/terms-and-conditions/"
        self.present(vc!, animated: true, completion: nil)
    }
    
    @IBAction func cancelOrderButtonAction(_ sender: Any) {
        var params = getParamsToDeleteLocation()
        let selectedManufactureList = self.arrCancelReasons.filter({ $0.isSelected == true })
        if selectedManufactureList.isEmpty {
            ErrorView.showWith(message: "Please Select  a cancellation reason", removed: nil)
            return
        }
        
        if let objectList: BQCancelReasons = selectedManufactureList.first {
            params["cancel_reason"] = objectList.reasonName
            if cancelString != "" {
                params["cancel_description"] = cancelString
            }
            
        }
        
        
        ActivityIndicator.sharedInstance.showActivityIndicator()
        HTTPClient.makeConcurrentConnectionWith(method: .POST, para: params, extendedUrl: "cancel_job") { (responseObject, error, _, statusCode) in
            
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            
            guard statusCode == .some(STATUS_CODES.SHOW_DATA) else {
                return
            }
            print(responseObject ?? "")
            if let dataRes = responseObject as? [String:Any] {
                if let data = dataRes["data"] as? [String:Any] {
                    print(data)
                    
                    let alertController = UIAlertController(title: "", message: "Your order has been cancelled Successfully ", preferredStyle: .alert)
                    let yesAction = UIAlertAction(title: "Ok", style: .destructive, handler: { (action) -> Void in
                        self.backAction()
                    })
                   
                    alertController.addAction(yesAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
        
    }
    
    private func getParamsToDeleteLocation() -> [String: Any] {
        return [
            "app_access_token": Vendor.current!.appAccessToken!,
            "job_id": "\(self.orderID ?? 0)"
        ]
    }
    
    private func getCancelOrderOptions() {
        
        var param = [String: Any]()
        param["user_type"] = "0"
        
        HTTPClient.makeConcurrentConnectionWith(method: .POST, para: param, extendedUrl: "get_customer_cancel_reasons") { (responseObject, error, _, statusCode) in
            
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            guard statusCode == .some(STATUS_CODES.SHOW_DATA) else {
                return
            }
            print(responseObject ?? "")
            if let dataRes = responseObject as? [String:Any] {
                if let data = dataRes["data"] as? [String: Any], let reasons = data["reasons"] as? [String] {
                    print(data)
                    for item in reasons {
                        let cancelReasons = BQCancelReasons()
                        cancelReasons.reasonName = item
                        cancelReasons.isSelected = false
                        self.arrCancelReasons.append(cancelReasons)
                    }
                    self.tblView.reloadData()
                }
            }
        }
    }
    
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        //Use for showing two right bar buttons on navigation bar
        
        let navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "#\(orderID ?? 0)", leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"), rightButtonImage: nil, secondRightButtonImage: nil)
            , leftButtonAction: {[weak self] in
                self?.backAction()
            }, rightButtonAction: nil,
               rightButtonSecondAction: nil
        )
        navigationBar.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
    }
    // MARK: - IBAction
    func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        if section == 0 {
//            return "Select a reason to cancel"
//        }
//            return "Input you cancel reason"
//
//
//    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 32
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrCancelReasons.count
        }
        return 1
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1 {
            return 132
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: BQAddCommentCell.cellIdentifier()) else {
                fatalError()
            }
            if let cell = cell as? BQAddCommentCell {
                cell.delegate = self
            }
            return cell
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: BQSingleSelectionCell.cellIdentifier()) else {
            fatalError()
        }
        let cellDataModel = self.arrCancelReasons[indexPath.row]
        if let cell = cell as? BQSingleSelectionCell {
            cell.lblOption?.text = cellDataModel.reasonName
            cell.btnRadioCheck.imageView?.image = cellDataModel.isSelected ? #imageLiteral(resourceName: "radioFill") : #imageLiteral(resourceName: "radio")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.arrCancelReasons[indexPath.row].isSelected = true
        if let cell = tableView.cellForRow(at: indexPath) as? BQSingleSelectionCell {
            cell.btnRadioCheck.setImage(#imageLiteral(resourceName: "radioFill"), for: .normal)
            
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        self.arrCancelReasons[indexPath.row].isSelected = false
        if let cell = tableView.cellForRow(at: indexPath) as? BQSingleSelectionCell {
            cell.btnRadioCheck.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
        }
    }
    
}
extension BQOrderCancellationReasonVC: BQAddCommentCellDelegate {
    func cancelComment(textReview: String) {
        cancelString = textReview
    }
    
    
}


