//
//  FamilyMemberListViewController.swift
//  TookanVendor
//
//  Created by Vishal on 30/01/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class FamilyMemberListViewController: UIViewController {
    
    @IBOutlet weak var familyMemberTableView: UITableView!
    var presenter: FamilyMemberPresenter!
    var navigationBar: NavigationView!

    override func viewDidLoad() {
        super.viewDidLoad()
        presenterSetup()
        setNavigationBar()
        familyMemberTableView.tableFooterView = UIView(frame: CGRect.zero)
        // Do any additional setup after loading the view.
    }
    
    func setNavigationBar() {
        navigationBar = NavigationView.getNibFile(leftButtonAction: { [weak self] in
            self?.backAction()
            }, rightButtonAction: nil)
        navigationBar.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        navigationBar.bottomLine.isHidden = false
        navigationBar.titleLabel.text = "Added Members".localized
        self.view.addSubview(navigationBar)
    }
    
    func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    //Presenter setup
    private func presenterSetup() {
        self.presenter = FamilyMemberPresenter(view: self)
        self.presenter.getAllFamilyMember { (success, response) in
            self.familyMemberTableView.reloadData()
        }
    }
    
    
    @IBAction func addNewMemberAction(_ sender: UIButton) {
    self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func proceedAction(_ sender: UIButton) {
        Singleton.sharedInstance.pushToHomeScreen() {}
    }
    
}

extension FamilyMemberListViewController: FamilyMemberProtocol {
    func receivedResponse() {
        
    }
    
    func showError(error: Error?) {
        
    }
    
}

extension FamilyMemberListViewController: FamilyMemberTableViewCellDelegate {
    func deleteFamilyMember() {
        
    }
    
}

extension FamilyMemberListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "FamilyMemberTableViewCell", for: indexPath) as? FamilyMemberTableViewCell else {
            return UITableViewCell()
        }
        cell.data = presenter.cellForRow(indexPath: indexPath)
        return cell
    }
    
    
}

///Family protocols
protocol FamilyMemberProtocol: class  {
    func receivedResponse()
    func showError(error:Error?)
}

//FamilyMember Presenter
class FamilyMemberPresenter {
    
    weak private var view: FamilyMemberProtocol?
    var familyMembers = [FamilyMember]()
    
    init(view: FamilyMemberProtocol) {
        self.view = view
        
    }
    func cellForRow(indexPath: IndexPath) -> FamilyMember {
        return familyMembers[indexPath.row]
    }
    
    var numberOfRows: Int {
        return familyMembers.count
    }
    
    func getAllFamilyMember(_ receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        let params = [
            "app_access_token":Vendor.current!.appAccessToken!
        ]
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getFamilyMember,
                                                         params: params as [String : AnyObject],
                                                         httpMethod: "POST") { (succeeded, response) in
            DispatchQueue.main.async {
                print(response)
                if(succeeded) {
                        if let data = response["data"] as? [[String:Any]] {
                            self.familyMembers = data.map({FamilyMember(data: $0)})
                        }
                    receivedResponse(true, response)
                } else {
                    receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                }
            }
        }
    }
    
}

struct FamilyMember {
    var email: String!
    var isAccountVerified: Int!
    var phoneNo: String!
    var username: String!
    var vendorId: Int!
    var walletLimit: Int!
    var isAbove21: Int!
    var dob: String?
    var gender: String?
    var nationality: String?
    
    init(data:[String: Any]) {
        if let email = data["email"] as? String {
            self.email = email
        }
        if let isAccountVerified = data["is_account_verified"] as? Int {
            self.isAccountVerified = isAccountVerified
        }
        if let phoneNo = data["phone_no"] as? String {
            self.phoneNo = phoneNo
        }
        if let username = data["username"] as? String {
            self.username = username
        }
        if let vendorId = data["vendor_id"] as? Int {
            self.vendorId = vendorId
        }
        if let walletLimit = data["wallet_limit"] as? Int {
            self.walletLimit = walletLimit
        }
        if let above21 = data["is_above_21"] as? Int {
            self.isAbove21 = above21
        }
        if let dob = data["dob"] as? String {
            self.dob = dob
        }
        if let gender = data["gender"] as? String {
            self.gender = gender
        }
        if let nationality = data["nationality"] as? String {
            self.nationality = nationality
        }
    }
    
    
    func deleteMember(_ receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        let params: [String: Any] = [
            "app_access_token":Vendor.current!.appAccessToken!,
            "user_id": vendorId
        ]
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.deleteFamilyMember,
                                                         params: params as [String : AnyObject],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                            DispatchQueue.main.async {
                                                                print(response)
                                                                if(succeeded) {
                                                                    receivedResponse(true, response)
                                                                } else {
                                                                    receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                                                                }
                                                            }
        }
    }
}
