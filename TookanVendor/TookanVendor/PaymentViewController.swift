//
//  PaymentViewController.swift
//  TookanVendor
//
//  Created by Vishal on 15/02/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController, PaymentView, UITableViewDelegate, UITableViewDataSource, PaymentCellDelegate {
  
    
    @IBOutlet weak var cardTable: UITableView!
    var presenter: PaymentPresenter?
    var paymentOptionSelectedString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpPresenter()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if let cartSegmentVc = self.parent as? CartSegmentViewController {
            cartSegmentVc.navigationBar.titleLabel.text = "Payment"
            self.cardTable.reloadData()
        }
    }
    
    private func setUpPresenter() {
        presenter = PaymentPresenterImplementation(withView: self)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (presenter?.numberOfItemsInTable())!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell", for: indexPath) as? PaymentCell else {
            return UITableViewCell()
        }
       // if indexPath.row == 0 {
            cell.btnRadioOption.tag = indexPath.row
        //}
        presenter?.setupDataTable(view: cell, index: indexPath.row)
        cell.delegate = self
        return cell
    }
    
    
    func reloadTable() {
        
    }
    
    @IBAction func contiNueAction(_ sender: Any) {
        
        if paymentOptionSelectedString != "" {
            BQBookingModel.shared.paymentOption = paymentOptionSelectedString
            
            
            if BQBookingModel.shared.paymentOption == "Card" {
                if let amount = BQBookingModel.shared.totalAmount {
                    BQBookingModel.shared.totalAmountAfterBankCharge = String((Double(amount)!))
//                        String(String((Double(amount)!*5)/100 + Double(amount)!))
                }
            }
            
            
            if let parent = self.parent as? CartSegmentViewController {
                parent.switchController(from: CartController.payment)
            }
        } else {
            //SHow alert for selecting payment option
            Singleton.sharedInstance.showAlert("Please select payment option.")
        }
        
    }
    func paymentOptionSelected(paymentOption: String) {
        //if paymentOption == "Cash" {
            paymentOptionSelectedString = paymentOption
        //}
    }
    
    func rechargeWallet() {
        Singleton.sharedInstance.showAlertWithOption(owner: self, title: "", message: "Please recharge your wallet.", showRight: true, leftButtonAction: {
            print("Cancel")
        }, rightButtonAction: {
            if let vc = UIStoryboard(name: "MyOrders", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQWalletVC") as? BQWalletVC {
                vc.isFromPaymentScreen = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }, leftButtonTitle: "Cancel", rightButtonTitle: "Ok")
    }
    
    
//    func showAlertForMessage(message: String) {
//        let alertController = UIAlertController(title: "", message: message, preferredStyle: .actionSheet)
//        let yesAction = UIAlertAction(title: "OK", style: .destructive, handler: { (action) -> Void in
//
//        })
//
//        alertController.addAction(yesAction)
//        self.present(alertController, animated: true, completion: nil)
//    }
    
    
}



protocol PaymentView: class  {
    func reloadTable()
}

protocol PaymentPresenter {
    func numberOfItemsInTable() -> Int
    func setupDataTable(view: PaymentCellView, index: Int)
}

class PaymentPresenterImplementation: PaymentPresenter {
    
    var cards: [CardType] = [.cod,.credit,.wallet]
    
    weak var view: PaymentView?
    
    init(withView view: PaymentView) {
        self.view = view
    
    }
    
    
    func numberOfItemsInTable() -> Int {
        return cards.count
    }
    
    func setupDataTable(view: PaymentCellView, index: Int) {
        let product = cards[index]
        view.setupData(type: product)
        
    }
}

enum CardType {
    case cod
    case credit
    case wallet
}

protocol PaymentCellView {
    func setupData(type: CardType)
}

protocol PaymentCellDelegate: class {
    func paymentOptionSelected(paymentOption: String)
    func rechargeWallet()
}
class PaymentCell: UITableViewCell, PaymentCellView {
    
    @IBOutlet weak var creditDebitLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var rightStack: UIStackView!
    @IBOutlet weak var changeCardButton: UIButton!
    @IBOutlet weak var btnRadioOption: UIButton!
    @IBOutlet weak var walletAmount: UILabel!
    @IBOutlet weak var bgView: UIView!
    weak var delegate: PaymentCellDelegate?
    
    //
    var cardType: CardType? {
        didSet {
            switch cardType! {
            case .cod:
                rightStack.isHidden = true
                creditDebitLabel.isHidden = true
                cardNumberLabel.text = "Cash on Delivery"
                break
            case .credit:
                walletAmount.isHidden = true
                changeCardButton.isHidden = true
                creditDebitLabel.isHidden = true
                cardNumberLabel.text = "Pay by Credit/Debit card"
                break
            case .wallet:
                changeCardButton.isHidden = true
                creditDebitLabel.isHidden = true
                walletAmount.text = "AED " + String(format: "%.2f", Vendor.current?.wallet_amount ?? 0.0) //"AED \(Vendor.current?.wallet_amount ?? 0.0)"
                cardNumberLabel.text = "Pay by Bwallet"
                break
            
            }
        }
    }
    
    @IBAction func selectPayment(_ sender: UIButton) {
        
        if sender.tag == 0 {
            sender.isSelected = true
            delegate?.paymentOptionSelected(paymentOption: "Cash")
        } else if sender.tag == 1 {
            sender.isSelected = true
            delegate?.paymentOptionSelected(paymentOption: "Card")
        } else if sender.tag == 2 {
            guard let bookingAmount = BQBookingModel.shared.totalAmount else {
                return
            }
            guard let walletAmount = Vendor.current?.wallet_amount else {
                return
            }
            guard let bookingAmnt = Double(bookingAmount) else {
                return
            }
            if walletAmount < bookingAmnt {
                delegate?.rechargeWallet()
            } else {
                sender.isSelected = true
                delegate?.paymentOptionSelected(paymentOption: "Wallet")
            }
        }
        
        if sender.isSelected {
            deselectOtherButton()
        }
        
    }
    
    func deselectOtherButton() {
        var tableView = UITableView()
        if #available(iOS 11.0, *) {
            print("iOS 11.0 and greater")
            tableView = self.superview as! UITableView
        } else {
            print("iOS < 11")
            tableView = self.superview?.superview as! UITableView
        }
        
        let tappedCellIndexPath = tableView.indexPath(for: self)!
        let section = tappedCellIndexPath.section
        let rowCounts = tableView.numberOfRows(inSection: section)
        
        for row in 0..<rowCounts {
            if row != tappedCellIndexPath.row {
                let cell = tableView.cellForRow(at: IndexPath(row: row, section: section)) as! PaymentCell
                cell.btnRadioOption.isSelected = false
            }
        }
        
    }
    
    
    func setupData(type: CardType) {
        self.cardType = type
    }
    
    
}
