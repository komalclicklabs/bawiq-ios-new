//
//  AddressViewCell.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 11/24/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class AddressViewCell: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet weak var bottomUnderLine: UIView!
    @IBOutlet weak var rightImage: UIImageView!
    @IBOutlet weak var addressText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        /*------------ Title Label ---------------*/
        self.titleLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        self.titleLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        self.titleLabel.text = TEXT.GOTO
        
        /*------------ Address ----------------*/
        self.addressText.text = TEXT.ENTER_LOCATION
        self.addressText.textColor = COLOR.SPLASH_TEXT_COLOR
        self.addressText.font = UIFont(name: FONT.ultraLight, size: FONT_SIZE.large)
        
        /*-------------- Underline --------------*/
        self.bottomUnderLine.backgroundColor = COLOR.SPLASH_LINE_COLOR
        
    }

    func setAddress() {
        if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
            self.addressText.text = Singleton.sharedInstance.createTaskDetail.jobPickupAddress
        } else {
            self.addressText.text = Singleton.sharedInstance.createTaskDetail.customerAddress
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
