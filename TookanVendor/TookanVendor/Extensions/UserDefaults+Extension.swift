//
//  UserDefaults+Extension.swift
//  TookanVendor
//
//  Created by CL-Macmini-110 on 9/28/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation


extension UserDefaults {
    func setToNil(defaultName: String) {
        removeObject(forKey: defaultName)
    }
}
