//
//  UIButton+Extension.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 12/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit


extension UIButton {
  
    
  // MARK: - Config
  func configureSmallButtonWith(title: String) {
    setTitle(title, for: .normal)
    titleLabel?.font = UIFont(name: FONT.regular, size: 15.0)
    setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: UIControlState.normal)
  }
  
  func configureNormalButtonWith(title: String) {
    setTitle(title, for: .normal)
    titleLabel?.font = UIFont(name: FONT.regular, size: 17.0)
    setTitleColor(COLOR.LOGIN_BUTTON_TITLE_COLOR, for: .normal)
    backgroundColor = COLOR.THEME_FOREGROUND_COLOR
    layer.cornerRadius = 2
    setSameColorShadow()
  }
  
  func configureNormalButtonWithOppositeColorThemeWith(title: String) {
    configureNormalButtonWith(title: title)
    
    setTitleColor(COLOR.SIGNIN_TITLE_TYPE_LABEL, for: .normal)
    backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
    layer.borderColor = COLOR.SIGNIN_TITLE_TYPE_LABEL.cgColor
    setSameColorShadow()
    layer.borderWidth = 1
  }
    
    func setButtonShadow() {
        self.layer.shadowOffset = CGSize(width: 0, height: 5)
        self.layer.shadowOpacity = 0.1
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1).cgColor
        self.layer.shadowRadius = 5
    }
}
