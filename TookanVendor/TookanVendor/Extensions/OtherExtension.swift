//
//  OtherExtension.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 19/06/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import Foundation

extension IndexPath {
   func generateTag() -> Int {
      let tag = (1000 * (self.section + 1)) + (self.row + 1)
      return tag
   }
   
}

extension Int {
   func getRowAndSection() -> (row: Int, section: Int) {
      return ((self % 1000)-1, (self / 1000) - 1)
   }
}

extension UITableView {
   func registerCellWith(nibName: String, reuseIdentifier: String) {
      let cellNib = UINib(nibName: nibName, bundle: nil)
      self.register(cellNib, forCellReuseIdentifier: reuseIdentifier)
   }
}

extension NSMutableAttributedString {
    
    public func setAsLink(textToFind:String, linkURL:String) -> Bool {
        
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(NSLinkAttributeName, value: linkURL, range: foundRange)
            return true
        }
        return false
    }
}
