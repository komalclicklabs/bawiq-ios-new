//
//  UILabel+Extension.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 15/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

extension UILabel {
  func configureSignInTitleTypeWith(text: String) {
    self.text = text
    textColor = COLOR.SPLASH_TEXT_COLOR
    font = UIFont(name: FONT.light, size: 30)
  }
  
  func configureNewUserTypeLabelWith(text: String) {
    self.text = text
    textColor = COLOR.SPLASH_TEXT_COLOR
    font = UIFont(name: FONT.regular, size: 15)
  }
  
  func configureHeadingTypeLabelWith(text: String) {
    self.text = text
    textColor = COLOR.SPLASH_TEXT_COLOR
    font = UIFont(name: FONT.regular, size: 20)
  }
  
  func configureDescriptionTypeLabelWith(text: String) {
    self.text = text
    textColor = COLOR.SPLASH_TEXT_COLOR
    font = UIFont(name: FONT.light, size: 16)
  }
}
