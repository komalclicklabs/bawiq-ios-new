//
//  UINavigationController+Extension.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 16/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

extension UINavigationController {
  func getSecondLastController() -> UIViewController {
    let totalViewControllersInNavigationStack = viewControllers.count
    let secondLastController = viewControllers[totalViewControllersInNavigationStack-2]
    return secondLastController
  }
}
