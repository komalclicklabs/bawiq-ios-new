//
//  UIViewController+Extension.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 15/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

extension UIViewController {
  class func findIn(storyboard: StoryBoard, withIdentifier identifier: String?) -> UIViewController? {
    
    if let _ = identifier {
      return UIStoryboard(name: storyboard.rawValue, bundle: Bundle.main).instantiateViewController(withIdentifier: identifier!)
    } else {
      return UIStoryboard(name: storyboard.rawValue, bundle: Bundle.main).instantiateInitialViewController()
    }
  }
    
    func findInWithFacebook(storyboard: StoryBoard, withIdentifier identifier: String?) -> UIViewController? {
        
        if let _ = identifier {
            return UIStoryboard(name: storyboard.rawValue, bundle: Bundle.main).instantiateViewController(withIdentifier: identifier!)
        } else {
            return UIStoryboard(name: storyboard.rawValue, bundle: Bundle.main).instantiateInitialViewController()
        }
    }
    
}

extension UIAlertController {
  class func showWith(message: String, title: String?, buttonTitle: String, buttonAction: (() -> Void)?) {
    let rootViewController = UIApplication.shared.keyWindow?.rootViewController
  
    Singleton.sharedInstance.showAlertWithOption(owner: rootViewController!, title: title ?? "", message: message, showRight: false, leftButtonAction: buttonAction, rightButtonAction: nil, leftButtonTitle: buttonTitle, rightButtonTitle: "")
  }
   
   class func showActionSheetWith(title: String?, message: String?, options: [String], actions: [(() -> Void)?]) {
      
      let rootViewController = UIApplication.shared.keyWindow?.rootViewController
      
      let alertController = getActionSheetWith(title: title, message: message, options: options, actions: actions)
      
      rootViewController?.present(alertController, animated: true, completion: nil)
   }
   
   class func getActionSheetWith(title: String?, message: String?, options: [String], actions: [(() -> Void)?]) -> UIAlertController {
      let actionSheet = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
      
      for index in 0..<options.count where actions.count == options.count {
         let alertAction = UIAlertAction(title: options[index], style: .default) { _ in
            actions[index]?()
         }
         actionSheet.addAction(alertAction)
      }
      
      let cancelAction = UIAlertAction(title: TEXT.CANCEL, style: .cancel, handler: nil)
      actionSheet.addAction(cancelAction)
      
      return actionSheet
   }
   
}
