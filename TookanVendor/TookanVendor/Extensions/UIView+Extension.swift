//
//  UIView+Extension.swift
//  TookanVendor
//
//  Created by cl-macmini-117 on 10/05/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

extension UIView {
   
    func border(width: CGFloat = 1.0 , color: UIColor, radius: CGFloat = 4) {
        
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
        self.layer.cornerRadius = radius
    }
    
   func setShadow() {
      setShadowBounds()
      self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3).cgColor
   }
   
    
    var safeAreaInsetsForAllOS: UIEdgeInsets {
        var insets: UIEdgeInsets
        if #available(iOS 11.0, *) {
            insets = safeAreaInsets
        } else {
            insets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        return insets
    }
    
   func setShadowBounds() {
      self.layer.shadowOffset = CGSize(width: 2, height: 3)
      self.layer.shadowOpacity = 0.3
      self.layer.shadowRadius = 3
    self.clipsToBounds = false  
   }
  
  func showSmoothly(duration: TimeInterval = 1,completion: Completion) {
    isHidden = false
    UIView.animate(withDuration: 1.0, animations: {
      self.alpha = 1.0
    }, completion: { completed in
      completion?(completed)
    })
  }
  
  func setAlphaToZero() {
    self.alpha = 0
    isHidden = true
  }
  
  func hideSmoothly(completion: Completion) {
    UIView.animate(withDuration: 0.4, animations: {
      self.alpha = 0
    }, completion: { [weak self] completed in
      completion?(completed)
      self?.isHidden = true
    })
  }
  
  func setCornerRadius(radius:CGFloat) {
    self.layer.cornerRadius = radius
    self.layer.masksToBounds = true
  }
  
  func setBorder(borderColor: UIColor) {
    self.layer.borderColor = borderColor.cgColor
    self.layer.borderWidth = 1.0
    self.layer.masksToBounds = true
  }
   
   func setSameColorShadow() {
      setShadowBounds()
      self.layer.shadowColor = self.backgroundColor?.cgColor
   }
  
  func roundCorners(cornersToRound: UIRectCorner) {
    let path = UIBezierPath(roundedRect: self.bounds,
                            byRoundingCorners: cornersToRound,
                            cornerRadii: CGSize(width: 15, height:  15))
    
    let maskLayer = CAShapeLayer()
    
    maskLayer.path = path.cgPath
    self.layer.mask = maskLayer
  }
  
  @IBInspectable var _cornerRadius: CGFloat {
    set {
      self.layer.cornerRadius = newValue
      self.layer.masksToBounds = true
    } get {
      return self.layer.cornerRadius
    }
  }
    
    
    func addDashedLine(fromPoint start: CGPoint, toPoint end:CGPoint) {
        let linePath = UIBezierPath()
        linePath.move(to: start)
        linePath.addLine(to: end)
        
        let line = CAShapeLayer()
        line.path = linePath.cgPath
        line.strokeColor = UIColor(red:157.0/255, green: 157.0/255, blue: 157.0/255, alpha: 1.0).cgColor//UIColor.red.cgColor
        line.lineWidth = 1
        line.lineJoin = kCALineJoinRound
        line.lineDashPattern = [4, 4]
        self.layer.addSublayer(line)
    }
    
    func dropShadow(backgroundColor: UIColor, shadowColor: UIColor?, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1) {
        self.backgroundColor = backgroundColor
        self.layer.cornerRadius = radius //4.0
        self.layer.shadowOffset = offSet //CGSize(width: 2, height: 3)
        self.layer.shadowOpacity = opacity //0.7
        self.layer.shadowColor = shadowColor?.cgColor //UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
    }
  
}

extension String {
    func applyPatternOnNumbers(pattern: String, replacmentCharacter: Character) -> String {
        var pureNumber = self.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
        for index in 0 ..< pattern.count {
            guard index < pureNumber.count else { return pureNumber }
            let stringIndex = String.Index(encodedOffset: index)
            let patternCharacter = pattern[stringIndex]
            guard patternCharacter != replacmentCharacter else { continue }
            pureNumber.insert(patternCharacter, at: stringIndex)
        }
        return pureNumber
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}


