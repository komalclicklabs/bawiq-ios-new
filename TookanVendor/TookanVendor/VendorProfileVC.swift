//
//  VendorProfileVC.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 11/29/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//LavishLabs123

import UIKit
import GoogleMaps
import GooglePlaces

class VendorProfileVC: UIViewController,UITableViewDelegate,UITableViewDataSource,NavigationDelegate,ProfileFieldChanged,VendorNameImageChanged/*,PickerDelegate*/,UIImagePickerControllerDelegate,UINavigationControllerDelegate, ErrorDelegate{
    func setUserCurrentLocation() {
        
    }
    
    var cellTypeArray = [FIELD_TYPE.name,FIELD_TYPE.name,FIELD_TYPE.email,.contact,.password]
    var countryCodeAndCountryName = [String:String]()
    let picker = UIImagePickerController()
    var selectedLocale:String!
    var profileDetailsinfo = ProfileDetails()
    var isProfileEditting = false
    var pickerForDropdown:CustomDropdown!
    var isNeedToUpdateNewImage = false
    var didMakeAnyChanges = false
    var errorMessageView:ErrorView!
    var changePassWordHit = false
    var navigationBar : NavigationView?
    
    @IBOutlet weak var changePasswordBtn: UIButton!
    @IBOutlet weak var addNewMember: UIButton!
    var familyMenbers = [FamilyMember]()
    var isComingFromConfirmBooking = false
    
    @IBOutlet weak var profileTableView: UITableView!
    
    
    //MARK: - VIEW APPEAR & LOAD FUNCTIONS
    override func viewDidLoad() {
        super.viewDidLoad()
        profileTableView.bounces = false
        profileTableView.tableFooterView = UIView(frame: CGRect.zero)
        // bottomButtonConstraint.constant = 50
        editorSaveButtonAction(showEdit: false)
        profileTableView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        picker.delegate = self
        self.view.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
        setNavigationBar()
        self.profileTableView.separatorStyle  =  .none
        self.profileTableView.tableFooterView = UIView(frame: CGRect.zero)
        
        NotificationCenter.default.addObserver(self, selector: #selector(setUserLocationFromMap), name: NSNotification.Name(rawValue: "AddUserLocation"), object: nil)
        
        self.profileTableView.register(UINib.init(nibName: "ProfilePictureCell", bundle: Bundle.main), forCellReuseIdentifier: "ProfilePictureCell")
        
        self.profileTableView.register(UINib.init(nibName: "AddressCell", bundle: Bundle.main), forCellReuseIdentifier: "AddressCell")
        
        self.profileTableView.register((UINib.init(nibName: "AddedMemberCell", bundle: Bundle.main)), forCellReuseIdentifier: "AddedMemberCell")
        
        self.view.backgroundColor = UIColor.white
        setUserInfo()
        
//        self.profileTableView.reloadData()
        
        addNewMember.isHidden = Vendor.current?.signUpType == SignUptype.individual.rawValue ? true : false
 
        if Vendor.current?.socialSignUpType != 0 {
            changePasswordBtn.isHidden = true
        }
        
    }
    
    func shoWProfileImage(on image: UIImageView) {
        if let url = Vendor.current?.vendorImage {
            let imageUrl = URL.get(from: url)
            image.kf.setImage(with: imageUrl, placeholder: placeholdeImage)
        }
    }
    
    
    //    func applyButtonConfigure() {
    //        applyButton.clipsToBounds = true
    //        applyButton.layer.borderWidth = 1
    //        applyButton.layer.borderColor = UIColor.white.cgColor
    //        applyTextField.placeHolderColor = .white
    //    }
    
    func getAllFamilyMember(_ receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        if let vendor = Vendor.current, let vendorAccessToken = vendor.appAccessToken {
            let params = [
                "app_access_token": vendorAccessToken
            ]
            NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getFamilyMember,
                                                             params: params as [String : AnyObject],
                                                             httpMethod: "POST") { (succeeded, response) in
                                                                DispatchQueue.main.async {
                                                                    print(response)
                                                                    if(succeeded) {
                                                                        if let data = response["data"] as? [[String:Any]] {
                                                                            self.familyMenbers = data.map({FamilyMember(data: $0)})
                                                                        }
                                                                        receivedResponse(true, response)
                                                                    } else {
                                                                        receivedResponse(false, ["message":ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                                                                    }
                                                                }
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAllFamilyMember { (success, response) in
            self.profileTableView.reloadData()
        }
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    //MARK: - EDIT BUTTON TAPPED UPDATE ALL TABLE VIEW CELLS CELLS
    func editButtonTapped() {
        isProfileEditting = true
        // navigationBar.rightButton.isSelected = true
        //self.navigationBar.rightButton.setTitle(TEXT.SAVE, for: .normal)
        for i in 2..<5 {
            if let profileCell = self.profileTableView.cellForRow(at: IndexPath(row: i, section: 0)) as? VendorProfileCell {
                profileCell.cellField.isUserInteractionEnabled = isProfileEditting
                profileCell.cellField.countryCodeButton.isUserInteractionEnabled = isProfileEditting
            }
        }
    }
    
    //MARK: - IMAGE PICKER TO CHANGE IMAGE
    func openImagePicker() {
        if isProfileEditting == true {
            let alertController = UIAlertController(title: "", message: TEXT.CHOOSE_IMAGE, preferredStyle: .actionSheet)
            let galleryAction = UIAlertAction(title: TEXT.GALLERY, style: .default, handler: { (action) -> Void in
                self.openCameraOrGallery(imagePickerType: .photoLibrary)
            })
            let cameraAction = UIAlertAction(title: TEXT.CAMERA, style: .default, handler: { (action) -> Void in
                self.openCameraOrGallery(imagePickerType: .camera)
            })
            let cancelAction = UIAlertAction(title: TEXT.CANCEL_ALERT, style: .cancel, handler: { (action) -> Void in
                self.dismiss(animated: true, completion: nil)
            })
            alertController.addAction(galleryAction)
            alertController.addAction(cameraAction)
            alertController.addAction(cancelAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    func openCameraOrGallery(imagePickerType:UIImagePickerControllerSourceType){
        if UIImagePickerController.isSourceTypeAvailable(imagePickerType){
            picker.delegate = self
            picker.sourceType = imagePickerType
            picker.allowsEditing = false
            self.present(picker, animated: true, completion: nil)
        } else {
            switch (imagePickerType){
            case .camera:
                Singleton.sharedInstance.showAlert(TEXT.CAMERA_NOT_ACCESSIBLE)
            case .photoLibrary:
                Singleton.sharedInstance.showAlert(TEXT.GALLERY_NOT_ACCESSIBLE)
            default:
                break
            }
        }
        
    }
    
    //MARK: - IMAGE PICKER CONTROLLER DELEGATES
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        isNeedToUpdateNewImage = false
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            isNeedToUpdateNewImage = true
            profileDetailsinfo.image = pickedImage
            if let profileCell = self.profileTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? VendorNamePicCell {
                //profileCell.imagePickerButton.setImage(pickedImage, for: .normal)
                profileCell.vendorImageView.image = pickedImage
            }
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func setUserInfo() {
        profileDetailsinfo = ProfileDetails()
        profileDetailsinfo.email = Vendor.current?.email ?? ""
        profileDetailsinfo.fullName = (Vendor.current?.firstName ?? "") + " " +  (Vendor.current?.lastName ?? "")
        profileDetailsinfo.firstName = (Vendor.current?.firstName ?? "")
        profileDetailsinfo.lastName = (Vendor.current?.lastName ?? "")
        profileDetailsinfo.company = Vendor.current?.company ?? ""
        profileDetailsinfo.address  = Vendor.current?.address ?? ""
        profileDetailsinfo.imageString = Vendor.current?.vendorImage ?? ""
        profileDetailsinfo.phoneNumber = Vendor.current?.phoneNo ?? ""
        profileDetailsinfo.gender = Vendor.current?.gender ?? ""
        profileDetailsinfo.dob = Vendor.current?.dob ?? ""
        profileDetailsinfo.nationality = Vendor.current?.nationality ?? ""
    }
    
    //MARK: - CHECK VALIDATIONS FOR EDIT PROFILE API
    func checkValidations() {
        self.view.endEditing(true)
        if profileDetailsinfo.fullName.trimText == "" {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_NAME, isError: true)
            //Singleton.sharedInstance.showAlert(ERROR_MESSAGE.INVALID_NAME)
            return
        }
        
        //        if profileDetailsinfo.countryCode == "" || profileDetailsinfo.countryCode == "+"  {
        //            Singleton.sharedInstance.showAlert(ERROR_MESSAGE.INVALID_COUNTRY_CODE)
        //            return
        //        }
        
        
        if profileDetailsinfo.phoneNumber.trimText == ""  || profileDetailsinfo.phoneNumber.trimText.count < 10 {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PHONE_NUMBER, isError: true)
            //Singleton.sharedInstance.showAlert(ERROR_MESSAGE.INVALID_PHONE_NUMBER)
            return
        }
        
        //        if profileDetailsinfo.company == ""   {
        //            Singleton.sharedInstance.showAlert(ERROR_MESSAGE.INVALID_COMPANY)
        //            return
        //        }
        
        //        if profileDetailsinfo.address == ""   {
        //            Singleton.sharedInstance.showAlert(ERROR_MESSAGE.INVALID_ADDRESS)
        //            return
        //        }
        
        if changePassWordHit == true{
            let oldPasswordText = (profileDetailsinfo.oldPassWord.trimText)
            
            guard Singleton.sharedInstance.validPassword(password:oldPasswordText) == true else {
                self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PASSWORD,isError: true)
                return
            }
            
            let newPasswordText = (profileDetailsinfo.newPassWord.trimText)
            guard Singleton.sharedInstance.validPassword(password:newPasswordText) == true else {
                self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PASSWORD,isError: true)
                return
            }
            
            //        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
            //        if newPasswordText.rangeOfCharacter(from: characterset.inverted) == nil {
            //
            //            self.showErrorMessage(error: TEXT.specialCharecterString, isError: true)
            //            return
            //        }
            
            if oldPasswordText == newPasswordText  {
                self.showErrorMessage(error: ERROR_MESSAGE.OLD_NEW_PASSWORD,isError: true)
                return
            }
        }
        guard self.didMakeAnyChanges == true || self.isNeedToUpdateNewImage == true else {
            self.resetEditProfileView()
            return
        }
        
        
        
        APIManager.sharedInstance.editProfile(apiName: API_NAME.editProfile, params: profileDetailsinfo.returnJson(), httpMethod: HTTP_METHOD.POST, isImage: isNeedToUpdateNewImage, vendorImage: profileDetailsinfo.image, imageKey: "vendor_image", isNeedToShowActivityIndictor: true) {  (isSuccess, response) in
            DispatchQueue.main.async {
                print("API_NAME.editProfile: \(response)")
                if isSuccess {
                    if let status = response["status"] as? Int {
                        logEvent(label: "profile_edit_success")
                        switch(status) {
                            
                        case STATUS_CODES.SHOW_DATA:
                            DispatchQueue.main.async(execute: {[weak self] () -> Void in
                                if let data = response["data"] as? [String:Any] {
                                    let vendorDetails = Vendor(json: data)
                                    Vendor.current = vendorDetails
                                    BQBookingModel.shared.email = Vendor.current?.email
                                    UserDefaults.standard.set(vendorDetails.appAccessToken, forKey: USER_DEFAULT.accessToken)
                                    
                                    if self?.changePassWordHit == true    {
                                        self?.checkValidation()
                                    }else{
                                        // self?.editorSaveButtonAction()
                                        self?.isNeedToUpdateNewImage = false
                                        self?.didMakeAnyChanges = false
                                        self?.changePassWordHit = false
                                        self?.resetEditProfileView()
                                        self?.showErrorMessage(error: response["message"] as? String ?? "", isError: false)
                                    }
                                    //Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
                                }
                            })
                            break
                        default:
                            break
                        }
                    }
                }else{
                    self.showErrorMessage(error: response["message"] as? String ?? "", isError: true)
                }
            }
        }
    }
    
    
    func checkValidation() {
        
        let oldPasswordText = (profileDetailsinfo.oldPassWord.trimText)
        
        guard Singleton.sharedInstance.validPassword(password:oldPasswordText) == true else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PASSWORD,isError: true)
            return
        }
        
        let newPasswordText = (profileDetailsinfo.newPassWord.trimText)
        guard Singleton.sharedInstance.validPassword(password:newPasswordText) == true else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PASSWORD,isError: true)
            return
        }
        
        //        let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
        //        if newPasswordText.rangeOfCharacter(from: characterset.inverted) == nil {
        //
        //            self.showErrorMessage(error: TEXT.specialCharecterString, isError: true)
        //            return
        //        }
        
        if oldPasswordText == newPasswordText  {
            self.showErrorMessage(error: ERROR_MESSAGE.OLD_NEW_PASSWORD,isError: true)
            return
        }
        
        
        let changePass = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,"new_password":newPasswordText,"current_password":oldPasswordText,"app_device_type":APP_DEVICE_TYPE]
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.serverCall(apiName: API_NAME.vendorUpdate, params: changePass as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) {[weak self] (isSuccess, response) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSuccess {
                    logEvent(label: "password_change_success")
                    //  self.emailField.textField.text = ""
                    if let data = response["data"] as? [String:AnyObject] {
                        if let accessToken = data["access_token"] as? String {
                            UserDefaults.standard.setValue(accessToken, forKey: USER_DEFAULT.accessToken)
                            Vendor.current?.appAccessToken = accessToken
                            //   Singleton.sharedInstance.showAlert(response["message"] as? String ?? "")
                            self?.isNeedToUpdateNewImage = false
                            self?.didMakeAnyChanges = false
                            self?.changePassWordHit = false
                            self?.resetEditProfileView()
                            //  self?.editorSaveButtonAction()
                            self?.showErrorMessage(error: response["message"] as? String ?? "", isError: false)
                            
                        }
                    }
                }else{
                    self?.showErrorMessage(error: response["message"] as? String ?? "", isError: true )
                }
            }
        }
    }
    
    
    
    //MARK: - RESET EDIT PROFILE VIEW AFTER EDIT DONE OR CANCELLED
    func resetEditProfileView(){
        navigationBar?.rightButton.setTitle("Logout", for: .normal)
        navigationBar?.rightButton.isSelected = false
        setUserInfo()
        isProfileEditting = true
        self.view.endEditing(true)
        profileTableView.reloadData()
        //        bottomButtonConstraint.constant = 50
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutIfNeeded()
        }
        editorSaveButtonAction(showEdit: false)
        
        // navigationBar?.rightButton.setImage(#imageLiteral(resourceName: "logout"), for: UIControlState.normal)
        navigationBar?.backgroundColor = COLOR.App_Red_COLOR
        navigationBar?.rightButton.isHidden = false 
//        navigationBar?.rightButton.addTarget(self, action: Selector("logoutAction"), for: UIControlEvents.touchUpInside)
        // editorSaveButtonAction()
    }
    
    func logoutAction(){
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: { [weak self] in
            let alertController = UIAlertController(title: "", message: TEXT.ARE_YOU_WANT_LOGOUT_ALERT, preferredStyle: .actionSheet)
            let yesAction = UIAlertAction(title: TEXT.LOGOUT_ALERT, style: .destructive, handler: { (action) -> Void in
                Singleton.sharedInstance.logoutButtonAction()
                logEvent(label: "user_log_out")
            })
            let noAction = UIAlertAction(title: TEXT.CANCEL_ALERT, style: .cancel, handler: { (action) -> Void in
                self?.dismiss(animated: true, completion: nil)
            })
            alertController.addAction(yesAction)
            alertController.addAction(noAction)
            self?.present(alertController, animated: true, completion: nil)
        })
    }
    
    //MARK: NAVIGATION DELEGATE METHODS
    func backAction() {
        
        if isComingFromConfirmBooking {
            self.navigationController?.popViewController(animated: true)
        } else {
            presentLeftMenuViewController()
        }
        
//        return
//        if isProfileEditting == false {
//            self.view.endEditing(true)
//            _ = self.navigationController?.popViewController(animated: true)
//        }else {
//            Singleton.sharedInstance.showAlertWithOption(owner: self, title: "", message: "Are you sure you want to discard the changes ?", showRight: true, leftButtonAction: {[weak self] in
//                self?.view.endEditing(true)
//                self?.resetEditProfileView()
//                }, rightButtonAction: {
//                    print("nothing")
//            }, leftButtonTitle: "Yes", rightButtonTitle: "Cancel")
//
//
//        }
    }
    
    //MARK: - EDIT OR SAVE ACTION
    func editorSaveButtonAction(showEdit:Bool) {
        
        
        
        if showEdit == false {
            cellTypeArray = self.cellTypeArray.filter{$0 != FIELD_TYPE.newPassword && $0 != FIELD_TYPE.changepassword }
            if self.cellTypeArray.contains(FIELD_TYPE.password) == false    {
                self.cellTypeArray.append(FIELD_TYPE.password)
            }
            self.view.endEditing(true)
            isProfileEditting = false
            didMakeAnyChanges = false
            changePassWordHit = false
            isNeedToUpdateNewImage = false
            //    editProfileButton.setTitle("Edit", for: UIControlState.normal)
        }else
        {
            isProfileEditting = true
            cellTypeArray = self.cellTypeArray.filter{$0 != FIELD_TYPE.password}
            self.cellTypeArray.append(FIELD_TYPE.changepassword)
            self.cellTypeArray.append(FIELD_TYPE.newPassword)
            
            if let profileNameCell = self.profileTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? VendorNamePicCell {
                profileNameCell.editButtonAction()
            }
            //        editProfileButton.setTitle("Save", for: UIControlState.normal)
        }
        self.profileTableView.reloadData()
    }
    
    //MARK: NAVIGATION BAR
    func setNavigationBar() {
        navigationBar?.backgroundColor = COLOR.App_Red_COLOR
        
        
        if isComingFromConfirmBooking {
            navigationBar = NavigationView.getNibFile(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "Profile", leftButtonImage: #imageLiteral(resourceName: "BackButtonWhite"), rightButtonImage: nil)
                , leftButtonAction: {[weak self] in
                    self?.backAction()
                }, rightButtonAction: nil)
        } else {
            navigationBar = NavigationView.getNibFile(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "Profile", leftButtonImage: #imageLiteral(resourceName: "menu"), rightButtonImage: nil)
                , leftButtonAction: {[weak self] in
                    self?.backAction()
                }, rightButtonAction: nil)
        }
        
        self.view.addSubview(navigationBar!)
        navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
    }
    
    @IBAction func editAction(_ sender: UIButton) {
        if sender.titleLabel?.text == "Save" {
            if profileDetailsinfo.newPassWord == "" && profileDetailsinfo.oldPassWord == ""{
                changePassWordHit = false
            }
            
            
            if isNeedToUpdateNewImage == false   &&  didMakeAnyChanges == false  && changePassWordHit == true{
                checkValidation()
            }else{
                checkValidations()
            }
        }else{
            editorSaveButtonAction(showEdit:true)
        }
        
    }
    
    
    //MARK: - TABLE VIEW DELEGATES & DATASOURCE
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            return 238
        case 1:
            return 105
        case 2:
            return 180
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            if let vc = UIStoryboard(name: "Booking", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQUserSavedLocationsVC") as? BQUserSavedLocationsVC {
//                vc.isComingFromHome = true
                vc.isComingFromProfile = true
                vc.delegate = self
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else if indexPath.section == 2 {
           // gorToEditAddress(tag: indexPath.row)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 2 {
            if familyMenbers.count > 0 {
                return "ADDED MEMBERS"
            }
            
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            if familyMenbers.count > 0 {
                return 30
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 1
        case 2:
            return familyMenbers.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfilePictureCell", for: indexPath) as! ProfilePictureCell
            cell.selectionStyle =  .none
//            cell.phoneNumberTxtField.setFormatting("XXX-XXX-XXXX", replacementChar: "X")
//            cell.phoneNumberTxtField.text = Vendor.current?.phoneNo ?? ""
            cell.phoneNumberTxtField.text = Vendor.current?.phoneNo.applyPatternOnNumbers(pattern: "##-###-####", replacmentCharacter: "#")
            cell.username.text = Vendor.current?.username ?? ""
            cell.emailAddress.text = Vendor.current?.email ?? ""
            if let gender = Vendor.current?.gender {
                cell.gender.text = gender
            } else {
                cell.gender.isHidden = true
            }
            
            if let dob = Vendor.current?.dob {
                cell.dobLabel.text = dob
            } else {
                cell.dobLabel.isHidden = true
            }
            
            if let nationality = Vendor.current?.nationality {
                cell.nationality.text = nationality
            } else {
                cell.nationality.isHidden = true
            }
            
           // cell.phoneNumber.text = Vendor.current?.phoneNo ?? ""
            shoWProfileImage(on: cell.profilePic)
            cell.editProfile.addTarget(self, action: #selector(self.goToProfile), for: .touchUpInside)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier:  "AddressCell", for: indexPath) as! AddressCell
            cell.selectionStyle =  .none
            
            cell.addressLine1.text = "\(BQBookingModel.shared.addressLine1 ?? "" )" + " \(BQBookingModel.shared.addressLine2 ?? "" )"
            cell.addressLine2.text = BQBookingModel.shared.addressLine3
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddedMemberCell") as! AddedMemberCell
            cell.selectionStyle =  .none
            cell.backgroundColor = .clear
            cell.delegate = self
            cell.deleteButton.tag = indexPath.row
            cell.editMemberBtn.tag = indexPath.row
            cell.setupData(_data: familyMenbers[indexPath.row])
//            cell.data = familyMenbers[indexPath.row]
            return cell
        }
    }
    
    
    func gorToEditAddress(tag: Int) {
        let storyboard = UIStoryboard(name: STORYBOARD_NAME.main, bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "AddFamilyMemberViewController") as? AddFamilyMemberViewController {
            vc.editMember = true
            vc.data = familyMenbers[tag]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    func goToProfile() {
        if let vc = UIStoryboard(name: STORYBOARD_NAME.afterLogin, bundle: Bundle.main).instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    //MARK: - TEXTFIELD VALUE CHANGED & UPDATE DELGATE METHOD
    func profileValueChanged(fieldType: Int,yourText:String) {
        switch fieldType {
        case FIELD_TYPE.email.hashValue:
            guard yourText != profileDetailsinfo.email else {
                return
            }
            didMakeAnyChanges = true
            profileDetailsinfo.email = yourText
        case FIELD_TYPE.contact.hashValue:
            guard yourText != profileDetailsinfo.phoneNumber else {
                return
            }
            didMakeAnyChanges = true
            profileDetailsinfo.phoneNumber = yourText
        case FIELD_TYPE.companyName.hashValue:
            guard yourText != profileDetailsinfo.company else {
                return
            }
            didMakeAnyChanges = true
            profileDetailsinfo.company = yourText
        case FIELD_TYPE.address.hashValue:
            guard yourText != profileDetailsinfo.address else {
                return
            }
            didMakeAnyChanges = true
            profileDetailsinfo.address = yourText
        case FIELD_TYPE.name.hashValue :
            guard yourText != profileDetailsinfo.fullName else {
                return
            }
            didMakeAnyChanges = true
            profileDetailsinfo.fullName = yourText
            
        case FIELD_TYPE.newPassword.hashValue:
            guard yourText != profileDetailsinfo.newPassWord else {
                return
            }
            self.changePassWordHit = true
            profileDetailsinfo.newPassWord = yourText
        case FIELD_TYPE.changepassword.hashValue :
            guard yourText != profileDetailsinfo.oldPassWord else {
                return
            }
            self.changePassWordHit = true
            profileDetailsinfo.oldPassWord = yourText
        default:
            print("profileValueChanged")
        }
        
        print(profileDetailsinfo)
    }
    
    //MARK: - UDPATING NAME & IMAGE FROM TABLE CELL USING PROTOCOL
    func nameChanged(yourText: String) {
        guard yourText != profileDetailsinfo.fullName else {
            return
        }
        didMakeAnyChanges = true
        profileDetailsinfo.fullName = yourText
        
        let nameComponents = yourText.components(separatedBy: " ")
        
        guard nameComponents.count > 1  else {
            profileDetailsinfo.firstName = yourText
            profileDetailsinfo.lastName = ""
            return
        }
        
        profileDetailsinfo.firstName = nameComponents[0]
        
        for index in 1..<nameComponents.count {
            if index != 1 {
                profileDetailsinfo.lastName += " "
            }
            profileDetailsinfo.lastName += nameComponents[index]
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: ERROR MESSAGE
    func showErrorMessage(error:String, isError:Bool) {
        if errorMessageView != nil {
            self.removeErrorView()
        }
        if errorMessageView == nil {
            errorMessageView = UINib(nibName: NIB_NAME.errorView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
            errorMessageView.delegate = self
            errorMessageView.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: HEIGHT.errorMessageHeight)
            self.view.addSubview(errorMessageView)
            errorMessageView.setErrorMessage(message: error, isError: isError)
        }
    }
    
    //MARK: ERROR DELEGATE METHOD
    func removeErrorView() {
        if self.errorMessageView != nil {
            self.errorMessageView = nil
        }
    }
    @IBAction func addnewMemberAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: STORYBOARD_NAME.main, bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "AddFamilyMemberViewController") as? AddFamilyMemberViewController {
            vc.hideSkip = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func deleteMember(tag: Int) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.2, execute: { [weak self] in
            let alertController = UIAlertController(title: "", message: TEXT.ARE_YOU_WANT_Delete_ALERT, preferredStyle: .actionSheet)
            let yesAction = UIAlertAction(title: TEXT.YES_ACTION, style: .destructive, handler: { (action) -> Void in
                self?.delete(member: (self?.familyMenbers[tag])!,tag:tag)
            })
            let noAction = UIAlertAction(title: TEXT.CANCEL_ALERT, style: .cancel, handler: { (action) -> Void in
                self?.dismiss(animated: true, completion: nil)
            })
            alertController.addAction(yesAction)
            alertController.addAction(noAction)
            self?.present(alertController, animated: true, completion: nil)
        })
    }
    
    func delete(member: FamilyMember, tag: Int) {
        member.deleteMember { (success, response) in
            DispatchQueue.main.async {
                if success {
                    self.familyMenbers.remove(at: tag)
                    let indexpath = IndexPath(row: tag, section: 2)
                    self.profileTableView.deleteRows(at: [indexpath], with: .right)
                }
            }
        }
    }
}

extension VendorProfileVC: BQUserSavedLocationsVCDelegate {
    func selectedAddress(address: BQFavoriteAddressModel) {
        if let locType = address.locType {
            
            
            //Deepak App is getting crashed after getting any nil value in address
            if let thoroughfare = address.landmark {
                BQBookingModel.shared.addressLine1 = thoroughfare
            }
            
            if let postalCode = address.postal_code {
                BQBookingModel.shared.addressLine3 = postalCode
            }
            
            if let landmark = address.address {
                BQBookingModel.shared.addressLine2 = landmark
            }
            
            var tempAddString = [String]()
            let buildingNo = address.building_number ?? ""
            let floorNo = address.floor_number ?? ""
            let doorNo = address.door_number ?? ""
            let streetNo = address.street_number ?? ""
            
            if !buildingNo.isEmpty {
                tempAddString.append(buildingNo)
                BQBookingModel.shared.apartmentNo = buildingNo
            }
            if !floorNo.isEmpty {
                tempAddString.append(floorNo)
                BQBookingModel.shared.floorNo = floorNo
            }
            if !doorNo.isEmpty {
                tempAddString.append(doorNo)
                BQBookingModel.shared.doorNo = doorNo
            }
            if !streetNo.isEmpty {
                tempAddString.append(streetNo)
                BQBookingModel.shared.streetNo = streetNo
            }
            
            let additionalAddressString = tempAddString.joined(separator: ", ")
            
            BQBookingModel.shared.addressLine4 = additionalAddressString
                //buildingNo + ", " + floorNo + ", " + doorNo + ", " + streetNo
            
            BQBookingModel.shared.latitude = Double("\(address.latitude ?? "")")
            BQBookingModel.shared.longitude = Double("\(address.longitude ?? "")")
            
            profileTableView.reloadData()
        }
    }
    
    func setUserLocationFromMap(notification: NSNotification) {
        print("setUserLocationFromMap")
        if let userAddress = notification.object as? GMSReverseGeocodeResponse {
            // do something with your object
            print(userAddress)
            
            if (userAddress.results()?.count)! > 0{
                if userAddress.results()?[0].lines?.count != 0 {
                    if let lines = userAddress.firstResult()?.lines {
                        
                        //Deepak App is getting crashed after getting any nil value in address
                        if let thoroughfare = userAddress.results()?[0].thoroughfare {
                            BQBookingModel.shared.addressLine1 = thoroughfare
                        }
                        
                        if let postalCode = userAddress.results()?[0].postalCode {
                            BQBookingModel.shared.addressLine3 = postalCode
                        }
                        
                        
                        var middleAddress = ""
                        if let subLocality = userAddress.results()?[0].subLocality {
                            middleAddress = subLocality
                        }
                        if let locality = userAddress.results()?[0].locality {
                            if middleAddress.length != 0 {
                                middleAddress = middleAddress + ", " + locality
                            } else {
                                middleAddress = locality
                            }
                        }
                        if let country = userAddress.results()?[0].country {
                            if middleAddress.length != 0 {
                                middleAddress = middleAddress + ", " + country
                            } else {
                                middleAddress = country
                            }
                        }
                        BQBookingModel.shared.addressLine2 = middleAddress
                        
                        BQBookingModel.shared.latitude = userAddress.results()?[0].coordinate.latitude
                        BQBookingModel.shared.longitude = userAddress.results()?[0].coordinate.longitude
                        
                        profileTableView.reloadData()
                    }
                }
            }
        }
    }
    
}

extension VendorProfileVC: AddedMemberCellDelegate {
    func deleteButtonAction(tag: Int) {
        deleteMember(tag: tag)
    }
    
    func editButtonAction(tag: Int) {
        gorToEditAddress(tag: tag)
    }
}
