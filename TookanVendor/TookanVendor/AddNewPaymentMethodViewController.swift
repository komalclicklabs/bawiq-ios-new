//
//  AddNewPaymentMethodViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 27/02/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class AddNewPaymentMethodViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableVIew: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addUiAttributes()
        
        tableVIew.delegate = self
        tableVIew.dataSource = self
        
        tableVIew.register( UINib(nibName: NIB_NAME.PayementMethodCell, bundle: Bundle.main)    , forCellReuseIdentifier: CELL_IDENTIFIER.payementMethod)
        
        self.tableVIew.rowHeight = UITableViewAutomaticDimension
        self.tableVIew.estimatedRowHeight = 120
        self.tableVIew.separatorStyle = .none
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableVIew.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.payementMethod, for: indexPath) as! PaymentMethodTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func addUiAttributes(){
        setTitleView()
    }
    
    func setTitleView() {
        let titleView = UINib(nibName: NIB_NAME.titleView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TitleView
        titleView.frame = CGRect(x: 0, y: HEIGHT.navigationHeight, width: self.view.frame.width, height: HEIGHT.titleHeight)
        titleView.backgroundColor = UIColor.clear
        self.view.addSubview(titleView)
        titleView.setTitle(title: TEXT.ADD_NEW_PAYMENT, boldTitle: TEXT.METHOD)
    }
}
