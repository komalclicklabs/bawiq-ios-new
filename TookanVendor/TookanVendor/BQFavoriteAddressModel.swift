//
//  BQFavoriteAddressModel.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 07/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQFavoriteAddressModel: NSObject {
    var address: String?
    var default_location: String?
    var fav_id: Int?
    var landmark: String?
    var latitude: String?
    var locType: String?
    var longitude: String?
    var postal_code: String?
    var vendor_id: Int?
    var building_number: String?
    var floor_number: String?
    var door_number: String?
    var street_number: String?
    
    convenience init(param: [String: Any]) {
        self.init()
        
        if let accessoryID = param["address"] as? String {
            self.address = accessoryID
        }
        if let accessoryID = param["default_location"] as? String {
            self.default_location = accessoryID
        }
        if let accessoryID = param["fav_id"] as? Int {
            self.fav_id = accessoryID
        }
        if let accessoryID = param["landmark"] as? String {
            self.landmark = accessoryID
        }
        if let accessoryID = param["latitude"] as? String {
            self.latitude = accessoryID
        }
        if let accessoryID = param["locType"] as? Int {
            if accessoryID == 0 {
                self.locType = "Home"
            } else if accessoryID == 1 {
                self.locType = "Work"
            } else {
                self.locType = "Other"
            }
            
        }
        if let accessoryID = param["longitude"] as? String {
            self.longitude = accessoryID
        }
        if let accessoryID = param["postal_code"] as? String {
            self.postal_code = accessoryID
        }
        if let accessoryID = param["vendor_id"] as? Int {
            self.vendor_id = accessoryID
        }
        if let building_number = param["building_number"] as? String {
            self.building_number = building_number
        }
        if let floor_number = param["floor_number"] as? String {
            self.floor_number = floor_number
        }
        if let door_number = param["door_number"] as? String {
            self.door_number = door_number
        }
        if let street_number = param["street_number"] as? String {
            self.street_number = street_number
        }
    }
}
