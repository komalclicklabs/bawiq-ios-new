//
//  carModal.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 22/03/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation

class carModal{
    var eta = Double()
    var fleetId = String()
    var latitude = Double()
    var longitude = Double()
    
    
    
    init(json:[String:Any]) {
        if let value = json["eta"] as? Double{
            self.eta = value
        }else if let value = json["eta"] as? String{
            self.eta = Double(value)!
        }
        
        if let value = json["latitude"] as? Double{
            self.latitude = value
        }else if let value = json["latitude"] as? String{
            self.latitude = Double(value)!
        }
        
        if let value = json["longitude"] as? Double{
            self.longitude = value
        }else if let value = json["longitude"] as? String{
            self.longitude = Double(value)!
        }
    }
    
    
}
