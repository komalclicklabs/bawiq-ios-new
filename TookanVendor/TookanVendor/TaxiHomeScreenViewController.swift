//
//  TaxiHomeScreenViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 22/03/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import CoreLocation


protocol updateMetaData:class {
    func updateMetaData(data:[Any],date:String,currency:String,paymentMethod:String,cardId:String,ammount:String,isNow:Bool,tip:String)
}

class TaxiHomeScreenViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,CLLocationManagerDelegate,GMSMapViewDelegate,updateMetaData ,CAAnimationDelegate{
  
  // MARK: - IBOUTLETS
    @IBOutlet weak var viewForLoader: UIView!
  @IBOutlet weak var scheduleButtoBackgroundView: UIView!
  @IBOutlet weak var buttonWidth: NSLayoutConstraint!
  @IBOutlet weak var scheduleButton: UIButton!
  @IBOutlet weak var bottomView: UIView!
  @IBOutlet weak var makeBookingButton: UIButton!
  @IBOutlet weak var collectionView: UICollectionView!
  @IBOutlet weak var jobPickUpLabel: MarqueeLabel!
  @IBOutlet weak var jobDropOffLabel: MarqueeLabel!
  @IBOutlet weak var mapView: GMSMapView!
  @IBOutlet weak var locationVIew: UIView!
  @IBOutlet weak var scheduleLabel: UILabel!
  @IBOutlet weak var scheduleImahe: UIImageView!
  @IBOutlet weak var currentLocationButton: UIButton!
  @IBOutlet weak var removeDestinatioButton: UIButton!
  @IBOutlet weak var backButton: UIButton!
    
  var gradientLayer:CAGradientLayer!
  var toColors = [CGColor]()//: [AnyObject] = [UIColor.cyan.cgColor, UIColor.blue.cgColor, UIColor.yellow.cgColor, UIColor.green.cgColor, UIColor.red.cgColor, UIColor.orange.cgColor]
  var fromColor = [CGColor]()//[UIColor.blue.cgColor, UIColor.yellow.cgColor, UIColor.green.cgColor, UIColor.red.cgColor, UIColor.orange.cgColor, UIColor.cyan.cgColor]
  var stopGradientAnimation = false
  
  var inRequest = false
  var bookingView: ScheduleBookingView?
  var pickupCustomFieldTemplate = "taxi-template"
  let locationManager = CLLocationManager()
  var selectedLat = Double()
  var selectedLong = Double()
  var isPickUp = Bool()
  var toUpdate = Bool()
  var getEtaTimer = Timer()
  var selected = 0
  var pickUpMetaData = [Any]()
  var noDataFound = Bool()
  var currentJobId = String()
  var totalWatingTime = Int()
  var currentLocationLat = Double()
  var currentLocationLong = Double()
  var changeBounds = true
  var adjustZoom = true
  var toReverseGeoCode = false
  
  let maxZoom : Float = 16
  var tohidebackButton  = true
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addGradientLayer()
    viewForLoader.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
    bottomView.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
    makeBookingButton.isExclusiveTouch = true
    scheduleButton.isExclusiveTouch = true
    collectionView.showsHorizontalScrollIndicator = false
    removeDestinatioButton.setImage(#imageLiteral(resourceName: "clearWithShadow"), for: UIControlState.normal)
    removeDestinatioButton.isHidden = true
    currentLocationButton.backgroundColor = UIColor.white
    setGoogleMap()
    checkIfScheduleIsEnabled()
    backButton.isHidden = tohidebackButton
    configureUi()
    mapView.settings.allowScrollGesturesDuringRotateOrZoom = false
    giveTextColor()
    collectionView.delegate = self
    collectionView.dataSource = self
    locationManager.delegate = self
    mapView.delegate = self
    Singleton.sharedInstance.resetPickupMetaData()
    makeMetaData()
    setGradientLayer()
    mapView.isMyLocationEnabled = false
    
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.3) {
      self.toReverseGeoCode = true
      self.reverseGeocodeCoordinate(coordinate: self.mapView.camera.target)
    }

    apiHitToGetNearestDriverETA()
    
    collectionView.register(UINib(nibName: NIB_NAME.TaxiCarTypes, bundle: Bundle.main), forCellWithReuseIdentifier: CELL_IDENTIFIER.carTypeItem)
    
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
    locationManager.startUpdatingLocation()
    currentLocationButton.isHidden = false
    currentLocationButton.setCornerRadius(radius: currentLocationButton.frame.width/2)
    toUpdate = true
  }
  
    
    override func viewDidAppear(_ animated: Bool) {
        //self.marqueLabel.restartLabel()
        Singleton.sharedInstance.enableDisableIQKeyboard(enable: true)
    }
    
  override func viewWillAppear(_ animated: Bool) {
    //Setting Up Timer for Geting updated location of drivers
    Singleton.sharedInstance.resetPickupMetaData()
    getEtaTimer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(TaxiHomeScreenViewController.apiHitToGetNearestDriverETA), userInfo: nil, repeats: true  )
    
    inRequest = false
    
    
    if Singleton.sharedInstance.selectedDestinationLatForTaxi != Double(){
      self.removeDestinatioButton.isHidden = false
    }else{
      self.removeDestinatioButton.isHidden = true
    }
    
    self.jobDropOffLabel.text = Singleton.sharedInstance.selectedDestinationAddressForTaxi
    
    //Update on recieving notification
    NotificationCenter.default.addObserver(self, selector: #selector(self.apiHitToGetNearestDriverETA), name: NSNotification.Name(rawValue: NOTIFICATION_OBSERVER.updateOnPushNotification), object: nil)
  }
  
  
  override func viewWillDisappear(_ animated: Bool) {
    getEtaTimer.invalidate()
    inRequest = false
    NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFICATION_OBSERVER.updateOnPushNotification), object: nil)
  }
  
  
  func configureUi(){
    makeBookingButton.backgroundColor = UIColor.clear
    makeBookingButton.titleLabel?.font = UIFont(name: FONT.light, size: FONT_SIZE.large)
    makeBookingButton.setTitle(TEXT.proceedToPay.capitalized, for: UIControlState.normal)
    jobPickUpLabel.font = UIFont(name: FONT.ultraLight, size: FONT_SIZE.buttonTitle)
    jobDropOffLabel.font = UIFont(name: FONT.ultraLight, size: FONT_SIZE.buttonTitle)
    jobPickUpLabel.text = TEXT.PickUpLocation
    jobDropOffLabel.text = TEXT.DropLocation
    Singleton.sharedInstance.selectedDestinationAddressForTaxi = TEXT.DropLocation
    Singleton.sharedInstance.selectedDestinationlongForTaxi = Double()
    Singleton.sharedInstance.selectedDestinationLatForTaxi = Double()
  }
  
  
  @IBAction func jobDropOffLocation(_ sender: UIButton) {
    isPickUp = false
    toUpdate = false
    let autocompleteController = GMSAutocompleteViewController()
    
    let latestLat = self.selectedLat
    let latestLong = self.selectedLong
    let currentLocation = CLLocation(latitude: latestLat, longitude: latestLong)
    
    autocompleteController.autocompleteBounds = GMSCoordinateBounds(coordinate: currentLocation.coordinate, coordinate: currentLocation.coordinate)
    
    autocompleteController.delegate = self
    let vc: AddNewLocationViewController?
    vc = UIViewController.findIn(storyboard: .favLocation, withIdentifier: STORYBOARD_ID.addLocation) as? AddNewLocationViewController
    vc?.completionHandler = {(coordinate,address) in
        logEvent(label: "taxi_drop_location_added_map")
        self.updatemap(coordinate: coordinate, address: address)
    }
    if IJReachability.isConnectedToNetwork() == true{
    self.navigationController?.pushViewController(vc!, animated: true)
    }else{
        ErrorView.showWith(message: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isErrorMessage: true, removed: nil)
    }
    
    // self.present(autocompleteController, animated: true, completion: nil)
    
    
  }
  @IBAction func bookNowButtonAction(_ sender: UIButton) {
    logEvent(label: "taxi_ride_now")
    if jobPickUpLabel.text != TEXT.GettingAddress && jobPickUpLabel.text != TEXT.PickUpLocation{
      if IJReachability.isConnectedToNetwork() == true{
        if Singleton.sharedInstance.typeCategories[selected].carArray.count != 0{
          
          
          if Singleton.sharedInstance.selectedDestinationLatForTaxi == Double(){
            if Singleton.sharedInstance.formDetailsInfo.isDestinationRequired != "1"{
              let vc = UIStoryboard(name: STORYBOARD_NAME.afterLogin, bundle: Bundle.main).instantiateViewController(withIdentifier: STORYBOARD_ID.payment) as? NewPaymentScreenViewController
              
              vc?.isDeliveryAdded = false
              vc?.amount = ""
              vc?.selected = self.selected
              print(self.selected)
              vc?.selectedLat = self.selectedLat
              vc?.selectedLong = self.selectedLong
              vc?.ammountLabelText = TEXT.estimatedCost
              vc?.delegate = self
              
              self.navigationController?.pushViewController(vc!, animated: true)
            }else{
              self.showErrorMessage(error: TEXT.pleaseEnterTheDropLocation, isError: true)
            }
          }else{
            makeMetaData()
            let param = [
              "access_token":"\(Vendor.current!.appAccessToken!)",
              "device_token":"\(APIManager.sharedInstance.getDeviceToken())",
              "app_version":"\(APP_VERSION)",
              "app_device_type":APP_DEVICE_TYPE,
              "app_access_token":"\(Vendor.current!.appAccessToken!)",
              "pickup_latitude":"\(selectedLat)",
              "pickup_longitude":"\(selectedLong)",
              "user_id":"\(Singleton.sharedInstance.formDetailsInfo.user_id!)",
              "pickup_meta_data":self.pickUpMetaData,
              "vendor_id":"\(Vendor.current!.id!)",
              "payment_method":"\(Singleton.sharedInstance.formDetailsInfo.paymentMethodArray[0])",
              "layout_type":"\(Singleton.sharedInstance.formDetailsInfo.work_flow!)",
              "has_pickup":"1",
              "has_delivery":"0",
              "form_id":Singleton.sharedInstance.formDetailsInfo.form_id!
              ] as [String : Any]
            
            print(param)
            
            ActivityIndicator.sharedInstance.showActivityIndicator()
            APIManager.sharedInstance.serverCall(apiName: "fare_estimate", params: param as [String : AnyObject]?, httpMethod: "POST", receivedResponse: { (isSuccess, response) in
              ActivityIndicator.sharedInstance.hideActivityIndicator()
              
              if isSuccess == true    {
                if let data = response["data"] as? [String:Any]{
                  if let value = data["currency"] as? [String:Any]{
                    guard let currencyValue = Currency(json: value) else {
                      return
                    }
                    if let totalCost = data["fare_lower_limit"] as? NSNumber{
                      if let upperCost = data["fare_upper_limit"] as? NSNumber{
                        let vc = UIStoryboard(name: STORYBOARD_NAME.afterLogin, bundle: Bundle.main).instantiateViewController(withIdentifier: STORYBOARD_ID.payment) as? NewPaymentScreenViewController
                        vc?.amount = "\(currencyValue.symbol) \(totalCost.intValue) - \(currencyValue.symbol) \(upperCost.intValue)"
                        vc?.delegate = self
                        vc?.selectedLat = self.selectedLat
                        vc?.selectedLong = self.selectedLong
                        
                        vc?.selected = self.selected
                        vc?.selectedDropLat = Singleton.sharedInstance.selectedDestinationLatForTaxi
                        vc?.selectedDropLong = Singleton.sharedInstance.selectedDestinationlongForTaxi
                        vc?.selectedDestinationAddress = self.jobDropOffLabel.text!
                        vc?.ammountLabelText = TEXT.estimatedCost
                        
                        self.navigationController?.pushViewController(vc!, animated: true)
                      }
                    }
                  }
                }
              }else{
                if let value = response["message"] as? String{
                  self.showErrorMessage(error: value, isError: true)
                }
              }
              print(response)
            })
          }
        }else{
          showErrorMessage(error: TEXT.NoCarsFoundMessage, isError: true)
        }
      }else{
        self.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: true)
      }
    }else{
      ErrorView.showWith(message: "Please wait while we are fetching the location.", isErrorMessage: true, removed: nil)
    }
  }
  
  
  @IBAction func laterBookingButton(_ sender: UIButton) {
    logEvent(label: "taxi_scheduled")
    if IJReachability.isConnectedToNetwork() == true{
      if let topParent = parent as? GenricHomeWithSideMenuViewController{
        topParent.showOrHideMenuButton.isHidden = true
      }
      bookingView = UINib(nibName: NIB_NAME.scheduleViewId, bundle: nil).instantiate(withOwner: self, options: nil)[0] as? ScheduleBookingView
      bookingView?.frame = CGRect(x: 0, y: 0, width: SCREEN_SIZE.width, height: SCREEN_SIZE.height )
      bookingView?.onDoneAction = {(date,dateInDateFormat) in
        print(dateInDateFormat)
        print(date)
        self.forLaterBookings(dateInString: date, dateInDateFormat: dateInDateFormat)
        if let topParent = self.parent as? GenricHomeWithSideMenuViewController{
          topParent.showOrHideMenuButton.isHidden = false
        }
      }
      bookingView?.cancelAction = {
        if let topParent = self.parent as? GenricHomeWithSideMenuViewController{
          topParent.showOrHideMenuButton.isHidden = false
        }
      }
      bookingView?.textfieldUpperConstraint.constant = 60
      bookingView?.overlay.alpha = 1
      self.bookingView?.scheduleBookingTextView.becomeFirstResponder()
      self.view.addSubview(bookingView!)
      //        UIView.animate(withDuration: 0, animations: {
      //            self.bookingView?.overlay.alpha = 1
      //        }) { (true) in
      //            self.bookingView?.textfieldUpperConstraint.constant = 60
      //            self.bookingView?.scheduleBookingTextView.becomeFirstResponder()
      //            UIView.animate(withDuration: 0.2, animations: {
      //                self.view.layoutIfNeeded()
      //            }, completion: { (true) in
      //
      //            })
      //        }
    }else{
      self.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: true)
    }
  }
  
  
  @IBAction func jobPickUpLocationAction(_ sender: UIButton) {
    isPickUp = true
    toUpdate = true
    let autocompleteController = GMSAutocompleteViewController()
    autocompleteController.delegate = self
    let latestLat = self.selectedLat
    let latestLong = self.selectedLong
    let currentLocation = CLLocation(latitude: latestLat, longitude: latestLong)
    
    autocompleteController.autocompleteBounds = GMSCoordinateBounds(coordinate: currentLocation.coordinate, coordinate: currentLocation.coordinate)
    
//    self.present(autocompleteController, animated: true, completion: nil)
    let vc: AddNewLocationViewController?
    vc = UIViewController.findIn(storyboard: .favLocation, withIdentifier: STORYBOARD_ID.addLocation) as? AddNewLocationViewController
    vc?.completionHandler = {(coordinate,address) in
        
        self.updatemap(coordinate: coordinate, address: address)
    }
    if IJReachability.isConnectedToNetwork() == true{
        self.navigationController?.pushViewController(vc!, animated: true)
    }else{
        ErrorView.showWith(message: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isErrorMessage: true, removed: nil)
    }
  }
  
  
  
  
  
  
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    removeTimer()
    if let topParent = self.parent as? GenricHomeWithSideMenuViewController{
      topParent.showOrHideMenuButton.isHidden = false
    }
  }
  
  
  func removeTimer(){
    if self.bookingView != nil{
      UIView.animate(withDuration: 0.4, animations: {
        self.bookingView?.textfieldUpperConstraint.constant = -60
      }, completion: { (true) in
        UIView.animate(withDuration: 0.5, animations: {
          self.bookingView?.overlay.alpha = 0
        }, completion: { (true) in
          self.bookingView?.removeFromSuperview()
        })
      })
    }
  }
  
  
  
  //UICOLLECTION VIEW DELEGATE
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return Singleton.sharedInstance.typeCategories.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CELL_IDENTIFIER.carTypeItem, for: indexPath) as? TaxiCarTypes
    
    cell?.carTypeName.text = Singleton.sharedInstance.typeCategories[indexPath.row].carTypeName
     cell?.carTypeName.textColor = UIColor(colorLiteralRed: 51/255, green: 51/255, blue: 51/255, alpha: 1)
    cell?.carTypeName.letterSpacing = 0.5
    
    if Int(Singleton.sharedInstance.typeCategories[indexPath.row].eta) != nil {
      if Int(Singleton.sharedInstance.typeCategories[indexPath.row].eta) != 0{
        cell?.timeLabel.text = "\(Int(Singleton.sharedInstance.typeCategories[indexPath.row].eta)) Min"
      }else{
        cell?.timeLabel.text = "\(1) Min"
      }
    }else{
      cell?.timeLabel.text = "\(Singleton.sharedInstance.typeCategories[indexPath.row].eta) Min"
    }
    if Singleton.sharedInstance.typeCategories[indexPath.row].carArray.count == 0 || noDataFound == true {
      cell?.timeLabel.text = "unavailable"
    }
    if selected == indexPath.row{
      cell?.carImage.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
      cell?.carImage.setImageUrl(urlString: Singleton.sharedInstance.typeCategories[indexPath.row].carImageString, placeHolderImage: #imageLiteral(resourceName: "placeHolder"), indexPath: indexPath, view: collectionView)
    }else{
      
      cell?.carImage.backgroundColor = COLOR.SPLASH_TEXT_COLOR.withAlphaComponent(0.4)
      cell?.carImage.setImageUrl(urlString: Singleton.sharedInstance.typeCategories[indexPath.row].carImageString, placeHolderImage: #imageLiteral(resourceName: "placeHolder"), indexPath: indexPath, view: collectionView)
        
    }
    cell?.carTypeName.numberOfLines = 1
    cell?.timeLabel.letterSpacing = 0.5
    return cell!
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    if Singleton.sharedInstance.typeCategories.count <= 3{
      return CGSize(width: collectionView.frame.width/CGFloat(Singleton.sharedInstance.typeCategories.count), height: collectionView.frame.height)
    }else{
      return CGSize(width: collectionView.frame.width/CGFloat(3.5), height: collectionView.frame.height)
    }
  }
  
  
  func reverseGeocodeCoordinate(coordinate: CLLocationCoordinate2D) {
    print(coordinate.latitude)
    print(coordinate.longitude)
    if IJReachability.isConnectedToNetwork()  == true   {
      let geocoder = GMSGeocoder()
      if toUpdate == true {
        self.jobPickUpLabel.text = TEXT.GettingAddress
      }
      geocoder.reverseGeocodeCoordinate(coordinate) {[weak self] response, error in
        if  response != nil {
          if (response?.results()?.count)! > 0{
            if self?.toUpdate == true{
              if response?.results()?[0].lines?.count != 0{
                let finalAddress = response?.results()?[0].lines?.reduce(" ", +)
                self?.jobPickUpLabel.text = finalAddress
              }
            }else{
              self?.toUpdate = true
            }
          }else{
            self?.jobPickUpLabel.text = TEXT.goToPinText
          }
        }else{
          self?.jobPickUpLabel.text = TEXT.goToPinText
        }
      }
    }
  }
  
  
  func makeMetaData()  {
    var pickUpMentaData = [[String:String]]()
    for i in Singleton.sharedInstance.createTaskDetail.pickupMetadata {
      if let data  = i as? CustomFieldDetails {
        pickupCustomFieldTemplate = data.template_id!
        print(pickupCustomFieldTemplate)
        switch data.label! {
        case "baseFare":
          pickUpMentaData.append(["label":data.label!,"data":Singleton.sharedInstance.typeCategories[selected].baseFare])
        case "destinationAddress":
          pickUpMentaData.append(["label":data.label!,"data":jobDropOffLabel.text!])
        case "destinationLatitude":
          
          
          pickUpMentaData.append(["label":data.label!,"data":"\(Singleton.sharedInstance.selectedDestinationLatForTaxi == Double() ? "":"\(Singleton.sharedInstance.selectedDestinationLatForTaxi)")"])
          
        case "destinationLongitude":
          pickUpMentaData.append(["label":data.label!,"data":"\(Singleton.sharedInstance.selectedDestinationlongForTaxi == Double() ? "" : "\(Singleton.sharedInstance.selectedDestinationlongForTaxi)")"])
          
        case "categoryId":
          pickUpMentaData.append(["label":data.label!,"data":"\(Singleton.sharedInstance.typeCategories[selected].categoryId)"])
        case "paymentMode":
          pickUpMentaData.append(["label":data.label!,"data":"\(paymentTypeDict["\(Singleton.sharedInstance.formDetailsInfo.payementMethod)"]!)"])
        case "paymentMethod":
          pickUpMentaData.append(["label":data.label!,"data":"\(Singleton.sharedInstance.formDetailsInfo.payementMethod)"])
        case "cancellationCharges":
          pickUpMentaData.append(["label":data.label!,"data":"\(Singleton.sharedInstance.typeCategories[selected].cancellation_charges)"])
            
        case "distanceFare" :
            
            pickUpMentaData.append(["label":data.label!,"data":"\(Singleton.sharedInstance.typeCategories[selected].perKilometerCharges)"])
            
        case "timeFare" :
            
            pickUpMentaData.append(["label":data.label!,"data":"\(Singleton.sharedInstance.typeCategories[selected].perMinuteCharges)"])
            
        default:
          pickUpMentaData.append(["label":data.label!,"data":data.data!])
        }
      }
    }
    print(pickUpMentaData)
    print(Singleton.sharedInstance.typeCategories[selected])
    self.pickUpMetaData = pickUpMentaData
  }
  
  func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
    
    let newLoc = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
    
    let distance = newLoc.distance(from: CLLocation(latitude:self.selectedLat, longitude: self.selectedLong))
    if distance > 70{
      if changeBounds == true{
        if self.toReverseGeoCode == true    {
          self.reverseGeocodeCoordinate(coordinate: position.target)
        }
      }else{
        changeBounds = true
      }
      
      let currentLocation = CLLocation(latitude: self.currentLocationLat, longitude: self.currentLocationLong)
      print(currentLocation.distance(from: CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)))
      if currentLocation.distance(from: CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)) < 60{
        self.hideOrShowCurrentLocationButton(hide: true)
      }else{
        self.hideOrShowCurrentLocationButton(hide: false)
      }
      
      updateSelectedLatAndLong(coordinate: position.target)
      apiHitToGetNearestDriverETA()
    }
    
  }
  
  //MARK: LOCATION MANAGER DELEGATE FUNCTIONS
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    print(locations[0].coordinate.latitude)
    
    mapView.isMyLocationEnabled = true
    updateSelectedLatAndLong(coordinate: locations[0].coordinate)
    
    self.currentLocationLat = locations[0].coordinate.latitude
    self.currentLocationLong = locations[0].coordinate.longitude
    if self.toReverseGeoCode == true    {
      reverseGeocodeCoordinate(coordinate:  locations[0].coordinate)
    }
    self.mapView.animate(to: GMSCameraPosition(target: locations[0].coordinate, zoom: Float(maxZoom), bearing: 0, viewingAngle: 0))
    locationManager.stopUpdatingLocation()
  }
  
  func updateSelectedLatAndLong(coordinate:CLLocationCoordinate2D){
    selectedLat = coordinate.latitude
    selectedLong = coordinate.longitude
  }
  
  func updateSelectedLatAndLongForDrop(coordinate:CLLocationCoordinate2D){
    Singleton.sharedInstance.selectedDestinationLatForTaxi = coordinate.latitude
    Singleton.sharedInstance.selectedDestinationlongForTaxi = coordinate.longitude
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    selected = indexPath.row
    showMarkers(ForIndex: selected)
    collectionView.reloadData()
  }
  
  
  
  func apiHitToGetNearestDriverETA(){
    if inRequest == false   {
      let param = [
        "access_token":"\(Vendor.current!.appAccessToken!)",
        "device_token":"\(APIManager.sharedInstance.getDeviceToken())",
        "app_version":"\(APP_VERSION)",
        "app_device_type":"\(APP_DEVICE_TYPE)",
        "user_id":"\(Vendor.current!.userId!)",
        "app_access_token":"\(Vendor.current!.appAccessToken!)",
        "latitude":"\(selectedLat)",
        "longitude":"\(selectedLong)",
        "form_id" : Singleton.sharedInstance.formDetailsInfo.form_id
      ]
      print(param)
        stopGradientAnimation = false
        self.setGradientLayer()
      APIManager.sharedInstance.serverCall(apiName: "taxi_eta", params: param as [String : AnyObject]?, httpMethod: "POST") { (isSuccess, response) in
        print(response)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.7, execute: { [weak self] in
            self?.stopGradientAnimation = true
        })
        
        if isSuccess == true    {
        if let data = response["data"] as? [Any]{
          Singleton.sharedInstance.typeCategories.removeAll()
          for i in data {
            Singleton.sharedInstance.typeCategories.append(CarTypeModal(json: i as! [String : Any]))
          }
        }
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.8, execute: {
          self.showMarkers(ForIndex: self.selected)
          self.collectionView.reloadData()
        })
        }else{
            ErrorView.showWith(message: response["message"] as! String , isErrorMessage: true, removed: nil)
            self.navigationController?.popViewController(animated: true)
        }
      }
    }else{
      
      let orderParams = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,"vendor_id":String( Vendor.current!.id!),"app_device_type": APP_DEVICE_TYPE,"job_id":self.currentJobId] as [String : Any]
      print(orderParams)
      APIManager.sharedInstance.serverCall(apiName: API_NAME.getOrderHistory, params: orderParams as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) {
        [weak self](isSuccess, response) in
        print(response)
        if let data = response["data"] as? [Any]{
          if data.count > 0 {
            if let json = data[0] as? [String:Any]{
              if let value = json["job_status"] as? Int{
                print(value)
                self?.changeUiAccordingTo(jobStatus: value)
                self?.totalWatingTime = (self?.totalWatingTime)! + 30
                if (self?.totalWatingTime)! >= Singleton.sharedInstance.formDetailsInfo.waiting_screen_time + 30{
                  self?.inRequest = false
                  self?.resetController()
                  if [0,1,2,4,5,7].contains(value) == false {
                    self?.showErrorMessage(error: "Unable to assign a driver.", isError: true)
                  }
                  return
                }
                
              }
            }
          }
        }
        
      }
    }
  }
  
  
  @IBAction func gotToCurrentLoc(_ sender: UIButton) {
    self.locationManager.startUpdatingLocation()
  }
  
  func showMarkers(ForIndex:Int){
    
    mapView.clear()
    
    let latestLat = self.selectedLat
    let latestLong = self.selectedLong
    let currentLocation = CLLocation(latitude: latestLat, longitude: latestLong)
    if Singleton.sharedInstance.typeCategories.count > 0{
      Singleton.sharedInstance.typeCategories[ForIndex].carArray.sort { (first, second) -> Bool in
        if currentLocation.distance(from: CLLocation(latitude: first.latitude, longitude: first.longitude)) > currentLocation.distance(from: CLLocation(latitude: second.latitude, longitude: second.longitude)){
          return false
        }else{
          return true
        }
      }
      
      
      if  Singleton.sharedInstance.typeCategories[ForIndex].carArray.count > 0 && currentLocation.distance(from: CLLocation(latitude: Singleton.sharedInstance.typeCategories[ForIndex].carArray[0].latitude, longitude: Singleton.sharedInstance.typeCategories[ForIndex].carArray[0].longitude)) > 650 && self.adjustZoom == true {
        
        
        let mapCircle = GMSCircle(position: currentLocation.coordinate, radius: currentLocation.distance(from: CLLocation(latitude: Singleton.sharedInstance.typeCategories[ForIndex].carArray[0].latitude, longitude: Singleton.sharedInstance.typeCategories[ForIndex].carArray[0].longitude)))
        let camera = GMSCameraUpdate.fit(mapCircle.bounds())
        mapView.animate(with: camera)
        self.adjustZoom = false
        self.changeBounds = false
        
        
      }else{
        
        if self.mapView.camera.zoom >= 20 || self.mapView.camera.zoom <= 9{
          self.mapView.animate(toZoom: Float(self.maxZoom))
        }else if  self.adjustZoom  == true  {
          self.mapView.animate(toZoom: Float(self.maxZoom))
        }
        //self.changeBounds = false
        
      }
      
      for i in Singleton.sharedInstance.typeCategories[ForIndex].carArray{
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: i.latitude, longitude: i.longitude)
        marker.icon = #imageLiteral(resourceName: "movingCar")
        marker.map = mapView
      }
    }else{
      getEtaTimer.invalidate()
    }
  }
  
  //ERROR DELEGATE METHODS
  func showErrorMessage(error:String,isError:Bool) {
    ErrorView.showWith(message: error, isErrorMessage: isError, removed: nil)
  }
  
  func changeUiAccordingTo(jobStatus:Int) {
    switch jobStatus {
    case 6,11:
      print("continue")
    case 8,9,3,10:
      self.inRequest = false
      self.resetController()
      self.showErrorMessage(error: "Unable to assign a driver.", isError: true)
    case 0,1,2,4,5,7:
      if self.inRequest == true   {
        self.sendRequestForTrackAgent(jobId: self.currentJobId)
      }else{
        if bottomView.alpha == 0{
          self.resetController()
        }
      }
    default:
      print("continue")
    }
  }
  
    func updateMetaData(data: [Any], date: String, currency: String, paymentMethod: String, cardId: String,ammount:String,isNow:Bool,tip:String) {
    if data.count != 0{
      self.pickUpMetaData = data
    }else{
      makeMetaData()
    }
    
    ActivityIndicator.sharedInstance.showActivityIndicator()
    APIManager.sharedInstance.createTaxiTaskHit(url: "create_booking", autoAssignment: Singleton.sharedInstance.formDetailsInfo.auto_assign!,
                                                customerAddress: jobPickUpLabel.text!,
                                                customerEmail: Vendor.current!.email!,
                                                customerPhone: Vendor.current!.phoneNo,
                                                customerUsername: Vendor.current!.firstName! + " " + Vendor.current!.lastName! ,
                                                latitude: "0",
                                                longitude: "0",
                                                jobDeliveryDatetime: "",
                                                hasDelivery: 0,
                                                hasPickup: 1,
                                                jobDescription: "",
                                                jobPickupAddress: jobPickUpLabel.text!,
                                                jobPickupEmail: Vendor.current!.email!,
                                                jobPickupPhone: Vendor.current!.phoneNo,
                                                jobPickupDateTime: "\(date)",
                                                jobPickupLatitude: "\(selectedLat)",
                                                jobPickupLongitude: "\(selectedLong)",
                                                jobPickupName: Vendor.current!.firstName! + " " + Vendor.current!.lastName!,
                                                domainName:Singleton.sharedInstance.formDetailsInfo.domain_name! ,
                                                layoutType: Singleton.sharedInstance.formDetailsInfo.work_flow!,
                                                vendorId: Vendor.current!.id!,
                                                metadata: [],
                                                pickupMetadata: self.pickUpMetaData,
                                                pickupCustomFieldTemplate: self.pickupCustomFieldTemplate,
                                                customFieldTemplate: "",
                                                withPayment: true,
                                                ammount: ammount,
                                                currency: currency,
                                                payment_method: paymentMethod,
                                                card_id: cardId,
                                                tags :Singleton.sharedInstance.typeCategories[selected].carTypeName,
                                                tipAmount:tip) { (isSuccess, Response) in
        print(Response)
        //ActivityIndicator.sharedInstance.hideActivityIndicator()
        if isSuccess == true    {
          if let data = Response["data"] as? [String:Any]{
            
            
            if isNow == false   {
              ActivityIndicator.sharedInstance.hideActivityIndicator()
              self.inRequest = false
              if let message = Response["message"] as? String{
                self.showErrorMessage(error: message, isError: false )
              }
              self.resetController()
            }else{
              
              if let value = data["job_id"] as? String{
                self.currentJobId = value
              }else if let value = data["job_id"] as? Int{
                self.currentJobId = "\(value)"
              }
              
              self.inRequest = true
              self.mapView.animate(toZoom: self.maxZoom)
              self.mapView.clear()
              
              
              UIView.animate(withDuration: 0.5, animations: {
                self.bottomView.alpha = 0
                self.locationVIew.alpha = 0
              }, completion: { (true) in
                ActivityIndicator.sharedInstance.pleaseWait = true
              })
            }
          }
          
          //                                                        if let message = Response["message"] as? String{
          //
          //                                                          self.showErrorMessage(error: message, isError: false )
          //                                                        }
          //                                                        self.resetController()
          
        }else{
          self.inRequest = false
          ActivityIndicator.sharedInstance.hideActivityIndicator()
          if let message = Response["message"] as? String{
            self.showErrorMessage(error: message, isError: true )
          }
          self.resetController()
        }
    }
    
    
  }
  
  
  func sendRequestForTrackAgent(jobId:String) {
    
    let params = ["access_token":UserDefaults.standard.value(forKey: USER_DEFAULT.accessToken) as! String,"job_id":jobId,"app_device_type": APP_DEVICE_TYPE,"request_type":"1"] as [String : Any]
    APIManager.sharedInstance.serverCall(apiName: API_NAME.trackAgent, params: params as [String : AnyObject]?, httpMethod: HTTP_METHOD.POST) {[weak self] (isSucceeded, response) in
      if isSucceeded == true{
        if let weakself = self {
          DispatchQueue.main.async {
            print(response)
            if let data = response["data"] as? [String:Any] {
              if let sessionId = data["session_id"] as? String {
                self?.resetController()
//                var orderDetailVC : OrderDetailVC!
//                let stroryBoard = UIStoryboard(name: STORYBOARD_NAME.afterLogin, bundle: Bundle.main)
//
//
//                orderDetailVC = stroryBoard.instantiateViewController(withIdentifier: STORYBOARD_ID.orderDetailVC) as! OrderDetailVC
//                orderDetailVC.sessionId = sessionId
//                orderDetailVC.jobId = jobId
//                orderDetailVC.forTracking = true
//                weakself.navigationController?.pushViewController(orderDetailVC, animated: true)
              }
            }
          }
        }
      }else{
        self?.showErrorMessage(error: response["message"] as! String, isError: true)
      }
    }
  }
  
  
  
  func resetController(){
    inRequest = false
    totalWatingTime = 0
    selected = 0
    collectionView.reloadData()
    self.pickUpMetaData = []
    self.jobDropOffLabel.text = TEXT.DropLocation
    self.removeDestinatioButton.isHidden = true
    Singleton.sharedInstance.selectedDestinationAddressForTaxi = TEXT.DropLocation
    Singleton.sharedInstance.selectedDestinationLatForTaxi = Double()
    Singleton.sharedInstance.selectedDestinationlongForTaxi = Double()
    self.locationManager.startUpdatingLocation()
    ActivityIndicator.sharedInstance.pleaseWait = false
    ActivityIndicator.sharedInstance.hideActivityIndicator()
    self.makeMetaData()
    UIView.animate(withDuration: 0.5, animations: {
      self.bottomView.alpha = 1
      self.locationVIew.alpha = 1
    }, completion:nil)
  }
  
  
  func checkIfScheduleIsEnabled(){
    
    self.scheduleLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.carTimeLabelSize)
    self.scheduleLabel.textColor = UIColor.white
    self.scheduleButtoBackgroundView.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
    self.scheduleButton.backgroundColor = UIColor.black.withAlphaComponent(0.1)
    
    if Singleton.sharedInstance.formDetailsInfo.isSchedulingEnabled == "1"{
      
      self.buttonWidth.constant = SCREEN_SIZE.width*0.275
      self.scheduleLabel.text = TEXT.ScheduleText
      self.scheduleImahe.isHidden = false
      self.scheduleButtoBackgroundView.isHidden = false
      
    }else{
      
      self.buttonWidth.constant = 0
      self.scheduleLabel.text = ""
      self.scheduleImahe.isHidden = true
      self.scheduleButtoBackgroundView.isHidden = true
      
    }
  }
  
  func forLaterBookings(dateInString:String,dateInDateFormat:Date){
    
    print(dateInString)
    print(dateInDateFormat)
    removeTimer()
    
    
    if jobPickUpLabel.text != TEXT.GettingAddress && jobPickUpLabel.text != TEXT.PickUpLocation{
      if  Singleton.sharedInstance.selectedDestinationlongForTaxi == Double(){
        if Singleton.sharedInstance.formDetailsInfo.isDestinationRequired != "1"{
          let vc = UIStoryboard(name: STORYBOARD_NAME.afterLogin, bundle: Bundle.main).instantiateViewController(withIdentifier: STORYBOARD_ID.payment) as? NewPaymentScreenViewController
          
          vc?.isDeliveryAdded = false
          vc?.amount = ""
          vc?.selected = self.selected
          vc?.selectedLat = self.selectedLat
          vc?.selectedLong = self.selectedLong
          vc?.delegate = self
          vc?.isNow = false
          vc?.dateInString = dateInString
          vc?.dateInDateFormat = dateInDateFormat
          vc?.ammountLabelText = TEXT.estimatedCost
          self.navigationController?.pushViewController(vc!, animated: true)
          
        }else{
          self.showErrorMessage(error: TEXT.pleaseEnterTheDropLocation, isError: true)
        }
      }else{
        makeMetaData()
        let param = [
          "access_token":"\(Vendor.current!.appAccessToken!)",
          "device_token":"\(APIManager.sharedInstance.getDeviceToken())",
          "app_version":"\(APP_VERSION)",
          "app_device_type":APP_DEVICE_TYPE,
          "app_access_token":"\(Vendor.current!.appAccessToken!)",
          "pickup_latitude":"\(selectedLat)",
          "pickup_longitude":"\(selectedLong)",
          "user_id":"\(Singleton.sharedInstance.formDetailsInfo.user_id!)",
          "pickup_meta_data":self.pickUpMetaData,
          "vendor_id":"\(Vendor.current!.id!)",
          "payment_method":"\(Singleton.sharedInstance.formDetailsInfo.paymentMethodArray[0])",
          "layout_type":"\(Singleton.sharedInstance.formDetailsInfo.work_flow!)",
          "has_pickup":"1",
          "has_delivery":"0",
          "form_id":Singleton.sharedInstance.formDetailsInfo.form_id!
          ] as [String : Any]
        
        print(param)
        
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.serverCall(apiName: "fare_estimate", params: param as [String : AnyObject]?, httpMethod: "POST", receivedResponse: { (isSuccess, response) in
          ActivityIndicator.sharedInstance.hideActivityIndicator()
          
          if isSuccess == true {
            if let data = response["data"] as? [String: Any] {
              if let value = data["currency"] as? [String: Any] {
                guard let currencyValue = Currency(json: value) else {
                  return
                }
                if let totalCost = data["fare_lower_limit"] as? NSNumber{
                  if let upperCost = data["fare_upper_limit"] as? NSNumber{
                    let vc = UIStoryboard(name: STORYBOARD_NAME.afterLogin, bundle: Bundle.main).instantiateViewController(withIdentifier: STORYBOARD_ID.payment) as? NewPaymentScreenViewController
                    vc?.amount = "\(currencyValue.symbol) \(totalCost.intValue) - \(currencyValue.symbol) \(upperCost.intValue)"
                    vc?.delegate = self
                    vc?.isNow = false
                    vc?.dateInString = dateInString
                    vc?.selectedLat = self.selectedLat
                    vc?.selectedLong = self.selectedLong
                    
                    vc?.dateInDateFormat = dateInDateFormat
                    vc?.selected = self.selected
                    vc?.selectedDropLat =  Singleton.sharedInstance.selectedDestinationLatForTaxi
                    vc?.selectedDropLong =  Singleton.sharedInstance.selectedDestinationlongForTaxi
                    vc?.selectedDestinationAddress = self.jobDropOffLabel.text!
                    vc?.ammountLabelText = TEXT.estimatedCost
                    
                    
                    self.navigationController?.pushViewController(vc!, animated: true)
                  }
                }
              }
            }
          }else{
            if let value = response["message"] as? String{
              self.showErrorMessage(error: value, isError: true)
            }
          }
          
          
          print(response)
        })
      }
    }else{
      ErrorView.showWith(message: "Please wait while we are fetching the location.", isErrorMessage: true, removed: nil)
    }
    //        }else{
    //            showErrorMessage(error: TEXT.NoCarsFoundMessage, isError: true)
    //        }
  }
  
  func giveTextColor(){
    jobDropOffLabel.textColor = UIColor.black
    jobPickUpLabel.textColor = UIColor.black
    locationVIew.backgroundColor = COLOR.popUpColor
  //  self.bottomView.backgroundColor = COLOR.popUpColor
    self.collectionView.backgroundColor = COLOR.popUpColor
    self.makeBookingButton.setTitleColor(COLOR.LOGIN_BUTTON_TITLE_COLOR, for: UIControlState.normal)
    scheduleLabel.textColor = COLOR.LOGIN_BUTTON_TITLE_COLOR
    scheduleImahe.image = #imageLiteral(resourceName: "carSchedule").withRenderingMode(.alwaysTemplate)
    scheduleImahe.tintColor = COLOR.LOGIN_BUTTON_TITLE_COLOR
    //self.inactiveImage.withRenderingMode(.alwaysTemplate)
  }
  
  
  func setGoogleMap() {
    
    var mapType  = String()
    
    switch defaultMapType {
    case .white:
      mapType = "Whitestyle"
    case .black:
      mapType = "style"
    default:
      mapType = ""
    }
    if mapType == ""{
      return
    }
    
    do {
      
      // Set the map style by passing the URL of the local file.
      
      //            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
      
      if let styleURL = Bundle.main.url(forResource: mapType, withExtension: "json") {
        
        self.mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
        
      } else {
        
        NSLog("Unable to find style.json")
        
      }
      
    } catch {
      
      NSLog("The style definition could not be loaded: \(error)")
      
    }
  }
  
  @IBAction func backButtonAction(_ sender: UIButton) {
    
    self.navigationController?.popViewController(animated:true)
    
  }
  
  func hideOrShowCurrentLocationButton(hide:Bool){
    switch hide {
    case true:
      UIView.animate(withDuration: 0.5, animations: {
        self.currentLocationButton.alpha = 1
      }, completion: { (true) in
        self.currentLocationButton.isHidden = false
        
      })
    default:
      self.currentLocationButton.isHidden = false
      UIView.animate(withDuration: 0.5, animations: {
        self.currentLocationButton.alpha = 1
      })
    }
  }
  
  @IBAction func removeDestinationButton(_ sender: UIButton) {
    
    Singleton.sharedInstance.selectedDestinationAddressForTaxi = TEXT.DropLocation
    self.jobDropOffLabel.text = TEXT.DropLocation
    Singleton.sharedInstance.selectedDestinationLatForTaxi = Double()
    Singleton.sharedInstance.selectedDestinationlongForTaxi = Double()
    sender.isHidden = true
    
  }
    
    
    func updatemap(coordinate:CLLocationCoordinate2D,address:String){
        
        if isPickUp == true {
            self.jobPickUpLabel.text = address
            self.mapView.camera = GMSCameraPosition(target: coordinate, zoom: mapView.camera.zoom, bearing: 0, viewingAngle: 0)
            updateSelectedLatAndLong(coordinate: coordinate)
            self.adjustZoom = true
        }else{
            removeDestinatioButton.isHidden = false
            self.jobDropOffLabel.text = address
            Singleton.sharedInstance.selectedDestinationAddressForTaxi = address
            updateSelectedLatAndLongForDrop(coordinate: coordinate)
        }
        
        if self.changeBounds == true{
            apiHitToGetNearestDriverETA()
        }else{
            self.changeBounds = true
        }
        
        
        
    }
    
  
}


// MARK: - GMSAutocompleteViewControllerDelegate
extension TaxiHomeScreenViewController: GMSAutocompleteViewControllerDelegate {
  // Handle the user's selection.
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    print("Place name: \(place.name)")
    print("Place address: \(place.formattedAddress)")
    print("Place attributions: \(place.coordinate.latitude)")
    print("Place attributions: \(place.coordinate.longitude)")
    if isPickUp == true {
      self.jobPickUpLabel.text = place.formattedAddress
      self.mapView.camera = GMSCameraPosition(target: place.coordinate, zoom: mapView.camera.zoom, bearing: 0, viewingAngle: 0)
      updateSelectedLatAndLong(coordinate: place.coordinate)
      self.adjustZoom = true
    }else{
      removeDestinatioButton.isHidden = false
      self.jobDropOffLabel.text = place.formattedAddress
      Singleton.sharedInstance.selectedDestinationAddressForTaxi = place.formattedAddress!
      updateSelectedLatAndLongForDrop(coordinate: place.coordinate)
    }
    
    if self.changeBounds == true{
      apiHitToGetNearestDriverETA()
    }else{
      self.changeBounds = true
    }
    self.dismiss(animated: true, completion: nil)
  }
  
  func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // TODO: handle the error.
    print("Error: ", error.localizedDescription)
  }
  
  // User canceled the operation.
  func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    self.dismiss(animated: true, completion: nil)
  }
  
  // Turn the network activity indicator on and off again.
  func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
  }
  
  func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
  }
  
  
  func addGradientLayer(){
    let gradientLayerView: UIView = UIView(frame: CGRect(x: 0, y: -2, width: view.bounds.width, height: 80))
    let gradient: CAGradientLayer = CAGradientLayer()
    gradient.frame = gradientLayerView.bounds
    gradient.colors = [UIColor.white.withAlphaComponent(1).cgColor,UIColor.white.withAlphaComponent(0.7).cgColor,UIColor.white.withAlphaComponent(0.6).cgColor,UIColor.white.withAlphaComponent(0.3).cgColor,UIColor.white.withAlphaComponent(0).cgColor]
    gradient.shouldRasterize = true
    gradient.rasterizationScale = UIScreen.main.scale
    gradient.locations = [0.0,0.33,0.66,0.77,1]
    
    gradientLayerView.layer.insertSublayer(gradient, at: 0)
    
    self.view.layer.insertSublayer(gradientLayerView.layer, at: 0)
    self.view.addSubview(gradientLayerView)
    self.view.bringSubview(toFront: gradientLayerView)
    self.view.bringSubview(toFront: backButton)
  }
  
    
    func setGradientLayer() {
        
        
        self.fromColor = [UIColor(red: 70/255, green: 148/255, blue: 246/255, alpha: 1.0).cgColor,
                          UIColor(red: 88/255, green: 160/255, blue: 245/255, alpha: 1.0).cgColor,
                          UIColor(red: 107/255, green: 170/255, blue: 249/255, alpha: 1.0).cgColor,
                          UIColor(red: 125/255, green: 180/255, blue: 247/255, alpha: 1.0).cgColor,
                          UIColor(red: 145/255, green: 191/255, blue: 250/255, alpha: 1.0).cgColor,
                          UIColor(red: 162/255, green: 202/255, blue: 251/255, alpha: 1.0).cgColor,
                          UIColor(red: 181/255, green: 213/255, blue: 251/255, alpha: 1.0).cgColor,
                          UIColor(red: 199/255, green: 222/255, blue: 253/255, alpha: 1.0).cgColor,
        ]

        var shiftedColors = self.fromColor
        
        shiftedColors.removeLast()
        
        shiftedColors.insert(self.fromColor.last!, at: 0)
        
        self.toColors = shiftedColors
        
        
        
        if self.gradientLayer != nil {
            
            self.gradientLayer.removeAllAnimations()
            
            self.gradientLayer.removeFromSuperlayer()
            
            self.gradientLayer = nil
            
        }
        
        gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = CGRect(x: 0, y: SCREEN_SIZE.height - 62, width: SCREEN_SIZE.width , height: 2)
        
        
        gradientLayer.colors = self.fromColor
        
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5) //CGPointMake(0.0, 0.5)
        
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5) //CGPointMake(1.0, 0.5)
        if self.stopGradientAnimation == false   {
        self.view.layer.addSublayer(gradientLayer)
        self.animateLayer()
        }
        
    }
  
  
    func animateLayer(){
        
        self.gradientLayer.colors = self.toColors
        
        let animation : CABasicAnimation = CABasicAnimation(keyPath: "colors")
        
        animation.fromValue = self.fromColor
        
        animation.toValue = toColors
        
        animation.duration = 0.08
        
        animation.isRemovedOnCompletion = true
        
        animation.fillMode = kCAFillModeForwards
        
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        
        animation.delegate = self
        
        self.gradientLayer.add(animation, forKey:"animateGradient")
        
    }
    
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        
        if self.stopGradientAnimation == false {
            
            var shiftedColors = self.fromColor
            
            shiftedColors.removeLast()
            
            shiftedColors.insert(self.fromColor.last!, at: 0)
            
            self.toColors = shiftedColors
            
            self.fromColor = self.gradientLayer.colors as! [CGColor]
            
            animateLayer()
            
        } else {
            
            guard self.gradientLayer != nil else {
                
                return
                
            }
            
            self.gradientLayer.removeAllAnimations()
            
            self.gradientLayer.removeFromSuperlayer()
            
            self.gradientLayer = nil
            
            self.fromColor.removeAll(keepingCapacity: false)
            
            self.toColors.removeAll(keepingCapacity: false)
            
        }
        
    }
    
    
    
}



extension GMSMapView {
  func getCenterCoordinate() -> CLLocationCoordinate2D {
    let centerPoint = self.center
    let centerCoordinate = self.projection.coordinate(for: centerPoint)
    return centerCoordinate
  }
  
  func getTopCenterCoordinate() -> CLLocationCoordinate2D {
    // to get coordinate from CGPoint of your map
    let topCenterCoor = self.convert(CGPoint(x: self.frame.size.width, y: 60), from: self)
    let point = self.projection.coordinate(for: topCenterCoor)
    return point
  }
  
  func getRadius() -> CLLocationDistance {
    let centerCoordinate = getCenterCoordinate()
    let centerLocation = CLLocation(latitude: centerCoordinate.latitude, longitude: centerCoordinate.longitude)
    let topCenterCoordinate = self.getTopCenterCoordinate()
    let topCenterLocation = CLLocation(latitude: topCenterCoordinate.latitude, longitude: topCenterCoordinate.longitude)
    let radius = CLLocationDistance(centerLocation.distance(from: topCenterLocation))
    return round(radius)
  }
}

extension GMSCircle {
  func bounds () -> GMSCoordinateBounds {
    func locationMinMax(positive : Bool) -> CLLocationCoordinate2D {
      let sign:Double = positive ? 1 : -1
      let dx = sign * self.radius  / 6378000 * (180/M_PI)
      let lat = position.latitude + dx
      let lon = position.longitude + dx / cos(position.latitude * M_PI/180)
      return CLLocationCoordinate2D(latitude: lat, longitude: lon)
    }
    
    return GMSCoordinateBounds(coordinate: locationMinMax(positive: true),
                               coordinate: locationMinMax(positive: false))
  }
}



