//
//  DeliveryAddressCell.swift
//  TookanVendor
//
//  Created by Vishal on 16/02/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

protocol DeliveryAddressDelegate {
    func editAddress()
}

class DeliveryAddressCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var phoneTextfield: VSTextField!
    @IBOutlet weak var additionalAddress: UILabel!
    
    @IBOutlet weak var zipcode: UILabel!
    @IBOutlet weak var landmark: UILabel!
    @IBOutlet weak var streetNo: UILabel!
    @IBOutlet weak var doorNo: UILabel!
    @IBOutlet weak var floorNo: UILabel!
    var delegate : DeliveryAddressDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        name.text = "UserName"
        email.text = "Email"
        //phone.text = "Phone"
        phoneTextfield.text = "Phone"
    }
    
    @IBAction func editAddress(_ sender: UIButton) {
        delegate?.editAddress()
    }
    func setupDataForBookingDetails(orderModel: BQOrderListModel) {
//        self.additionalAddress.isHidden = true
        if let userName = orderModel.customer_name {
            name.text = userName
        } else {
            name.text = "N/A"
        }
        if let userEmail =  orderModel.customer_email {
            email.text = userEmail
        } else {
            email.text = "N/A"
        }
        if let userPhone =  orderModel.customer_phone {
            //phone.text = userPhone
            //phoneTextfield.setFormatting("XXX-XXX-XXXX", replacementChar: "X")
            
            phoneTextfield.text = userPhone.applyPatternOnNumbers(pattern: "##-###-####", replacmentCharacter: "#")
        } else {
            phone.text = "N/A"
            phoneTextfield.text = "N/A"
        }
        var completeAddress = ""
        if let address1 = orderModel.landmark {
            completeAddress = address1
        }
        
        if let address2 = orderModel.job_address {
            if completeAddress.length != 0 {
                completeAddress = completeAddress + ", " + address2
            } else {
                completeAddress = address2
            }
        }
        
        if let address3 = orderModel.postal_code {
            if completeAddress.length != 0 {
                completeAddress = completeAddress + ", " + address3
            } else {
                completeAddress = address3
            }
        }
        
        if let area = BQBookingModel.shared.addressLine2 {
            address.text = "Area : " + area //completeAddress
        } else {
            address.isHidden = true
        }
        
        if let apartmentNo = BQBookingModel.shared.apartmentNo {
            additionalAddress.text = "Apartment No. : " + apartmentNo
        } else {
            self.additionalAddress.isHidden = true
        }
        
        if let floor = BQBookingModel.shared.floorNo {
            floorNo.text = "Floor No. " + floor //completeAddress
        } else {
            floorNo.isHidden = true
        }
        
        if let door = BQBookingModel.shared.doorNo {
            doorNo.text = "Door No. " + door
        } else {
            self.doorNo.isHidden = true
        }
        
        if let street = BQBookingModel.shared.streetNo {
            streetNo.text = "Street No. " + street //completeAddress
        } else {
            streetNo.isHidden = true
        }
        
        if let landmarkText = BQBookingModel.shared.addressLine1 {
            landmark.text = "Landmark : " + landmarkText
        } else {
            self.landmark.isHidden = true
        }
        
        if let zipcodeText = BQBookingModel.shared.addressLine3 {
            zipcode.text = "Zip Code : " + zipcodeText
        } else {
            self.zipcode.isHidden = true
        }
        
//        address.text = completeAddress
        
    }
    
    func setUserData() {
        self.additionalAddress.isHidden = false
        if let userName = BQBookingModel.shared.userName {
            name.text = userName
        }
        if let userEmail =  BQBookingModel.shared.email {
            email.text = userEmail
        }
        if let userPhone =  BQBookingModel.shared.phone {
            //phone.text = userPhone
//            phoneTextfield.setFormatting("XXX-XXX-XXXX", replacementChar: "X")
//            phoneTextfield.text = userPhone
            phoneTextfield.text = userPhone.applyPatternOnNumbers(pattern: "##-###-####", replacmentCharacter: "#")
        }
        var completeAddress = ""
        
        if let address1 = BQBookingModel.shared.addressLine1 {
            completeAddress = address1
        }
        
        if let address2 = BQBookingModel.shared.addressLine2 {
            if completeAddress.length != 0 {
                completeAddress = completeAddress + ", " + address2
            } else {
                completeAddress = address2
            }
        }
        
        
        if let address3 = BQBookingModel.shared.addressLine3 {
            if completeAddress.length != 0 {
                completeAddress = completeAddress + ", " + address3
            } else {
                completeAddress = address3
            }
        }
        if let area = BQBookingModel.shared.addressLine2 {
            address.text = "Area " + area //completeAddress
        } else {
            address.isHidden = true
        }
        
        if let apartmentNo = BQBookingModel.shared.apartmentNo {
            additionalAddress.text = "Apartment No. : " + apartmentNo
        } else {
            self.additionalAddress.isHidden = true
        }
        
        if let floor = BQBookingModel.shared.floorNo {
            floorNo.text = "Floor No. " + floor //completeAddress
        } else {
            floorNo.isHidden = true
        }
        
        if let door = BQBookingModel.shared.doorNo {
            doorNo.text = "Door No. : " + door
        } else {
            self.doorNo.isHidden = true
        }
        
        if let street = BQBookingModel.shared.streetNo {
            streetNo.text = "Street No. : " + street //completeAddress
        } else {
            streetNo.isHidden = true
        }
        
        if let landmarkText = BQBookingModel.shared.addressLine1 {
            landmark.text = "Landmark : " + landmarkText
        } else {
            self.landmark.isHidden = true
        }
        
        if let zipcodeText = BQBookingModel.shared.addressLine3 {
            zipcode.text = "Zip Code : " + zipcodeText
        } else {
            self.zipcode.isHidden = true
        }
        
            //BQBookingModel.shared.addressLine4 ?? ""
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
