//
//  BQSingleSelectionCell.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 06/06/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQSingleSelectionCell: UITableViewCell {

    @IBOutlet weak var lblOption: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var btnRadioCheck: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        //self.bgView.layer.cornerRadius = 5.0
        //self.bgView.layer.borderColor = UIColor.lightGray.cgColor
        //self.bgView.layer.borderWidth = 1.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    class func cellIdentifier() -> String {
        return String(describing: BQSingleSelectionCell.self)
    }
    
}
