//
//  CreateTaskDetails.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 30/11/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class CreateTaskDetails: NSObject {
    
    var autoAssignment:Int = 1
    var customerAddress:String = ""
    var customerEmail:String = ""
    var customerPhone:String = ""
    var customerUsername:String = ""
    var jobDeliveryDatetime:String = ""
    var latitude:String = ""
    var longitude:String = ""
    var hasDelivery:Int = 0
    var hasPickup:Int = 1
    var jobDescription:String = ""
    var jobPickupAddress:String = ""
    var jobPickupDateTime:String = ""
    var jobPickupLatitude:String = ""
    var jobPickupLongitude:String = ""
    var jobPickupName:String = ""
    var jobPickupEmail:String = ""
    var jobPickupPhone:String = ""
    var layoutType:Int = 0
    var refImages:[String] = []
    var domainName:String = ""
    var vendorId:Int = 2
    var hidePickupDelivery:Bool = false
    var pickupMetadata = [Any]()
    var metaData = [Any]()
    var fleetId = ""
    var pendingTaskFromDemo = false
    var responseOfGetPayment:[String: Any]? = [:]
    var metaDataArray : [[String:Any]]?
    var pickUpMetaDataArray : [[String:Any]]?
    var paramForDemoTask : [String:Any]?
    var createTaskParamsToSend : [String:Any]?
    
    init(json: [String:Any]) {
      self.autoAssignment = (json["autoAssignment"] as? Int) ?? 1
      self.customerAddress = (json["customerAddress"] as? String) ?? ""
      self.customerEmail = (json["customerEmail"] as? String) ?? ""
      self.customerPhone = (json["customerPhone"] as? String) ?? ""
      self.customerUsername = (json["customerUsername"] as? String) ?? ""
      self.jobDeliveryDatetime = (json["jobDeliveryDatetime"] as? String) ?? ""
      self.latitude = (json["latitude"] as? String) ?? ""
      self.longitude = (json["longitude"] as? String) ?? ""
      self.jobDescription = (json["jobDescription"] as? String) ?? ""
      self.jobPickupDateTime = (json["jobPickupDateTime"] as? String) ?? ""
      self.jobPickupLatitude = (json["jobPickupLatitude"] as? String) ?? ""
      self.jobPickupLongitude = (json["jobPickupLongitude"] as? String) ?? ""
      self.jobPickupName = (json["jobPickupName"] as? String) ?? ""
      self.jobPickupEmail = (json["jobPickupEmail"] as? String) ?? ""
      self.jobPickupPhone = (json["jobPickupPhone"] as? String) ?? ""
      self.domainName = (json["domainName"] as? String) ?? ""
      self.fleetId = (json["fleetId"] as? String) ?? ""
      
      self.hasDelivery = (json["hasDelivery"] as? Int) ?? 0
      self.hasPickup = (json["hasPickup"] as? Int) ?? 1
      self.layoutType = (json["layoutType"] as? Int) ?? 0
      self.vendorId = (json["vendorId"] as? Int) ?? 2
      
      self.hidePickupDelivery = (json["hidePickupDelivery"] as? Bool) ?? false
      self.pendingTaskFromDemo = (json["pendingTaskFromDemo"] as? Bool) ?? false
      
      self.pickupMetadata = (json["pickupMetadata"] as? [Any]) ?? [Any]()
      self.metaData = (json["metaData"] as? [Any]) ?? [Any]()
      
      self.refImages = (json["refImages"] as? [String]) ?? []
      
      self.responseOfGetPayment = (json["responseOfGetPayment"] as? [String: Any]) ?? [:]
      
      self.pickUpMetaDataArray = (json["pickUpMetaDataArray"] as? [[String: Any]]) ?? []
      self.metaDataArray = (json["metaDataArray"] as? [[String: Any]]) ?? []
    }
   
   func setParmForDemoTask() -> [String:Any]{
    if paramForDemoTask != nil {
      paramForDemoTask!["api_key"] = nil
      paramForDemoTask!["app_access_token"] = Vendor.current!.appAccessToken!
      paramForDemoTask!["vendor_id"] = Vendor.current!.id
      paramForDemoTask!["customer_email"] = Vendor.current!.email
      paramForDemoTask!["access_token"] = Vendor.current!.appAccessToken!
      paramForDemoTask!["app_device_type"] = APP_DEVICE_TYPE
      paramForDemoTask!["reference_id"] = Vendor.current!.referenceId
      paramForDemoTask!["vendor_id"] = Vendor.current!.id
      paramForDemoTask!["user_id"] = nil
      paramForDemoTask!["form_id"] = nil
    }
      return paramForDemoTask!
   }
}
