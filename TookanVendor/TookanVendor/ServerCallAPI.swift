//
//  ServerCallAPI.swift
//  Butlers
//
//  Created by Rakesh Kumar on 5/14/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import Foundation
import UIKit
import Fabric
import Crashlytics

func sendRequestToServer(baseUrl:String = (UserDefaults.standard.value(forKey: USER_DEFAULT.selectedServer) as! String),_ url: String, params: [String:AnyObject], httpMethod: String, isZipped:Bool, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()){

    
    
    let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    var request = URLRequest(url: URL(string: (baseUrl) + urlString!)!)
    request.httpMethod = httpMethod as String
    request.timeoutInterval = 20
    print(request)
    if(httpMethod == "POST")
    {
        request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: [])
        if isZipped == false {
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        } else {
            request.addValue("application/octet-stream", forHTTPHeaderField: "Content-Type")
            request.addValue("application/octet-stream", forHTTPHeaderField: "Content-Encoding: gzip")
        }
        request.addValue("application/json", forHTTPHeaderField: "Accept")
    }
  
    let task = URLSession.shared.dataTask(with: request) {data, response, error in
        if(response != nil && data != nil) {
            
            let newStr = String(data: data!, encoding: .utf8)

            print("----> \(newStr)")
            
            do {
                if let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any] {
                    
                    

                    
                    //     let success = json["success"] as? Int                                  // Okay, the `json` is here, let's get the value for 'success' out of it
                    // print("Success: \(success)")
                //    FIRAnalytics.logEvent(withName: ANALYTICS_KEY.API_SUCCESS, parameters: ["API":urlString! as NSObject])
                //    Answers.logCustomEvent(withName: ANALYTICS_KEY.API_SUCCESS, customAttributes: ["API":urlString])
                    receivedResponse(true, json)
                } else {
                //    FIRAnalytics.logEvent(withName: ANALYTICS_KEY.API_FAILURE, parameters: ["API":urlString! as NSObject])
                //    Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":urlString])

                    let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)    // No error thrown, but not NSDictionary
                    print("Error could not parse JSON: \(jsonStr)")
                    receivedResponse(false, [:])
                }
            } catch let parseError {
                print(parseError)                                                          // Log the error thrown by `JSONObjectWithData`
                let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("Error could not parse JSON: '\(jsonStr)'")
            //    FIRAnalytics.logEvent(withName: ANALYTICS_KEY.API_FAILURE, parameters: ["API":urlString! as NSObject])
            //    Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":urlString])
                receivedResponse(false, [:])
            }
        } else {
           // FIRAnalytics.logEvent(withName: ANALYTICS_KEY.API_FAILURE, parameters: ["API":urlString! as NSObject])
          //  Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":urlString])
            receivedResponse(false, [:])
        }
    }
    task.resume()
}

func uploadingMultipleTask(_ url:String, params: [String:AnyObject], isImage:Bool, imageData:[UIImage?], imageKey:String, receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ())
{
    var session = URLSession.shared
    let boundary:NSString = "----WebKitFormBoundarycC4YiaUFwM44F6rT"
    let body:NSMutableData = NSMutableData()
    
    let paramsArray = params.keys
    for item in paramsArray {
        body.append(("--\(boundary)\r\n" as String).data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        body.append("Content-Disposition: form-data; name=\"\(item)\"\r\n\r\n" .data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        body.append("\(params[item]!)\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
    }
    
    if(isImage) {
        for i in (0..<imageData.count) {
            body.append(("--\(boundary)\r\n" as String).data(using: String.Encoding.utf8, allowLossyConversion: true)!)
            body.append("Content-Disposition: form-data; name=\"\(imageKey)\"; filename=\"photoName.jpeg\"\r\n" .data(using: String.Encoding.utf8, allowLossyConversion: true)!)
            body.append("Content-Type: image/jpeg\r\n\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
            body.append(UIImageJPEGRepresentation(imageData[i]!, 0.02)!)
            body.append("\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
        }
    }
    
    body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
    var request = URLRequest(url: URL(string: (UserDefaults.standard.value(forKey: USER_DEFAULT.selectedServer) as! String) + (url as String))!)
    request.httpMethod = "POST"
    request.httpBody = body as Data
    request.timeoutInterval = 35
    request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
    
    let config:URLSessionConfiguration = URLSessionConfiguration.default
    session = URLSession(configuration: config)
    
    let task = session.dataTask(with: request, completionHandler: {data, response, error -> Void in
        //  print("Response: \(response)")
        //  let strData = NSString(data: data!, encoding: NSUTF8StringEncoding)
        //  print("Body: \(strData)")
        if(response != nil) {
            do {
                if let json = try JSONSerialization.jsonObject(with: data!, options: .mutableLeaves) as? [String:Any] {
                    let success = json["success"] as? Int                                  // Okay, the `json` is here, let's get the value for 'success' out of it
                    print("Success: \(success)")
                 //   FIRAnalytics.logEvent(withName: ANALYTICS_KEY.API_SUCCESS, parameters: ["API":url as NSObject])
                 //   Answers.logCustomEvent(withName: ANALYTICS_KEY.API_SUCCESS, customAttributes: ["API":url])
                    receivedResponse(true, json)
                } else {
                    let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)    // No error thrown, but not NSDictionary
                    print("Error could not parse JSON: \(jsonStr)")
                 //   FIRAnalytics.logEvent(withName: ANALYTICS_KEY.API_FAILURE, parameters: ["API":url as NSObject])
                 //   Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":url])
                    receivedResponse(false, [:])
                }
            } catch let parseError {
                print(parseError)                                                          // Log the error thrown by `JSONObjectWithData`
                let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                print("Error could not parse JSON: '\(jsonStr)'")
             //   FIRAnalytics.logEvent(withName: ANALYTICS_KEY.API_FAILURE, parameters: ["API":url as NSObject])
              //  Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":url])
                receivedResponse(false, [:])
            }
        } else {
          //  FIRAnalytics.logEvent(withName: ANALYTICS_KEY.API_FAILURE, parameters: ["API":url as NSObject])
         //   Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":url])
            receivedResponse(false, [:])
        }
    })
    task.resume()
}

//func sendSynchronousRequestToUploadImage(_ url:String, params:[String:AnyObject], imageData:Data!,imageKey:String) -> [String:Any]! {
//    let boundary:NSString = "----WebKitFormBoundarycC4YiaUFwM44F6rT"
//    let body:NSMutableData = NSMutableData()
//    
//    let paramsArray = params.keys
//    for item in paramsArray {
//        body.append(("--\(boundary)\r\n" as String).data(using: String.Encoding.utf8, allowLossyConversion: true)!)
//        body.append("Content-Disposition: form-data; name=\"\(item)\"\r\n\r\n" .data(using: String.Encoding.utf8, allowLossyConversion: true)!)
//        body.append("\(params[item]!)\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
//    }
//    
//    if(imageData != nil) {
//        body.append(("--\(boundary)\r\n" as String).data(using: String.Encoding.utf8, allowLossyConversion: true)!)
//        body.append("Content-Disposition: form-data; name=\"\(imageKey)\"; filename=\"photoName.jpeg\"\r\n" .data(using: String.Encoding.utf8, allowLossyConversion: true)!)
//        body.append("Content-Type: image/jpeg\r\n\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
//        body.append(imageData!)
//        body.append("\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
//    }
//    
//    body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8, allowLossyConversion: true)!)
//    let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//    var request = URLRequest(url: URL(string: (UserDefaults.standard.value(forKey: USER_DEFAULT.selectedServer) as! String) + urlString!)!)
//    request.httpMethod = "POST"
//    request.httpBody = body as Data
//    request.timeoutInterval = 20
//    request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
//    UIApplication.shared.isNetworkActivityIndicatorVisible = true
//    let response:[String:Any]!
//    if let data = URLSession.requestSynchronousData(request as URLRequest){//NSURLSession.requestSynchronousJSON(request) {
//        do {
//            if let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any] {
//                UIApplication.shared.isNetworkActivityIndicatorVisible = false
//                print(json)
//                FIRAnalytics.logEvent(withName: ANALYTICS_KEY.API_SUCCESS, parameters: ["API":url as NSObject])
//                Answers.logCustomEvent(withName: ANALYTICS_KEY.API_SUCCESS, customAttributes: ["API":url])
//                response = json
//            } else {
//                UIApplication.shared.isNetworkActivityIndicatorVisible = false
//                let jsonStr = NSString(data: data, encoding: String.Encoding.utf8.rawValue)    // No error thrown, but not NSDictionary
//                print("Error could not parse JSON: \(jsonStr)")
//                FIRAnalytics.logEvent(withName: ANALYTICS_KEY.API_FAILURE, parameters: ["API":url as NSObject])
//                Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":url])
//                return nil
//            }
//        } catch let parseError {
//            UIApplication.shared.isNetworkActivityIndicatorVisible = false
//            print(parseError)                                                          // Log the error thrown by `JSONObjectWithData`
//            let jsonStr = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
//            print("Error could not parse JSON: '\(jsonStr)'")
//            FIRAnalytics.logEvent(withName: ANALYTICS_KEY.API_FAILURE, parameters: ["API":url as NSObject])
//            Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":url])
//            return nil
//        }
//    } else {
//        UIApplication.shared.isNetworkActivityIndicatorVisible = false
//        FIRAnalytics.logEvent(withName: ANALYTICS_KEY.API_FAILURE, parameters: ["API":url as NSObject])
//        Answers.logCustomEvent(withName: ANALYTICS_KEY.API_FAILURE, customAttributes: ["API":url])
//        return nil
//    }
//    return response
//}


func downloadImageAsynchronously(_ imageURL:String, imageView:UIImageView, placeHolderImage:UIImage, contentMode:UIViewContentMode) -> UIImage {
    imageView.contentMode = contentMode
    imageView.clipsToBounds = true
    if let cacheImage = Singleton.sharedInstance.allImagesCache.object(forKey: "\(imageURL)" as NSString) {             //image caching
        return cacheImage
    } else {
        // The image isn't cached, download the img data
        // We should perform this in a background thread
        let url = URL(string: imageURL as String)
        let request: Foundation.URLRequest = Foundation.URLRequest(url: url!)
        let mainQueue = OperationQueue.main
        
        NSURLConnection.sendAsynchronousRequest(request, queue: mainQueue, completionHandler: { (response, data, error) -> Void in
            if error == nil {
                // Convert the downloaded data in to a UIImage object
                let image = UIImage(data: data!)
                // Store the image in to our cache
                if(image != nil) {
                    Singleton.sharedInstance.allImagesCache.setObject(image!, forKey: "\(request.url!.absoluteString)" as NSString)
                    // Update the cell
                    DispatchQueue.main.async(execute: {
                        imageView.contentMode = contentMode
                        imageView.image = image
                    })
                } else {
                    imageView.contentMode = .center
                    imageView.image = placeHolderImage
                }
            }
            else {
                print("Error")
            }
        })
    }
    imageView.contentMode = .center
    return placeHolderImage
}

func getImageFromDocumentDirectory(_ imagePath:String, imageView:UIImageView, placeHolderImage:UIImage, contentMode:UIViewContentMode) {
    imageView.image = placeHolderImage
    imageView.contentMode = contentMode
    imageView.clipsToBounds = true
    if let cacheImage = Singleton.sharedInstance.allImagesCache.object(forKey: "\(imagePath)" as NSString) {             //image caching
        DispatchQueue.main.async(execute: {
            imageView.image = cacheImage
        })
    } else {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            if(Singleton.sharedInstance.isFileExistAtPath(imagePath) == true) {
                let imageFromDocumentDirectory = UIImage(contentsOfFile: Singleton.sharedInstance.createPath(imagePath))
                //dispatch_async(dispatch_get_main_queue(), {
                Singleton.sharedInstance.allImagesCache.setObject(imageFromDocumentDirectory!, forKey: "\(imagePath)" as NSString)
                imageView.image = imageFromDocumentDirectory
                //})
            }
        }
    }
    
    
    
    
}


extension URLSession {
    
    /// Return data from synchronous URL request
    public static func requestSynchronousData(_ request: URLRequest) -> Data? {
        var data: Data? = nil
        let semaphore: DispatchSemaphore = DispatchSemaphore(value: 0)
        let task = URLSession.shared.dataTask(with: request, completionHandler: {
            taskData, _, error -> () in
            data = taskData
            if data == nil, let error = error {print(error)}
            semaphore.signal();
        })
        task.resume()
        semaphore.wait(timeout: DispatchTime.distantFuture)
        return data
    }
    
    /// Return data synchronous from specified endpoint
    public static func requestSynchronousDataWithURLString(_ requestString: String) -> Data? {
        guard let url = URL(string:requestString) else {return nil}
        let request = URLRequest(url: url)
        return URLSession.requestSynchronousData(request)
    }
    
    /// Return JSON synchronous from URL request
    public static func requestSynchronousJSON(_ request: URLRequest) -> [String:Any]? {
        guard let data = URLSession.requestSynchronousData(request) else {return nil}
        print(data)
        return try! JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String:Any]
    }
    
    /// Return JSON synchronous from specified endpoint
    public static func requestSynchronousJSONWithURLString(_ requestString: String) -> AnyObject? {
        guard let url = URL(string: requestString) else {return nil}
        let request = NSMutableURLRequest(url:url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        return URLSession.requestSynchronousJSON(request as URLRequest) as AnyObject?
    }
}

