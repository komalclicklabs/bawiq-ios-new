//
//  BQStoreDetailsCell.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 15/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit
import MessageUI

protocol BQStoreDetailsCellDelegate: class {
    func feedbackToDriver()
    func callBtnAction()
    func messageBtnAction()
}


class BQStoreDetailsCell: UITableViewCell {
    @IBOutlet weak var bgView: UIView!

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var btnCall: UIButton!
    @IBOutlet weak var btnMessage: UIButton!
    @IBOutlet weak var btnFeedback: UIButton!
    
    @IBOutlet weak var bgRatingView: UIView!
    @IBOutlet weak var imgRated1: UIImageView!
    @IBOutlet weak var imgRated2: UIImageView!
    @IBOutlet weak var imgRated3: UIImageView!
    @IBOutlet weak var imgRated4: UIImageView!
    @IBOutlet weak var imgRated5: UIImageView!
    
    weak var delegate: BQStoreDetailsCellDelegate?
    
    
    @IBAction func callButtonAction(_ sender: Any) {
        delegate?.messageBtnAction()
    }
    
    @IBAction func messageButtonAction(_ sender: Any) {
        delegate?.callBtnAction()
    }
    @IBAction func feedbackButtonAction(_ sender: Any) {
        delegate?.feedbackToDriver()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
