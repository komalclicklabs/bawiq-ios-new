//
//  AllPaymentMethodsViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 28/02/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class AllPaymentMethodsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,ErrorDelegate {

    
    
    //IBOUTLETS
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noCardsFoundLabel: UILabel!
    @IBOutlet weak var backgroundView: UIView!
    
    var cardsArray = [cardsModel]()
    var errorMessageView:ErrorView!
    var keyboardSize:CGSize = CGSize(width: 0.0, height: 0.0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        assignUiAttributes()
        self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register( UINib(nibName: NIB_NAME.PayementMethodCell, bundle: Bundle.main)    , forCellReuseIdentifier: CELL_IDENTIFIER.payementMethod)

        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 120
        self.tableView.separatorStyle = .none
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }
        
        apiHitTOGetAllCards()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    

    @IBAction func backButtonAction(_ sender: UIButton) {
     let _ = self.navigationController?.popViewController(animated:true)
    }
    
    func assignUiAttributes(){
        addButton.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
        addButton.layer.cornerRadius = 51/2
        addButton.setShadow()
        backgroundView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        setTitleView()
        noCardsFoundLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)
        noCardsFoundLabel.text = TEXT.NO_CARDS_FOUND
    }
    
    
    func setTitleView() {
        let titleView = UINib(nibName: NIB_NAME.titleView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TitleView
        titleView.frame = CGRect(x: 0, y: HEIGHT.navigationHeight, width: self.view.frame.width, height: HEIGHT.titleHeight)
        titleView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        self.view.addSubview(titleView)
        titleView.setTitle(title: TEXT.YOUR_PAYMENT, boldTitle: TEXT.METHOD)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cardsArray.count == 0{
            noCardsFoundLabel.isHidden = false
        }else{
            noCardsFoundLabel.isHidden = true
        }
        return cardsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.payementMethod, for: indexPath) as! PaymentMethodTableViewCell
        cell.getData(showRightImage: false, rightImageLabel: "**** **** **** \(cardsArray[indexPath.row].lastFourDigit)", leftString: "", rightImage: #imageLiteral(resourceName: "sampleCard"), showImage: true   )
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func apiHitTOGetAllCards(){
        var paymentMethodToSend = Int()
        if Singleton.sharedInstance.formDetailsInfo.paymentMethodArray.contains(2) == true{
           paymentMethodToSend = 2
        }else if Singleton.sharedInstance.formDetailsInfo.paymentMethodArray.contains(4) == true {
            paymentMethodToSend = 4
        }
        
        
        
        let param = [
            "access_token" : Vendor.current!.appAccessToken!,
            "app_device_type" : APP_DEVICE_TYPE,
            "payment_method": "\(paymentMethodToSend)"
            ] as [String : String]
        print(param)
         ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.serverCall(apiName: API_NAME.fetchCards, params: param as [String : AnyObject]?, httpMethod: "POST") { (isSuccess, data) in
            print(data)
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if isSuccess == true{
            if let dataObject = data["data"] as? [Any]{
             print(dataObject)
                self.cardsArray.removeAll()
                if dataObject.count > 0 {
                for i in dataObject {
                    self.cardsArray.append(cardsModel(json: i as! [String : Any]))
                }
                }else{
                // self.addButtonAction(UIButton())
                }
                self.tableView.reloadData()
                }else{
                    //self.addButtonAction(UIButton())
                }
            }else{
                if let message = data["message"] as? String{
                    if message == "No data found."{
                        if let dataObject = data["data"] as? [Any]{
                            print(dataObject)
                            self.cardsArray.removeAll()
                            if dataObject.count > 0 {
                                for i in dataObject {
                                    self.cardsArray.append(cardsModel(json: i as! [String : Any]))
                                }
                            }else{
                                // self.addButtonAction(UIButton())
                            }
                            self.tableView.reloadData()
                        }
                    }else{
                         self.showErrorMessage(error: data["message"] as! String!)
                    }
                }
            }
        }
    }
    
     func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "viewCard") as? ViewCardViewcontroller
        vc?.cardData = cardsArray[indexPath.row]
        vc?.completionHandler = {
            self.apiHitTOGetAllCards()
        }
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
   
    
    @IBAction func addButtonAction(_ sender: UIButton) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.addCardWeb) as? AddCardWebViewViewController
        vc?.completionHandler = {
            self.apiHitTOGetAllCards()
        }
        self.present(vc!, animated: true, completion: nil)
        
    }

    func refresh(_ refreshControl: UIRefreshControl) {
        // Do your job, when done:
         apiHitTOGetAllCards()
        refreshControl.endRefreshing()
    }
    
    //MARK: ERROR MESSAGE
    func showErrorMessage(error:String) {
        if errorMessageView == nil {
            errorMessageView = UINib(nibName: NIB_NAME.errorView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
            errorMessageView.delegate = self
            errorMessageView.frame = CGRect(x: 0, y: self.view.frame.height - keyboardSize.height, width: self.view.frame.width, height: HEIGHT.errorMessageHeight)
            self.view.addSubview(errorMessageView)
            errorMessageView.setErrorMessage(message: error,isError: true)
        }
    }
    
    //MARK: ERROR DELEGATE METHOD
    func removeErrorView() {
        self.errorMessageView = nil
    }
    

}

