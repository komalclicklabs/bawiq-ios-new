//
//  BQOrderListModel.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 12/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQOrderListModel: NSObject {

    var category_description: String?
    var category_id: Int?
    var category_image: String?
    var category_name: String?
    var customer_name: String?
    var creation_datetime: String?
    var is_blocked: Bool?
    var job_delivery_datetime: String?
    var productListModel = [BQProductListModel]()
    var customer_phone: String?
    var customer_email: String?
    var job_latitude: Int?
    var job_longitude: Int?
    var job_status: String?
    var landmark: String?
    var job_address: String?
    var job_description: String?
    var postal_code: String?
    var product_count: Int?
    var total_price: Double?
    var payment_type: String?
    var statusColor: UIColor?
    
    var store_email: String?
    var store_image: String?
    var store_name: String?
    var store_phone: String?
    var store_id: Int?
    var is_customer_rated: Bool?
    var fleet_rating: Int?
    
    var promo_type: Int?
    var promo_value: Double?
    var promo_code: String?
    var bank_fee: Double?
    
    var payableAmount: Double?
    
    convenience init(param: [String: Any]) {
        self.init()
        
        if let categoryDescription = param["bank_fee"] as? Double {
            self.bank_fee = categoryDescription
        }
        
        if let categoryDescription = param["promo_code"] as? String {
            self.promo_code = categoryDescription
        }
        
        if let categoryDescription = param["promo_value"] as? Double {
            self.promo_value = categoryDescription
        }
        
        if let categoryDescription = param["promo_type"] as? Int {
            self.promo_type = categoryDescription
        }
        
        if let categoryDescription = param["fleet_rating"] as? Int {
            self.fleet_rating = categoryDescription
        }
        
        if let categoryDescription = param["customer_rating"] as? Int {
            self.fleet_rating = categoryDescription
        }
        
        if let categoryDescription = param["is_customer_rated"] as? Bool {
            self.is_customer_rated = categoryDescription
        }
        if let categoryDescription = param["store_id"] as? Int {
            self.store_id = categoryDescription
        }
        if let categoryDescription = param["store_phone"] as? String {
            self.store_phone = categoryDescription
        }
        if let categoryDescription = param["store_name"] as? String {
            self.store_name = categoryDescription
        }
        
        if let categoryDescription = param["store_email"] as? String {
            self.store_email = categoryDescription
        }
        
        if let categoryDescription = param["store_image"] as? String {
            self.store_image = categoryDescription
        }
        
        if let categoryDescription = param["job_address"] as? String {
            self.job_address = categoryDescription
        }
        
        if let categoryDescription = param["job_description"] as? String {
            self.job_description = categoryDescription
        }
        
        if let categoryDescription = param["payable_price"] as? Double {
            self.payableAmount = categoryDescription
        }
        if let categoryDescription = param["payment_type"] as? Int {
            if categoryDescription == 1 {
                self.payment_type = "CASH"
            } else if categoryDescription == 2 {
                self.payment_type = "CARD"
            } else if categoryDescription == 3 {
                self.payment_type = "WALLET"
            }
        }
        if let categoryDescription = param["customer_username"] as? String {
            self.customer_name = categoryDescription
        }
        
        if let categoryDescription = param["total_price"] as? Double {
            self.total_price = categoryDescription
        }
        
        if let categoryDescription = param["product_count"] as? Int {
            self.product_count = categoryDescription
        }
        if let categoryDescription = param["postal_code"] as? String {
            self.postal_code = categoryDescription
        }
        if let categoryDescription = param["landmark"] as? String {
            self.landmark = categoryDescription
        }
        if let categoryDescription = param["job_status"] as? Int {
            
            
            if categoryDescription == 6 {
                self.job_status = OrderStatus.Unassigned
                self.statusColor = COLOR.App_Status_Unassigned_Color
            }
            if categoryDescription == 0 {
                self.job_status = OrderStatus.Unassigned //OrderStatus.Accepted
                self.statusColor = COLOR.App_Status_Unassigned_Color //COLOR.App_Status_Accepted_Color
            }
            if categoryDescription == 7 {
                self.job_status = OrderStatus.Accepted
                self.statusColor = COLOR.App_Status_Accepted_Color
            }
            if categoryDescription == 1 {
                self.job_status = OrderStatus.Started
                self.statusColor = COLOR.App_Status_Started_Color
            }
            if categoryDescription == 5 {
                self.job_status = OrderStatus.OnTheWay
                self.statusColor = COLOR.App_Status_On_The_Way_Color
            }
            if categoryDescription == 2 {
                self.job_status = OrderStatus.Delivered
                self.statusColor = COLOR.App_Green_COLOR
            }
            if categoryDescription == 3 {
                self.job_status = OrderStatus.Cancelled
                self.statusColor = COLOR.App_Red_COLOR
            }
            if categoryDescription == 4 {
                self.job_status = OrderStatus.Unassigned //OrderStatus.Cancelled
                self.statusColor = COLOR.App_Status_Unassigned_Color //COLOR.App_Red_COLOR
            }
            if categoryDescription == 8 {
                self.job_status = OrderStatus.Cancelled
                self.statusColor = COLOR.App_Red_COLOR
            }
            if categoryDescription == 9 {
                self.job_status = OrderStatus.Cancelled
                self.statusColor = COLOR.App_Red_COLOR
            }
            if categoryDescription == 10 {
                self.job_status = OrderStatus.Cancelled
                self.statusColor = COLOR.App_Red_COLOR
            }
            if categoryDescription == 11 {
                self.job_status = OrderStatus.Cancelled
                self.statusColor = COLOR.App_Red_COLOR
            }
        }
        if let categoryDescription = param["job_latitude"] as? Int {
            self.job_latitude = categoryDescription
        }
        if let categoryDescription = param["job_longitude"] as? Int {
            self.job_longitude = categoryDescription
        }
        if let categoryDescription = param["customer_email"] as? String {
            self.customer_email = categoryDescription
        }
        if let categoryDescription = param["customer_phone"] as? String {
            self.customer_phone = categoryDescription
        }
        if let categoryDescription = param["category_description"] as? String {
            self.category_description = categoryDescription
        }
        
        if let categoryId = param["job_id"] as? Int {
            self.category_id = categoryId
        }
        
        if let categoryImage = param["category_image"] as? String {
            self.category_image = categoryImage
        }
        if let categoryName = param["category_name"] as? String {
            self.category_description = categoryName
        }
        
        if let creationDatetime = param["creation_datetime"] as? String {
             self.creation_datetime = self.utcToLocal(date: creationDatetime)
        }
        if let creationDatetime = param["job_delivery_datetime"] as? String {
            self.job_delivery_datetime = self.utcToLocal(date: creationDatetime)
//             = creationDatetime
        }
        
        if let isBlocked = param["is_blocked"] as? Bool {
            self.is_blocked = isBlocked
        }
        if let favLocations = param["products"] as? [[String: Any]] {
            print(favLocations)
            
            for item in favLocations {
                let rollData = BQProductListModel.init(param: item)
                self.productListModel.append(rollData)
            }
            print(self.productListModel.count)
        }
        
        
    }
    
    
    func utcToLocal(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if let dt = dateFormatter.date(from: date) {
            dateFormatter.timeZone = TimeZone.current
            dateFormatter.dateFormat = "hh:mma, dd MMM yyyy" //09:00pm, 28 Sep 2017
            return dateFormatter.string(from: dt)
        }
        return ""
    }
    
}
