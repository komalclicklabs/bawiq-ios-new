//
//  ViewCardViewcontroller.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 02/03/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class ViewCardViewcontroller: UIViewController {

    
    
    
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var cardDetailsLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var CARDLABEL: UILabel!
    @IBOutlet weak var validThruLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var DebitTypeLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
    
    var cardData:cardsModel?
    var completionHandler : (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        giveUiAttributes()
        addShadowToView()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addShadowToView(){
        cardView.layer.shadowColor = UIColor.black.cgColor
        cardView.layer.shadowOpacity = 0.3
        cardView.layer.shadowOffset = CGSize(width: 0, height: 7)
        cardView.layer.shadowRadius = 6
        cardView.layer.cornerRadius = 4.3
    }
    
    
    @IBAction func backButtonMenuAction(_ sender: UIButton) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func giveUiAttributes(){
        //HEADING LABEL
        let firstAttributedText = NSMutableAttributedString(string: TEXT.VIEW , attributes: [NSFontAttributeName:UIFont(name: FONT.light, size: FONT_SIZE.large)!])
        let secondAttributedText = NSMutableAttributedString(string: " " + TEXT.CARD_TEXT, attributes: [NSFontAttributeName:UIFont(name: FONT.regular, size: FONT_SIZE.large)!])
        firstAttributedText.append(secondAttributedText)
        headingLabel.attributedText = firstAttributedText
        headingLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        cardDetailsLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.smallest)
        cardDetailsLabel.text = TEXT.CARD_DETAILS.capitalized
        if cardData != nil{
            CARDLABEL.text  = "**** **** **** \(cardData!.lastFourDigit)"
            dateLabel.text = cardData?.expiryDate
            DebitTypeLabel.text = cardData?.funding
            DebitTypeLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        }

         validThruLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.extraSmall)
         validThruLabel.textColor = COLOR.SPLASH_TEXT_COLOR
         validThruLabel.text = TEXT.VALID_THRU
         typeLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.extraSmall)
         typeLabel.textColor = COLOR.SPLASH_TEXT_COLOR
         typeLabel.text = cardData?.brand ?? TEXT.TYPE
        
         dateLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
         dateLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        cardDetailsLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        
        
         DebitTypeLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        
        CARDLABEL.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)
        CARDLABEL.textColor = COLOR.ADD_NEW_CARD_HEADING_LABEL
        deleteButton.setTitle("   "+TEXT.Delete_Card, for: UIControlState.normal)
        deleteButton.setTitleColor(JOB_STATUS_COLOR.DECLINED, for: UIControlState.normal)
        deleteButton.titleLabel?.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
        
        cardView.backgroundColor = COLOR.popUpColor
        self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        
    }
    
   @IBAction func deleteButtonAction(_ sender: UIButton) {
      let  alertView = UIAlertController(title: "", message: TEXT.DELETE_CARD_POPUP_TEXT, preferredStyle: UIAlertControllerStyle.alert)
      let cancelAction = UIAlertAction(title: TEXT.CANCEL_ALERT, style: UIAlertActionStyle.default) { (action) in
         print("asd")
         
      }
      let yesAction = UIAlertAction(title: TEXT.YES_ACTION, style: UIAlertActionStyle.default) { (action) in
         
         var paymentMethodToSend = Int()
         for paymentMethod in Singleton.sharedInstance.formDetailsInfo.paymentMethodArray {
            if paymentMethod != 8 {
               paymentMethodToSend = paymentMethod
               break
            }
         }
            let param = [
                "access_token" : Vendor.current!.appAccessToken!,
                "app_device_type" : APP_DEVICE_TYPE,
                "payment_method": "\(paymentMethodToSend)",
                "card_id" : self.cardData?.id
            ]
         
            ActivityIndicator.sharedInstance.showActivityIndicator()
            APIManager.sharedInstance.serverCall(apiName: "delete_merchant_card", params: param as [String : AnyObject]?, httpMethod: "POST", receivedResponse: { (isSuccess, data) in
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSuccess == true{
                    print(data)
                    if self.completionHandler != nil{
                        self.completionHandler!()
                    }
                   _ = self.navigationController?.popViewController(animated: true)
                }
            })
        
      }
        alertView.addAction(cancelAction)
         alertView.addAction(yesAction)
        self.present(alertView, animated: true, completion: nil)
        
    }

}
