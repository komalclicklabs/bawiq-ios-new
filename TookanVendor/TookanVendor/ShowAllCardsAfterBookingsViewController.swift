//
//  ShowAllCardsAfterBookingsViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 28/02/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit
import WebKit
import GoogleMaps
import GooglePlaces
import CoreLocation


class ShowAllCardsAfterBookingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,ErrorDelegate,GMSMapViewDelegate{

    
    @IBOutlet weak var addNewCardLabel: UILabel!
    @IBOutlet weak var ammountLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var chooseFromLabel: UILabel!
    @IBOutlet weak var wontBeChargedLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    
    var errorMessageView:ErrorView!
    var delegate : updateMetaData?
    var pickupCustomFieldTemplate = ""
    var selectedLat = Double()
    var selectedLong = Double()
    var isNow = true
    var dateInString = String()
    var dateInDateFormat = Date()
    var ammountLabelText : String?
    
    
    var isForPayment = false
    var forPending = false
    
    @IBOutlet weak var addDropLocation: UIButton!
    var deliveryAdded = true
    var selected = Int()
    var pickUpMetaData = [Any]()
    
    var selectedDropLat = Double()
    var selectedDropLong = Double()
    var selectedDestinationAddress = String()
    var currencyId = ""
    
    
    var AddDestination:(addDestination:Bool,onCompletion:(_ estimatedPrice:String)->Void)?
    var cardsArray = [cardsModel]()
    var keyboardSize:CGSize = CGSize(width: 0.0, height: 0.0)
    var amount : String?
    
    var allCardsArray = [Any]()
    var afterSelecting : ((_ id:String,_ Type:String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        giveColorToViewsAndLabels()
        
        if forPending == true{
            backButton.isHidden = true
        }
        addNewCardLabel.text = TEXT.tapOnPlus
        addNewCardLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.carTypeFontSize)
        addNewCardLabel.textColor = COLOR.FILLED_IMAGE_COLOR
        wontBeChargedLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)
        wontBeChargedLabel.text = "You won't be charged for this Task."
        tableView.register( UINib(nibName: NIB_NAME.PayementMethodCell, bundle: Bundle.main)    , forCellReuseIdentifier: CELL_IDENTIFIER.payementMethod)
        // Do any additional setup after loading the view.
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 120
        self.tableView.separatorStyle = .none
        setTitleView()
        
        ammountLabel.text = amount!
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh(_:)), for: .valueChanged)
        
        
        if Singleton.sharedInstance.formDetailsInfo.verticalType == .taxi {
            ammountLabel.textColor = COLOR.THEME_FOREGROUND_COLOR
            
            if deliveryAdded == false{
                addDropLocation.setTitle(TEXT.getFareEstimate, for: UIControlState.normal)
                addDropLocation.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.buttonTitle)
                addDropLocation.setTitleColor(COLOR.THEME_FOREGROUND_COLOR, for: UIControlState.normal)
                
                addDropLocation.isHidden = false
                ammountLabel.isHidden = true
                priceLabel.isHidden = true
            }else{
                addDropLocation.isHidden = true
                ammountLabel.isHidden = false
                priceLabel.isHidden = false
            }
        }else{
            ammountLabel.isHidden = false
            priceLabel.isHidden = false
            addDropLocation.isHidden = true
        }
        
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.backgroundView = refreshControl
        }
        
        apiHitTOGetAllCards()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
     _ =  self.navigationController?.popViewController(animated: true)
    }
    
    func setTitleView() {
        let titleView = UINib(nibName: NIB_NAME.titleView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TitleView
        titleView.frame = CGRect(x: 0, y: HEIGHT.navigationHeight, width: self.view.frame.width, height: HEIGHT.titleHeight)
        titleView.backgroundColor = UIColor.clear
        self.view.addSubview(titleView)
        titleView.setTitle(title: TEXT.selectA , boldTitle: TEXT.paymentMethod.capitalized)
        if ammountLabelText == nil  {
        priceLabel.text = TEXT.TOTAL_AMMOUNT
        }else{
        priceLabel.text = ammountLabelText
        }
        priceLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        priceLabel.textColor = COLOR.ADD_NEW_CARD_HEADING_LABEL
        ammountLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        ammountLabel.textColor = COLOR.ADD_NEW_CARD_HEADING_LABEL
        chooseFromLabel.text = TEXT.CHOOSE_FROM_TEXT
        chooseFromLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.smallest)
        addButton.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
        addButton.layer.cornerRadius = 51/2
        addButton.setShadow()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
       return cardsArray.count
        }else{
            if self.forPending == true{
                return 0
            }else{
            return 1
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.payementMethod, for: indexPath) as! PaymentMethodTableViewCell
        if indexPath.section == 0 {
        cell.getData(showRightImage: false, rightImageLabel: "**** **** **** \(cardsArray[indexPath.row].lastFourDigit)", leftString: "", rightImage: #imageLiteral(resourceName: "sampleCard"), showImage: true   )
        }else{
            cell.getData(showRightImage: false, rightImageLabel: TEXT.PayByCash, leftString: "", rightImage:#imageLiteral(resourceName: "iconCash"), showImage: true   )
        }
        return cell
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if Singleton.sharedInstance.formDetailsInfo.paymentMethodArray.contains(8) == true  {
            return 2
        }else{
            return 1
        }
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // self.delegate?.updateMetaData(data: self.pickUpMetaData)
          if self.forPending == false{
            let endIndex = Singleton.sharedInstance.formDetailsInfo.call_tasks_as.index(Singleton.sharedInstance.formDetailsInfo.call_tasks_as.endIndex, offsetBy: -1)
            
            let calTaskAs = Singleton.sharedInstance.formDetailsInfo.call_tasks_as[Singleton.sharedInstance.formDetailsInfo.call_tasks_as.index(before: Singleton.sharedInstance.formDetailsInfo.call_tasks_as.endIndex)] == "s" ? Singleton.sharedInstance.formDetailsInfo.call_tasks_as.substring(to: endIndex).lowercased() :  Singleton.sharedInstance.formDetailsInfo.call_tasks_as
            
            
        Singleton.sharedInstance.showAlertWithOption(owner: self, title: "Are you sure you want to book this \(calTaskAs) ?", message: "", showRight: true, leftButtonAction: {
            print("cancel")
        }, rightButtonAction: { 
        if Singleton.sharedInstance.formDetailsInfo.verticalType != .taxi {
            if indexPath.section == 0{
        self.afterSelecting!(self.cardsArray[indexPath.row].id,"\(Singleton.sharedInstance.formDetailsInfo.paymentMethodArray[0])")
            }else{
        self.afterSelecting!("","\(Singleton.sharedInstance.formDetailsInfo.paymentMethodArray[1])")
            }
        }else{
            var dateString = String()
            
            if self.isNow == true{
          let dateArray = "\(Date().addingTimeInterval(60*5))".components(separatedBy: " ")
          dateString = ("\(dateArray[0]) " + "\(dateArray[1])").convertDateFromUTCtoLocal(input: "yyyy-MM-dd HH:mm:ss", output: "yyyy-MM-dd HH:mm:ss")
            }else{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd MMM yyyy hh:mm a"
                dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
                self.dateInDateFormat = dateFormatter.date(from: self.dateInString)!
                
                let difference =  self.dateInDateFormat.timeIntervalSince(Date())
                print(self.dateInDateFormat)
                print(Int(difference/60.0))
                print( Singleton.sharedInstance.formDetailsInfo.scheduleOffsetTime)
                if Int(difference/60.0) < (Singleton.sharedInstance.formDetailsInfo.scheduleOffsetTime/60){
                    _ = self.navigationController?.popViewController(animated: true)
                    Singleton.sharedInstance.showAlert(TEXT.scheduleErrorMessage)
                    return
                    
                }
                dateString = ("\(self.dateInString)").convertDateFromUTCtoLocal(input: "dd MMM yyyy hh:mm a", output: "yyyy-MM-dd HH:mm:ss",toConvert:false)
            }
          print(dateString)
            if indexPath.section == 0{
                self.makeMetaData(paymentMethod: Singleton.sharedInstance.formDetailsInfo.paymentMethodArray[0])
                self.makeMetaData(paymentMethod: Singleton.sharedInstance.formDetailsInfo.paymentMethodArray[0])
                
                self.delegate?.updateMetaData(data: self.pickUpMetaData, date: dateString, currency: self.currencyId, paymentMethod: "\(Singleton.sharedInstance.formDetailsInfo.paymentMethodArray[0])", cardId: self.cardsArray[indexPath.row].id, ammount: "",isNow: self.isNow,tip:"")
            }else{
                Singleton.sharedInstance.formDetailsInfo.payementMethod = Singleton.sharedInstance.formDetailsInfo.paymentMethodArray[1]
                self.makeMetaData(paymentMethod: Singleton.sharedInstance.formDetailsInfo.paymentMethodArray[1])
                
                self.delegate?.updateMetaData(data: self.pickUpMetaData, date: dateString, currency: self.currencyId, paymentMethod: "\(Singleton.sharedInstance.formDetailsInfo.paymentMethodArray[1])", cardId: "", ammount: "",isNow: self.isNow,tip:"")
            }
            _ = self.navigationController?.popViewController(animated: true)
        }
                
               }, leftButtonTitle: TEXT.CANCEL.capitalized, rightButtonTitle: TEXT.YES_ACTION.capitalized)
            
        }else{
            
            self.apiHitForPendingPayment(cardId: self.cardsArray[indexPath.row].id, paymentMethod: "\(Singleton.sharedInstance.formDetailsInfo.paymentMethodArray[0])")
            
        }
        
    }
    
    func apiHitTOGetAllCards(){
        
        var paymentMethodToSend = Int()
        if Singleton.sharedInstance.formDetailsInfo.paymentMethodArray.contains(2) == true{
            paymentMethodToSend = 2
        }else if Singleton.sharedInstance.formDetailsInfo.paymentMethodArray.contains(4) == true{
            paymentMethodToSend = 4
        }
        
        let param = [
            "access_token" : Vendor.current!.appAccessToken!,
            "app_device_type" : APP_DEVICE_TYPE,
            "payment_method": "\(paymentMethodToSend)"
            ] as [String : String]
        print(param)
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.serverCall(apiName: API_NAME.fetchCards, params: param as [String : AnyObject]?, httpMethod: "POST") { (isSuccess, data) in
            print(data)
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if isSuccess == true{
                if let dataObject = data["data"] as? [Any]{
                    print(dataObject)
                    self.cardsArray.removeAll()
                    if dataObject.count > 0 {
                        for i in dataObject {
                            self.cardsArray.append(cardsModel(json: i as! [String : Any]))
                        }
                        if dataObject.count >= 2{
                            self.addNewCardLabel.isHidden = true
                        }else{
                            self.addNewCardLabel.isHidden = false
                        }
                        self.chooseFromLabel.text = TEXT.CHOOSE_FROM_TEXT
                        self.chooseFromLabel.textAlignment = NSTextAlignment.center
                    }else{
                        //self.addButtonAction(UIButton())
                        self.chooseFromLabel.text = TEXT.NO_CARDS_FOUND
                        self.chooseFromLabel.textAlignment = NSTextAlignment.center
                    }
                    self.tableView.reloadData()
                }else{
                   // self.addButtonAction(UIButton())
                }
            }else{
                
                if let message = data["message"] as? String{
                    if message == "No data found."{
                        if let dataObject = data["data"] as? [Any]{
                            print(dataObject)
                            self.cardsArray.removeAll()
                            if dataObject.count > 0 {
                                for i in dataObject {
                                    self.cardsArray.append(cardsModel(json: i as! [String : Any]))
                                }
                                if dataObject.count >= 2{
                                    self.addNewCardLabel.isHidden = true
                                }else{
                                    self.addNewCardLabel.isHidden = false
                                }
                                self.chooseFromLabel.text = TEXT.CHOOSE_FROM_TEXT
                                self.chooseFromLabel.textAlignment = NSTextAlignment.center
                            }else{
                                //self.addButtonAction(UIButton())
                                self.chooseFromLabel.text = TEXT.NO_CARDS_FOUND
                                self.chooseFromLabel.textAlignment = NSTextAlignment.center
                            }
                            self.tableView.reloadData()
                        }
                    }else{
                        self.chooseFromLabel.text = TEXT.NO_CARDS_FOUND
                        self.chooseFromLabel.textAlignment = NSTextAlignment.center
                        self.showErrorMessage(error:  data["message"] as! String!, isError: true    )
                    }
                }
                
                
            }
        }
    }
    
    
    func refresh(_ refreshControl: UIRefreshControl) {
        // Do your job, when done:
        apiHitTOGetAllCards()
        refreshControl.endRefreshing()
    }
    
    @IBAction func addDestination(_ sender: UIButton) {
        
        
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        let latestLat = self.selectedLat
        let latestLong = self.selectedLong
        let currentLocation = CLLocation(latitude: latestLat, longitude: latestLong)
        
        autocompleteController.autocompleteBounds = GMSCoordinateBounds(coordinate: currentLocation.coordinate, coordinate: currentLocation.coordinate)
        self.present(autocompleteController, animated: true, completion: nil)
    }
    
    
    
    @IBAction func addButtonAction(_ sender: UIButton) {
        if IJReachability.isConnectedToNetwork()  == true{
        let vc = self.storyboard?.instantiateViewController(withIdentifier: STORYBOARD_ID.addCardWeb) as? AddCardWebViewViewController
        vc?.completionHandler = {
            self.apiHitTOGetAllCards()
        }
            self.present(vc!, animated: true, completion: nil)}
        else{
        self.showErrorMessage(error: ERROR_MESSAGE.NO_INTERNET_CONNECTION, isError: true)
        }
        
    }
    
    func showErrorMessage(error:String,isError:Bool) {
        if errorMessageView == nil {
            errorMessageView = UINib(nibName: NIB_NAME.errorView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ErrorView
            errorMessageView.delegate = self
            errorMessageView.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: HEIGHT.errorMessageHeight)
            self.view.addSubview(errorMessageView)
            errorMessageView.setErrorMessage(message: error,isError: isError)
        }
    }
    
    func removeErrorView() {
        self.errorMessageView = nil
    }
    
    
    func makeMetaData(paymentMethod : Int = Singleton.sharedInstance.formDetailsInfo.payementMethod )  {
        var pickUpMentaData = [[String:String]]()
        for i in Singleton.sharedInstance.createTaskDetail.pickupMetadata{
            if let data  = i as? CustomFieldDetails {
                pickupCustomFieldTemplate = data.template_id!
                print(selected)
                switch data.label! {
                case "baseFare":
                    pickUpMentaData.append(["label":data.label!,"data":Singleton.sharedInstance.typeCategories[selected].baseFare])
                case "destinationAddress":
                    pickUpMentaData.append(["label":data.label!,"data":selectedDestinationAddress])
                case "destinationLatitude":
                    pickUpMentaData.append(["label":data.label!,"data":"\(selectedDropLat)"])
                case "destinationLongitude":
                    pickUpMentaData.append(["label":data.label!,"data":"\(selectedDropLong)"])
                case "categoryId":
                    pickUpMentaData.append(["label":data.label!,"data":"\(Singleton.sharedInstance.typeCategories[selected].categoryId )"])
                case "paymentMode":
                    pickUpMentaData.append(["label":data.label!,"data":"\(paymentTypeDict["\(paymentMethod)"]!)"])
                case "paymentMethod":
                    pickUpMentaData.append(["label":data.label!,"data":"\(paymentMethod)"])
                case "cancellationCharges":
                    pickUpMentaData.append(["label":data.label!,"data":"\(Singleton.sharedInstance.typeCategories[selected].cancellation_charges)"])
                default:
                    pickUpMentaData.append(["label":data.label!,"data":data.data!])
                }
                
            }
        }
        print(pickUpMentaData)
        print(Singleton.sharedInstance.typeCategories[selected])
        self.pickUpMetaData = pickUpMentaData
    }
    
   
    func getEstimatedCost(){
        makeMetaData()
        let param = [
            "access_token":"\(Vendor.current!.appAccessToken!)",
            "device_token":"\(APIManager.sharedInstance.getDeviceToken())",
            "app_version":"\(APP_VERSION)",
            "app_device_type":APP_DEVICE_TYPE,
            "app_access_token":"\(Vendor.current!.appAccessToken!)",
            "pickup_latitude":"\(selectedLat)",
            "pickup_longitude":"\(selectedLong)",
            "user_id":"\(Singleton.sharedInstance.formDetailsInfo.user_id!)",
            "pickup_meta_data":self.pickUpMetaData,
            "vendor_id":"\(Vendor.current!.id!)",
            "payment_method": Singleton.sharedInstance.formDetailsInfo.paymentMethodArray[0],
            "layout_type":"\(Singleton.sharedInstance.formDetailsInfo.work_flow!)",
            "has_pickup":"1",
            "has_delivery":"0",
            "form_id":Singleton.sharedInstance.formDetailsInfo.form_id!
            ] as [String : Any]
        
        print(param)
        
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.serverCall(apiName: "fare_estimate", params: param as [String : AnyObject]?, httpMethod: "POST", receivedResponse: { (isSuccess, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            
            if isSuccess == true    {
                if let data = response["data"] as? [String:Any]{
                    if let value = data["currency"] as? [String:Any]{
                      guard let currencyValue = Currency(json: value) else {
                        return
                      }
                        if let totalCost = data["fare_lower_limit"] as? NSNumber{
                            if let upperCost = data["fare_upper_limit"] as? NSNumber{
                            self.amount = "\(currencyValue.symbol) \(totalCost.doubleValue) - \(currencyValue.symbol) \(upperCost.doubleValue)"
                            self.deliveryAdded = true
                            self.viewDidLoad()
                            }
                        }
                    }
                }
            }else{
                
            }
            print(response)
        })
    }
    
    func apiHitForPendingPayment(cardId:String,paymentMethod:String){
        
        let param  = [
        "access_token":Vendor.current!.appAccessToken!,
        "device_token" :APIManager.sharedInstance.getDeviceToken(),
        "app_version":APP_VERSION,
        "app_device_type":APP_DEVICE_TYPE,
        "app_access_token":Vendor.current!.appAccessToken!,
        "user_id":Vendor.current!.userId!,
        "card_id":"\(cardId)",
        "payment_method":"\(paymentMethod)",
        "form_id":Singleton.sharedInstance.formDetailsInfo.form_id!
        
        ] as [String : Any]
        
        print(param)
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.serverCall(apiName: "make_pending_payment", params: param as [String : AnyObject]?, httpMethod: "POST") { (isSuccess, Response) in
            print(Response)
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if isSuccess == true{
                    Vendor.current!.pendingAmount = 0.0
                    //Singleton.sharedInstance.pushToHomeScreen()
                Singleton.sharedInstance.checkForLogin(owener: self, startLoaderAnimation: { 
                    ActivityIndicator.sharedInstance.showActivityIndicator()
                }, stopLoaderAnimation: { 
                     ActivityIndicator.sharedInstance.hideActivityIndicator()
                })
            }else{
                if let message = Response["message"] as? String{
                self.showErrorMessage(error: message, isError: true)
                }
            }
        }
    }
    
    
    func giveColorToViewsAndLabels(){
        wontBeChargedLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        priceLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        self.view.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        self.tableView.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
        addNewCardLabel.textColor = COLOR.SPLASH_TEXT_COLOR
    }
    
 
    
    
}





extension ShowAllCardsAfterBookingsViewController: GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: \(place.name)")
        print("Place address: \(place.formattedAddress)")
        print("Place attributions: \(place.coordinate.latitude)")
        print("Place attributions: \(place.coordinate.longitude)")
         self.dismiss(animated: true, completion: nil)
        selectedDropLat = place.coordinate.latitude
        Singleton.sharedInstance.selectedDestinationLatForTaxi = place.coordinate.latitude
        selectedDropLong = place.coordinate.longitude
         Singleton.sharedInstance.selectedDestinationlongForTaxi = place.coordinate.longitude
        selectedDestinationAddress = place.formattedAddress!
        Singleton.sharedInstance.selectedDestinationAddressForTaxi = place.formattedAddress!
        getEstimatedCost()
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
}
}
