//
//  PopupPromotionVC.swift
//  TookanVendor
//
//  Created by Harshit Parikh on 08/10/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit
import Alamofire

class PopupPromotionVC: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var promotionImage: UIImageView!
    @IBOutlet weak var promotionTitle: UILabel!
    @IBOutlet weak var promotionDescription: UITextView!
    
    
    // MARK: - Button Action
    
    @IBAction func dismissBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: {
            Vendor.current?.promotions = nil
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
    }
    
    private func setData() {
        self.promotionTitle.text = Vendor.current?.promotions?.title ?? ""
        self.promotionDescription.text = Vendor.current?.promotions?.description ?? ""
        if let imgString = Vendor.current?.promotions?.image_url {
            let imageUrl = URL.get(from: imgString)
            self.promotionImage.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "logoTookanSplashGraphic"))
        } else {
            self.promotionImage.image = #imageLiteral(resourceName: "logoTookanSplashGraphic")
        }
    }

}
