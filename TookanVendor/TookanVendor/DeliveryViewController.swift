//
//  DeliveryViewController.swift
//  TookanVendor
//
//  Created by Vishal on 15/02/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class DeliveryViewController: UIViewController, DeliveryView, DateTimeSelectionCellDelegate, DeliveryAddressDelegate, BQSelectDateTimeVCDelegate, BQUpdateAddressVCDelegate {
   

    @IBOutlet weak var deliveryTable: UITableView!
    var presenter: DeliveryPresenter!
    var selectedDateTime = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //self.navigationItem.title = "Delivery Address"
        setUpPresent()
        setupTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let cartSegmentVc = self.parent as? CartSegmentViewController {
            cartSegmentVc.navigationBar.titleLabel.text = "Delivery Address"
        }
    }

    private func setUpPresent() {
        presenter = DeliveryPresenterImplementation(withView: self)
    }
    
    private func setupTable() {
        deliveryTable.delegate = self
        deliveryTable.dataSource = self
        deliveryTable.register(UINib(nibName: "DeliveryAddressCell", bundle: nil), forCellReuseIdentifier: "DeliveryAddressCell")
        deliveryTable.register(UINib(nibName: "ScheduleDateTimeTableViewCell", bundle: nil), forCellReuseIdentifier: "ScheduleDateTimeTableViewCell")
        
//        deliveryTable.rowHeight = 200
//        deliveryTable.estimatedRowHeight = UITableViewAutomaticDimension
    }
    
    
    func reloadTable() {
        deliveryTable.reloadData()
    }

    func selectDateTime() {
        let selectDateVC = BQSelectDateTimeVC(nibName: "BQSelectDateTimeVC", bundle: nil)
        selectDateVC.modalPresentationStyle = .overCurrentContext
        selectDateVC.delegate = self
        self.present(selectDateVC, animated: true, completion: nil)
        
    }
    
    func editAddress() {
        if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQUpdateAddressVC") as? BQUpdateAddressVC {
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func continueAction(_ sender: Any) {
        if BQBookingModel.shared.isScheduled != 1 {
            BQBookingModel.shared.isScheduled = 0
        }
    
        
        if BQBookingModel.shared.isScheduled == 1 {
            if let parent = self.parent as? CartSegmentViewController {
                parent.switchController(from: CartController.delivery)
            }
        } else {
            let startHour: Int = 7   // 7 am
            let endHour: Int = 23   // 11 pm
            let date1: Date = Date()
            if let gregorian: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian) {
                var components: DateComponents = gregorian.components(([.day, .month, .year]), from: date1)
                let hour = gregorian.component(.hour, from: date1)
                
                if startHour > hour {
                    ErrorView.showWith(message: "Sorry, we are operational from 7 am to 11 pm", isErrorMessage: true, removed: nil)
                } else if hour >= endHour {
                    ErrorView.showWith(message: "Sorry, we are operational from 7 am to 11 pm", isErrorMessage: true, removed: nil)
                } else {
                    if let parent = self.parent as? CartSegmentViewController {
                        parent.switchController(from: CartController.delivery)
                    }
                }
            }
        }
        
        
        
        
    }
    
    func bookingDate(date: String) {
        selectedDateTime = date
        BQBookingModel.shared.dateTime = self.localToUTC(date: selectedDateTime)
        deliveryTable.reloadData()
    }
    
    func showError(message: String) {
        Singleton.sharedInstance.showAlert(message)
    }
    
    private func localToUTC(date: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy hh:mm a" //"dd-MMM-yyyy hh:mm a" // "MMM dd, yyyy hh:mm a"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        if let dt = dateFormatter.date(from: date) {
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            return dateFormatter.string(from: dt)
        }
        return ""
    }
    
    func saveUserContactAddress() {
        deliveryTable.reloadData()
    }
    
}

extension DeliveryViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.numberOfSection()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfItemsInTable()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: presenter.identifierforSection(section: indexPath.section), for: indexPath) as? DeliveryAddressCell else {
                return UITableViewCell()
            }
            cell.backgroundColor = .clear
            cell.selectionStyle = .none
            cell.setUserData()
            cell.delegate = self
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: presenter.identifierforSection(section: indexPath.section), for: indexPath) as? ScheduleDateTimeTableViewCell else {
                return UITableViewCell()
            }
            cell.selectionStyle = .none
            cell.delegate = self
            if selectedDateTime != "" {
                cell.dateTimeBtn.isSelected = true
                BQBookingModel.shared.isScheduled = 1
                cell.dateTimeBtn.setImage(#imageLiteral(resourceName: "radioFill"), for: .normal)
                cell.selectedDateTime.text = selectedDateTime
            }
            
            return cell
            
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: presenter.identifierforSection(section: indexPath.section), for: indexPath) as? DateTimeSelectionCell else {
//                return UITableViewCell()
//            }
//            cell.selectionStyle = .none
//            cell.delegate = self
//            if selectedDateTime != "" {
//                cell.btnSelectDate.isSelected = true
//                BQBookingModel.shared.isScheduled = 1
//                cell.btnSelectDate.setImage(#imageLiteral(resourceName: "radioFill"), for: .normal)
//
//                cell.descriptionLabel.isHidden = true
//                cell.selectedDateTime.text = selectedDateTime
//            }
//
//            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1  {
            return 44
        }
        return UITableViewAutomaticDimension
    }
    
    
}

extension DeliveryViewController: ScheduleDateTimeDelegate {
    func dateTimeSelected() {
        self.selectDateTime()
    }
}

protocol DeliveryView: class  {
    func reloadTable()
}

protocol DeliveryPresenter {
    func numberOfSection() -> Int
    func numberOfItemsInTable() -> Int
    func identifierforSection(section: Int) -> String
}

class DeliveryPresenterImplementation: DeliveryPresenter {

    weak var view: DeliveryView?
    
    init(withView view: DeliveryView) {
        self.view = view
    }
    
    func numberOfItemsInTable() -> Int {
        return 1
    }
    
    func numberOfSection() -> Int {
        return 2
    }
    
    
    func identifierforSection(section: Int) -> String {
        if section == 0 {
            return "DeliveryAddressCell"
        }
        return "ScheduleDateTimeTableViewCell" //"DateTimeSelectionCell"
    }
    
}

protocol DateTimeSelectionCellDelegate {
    func selectDateTime()
}

class DateTimeSelectionCell: UITableViewCell {
    var delegate: DateTimeSelectionCellDelegate?
    
//    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var selectedDateTime: UILabel!
    @IBOutlet weak var btnSelectDate: UIButton!
    @IBOutlet weak var descriptionTopConstraint: NSLayoutConstraint! // Default 8
    @IBOutlet weak var dateTimeTopConstraint: NSLayoutConstraint! //Default 20
    @IBOutlet weak var dateTimeBottomConstraint: NSLayoutConstraint! //Default 24
    
    
    @IBAction func btnSelectDatePressed(_ sender: Any) {
        if btnSelectDate.isSelected == true {
            btnSelectDate.isSelected = false
            BQBookingModel.shared.isScheduled = 0
            btnSelectDate.setImage(#imageLiteral(resourceName: "radio"), for: .normal)
            selectedDateTime.text = "Schedule as per your convinience"
//            descriptionTopConstraint.constant = 8
//            dateTimeTopConstraint.constant = 20
//            dateTimeBottomConstraint.constant = 15
//            selectedDateTime.numberOfLines = 0
            descriptionLabel.backgroundColor = .clear
            descriptionLabel.isHidden = false
        } else {
            delegate?.selectDateTime()
        }
    }
}
