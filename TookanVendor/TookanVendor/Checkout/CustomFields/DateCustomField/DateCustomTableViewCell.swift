//
//  DateCustomTableViewCell.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 07/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class DateCustomTableViewCell: UITableViewCell,UITextFieldDelegate {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!

    let datePickerView:UIDatePicker = UIDatePicker()
    
    
    var selfType : CustomFieldTypes?
    var isStatic : Bool?
    var dataForNonStatic : CustomFieldDetails?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.delegate = self
        // Initialization code
    }

    
    func setUpForCustomField(type:CustomFieldTypes,textValueInCaseOfStatic:String,isForPickUp:Bool,isStatic:Bool,dataForNonStatic:CustomFieldDetails?,header:String){
        setUiAttributes(placeHolderText: header)
        self.label.text = header
        self.textField.placeholder = "Enter " + header
        selfType = type
        self.isStatic = isStatic
        
       // setTextField(type: type)
        self.dataForNonStatic = dataForNonStatic
        setUpDatePicker()
        if [CustomFieldTypes.dateTime,.endDateTime,.startDateTime,.dateTimeFuture,.dateTimePast].contains(type) == true  {
        isStatic == true ? (self.textField.text = textValueInCaseOfStatic.convertDateFromUTCtoLocal(input: "yyyy-MM-dd HH:mm:ss", output: "dd MMM yyyy, hh:mm a", toConvert: false)) : (self.textField.text = dataForNonStatic?.data?.convertDateFromUTCtoLocal(input: "yyyy-MM-dd HH:mm:ss", output: "dd MMM yyyy, hh:mm a", toConvert: false))
        }else{
           self.textField.text = dataForNonStatic?.data?.convertDateFromUTCtoLocal(input: "yyyy-MM-dd HH:mm:ss", output: "dd MMM yyyy", toConvert: false)
        }
        
        if dataForNonStatic != nil{
         if dataForNonStatic?.app_side == .readOnly ||  dataForNonStatic?.app_side == .hidden {
                self.textField.isUserInteractionEnabled = false
            }else{
                self.textField.isUserInteractionEnabled = true
            }
        }else{
            self.textField.isUserInteractionEnabled = true
        }
        
        
    }
    
    func setUiAttributes(placeHolderText:String)  {
        textField.font = UIFont(name: FONT.light, size: FONT_SIZE.priceFontSize)
        label.font = UIFont(name: FONT.light, size: FONT_SIZE.smallest)
        label.textColor = UIColor.black.withAlphaComponent(0.6)
    }
    
    @IBAction func textFieldEditing(_ sender: UITextField) {
        
         setUpDatePicker()
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
        
        
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        saveDate(date: dateFormatter.string(from: sender.date))
        if  [CustomFieldTypes.dateTime,.endDateTime,.startDateTime,.dateTimeFuture,.dateTimePast].contains(selfType!) == true {
            dateFormatter.dateFormat = "dd MMM yyyy, hh:mm a"
        }else{
            dateFormatter.dateFormat = "dd MMM yyyy"
        }
        textField.text = dateFormatter.string(from: sender.date)
    }
    
    
    func setUpDatePicker(){
        setUpDateConstraint(type: selfType!)
        switch selfType! {
            
        case .startDateTime,.endDateTime,.dateTime ,.dateTimePast,.dateTimeFuture:
            datePickerView.datePickerMode = UIDatePickerMode.dateAndTime
            
        case .date,.datePast,.dateFuture:
            datePickerView.datePickerMode = UIDatePickerMode.date
            
        default:
            print("")
        }
    }
    
    
    func setUpDateConstraint(type:CustomFieldTypes){
        
        switch type {
        
        case .startDateTime,.endDateTime,.dateTimeFuture,.dateFuture:
            datePickerView.minimumDate = Date().addingTimeInterval(60*6)
            datePickerView.maximumDate = nil
        
        case .date,.dateTime:
            datePickerView.minimumDate = nil
            datePickerView.maximumDate = nil
        
        default:
            datePickerView.minimumDate = nil
            datePickerView.maximumDate = Date()
        
        }
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField.text == ""{
            datePickerValueChanged(sender: self.datePickerView)
        }
    }
    
    func saveDate (date:String){
        if self.isStatic == true{
            self.selfType! == .startDateTime ? (saveDateInSingelton(variable: &Singleton.sharedInstance.createTaskDetail.jobPickupDateTime, date: date)) : (saveDateInSingelton(variable: &Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime, date: date))
        }else{
            self.dataForNonStatic?.data = date
        }
    }
    
    
    func saveDateInSingelton(variable:inout String,date:String){
        variable = date
    }
    
   
}
