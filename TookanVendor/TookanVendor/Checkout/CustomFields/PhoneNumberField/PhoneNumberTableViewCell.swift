//
//  PhoneNumberTableViewCell.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 07/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class PhoneNumberTableViewCell: UITableViewCell,CountryPhoneCodePickerDelegate,UITextFieldDelegate {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var countyCodeTextField: UITextField!
    @IBOutlet weak var countryCodeTextFieldWidth: NSLayoutConstraint!
    @IBOutlet weak var countryCodeLowerConstraint: NSLayoutConstraint!
    @IBOutlet weak var horizontalLine: UILabel!
    
    var isStatic :Bool?
    var isPickUp:Bool?
    var data:CustomFieldDetails?
    var isForProfile = false
    var delegate : ProfileFieldChanged!
    @IBOutlet weak var labelUpperConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUiAttributes()
        setCountryCodeTextField()
        phoneNumber.delegate = self
        // Initialization code
    }
    
    func setUiAttributes(){
        phoneNumber.font = UIFont(name: FONT.light, size: FONT_SIZE.priceFontSize)
        countyCodeTextField.font = UIFont(name: FONT.light, size: FONT_SIZE.priceFontSize)
        label.font = UIFont(name: FONT.light, size: FONT_SIZE.smallest)
        label.textColor = UIColor.black.withAlphaComponent(0.6)
    }
    
   
    func setUpPhoneNumberCell(fortype:CustomFieldTypes,headerName:String,isStatic:Bool,isPickUp:Bool,dataForNonStatic:CustomFieldDetails?,data:String){
        self.isPickUp = isPickUp
        self.isStatic = isStatic
        self.data = dataForNonStatic
        
        self.label.text = headerName
        addTextToFields(withData: data)
        self.phoneNumber.placeholder    = "Enter " + headerName
        self.phoneNumber.keyboardType = .numberPad
        
        if dataForNonStatic != nil{
         if dataForNonStatic?.app_side == .readOnly ||  dataForNonStatic?.app_side == .hidden {
                self.phoneNumber.isUserInteractionEnabled = false
                self.countyCodeTextField.isUserInteractionEnabled = false
            }else{
                self.phoneNumber.isUserInteractionEnabled = true
                self.countyCodeTextField.isUserInteractionEnabled = true
            }
        }
      
      labelUpperConstraint.constant = 5
      countryCodeLowerConstraint.constant = 3
        
    }
    
    
    func addTextToFields(withData : String){
      let dataArray = withData.components(separatedBy: " ")
        if withData != ""{
        if dataArray.count > 1{
            if isForProfile == false {
            countryCodeTextFieldWidth.constant = 50
            }else{
                countryCodeTextFieldWidth.constant = 40
            }
            countyCodeTextField.text = dataArray[0]
            phoneNumber.text = dataArray[1]
        }else{
            self.countryCodeTextFieldWidth.constant = 0
            phoneNumber.text = withData
        }
        }else{
            phoneNumber.text = ""
            setCountryCodeTextField()
        }
    }
    
    
    
    
     func setCountryCodeTextField() {
        let localCountryCode = "+" + NSLocale.locale.getCurrentDialingCode()
        countyCodeTextField.text = localCountryCode
        setCountryCodeFieldInputViewToCountryPicker()
        showRightViewOfCountryTextFieldAsDownArrow()
    }
    
    private func setCountryCodeFieldInputViewToCountryPicker() {
        let countryPicker = CountryPicker()
        countryPicker.countryPhoneCodeDelegate = self
        countyCodeTextField.inputView = countryPicker
    }
    
    private func showRightViewOfCountryTextFieldAsDownArrow() {
//        countyCodeTextField.rightView = UIImageView(image: #imageLiteral(resourceName: "downArrow").renderWithAlwaysTemplateMode())
        countyCodeTextField.rightView?.tintColor = COLOR.SPLASH_TEXT_COLOR
        countyCodeTextField.rightViewMode = .always
    }
    
    func countryPhoneCodePicker(picker: CountryPicker, didSelectCountryCountryWithName name: String, countryCode: String, phoneCode: String) {
        print(countryCode)
        print(phoneCode)
        self.countyCodeTextField.text = "+"+phoneCode.replacingOccurrences(of: "+", with: "")
        if isForProfile == false {
        updateText(withCountryCode: self.countyCodeTextField.text!, phoneNumber: phoneNumber.text!)
        }else{
            
            delegate.profileValueChanged(fieldType: FIELD_TYPE.contact.hashValue, yourText: self.countyCodeTextField.text! + " " + phoneNumber.text!)
        }
    }
    
    func updateText(withCountryCode:String,phoneNumber:String){
        if self.isStatic == true{
            if self.isPickUp == true    {
             Singleton.sharedInstance.createTaskDetail.jobPickupPhone = withCountryCode + " " + phoneNumber
            }else{
                Singleton.sharedInstance.createTaskDetail.customerPhone = withCountryCode + " " + phoneNumber
            }
        }else{
            self.data?.data = withCountryCode + " " + phoneNumber
        }
    }
    
    func setUpForProfile(isEditable:Bool){
        isForProfile = true
        countyCodeTextField.delegate = self
        phoneNumber.delegate = self
        countyCodeTextField.rightView = UIImageView()
        labelUpperConstraint.constant = 5
        countryCodeLowerConstraint.constant = 3
        horizontalLine.isHidden = !isEditable
        phoneNumber.isUserInteractionEnabled = isEditable
        countyCodeTextField.isUserInteractionEnabled = isEditable
        label.font = UIFont(name: FONT.light, size: FONT_SIZE.smallest)
        label.textColor = UIColor(colorLiteralRed: 51/255, green: 51/255, blue: 51/255, alpha: 0.5)
        self.label.text =  "Phone number"
         self.phoneNumber.keyboardType = .numberPad
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumber{
            var textToSend = String()
            if string != ""{
                textToSend = textField.text! + string
            }else{
                textToSend = textField.text!
                textToSend.remove(at:textToSend.index(before:  textToSend.endIndex))
            }
            if isForProfile == false {
            self.updateText(withCountryCode: countyCodeTextField.text!, phoneNumber: textToSend)
            }else{
                delegate.profileValueChanged(fieldType: FIELD_TYPE.contact.hashValue, yourText: countyCodeTextField.text! + " " + textToSend)
            }
            return true
        }
         return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.endEditing(true)
        return true 
    }
    
    
}
