//
//  CheckBoxTableViewCell.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 08/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class CheckBoxTableViewCell: UITableViewCell {

    @IBOutlet weak var checkBoxImage: UIImageView!
    @IBOutlet weak var label: UILabel!
    
    var customFieldData : CustomFieldDetails?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
         setUiAttributes()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUiAttributes(){
        label.font = UIFont(name: FONT.light, size: FONT_SIZE.priceFontSize)
    }
    
    func setUpCheckBoxCell(data:CustomFieldDetails,header:String){
        self.label.text = header
        customFieldData = data
        setSelectionOfTheCheckBox(checkStatus: data.data!)
        
        
    }
    
    func setSelectionOfTheCheckBox(checkStatus:String){
        switch checkStatus {
        case ""," ","false":
            checkBoxImage.image = optionEmpty
            self.customFieldData?.data = "false"
        default:
            checkBoxImage.image = greenTick
            self.customFieldData?.data = "true"
        }
    }
    
    
    func updateStatus(){
        if self.customFieldData?.data == "" || self.customFieldData?.data == "false"{
            self.customFieldData?.data = "true"
        }else{
            self.customFieldData?.data = "false"
        }
        setSelectionOfTheCheckBox(checkStatus: (self.customFieldData?.data)!)
    }
    

}
