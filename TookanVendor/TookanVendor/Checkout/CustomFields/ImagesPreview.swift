//
//  ImagesPreview.swift
//  Tookan
//
//  Created by Rakesh Kumar on 1/27/16.
//  Copyright © 2016 Click Labs. All rights reserved.
//

import UIKit

 protocol ImagePreviewDelegate {
    func dismissImagePreview(customFieldRowIndex:Int)
    func deleteImageActionFromImages(_ index:Int, customFieldRowIndex:Int,header:headerType)
}

class ImagesPreview: UIView, UICollectionViewDataSource, UICollectionViewDelegate, NoInternetConnectionDelegate {

   // MARK: - IBOutlets
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var pageNumber: UILabel!
    @IBOutlet weak var imageCollectionView: UICollectionView!
   
   // MARK: - Properties
    var imageArray = [Any]()
    var delegate:ImagePreviewDelegate!
    var isTopBottomViewHidden = false
    var currentIndex:Int!
    var collectionViewTag:Int!
    var internetToast:NoInternetConnectionView!
    var customFieldRowIndex:Int!
    var headerType:headerType?
   
   var deleteAtIndex: ((_ index: Int) -> Void)?
   var viewWillBeRemoved: (() -> Void)?
   
   // MARK: - View Life Cycle
    override func awakeFromNib() {
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self
        imageCollectionView.register(UINib(nibName: NIB_NAME.imagePreviewCell, bundle: nil), forCellWithReuseIdentifier: NIB_NAME.imagePreviewCell)
        
        /*------------- Tap Gesture ----------------*/
        let tap = UITapGestureRecognizer(target: self, action: #selector(ImagesPreview.hideTopBottomView))
        tap.numberOfTapsRequired = 1
        self.addGestureRecognizer(tap)
        
        /*------------ Buttons ----------------*/
        self.backButton.setImage(#imageLiteral(resourceName: "iconBackTitleBar").withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        self.deleteButton.setImage(#imageLiteral(resourceName: "iconDeleteRow").withRenderingMode(.alwaysTemplate), for: UIControlState.normal)
        self.deleteButton.tintColor = UIColor.white
        self.backButton.tintColor = UIColor.white

    }
    
    // MARK: - IBAction
    @IBAction func deleteAction(_ sender: AnyObject) {
        if IJReachability.isConnectedToNetwork() == false {
            self.showInternetToast()
        } else {
            delegate?.deleteImageActionFromImages(self.currentIndex, customFieldRowIndex: self.customFieldRowIndex,header: self.headerType!)
         if deleteAtIndex != nil {
            
            UIAlertController.showActionSheetWith(title: "", message: TEXT.deleteImage, options: [TEXT.YES_ACTION], actions: [{ [weak self] in
               self?.deleteAtIndex?(self!.currentIndex)
               }])
         }
         
        }
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        delegate?.dismissImagePreview(customFieldRowIndex: self.customFieldRowIndex)
      removingViewInSelf()
    }
    
    func confirmationDoneForDeletion() {
        imageArray.remove(at: currentIndex)
        if(imageArray.count == 0){
            delegate?.dismissImagePreview(customFieldRowIndex: self.customFieldRowIndex)
         removingViewInSelf()
        } else if(currentIndex >= imageArray.count) {
            self.setCollectionViewWithPage(imageArray.count - 1,customFieldRow: self.customFieldRowIndex,header:headerType!)
        } else {
            self.setCollectionViewWithPage(currentIndex, customFieldRow: self.customFieldRowIndex,header:headerType!)
        }
    }
    
    func setCollectionViewWithPage(_ index:Int, customFieldRow:Int,header:headerType = .AppointmentDetails) {
        self.customFieldRowIndex = customFieldRow
        self.headerType = header
        self.layoutIfNeeded()
        if IJReachability.isConnectedToNetwork() == false {
            self.showInternetToast()
        }
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = headerView.bounds
        headerView.addSubview(blurView)
        headerView.sendSubview(toBack: blurView)
        
        
        let darkBlurForPageNumber = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurViewForPageNumber = UIVisualEffectView(effect: darkBlurForPageNumber)
        blurViewForPageNumber.frame = bottomView.bounds
        bottomView.addSubview(blurViewForPageNumber)
        bottomView.sendSubview(toBack: blurViewForPageNumber)
        
        currentIndex = index
        pageNumber.text = "\(index + 1)/\(imageArray.count)"
        imageCollectionView.reloadData()
        if(index < imageArray.count) {
            imageCollectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: UICollectionViewScrollPosition.right, animated: false)
        }
    }
    
    //MARK: - UICollection View Delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let width:CGFloat = collectionView.frame.width
        let height = collectionView.frame.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NIB_NAME.imagePreviewCell, for: indexPath) as! ImagePreviewCell
        cell.imageView.layer.borderWidth = 0.0
        cell.imageScrollView.delegate = self
        
        cell.imageScrollView.minimumZoomScale = 1.0
        cell.imageScrollView.maximumZoomScale = 10.0
        cell.imageScrollView.zoomScale = 1.0
        
        let zoomTap = UITapGestureRecognizer(target: self, action: #selector(self.setZoom))
        zoomTap.numberOfTapsRequired = 2
        cell.addGestureRecognizer(zoomTap)
      
      let imagePathString = imageArray[indexPath.item] as! String
      let imagePathUrl = URL(string: imagePathString)
      
      cell.imageView.kf.setImage(with: imagePathUrl, placeholder: #imageLiteral(resourceName: "iconImageInactive"))
        
        cell.imageView.contentMode = UIViewContentMode.scaleAspectFit
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.imageCollectionView {
            let index = Int(scrollView.contentOffset.x / scrollView.frame.width)
            currentIndex = index
            pageNumber.text = "\(index + 1)/\(imageArray.count)"
        }
    }
    
   
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        if scale == 1.0 {
            self.imageCollectionView.isScrollEnabled = true
        } else {
            self.imageCollectionView.isScrollEnabled = false
        }
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        self.imageCollectionView.isScrollEnabled = false
    }
    
    /*-------- Dismiss ---------*/
    func hideTopBottomView() {
        if(isTopBottomViewHidden == false) {
            isTopBottomViewHidden = true
            UIApplication.shared.isStatusBarHidden = true
            self.imageCollectionView.backgroundColor = UIColor.black
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.headerView.transform = CGAffineTransform(translationX: 0.0, y: -self.headerView.frame.height)
                self.bottomView.transform = CGAffineTransform(translationX: 0.0, y: self.bottomView.frame.height)
            })
        } else {
            isTopBottomViewHidden = false
             UIApplication.shared.isStatusBarHidden = false
            self.imageCollectionView.backgroundColor = UIColor.white
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.headerView.transform = CGAffineTransform.identity
                self.bottomView.transform = CGAffineTransform.identity
            })
        }
    }
    
    //MARK: NoInternetConnectionView
    func showInternetToast() {
        if(internetToast == nil) {
            internetToast = UINib(nibName: NIB_NAME.noInternetConnectionView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! NoInternetConnectionView
            internetToast.frame = CGRect(x: 0, y:HEIGHT.navigationHeight, width: self.frame.width, height: 0)
            internetToast.delegate = self
            internetToast.showToast()
            self.addSubview(internetToast)
        }
    }
    
    func dismissInternetToast() {
        if(internetToast != nil) {
            self.internetToast.removeFromSuperview()
            self.internetToast = nil
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        guard currentIndex != nil else {
            return self
        }

        if let cell = imageCollectionView.cellForItem(at: NSIndexPath(item: currentIndex, section: 0) as IndexPath) as? ImagePreviewCell {
            if scrollView == cell.imageScrollView {
                return cell.imageView
            }
        }
        return self
    }
    
    func setZoom() {
        guard currentIndex != nil else {
            return
        }
        
        if let cell = imageCollectionView.cellForItem(at: NSIndexPath(item: currentIndex, section: 0) as IndexPath) as? ImagePreviewCell {
            print(cell.imageScrollView.zoomScale)
            UIView.animate(withDuration: 0.3, animations: {
                if cell.imageScrollView.zoomScale == 1.0 {
                    cell.imageScrollView.zoomScale = 2.0
                    self.imageCollectionView.isScrollEnabled = false
                } else {
                    cell.imageScrollView.zoomScale = 1.0
                    self.imageCollectionView.isScrollEnabled = true
                }
            })
            
        }
    }
   
   func removingViewInSelf() {
      if viewWillBeRemoved != nil {
         removeFromSuperview()
         viewWillBeRemoved?()
      }
   }
   
   // MARK: - Type Methods
   class func loadForImagePaths(arrayOfPath: [String], whilePreviewingImageAtIndex index: Int) -> ImagesPreview {
      let imagesPreview = UINib(nibName: NIB_NAME.imagesPreview, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! ImagesPreview
      imagesPreview.frame = UIScreen.main.bounds
      imagesPreview.imageCollectionView.frame = CGRect(x: imagesPreview.imageCollectionView.frame.origin.x, y: imagesPreview.imageCollectionView.frame.origin.y, width: imagesPreview.frame.width, height: imagesPreview.imageCollectionView.frame.height)
      imagesPreview.imageArray = arrayOfPath
      imagesPreview.backgroundColor = UIColor.clear
      imagesPreview.setCollectionViewWithPage(index, customFieldRow: index)
      UIApplication.shared.keyWindow?.rootViewController?.view?.addSubview(imagesPreview)
      
      imagesPreview.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
      UIView.animate(withDuration: 0.35, delay: 0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
         imagesPreview.transform = CGAffineTransform.identity
      }, completion: { finished in
         imagesPreview.backgroundColor = UIColor.white
      })
      
      return imagesPreview
   }

}
