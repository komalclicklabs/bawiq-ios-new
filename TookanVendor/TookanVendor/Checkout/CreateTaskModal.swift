//
//  CreateTaskModal.swift
//  TookanVendor
//
//  Created by Samneet Kharbanda on 06/07/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import Foundation
import UIKit


class CreateTaskModal{
   
   var headerArray = [headerType]()
   var collapsableArray = [headerType]()
   var constantPickUpCells = [CustomFieldTypes]()
   var customPickUpCells = [Any]()
   var constantDeliveryCells = [CustomFieldTypes]()
   var selectedProducts = [Product]()
   var originalSelectedProducts = [Product]()
   var cart : NLevelCart?
   var customDeliveryCells = [Any]()
   var isPickUpAdded = false
   var isDeliveryAdded = false
   var handlingType : ConstantFieldHandlingType?
   var nonReqiuredPickUpFields = [Any]()
   var nonRequiredDeliveryFields = [Any]()
   var customeFieldName = ""
   var pickUpCustomFieldName = ""
   let model = TaskReviewModel()
    
    var onjobCreation : (() -> Void)?
    
   init() {
      
   }
   
   func setUpPickUpDeliveryWorkFlow(){
      clearData()
      //setInitialData()
      Singleton.sharedInstance.resetPickupMetaData()
      Singleton.sharedInstance.resetMetaData()
      selectedProducts = cart?.getSelectedProductsForCust() ?? []
      originalSelectedProducts = cart?.getSelectedProducts() ?? []
      handlingType = Singleton.sharedInstance.formDetailsInfo.getPrefiledData()
    
      switch Singleton.sharedInstance.formDetailsInfo.work_flow {
      case WORKFLOW.pickupDelivery.rawValue?:

         switch Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag {
         case PICKUP_DELIVERY.pickup.rawValue?:
            constantPickUpCells = self.filterArray(array: [.name,.pickUpAddress,.phonenumber,.email])
            customPickUpCells = Singleton.sharedInstance.createTaskDetail.pickupMetadata
            headerArray = [.PickUpDetails,.pickUpCustomeDetails,.Description]
            self.isPickUpAdded = true
            self.collapsableArray.append(.PickUpDetails)
            self.collapsableArray.append(.pickUpCustomeDetails)
            break
         case PICKUP_DELIVERY.delivery.rawValue?:
            constantDeliveryCells = self.filterArray(array:  [.name,.dropAddress,.phonenumber,.email])
            customDeliveryCells = Singleton.sharedInstance.createTaskDetail.metaData
            headerArray = [.DeliveryDetails,.customDeliveryDetails,.Description]
            self.isDeliveryAdded = true
            self.collapsableArray.append(.DeliveryDetails)
            self.collapsableArray.append(.customDeliveryDetails)
            break
         case PICKUP_DELIVERY.both.rawValue?,PICKUP_DELIVERY.addDeliveryDetails.rawValue?:
            constantDeliveryCells = [.name,.dropAddress,.email,.phonenumber,.endDateTime]
            constantPickUpCells = self.filterArray(array: [.name,.pickUpAddress,.phonenumber,.email])
            customDeliveryCells = Singleton.sharedInstance.createTaskDetail.metaData
            customPickUpCells = Singleton.sharedInstance.createTaskDetail.pickupMetadata
            headerArray = [.PickUpDetails,.pickUpCustomeDetails,.DeliveryDetails,.customDeliveryDetails,.Description]
            break
         default:
            break
         }
         break
      case WORKFLOW.appointment.rawValue?, WORKFLOW.fieldWorkforce.rawValue?:
        
        
         self.collapsableArray.append(.AppointmentDetails)
         self.collapsableArray.append(.customDeliveryDetails)
         self.isDeliveryAdded = true
         constantDeliveryCells = self.filterArray(array:  [.name,.dropAddress,.email,.phonenumber,.startDateTime,.endDateTime])
         
         customDeliveryCells = Singleton.sharedInstance.createTaskDetail.metaData
         headerArray = [.AppointmentDetails,.customDeliveryDetails,.Description]
         
         break
      default:
         break
      }
      makeNonRequredCustmFields()
      if shouldCartSectionBeDisplayed() {
         self.headerArray = [headerType.Cart] + headerArray
         //            self.collapsableArray.append(.Cart)
      }
    setprefilType()
       prefillDate()
      
   }
   
   private func shouldCartSectionBeDisplayed() -> Bool {
      return cart != nil
   }
   
   func checkForColapsable(header:headerType) -> Int{
      if collapsableArray.contains(header) == true    {
         switch header {
         case .customDeliveryDetails:
            return customDeliveryCells.count
         case .DeliveryDetails,.AppointmentDetails:
            return constantDeliveryCells.count
         case .Description:
            return 1
         case .pickUpCustomeDetails:
            return customPickUpCells.count
         case .PickUpDetails:
            return constantPickUpCells.count
         case .Cart:
            if selectedProducts.count == 0 {
               return selectedProducts.count
            } else {
               return selectedProducts.count + 1 //subtotal cell
            }
            
         }
      }else{
         return 0
      }
   }
   
   
   func changeColapsableaState(forType:headerType){
      if self.collapsableArray.contains(forType) == true  {
         
         switch forType {
         case .DeliveryDetails:
            collapsableArray = collapsableArray.filter{$0 !=  .DeliveryDetails && $0 !=  .customDeliveryDetails}
            break
            
         case .Description:
            collapsableArray = collapsableArray.filter{$0 !=  .Description}
            break
            
         case .PickUpDetails:
            collapsableArray = collapsableArray.filter{$0 !=  .pickUpCustomeDetails && $0 !=  .PickUpDetails}
            
         case .AppointmentDetails:
            collapsableArray = collapsableArray.filter{$0 !=  .AppointmentDetails && $0 !=  .customDeliveryDetails}
            
         case .Cart:
            collapsableArray = collapsableArray.filter{$0 !=  .Cart}
         default: break
            
         }
      }else{
         switch forType {
         case .DeliveryDetails:
            self.isDeliveryAdded = true
            collapsableArray.append(.DeliveryDetails)
            collapsableArray.append(.customDeliveryDetails)
            break
         case .Description:
            collapsableArray.append(.Description)
            break
         case .PickUpDetails:
            self.isPickUpAdded = true
            collapsableArray.append(.pickUpCustomeDetails)
            collapsableArray.append(.PickUpDetails)
            
         case .AppointmentDetails:
            self.isDeliveryAdded = true
            collapsableArray.append(.AppointmentDetails)
            collapsableArray.append(.customDeliveryDetails)
         case .Cart:
            collapsableArray.append(.Cart)
         default: break
            
         }
      }
   }
   
   
   
   func validatePickUpDetails() -> Bool{
      let data = Singleton.sharedInstance.createTaskDetail
    
    guard data.jobPickupName.length > 0 else {
        ErrorView.showWith(message: ERROR_MESSAGE.INVALID_NAME, isErrorMessage: true, removed: nil)
        return false
    }
      
      guard  Singleton.sharedInstance.validatePhoneNumber(phoneNumber: data.jobPickupPhone) == true else {
         ErrorView.showWith(message: ERROR_MESSAGE.INVALID_PHONE_NUMBER, isErrorMessage: true, removed: nil)
         return false
      }
      
      guard  Singleton.sharedInstance.validateAddress(address: data.jobPickupAddress, latitude: data.jobPickupLatitude, longitude: data.jobPickupLongitude) == true  else {
         ErrorView.showWith(message: ERROR_MESSAGE.INVALID_ADDRESS, isErrorMessage: true, removed: nil)
         return false
      }
      
      if data.jobPickupEmail.length > 0 {
         guard Singleton.sharedInstance.validateEmail(data.jobPickupEmail) == true else {
            ErrorView.showWith(message: ERROR_MESSAGE.INVALID_EMAIL, isErrorMessage: true, removed: nil)
            return false
         }
      }
      
      guard  Singleton.sharedInstance.validateDate(jobDate: data.jobPickupDateTime) == true else {
         ErrorView.showWith(message: ERROR_MESSAGE.INVALID_DATETIME, isErrorMessage: true, removed: nil)
         return false
      }
      
      return checkForMetaData(data: customPickUpCells as! [CustomFieldDetails])
      
   }
   
   
   func checkForMetaData(data:[CustomFieldDetails]) -> Bool{
      
      for customField in data{
         if customField.app_side == .readWrite {
            if customField.required == 1 && customField.data == "" && customField.dataType.isDataTypeHandled() {
               ErrorView.showWith(message: customField.display_name + " Field is mandatory to fill.", isErrorMessage: true, removed: nil)
               return false
            }else if (customField.dataType == CustomFieldTypes.DropDown) && (customField.data == customField.input) && customField.required == 1{
               ErrorView.showWith(message: customField.display_name + " Field is mandatory to fill.", isErrorMessage: true, removed: nil)
               return false
            }
            
            switch customField.dataType {
            case .url:
               if (customField.data?.length)! > 0 {
                  guard Singleton.sharedInstance.verifyUrl(customField.data!) == true else {
                     ErrorView.showWith(message: ERROR_MESSAGE.PLEASE_ENTER_VALID + customField.display_name, isErrorMessage: true, removed: nil)
                     return false
                  }
               }
               break
            case .phonenumber:
               if (customField.data?.length)! > 0 {
                  guard Singleton.sharedInstance.validatePhoneNumber(phoneNumber: customField.data!) == true else {
                     ErrorView.showWith(message:  ERROR_MESSAGE.PLEASE_ENTER_VALID + customField.display_name, isErrorMessage: true, removed: nil)
                     return false
                  }
               }
               
            case .email:
               if (customField.data?.length)! > 0 {
                  guard Singleton.sharedInstance.validateEmail(customField.data!) == true else {
                     ErrorView.showWith(message: ERROR_MESSAGE.PLEASE_ENTER_VALID + customField.display_name, isErrorMessage: true, removed: nil)
                     return false
                  }
               }
               break
            default:
               break
            }
         }
      }
      return true
   }
   
   
   
   func validateDeliveryDetails() -> Bool{
      let data = Singleton.sharedInstance.createTaskDetail
      
      
      guard  Singleton.sharedInstance.validatePhoneNumber(phoneNumber: data.customerPhone) == true else {
         ErrorView.showWith(message: ERROR_MESSAGE.INVALID_PHONE_NUMBER, isErrorMessage: true, removed: nil)
         return false
      }
      
      guard  Singleton.sharedInstance.validateAddress(address: data.customerAddress, latitude: data.latitude, longitude: data.longitude) == true  else {
         ErrorView.showWith(message: ERROR_MESSAGE.INVALID_ADDRESS, isErrorMessage: true, removed: nil)
         return false
      }
      
      if data.customerEmail.length > 0 {
         guard Singleton.sharedInstance.validateEmail(data.customerEmail) == true else {
            ErrorView.showWith(message: ERROR_MESSAGE.INVALID_EMAIL, isErrorMessage: true, removed: nil)
            return false
         }
      }
      
      guard  Singleton.sharedInstance.validateDate(jobDate: data.jobDeliveryDatetime) == true else {
         if self.headerArray.contains(.AppointmentDetails) == true{
            ErrorView.showWith(message: ERROR_MESSAGE.invalidEnd, isErrorMessage: true, removed: nil)
         }else{
            ErrorView.showWith(message: ERROR_MESSAGE.INVALID_DATETIME, isErrorMessage: true, removed: nil)
         }
         return false
      }
      if self.headerArray.contains(.AppointmentDetails) == true{
         guard  Singleton.sharedInstance.validateDate(jobDate: data.jobPickupDateTime) == true else {
            ErrorView.showWith(message: ERROR_MESSAGE.invalidStart, isErrorMessage: true, removed: nil)
            return false
         }
      }
      
      return checkForMetaData(data: customDeliveryCells as! [CustomFieldDetails])
      
   }
   
   
   func getHeading(_ ForType: headerType) -> (headerTitle:String,HeaderImage:UIImage,isAdded:Bool){
      
      switch ForType {
      case .AppointmentDetails:
         return ( TEXT.AppointmentDetails,placeholdeImage!,isDeliveryAdded)
      case .DeliveryDetails :
         return (TEXT.DeliveryDetails,placeholdeImage!,isDeliveryAdded)
      case .PickUpDetails:
         return (TEXT.customerDetails,pickupHeading!,isPickUpAdded)
      case .Description :
         return (TEXT.Notes,taskDescription!,false)
      case .Cart:
         return (TEXT.Cart,cartHeading!.renderWithAlwaysTemplateMode(),false)
      default:
         return (TEXT.error,placeholdeImage!,false)
      }
   }
   
   func getStaticFieldData(headerType:headerType,index:Int) -> (type:CustomFieldTypes,initialData:String,isPickUp:Bool,LabelName:String){
      let dataArray = getStaticArray(forType: headerType)
      let creatTaskData = Singleton.sharedInstance.createTaskDetail
      switch headerType {
      case .AppointmentDetails,.DeliveryDetails:
         switch dataArray[index] {
         case .name:
            return (.name,creatTaskData.customerUsername,false, TEXT.NAME)
         case .email:
            return (.email,creatTaskData.customerEmail,false, TEXT.Email)
         case .dropAddress:
            return (.dropAddress,creatTaskData.customerAddress,false, TEXT.ADDRESS_PLACEHODLER)
         case .startDateTime:
            return (.startDateTime,creatTaskData.jobPickupDateTime,false, TEXT.StartTime)
         case .endDateTime:
            return (.endDateTime,creatTaskData.jobDeliveryDatetime,false, (headerType == .DeliveryDetails ? TEXT.DeliveryTime : TEXT.EndTime))
         case .phonenumber:
            return (.phonenumber,creatTaskData.customerPhone,false, TEXT.PhoneNumber)
         default:
            return (.phonenumber,creatTaskData.customerPhone,false, (headerType == .DeliveryDetails ? "error" : "error"))
         }
      case .PickUpDetails:
         switch dataArray[index] {
         case .name:
            return (.name,creatTaskData.jobPickupName,true,TEXT.NAME)
         case .email:
            return (.email,creatTaskData.jobPickupEmail,true, TEXT.Email)
         case .pickUpAddress:
            return (.pickUpAddress,creatTaskData.jobPickupAddress,true, TEXT.ADDRESS_PLACEHODLER)
         case .startDateTime:
            return (.startDateTime,creatTaskData.jobPickupDateTime,true, TEXT.PickUpTime)
         case .phonenumber:
            return (.phonenumber,creatTaskData.jobPickupPhone,true, TEXT.PhoneNumber)
         default:
            return (.phonenumber,creatTaskData.customerPhone,true, (headerType == .DeliveryDetails ? "error" : "error"))
         }
         
         
      default:
         return (.phonenumber,creatTaskData.customerPhone,true, (headerType == .DeliveryDetails ? "error" : "error"))
         
      }
      
      
      
      
      
   }
   
   
   func getStaticArray(forType:headerType)->[CustomFieldTypes]{
      switch forType {
      case .AppointmentDetails,.DeliveryDetails:
         return constantDeliveryCells
      case .PickUpDetails:
         return constantPickUpCells
      default:
         return []
         
      }
   }
   
   func resetAndRemoveData(forType:headerType,isRemove:Bool){
      switch forType {
      case .Description:
         Singleton.sharedInstance.createTaskDetail.jobDescription = ""
      case .AppointmentDetails,.DeliveryDetails:
         Singleton.sharedInstance.resetDeliveryDetails()
         self.customDeliveryCells = Singleton.sharedInstance.createTaskDetail.metaData
         isRemove == true ? (self.isDeliveryAdded = false) : (self.isDeliveryAdded = true)
      case .PickUpDetails:
         Singleton.sharedInstance.resetPickupDetails()
         self.customPickUpCells = Singleton.sharedInstance.createTaskDetail.pickupMetadata
         isRemove == true ? (self.isPickUpAdded = false) : (self.isPickUpAdded = true)
      default:
         print("")
      }
      
      if self.collapsableArray.contains(forType) == true{
         changeColapsableaState(forType: forType)
      }
   }
   
   func toShowRightButton() -> Bool{
      if (self.headerArray.contains(.DeliveryDetails) == true &&  self.headerArray.contains(.PickUpDetails) == false) || (self.headerArray.contains(.DeliveryDetails) == false &&  self.headerArray.contains(.PickUpDetails) == true) || (self.headerArray.contains(.AppointmentDetails) == true  ){
         return true
      }else{
         return false
      }
   }
   
   
   func saveAndReviewButtonTapped() -> Bool{
    if NLevelFlowManager.subTotal < Singleton.sharedInstance.formDetailsInfo.minimumOrderValue {
        ErrorView.showWith(message: "Minimum order amount is \(Singleton.sharedInstance.formDetailsInfo.currencyid) \(Singleton.sharedInstance.formDetailsInfo.minimumOrderValue)", removed: nil)
        return false
    }
    
    
    
      if self.isPickUpAdded == true{
         Singleton.sharedInstance.createTaskDetail.hasPickup = 1
         Singleton.sharedInstance.createTaskDetail.pickupMetadata = self.customPickUpCells
         if self.headerArray.contains(.DeliveryDetails) == true && self.headerArray.contains(.PickUpDetails) == true{
            Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag = PICKUP_DELIVERY.both.rawValue
         }else{
            Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag = PICKUP_DELIVERY.pickup.rawValue
         }
         
         
         if validatePickUpDetails() == false{
            return false
         }
      }else{
         Singleton.sharedInstance.createTaskDetail.hasPickup = 0
      }
      
      if self.isDeliveryAdded == true {
         Singleton.sharedInstance.createTaskDetail.hasDelivery = 1
         Singleton.sharedInstance.createTaskDetail.metaData = self.customDeliveryCells
         
         if self.headerArray.contains(.DeliveryDetails) == true && self.headerArray.contains(.PickUpDetails) == true{
            Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag = PICKUP_DELIVERY.both.rawValue
         }else{
            
            Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag = PICKUP_DELIVERY.delivery.rawValue
            
         }
         
         if validateDeliveryDetails() == false{
            return false
         }
      }else{
         Singleton.sharedInstance.createTaskDetail.hasDelivery = 0
      }
      
      if self.isDeliveryAdded == false && self.isPickUpAdded == false{
         ErrorView.showWith(message: "Please add above information", isErrorMessage: true, removed: nil)
         return false
      }
      
      if self.isDeliveryAdded == true && self.isPickUpAdded == true{
        Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag = PICKUP_DELIVERY.addDeliveryDetails.rawValue
        if Singleton.sharedInstance.comparePickupAndDeliveryDate(pickupDate: Singleton.sharedInstance.createTaskDetail.jobPickupDateTime, deliveryDate: Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime,greaterMessage:"Pick up time should be before delivery time",bufferMessage:"There should be buffer of atleast 15 mins between Pick up time and Delivery time.") ==  false{
          //  ErrorView.showWith(message: "Pick up time should be before delivery time", isErrorMessage: true, removed: nil)
            return false
         }
      }else if self.headerArray.contains(.AppointmentDetails) == true && self.isDeliveryAdded == true{
         if Singleton.sharedInstance.comparePickupAndDeliveryDate(pickupDate: Singleton.sharedInstance.createTaskDetail.jobPickupDateTime, deliveryDate: Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime) ==  false{
          //  ErrorView.showWith(message: "Start time should be before end time", isErrorMessage: true, removed: nil)
            return false
         }
      }
    if headerArray.contains(headerType.PickUpDetails){
        if self.checkIfImagesAreUploaded(metaData: self.customPickUpCells as! [CustomFieldDetails], isPickUp: true) == false{
            return false
        }
    }
    
    if headerArray.contains(.AppointmentDetails) == true  || headerArray.contains(.DeliveryDetails) == true  {
        if self.checkIfImagesAreUploaded(metaData: self.customDeliveryCells as! [CustomFieldDetails], isPickUp: false) == false{
            return false
        }
    }
    
    if checkForForcedPickUpDelivery() == false  {
        return false    
    }
    
    
      return true
   }
   
   func makeNonRequredCustmFields(){
      //        self.nonReqiuredPickUpFields = self.customPickUpCells.filter{($0 as! CustomFieldDetails).required == 0}
      //        customPickUpCells = self.customPickUpCells.filter{($0 as! CustomFieldDetails).required == 1}
      //        self.nonRequiredDeliveryFields = self.customDeliveryCells.filter{($0 as! CustomFieldDetails).required == 0}
      //         customDeliveryCells = self.customDeliveryCells.filter{($0 as! CustomFieldDetails).required == 1}
   }
   
   
   func clearData(){
      Singleton.sharedInstance.resetPickupDetails()
      Singleton.sharedInstance.resetDeliveryDetails()
      Singleton.sharedInstance.createTaskDetail.jobDescription = ""
   }
   
   func setInitialData(){
    
    let dateArray = "\(Date().addingTimeInterval(60*30))".components(separatedBy: " ")
    let dateString = ("\(dateArray[0]) " + "\(dateArray[1])").convertDateFromUTCtoLocal(input: "yyyy-MM-dd HH:mm:ss", output: "yyyy-MM-dd HH:mm:ss")
    let dateArrayEnd = "\(Date().addingTimeInterval(60*60))".components(separatedBy: " ")
    let dateStringEnd = ("\(dateArrayEnd[0]) " + "\(dateArrayEnd[1])").convertDateFromUTCtoLocal(input: "yyyy-MM-dd HH:mm:ss", output: "yyyy-MM-dd HH:mm:ss")
    
    if headerArray.contains(.PickUpDetails ) == true    {
        Singleton.sharedInstance.createTaskDetail.jobPickupPhone = Vendor.current!.phoneNo
    Singleton.sharedInstance.createTaskDetail.jobPickupName  = Vendor.current!.firstName!
    Singleton.sharedInstance.createTaskDetail.jobPickupEmail  = Vendor.current!.email!
     Singleton.sharedInstance.createTaskDetail.jobPickupDateTime = dateString
    }else{
        if headerArray.contains(.AppointmentDetails) == true{
            Singleton.sharedInstance.createTaskDetail.jobPickupDateTime = dateString
        }
        Singleton.sharedInstance.createTaskDetail.customerPhone = Vendor.current!.phoneNo
    Singleton.sharedInstance.createTaskDetail.customerUsername = Vendor.current!.firstName!
    Singleton.sharedInstance.createTaskDetail.customerEmail = Vendor.current!.email!
    Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime = dateStringEnd
    }
   }
    
    
    
    func prefillDate(){
        let dateArray = "\(Date().addingTimeInterval(60*30))".components(separatedBy: " ")
        let dateString = ("\(dateArray[0]) " + "\(dateArray[1])").convertDateFromUTCtoLocal(input: "yyyy-MM-dd HH:mm:ss", output: "yyyy-MM-dd HH:mm:ss")
        let dateArrayEnd = "\(Date().addingTimeInterval(60*60))".components(separatedBy: " ")
        let dateStringEnd = ("\(dateArrayEnd[0]) " + "\(dateArrayEnd[1])").convertDateFromUTCtoLocal(input: "yyyy-MM-dd HH:mm:ss", output: "yyyy-MM-dd HH:mm:ss")
        
        if headerArray.contains(.PickUpDetails ) == true    {
            Singleton.sharedInstance.createTaskDetail.jobPickupDateTime = dateString
        }else{
            if headerArray.contains(.AppointmentDetails) == true{
                Singleton.sharedInstance.createTaskDetail.jobPickupDateTime = dateString
            }
            Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime = dateStringEnd
        }
        
    }
   
   
   func getmetaData(forHeader:headerType,index:Int) -> CustomFieldDetails{
      switch forHeader {
      case .AppointmentDetails,.DeliveryDetails,.customDeliveryDetails:
         return self.customDeliveryCells[index] as! CustomFieldDetails
      default:
         return self.customPickUpCells[index] as! CustomFieldDetails
      }
   }
   
   func setprefilType(){
      switch handlingType! {
      case .hidePrefiled,.showPrefiled:
         setInitialData()
      default:
         print("empty")
      }
   }
   
   func getHeightForConstantFields(ForSection Header: headerType,type :CustomFieldTypes) -> CGFloat{
      switch self.handlingType! {
      case .empty,.showPrefiled:
         return getHeightFor(type: type)
      case .hidePrefiled:
         if Header == .PickUpDetails {
            if [CustomFieldTypes.email,.phonenumber,.name].contains(type) == true{
               return 0
            }else{
               return getHeightFor(type: type)
            }
         }else if ([headerType.AppointmentDetails,.DeliveryDetails].contains(Header) == true ) && ([WORKFLOW.appointment.rawValue,WORKFLOW.fieldWorkforce.rawValue].contains(Singleton.sharedInstance.formDetailsInfo.work_flow!) == true){
            
            if [CustomFieldTypes.email,.phonenumber,.name].contains(type) == true{
               return 0
            }else{
               return getHeightFor(type: type)
            }
            
         }else if (Header == .DeliveryDetails)&&([WORKFLOW.pickupDelivery.rawValue].contains(Singleton.sharedInstance.formDetailsInfo.work_flow!) == true)&&(Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag == PICKUP_DELIVERY.delivery.rawValue){
            if [CustomFieldTypes.email,.phonenumber,.name].contains(type) == true{
               return 0
            }else{
               return getHeightFor(type: type)
            }
         }
      }
      return getHeightFor(type: type)
   }
   
   private func getHeightFor(type:CustomFieldTypes) -> CGFloat{
      switch type {
      case .endDateTime,.startDateTime,.email,.name,.phonenumber:
         return 57
      default:
         return UITableViewAutomaticDimension
      }
   }
   
   
   func filterArray(array:[CustomFieldTypes]) -> [CustomFieldTypes]{
      var arrayToReturn = array
      switch self.handlingType! {
      case .empty,.showPrefiled:
         return arrayToReturn
      case .hidePrefiled:
         arrayToReturn = arrayToReturn.filter{[.startDateTime,.endDateTime,.dropAddress,.pickUpAddress].contains($0) == true}
         return arrayToReturn
      }
      return  arrayToReturn
   }
   
   func getSelectedLocation() -> (latitude: String, longitude: String) {
      
      let createTaskdetail = Singleton.sharedInstance.createTaskDetail
      
      let deliveryCoordinates = (createTaskdetail.latitude, createTaskdetail.longitude)
      let pickupCoordinates = (createTaskdetail.jobPickupLatitude, createTaskdetail.jobPickupLongitude)
      
      switch Singleton.sharedInstance.formDetailsInfo.work_flow {
      case WORKFLOW.pickupDelivery.rawValue?:
         
         switch Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag {
         case PICKUP_DELIVERY.delivery.rawValue?:
            return deliveryCoordinates
         default:
            return pickupCoordinates
         }
         
      case WORKFLOW.appointment.rawValue?, WORKFLOW.fieldWorkforce.rawValue?:
         return deliveryCoordinates
      default:
         break
      }
      
      return deliveryCoordinates
   }
    
    
   func checkIfImagesAreUploaded(metaData : [CustomFieldDetails],isPickUp : Bool) -> Bool {
      
      for i in 0..<metaData.count {
         //customField = Singleton.sharedInstance.getMetadataForTask(customFieldType:i)
         let customField = metaData[i]
         if isPickUp == true{
            self.pickUpCustomFieldName = customField.template_id!
         }else{
            self.customeFieldName = customField.template_id!
         }
         switch customField.dataType {
         case .image:
            for imageStatus in customField.imageUploadingStatus {
               guard imageStatus == true else {
                  ErrorView.showWith(message: ERROR_MESSAGE.IMAGE_UPLOADING, isErrorMessage: true, removed: nil)
                  return false
               }
            }
            
         default:
            //return true
            break
            
         }
      }
      
      return true
   }
    
    
    
    func checkForForcedPickUpDelivery() -> Bool{
         if !((self.isPickUpAdded == true && self.isDeliveryAdded == true)) && Singleton.sharedInstance.formDetailsInfo.forcePickupDelivery == 1 && Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag == PICKUP_DELIVERY.both.rawValue{
            
            ErrorView.showWith(message: TEXT.FORCE_PICKUP_DELIVERY_MESSAGE, isErrorMessage: true, removed: nil)
            return false
        }
        return true
    }
    
    
    func creatMetaDataAndPickUpMetaData() -> (pickUpmetaData:[Any],metaData:[Any]){
        let metaData = self.model.createMetaData(forCustom: "subtotal", value: "\(NLevelFlowManager.subTotal)")
        let pickupMetaData = self.model.createPickupMetaData(forCustom: "subtotal", value: "\(NLevelFlowManager.subTotal)")
    
    return (pickupMetaData,metaData)
    
    }
    
    func getHasPickUpAndHasDelivery() -> (hasPickUp:String,hasDelivery:String){
        
        var hasPickUp = String()
        var hasDelivery = String()
        
        switch Singleton.sharedInstance.formDetailsInfo.work_flow! {
        case WORKFLOW.appointment.rawValue, WORKFLOW.fieldWorkforce.rawValue:
            Singleton.sharedInstance.createTaskDetail.jobPickupAddress = ""
            Singleton.sharedInstance.createTaskDetail.jobPickupLatitude = ""
            Singleton.sharedInstance.createTaskDetail.jobPickupLongitude = ""
            self.pickUpCustomFieldName = ""
            hasPickUp = "0"
            hasDelivery = "0"
            break
        default:
               hasDelivery = self.isDeliveryAdded == true ? "1" :  "0"
                hasPickUp = self.isPickUpAdded == true ? "1" : "0"
                
               if hasPickUp == "1" && hasDelivery == "0" {
                
                Singleton.sharedInstance.createTaskDetail.customerAddress = ""
                Singleton.sharedInstance.createTaskDetail.latitude = ""
                Singleton.sharedInstance.createTaskDetail.longitude = ""
                Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime = ""
                self.customeFieldName = ""
                
               } else if hasPickUp == "0" && hasDelivery == "1" {
                
                Singleton.sharedInstance.createTaskDetail.jobPickupAddress = ""
                Singleton.sharedInstance.createTaskDetail.jobPickupLatitude = ""
                Singleton.sharedInstance.createTaskDetail.jobPickupLongitude = ""
                Singleton.sharedInstance.createTaskDetail.jobPickupDateTime = ""
                self.pickUpCustomFieldName = ""
                
            }
        
        
        
        }
        
        return (hasPickUp,hasDelivery)
        
        
    }
    
    func getElementWithId(id:String) -> Product?{
        for i in self.originalSelectedProducts{
            if i.id == id{
                return i
            }
        }
        return nil
    }
    

    
    func getPaymentForDemoCase(owner:UIViewController) {
        
        let hasPickOrDeliveryData = getHasPickUpAndHasDelivery()
        let metaDataArrays = creatMetaDataAndPickUpMetaData()
        ActivityIndicator.sharedInstance.showActivityIndicator()
        APIManager.sharedInstance.getPaymentForDemo { [weak self] (isSuccess, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if isSuccess == true {
                
                if Singleton.sharedInstance.formDetailsInfo.toShowPayment == false{
                    if let vc = UIStoryboard(name: STORYBOARD_NAME.demo, bundle: Bundle.main).instantiateViewController(withIdentifier: STORYBOARD_ID.orderSuccessfullVC) as? OrderSuccessfullVC {
                        owner.navigationController?.pushViewController(vc, animated: true)
                    }
                } else {
                    let currencyData =  self?.parsepaymentData(json:response)
                    
                    var vc : NewPaymentScreenViewController!
                    vc = UIStoryboard(name: STORYBOARD_NAME.afterLogin, bundle: Bundle.main).instantiateViewController(withIdentifier: STORYBOARD_ID.payment) as! NewPaymentScreenViewController
                    vc?.amount = "\(currencyData!.curruncyObject.symbol) \(currencyData!.totalCost)"
                    vc?.initialAmmount = "\(currencyData!.totalCost)"
                    
                    
                    vc?.afterSelecting = {(id,type,tip) in
                        ActivityIndicator.sharedInstance.showActivityIndicator()
                        NetworkingHelper.sharedInstance.createTaskHit(autoAssignment: Singleton.sharedInstance.formDetailsInfo.auto_assign!,
                                                                      customerAddress: Singleton.sharedInstance.createTaskDetail.customerAddress,
                                                                      customerEmail: Singleton.sharedInstance.createTaskDetail.customerEmail,
                                                                      customerPhone: Singleton.sharedInstance.createTaskDetail.customerPhone,
                                                                      customerUsername: Singleton.sharedInstance.createTaskDetail.customerUsername,
                                                                      latitude: Singleton.sharedInstance.createTaskDetail.latitude,
                                                                      longitude: Singleton.sharedInstance.createTaskDetail.longitude,
                                                                      jobDeliveryDatetime: Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime,
                                                                      hasDelivery: "\(hasPickOrDeliveryData.hasDelivery)",
                            hasPickup: "\(hasPickOrDeliveryData.hasPickUp)",
                            jobDescription: Singleton.sharedInstance.createTaskDetail.jobDescription,
                            jobPickupAddress: Singleton.sharedInstance.createTaskDetail.jobPickupAddress,
                            jobPickupEmail: Singleton.sharedInstance.createTaskDetail.jobPickupEmail,
                            jobPickupPhone: Singleton.sharedInstance.createTaskDetail.jobPickupPhone,
                            jobPickupDateTime: Singleton.sharedInstance.createTaskDetail.jobPickupDateTime,
                            jobPickupLatitude: Singleton.sharedInstance.createTaskDetail.jobPickupLatitude,
                            jobPickupLongitude: Singleton.sharedInstance.createTaskDetail.jobPickupLongitude,
                            jobPickupName: Singleton.sharedInstance.createTaskDetail.jobPickupName,
                            domainName: Singleton.sharedInstance.formDetailsInfo.domain_name!,
                            layoutType: Singleton.sharedInstance.formDetailsInfo.work_flow!,
                            vendorId: Vendor.current == nil ? 0 : Vendor.current!.id!,
                            metadata:metaDataArrays.metaData,
                            pickupMetadata:metaDataArrays.pickUpmetaData,
                            pickupCustomFieldTemplate:(self?.pickUpCustomFieldName)!,
                            customFieldTemplate:(self?.customeFieldName)!,
                            withPayment:true,ammount:"\(currencyData!.totalCost)",currency:currencyData!.curruncyObject.id,payment_method:type,card_id:id,tipAmount:tip,isDemo:Vendor.current == nil,productsJson:(self?.getProductsjson())!)
                        {[weak self] (isSuccess, response) in
                            
                            DispatchQueue.main.async {
                                ActivityIndicator.sharedInstance.hideActivityIndicator()
                            }
                            
                            print(response)
                            //                             Singleton.sharedInstance.createTaskDetail.responseOfGetPayment = response
                            if isSuccess == true{
                                self?.cart?.clearCart()
                                ErrorView.showWith(message: response["message"] as! String, isErrorMessage: false, removed: nil)
                                if Merchant.shared?.completionHandler == nil{
                                    let navController = owner.navigationController
                                    owner.navigationController?.popToRootViewController(animated: false)
                                    if let vc = UIStoryboard(name: STORYBOARD_NAME.demo, bundle: Bundle.main).instantiateViewController(withIdentifier: STORYBOARD_ID.orderSuccessfullVC) as? OrderSuccessfullVC {
                                        
                                        Singleton.sharedInstance.createTaskDetail.paramForDemoTask = Singleton.sharedInstance.createTaskDetail.createTaskParamsToSend
                                        Singleton.sharedInstance.pendingTaskFromDemo = true
                                        navController?.pushViewController(vc, animated: false)
                                    }
                                    // owner.navigationController?.popToRootViewController(animated: true)
                                }else{
                                    Merchant.shared?.completionHandler!(Singleton.sharedInstance.createTaskDetail.createTaskParamsToSend!)
                                }
                                //self?.afterTaskCreation()
                                //self.didRecieveResponseAfterTaskCreation(response:response)
                            }else{
                                _ = owner.navigationController?.popViewController(animated: true)
                                ErrorView.showWith(message: response["message"] as! String, isErrorMessage: true, removed: nil)
                            }
                            
                            
                        }
                    }
                    
                    
                    
                    
                    
                    
                    
                    owner.navigationController?.pushViewController(vc!, animated: true)
                }
                
            }else{
                ErrorView.showWith(message:  response["message"] as! String, isErrorMessage: true, removed: nil)
            }
            
        }
    }
    
    
    
    func apiHitToCreatTask(owner:UIViewController) {
//        guard Vendor.current != nil else {
//            self.getPaymentForDemoCase(owner:owner)
//            return
//        }
        
        
        print("the main data")
        print(getProductsjson())
        print("the end of main data")
        
        
        
        let hasPickOrDeliveryData = getHasPickUpAndHasDelivery()
        let metaDataArrays = creatMetaDataAndPickUpMetaData()
        
        if Singleton.sharedInstance.formDetailsInfo.toShowPayment == false  {
              ActivityIndicator.sharedInstance.showActivityIndicator()
         
            //When payment is switched off
            
            NetworkingHelper.sharedInstance.checkoutHit(metaData: metaDataArrays.metaData, pickUpMetaData: metaDataArrays.pickUpmetaData, pickUpMetaDataName: self.pickUpCustomFieldName, metaDataTemplateName: self.customeFieldName, hasPickup: hasPickOrDeliveryData.hasPickUp, hasDelivery: hasPickOrDeliveryData.hasDelivery, {[weak self] (isSuccess, Data) in
                  ActivityIndicator.sharedInstance.hideActivityIndicator()
                if isSuccess == true {
                    self?.cart?.clearCart()
                    //self.didRecieveResponseAfterTaskCreation(response:response)
                    self?.afterTaskCreation()
                    ErrorView.showWith(message: Data["message"] as! String, isErrorMessage: false, removed: nil)
                    
                    if Vendor.current == nil{
                        let navController = owner.navigationController
                        owner.navigationController?.popToRootViewController(animated: false)
                        if let vc = UIStoryboard(name: STORYBOARD_NAME.demo, bundle: Bundle.main).instantiateViewController(withIdentifier: STORYBOARD_ID.orderSuccessfullVC) as? OrderSuccessfullVC {
                            
                            Singleton.sharedInstance.createTaskDetail.paramForDemoTask = Singleton.sharedInstance.createTaskDetail.createTaskParamsToSend
                            Singleton.sharedInstance.pendingTaskFromDemo = true
                            navController?.pushViewController(vc, animated: false)
                        }
                        // owner.navigationController?.popToRootViewController(animated: true)
                    }else{
                        let firstVc = owner.navigationController?.viewControllers[1]
                        owner.navigationController?.popToViewController(firstVc!, animated: true)
                    }
                    //s
                    
                }else{
                    ErrorView.showWith(message:  Data["message"] as! String, isErrorMessage: true, removed: nil)
                }
            })
        }else{
            ActivityIndicator.sharedInstance.showActivityIndicator()
            NetworkingHelper.sharedInstance.getPayment(metaData: metaDataArrays.metaData, pickUpMetaData: metaDataArrays.pickUpmetaData, pickUpMetaDataName: self.pickUpCustomFieldName, metaDataTemplateName: self.customeFieldName, hasPickup: hasPickOrDeliveryData.hasPickUp, hasDelivery: hasPickOrDeliveryData.hasDelivery, { [weak self](isSuccess, response) in
                ActivityIndicator.sharedInstance.hideActivityIndicator()
               Singleton.sharedInstance.createTaskDetail.responseOfGetPayment = response
                if isSuccess == true {
                  
                    //self.didRecieveResponseAfterTaskCreation(response:response)
                    let currencyData =  self?.parsepaymentData(json:response)
                    
                    var vc : NewPaymentScreenViewController!
                    vc = UIStoryboard(name: STORYBOARD_NAME.afterLogin, bundle: frameworkBundle).instantiateViewController(withIdentifier: STORYBOARD_ID.payment) as! NewPaymentScreenViewController
                    vc?.amount = "\(currencyData!.curruncyObject.symbol) \(currencyData!.totalCost)"
                    vc?.initialAmmount = "\(currencyData!.totalCost)"
 
                    vc?.afterSelecting = {(id,type,tip) in
                         ActivityIndicator.sharedInstance.showActivityIndicator()
                        NetworkingHelper.sharedInstance.createTaskHit(autoAssignment: Singleton.sharedInstance.formDetailsInfo.auto_assign!,
                                                                customerAddress: Singleton.sharedInstance.createTaskDetail.customerAddress,
                                                                customerEmail: Singleton.sharedInstance.createTaskDetail.customerEmail,
                                                                customerPhone: Singleton.sharedInstance.createTaskDetail.customerPhone,
                                                                customerUsername: Singleton.sharedInstance.createTaskDetail.customerUsername,
                                                                latitude: Singleton.sharedInstance.createTaskDetail.latitude,
                                                                longitude: Singleton.sharedInstance.createTaskDetail.longitude,
                                                                jobDeliveryDatetime: Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime,
                                                                hasDelivery: "\(hasPickOrDeliveryData.hasDelivery)",
                                                                hasPickup: "\(hasPickOrDeliveryData.hasPickUp)",
                                                                jobDescription: Singleton.sharedInstance.createTaskDetail.jobDescription,
                                                                jobPickupAddress: Singleton.sharedInstance.createTaskDetail.jobPickupAddress,
                                                                jobPickupEmail: Singleton.sharedInstance.createTaskDetail.jobPickupEmail,
                                                                jobPickupPhone: Singleton.sharedInstance.createTaskDetail.jobPickupPhone,
                                                                jobPickupDateTime: Singleton.sharedInstance.createTaskDetail.jobPickupDateTime,
                                                                jobPickupLatitude: Singleton.sharedInstance.createTaskDetail.jobPickupLatitude,
                                                                jobPickupLongitude: Singleton.sharedInstance.createTaskDetail.jobPickupLongitude,
                                                                jobPickupName: Singleton.sharedInstance.createTaskDetail.jobPickupName,
                                                                domainName: Singleton.sharedInstance.formDetailsInfo.domain_name!,
                                                                layoutType: Singleton.sharedInstance.formDetailsInfo.work_flow!,
                                                                vendorId: Vendor.current == nil ? 0 : Vendor.current!.id!,
                                                                metadata:metaDataArrays.metaData,
                                                                pickupMetadata:metaDataArrays.pickUpmetaData,
                                                                pickupCustomFieldTemplate:(self?.pickUpCustomFieldName)!,
                                                                customFieldTemplate:(self?.customeFieldName)!,
                                                                withPayment:true,ammount:"\(currencyData!.totalCost)",currency:currencyData!.curruncyObject.id,payment_method:type,card_id:id,tipAmount:tip,isDemo:Vendor.current == nil,productsJson:(self?.getProductsjson())!)
                        {[weak self] (isSuccess, response) in
                            
                            DispatchQueue.main.async {
                                ActivityIndicator.sharedInstance.hideActivityIndicator()
                            }
                            
                            print(response)
//                             Singleton.sharedInstance.createTaskDetail.responseOfGetPayment = response
                            if isSuccess == true{
                              self?.cart?.clearCart()
                                 ErrorView.showWith(message: response["message"] as! String, isErrorMessage: false, removed: nil)
                                if Vendor.current == nil{
                                    let navController = owner.navigationController
                                    owner.navigationController?.popToRootViewController(animated: false)
                                    if let vc = UIStoryboard(name: STORYBOARD_NAME.demo, bundle: Bundle.main).instantiateViewController(withIdentifier: STORYBOARD_ID.orderSuccessfullVC) as? OrderSuccessfullVC {
                                        
                                        Singleton.sharedInstance.createTaskDetail.paramForDemoTask = Singleton.sharedInstance.createTaskDetail.createTaskParamsToSend
                                        Singleton.sharedInstance.pendingTaskFromDemo = true
                                        navController?.pushViewController(vc, animated: false)
                                    }
                                    // owner.navigationController?.popToRootViewController(animated: true)
                                }else{
                                    let firstVc = owner.navigationController?.viewControllers[1]
                                    owner.navigationController?.popToViewController(firstVc!, animated: true)
                                }
                               //self?.afterTaskCreation()
                                //self.didRecieveResponseAfterTaskCreation(response:response)
                            }else{
                                _ = owner.navigationController?.popViewController(animated: true)
                                ErrorView.showWith(message: response["message"] as! String, isErrorMessage: true, removed: nil)
                            }
                            
                            
                        }
                    }
                    //owner.navigationController?.popViewController(animated: false)
                    owner.navigationController?.pushViewController(vc!, animated: true)
                  
                }else{
                    ErrorView.showWith(message:  response["message"] as! String, isErrorMessage: true, removed: nil)
                }
            })
        }
        }
    
   
    
    func afterTaskCreation(){
        Singleton.sharedInstance.createTaskDetail = CreateTaskDetails(json: [:])
        Singleton.sharedInstance.resetMetaData()
        Singleton.sharedInstance.resetPickupMetaData()
        self.collapsableArray.removeAll()
        self.isPickUpAdded = false
        self.isDeliveryAdded = false
        self.setUpPickUpDeliveryWorkFlow()
        if Singleton.sharedInstance.formDetailsInfo.isNLevel {
            Singleton.sharedInstance.formDetailsInfo.nLevelController?.deleteTask()
        }
        //self.onjobCreation!()
        
        
        
    }
    
    func getProductsjson() -> [[String:Any]]{
        var dataToSent = [[String:Any]]()
        for i in self.originalSelectedProducts{
            dataToSent.append(contentsOf: i.getJsonFormatOfSelectedProduct())
         print(i.getJsonFormatOfSelectedProduct())
        }
        return dataToSent
    }
    
    
    func parsepaymentData(json:[String:Any]) -> (curruncyObject : Currency,totalCost : String){
        
        if let data = json["data"] as? [String:Any]{
            if let value = data["currency"] as? [String:Any]{
                guard let currencyValue = Currency(json: value) else {
                    print("ERROR -> Not able to parse currency")
                    return (Currency(json: [String : Any]())!,"0")
                }
                if let totalCost = data["per_task_cost"] as? String{
                return (currencyValue,totalCost)
                }else{
                    return (currencyValue,"0")
                }
            }
        }
     return (Currency(json: [String : Any]())!,"0")
   
}
}

enum headerType {
    case Cart
    case Description
    case PickUpDetails
    case pickUpCustomeDetails
    case AppointmentDetails
    case DeliveryDetails
    case customDeliveryDetails
}

enum ConstantFieldHandlingType{
    case empty
    case showPrefiled
    case hidePrefiled
   
}


enum CustomFieldTypes: String {
   case name = ""
   case pickUpAddress = "1"
   case dropAddress = "32"
   case startDateTime = "55"
   case endDateTime = "4"
   case description = "5"
   
   case Text = "Text"
   case email = "Email"
   case url = "URL"
   case Number = "Number"
   case phonenumber = "Telephone"
   case DropDown = "Dropdown"
   case image = "Image"
   
   case date = "Date"
   case dateTime = "Date-Time"
   case dateTimePast = "Datetime-Past"
   case dateTimeFuture = "Datetime-Future"
   case dateFuture = "Date-Future"
   case datePast = "Date-Past"
   
   case checkBox = "Checkbox"
   
   case BarCode = "Barcode"
   case checklist = "Checklist"
   case table = "Table"
   
   func isDataTypeHandled() -> Bool {
      switch self {
      case .checklist, .table, .BarCode:
         return false
      default:
         return true
      }
   }
}



