//
//  TaskReviewModel.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 08/12/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit


class TaskReviewModel: NSObject {
    
    let pickupDeliveryCellHeight:CGFloat = 190.0
    let appointmentCellHeight:CGFloat = 210.0


    func getNumberOfRowsForTaskReview() -> Int {
        switch Singleton.sharedInstance.formDetailsInfo.work_flow {
        case WORKFLOW.pickupDelivery.rawValue?:
            switch Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag {
            case PICKUP_DELIVERY.pickup.rawValue?:
                return 1
            case PICKUP_DELIVERY.delivery.rawValue?:
                return 1
            case PICKUP_DELIVERY.both.rawValue?:
                return 2
            case PICKUP_DELIVERY.addDeliveryDetails.rawValue?:
                return 2
            default:
                break
            }
            break
        case WORKFLOW.appointment.rawValue?:
            return 1
        case WORKFLOW.fieldWorkforce.rawValue?:
            return 1
        default:
            break
        }
        return 2
    }
    
    func getHeightForCell(index:Int) -> CGFloat {
        let address = self.getAddress(index: index)
        let name = self.getName(index: index)
        let addressSize = address.heightWithConstrainedWidth(width: SCREEN_SIZE.width - 90, font: UIFont(name: FONT.light, size: FONT_SIZE.medium)!)
        let nameSize = name.heightWithConstrainedWidth(width: SCREEN_SIZE.width - 90, font: UIFont(name: FONT.bold, size: FONT_SIZE.medium)!)
       
        switch Singleton.sharedInstance.formDetailsInfo.work_flow {
        case WORKFLOW.pickupDelivery.rawValue?:
            return pickupDeliveryCellHeight + addressSize.height + nameSize.height
        case WORKFLOW.appointment.rawValue?, WORKFLOW.fieldWorkforce.rawValue?:
            return appointmentCellHeight + addressSize.height + nameSize.height
        default:
            break
        }
        return 0.0
    }
    
    func getAddress(index:Int) -> String {
        switch Singleton.sharedInstance.formDetailsInfo.work_flow {
        case WORKFLOW.pickupDelivery.rawValue?:
            switch Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag {
            case PICKUP_DELIVERY.pickup.rawValue?:
                return Singleton.sharedInstance.createTaskDetail.jobPickupAddress
            case PICKUP_DELIVERY.delivery.rawValue?:
                return Singleton.sharedInstance.createTaskDetail.customerAddress
            case PICKUP_DELIVERY.both.rawValue?:
                return Singleton.sharedInstance.createTaskDetail.customerAddress
            case PICKUP_DELIVERY.addDeliveryDetails.rawValue?:
                if index == 0 {
                    return Singleton.sharedInstance.createTaskDetail.jobPickupAddress
                } else {
                    return Singleton.sharedInstance.createTaskDetail.customerAddress
                }
            default:
                break
            }
            break
        case WORKFLOW.appointment.rawValue?:
            return Singleton.sharedInstance.createTaskDetail.customerAddress
        case WORKFLOW.fieldWorkforce.rawValue?:
           return Singleton.sharedInstance.createTaskDetail.customerAddress
        default:
            break
        }
        return ""
    }
    
    func getName(index:Int) -> String {
        switch Singleton.sharedInstance.formDetailsInfo.work_flow {
        case WORKFLOW.pickupDelivery.rawValue?:
            switch Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag {
            case PICKUP_DELIVERY.pickup.rawValue?:
                return Singleton.sharedInstance.createTaskDetail.jobPickupName
            case PICKUP_DELIVERY.delivery.rawValue?:
                return Singleton.sharedInstance.createTaskDetail.customerUsername
            case PICKUP_DELIVERY.both.rawValue?:
                return Singleton.sharedInstance.createTaskDetail.customerUsername
            case PICKUP_DELIVERY.addDeliveryDetails.rawValue?:
                if index == 0 {
                    return Singleton.sharedInstance.createTaskDetail.jobPickupName
                } else {
                    return Singleton.sharedInstance.createTaskDetail.customerUsername
                }
            default:
                break
            }
            break
        case WORKFLOW.appointment.rawValue?:
            return Singleton.sharedInstance.createTaskDetail.customerUsername
        case WORKFLOW.fieldWorkforce.rawValue?:
            return Singleton.sharedInstance.createTaskDetail.customerUsername
        default:
            break
        }
        return ""
    }
    
    func createPickupMetaData(forCustom:String = "",value:String = "") -> [Any] {
        var pickupMetadata = [Any]()
        for i in 0..<Singleton.sharedInstance.createTaskDetail.pickupMetadata.count {
            let customField = Singleton.sharedInstance.createTaskDetail.pickupMetadata[i] as! CustomFieldDetails
            if forCustom != customField.label!{
            
                var dict = ["label":customField.label!,"data":customField.data!]
                if customField.label! == "Task_Details" {
                     dict = [
                        "label":customField.label!,
                        "data":NLevelFlowManager.selectedProducts]
                }
                
                pickupMetadata.append(dict)
                
                
                
                
            }else{
                
                let dict = ["label":forCustom,"data":value]
                pickupMetadata.append(dict)
                
            }
        }
        print(pickupMetadata)
        return pickupMetadata
    }
    
    func createMetaData(forCustom:String = "",value:String = "") -> [Any] {
        var metadata = [Any]()
        
        for i in 0..<Singleton.sharedInstance.createTaskDetail.metaData.count {
            let customField = Singleton.sharedInstance.createTaskDetail.metaData[i] as! CustomFieldDetails
            
            if forCustom != customField.label!{
                
            var dict = ["label":customField.label!,"data":customField.data!]
                if customField.label! == "Task_Details" {
                    dict = ["label":customField.label!,"data":NLevelFlowManager.selectedProducts]
                }
            metadata.append(dict)
                
            }else{
                
            let dict = ["label":forCustom,"data":value]
            metadata.append(dict)
            
        }
    }
        print(metadata)
        return metadata
}
    
    
}
