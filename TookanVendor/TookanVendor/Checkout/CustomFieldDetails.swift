//
//  CustomFieldDetails.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 14/12/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

enum CustomFieldAccessRights: Int {
   case readOnly = 0
   case readWrite = 1
   case hidden = 2
   
   var isEditable: Bool {
      return self == .readWrite
   }
}

enum ImageSourceField: Int {
   case cameraAndPhotoLibrary = 0
   case camera
   case photoLibrary
}

class CustomFieldDetails: NSObject, NSCoding {
    var app_side: CustomFieldAccessRights!
    var data:String?
    var input:String?
    var label:String?
    var required:Int?
    var template_id:String?
    var value:Int?
    var fleetData:String?
    var imageUploadingStatus = [Bool]()
    var dataType = CustomFieldTypes.date
    var display_name = String()
    var data_type = String()
   var attribute = ImageSourceField.cameraAndPhotoLibrary
   
   var isRequiredField: Bool {
      return required == 1
   }
    
   required init?(coder aDecoder: NSCoder) {
      let appSideRawValue = (aDecoder.decodeObject(forKey: "app_side") as? Int) ?? 0
      app_side = CustomFieldAccessRights(rawValue: appSideRawValue)
      data = aDecoder.decodeObject(forKey: "data") as? String
      
      let dataTypeRawValue = (aDecoder.decodeObject(forKey: "data_type") as? String) ?? ""
      self.data_type = dataTypeRawValue
      self.dataType = CustomFieldTypes(rawValue: dataTypeRawValue) ?? .Text
      
      input = aDecoder.decodeObject(forKey: "input") as? String
      label = aDecoder.decodeObject(forKey: "label") as? String
      required = aDecoder.decodeObject(forKey: "required") as? Int
      template_id = aDecoder.decodeObject(forKey: "template_id") as? String
      value = aDecoder.decodeObject(forKey: "value") as? Int
      imageUploadingStatus = aDecoder.decodeObject(forKey: "imageUploadingStatus") as! [Bool]
      fleetData = aDecoder.decodeObject(forKey: "fleetData") as? String
   }
   
    func encode(with aCoder: NSCoder) {
        aCoder.encode(app_side.rawValue, forKey: "app_side")
        aCoder.encode(data, forKey: "data")
        aCoder.encode("\(dataType)", forKey: "data_type")
        aCoder.encode(input, forKey: "input")
        aCoder.encode(label, forKey: "label")
        aCoder.encode(required, forKey: "required")
        aCoder.encode(template_id, forKey: "template_id")
        aCoder.encode(value, forKey: "value")
        aCoder.encode(imageUploadingStatus, forKey: "imageUploadingStatus")
        aCoder.encode(fleetData, forKey: "fleetData")
    }
    
    init(json:[String:Any]) {
      
        if let value = json["display_name"] as? String {
            self.display_name = value
        }
      
      if let value = json["app_side"] {
         let appSideRawValue = Int("\(value)") ?? 0
         self.app_side = CustomFieldAccessRights(rawValue: appSideRawValue)
      }
      
      if let rawImageAttribute = json["attribute"] as? NSNumber,
         let imageAttribute = ImageSourceField(rawValue: rawImageAttribute.intValue) {
         self.attribute = imageAttribute
      }
      
      if let value = json["data"] as? String {
         self.data = value
      } else if let value = json["data"] as? NSNumber {
         self.data = "\(value)"
      } else if let value = json["data"] as? [Any], value.count > 0 {
         self.data = value.jsonString
      } else {
         data = ""
      }
        
      if let value = json["vendor_data"] as? String {
         self.data = value
      } else if let value = json["fleet_data"] as? String {
         self.fleetData = value
      } else {
         self.fleetData = ""
      }
      
        if let value = json["data_type"] as? String {
           self.dataType = CustomFieldTypes(rawValue: value) ?? .Text
        }
        
        if let value = json["input"] as? String {
            self.input = value
        } else {
            self.input = ""
        }
        
        if let value = json["label"] as? String {
            self.label = value
        } else {
            self.label = ""
        }
        
        if let value = json["required"] as? String {
            self.required = Int(value)
        } else if let value = json["required"] as? Int {
            self.required = value
        } else {
            self.required = 0
        }
        
        if let value = json["template_id"] as? String {
            self.template_id = value
        } else {
            self.template_id = ""
        }
        
        if let value = json["value"] as? String {
            self.value = Int(value)
        } else if let value = json["value"] as? Int {
            self.value = value
        } else {
            self.value = 0
        }
    }
    
    func getDataType(type:String) {
        switch type.lowercased() {
        case "text":
            self.dataType = .Text
        default:
            self.dataType = .name
        }
    }
    
    
    func checkIfImageIsBeingUploaded() -> Bool {
        for i in self.imageUploadingStatus {
            if i == false {
                return true
            }
        }
        return false
    }
   
   func getParamsToSendToServer() -> [String: Any] {
      var params = [String: Any]()
      
      params["label"] = label!
      params["data"] = data!
      
      return params
   }
   
   func startImageUploadingProcessForImagePath(path: String) {
      appendImagePathToData(path: path)
      setImageUploadInProgressFlag()
   }
   
   func appendImagePathToData(path: String) {
      var images = data?.jsonObjectArray
      images?.append(path)
      data = images?.jsonString
   }
   
   func setImageUploadInProgressFlag() {
      imageUploadingStatus.append(false)
   }
   
   func getImageArrayCount() -> Int {
      return data?.jsonObjectArray.count ?? 0
   }
   
   func deleteImageAt(index: Int) {
      var arrayOfImageUrls = data?.jsonObjectArray
      
      guard (arrayOfImageUrls?.count ?? -1) > index else {
         return
      }
      
      arrayOfImageUrls?.remove(at: index)
      imageUploadingStatus.remove(at: index)
      
      data = arrayOfImageUrls?.jsonString
   }
   

   func imageUpadationSuccessfulAt(index: Int, withPath path: String) {
      var arrayOfImageUrls = data?.jsonObjectArray
      
      guard (arrayOfImageUrls?.count ?? -1) > index else {
         return
      }
      
      arrayOfImageUrls?[index] = path
      imageUploadingStatus[index] = true
      
      data = arrayOfImageUrls?.jsonString
   }
   
   func isRequiredFieldAndEditable() -> Bool {
      return app_side == .readWrite && isRequiredField
   }
   
   func getErrorMessageInCaseFieldIsEmpty() -> String {
         return "\(display_name) cannot be empty".localized
   }
   
   func getErrorMessageInCaseFieldIsNotValid() -> String {
      switch dataType {
      case .image:
         return ERROR_MESSAGE.IMAGE_UPLOADING
      default:
         return "\(display_name) is not valid"
      }
      
   }
   
   // MARK: - Types Methods
   class func getArrayOfCustomFieldsFrom(json: [[String: Any]]) -> [CustomFieldDetails] {
      var customFields = [CustomFieldDetails]()
      for rawCustomField in json {
         let customField = CustomFieldDetails(json: rawCustomField)
         customFields.append(customField)
      }
      return customFields
   }
   
   class func uploadImageWith(path: String, completion: @escaping (_ success: Bool, _ refPath: String?) -> Void) {
      
      HTTPClient.makeMultiPartRequestWith(method: .POST, showActivityIndicator: false, para: nil, extendedUrl: API_NAME.uploadImageForReference, imageList: ["ref_image": path]) { (responseObject, _, _, statusCode) in
         
         guard statusCode == STATUS_CODES.SHOW_DATA,
            let response = responseObject as? [String: Any],
         let data = response["data"] as? [String: Any],
            let refLink = data["ref_image"] as? String else {
               completion(false, nil)
               return
         }
         
         completion(true, refLink)
      }
   }
}
