//
//  BQFeedBackCommentCell.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 14/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

protocol BQFeedBackCommentCellDelegate: class {
    func feedbackComment(textReview: String)
}

class BQFeedBackCommentCell: UITableViewCell, UITextViewDelegate {
    @IBOutlet weak var txtView: UITextView!
    weak var delegate: BQFeedBackCommentCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        
        txtView.layer.cornerRadius = 0.0
        txtView.layer.borderColor = UIColor.gray.cgColor
        txtView.layer.borderWidth = 1.0
        // Initialization code
    }

    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Please input your reviews..." {
            textView.text = ""
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Please input your reviews..."
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let txtString = textView.text else {
            return false
        }
        let finalString = (txtString as NSString).replacingCharacters(in: range, with: text)
        
        delegate?.feedbackComment(textReview: finalString)
        return true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
