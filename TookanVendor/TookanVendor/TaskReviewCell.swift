//
//  TaskReviewCell.swift
//  TookanVendor
//
//  Created by CL-macmini-73 on 11/30/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class TaskReviewCell: UITableViewCell {

    @IBOutlet var smallDotLabel: UILabel!
    @IBOutlet var firstBigDot: UIImageView!
    @IBOutlet var detailView: UIView!
    @IBOutlet var lastBigDot: UIImageView!
    @IBOutlet var nameHeader: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var addressHeader: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var dateHeader: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var timeHeader: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var deleteButton: UIButton!
    @IBOutlet var editButton: UIButton!
    @IBOutlet var seperateLine: UIView!
    @IBOutlet var pickupDeliveryTag: UIButton!
    @IBOutlet var leftConstraint: NSLayoutConstraint!
    @IBOutlet var addLocationButton: UIButton!
    
    let pickupDeliveryTagWidth:CGFloat = 22.0
    let leftMarginWithDot:CGFloat = 45.0
    let leftMarginWithoutDot:CGFloat = 15.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        smallDotLabel.backgroundColor = UIColor(patternImage:#imageLiteral(resourceName: "iconPattern"))
        
        /*-------------- pickup/delivery tag ----------*/
        self.pickupDeliveryTag.setTitleColor(UIColor.white, for: UIControlState.selected)
        self.pickupDeliveryTag.setTitleColor(COLOR.SPLASH_TEXT_COLOR, for: UIControlState.normal)
        self.pickupDeliveryTag.backgroundColor = UIColor.white
        self.pickupDeliveryTag.titleLabel?.font = UIFont(name: FONT.regular, size: 9.0)
        self.pickupDeliveryTag.layer.cornerRadius = pickupDeliveryTagWidth/2
        self.pickupDeliveryTag.layer.borderColor = COLOR.SPLASH_TEXT_COLOR.cgColor
        self.pickupDeliveryTag.isUserInteractionEnabled = false
        /*-------------- Header Label ------------------*/
        self.nameHeader.textColor = COLOR.PLACEHOLDER_COLOR
        self.nameHeader.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        self.addressHeader.textColor = COLOR.PLACEHOLDER_COLOR
        self.addressHeader.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        self.dateHeader.textColor = COLOR.PLACEHOLDER_COLOR
        self.dateHeader.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        self.timeHeader.textColor = COLOR.PLACEHOLDER_COLOR
        self.timeHeader.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        self.timeHeader.isHidden = true
        /*-------------- Text Label ------------------*/
        self.nameLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        self.nameLabel.font = UIFont(name: FONT.bold, size: FONT_SIZE.medium)
        self.nameLabel.text = TEXT.NO_NAME
        self.addressLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        self.addressLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
        self.addressLabel.text = TEXT.NO_ADDRESS
        self.dateLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        self.dateLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
        self.dateLabel.text = TEXT.NO_DATETIME
        self.timeLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        self.timeLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
        self.timeLabel.text = TEXT.NO_DATETIME
        self.timeLabel.isHidden = true
        /*--------------- Line ---------------------*/
        self.seperateLine.backgroundColor = COLOR.SPLASH_LINE_COLOR
        /*--------------- Buttons -------------*/
        self.deleteButton.setImage(#imageLiteral(resourceName: "iconDeleteRedRow"), for: UIControlState.normal)
        self.deleteButton.setTitle(TEXT.DELETE, for: UIControlState.normal)
        self.deleteButton.setTitleColor(COLOR.DELETE_COLOR, for: UIControlState.normal)
        self.deleteButton.titleLabel?.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
        self.deleteButton.backgroundColor = COLOR.REVIEW_BACKGROUND_COLOR
        
        self.editButton.setImage(#imageLiteral(resourceName: "iconEditRow"), for: UIControlState.normal)
        self.editButton.setTitle(TEXT.EDIT, for: UIControlState.normal)
        self.editButton.setTitleColor(COLOR.SPLASH_TEXT_COLOR, for: UIControlState.normal)
        self.editButton.titleLabel?.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
        self.editButton.backgroundColor = COLOR.REVIEW_BACKGROUND_COLOR
        /*----------------- Detail View --------------*/
        self.detailView.backgroundColor = UIColor.white
        self.detailView.layer.cornerRadius = 4.0
        self.detailView.layer.shadowOffset = CGSize(width: 2, height: 3)
        self.detailView.layer.shadowOpacity = 0.7
        self.detailView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        /*------------------ Add Location ----------------*/
        self.addLocationButton.setTitleColor(COLOR.PLACEHOLDER_COLOR, for: UIControlState.normal)
        self.addLocationButton.titleLabel?.font = UIFont(name: FONT.bold, size: FONT_SIZE.medium)
        self.addLocationButton.isUserInteractionEnabled = false
    }
    
    func setTaskReviewAsPerLayout(index:Int) {
        if Singleton.sharedInstance.createTaskDetail.hasDelivery == 1 && Singleton.sharedInstance.createTaskDetail.hasPickup == 1{
            Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag = PICKUP_DELIVERY.addDeliveryDetails.rawValue
        }
        
        switch Singleton.sharedInstance.formDetailsInfo.work_flow {
        case WORKFLOW.pickupDelivery.rawValue?:
            switch Singleton.sharedInstance.formDetailsInfo.pickup_delivery_flag {
            case PICKUP_DELIVERY.pickup.rawValue?:
                self.setViewForSingleTask()
                self.setPickupTaskDetails()
                break
            case PICKUP_DELIVERY.delivery.rawValue?:
                self.setViewForSingleTask()
                self.setDeliveryTaskDetails()
                break
            case PICKUP_DELIVERY.both.rawValue?:
                self.setViewForPickupDeliveryTask()
                if Singleton.sharedInstance.createTaskDetail.hasPickup == 1 {
                    if index == 0 {
                        self.firstBigDot.isHidden = false
                        self.lastBigDot.isHidden = true
                        self.setPickupTaskDetails()
                        self.setPickupDeliveryTag(title: "P")
                    } else {
                        self.setAddDeliveryLocation()
                        self.firstBigDot.isHidden = true
                        self.lastBigDot.isHidden = false
                    }
                } else {
                    if index == 0 {
                        self.setAddPickupLocation()
                        self.firstBigDot.isHidden = false
                        self.lastBigDot.isHidden = true
                    } else {
                        self.firstBigDot.isHidden = true
                        self.lastBigDot.isHidden = false
                        self.setDeliveryTaskDetails()
                        self.setPickupDeliveryTag(title: "D")
                    }
                }
                break
            case PICKUP_DELIVERY.addDeliveryDetails.rawValue?:
                self.setViewForPickupDeliveryTask()
                if index == 0 {
                    self.firstBigDot.isHidden = false
                    self.lastBigDot.isHidden = true
                    self.setPickupTaskDetails()
                    self.setPickupDeliveryTag(title: "P")
                } else {
                    self.firstBigDot.isHidden = true
                    self.lastBigDot.isHidden = false
                    self.setDeliveryTaskDetails()
                    self.setPickupDeliveryTag(title: "D")
                }
                break
            default:
                break
            }
            break
        case WORKFLOW.appointment.rawValue?:
            self.setViewForSingleTask()
            self.setAppointmentFieldWorkForceTaskDetails()
            break
        case WORKFLOW.fieldWorkforce.rawValue?:
            self.setViewForSingleTask()
            self.setAppointmentFieldWorkForceTaskDetails()
            break
        default:
            break
        }
    }
    
    func setViewForSingleTask() {
        self.leftConstraint.constant = leftMarginWithoutDot
        self.firstBigDot.isHidden = true
        self.smallDotLabel.isHidden = true
        self.addLocationButton.isHidden = true
        self.pickupDeliveryTag.isHidden = true
    }
    
    func setViewForPickupDeliveryTask() {
        self.leftConstraint.constant = leftMarginWithDot
        self.firstBigDot.isHidden = false
        self.smallDotLabel.isHidden = false
        self.addLocationButton.isHidden = true
        self.pickupDeliveryTag.isHidden = false
        self.detailView.isHidden = false
    }
    
    func setAddDeliveryLocation() {
        self.detailView.isHidden = true
        self.addLocationButton.isHidden = false
        self.addLocationButton.setTitle(TEXT.ADD_DELIVERY_LOCATION, for: UIControlState.normal)
        self.setAddTag()
    }
    
    func setAddPickupLocation() {
        self.detailView.isHidden = true
        self.addLocationButton.isHidden = false
        self.addLocationButton.setTitle(TEXT.ADD_PICKUP_LOCATION, for: UIControlState.normal)
        self.setAddTag()
    }
    
    
    func setPickupTaskDetails() {
        /*------------ Header -----------*/
        self.nameHeader.text = TEXT.NAME
        self.addressHeader.text = TEXT.ADDRESS_PLACEHODLER
        self.dateHeader.text = TEXT.DATE_TIME
        /*------------ Details ---------------*/
        if Singleton.sharedInstance.createTaskDetail.jobPickupName.length > 0 {
            self.nameLabel.text = Singleton.sharedInstance.createTaskDetail.jobPickupName
        }
        self.addressLabel.text = Singleton.sharedInstance.createTaskDetail.jobPickupAddress
        self.dateLabel.text = Singleton.sharedInstance.createTaskDetail.jobPickupDateTime.dateFromString(withFormat: "dd MMM yyyy, hh:mm a")
    }
    
    func setDeliveryTaskDetails() {
        /*------------ Header -----------*/
        self.nameHeader.text = TEXT.NAME
        self.addressHeader.text = TEXT.ADDRESS_PLACEHODLER
        self.dateHeader.text = TEXT.DATE_TIME
        /*------------ Details ---------------*/
        if Singleton.sharedInstance.createTaskDetail.customerUsername.length > 0 {
            self.nameLabel.text = Singleton.sharedInstance.createTaskDetail.customerUsername
        }
        self.addressLabel.text = Singleton.sharedInstance.createTaskDetail.customerAddress
        self.dateLabel.text = Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime.dateFromString(withFormat: "dd MMM yyyy, hh:mm a")
    }
    
    func setAppointmentFieldWorkForceTaskDetails() {
        self.setViewForSingleTask()
        /*------------ Header -----------*/
        self.nameHeader.text = TEXT.NAME
        self.addressHeader.text = TEXT.ADDRESS_PLACEHODLER
        self.dateHeader.text = TEXT.START_TIME + " - " + TEXT.END_TIME
        /*------------ Details ---------------*/
        if Singleton.sharedInstance.createTaskDetail.customerUsername.length > 0 {
            self.nameLabel.text = Singleton.sharedInstance.createTaskDetail.customerUsername
        }
        self.addressLabel.text = Singleton.sharedInstance.createTaskDetail.customerAddress
        self.dateLabel.text = Singleton.sharedInstance.createTaskDetail.jobPickupDateTime.dateFromString(withFormat: "dd MMM yyyy, hh:mm a") + " - " + Singleton.sharedInstance.createTaskDetail.jobDeliveryDatetime.dateFromString(withFormat: "dd MMM yyyy, hh:mm a")
    }

    //MARK: SET TAG
    func setPickupDeliveryTag(title:String) {
        self.pickupDeliveryTag.layer.borderWidth = 0.8
        self.pickupDeliveryTag.setImage(nil, for: UIControlState.normal)
        self.pickupDeliveryTag.setTitle(title, for: UIControlState.normal)
        if self.pickupDeliveryTag.isSelected == true {
            self.pickupDeliveryTag.backgroundColor = COLOR.SPLASH_TEXT_COLOR
        } else {
            self.pickupDeliveryTag.backgroundColor = UIColor.white
        }
    }
    
    func setAddTag() {
        self.pickupDeliveryTag.setTitle("", for: UIControlState.normal)
        self.pickupDeliveryTag.setImage(#imageLiteral(resourceName: "iconAddLocationReview"), for: UIControlState.normal)
        self.pickupDeliveryTag.layer.borderWidth = 0.0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
