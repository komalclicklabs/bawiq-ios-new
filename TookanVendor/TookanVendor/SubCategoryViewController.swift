//
//  SubCategoryViewController.swift
//  TookanVendor
//
//  Created by Vishal on 12/02/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class SubCategoryViewController: UIViewController, NavigationDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, SubCategoryView, SubCategoryFlowLayouttDelegate, BQSelectDateTimeVCDelegate {
    
    @IBOutlet weak var subCategoryTableView: UITableView!
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    @IBOutlet weak var noSubCategoryLbl: UILabel!
    @IBOutlet var sectionHeaderView: UIView!
    @IBOutlet weak var subCategoryCollectionView: UICollectionView!
    
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var cartBottomView: UIView!
    var seletedCategory: Category!
    var selectedCategoryId: Int!
    var navigationBar:NavigationView!
    let colectionCellWidthConstant: CGFloat = 10
    
    var presenter :SubCategoryPresenter!
    fileprivate var itemCellView: ProductCellView?
    fileprivate var itemSelectedIndex: Int = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblTotalAmount.text = "0"
        self.setUpPresenter()
        self.initializeView()
        noSubCategoryLbl.isHidden = true
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
    }
    
    func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
                let selectedIndex = presenter.selectedCollectionIndex
                let newIndex = selectedIndex - 1
                if newIndex >= 0 {
                    presenter.selectedCollectionIndex = newIndex
                    let indexPath = IndexPath(item: newIndex, section: 0)
                    presenter.didSelectItemAt(index: indexPath)
                }
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                let selectedIndex = presenter.selectedCollectionIndex
                let newIndex = selectedIndex + 1
                let subcategoryCount = presenter.noOfItemsInSection(section: 0)
                if newIndex < subcategoryCount {
                    presenter.selectedCollectionIndex = newIndex
                    let indexPath = IndexPath(item: newIndex, section: 0)
                    presenter.didSelectItemAt(index: indexPath)
                }
            default:
                break
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        guard let category = seletedCategory else {
            return
        }
        self.presenter.getSubCategories(category: category)
    }
    
    private func setUpPresenter() {
        presenter = SubCategoryImplementation(withView: self)
    }
    
    private func initializeView() {
        cartBottomView.isHidden = true
        self.setNavigationBar()
        self.setupTableViews()
        self.setupCollectionViews()
        
        UIApplication.shared.statusBarStyle = .default
    }
    
    func backAction() {
        
    }
    
    func setUserCurrentLocation() {
        print("setUserCurrentLocation")
    }
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        var title = ""
        guard let category = seletedCategory else {
            return
        }
        
        if let tle = category.categoryName {
            title = tle
        }
        
        let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
        if isGuestUser {
            navigationBar = NavigationView.getNibFile(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: title.capitalizingFirstLetter(), leftButtonImage: #imageLiteral(resourceName: "BackButtonWhite"), rightButtonImage: nil)
                , leftButtonAction: {[weak self] in
                    self?.navigationController?.popViewController(animated: true)
                }, rightButtonAction: {[weak self] in
                    //self?.gotoCart()
            })
            
            navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
            navigationBar.delegate = self
            self.view.addSubview(navigationBar)
        } else {
            navigationBar = NavigationView.getNibFile(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: title.capitalizingFirstLetter(), leftButtonImage: #imageLiteral(resourceName: "BackButtonWhite"), rightButtonImage: #imageLiteral(resourceName: "cartButton"))
                , leftButtonAction: {[weak self] in
                    self?.navigationController?.popViewController(animated: true)
                }, rightButtonAction: {[weak self] in
                    self?.gotoCart()
            })
            
            navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
            navigationBar.delegate = self
            self.view.addSubview(navigationBar)
        }
        
        
    }
    func searchText(text: String) {
        print(text)
    }
    private func setupCollectionViews() {
        subCategoryCollectionView.delegate = self
        subCategoryCollectionView.dataSource = self
        if let flowLayout = subCategoryCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = UICollectionViewFlowLayoutAutomaticSize
        }
        bannerCollectionView.delegate = self
        bannerCollectionView.dataSource = self
        bannerCollectionView.reloadData()
        
    }
    
    private func setupTableViews() {
        self.subCategoryTableView.delegate = self
        self.subCategoryTableView.dataSource = self
        self.subCategoryTableView.rowHeight = UITableViewAutomaticDimension
        self.subCategoryTableView.estimatedRowHeight = 200
        // self.subCategoryTableView.estimatedRowHeight = 50
        self.subCategoryTableView.register(UINib(nibName: "ProductCell", bundle: nil), forCellReuseIdentifier: "ProductCell")
    }
    
    func reloadCollection() {
        self.subCategoryTableView.reloadData()
        self.subCategoryCollectionView.reloadData()
        self.bannerCollectionView.reloadData()
    }
    func reloadProductTable() {
        self.subCategoryCollectionView.reloadData()
        self.subCategoryTableView.reloadData()
        self.bannerCollectionView.reloadData()
    }
    
    fileprivate func gotoCart() {
        
        if self.cartBottomView.isHidden {
            if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQNoItemsInCartViewVC") as? BQNoItemsInCartViewVC {
                self.navigationController?.present(vc, animated: true, completion: nil)
            }
        } else {
            if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartSegmentViewController") as? CartSegmentViewController {
                self.navigationController?.pushViewController(vc, animated: true)
            }
//            if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "RecommendedProductViewController") as? RecommendedProductViewController {
//                self.navigationController?.pushViewController(vc, animated: true)
//            }
        }
    }
    
    func goToRecommendedProductsScreen() {
        if self.cartBottomView.isHidden {
            if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQNoItemsInCartViewVC") as? BQNoItemsInCartViewVC {
                self.navigationController?.present(vc, animated: true, completion: nil)
            }
        } else {
            
            self.getRecommendedProducts(limit: 10, skip: 0) { (success, products) in
                DispatchQueue.main.async {
                    ActivityIndicator.sharedInstance.hideActivityIndicator()
                    if success {
                        if let result = products {
                            if result.count > 0 {
                                if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "RecommendedProductViewController") as? RecommendedProductViewController {
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            } else {
                                if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartSegmentViewController") as? CartSegmentViewController {
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                        } else {
                            if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartSegmentViewController") as? CartSegmentViewController {
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    } else {
                        if let vc = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartSegmentViewController") as? CartSegmentViewController {
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                }
            }
            
            
        }
    }
    
    @IBAction func goToCartView(_ sender: UIButton) {
        goToRecommendedProductsScreen()
        //gotoCart()
    }
    
    func getRecommendedProducts(limit: Int = 10, skip: Int = 0, _ receivedResponse: @escaping (_ succeeded: Bool, _ response:[Products]?) -> ()){
        guard let lat = BQBookingModel.shared.latitude else {
            return
        }
        guard let long = BQBookingModel.shared.longitude else {
            return
        }
        
        let params: [String : Any] = [
            "limit": limit,
            "skip": skip,
            "app_access_token": Vendor.current!.appAccessToken!,
            "latitude": "\(lat)",
            "longitude": "\(long)"
            // ,"product_id": "27584"
        ]
        
        
        print("Get recommended products param: ", params)
        ActivityIndicator.sharedInstance.showActivityIndicator()
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.getRecommendedProducts,
                                                         params: params as [String : AnyObject],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                            
                                                            DispatchQueue.main.async {
                                                                if succeeded {
                                                                    guard let data = response["data"] as? [String: Any], let products = data["data"] as? [[String: Any]] else {
                                                                        receivedResponse(false,nil)
                                                                        return
                                                                    }
                                                                    guard let recommendedCount = data["count"] as? Int else {
                                                                        return
                                                                    }
                                                                    let stringJson = products.jsonString
                                                                    let productsData = stringJson.data(using: .utf8)
                                                                    do {
                                                                        let productsArray = try JSONDecoder().decode([Products].self, from: productsData!)
                                                                        receivedResponse(true, productsArray)
                                                                        
                                                                    } catch {
                                                                        print(error.localizedDescription)
                                                                        receivedResponse(false,nil)
                                                                    }
                                                                } else {
                                                                    receivedResponse(false,nil)
                                                                }
                                                            }
        }
        
    }
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        print("number of section \(presenter.noOfSections)")
        return 1 //presenter.noOfSections
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("number of items \(presenter.noOfItemsInSection(section: section)) in section \(section) ")
        
        return presenter.noOfItemsInSection(section: section)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == bannerCollectionView {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryBannerCell", for: indexPath) as? SubCategoryBannerCell else {
                return UICollectionViewCell()
            }
            presenter.setupData(view: cell, index: presenter.selectedCollectionIndex)
//            presenter.setupData(view: cell, index: indexPath.row)
            return cell
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath) as? SubCategoryCell else {
                return UICollectionViewCell()
            }
            presenter.setupData(view: cell, index: indexPath.row)
//            if indexPath.row == 0 {
//                presenter.didSelectItemAt(index: indexPath)
//            }
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == bannerCollectionView {
            return CGSize(width: self.view.frame.size.width,
                          height: self.view.frame.size.height/3)
        } else {
            let subCategory = presenter.cellForRowProduct(indexpath: indexPath)
            guard let subcateoryName = subCategory.subCategoryName else  {
                fatalError("sub category name not found")
            }
            let approximateWidth = CGSize(width: 150, height: 50)
            let estimatedFrame = NSString(string: subcateoryName).boundingRect(with: approximateWidth, options: .usesLineFragmentOrigin, attributes: [:], context: nil)
            return CGSize(width: estimatedFrame.width, height: 50)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath) as? SubCategoryCell {

//        }
        if collectionView == bannerCollectionView {
            presenter.selectedBannerCollectionIndex = indexPath.row
        }
        if collectionView == subCategoryCollectionView {
            presenter.selectedCollectionIndex = indexPath.row
            presenter.didSelectItemAt(index: indexPath)
        }
    }
    
    
    func refreshCell(at index: Int, isAdded: Bool, cartCount: Int, totalPrice: Float) {
        
        
        if cartCount > 0 {
            nextBtn.isUserInteractionEnabled = true
            cartBottomView.isHidden = false
        } else {
            nextBtn.isUserInteractionEnabled = false
            cartBottomView.isHidden = false
        }
        
        self.lblTotalAmount.text = String(format: "%.2f", totalPrice) //String(totalPrice)
//        if subCategoryTableView != nil {
//            subCategoryTableView.reloadData()
//        }
        
//        let indexPath = IndexPath(row: index, section: 0)
//        subCategoryTableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func selectDOB() {
        let selectDateVC = BQSelectDateTimeVC(nibName: "BQSelectDateTimeVC", bundle: nil)
        selectDateVC.modalPresentationStyle = .overCurrentContext
        if let vc = UIApplication.shared.keyWindow?.topMostWindowController {
            selectDateVC.delegate = self
            selectDateVC.isFromAddItem = true
            selectDateVC.messageStr = "You must be 21 years above to add this product."
            self.present(selectDateVC, animated: true, completion: nil)
        }
    }
    
    func bookingDate(date: String) {
        if self.itemSelectedIndex > -1 {
            if let view = self.itemCellView {
                presenter.updateView(view: view, index: self.itemSelectedIndex, add: true, dobString: date)
                print("Your age is greater than 21", date)
            }
        }
    }
    
    func showError(message: String) {
        Singleton.sharedInstance.showAlert(message)
    }
    
}


extension SubCategoryViewController: ProductCellDelegate {
    func productCellAddButtonTapped(cell: ProductCell) {
        let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
        if isGuestUser {
            self.showGuestUserError()
        } else {
            if let index = subCategoryTableView.indexPath(for: cell) {
                self.itemCellView = cell
                self.itemSelectedIndex = index.row
                presenter.updateView(view: cell, index: index.row, add: true, dobString: "")
            }
        }
    }
    func productCellRemoveCategoryButtonTapped(cell: ProductCell) {
        print("productCellRemoveCategoryButtonTapped")
    }
    func productCellDeleteButtonTapped(cell: ProductCell) {
        let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
        if isGuestUser {
            self.showGuestUserError()
        } else {
            if let index = subCategoryTableView.indexPath(for: cell) {
                presenter.updateView(view: cell, index: index.row, add: false, dobString: "")
            }
        }
        
    }
    
    func productCellFavButtonTapped(cell: ProductCell, isfavStatus: Bool) {
        let isGuestUser = UserDefaults.standard.bool(forKey: "isGuestUser")
        if isGuestUser {
            self.showGuestUserError()
        } else {
            if let index = subCategoryTableView.indexPath(for: cell) {
                presenter.markFavUnfavProduct(view: cell, index: index.row)
            }
        }
    }
    
    func showGuestUserError() {
        let alertController = UIAlertController(title: "", message: "Please login to proceed further.", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .destructive, handler: { (action) -> Void in
            Singleton.sharedInstance.logoutButtonAction()
        })
        let noAction = UIAlertAction(title: "No", style: .cancel, handler: { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        //        alertController.view.tintColor = UIColor.red
        self.present(alertController, animated: true, completion: nil)
    }
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

extension SubCategoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = sectionHeaderView
        view?.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 50)
        return view
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if presenter.numberOfItemsInTable() > 0 {
            bannerView.isHidden = false
            subCategoryCollectionView.isHidden = false
            noSubCategoryLbl.isHidden = true
            return presenter.numberOfItemsInTable()
        } else {
            bannerView.isHidden = false
            subCategoryCollectionView.isHidden = false
            noSubCategoryLbl.isHidden = false
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as? ProductCell else {
            return UITableViewCell()
        }
        cell.delegate = self
        cell.leftProductDescription.isHidden = true
        cell.productDescription.isHidden = false
        cell.discountImage.isHidden = true
        cell.lblDiscount.isHidden = true
        presenter.setupDataTable(view: cell, index: indexPath.row)
        return cell
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {

        if (subCategoryTableView.contentOffset.y + subCategoryTableView.frame.size.height) >= subCategoryTableView.contentSize.height - 20 {
            presenter.paginationHit()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
}
////////////////////////////////////////////////////////////////////////////////////////////////////////
enum SubCategoryCellType {
    case subCategoryBannerCell
    case subCategoryCell
}

protocol SubCategoryView: class  {
    func reloadCollection()
    func reloadProductTable()
    func refreshCell(at index: Int, isAdded: Bool, cartCount: Int, totalPrice: Float)
    func selectDOB()
    
}



protocol SubCategoryPresenter {
    var selectedBannerCollectionIndex: Int {get set}
    var selectedCollectionIndex: Int {get set}
    var noOfSections: Int {get}
    func noOfItemsInSection(section: Int) -> Int
    func identifierForSection(indexpath: IndexPath) -> String
    func cellForRowProduct(indexpath: IndexPath) -> Category
    func setupData(view: SubCategoryBannerCellView, index: Int)
    func setupData(view: SubCategoryCellView, index: Int)
    func getSubCategories(category: Category)
    func didSelectItemAt(index: IndexPath)
    func numberOfItemsInTable() -> Int
    func setupDataTable(view: ProductCellView, index: Int)
    func updateView(view: ProductCellView, index: Int, add: Bool, dobString: String)
    func paginationHit()
    func markFavUnfavProduct(view: ProductCellView, index: Int)
}

class SubCategoryImplementation: SubCategoryPresenter {
    
    var mainCategory: Category?
    weak var view: SubCategoryView?
    var subcategories = [Category]()
    var products = [Products]()
    var selectedIndex = 0
    var selectedBannerIndex = 0
    
    private var skip: Int = 0
    private var limit: Int = 20
    var isPaginationRequired: Bool = false
    
    
    var selectedBannerCollectionIndex: Int {
        set {
            selectedBannerIndex = newValue
        }
        get {
            return selectedBannerIndex
        }
    }
    
    var selectedCollectionIndex: Int {
        set {
            selectedIndex = newValue
        }
        get {
            return selectedIndex
        }
    }
    
    init(withView view: SubCategoryView) {
        self.view = view
    }
    
    var noOfSections: Int {
        return 3
    }
    
    var tableCellIdentifier = "ProductView"
    
    func identifierForSection(indexpath: IndexPath) -> String {
        switch indexpath.section {
        case 0:
            return "SubCategoryBannerCell"
        case 1:
            return "SubCategoryCell"
        case 2:
            return "ProductListCollectionCell"
        default:
            return ""
        }
    }
    
    func numberOfItemsInTable() -> Int {
        return products.count
    }
    
    func noOfItemsInSection(section: Int) -> Int {
        switch section {
        case 0:
            return subcategories.count
        case 1:
            return subcategories.count
        case 2:
            return 0
        default:
            return 0
        }
    }
    
    func setupData(view: SubCategoryBannerCellView, index: Int) {
        let subcategory = subcategories[index]
        let model = SubCategoryBannerCellImplemention(data: subcategory)
        view.setupData(model: model)
    }
    
    
    func setupData(view: SubCategoryCellView, index: Int) {
        if subcategories.count > 0 {
            let subcategory = subcategories[index]
            view.setupData(subCategory: subcategory.subCategoryName!, underlineViewHidden: selectedIndex == index ? false : true)
        }
    }
    
    func setupDataTable(view: ProductCellView, index: Int) {
        if products.count > 0 {
            let product = products[index]
            view.setupDataForSubCategoryProducts(data: product)//(data: product)
        }
    }
    
    func cellForRowProduct(indexpath: IndexPath) -> Category {
        return subcategories[indexpath.row]
    }
    
    func getSubCategories(category: Category) {
        self.mainCategory = category
        
        ActivityIndicator.sharedInstance.showActivityIndicator()

        mainCategory?.getSubCategories(limit: 20, skip: 0, { (success, categories, totalPrice) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if success {
                    guard let _categories = categories else {
                        return
                    }
                    if let price = totalPrice {
                        if price > 0 {
                            self.view?.refreshCell(at: 0, isAdded: true, cartCount: _categories.count, totalPrice: price)
                        } else {
                            self.view?.refreshCell(at: 0, isAdded: false, cartCount: 0, totalPrice: price)
                        }
                    }
                    self.subcategories = _categories
                    let indexPath = IndexPath(item: 0, section: 0)
                    self.didSelectItemAt(index: indexPath)
                    self.view?.reloadCollection()
                }
            }
        })
    }
    
    func didSelectItemAt(index: IndexPath) {
        if subcategories.count > 0 {
            skip = 0
            limit = 20
            self.products.removeAll()
            getProductsForSubCategory(category: mainCategory!, subCategory: subcategories[index.item])
        }
    }
    
    func reset() {
        skip = 0
        limit = 20
    }
    
    func paginationHit() {
        if self.isPaginationRequired == true {
            skip += 20
            if subcategories.count > 0 {
                getProductsForSubCategory(category: mainCategory!, subCategory: subcategories[self.selectedIndex])
            }
        }
    }
    
    func getProductsForSubCategory(category: Category, subCategory: Category) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        subCategory.getProducts(category: category, limit: limit, skip: skip) { (success, products, paginationRequired) in
            DispatchQueue.main.async {
                ActivityIndicator.sharedInstance.hideActivityIndicator()
                if success {
                    guard let _products = products else {
                        return
                    }
                    self.isPaginationRequired = paginationRequired
                    if !paginationRequired {
                        self.products = _products
                    } else {
                        self.products += _products
                    }
                    //self.products = _products
                    self.view?.reloadProductTable()
                } 
            }
        }
    }
    
    func updateView(view: ProductCellView, index: Int, add: Bool, dobString: String = "") {
        if add {
            addProductToCart(product: products[index],view: view, index: index, dobString: dobString)
        } else {
            deleteProductFromCart(product: products[index],view: view, index: index)
        }
    }
    
    
    func addProductToCart(product: Products, view: ProductCellView, index: Int, dobString: String = "") {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        product.addProduct(dobString: dobString) { (success, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if success {
                view.updateValue(added: true)
                if let count = self.products[index].inCartCount {
                    self.products[index].inCartCount = count + 1
                }
                
                if let totalPrice = response!["total_price"] as? Double, let cartCount = response!["cart_count"] as? Int {
                    print(totalPrice, cartCount)
                    self.view?.refreshCell(at: index, isAdded: true, cartCount: cartCount, totalPrice: Float(totalPrice))
                }
            } else {
                if let data = response!["message"] as? String {
                    self.view?.selectDOB()
                    //Singleton.sharedInstance.showAlert(data)
                }
            }
            
        }
    }
    
    //MARK: ERROR MESSAGE
    func showErrorMessage(error:String) {
        ErrorView.showWith(message: error, removed: nil)
    }

    func deleteProductFromCart(product: Products, view: ProductCellView, index: Int) {
        ActivityIndicator.sharedInstance.showActivityIndicator()
        product.deleteProduct { (success, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if success {
                view.updateValue(added: false)
                if let count = self.products[index].inCartCount {
                    self.products[index].inCartCount = count - 1
                }
                if let totalPrice = response!["total_price"] as? Double, let cartCount = response!["cart_count"] as? Int {
                    print(totalPrice, cartCount)
                    self.view?.refreshCell(at: index, isAdded: false, cartCount: cartCount, totalPrice: Float(totalPrice))
                }
            }
        }
    }
    
    func markFavUnfavProduct(view: ProductCellView, index: Int) {
        let product = self.products[index]
        ActivityIndicator.sharedInstance.showActivityIndicator()
        product.markFavUnfavApi { (success, response) in
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if success {
                if let status = self.products[index].is_fav, status == 1 {
                    self.products[index].is_fav = 0
                } else {
                    self.products[index].is_fav = 1
                }
                self.view?.reloadProductTable()
            }
        }
    }
    
}


////////////////////////////////////////////////////////////////////////////////////////////////////////
protocol SubCategoryBannerCellModel {
    var productImage: String {get}
}

struct SubCategoryBannerCellImplemention: SubCategoryBannerCellModel {
    let productImage: String
}

extension SubCategoryBannerCellImplemention {
    init(data: Category) {
        if let image = data.subCategoryImage {
            productImage = image
        } else {
            productImage = ""
        }
    }
}



protocol SubCategoryBannerCellView {
    func setupData(model: SubCategoryBannerCellModel)
}

class SubCategoryBannerCell: UICollectionViewCell, SubCategoryBannerCellView {
    @IBOutlet weak var bannerImageView: UIImageView!
    
    func setupData(model: SubCategoryBannerCellModel) {
        let imageUrl = URL.get(from: model.productImage)
//        self.bannerImageView.kf.setImage(with: imageUrl)
        self.bannerImageView.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "logoTookanSplashGraphic"))
    }
    
    
}


////////////////////////////////////////////////////////////////////////////////////////////////////////

protocol SubCategoryCellView {
    func setupData(subCategory: String, underlineViewHidden: Bool)
}


class SubCategoryCell: UICollectionViewCell, SubCategoryCellView {
    @IBOutlet weak var subCategoryLabel: UILabel!
    @IBOutlet weak var underlineView: UIImageView!
    
    func setupData(subCategory: String, underlineViewHidden: Bool) {
        self.subCategoryLabel.text = subCategory
        self.underlineView.isHidden = underlineViewHidden
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

class ProductListCollectionCell: UICollectionViewCell {
    @IBOutlet weak var productTableView: UITableView!
    
}

protocol ProductListCollectionView: class  {
    func reloadTable()
}

protocol ProductListPresenter {
    var noOfSections: Int {get}
    func noOfItemsInSection(section: Int) -> Int
    func identifierForSection(indexpath: IndexPath) -> String
    func setupData(view: SubCategoryBannerCellView, index: Int)
    
}

class ProductListImplementation: ProductListPresenter {
    var products = [Products]() {
        didSet {
            view?.reloadTable()
        }
    }
    weak var view: ProductListCollectionView?
    
    init(withView view: ProductListCollectionView, products: [Products]) {
        self.view = view
        self.products = products
    }

    var noOfSections: Int {
        return 1
    }
    
    func noOfItemsInSection(section: Int) -> Int {
        return products.count
    }
    
    func identifierForSection(indexpath: IndexPath) -> String {
        return "ProductCell"
    }
    
    func setupData(view: SubCategoryBannerCellView, index: Int) {
        
    }
}



