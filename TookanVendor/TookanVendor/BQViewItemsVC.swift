//
//  BQViewItemsVC.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 12/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQViewItemsVC: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    var navigationBar:NavigationView!
    var orderListDataModel: BQOrderListModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setNavigationBar()
        self.tblView.register(UINib(nibName: "ProductCell", bundle: nil), forCellReuseIdentifier: "ProductCell")
        
        self.tblView.rowHeight = UITableViewAutomaticDimension
        self.tblView.estimatedRowHeight = 200
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.reloadData()
    }
    
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        //Use for showing two right bar buttons on navigation bar
        
        navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "#\(orderListDataModel?.category_id ?? 0)", leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"), rightButtonImage: nil, secondRightButtonImage: nil)
            , leftButtonAction: {[weak self] in
                self?.backAction()
            }, rightButtonAction: nil,
               rightButtonSecondAction: nil
        )
        navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
    }
    
    // MARK: - IBAction
    func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
}


extension BQViewItemsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (orderListDataModel?.productListModel.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as? ProductCell else {
            return UITableViewCell()
        }
        
        let cellDataModel: BQProductListModel = (orderListDataModel?.productListModel[indexPath.row])!
        cell.leftProductDescription.isHidden = false
        cell.productDescription.isHidden = true
        cell.favBtn.isHidden = true
        cell.contentView.backgroundColor = .clear
        cell.stepperView.isHidden = true
        cell.productName.text = cellDataModel.product_name
        cell.productDescription.text = "\(cellDataModel.pack_size ?? "")" + "\(cellDataModel.measuring_unit ?? "")" //cellDataModel.product_description
        cell.leftProductDescription.text = "\(cellDataModel.pack_size ?? "")" + "\(cellDataModel.measuring_unit ?? "")" 
        cell.productPrice.text = "\(cellDataModel.quantity ?? 0) X " + "AED \(cellDataModel.discounted_price ?? 0)"
        cell.brandName.text = cellDataModel.brand_name
        cell.deleteButton.isHidden = true
        cell.discountImage.isHidden = true
        
        if let discount = cellDataModel.discount_percentage {
            if discount > 0 {
                cell.discountImage.isHidden = false
                cell.lblDiscount.isHidden = false
                cell.lblDiscount.text = "\(cellDataModel.discount_percentage ?? 0)%"
            } else {
                cell.discountImage.isHidden = true
                cell.lblDiscount.isHidden = true
            }
        }
        
        let imageUrl = URL.get(from: cellDataModel.product_image_url)
        cell.productImage.kf.setImage(with: imageUrl, placeholder: #imageLiteral(resourceName: "bawiqPlaceholde"))
        
        return cell
    }

}


