//
//  AddFamilyMemberViewController.swift
//  TookanVendor
//
//  Created by Vishal on 30/01/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class AddFamilyMemberViewController: UIViewController, UITextFieldDelegate, CountryPhoneCodePickerDelegate {
    
    @IBOutlet weak var nationalityTxtField: MKTextField!
    @IBOutlet weak var dobTxtField: MKTextField!
    @IBOutlet weak var genderTxtField: MKTextField!
    @IBOutlet weak var nameTextField: MKTextField!
    @IBOutlet weak var emailTextField: MKTextField!
  //  @IBOutlet weak var phoneTextField: MKTextField!
    @IBOutlet weak var aedTextField: MKTextField!
    @IBOutlet weak var sectiontitle: UILabel!
    @IBOutlet weak var skipButton: UIButton!
    
    var pickOption = ["Male", "Female", "Other"]
    var navigationBar: NavigationView!
    var presenter: AddFamilyPresenter!
    var editMember = false
    var vendorID = 0
    var data: FamilyMember?
    var hideSkip = false
    private var isAbove21: Bool = false
    
    @IBOutlet weak var below21Btn: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var phoneTextField: VSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar()
        presenterSetup()
        configureTextFields()
        showData()
        below21Btn.isUserInteractionEnabled = false
        if hideSkip {
            skipButton.isHidden = true
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case phoneTextField:
            phoneTextField.setFormatting("XXX-XXX-XXXX", replacementChar: "X")
            if textField.text == "" {
                //            if string == "0" {
                //                phoneField.text = string
                //            } else {
                //                phoneField.text = "0" + string
                //            }
                phoneTextField.text = "0" + string
                return false
            }
            return true
        default:
            break
        }
        return true
    }
    
    func showData() {
        if editMember {
            self.registerButton.setTitle("Update", for: .normal)
            sectiontitle.text = "Update Family Member"
            skipButton.isHidden = true
            guard let _data = data else {
                return
            }
            if _data.phoneNo.count < 10 {
                self.phoneTextField.setFormatting("XX-XXX-XXXX", replacementChar: "X")
            } else {
                self.phoneTextField.setFormatting("XXX-XXX-XXXX", replacementChar: "X")
            }
            self.phoneTextField.text = _data.phoneNo
            if let dob = _data.dob {
                self.dobTxtField.text = dob
            }
            if let gender = _data.gender {
                self.genderTxtField.text = gender
            }
            if let nationality = _data.nationality {
                self.nationalityTxtField.text = nationality
            }
            
            self.aedTextField.text = String(_data.walletLimit)
            self.nameTextField.text = _data.username
            self.emailTextField.text = _data.email
            if _data.isAbove21 == 1 {
                self.below21Btn.isSelected = true
            } else {
                self.below21Btn.isSelected = false
            }
            self.vendorID = _data.vendorId
        }
    }
    
    //Presenter setup
    private func presenterSetup() {
        self.presenter = AddFamilyPresenter(view: self)
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.bringSubview(toFront: skipButton)
    }
    
    func configureTextFields() {
        self.nameTextField.delegate = self
        self.emailTextField.delegate = self
        self.phoneTextField.delegate = self
        self.aedTextField.delegate = self
        self.genderTxtField.delegate = self
        
        self.nationalityTxtField.delegate = self
        let countryPicker = CountryPicker()
        countryPicker.countryPhoneCodeDelegate = self
        nationalityTxtField.inputView = countryPicker
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        genderTxtField.inputView = pickerView
        
        self.dobTxtField.delegate = self
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = UIDatePickerMode.date
        datePickerView.maximumDate = Date()
        dobTxtField.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(AddFamilyMemberViewController.datePickerValueChanged), for: UIControlEvents.valueChanged)
        self.genderTxtField.placeHolderColor = .white
        self.dobTxtField.placeHolderColor = .white
        self.nameTextField.placeHolderColor = .white
        self.emailTextField.placeHolderColor = .white
        self.nationalityTxtField.placeHolderColor = .white
        // self.phoneTextField.placeHolderColor = .white
//        self.phoneTextField.setFormatting("XXX-XXX-XXXX", replacementChar: "X")
        self.phoneTextField.attributedPlaceholder = NSAttributedString(string: "Phone Number e.g. (052-993-3645)",
                                                         attributes: [NSForegroundColorAttributeName: UIColor.white])
        self.aedTextField.placeHolderColor = .white
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        
        dateFormatter.timeStyle = DateFormatter.Style.none
        
        dobTxtField.text = dateFormatter.string(from: sender.date)
        let age = calcAge(birthday: dobTxtField.text!)
        if age < 21 {
            isAbove21 = true
            below21Btn.isSelected = isAbove21
        } else {
            isAbove21 = false
            below21Btn.isSelected = isAbove21
        }
        
    }
    
    func calcAge(birthday: String) -> Int {
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "MMM dd, yyyy"  // Jun 23, 2018
        if let birthdayDate = dateFormater.date(from: birthday) {
            if let calendar: NSCalendar = NSCalendar(calendarIdentifier: .gregorian) {
                let now = Date()
                let calcAge = calendar.components(.year, from: birthdayDate, to: now, options: [])
                if let age = calcAge.year {
                    return age
                }
            }
        }
        return 0
    }
    
    
    func countryPhoneCodePicker(picker: CountryPicker, didSelectCountryCountryWithName name: String, countryCode: String, phoneCode: String) {
        self.nationalityTxtField.text = name
    }
    
    //MARK: NAVIGATION BAR
    func setNavigationBar() {
        navigationBar = NavigationView.getNibFile(leftButtonAction: { [weak self] in
            self?.backAction()
            }, rightButtonAction: nil)
        navigationBar.setBackgroundColor(color: UIColor.clear, andTintColor: COLOR.SPLASH_TEXT_COLOR)
        navigationBar.bottomLine.isHidden = true
        self.view.addSubview(navigationBar)
    }
    
    //MARK: - IBAction
    func backAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func skipAction(_ sender: UIButton) {
        Singleton.sharedInstance.pushToHomeScreen() {
            
        }
    }
    @IBAction func below21Action(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.isAbove21 = sender.isSelected
    }
    
    @IBAction func addFamilyMemberAction(_ sender: UIButton) {
        self.checkValidation()
    }
    
    
    //MARK: VALIDATION CHECK
    func checkValidation() {
        var email = ""
        var name = ""
        let phone = self.phoneTextField.text
        let ammount = self.aedTextField.text
        let gender = self.genderTxtField.text ?? ""
        let dob = self.dobTxtField.text ?? ""
        let nationality = self.nationalityTxtField.text ?? ""
        
        if let _name = self.nameTextField.text {
            name = _name
        }
        
        if let _email = self.emailTextField.text?.trimText {
            email = _email
        }
        guard name.length > 0 else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_NAME)
            return
        }
        
        guard Singleton.sharedInstance.validateEmail(email) == true else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_EMAIL)
            return
        }
        
        guard let _phone = phone, Singleton.sharedInstance.validatePhoneNumber(phoneNumber: _phone), phoneNumberContainsCharacterOtherThanNumbers(number: _phone) == false else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_PHONE_NUMBER)
            return
        }
        
        guard let _ammount = ammount,let amm = Int(_ammount), amm > 0 else {
            self.showErrorMessage(error: ERROR_MESSAGE.INVALID_Ammount)
            return
        }
        
        if self.editMember {
            
            if self.vendorID == 0 {
                self.showErrorMessage(error: "ID not found")
                return
            }
            
            presenter.updateFamilyMember(email: email,
                                         phoneNo: phone!,
                                         username: name,
                                         vendorID: self.vendorID,
                                         isAbove21: self.isAbove21,
                                         gender: gender,
                                         dob: dob,
                                         nationality: nationality,
                                         walletLimit: ammount!) { (success, response) in
                                            if success
                                            {
                                                self.phoneTextField.text = ""
                                                self.aedTextField.text = ""
                                                self.nameTextField.text = ""
                                                self.emailTextField.text = ""
                                                self.genderTxtField.text = ""
                                                self.dobTxtField.text = ""
                                                self.nationalityTxtField.text = ""
                                                self.navigationController?.popViewController(animated: true)
                                            }
            }
        } else {
            presenter.addFamilyMember(email: email,
                                      phoneNo: phone!,
                                      username: name,
                                      walletLimit: ammount!,
                                      isAbove21: self.isAbove21,
                                      gender: gender,
                                      dob: dob,
                                      nationality: nationality) { (success, response) in
                                        if success
                                        {
                                            self.phoneTextField.text = ""
                                            self.aedTextField.text = ""
                                            self.nameTextField.text = ""
                                            self.emailTextField.text = ""
                                            self.genderTxtField.text = ""
                                            self.dobTxtField.text = ""
                                            self.nationalityTxtField.text = ""
                                            if self.hideSkip {
                                                self.navigationController?.popViewController(animated: true)
                                                return
                                            }
                                            if let vc = self.storyboard?.instantiateViewController(withIdentifier: "FamilyMemberListViewController") as? FamilyMemberListViewController {
                                                self.navigationController?.pushViewController(vc, animated: true)
                                            }
                                        } else {
                                            if let result = response as? [String: Any] {
                                                if let message = response["message"] as? String {
                                                    ErrorView.showWith(message: message, removed: nil)
                                                }
                                            }
                                            
                                        }
            }
        }        
    }
    
    func phoneNumberContainsCharacterOtherThanNumbers(number: String) -> Bool {
        return number.count != number.stripOutUnwantedCharacters(charactersYouWant: "0123456789+-").count
    }
    
    //MARK: ERROR MESSAGE
    func showErrorMessage(error:String) {
        ErrorView.showWith(message: error, removed: nil)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
}

extension AddFamilyMemberViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickOption[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        genderTxtField.text = pickOption[row]
    }
    
}

extension AddFamilyMemberViewController: AddFamilyProtocol {
    func receivedResponse() {
        
    }
    
    func showError(error: Error?) {
        
    }
}

//AddFamilyProtocol
protocol AddFamilyProtocol: class  {
    func receivedResponse()
    func showError(error:Error?)
}

//AddFamilyPresenter
class AddFamilyPresenter {
    
    weak private var view: AddFamilyProtocol?
    var familyMembers = [Vendor]()
    
    init(view: AddFamilyProtocol) {
        self.view = view
        
    }
    var numberOfRows: Int {
        return familyMembers.count
    }
    
    func updateFamilyMember(email: String, phoneNo: String, username: String,vendorID: Int, isAbove21: Bool = false, gender: String, dob: String , nationality: String, walletLimit: String, _ receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        var params = [
            "email": email,
//            "phone_no": phoneNo,
            "username": username,
            "wallet_limit": walletLimit,
            "user_id": vendorID,
            "app_access_token": Vendor.current!.appAccessToken!] as [String : Any]
        if phoneNo.first == "0" {
            params["phone_no"] = phoneNo.dropFirst()
        } else {
            params["phone_no"] = phoneNo
        }
        
        if gender != "" {
            params["gender"] = gender
        }
        
        if dob != "" {
            params["dob"] = dob
        }
        
        if nationality != "" {
            params["nationality"] = nationality
        }
        
        if isAbove21 {
            params["is_above_21"] = "1"
        } else {
            params["is_above_21"] = "0"
        }
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.updateFamilyMember,
                                                         params: params as [String : AnyObject],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                            DispatchQueue.main.async {
                                                                print(response)
                                                                if succeeded {
                                                                    receivedResponse(true, response)
                                                                } else {
                                                                    receivedResponse(false, ["message": ERROR_MESSAGE.SERVER_NOT_RESPONDING])
                                                                }
                                                            }
        }
    }
    func addFamilyMember(email: String, phoneNo: String, username: String, walletLimit: String, isAbove21: Bool = false, gender: String, dob: String, nationality: String,  _ receivedResponse:@escaping (_ succeeded:Bool, _ response:[String:Any]) -> ()) {
        var params = [
            "email": email,
//            "phone_no": phoneNo,
            "username": username,
            "wallet_limit": walletLimit,
            "app_access_token": Vendor.current!.appAccessToken!] as [String : Any]
        if phoneNo.first == "0" {
            params["phone_no"] = phoneNo.dropFirst()
        } else {
            params["phone_no"] = phoneNo
        }
        
        if gender != "" {
            params["gender"] = gender
        }
        
        if dob != "" {
            params["dob"] = dob
        }
        
        if nationality != "" {
            params["nationality"] = nationality
        }
        
        if isAbove21 {
            params["is_above_21"] = "1"
        } else {
            params["is_above_21"] = "0"
        }
        
        NetworkingHelper.sharedInstance.commonServerCall(apiName: API_NAME.addFamilyMember,
                                                         params: params as [String : AnyObject],
                                                         httpMethod: "POST") { (succeeded, response) in
                                                            DispatchQueue.main.async {
                                                                print(response)
                                                                if succeeded {
                                                                    receivedResponse(true, response)
                                                                } else {
                                                                    receivedResponse(false, response)
                                                                }
                                                            }
        }
    }
    
}
