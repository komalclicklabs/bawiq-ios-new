//
//  PaymentMethodTableViewCell.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 27/02/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class PaymentMethodTableViewCell: UITableViewCell {

    @IBOutlet weak var addImage: UIImageView!
    @IBOutlet weak var RightLabel: UILabel!
    @IBOutlet weak var paymentNameLabel: UILabel!
    @IBOutlet weak var paymentTypeLogo: UIImageView!
    @IBOutlet weak var viewForShadow: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setFont()
        showShadow()
        self.backgroundColor  = COLOR.SPLASH_BACKGROUND_COLOR
        self.viewForShadow.backgroundColor = COLOR.popUpColor
        RightLabel.textColor = COLOR.SPLASH_TEXT_COLOR
        self.selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
      
        // Configure the view for the selected state
    }
    
    func getData(showRightImage:Bool = true,rightImageLabel : String = "" , leftString : String = "",rightImage:UIImage,showImage : Bool = true){
         addImage.isHidden = !showRightImage
         RightLabel.isHidden = showRightImage
         paymentNameLabel.isHidden = !showRightImage
         paymentTypeLogo.isHidden = showRightImage
         showShadow()
         RightLabel.text = rightImageLabel
         paymentNameLabel.text = leftString
         paymentTypeLogo.image = rightImage
        
    }
    
    
    func showShadow(){
        viewForShadow.layer.shadowColor = UIColor.black.cgColor
        viewForShadow.layer.shadowOpacity = 0.3
        viewForShadow.layer.shadowOffset = CGSize(width: 0, height: 2)
        viewForShadow.layer.shadowRadius = 0.7
        viewForShadow.layer.cornerRadius = 4.2
    }
    
    func setFont(){
        self.RightLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
        self.paymentNameLabel.font = UIFont(name: FONT.light, size: FONT_SIZE.buttonTitle)
    }

}
