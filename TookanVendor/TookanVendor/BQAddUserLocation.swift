//
//  BQAddUserLocation.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 07/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces


class BQAddUserLocation: UIViewController, GMSMapViewDelegate {
    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var btnWork: UIButton!
    @IBOutlet weak var btnOther: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnSave: UIButton!
    
    
    var addressPart1 = ""
    var addressPart2 = ""
    var addressPart3 = ""
    var landmark = ""
    var buildingNumber = ""
    var floorNumber = ""
    var doorNumber = ""
    var streetNumber = ""
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var response: GMSReverseGeocodeResponse?
    var locType = 0
    var navigationBar:NavigationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerTableViewCells()
        self.setNavigationBar()
        self.giveUiAttributes()
//        self.setHomeButtonUI()
        
        if self.locType == 0 {
            //Home Button click
            self.setHomeButtonUI()
            
        } else if self.locType == 1 {
            //Work button click
            self.setWorkButtonUI()
        } else {
            //Other button click
            self.setOtherButtonUI()
            
        }
        
        self.tblView.tableFooterView = UIView()

        if  response != nil {
            if (response?.results()?.count)! > 0 {
                guard let thoroughfare = response?.results()?[0].thoroughfare else {
                    return
                }
                addressPart1 = thoroughfare // (response?.results()?[0].thoroughfare)!
                guard let locality = response?.results()?[0].locality, let country = response?.results()?[0].country else {
                    return
                }
                addressPart2 = locality + ", " + country
                if let subLocality = response?.results()?[0].subLocality {
                    addressPart2 = subLocality + ", " + addressPart2
                }
                
                if let postalCode = response?.results()?[0].postalCode {
                    addressPart3 = postalCode
                }
                
                if let lat = response?.results()?[0].coordinate.latitude,
                    let long = response?.results()?[0].coordinate.longitude {
                    latitude = lat
                    longitude = long
                    
                }
                tblView.reloadData()
            }
        }
    }
    
    
    private func registerTableViewCells() {
        tblView.registerCellWith(nibName: NIB_NAME.addressListCell, reuseIdentifier: CELL_IDENTIFIER.addressCellIdentifier)
    }
    
    func giveUiAttributes() {
        
        //Home Button
        btnHome.layer.cornerRadius = 15.0
        btnHome.layer.borderColor = UIColor.white.cgColor
        btnHome.layer.borderWidth = 1.0
        btnHome.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
        btnHome.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        btnHome.setTitleColor(COLOR.LOGIN_BUTTON_TITLE_COLOR, for: UIControlState.normal)
        btnHome.setTitle("Home", for: UIControlState.normal)
        
        //Work button
        btnWork.layer.cornerRadius = 15.0
        btnWork.layer.borderColor = UIColor.white.cgColor
        btnWork.layer.borderWidth = 1.0
        btnWork.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
        btnWork.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        btnWork.setTitleColor(COLOR.LOGIN_BUTTON_TITLE_COLOR, for: UIControlState.normal)
        btnWork.setTitle("Work", for: UIControlState.normal)
        
        //Other Button
        btnOther.layer.cornerRadius = 15.0
        btnOther.layer.borderColor = UIColor.white.cgColor
        btnOther.layer.borderWidth = 1.0
        btnOther.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
        btnOther.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.small)
        btnOther.setTitleColor(COLOR.LOGIN_BUTTON_TITLE_COLOR, for: UIControlState.normal)
        btnOther.setTitle("Other", for: UIControlState.normal)
        
        //Save Button
        btnSave.layer.cornerRadius = 20.0
        btnSave.backgroundColor = .white
        btnSave.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.large)
        btnSave.setTitleColor(COLOR.App_Red_COLOR, for: UIControlState.normal)
        btnSave.setTitle("Save", for: UIControlState.normal)
        
    }
    
    @IBAction func selectAddressbuttonPressed(_ sender: UIButton) {
        locType = sender.tag
        
        if sender.tag == 0 {
            //Home Button click
            self.setHomeButtonUI()
            
        } else if sender.tag == 1 {
            //Work button click
            self.setWorkButtonUI()
        } else {
           //Other button click
            self.setOtherButtonUI()
            
        }
    }
    
    private func setHomeButtonUI() {
        btnHome.isSelected = true
        btnHome.backgroundColor = .white
        
        btnWork.backgroundColor = .clear
        btnWork.isSelected = false
        
        btnOther.backgroundColor = .clear
        btnOther.isSelected = false
    }
    
    private func setWorkButtonUI() {
        btnHome.isSelected = false
        btnHome.backgroundColor = .clear
        
        btnWork.isSelected = true
        btnWork.backgroundColor = .white
        
        btnOther.backgroundColor = .clear
        btnOther.isSelected = false
    }
    
    private func setOtherButtonUI() {
        btnHome.isSelected = false
        btnHome.backgroundColor = .clear
        
        btnWork.isSelected = false
        btnWork.backgroundColor = .clear
        
        btnOther.backgroundColor = .white
        btnOther.isSelected = true
    }
    
    
    @IBAction func savebuttonPressed(_ sender: Any) {
        if self.landmark.isEmpty {
            Singleton.sharedInstance.showAlert("Please add your landmark")
            return
        }
        
        var params = getParamsToDeleteLocation()
        if !self.buildingNumber.isEmpty {
            params["building_number"] = self.buildingNumber
        }
        if !self.floorNumber.isEmpty {
            params["floor_number"] = self.floorNumber
        }
        if !self.doorNumber.isEmpty {
            params["door_number"] = self.doorNumber
        }
        if !self.streetNumber.isEmpty {
            params["street_number"] = self.streetNumber
        }
        
        print("Add address params: ", params)
        
        ActivityIndicator.sharedInstance.showActivityIndicator()
        HTTPClient.makeConcurrentConnectionWith(method: .POST, para: params, extendedUrl: "add_update_fav_location") { (responseObject, error, _, statusCode) in
            
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            
            guard statusCode == .some(STATUS_CODES.SHOW_DATA) else {
                return
            }
            
            print(responseObject ?? "")
            if let dataRes = responseObject as? [String:Any] {
                if let data = dataRes["data"] as? [String:Any] {
                    print(data)
                    let viewControllers: [UIViewController] = self.navigationController!.viewControllers
                    var sideMenuVC: UIViewController?
                    for i in 0 ..< viewControllers.count {
                        let vc = viewControllers[i]
                        if vc is BQUserSavedLocationsVC {
                            sideMenuVC = nil
                            self.navigationController!.popToViewController(vc, animated: true)
                            return
                        } else if vc is SSASideMenu {
                            sideMenuVC = vc
                        }
                    }
                    if let vc = sideMenuVC {
                        self.navigationController!.popToViewController(vc, animated: true)
                        return
                    }
                    
                    
//                    for aViewController in viewControllers {
//                        if aViewController is SSASideMenu {
//                            self.navigationController!.popToViewController(aViewController, animated: true)
//                        }
//                    }
                    
                    
//                    self.navigationController?.popViewController(animated: true)
                    
//                    if let vc = UIStoryboard(name: "Booking", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQUserSavedLocationsVC") as? BQUserSavedLocationsVC {
//                        vc.isComingFromHome = true
//                        self.navigationController?.pushViewController(vc, animated: true)
//                    }
                    
                }
            }
        }
        
    }
    
    
    // MARK: - Methods
    
    private func getParamsToDeleteLocation() -> [String: Any] {
        return [
            "app_access_token": Vendor.current!.appAccessToken!,
            "address": self.addressPart1 + " " + self.addressPart2,
            "latitude": self.latitude, //response?.results()?[0].coordinate.latitude as Any,
            "longitude" : self.longitude, //response?.results()?[0].coordinate.longitude as Any,
            "locType" : self.locType,
            "landmark": self.landmark, //self.addressPart1,
            "postal_code": self.addressPart3,
        ]
    }
    
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        //Use for showing two right bar buttons on navigation bar
        
        navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "Location", leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"), rightButtonImage: nil, secondRightButtonImage: nil)
        , leftButtonAction: {[weak self] in
        self?.backAction()
        }, rightButtonAction: nil,
           rightButtonSecondAction: nil
        )
        navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
    }
    
    // MARK: - IBAction
    func backAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
}

extension BQAddUserLocation: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let addressCell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.addressCellIdentifier) as? AddressListCell else {
            fatalError("Cell not found")
        }
        addressCell.delegate = self
        addressCell.addressLabel.isHidden = true
        addressCell.selectionStyle = .none
        addressCell.backgroundColor = .clear
        addressCell.contentView.backgroundColor = .clear
//        addressCell.addressLabel.textColor = .white
        addressCell.addressTxtField.textColor = .white
        addressCell.bgView.backgroundColor = .clear
    
        if indexPath.row == 0 {
            addressCell.addressTxtField.isUserInteractionEnabled = true
            addressCell.addressTxtField.text = self.buildingNumber
            let attributes = [
                NSForegroundColorAttributeName: UIColor.white,
                ]
            addressCell.addressTxtField.attributedPlaceholder = NSAttributedString(string: "Building Number", attributes:attributes)
            // addressCell.addressLabel.text = addressPart2
            addressCell.imageIcon.image = #imageLiteral(resourceName: "homeBold")
        } else if indexPath.row == 1 {
            addressCell.addressTxtField.isUserInteractionEnabled = true
            addressCell.addressTxtField.text = self.floorNumber
            let attributes = [
                NSForegroundColorAttributeName: UIColor.white,
                ]
            addressCell.addressTxtField.attributedPlaceholder = NSAttributedString(string: "Floor Number", attributes:attributes)
            // addressCell.addressLabel.text = addressPart2
            addressCell.imageIcon.image = #imageLiteral(resourceName: "homeBold")
        } else if indexPath.row == 2 {
            addressCell.addressTxtField.isUserInteractionEnabled = true
            addressCell.addressTxtField.text = self.doorNumber
            let attributes = [
                NSForegroundColorAttributeName: UIColor.white,
                ]
            addressCell.addressTxtField.attributedPlaceholder = NSAttributedString(string: "Door Number", attributes:attributes)
            // addressCell.addressLabel.text = addressPart2
            addressCell.imageIcon.image = #imageLiteral(resourceName: "homeBold")
        } else if indexPath.row == 3 {
            addressCell.addressTxtField.isUserInteractionEnabled = true
            addressCell.addressTxtField.text = self.streetNumber
            let attributes = [
                NSForegroundColorAttributeName: UIColor.white,
                ]
            addressCell.addressTxtField.attributedPlaceholder = NSAttributedString(string: "Street Number", attributes:attributes)
            // addressCell.addressLabel.text = addressPart2
            addressCell.imageIcon.image = #imageLiteral(resourceName: "homeBold")
        } else if indexPath.row == 4 {
           // addressCell.addressLabel.text = addressPart1
            addressCell.addressTxtField.isUserInteractionEnabled = false
            let attributes = [
                NSForegroundColorAttributeName: UIColor.white,
                ]
            addressCell.addressTxtField.attributedPlaceholder = NSAttributedString(string: "Address", attributes:attributes)
            addressCell.addressTxtField.text = addressPart1 + " " + addressPart2
            addressCell.imageIcon.image = #imageLiteral(resourceName: "location")
            
        } else if indexPath.row == 5 {
           addressCell.addressTxtField.isUserInteractionEnabled = true
            addressCell.addressTxtField.text = self.landmark
            let attributes = [
                NSForegroundColorAttributeName: UIColor.white,
            ]
            addressCell.addressTxtField.attributedPlaceholder = NSAttributedString(string: "Landmark", attributes:attributes)
           // addressCell.addressLabel.text = addressPart2
             addressCell.imageIcon.image = #imageLiteral(resourceName: "homeBold")
        } else {
            let attributes = [
                NSForegroundColorAttributeName: UIColor.white,
                ]
            addressCell.addressTxtField.attributedPlaceholder = NSAttributedString(string: "Zipcode", attributes:attributes)
            addressCell.addressTxtField.isUserInteractionEnabled = true
            addressCell.addressTxtField.text = addressPart3
           //  addressCell.addressLabel.text = addressPart3
            addressCell.imageIcon.image = #imageLiteral(resourceName: "pincode")
        }
        
        
        return addressCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 4 {
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            present(autocompleteController, animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
}

extension BQAddUserLocation: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let address = place.formattedAddress ?? ""
        self.addressPart1 = address
        self.tblView.reloadData()
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}


extension BQAddUserLocation: AddressCellDelegate {
    func shouldChange(text: String, string: String, cell: UITableViewCell) -> Bool {
        guard let index = self.tblView.indexPath(for: cell) else {
            return false
        }
        if index.row == 0 {
            let stringWithoutWhiteSpace = text.trimmingCharacters(in: .whitespaces)
            self.buildingNumber = stringWithoutWhiteSpace
        } else if index.row == 1 {
            let stringWithoutWhiteSpace = text.trimmingCharacters(in: .whitespaces)
            self.floorNumber = stringWithoutWhiteSpace
        } else if index.row == 2 {
            let stringWithoutWhiteSpace = text.trimmingCharacters(in: .whitespaces)
            self.doorNumber = stringWithoutWhiteSpace
        } else if index.row == 3 {
            let stringWithoutWhiteSpace = text.trimmingCharacters(in: .whitespaces)
            self.streetNumber = stringWithoutWhiteSpace
        } else if index.row == 5 {
            let stringWithoutWhiteSpace = text.trimmingCharacters(in: .whitespaces)
            self.landmark = stringWithoutWhiteSpace
        } else if index.row == 6 {
            let stringWithoutWhiteSpace = text.trimmingCharacters(in: .whitespaces)
            self.addressPart3 = stringWithoutWhiteSpace
        }
        
        return true
    }
}
