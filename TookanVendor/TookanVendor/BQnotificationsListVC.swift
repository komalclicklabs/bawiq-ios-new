//
//  BQnotificationsListVC.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 21/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit

class BQnotificationsListVC: UIViewController {
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var noNotificationLbl: UILabel!
    
    var arrSavedAddress = [BQNotificationData]()
    var isPullToRefreshActive: Bool = false
    var refreshControl = UIRefreshControl()
    private var skip: Int = 0
    private var limit: Int = 10
    var isPaginationRequired: Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.noNotificationLbl.isHidden = true
        self.setNavigationBar()
        self.setupTableView()
        self.addPulltoRefresh()
    }
    
    private func setupTableView() {
        self.tblView.delegate = self
        self.tblView.dataSource = self
        self.tblView.register(UINib(nibName: "BQNotificationListCell", bundle: nil), forCellReuseIdentifier: "BQNotificationListCell")
        self.tblView.rowHeight = 94
        self.tblView.estimatedRowHeight = UITableViewAutomaticDimension
        self.getContactUsDetails()
        
    }
    private func addPulltoRefresh() {
        //Add pullto refresh
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = .white
        refreshControl.tintColor = UIColor.gray
        refreshControl.addTarget(self, action: #selector(self.pullToRefresh), for: UIControlEvents.valueChanged)
        self.tblView.addSubview(refreshControl)
    }
    @objc func pullToRefresh() {
        isPullToRefreshActive = true
        self.arrSavedAddress.removeAll()
        self.reset()
        self.getContactUsDetails()
        
    }
    
    func reset() {
        skip = 0
        limit = 10
    }
    
    func paginationHit() {
        if self.isPaginationRequired == true {
            skip += 10
            self.getContactUsDetails(isFromPagination: true)
        }
    }
    
    private func getContactUsDetails(isFromPagination: Bool = false) {
        
        let params = getParamsToDeleteLocation()
        print("Get notification params:", params)
        if isPullToRefreshActive == true {
            isPullToRefreshActive = false
            ActivityIndicator.sharedInstance.showActivityIndicator()
        }
        HTTPClient.makeConcurrentConnectionWith(method: .POST, para: params, extendedUrl: "get_vendor_notifications") { (responseObject, error, _, statusCode) in
            
            ActivityIndicator.sharedInstance.hideActivityIndicator()
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
            guard statusCode == .some(STATUS_CODES.SHOW_DATA) else {
                return
            }
            print("get_vendor_notifications: \(responseObject ?? "")")
            if let dataRes = responseObject as? [String:Any] {
                if let data = dataRes["data"] as? [String: Any], let contactUS = data["notifications"] as? [[String: Any]] {
                    
                    print(data)
                    var tempArray = [BQNotificationData]()
                    for item in contactUS {
                        let notificationData = BQNotificationData.init(param: item)
                        tempArray.append(notificationData)
                     //   self.arrSavedAddress.append(notificationData)
                    }
                    
                    if let totalCount = data["total_count"] as? Int, tempArray.count > 0 {
                        self.isPaginationRequired =  totalCount > self.arrSavedAddress.count
                        if !self.isPaginationRequired {
                            self.arrSavedAddress = tempArray
                        } else {
                            self.arrSavedAddress += tempArray
                        }
                    }
            
                    self.tblView.reloadData()
                }
            }
        }
    }
    private func getParamsToDeleteLocation() -> [String: Any] {
        return [
            "app_access_token": Vendor.current!.appAccessToken!,
            "limit": limit,
            "skip": skip
        ]
    }
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        //Use for showing two right bar buttons on navigation bar
        
        let navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "My Notifications", leftButtonImage: #imageLiteral(resourceName: "menu"), rightButtonImage: nil, secondRightButtonImage: nil)
            , leftButtonAction: {[weak self] in
                self?.presentLeftMenuViewController()
            }, rightButtonAction: nil,
               rightButtonSecondAction: nil
        )
        navigationBar.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
        
    }
}

extension BQnotificationsListVC: UITableViewDataSource, UITableViewDelegate {
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension//94.0
//    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrSavedAddress.count > 0 {
            self.noNotificationLbl.isHidden = true
            return arrSavedAddress.count
        } else {
            self.noNotificationLbl.isHidden = false
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BQNotificationListCell", for: indexPath) as? BQNotificationListCell else {
            return UITableViewCell()
        }
        if let cellDataModel: BQNotificationData = self.arrSavedAddress[indexPath.row] {
            cell.lblTitle.text = cellDataModel.notification_title
            cell.lblDescription.text = cellDataModel.notification_text
            cell.lblDate.text = cellDataModel.creation_datetime
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cellDataModel: BQNotificationData = self.arrSavedAddress[indexPath.row] {
            if let vc = UIStoryboard(name: "MyOrders", bundle: Bundle.main).instantiateViewController(withIdentifier: "BQOrderDetailsVC") as? BQOrderDetailsVC {
                if cellDataModel.job_id != nil {
                    vc.orderID = cellDataModel.job_id
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if (tblView.contentOffset.y + tblView.frame.size.height) >= tblView.contentSize.height - 20 {
            self.paginationHit()
        }
    }
    
}

