//
//  CurrentTaskCell.swift
//  TookanVendor
//
//  Created by cl-macmini-45 on 22/12/16.
//  Copyright © 2016 clicklabs. All rights reserved.
//

import UIKit

class CurrentTaskCell: UITableViewCell {

    @IBOutlet var firstBigDot: UIImageView!
    @IBOutlet var trackingLine: UILabel!
    @IBOutlet var lastBigDot: UIImageView!
    @IBOutlet var pickupDeliveryTag: UIButton!
    @IBOutlet var secondViewPickupDeliveryTag: UIButton!
    
    @IBOutlet var parentView: UIView!
    @IBOutlet var firstCard: UIView!
    @IBOutlet var secondCard: UIView!
   
    @IBOutlet var firstCardTimeLabel: UILabel!
    @IBOutlet var firstCardStatus: UILabel!
    @IBOutlet var firstCardTimeText: UILabel!
    @IBOutlet var firstCardJobTypeHeader: UILabel!
    @IBOutlet var firstCardTaskIdHeader: UILabel!
    @IBOutlet var firstCardJobType: UILabel!
    @IBOutlet var firstCardTaskId: UILabel!
    @IBOutlet var firstCardAddressHeader: UILabel!
    @IBOutlet var firstCardAddress: UILabel!
    @IBOutlet var firstCardTrackingButton: UIButton!
    @IBOutlet var firstCardSeperatorLine: UIView!
    
    @IBOutlet var secondCardTimeHeader: UILabel!
    @IBOutlet var secondCardStatus: UILabel!
    @IBOutlet var secondCardTime: UILabel!
    @IBOutlet var secondCardJobTypeHeader: UILabel!
    @IBOutlet var secondCardTaskIdHeader: UILabel!
    @IBOutlet var secondCardJobType: UILabel!
    @IBOutlet var secondCardTaskId: UILabel!
    @IBOutlet var secondCardAddressHeader: UILabel!
    @IBOutlet var secondCardAddress: UILabel!
    @IBOutlet var secondCardTrackingButton: UIButton!
    @IBOutlet var secondCardSeperatorLine: UIView!
    
    @IBOutlet var leadingConstraint: NSLayoutConstraint!
    @IBOutlet var firstCardButtonHeightConstraint: NSLayoutConstraint!
    @IBOutlet var firstCardStatusHeightConstraint: NSLayoutConstraint!
    @IBOutlet var firstCardTopConstraint: NSLayoutConstraint!
    @IBOutlet var firstCardButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet var secondCardTopConstraint: NSLayoutConstraint!
    
    
    let pickupDeliveryTagWidth:CGFloat = 22.0
    let trackingButtonHeight:CGFloat = 45.0
    let statusCardHeight:CGFloat = 14.0
    let firstCardTopMargin:CGFloat = 14.0
    let firstCardButtonTopMargin:CGFloat = 11.8
    let secondCardTopValueWithoutFirstCard:CGFloat = -33.2
    let leftMarginWithDot:CGFloat = 45.0
    let leftMarginWithoutDot:CGFloat = 15.0
    

    override func awakeFromNib() {
        super.awakeFromNib()
        self.trackingLine.backgroundColor = UIColor(patternImage:#imageLiteral(resourceName: "iconPattern"))
        /*----------------- Parent View --------------*/
        self.parentView.backgroundColor = UIColor.white
        self.parentView.layer.cornerRadius = 4.0
        self.parentView.layer.shadowOffset = CGSize(width: 2, height: 3)
        self.parentView.layer.shadowOpacity = 0.7
        self.parentView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5).cgColor
        /*------------ First Card ---------------*/
        /*-------------- pickup/delivery tag ----------*/
        self.pickupDeliveryTag.setTitleColor(COLOR.SPLASH_TEXT_COLOR, for: UIControlState.normal)
        self.pickupDeliveryTag.backgroundColor = COLOR.REVIEW_BACKGROUND_COLOR
        self.pickupDeliveryTag.titleLabel?.font = UIFont(name: FONT.regular, size: 9.0)
        self.pickupDeliveryTag.layer.cornerRadius = pickupDeliveryTagWidth/2
        self.pickupDeliveryTag.layer.borderColor = COLOR.SPLASH_TEXT_COLOR.cgColor
        self.pickupDeliveryTag.layer.borderWidth = 1.0
        self.pickupDeliveryTag.isUserInteractionEnabled = false
        /*-------------- Header Label ------------------*/
        self.firstCardTimeLabel.textColor = COLOR.PLACEHOLDER_COLOR
        self.firstCardTimeLabel.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        self.firstCardTimeLabel.text = TEXT.DATE_TIME
        
        self.firstCardJobTypeHeader.textColor = COLOR.PLACEHOLDER_COLOR
        self.firstCardJobTypeHeader.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        self.firstCardJobTypeHeader.text = TEXT.TYPE
        
        self.firstCardTaskIdHeader.textColor = COLOR.PLACEHOLDER_COLOR
        self.firstCardTaskIdHeader.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        self.firstCardTaskIdHeader.text = Singleton.sharedInstance.formDetailsInfo.call_tasks_as + " " + "ID"
        
        self.firstCardAddressHeader.textColor = COLOR.PLACEHOLDER_COLOR
        self.firstCardAddressHeader.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        self.firstCardAddressHeader.text = TEXT.ADDRESS_PLACEHODLER
        
        /*-------------- Text Label ------------------*/
        self.firstCardTimeText.textColor = COLOR.SPLASH_TEXT_COLOR
        self.firstCardTimeText.font = UIFont(name: FONT.bold, size: FONT_SIZE.large)
        self.firstCardTimeText.text = TEXT.NO_DATETIME
        
        self.firstCardJobType.textColor = COLOR.SPLASH_TEXT_COLOR
        self.firstCardJobType.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
        self.firstCardJobType.text = TEXT.TYPE
        
        self.firstCardTaskId.textColor = COLOR.SPLASH_TEXT_COLOR
        self.firstCardTaskId.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
        self.firstCardTaskId.text = "######"
        
        self.firstCardAddress.textColor = COLOR.SPLASH_TEXT_COLOR
        self.firstCardAddress.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
        self.firstCardAddress.text = TEXT.NO_ADDRESS
        /*-------------- Status -----------------*/
        self.firstCardStatus.textColor = UIColor.white
        self.firstCardStatus.layer.masksToBounds = true
        self.firstCardStatus.layer.cornerRadius = 3.0
        self.firstCardStatus.font = UIFont(name: FONT.regular, size: 9.0)
        /*--------------- Tracking Button --------------*/
        self.firstCardTrackingButton.setImage(#imageLiteral(resourceName: "iconTrackOrder"), for: UIControlState.normal)
        self.firstCardTrackingButton.setTitle(TEXT.TRACK + " " + Singleton.sharedInstance.formDetailsInfo.call_tasks_as, for: .normal)
        self.firstCardTrackingButton.backgroundColor = COLOR.REVIEW_BACKGROUND_COLOR
        self.firstCardTrackingButton.setTitleColor(COLOR.SPLASH_TEXT_COLOR, for: .normal)
        
        /*--------------- Line ---------------------*/
        self.firstCardSeperatorLine.backgroundColor = COLOR.SPLASH_LINE_COLOR
        
        /*----------------------------------------------------------------------------*/
        /*------------------######## Second Card #########----------------------------*/

        /*-------------- pickup/delivery tag ----------*/
        self.secondViewPickupDeliveryTag.setTitleColor(COLOR.SPLASH_TEXT_COLOR, for: UIControlState.normal)
        self.secondViewPickupDeliveryTag.titleLabel?.font = UIFont(name: FONT.regular, size: 9.0)
        self.secondViewPickupDeliveryTag.layer.cornerRadius = pickupDeliveryTagWidth/2
        self.secondViewPickupDeliveryTag.layer.borderColor = COLOR.SPLASH_TEXT_COLOR.cgColor
        self.secondViewPickupDeliveryTag.layer.borderWidth = 1.0
        self.secondViewPickupDeliveryTag.isUserInteractionEnabled = false
        self.secondViewPickupDeliveryTag.backgroundColor = COLOR.REVIEW_BACKGROUND_COLOR
        /*-------------- Header Label ------------------*/
        self.secondCardTimeHeader.textColor = COLOR.PLACEHOLDER_COLOR
        self.secondCardTimeHeader.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        self.secondCardTimeHeader.text = TEXT.DATE_TIME
        
        self.secondCardJobTypeHeader.textColor = COLOR.PLACEHOLDER_COLOR
        self.secondCardJobTypeHeader.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        self.secondCardJobTypeHeader.text = TEXT.TYPE
        
        self.secondCardTaskIdHeader.textColor = COLOR.PLACEHOLDER_COLOR
        self.secondCardTaskIdHeader.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        self.secondCardTaskIdHeader.text = Singleton.sharedInstance.formDetailsInfo.call_tasks_as + " " + "ID"
        
        self.secondCardAddressHeader.textColor = COLOR.PLACEHOLDER_COLOR
        self.secondCardAddressHeader.font = UIFont(name: FONT.regular, size: FONT_SIZE.smallest)
        self.secondCardAddressHeader.text = TEXT.ADDRESS_PLACEHODLER
        
        /*-------------- Text Label ------------------*/
        self.secondCardTime.textColor = COLOR.SPLASH_TEXT_COLOR
        self.secondCardTime.font = UIFont(name: FONT.bold, size: FONT_SIZE.large)
        self.secondCardTime.text = TEXT.NO_DATETIME
        
        self.secondCardJobType.textColor = COLOR.SPLASH_TEXT_COLOR
        self.secondCardJobType.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
        self.secondCardJobType.text = TEXT.TYPE
        
        self.secondCardTaskId.textColor = COLOR.SPLASH_TEXT_COLOR
        self.secondCardTaskId.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
        self.secondCardTaskId.text = "######"
        
        self.secondCardAddress.textColor = COLOR.SPLASH_TEXT_COLOR
        self.secondCardAddress.font = UIFont(name: FONT.light, size: FONT_SIZE.medium)
        self.secondCardAddress.text = TEXT.NO_ADDRESS
        /*-------------- Status -----------------*/
        self.secondCardStatus.textColor = UIColor.white
        self.secondCardStatus.layer.masksToBounds = true
        self.secondCardStatus.layer.cornerRadius = 3.0
        self.secondCardStatus.font = UIFont(name: FONT.regular, size: 9.0)
        /*--------------- Tracking Button --------------*/
        self.secondCardTrackingButton.setImage(#imageLiteral(resourceName: "iconTrackOrder"), for: UIControlState.normal)
        self.secondCardTrackingButton.setTitle(TEXT.TRACK + " " + Singleton.sharedInstance.formDetailsInfo.call_tasks_as, for: .normal)
        self.secondCardTrackingButton.backgroundColor = COLOR.REVIEW_BACKGROUND_COLOR
        self.secondCardTrackingButton.setTitleColor(COLOR.SPLASH_TEXT_COLOR, for: .normal)
        
        /*--------------- Line ---------------------*/
        self.secondCardSeperatorLine.backgroundColor = COLOR.SPLASH_LINE_COLOR
        self.secondCardSeperatorLine.isHidden = true
    }
    
    func setTwoTaskCard(orderHistory:OrderHistory) {
        self.resetConstraint()
        self.setDefaultText()
        
        for i in 0..<orderHistory.tasksOfOrder.count {
            switch orderHistory.tasksOfOrder[i].job_type {
            case JOB_TYPE.pickup:
                self.firstCardAddress.text = orderHistory.tasksOfOrder[i].job_pickup_address.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.firstCardTaskId.text = "#"+orderHistory.tasksOfOrder[i].job_id
                self.firstCardTimeText.text = orderHistory.tasksOfOrder[i].job_pickup_datetime.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd'T'HH:mm:ss.000Z", output: "hh:mm a, dd MMM yyyy")
                let temp = orderHistory.tasksOfOrder[i].job_status!.getTextAndColor(vertical: orderHistory.tasksOfOrder[i].vertical)
                self.firstCardStatus.text = temp.text
                self.firstCardStatus.backgroundColor = temp.color
                if orderHistory.tasksOfOrder[i].vertical == .taxi {
                self.pickupDeliveryTag.setTitle("T", for: .normal)
                self.firstCardJobType.text = TEXT.Taxi
                }else if orderHistory.tasksOfOrder[i].vertical == .multipleCategory {
                self.pickupDeliveryTag.setTitle("L", for: .normal)
                self.firstCardJobType.text = TEXT.Laundry
                }else{
                    self.pickupDeliveryTag.setTitle("P", for: .normal)
                    self.firstCardJobType.text = TEXT.PICKUP
                }
                
            default:
                self.secondCardAddress.text = orderHistory.tasksOfOrder[i].job_address.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                self.secondCardTaskId.text = "#"+orderHistory.tasksOfOrder[i].job_id
                self.secondCardTime.text = orderHistory.tasksOfOrder[i].job_delivery_datetime.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd'T'HH:mm:ss.000Z", output: "hh:mm a, dd MMM yyyy")
                let temp = orderHistory.tasksOfOrder[i].job_status!.getTextAndColor(vertical: orderHistory.tasksOfOrder[i].vertical)
                self.secondCardStatus.text = temp.0
                self.secondCardStatus.backgroundColor = temp.1
                self.secondViewPickupDeliveryTag.setTitle("D", for: .normal)
                self.secondCardJobType.text = TEXT.DELIVERY
            }
        }
    }
    
    func setSingleTaskCard(orderHistory:OrderDetails) {
        self.setTextToBlank()
        self.setConstraintToZero()
        self.firstCard.isHidden = true
        self.pickupDeliveryTag.isHidden = true
        self.secondViewPickupDeliveryTag.isHidden = false
        self.firstBigDot.isHidden = false
        self.lastBigDot.isHidden = false
        self.trackingLine.isHidden = false
        self.leadingConstraint.constant = leftMarginWithDot

        switch orderHistory.job_type {
        case JOB_TYPE.pickup:
            self.secondCardAddress.text = orderHistory.job_pickup_address.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.secondCardTaskId.text = "#" + orderHistory.job_id
            self.secondCardTime.text = orderHistory.job_pickup_datetime.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd'T'HH:mm:ss.000Z", output: "hh:mm a, dd MMM yyyy")
            let temp = orderHistory.job_status!.getTextAndColor(vertical: orderHistory.vertical)
            self.secondCardStatus.text = temp.0
            self.secondCardStatus.backgroundColor = temp.1
//            self.secondViewPickupDeliveryTag.setTitle("P", for: .normal)
//            self.secondCardJobType.text = TEXT.PICKUP
            
            if orderHistory.vertical == .taxi {
                self.secondViewPickupDeliveryTag.setTitle("T", for: .normal)
                self.secondCardJobType.text = TEXT.Taxi
            }else if orderHistory.vertical == .multipleCategory {
                self.secondViewPickupDeliveryTag.setTitle("L", for: .normal)
                self.secondCardJobType.text = TEXT.Laundry
            }else{
                self.secondViewPickupDeliveryTag.setTitle("P", for: .normal)
                self.secondCardJobType.text = TEXT.PICKUP
            }
            
            
            
            break
            
        case JOB_TYPE.delivery:
            self.secondCardAddress.text = orderHistory.job_address.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            self.secondCardTaskId.text = "#"+orderHistory.job_id
            self.secondCardTime.text = orderHistory.job_delivery_datetime.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd'T'HH:mm:ss.000Z", output: "hh:mm a, dd MMM yyyy")
            let temp = orderHistory.job_status!.getTextAndColor(vertical: orderHistory.vertical)
            self.secondCardStatus.text = temp.0
            self.secondCardStatus.backgroundColor = temp.1
            self.secondViewPickupDeliveryTag.setTitle("D", for: .normal)
            self.secondCardJobType.text = TEXT.DELIVERY
            break
            
        default:
            self.secondCardAddress.text = orderHistory.job_address
            self.secondCardTaskId.text = "#" + orderHistory.job_id
            self.secondCardTime.text = orderHistory.job_pickup_datetime.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd'T'HH:mm:ss.000Z", output: "dd, MMM yyyy, hh:mm a") + " - " + orderHistory.job_delivery_datetime.datefromYourInputFormatToOutputFormat(input: "yyyy-MM-dd'T'HH:mm:ss.000Z", output: "dd, MMM yyyy, hh:mm a")
            let temp = orderHistory.job_status!.getTextAndColor(vertical: orderHistory.vertical)
            self.secondCardStatus.text = temp.0
            self.secondCardStatus.backgroundColor = temp.1
            self.secondCardJobType.text = TEXT.DELIVERY
            self.secondViewPickupDeliveryTag.isHidden = true
            self.firstBigDot.isHidden = true
            self.lastBigDot.isHidden = true
            self.trackingLine.isHidden = true
            self.leadingConstraint.constant = leftMarginWithoutDot
            break
        }
    }

    func setConstraintToZero() {
        self.firstCardButtonHeightConstraint.constant = 0
        self.firstCardStatusHeightConstraint.constant = 0
        self.firstCardTopConstraint.constant = 0
        self.firstCardButtonTopConstraint.constant = 0
        self.secondCardTopConstraint.constant = self.secondCardTopValueWithoutFirstCard
    }
    
    func resetConstraint() {
        self.firstCardButtonHeightConstraint.constant = self.trackingButtonHeight
        self.firstCardStatusHeightConstraint.constant = self.statusCardHeight
        self.firstCardTopConstraint.constant = self.firstCardTopMargin
        self.firstCardButtonTopConstraint.constant = self.firstCardButtonTopMargin
        self.secondCardTopConstraint.constant = 0
    }
    
    func setTextToBlank() {
        self.firstCardAddress.text = ""
        self.firstCardTaskId.text = ""
        self.firstCardTimeText.text = ""
        self.firstCardJobType.text = ""
        self.firstCardTimeLabel.text = ""
        self.firstCardJobTypeHeader.text = ""
        self.firstCardTaskIdHeader.text = ""
        self.firstCardAddressHeader.text = ""
        self.firstCardStatus.text = ""
    }
    
    func setDefaultText() {
        self.firstCardTimeLabel.text = TEXT.DATE_TIME
        self.firstCardJobTypeHeader.text = TEXT.TYPE
        self.firstCardTaskIdHeader.text = Singleton.sharedInstance.formDetailsInfo.call_tasks_as + " " + "ID"
        self.firstCardAddressHeader.text = TEXT.ADDRESS_PLACEHODLER

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
