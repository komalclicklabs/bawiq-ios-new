//
//  CartViewViewController.swift
//  TookanVendor
//
//  Created by cl-macmini-57 on 26/04/17.
//  Copyright © 2017 clicklabs. All rights reserved.
//

import UIKit

class CartViewViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var addAddressButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backgroundShadowView: UIView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    var subTotal = Double()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTitleView()
        getSubTotal()
        tableView.register(UINib(nibName: NIB_NAME.cartViewCell, bundle: Bundle.main), forCellReuseIdentifier: CELL_IDENTIFIER.cartCell)
        tableView.separatorStyle = .none
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 200
        tableView.dataSource = self
        tableView.delegate = self
        
        backgroundShadowView.layer.shadowColor = UIColor.black.cgColor
        backgroundShadowView.layer.shadowOpacity = 0.4
        backgroundShadowView.layer.shadowOffset = CGSize(width: 2, height: 4)
        backgroundShadowView.layer.shadowRadius = 4
        addAddressButton.backgroundColor = COLOR.THEME_FOREGROUND_COLOR
        addAddressButton.setTitle(TEXT.SetAddress, for: UIControlState.normal)
        addAddressButton.setTitleColor(COLOR.LOGIN_BUTTON_TITLE_COLOR, for: UIControlState.normal)
        addAddressButton.setCornerRadius(radius: 50/2)
        addAddressButton.setShadow()
        addAddressButton.titleLabel?.font = UIFont(name: FONT.regular, size: FONT_SIZE.medium)
        
     //   setTaskDescription()
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
             return Singleton.sharedInstance.typeCategories.count
        default:
            return 1
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableViewHeight.constant = tableView.contentSize.height
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER.cartCell, for: indexPath) as? cartViewCell
        switch indexPath.section {
        case 0:
            cell?.setCellLabelsText(quantity: Singleton.sharedInstance.typeCategories[indexPath.row].quantity, productName: Singleton.sharedInstance.typeCategories[indexPath.row].carTypeName, finalAmmount:  Double(Singleton.sharedInstance.typeCategories[indexPath.row].quantity) * Double(Singleton.sharedInstance.typeCategories[indexPath.row].baseFare)!)
            cell?.backgroundColor = COLOR.SPLASH_BACKGROUND_COLOR
            return cell!
        default:
            cell?.setCellLabelsText(quantity: 0,productName: "Grand Total", finalAmmount: self.subTotal)
            cell?.backgroundColor = COLOR.REVIEW_BACKGROUND_COLOR
            return cell!
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
            case 0:
        if Singleton.sharedInstance.typeCategories[indexPath.row].quantity == 0 {
            return 0
        }else{
            return UITableViewAutomaticDimension
        }
         default:
            return UITableViewAutomaticDimension
        }
    }
    
    @IBAction func backAction(_ sender: UIButton) {
     _ = self.navigationController?.popViewController(animated:true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func getSubTotal(){
        subTotal = 0.0
         Singleton.sharedInstance.selectedData = ""
        for i in Singleton.sharedInstance.typeCategories{
            if i.quantity > 0{
                subTotal = subTotal + Double(i.quantity)*Double(i.baseFare)!
                if Singleton.sharedInstance.selectedData == ""{
                    
                    Singleton.sharedInstance.selectedData =  "\(i.quantity)x \(i.carTypeName)    \(Singleton.sharedInstance.formDetailsInfo.currencyid)\( Double(i.quantity)*Double(i.baseFare)!)\n"
                }else{
                    Singleton.sharedInstance.selectedData =  Singleton.sharedInstance.selectedData + "\(i.quantity)x \(i.carTypeName)    \(Singleton.sharedInstance.formDetailsInfo.currencyid)\( Double(i.quantity)*Double(i.baseFare)!)\n"
                }
            }
            
        }
        Singleton.sharedInstance.subTotal = self.subTotal
        self.tableView.reloadData()
    }

    func setTitleView() {
        let titleView = UINib(nibName: NIB_NAME.titleView, bundle: nil).instantiate(withOwner: self, options: nil)[0] as! TitleView
        titleView.frame = CGRect(x: 0, y: HEIGHT.navigationHeight, width: self.view.frame.width, height: HEIGHT.titleHeight)
        titleView.backgroundColor = UIColor.clear
        self.view.addSubview(titleView)
        titleView.setTitle(title: TEXT.ReviewThe, boldTitle: TEXT.basket)
    }
    
    @IBAction func setAddressAction(_ sender: UIButton) {
         let vc = UIStoryboard(name: STORYBOARD_NAME.afterLogin, bundle: Bundle.main).instantiateViewController(withIdentifier: STORYBOARD_ID.homeController) as? HomeVC
        //vc?.hideBackButton = false
        self.navigationController?.pushViewController(vc!,animated:true)
    }
//    
//    func setTaskDescription(){
//        Singleton.sharedInstance.createTaskDetail.jobDescription = ""  
//        for i in Singleton.sharedInstance.typeCategories{
//            if i.quantity != 0{
//                if Singleton.sharedInstance.createTaskDetail.description == ""{
//                Singleton.sharedInstance.createTaskDetail.jobDescription =  "\("\(i.quantity)x \(i.carTypeName)")\n"
//                }else{
//                     Singleton.sharedInstance.createTaskDetail.jobDescription =  Singleton.sharedInstance.createTaskDetail.jobDescription + " \("\(i.quantity)x \(i.carTypeName)")\n"
//                }
//            }
//        }
//    }
    
    
}
