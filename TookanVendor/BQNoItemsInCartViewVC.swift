//
//  BQNoItemsInCartViewVC.swift
//  TookanVendor
//
//  Created by cl-lap-147 on 08/05/18.
//  Copyright © 2018 clicklabs. All rights reserved.
//

import UIKit


protocol BQNoItemsInCartViewVCDelegate: class {
    func startShopping()
}

class BQNoItemsInCartViewVC: UIViewController {

    @IBOutlet weak var btnStartShopping: UIButton!
    weak var delegate: BQNoItemsInCartViewVCDelegate? = nil
    var navigationBar:NavigationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        btnStartShopping.layer.cornerRadius = 26.0
        self.setNavigationBar()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func startShoppingButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - NAVIGATION BAR
    private func setNavigationBar() {
        //Use for showing two right bar buttons on navigation bar
        
        navigationBar = NavigationView.getNibFileForTwoRightButtons(params: NavigationView.NavigationViewParams(leftButtonTitle: "", rightButtonTitle: "", title: "Cart", leftButtonImage: #imageLiteral(resourceName: "iconBackTitleBar"), rightButtonImage: nil, secondRightButtonImage: nil)
            , leftButtonAction: {[weak self] in
                self?.backAction()
            }, rightButtonAction: nil,
               rightButtonSecondAction: nil
        )
        navigationBar?.setBackgroundColor(color: COLOR.App_Red_COLOR, andTintColor: .white)
        self.view.addSubview(navigationBar)
    }
    
    // MARK: - IBAction
    func backAction() {
        self.dismiss(animated: true, completion: nil)
    }

}
